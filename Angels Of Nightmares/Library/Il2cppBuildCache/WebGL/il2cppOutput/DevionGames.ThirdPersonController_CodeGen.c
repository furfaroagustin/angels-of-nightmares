﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Instantiate::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void Instantiate_OnStateUpdate_m36B64CE3E70FF40D43C32868E8427700AD0F6A02 (void);
// 0x00000002 System.Void Instantiate::.ctor()
extern void Instantiate__ctor_mF1498BA1474FA6BDCF17B820DCF1888CF05BAC34 (void);
// 0x00000003 System.Void PlaySound::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void PlaySound_OnStateEnter_mFF6D33B1A1519D3DC46A5C8941CB998DF5871D30 (void);
// 0x00000004 System.Void PlaySound::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void PlaySound_OnStateUpdate_mD571D80F0F61D218A03E3BB2DDDD3C9D54A870FE (void);
// 0x00000005 System.Void PlaySound::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void PlaySound_OnStateExit_m433D6DBB993C131988DC2861EA5257D6661F5E00 (void);
// 0x00000006 System.Void PlaySound::Play(UnityEngine.AudioClip,System.Single)
extern void PlaySound_Play_m284B6A96DCFF9AD0FCC227EEB17F0214E6AEA797 (void);
// 0x00000007 System.Void PlaySound::.ctor()
extern void PlaySound__ctor_mE2894569DE25F7E4176462EA3B82488AAB5AA262 (void);
// 0x00000008 System.String DevionGames.CameraSettings::get_Name()
extern void CameraSettings_get_Name_m2F5F2F18B94E370F29020E0F6811BADD161702BD (void);
// 0x00000009 System.Void DevionGames.CameraSettings::set_Name(System.String)
extern void CameraSettings_set_Name_mF74BAF475E1E9953A232E7C6D1F4548DD419BE6B (void);
// 0x0000000A System.String DevionGames.CameraSettings::get_InputName()
extern void CameraSettings_get_InputName_m074BE02D8B2478E10045094BAB47A6199A008F89 (void);
// 0x0000000B System.Void DevionGames.CameraSettings::set_InputName(System.String)
extern void CameraSettings_set_InputName_mBDCBEAD19D08B5543C00BAF41E7FD528FE69258C (void);
// 0x0000000C DevionGames.CameraSettings/ActivationType DevionGames.CameraSettings::get_Activation()
extern void CameraSettings_get_Activation_m75FE5269D56A5DB49099CE121D101063DA721EF7 (void);
// 0x0000000D System.Void DevionGames.CameraSettings::set_Activation(DevionGames.CameraSettings/ActivationType)
extern void CameraSettings_set_Activation_m632C11EC4C8ED23934998AB3E3D4497FCA4AC40E (void);
// 0x0000000E UnityEngine.Vector2 DevionGames.CameraSettings::get_Offset()
extern void CameraSettings_get_Offset_m3866C7B1D6194C365CC3E289444D9E9CE58F658B (void);
// 0x0000000F System.Void DevionGames.CameraSettings::set_Offset(UnityEngine.Vector2)
extern void CameraSettings_set_Offset_m330BA68BC431AA74E751B751EDB0A08D093E5D1A (void);
// 0x00000010 System.Single DevionGames.CameraSettings::get_Distance()
extern void CameraSettings_get_Distance_m1B9FCFBFCF18E533D08CE29CF3C1C2D622B67E7F (void);
// 0x00000011 System.Void DevionGames.CameraSettings::set_Distance(System.Single)
extern void CameraSettings_set_Distance_m9D915936C4DEA68C489EE91EF59650B8AB5461E8 (void);
// 0x00000012 UnityEngine.Sprite DevionGames.CameraSettings::get_Crosshair()
extern void CameraSettings_get_Crosshair_m38400FF3C6A7E5C7EC8BD44F6448A8EF9D6C776D (void);
// 0x00000013 System.Void DevionGames.CameraSettings::set_Crosshair(UnityEngine.Sprite)
extern void CameraSettings_set_Crosshair_m628D3969E8F965F50B6DA2E8FA84749FB1EAA1A5 (void);
// 0x00000014 System.String DevionGames.CameraSettings::get_TurnButton()
extern void CameraSettings_get_TurnButton_mC142CB853F51B97DAEA3E19ED4AFF9D71E6DA16F (void);
// 0x00000015 System.Void DevionGames.CameraSettings::set_TurnButton(System.String)
extern void CameraSettings_set_TurnButton_mBB9E710963BD9CBFDE5A5715DC62926EA700C678 (void);
// 0x00000016 System.Single DevionGames.CameraSettings::get_TurnSpeed()
extern void CameraSettings_get_TurnSpeed_m17C6AE46F8BCAF83B73E01689656B9C51DD84202 (void);
// 0x00000017 System.Void DevionGames.CameraSettings::set_TurnSpeed(System.Single)
extern void CameraSettings_set_TurnSpeed_m68D89B9A7A9E7117AB7809EEBE7880E5E7492721 (void);
// 0x00000018 System.Single DevionGames.CameraSettings::get_TurnSmoothing()
extern void CameraSettings_get_TurnSmoothing_m3683449822AE33204DD17B75EA18D3C83354B4D8 (void);
// 0x00000019 System.Void DevionGames.CameraSettings::set_TurnSmoothing(System.Single)
extern void CameraSettings_set_TurnSmoothing_m4477BDE8AF44446A16A952C921CB1439638B4D02 (void);
// 0x0000001A UnityEngine.Vector2 DevionGames.CameraSettings::get_YawLimit()
extern void CameraSettings_get_YawLimit_m791F93A81F117FDEE50BBD73A4EBAD7FAB93989B (void);
// 0x0000001B System.Void DevionGames.CameraSettings::set_YawLimit(UnityEngine.Vector2)
extern void CameraSettings_set_YawLimit_m2CACE52FF6DF2608CC0D6FE1EE1261E324F0A81C (void);
// 0x0000001C UnityEngine.Vector2 DevionGames.CameraSettings::get_PitchLimit()
extern void CameraSettings_get_PitchLimit_mC5E51AB9E417115B568C0CDF0C6DC572A488DD34 (void);
// 0x0000001D System.Void DevionGames.CameraSettings::set_PitchLimit(UnityEngine.Vector2)
extern void CameraSettings_set_PitchLimit_mD0AFBF06E2C8B04A9A5B3200BE24AFD8819DB119 (void);
// 0x0000001E System.Single DevionGames.CameraSettings::get_VisibilityDelta()
extern void CameraSettings_get_VisibilityDelta_mA7A46E27215123B91C63143A685014F4BF554B3C (void);
// 0x0000001F System.Void DevionGames.CameraSettings::set_VisibilityDelta(System.Single)
extern void CameraSettings_set_VisibilityDelta_mFC55FD26E08B3D8180E6357A46DDC1450D8AC58A (void);
// 0x00000020 System.Single DevionGames.CameraSettings::get_ZoomSpeed()
extern void CameraSettings_get_ZoomSpeed_mC1FB1A62066F4219FC23333D3070C51C4C771FA0 (void);
// 0x00000021 System.Void DevionGames.CameraSettings::set_ZoomSpeed(System.Single)
extern void CameraSettings_set_ZoomSpeed_mA1CCFC97AC36BEA59F2782EE790E2392E7336723 (void);
// 0x00000022 UnityEngine.Vector2 DevionGames.CameraSettings::get_ZoomLimit()
extern void CameraSettings_get_ZoomLimit_m36F7B9184B6708EC52BBCB66E49C6504D7245D93 (void);
// 0x00000023 System.Void DevionGames.CameraSettings::set_ZoomLimit(UnityEngine.Vector2)
extern void CameraSettings_set_ZoomLimit_m83DF5CC2040B54A2F617C0FE5CBD580F3B2DAC9C (void);
// 0x00000024 System.Single DevionGames.CameraSettings::get_ZoomSmoothing()
extern void CameraSettings_get_ZoomSmoothing_m4FAC20ADCABCD683F2DBA67276409E7787A58B38 (void);
// 0x00000025 System.Void DevionGames.CameraSettings::set_ZoomSmoothing(System.Single)
extern void CameraSettings_set_ZoomSmoothing_mC199F5187A4D62B6D9B03B14A7FD94C2688A731C (void);
// 0x00000026 System.Single DevionGames.CameraSettings::get_MoveSmoothing()
extern void CameraSettings_get_MoveSmoothing_mAB28154580A4E5D2945BB30006F1CF86E908C960 (void);
// 0x00000027 System.Void DevionGames.CameraSettings::set_MoveSmoothing(System.Single)
extern void CameraSettings_set_MoveSmoothing_m9A43EF77E557FC8A6BA977949EAEFED8563C9535 (void);
// 0x00000028 UnityEngine.CursorLockMode DevionGames.CameraSettings::get_CursorMode()
extern void CameraSettings_get_CursorMode_m7AEDBCCA92348B286FDC2AEC0412C02BF2E5F65E (void);
// 0x00000029 System.Void DevionGames.CameraSettings::set_CursorMode(UnityEngine.CursorLockMode)
extern void CameraSettings_set_CursorMode_m35AF59286F39A5BFDFC23B15064333286F2D05C4 (void);
// 0x0000002A System.Boolean DevionGames.CameraSettings::get_ConsumeInputOverUI()
extern void CameraSettings_get_ConsumeInputOverUI_mFD10B7749694328332B707912866C56DE0AAA87C (void);
// 0x0000002B System.Void DevionGames.CameraSettings::set_ConsumeInputOverUI(System.Boolean)
extern void CameraSettings_set_ConsumeInputOverUI_m3A9FFC055E08003FFD5AA14E43EA3DC9E2AEDB0E (void);
// 0x0000002C UnityEngine.LayerMask DevionGames.CameraSettings::get_CollisionLayer()
extern void CameraSettings_get_CollisionLayer_m4F624E88B931C0E494EF47CDA9DCBBC099EB2953 (void);
// 0x0000002D System.Void DevionGames.CameraSettings::set_CollisionLayer(UnityEngine.LayerMask)
extern void CameraSettings_set_CollisionLayer_m02FF188E6B533EC51C9AE8624B8FA9FD30F2A6A5 (void);
// 0x0000002E System.Single DevionGames.CameraSettings::get_CollisionRadius()
extern void CameraSettings_get_CollisionRadius_m5AFBA1BCE44FA3C1FBEC1F5F7892A85519932907 (void);
// 0x0000002F System.Void DevionGames.CameraSettings::set_CollisionRadius(System.Single)
extern void CameraSettings_set_CollisionRadius_mEDF27AEAA9A80553AB2C56739C8FA70CDCA6F8F0 (void);
// 0x00000030 System.Boolean DevionGames.CameraSettings::get_IsActive()
extern void CameraSettings_get_IsActive_m3CB334BA2F41488F4D5BEF6112EA8C061D88EE1E (void);
// 0x00000031 System.Void DevionGames.CameraSettings::set_IsActive(System.Boolean)
extern void CameraSettings_set_IsActive_m0826D626219FFDC147BC399403FF6218AB1D2A6D (void);
// 0x00000032 System.Single DevionGames.CameraSettings::get_Zoom()
extern void CameraSettings_get_Zoom_m4774EDD7870F1088A1073BBBDE30B4E66F44A216 (void);
// 0x00000033 System.Void DevionGames.CameraSettings::set_Zoom(System.Single)
extern void CameraSettings_set_Zoom_m543F9A9606367C14F641A68304D1BFEB6696A450 (void);
// 0x00000034 System.Void DevionGames.CameraSettings::.ctor()
extern void CameraSettings__ctor_mE62990BBBFCD7D3E914477A3306B71368DA853AA (void);
// 0x00000035 System.Void DevionGames.FocusTarget::Start()
extern void FocusTarget_Start_mF8ACAD4B4B06E0D893755F4C05D10327A14B15AC (void);
// 0x00000036 System.Void DevionGames.FocusTarget::Update()
extern void FocusTarget_Update_m527CC87535BE59B9A405418E9FC8E4BDD17ED772 (void);
// 0x00000037 System.Void DevionGames.FocusTarget::Focus(System.Boolean)
extern void FocusTarget_Focus_m8E71117CFBC78AF8C268D193F5AC7659A308CEC8 (void);
// 0x00000038 System.Void DevionGames.FocusTarget::.ctor()
extern void FocusTarget__ctor_m70339E68778E09FCC38408B752E7FAA042D62603 (void);
// 0x00000039 UnityEngine.Transform DevionGames.ThirdPersonCamera::get_Target()
extern void ThirdPersonCamera_get_Target_m135A7718937E473F35CD5576736F8EBF6E396980 (void);
// 0x0000003A DevionGames.CameraSettings[] DevionGames.ThirdPersonCamera::get_Presets()
extern void ThirdPersonCamera_get_Presets_mB220A126E2968E4298431C663AB9C7D51B0DA319 (void);
// 0x0000003B System.Void DevionGames.ThirdPersonCamera::Start()
extern void ThirdPersonCamera_Start_mED52773B1B4079C894A1C8666D63952F4D5110B8 (void);
// 0x0000003C System.Void DevionGames.ThirdPersonCamera::OnEnable()
extern void ThirdPersonCamera_OnEnable_m5CAB66441B940CDB322F1147E1ACDA978C9A6E5F (void);
// 0x0000003D System.Void DevionGames.ThirdPersonCamera::OnDisable()
extern void ThirdPersonCamera_OnDisable_mC3C76890E789464F60E8F330F8634B6EEE50C832 (void);
// 0x0000003E System.Void DevionGames.ThirdPersonCamera::OnSetControllerActive(System.Boolean)
extern void ThirdPersonCamera_OnSetControllerActive_m353ED80C26DF796678FB7B07C19D1D76F4E7A497 (void);
// 0x0000003F System.Void DevionGames.ThirdPersonCamera::LateUpdate()
extern void ThirdPersonCamera_LateUpdate_m9721845907F1441F6C8A8949EB9440D16ABD089F (void);
// 0x00000040 System.Void DevionGames.ThirdPersonCamera::FixedUpdate()
extern void ThirdPersonCamera_FixedUpdate_m5F8C7CAFEBCD439C28F037294958BE6EE4CDD905 (void);
// 0x00000041 System.Void DevionGames.ThirdPersonCamera::UpdateTransform()
extern void ThirdPersonCamera_UpdateTransform_m6369B8526E00E22501A856A2C906AB03C238E10D (void);
// 0x00000042 System.Void DevionGames.ThirdPersonCamera::UpdateInput()
extern void ThirdPersonCamera_UpdateInput_m272A78FA8280BF463260B1E669C7C247AD8F85FA (void);
// 0x00000043 System.Single DevionGames.ThirdPersonCamera::ClampAngle(System.Single,System.Single,System.Single)
extern void ThirdPersonCamera_ClampAngle_mEB3C5CE36CDAB23299A9AFAB856C50CB3BDA0547 (void);
// 0x00000044 System.Void DevionGames.ThirdPersonCamera::ApplyCrosshair(UnityEngine.Sprite)
extern void ThirdPersonCamera_ApplyCrosshair_mA504A905D0310F650E539894E8AE7648443566EC (void);
// 0x00000045 System.Void DevionGames.ThirdPersonCamera::CreateCrosshairUI()
extern void ThirdPersonCamera_CreateCrosshairUI_m1A21B6B226B3540D321AFE92DB4AD3DC3DA2E7ED (void);
// 0x00000046 System.Void DevionGames.ThirdPersonCamera::.ctor()
extern void ThirdPersonCamera__ctor_m02A2D020EED2DE325F97F1962B62ECCF71A345EA (void);
// 0x00000047 System.Void DevionGames.CharacterIK::Start()
extern void CharacterIK_Start_mBAF6688F04C077BD146E5299E265047A2856A3CC (void);
// 0x00000048 System.Void DevionGames.CharacterIK::Update()
extern void CharacterIK_Update_m23E4BF47E7C8753AF25FE1E0F0E1C5004E408D6F (void);
// 0x00000049 System.Void DevionGames.CharacterIK::OnSetControllerActive(System.Boolean)
extern void CharacterIK_OnSetControllerActive_m6730B36DF30D884FEF006192CB3EE0591045086A (void);
// 0x0000004A System.Void DevionGames.CharacterIK::SetIK(System.Boolean)
extern void CharacterIK_SetIK_m9905D5CC94A53B37A8F83E758C357055A58D68F9 (void);
// 0x0000004B System.Void DevionGames.CharacterIK::OnAnimatorIK(System.Int32)
extern void CharacterIK_OnAnimatorIK_m235619FCCF9C3EA298DE8F74D75C31789828A6A7 (void);
// 0x0000004C System.Void DevionGames.CharacterIK::.ctor()
extern void CharacterIK__ctor_m8B1445F68D69B1614C6A3743E31BBC3B28E5C912 (void);
// 0x0000004D System.Void DevionGames.IControllerGrounded::OnControllerGrounded(System.Boolean)
// 0x0000004E System.Void DevionGames.IControllerAim::OnControllerAim(System.Boolean)
// 0x0000004F System.String DevionGames.MotionState::get_FriendlyName()
extern void MotionState_get_FriendlyName_m2E37D82936656FE87D1B56F80C17FDC064A77DD3 (void);
// 0x00000050 System.String DevionGames.MotionState::get_InputName()
extern void MotionState_get_InputName_m6ED2F3C38784BA9132A99B4469CF2D254239B294 (void);
// 0x00000051 System.Void DevionGames.MotionState::set_InputName(System.String)
extern void MotionState_set_InputName_m62C2FA34D18C533AEC4984392A716DC7FB18116D (void);
// 0x00000052 DevionGames.StartType DevionGames.MotionState::get_StartType()
extern void MotionState_get_StartType_mC31B140194F959BAAB82089031DCCD3CA18F3C33 (void);
// 0x00000053 System.Void DevionGames.MotionState::set_StartType(DevionGames.StartType)
extern void MotionState_set_StartType_mAA385808BFD6B881FBEDE4C290BA001AF5F256F1 (void);
// 0x00000054 DevionGames.StopType DevionGames.MotionState::get_StopType()
extern void MotionState_get_StopType_m322BBB22ADDC451E4CE4082BA3064DCC70513BCB (void);
// 0x00000055 System.Void DevionGames.MotionState::set_StopType(DevionGames.StopType)
extern void MotionState_set_StopType_mA7A9E318BF4272BB022619300AEB79B792E6C3DE (void);
// 0x00000056 System.Boolean DevionGames.MotionState::get_ConsumeInputOverUI()
extern void MotionState_get_ConsumeInputOverUI_m16294BF35B820EDC16086B1A918D97A93064CA42 (void);
// 0x00000057 System.Boolean DevionGames.MotionState::get_PauseItemUpdate()
extern void MotionState_get_PauseItemUpdate_m5744E5C2811EC4119F79F4C6B6A0C21C4FCB200A (void);
// 0x00000058 System.Single DevionGames.MotionState::get_TransitionDuration()
extern void MotionState_get_TransitionDuration_m9A9ECDCFACC53685647EA5704ACE79B8D45056AC (void);
// 0x00000059 System.String DevionGames.MotionState::get_State()
extern void MotionState_get_State_m68505E49A9692B31214C2CA6B9A9BD820F1B4027 (void);
// 0x0000005A System.Void DevionGames.MotionState::set_State(System.String)
extern void MotionState_set_State_m26D42BC25AE67B1FA509B68E6898824ED4D5159F (void);
// 0x0000005B System.String DevionGames.MotionState::get_CameraPreset()
extern void MotionState_get_CameraPreset_m4A55C028C70F6119BADAAA5DB8CF7C5426D5F78A (void);
// 0x0000005C System.Void DevionGames.MotionState::set_CameraPreset(System.String)
extern void MotionState_set_CameraPreset_m2DEBF67B9847A126A21073E7C35920990E1FA88B (void);
// 0x0000005D System.Boolean DevionGames.MotionState::get_IsActive()
extern void MotionState_get_IsActive_m9186282C449725307BFE805F642CBFB6B2BFBC45 (void);
// 0x0000005E System.Int32 DevionGames.MotionState::get_Index()
extern void MotionState_get_Index_m9BBC0BC8A6A6778105ADCA94CF18F0A0ADCB2D15 (void);
// 0x0000005F System.Void DevionGames.MotionState::set_Index(System.Int32)
extern void MotionState_set_Index_m9127DF697416A51771189885D1E2DDCB55CD525F (void);
// 0x00000060 System.Int32 DevionGames.MotionState::get_Layer()
extern void MotionState_get_Layer_mE2E30DB7E89A3BF0C593B5E16311B50EB72D6CD1 (void);
// 0x00000061 System.Void DevionGames.MotionState::set_Layer(System.Int32)
extern void MotionState_set_Layer_mE6620BFFE63CF0CEDE192AC906AFB87F28CEB8A4 (void);
// 0x00000062 DevionGames.ThirdPersonController DevionGames.MotionState::get_Controller()
extern void MotionState_get_Controller_m0726C169E4762550E883C15E366C062A373CF59D (void);
// 0x00000063 System.Void DevionGames.MotionState::set_Controller(DevionGames.ThirdPersonController)
extern void MotionState_set_Controller_m25813DCC6F47C18E0EB2F041AB6BEAE4BAF86A89 (void);
// 0x00000064 System.Void DevionGames.MotionState::Start()
extern void MotionState_Start_m2EA152613E852CEE0B88E6DC7B9A3FC9B977D9F2 (void);
// 0x00000065 System.Void DevionGames.MotionState::StopMotion()
extern void MotionState_StopMotion_mC06D24B24D5CEBA0390A7FEFD0F0A8C39E094083 (void);
// 0x00000066 System.Void DevionGames.MotionState::StopMotion(System.Boolean)
extern void MotionState_StopMotion_m2876A004AE5EC7C8C87599629A71DE0C0E4717A8 (void);
// 0x00000067 System.Void DevionGames.MotionState::StartMotion()
extern void MotionState_StartMotion_m7288F39BF6521D3A3F97C4282430685B04B2E63A (void);
// 0x00000068 System.Boolean DevionGames.MotionState::IsPlaying()
extern void MotionState_IsPlaying_m9BE6C15D262AAFF324C2157F4B77AFA294CAC2CA (void);
// 0x00000069 System.Boolean DevionGames.MotionState::CanStart()
extern void MotionState_CanStart_m7CBE8D33ADC3A4D94C6225C908DB066C4AA2AA60 (void);
// 0x0000006A System.Void DevionGames.MotionState::OnStart()
extern void MotionState_OnStart_mAA277DED77A6D3DD5E1D3A879CD9AFEC4141E3A6 (void);
// 0x0000006B System.Boolean DevionGames.MotionState::UpdateVelocity(UnityEngine.Vector3&)
extern void MotionState_UpdateVelocity_m3FA2496BDF171AD5EF220221202AAE7AAB4EF171 (void);
// 0x0000006C System.Boolean DevionGames.MotionState::UpdateRotation()
extern void MotionState_UpdateRotation_mD76ADDE0F08412308FD89B171BF8B1154F0BB4D2 (void);
// 0x0000006D System.Boolean DevionGames.MotionState::UpdateAnimator()
extern void MotionState_UpdateAnimator_m11FB1B75174F0495DE737F3986CCEC60439278BD (void);
// 0x0000006E System.Boolean DevionGames.MotionState::UpdateAnimatorIK(System.Int32)
extern void MotionState_UpdateAnimatorIK_mE5D69456189DF88DBFA1C3E8BC4B88E51147803E (void);
// 0x0000006F System.Boolean DevionGames.MotionState::CheckGround()
extern void MotionState_CheckGround_m9AB281E59548CF357053976B133387F2F6323064 (void);
// 0x00000070 System.Boolean DevionGames.MotionState::CheckStep()
extern void MotionState_CheckStep_mA5E6F0B06971F22162D41330EDE482D13FF2AD02 (void);
// 0x00000071 System.Boolean DevionGames.MotionState::CanStop()
extern void MotionState_CanStop_mF01B2EEA242EC27B83C827D05D1DBD8A2FBABA24 (void);
// 0x00000072 System.Void DevionGames.MotionState::OnStop()
extern void MotionState_OnStop_mE81FF86754C3ACC418E590A32E9D564A2C0CB74D (void);
// 0x00000073 System.String DevionGames.MotionState::GetDestinationState()
extern void MotionState_GetDestinationState_m8CC202AE9980D38D4D96CF7BA5ECA19BE67D7872 (void);
// 0x00000074 System.Void DevionGames.MotionState::MoveToTarget(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Action)
extern void MotionState_MoveToTarget_m429F292A47C2BAE03BE73785E4A91148174643BB (void);
// 0x00000075 System.Collections.IEnumerator DevionGames.MotionState::MoveToTargetInternal(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Action)
extern void MotionState_MoveToTargetInternal_m72F3F436692B580CD4928242CCBB777E42D9F73E (void);
// 0x00000076 System.Void DevionGames.MotionState::.ctor()
extern void MotionState__ctor_m7D12BFEFD99B9DC8409C56FFC363F93148B68AC4 (void);
// 0x00000077 System.Boolean DevionGames.MotionState::<StopMotion>b__55_0(DevionGames.CameraSettings)
extern void MotionState_U3CStopMotionU3Eb__55_0_m2EC938B7561925B6F1F570CCA17EDC47640CBAF2 (void);
// 0x00000078 System.Boolean DevionGames.MotionState::<StartMotion>b__56_0(DevionGames.CameraSettings)
extern void MotionState_U3CStartMotionU3Eb__56_0_mCAB72567DA15AC8E57D4880F4C11C5365AF9A1B7 (void);
// 0x00000079 System.Void DevionGames.MotionState/<MoveToTargetInternal>d__70::.ctor(System.Int32)
extern void U3CMoveToTargetInternalU3Ed__70__ctor_mE0912DBF67EA22130A0FAC617B41761DFB0103EB (void);
// 0x0000007A System.Void DevionGames.MotionState/<MoveToTargetInternal>d__70::System.IDisposable.Dispose()
extern void U3CMoveToTargetInternalU3Ed__70_System_IDisposable_Dispose_m023D24E65C4F951C4520F067C13E7B7359BA5575 (void);
// 0x0000007B System.Boolean DevionGames.MotionState/<MoveToTargetInternal>d__70::MoveNext()
extern void U3CMoveToTargetInternalU3Ed__70_MoveNext_mC160CD9C016EF209CA9E806E359FF1920385BF94 (void);
// 0x0000007C System.Object DevionGames.MotionState/<MoveToTargetInternal>d__70::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveToTargetInternalU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58C1F09B256FCDB31569F143F4E939C24B2CC96D (void);
// 0x0000007D System.Void DevionGames.MotionState/<MoveToTargetInternal>d__70::System.Collections.IEnumerator.Reset()
extern void U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_Reset_m4FCAF97E4E9531B458C76E05942EFD943AB035A1 (void);
// 0x0000007E System.Object DevionGames.MotionState/<MoveToTargetInternal>d__70::System.Collections.IEnumerator.get_Current()
extern void U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_get_Current_m015E37275E63AD25EC7633D4814097579FCE2700 (void);
// 0x0000007F System.Void DevionGames.MotionTrigger::.ctor()
extern void MotionTrigger__ctor_m99A4BB05D312956038CBD765FB06CFA3B6A8D8F7 (void);
// 0x00000080 System.Void DevionGames.ChangeHeight::OnStart()
extern void ChangeHeight_OnStart_mBE2AB8C65A894538CD5B0402CCE6088A64FA45B6 (void);
// 0x00000081 System.Void DevionGames.ChangeHeight::OnStop()
extern void ChangeHeight_OnStop_m2F9CACD3F11147107FFD0072A70AE81AE38FC351 (void);
// 0x00000082 System.Void DevionGames.ChangeHeight::.ctor()
extern void ChangeHeight__ctor_m9D5EA7291013DC36D3762845C0D6A29AFAEBF122 (void);
// 0x00000083 System.Void DevionGames.ChangeSpeed::OnStart()
extern void ChangeSpeed_OnStart_mC8FBC3ED8C60ABD34473ED755028F158B8361F7F (void);
// 0x00000084 System.Void DevionGames.ChangeSpeed::OnStop()
extern void ChangeSpeed_OnStop_mAC9944D3ABE93311C68627D9741EFE25F5617F97 (void);
// 0x00000085 System.Boolean DevionGames.ChangeSpeed::CanStop()
extern void ChangeSpeed_CanStop_m66FBE793B98108D34DC59CA0C8FA761800722BBF (void);
// 0x00000086 System.Void DevionGames.ChangeSpeed::.ctor()
extern void ChangeSpeed__ctor_mEAE5C5E02111F68AFC585D31C39CFA6EEF760449 (void);
// 0x00000087 System.Void DevionGames.Climb::OnStart()
extern void Climb_OnStart_m413689D85F7F3C53753BA1663B87E3B9447B59DA (void);
// 0x00000088 System.Void DevionGames.Climb::OnStop()
extern void Climb_OnStop_m4C966990CDDBED2DAF75D49370063C7331A9E535 (void);
// 0x00000089 System.Boolean DevionGames.Climb::CanStart()
extern void Climb_CanStart_m38150F14FF2ACE192ACB318026F12C13A32BFCE4 (void);
// 0x0000008A System.Boolean DevionGames.Climb::UpdateAnimatorIK(System.Int32)
extern void Climb_UpdateAnimatorIK_m0B3C9E7729167A9F0FF838E3B725258EBA6128BD (void);
// 0x0000008B System.Void DevionGames.Climb::OnDrawGizmosSelected()
extern void Climb_OnDrawGizmosSelected_m6DB1D44BE8FE05104E311E9997B1C8935F1DBE0A (void);
// 0x0000008C System.Boolean DevionGames.Climb::DebugRay(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern void Climb_DebugRay_mE87CFF76E5750707BD33C3B33D4F7A85A8AF8A8A (void);
// 0x0000008D System.Boolean DevionGames.Climb::UpdateVelocity(UnityEngine.Vector3&)
extern void Climb_UpdateVelocity_m8E7F3F4A77716D66F71E959768AF7079B840A33F (void);
// 0x0000008E System.Void DevionGames.Climb::SetIKWeight(System.Single)
extern void Climb_SetIKWeight_mFA96105961A064ED4CEBF827A21CF943EBE4F491 (void);
// 0x0000008F System.Boolean DevionGames.Climb::UpdateRotation()
extern void Climb_UpdateRotation_m37A457F4C3008AC1D44B2939F3B2716E27D7127A (void);
// 0x00000090 System.Boolean DevionGames.Climb::CheckGround()
extern void Climb_CheckGround_m4E49A4955F2079B730B62AFB2986AD28EE4FF44C (void);
// 0x00000091 System.Boolean DevionGames.Climb::CheckStep()
extern void Climb_CheckStep_mD7C820CB5053D760A2F4EA5446AD576E86AC3C91 (void);
// 0x00000092 System.Void DevionGames.Climb::.ctor()
extern void Climb__ctor_m6B5B28F82C4B4560A5972E794767BD69F0E593DC (void);
// 0x00000093 System.Void DevionGames.Fall::OnStart()
extern void Fall_OnStart_m7CA3AD39DD0DCDD00AF5EB3F93209B20C0782899 (void);
// 0x00000094 System.Boolean DevionGames.Fall::UpdateVelocity(UnityEngine.Vector3&)
extern void Fall_UpdateVelocity_m3A3954522CE352CE5B8324D4F9DC4899481901BF (void);
// 0x00000095 System.Boolean DevionGames.Fall::UpdateAnimator()
extern void Fall_UpdateAnimator_m281E0E927403E3826DA2A91BB4BDF335F596906E (void);
// 0x00000096 System.Boolean DevionGames.Fall::CanStart()
extern void Fall_CanStart_m1CAF87A6D903A48DEC6773DD947D3864B1E751F5 (void);
// 0x00000097 System.Void DevionGames.Fall::OnControllerLanded()
extern void Fall_OnControllerLanded_mB052A5F7AB54387362C47AF2AFC31F17154E28CA (void);
// 0x00000098 System.Void DevionGames.Fall::OnControllerGrounded(System.Boolean)
extern void Fall_OnControllerGrounded_m24CE44325916D640D39005AA579C08FF6A9D3B58 (void);
// 0x00000099 System.Void DevionGames.Fall::.ctor()
extern void Fall__ctor_m66A7BE743BF7F78B4BD6DF1F72FB735CCA871D37 (void);
// 0x0000009A System.Void DevionGames.Jump::OnStart()
extern void Jump_OnStart_m265FF26E362B420CD8F16A409C244C59E3A78008 (void);
// 0x0000009B System.Boolean DevionGames.Jump::UpdateAnimator()
extern void Jump_UpdateAnimator_m72A3A13EB3F2D487C8121CB64EB9BAD343F9D615 (void);
// 0x0000009C System.Void DevionGames.Jump::StartJump()
extern void Jump_StartJump_m3674E2CC1559CABAC674707672DD46E68E891EC1 (void);
// 0x0000009D System.Boolean DevionGames.Jump::CheckGround()
extern void Jump_CheckGround_mE797CDD80464FBBC78D414CF1B35C935CCE68352 (void);
// 0x0000009E System.Boolean DevionGames.Jump::CheckStep()
extern void Jump_CheckStep_m1B906AC1068A9075B47F8FCA6B5C0CEAA0CE6264 (void);
// 0x0000009F System.Void DevionGames.Jump::OnControllerGrounded(System.Boolean)
extern void Jump_OnControllerGrounded_m80387B520806C6D3A3F73DC0D34FEADC76D5062C (void);
// 0x000000A0 System.Boolean DevionGames.Jump::CanStart()
extern void Jump_CanStart_m022E99C906EB3682F006BFEB11962DC5222BBA0D (void);
// 0x000000A1 System.Boolean DevionGames.Jump::CanStop()
extern void Jump_CanStop_mADE3B195DD8146EA78393FEAA058CA64A1AAA687 (void);
// 0x000000A2 System.Void DevionGames.Jump::.ctor()
extern void Jump__ctor_m47E13118BE82F95E094B545955B4F00B4F2DE74D (void);
// 0x000000A3 System.Void DevionGames.Ladder::OnStart()
extern void Ladder_OnStart_m3030297160C2FB9E275B06B450C8885EDD956DA7 (void);
// 0x000000A4 System.Void DevionGames.Ladder::OnStop()
extern void Ladder_OnStop_mE618431B5D65DF22EB12A6BEFF927A22E3747713 (void);
// 0x000000A5 System.Boolean DevionGames.Ladder::CanStart()
extern void Ladder_CanStart_m87146BA4E66AA5BB7DB1EF2D5AD8B54203049465 (void);
// 0x000000A6 System.Boolean DevionGames.Ladder::CanStop()
extern void Ladder_CanStop_mADDA032D7CB2DE9B45EB72E1DD96FE2E9EA5FA31 (void);
// 0x000000A7 System.Boolean DevionGames.Ladder::UpdateVelocity(UnityEngine.Vector3&)
extern void Ladder_UpdateVelocity_m50ED91BB0A4E180EF8AC3F73F89D5CE25223930E (void);
// 0x000000A8 System.Boolean DevionGames.Ladder::UpdateAnimator()
extern void Ladder_UpdateAnimator_m46C54DF0C5EDB98A3229E3554B42959F53357AF4 (void);
// 0x000000A9 System.Boolean DevionGames.Ladder::CheckGround()
extern void Ladder_CheckGround_m81E79395BD8EB7D6EA4A1F911BB667034F45B356 (void);
// 0x000000AA System.Boolean DevionGames.Ladder::UpdateRotation()
extern void Ladder_UpdateRotation_mBB6FA81C8110C91348BFF4631CB9947823A888AA (void);
// 0x000000AB System.Void DevionGames.Ladder::OnTriggerEnter(UnityEngine.Collider)
extern void Ladder_OnTriggerEnter_m5D56272F2E903A0C6B35A1F141EA8520EB26F21D (void);
// 0x000000AC System.Void DevionGames.Ladder::OnTriggerExit(UnityEngine.Collider)
extern void Ladder_OnTriggerExit_mE5620D722E1C8412E57CC2053E6B252485C92358 (void);
// 0x000000AD System.Void DevionGames.Ladder::.ctor()
extern void Ladder__ctor_m309E03A049C0D27D86D89BFF3CF83C4FFD1B1944 (void);
// 0x000000AE System.Void DevionGames.Ladder::<OnStart>b__3_0()
extern void Ladder_U3COnStartU3Eb__3_0_mBC4D49695B7E552BC419F620728064B13CD93DF3 (void);
// 0x000000AF System.Boolean DevionGames.Push::CanStart()
extern void Push_CanStart_mCBF13130E6C762BACCE993BA03A3065FA49CEDF8 (void);
// 0x000000B0 System.Boolean DevionGames.Push::CanStop()
extern void Push_CanStop_mACD4987B93E2BE2BC1AC2971A30DA6F71C1753C4 (void);
// 0x000000B1 System.Void DevionGames.Push::OnStart()
extern void Push_OnStart_m40B0BE6235F95E7DAFAD2EDCE60342387F5D6679 (void);
// 0x000000B2 System.Void DevionGames.Push::OnStop()
extern void Push_OnStop_mAFBDE280C7A0E7DF5E351ED34C6E2E8E00F21B81 (void);
// 0x000000B3 System.Boolean DevionGames.Push::UpdateVelocity(UnityEngine.Vector3&)
extern void Push_UpdateVelocity_m0294E2801C59CA2EFDBD0C0536B39E3F67BCABB5 (void);
// 0x000000B4 System.Boolean DevionGames.Push::UpdateAnimatorIK(System.Int32)
extern void Push_UpdateAnimatorIK_m54222D970F47B2AE0F800EC785E50148AE0037E1 (void);
// 0x000000B5 System.Boolean DevionGames.Push::UpdateRotation()
extern void Push_UpdateRotation_m10841DC32DDFC59ED1A1FE03082CD6CEF07BF80C (void);
// 0x000000B6 System.Boolean DevionGames.Push::CheckPush(System.Single)
extern void Push_CheckPush_m2C4AF8960CBFA66C3D1016B76B7EA8CD37B11CFD (void);
// 0x000000B7 System.Void DevionGames.Push::.ctor()
extern void Push__ctor_mD719C94C901B27B1B22AD9F84C7A66EFE3487EA2 (void);
// 0x000000B8 System.Void DevionGames.Push::<OnStart>b__5_0()
extern void Push_U3COnStartU3Eb__5_0_mDCFA0375A9026A356B13D8E7B2FF6DD0F030CB0D (void);
// 0x000000B9 System.String DevionGames.RandomMotion::GetDestinationState()
extern void RandomMotion_GetDestinationState_mE50F0CC119DAFEA1499426C7D55AC8FDB5CA77C8 (void);
// 0x000000BA System.Void DevionGames.RandomMotion::.ctor()
extern void RandomMotion__ctor_m95981E967D8C68A0D128A11557E2FF5C0EA91D37 (void);
// 0x000000BB System.Void DevionGames.SimpleMotion::OnStop()
extern void SimpleMotion_OnStop_m0E5114C7FD925CEE4644D7B40FFAE7C2151B88A7 (void);
// 0x000000BC System.Void DevionGames.SimpleMotion::OnStart()
extern void SimpleMotion_OnStart_m7FFA5BA266E6C3D4467CEEE37B2772E5289FE49F (void);
// 0x000000BD System.Boolean DevionGames.SimpleMotion::CanStart()
extern void SimpleMotion_CanStart_m7724CE681A1D388420880FC2F0D9D4F382E950B2 (void);
// 0x000000BE System.Void DevionGames.SimpleMotion::.ctor()
extern void SimpleMotion__ctor_mF6B316BA85468044C2D4160738D0C828CDF9FCC7 (void);
// 0x000000BF System.Boolean DevionGames.Slide::CanStart()
extern void Slide_CanStart_m0C72EBFE3518C8CDF46343B6AB45CC980C44C7D2 (void);
// 0x000000C0 System.Void DevionGames.Slide::.ctor()
extern void Slide__ctor_mA743ABA827FA73E769E238751EBDF81C17B68930 (void);
// 0x000000C1 System.Void DevionGames.Swim::OnStart()
extern void Swim_OnStart_mF0546141A4CF072DCC851013E90E73BEE84D4E0C (void);
// 0x000000C2 System.Void DevionGames.Swim::OnStop()
extern void Swim_OnStop_m099152A263A3F84C92C6A3F9F403E238A675E3A4 (void);
// 0x000000C3 System.Boolean DevionGames.Swim::CanStart()
extern void Swim_CanStart_mD99A62FA03A20CF5FBB48EC4E79E8814B67B320A (void);
// 0x000000C4 System.Boolean DevionGames.Swim::CanStop()
extern void Swim_CanStop_mDEA2522B7BD3F0BECDE1562A490B02C93AA8EC19 (void);
// 0x000000C5 System.Boolean DevionGames.Swim::UpdateVelocity(UnityEngine.Vector3&)
extern void Swim_UpdateVelocity_m1081E2D3092D694361AC5A30F8DC8A045E9BE691 (void);
// 0x000000C6 System.Boolean DevionGames.Swim::CheckGround()
extern void Swim_CheckGround_m95F5D7F6262491CEBED6EA13A73858CAEA29D4AC (void);
// 0x000000C7 System.Boolean DevionGames.Swim::UpdateAnimatorIK(System.Int32)
extern void Swim_UpdateAnimatorIK_m5F5A9C3416D93F25E002A78B2FA0F3C63D3FD798 (void);
// 0x000000C8 System.Void DevionGames.Swim::OnTriggerEnter(UnityEngine.Collider)
extern void Swim_OnTriggerEnter_m6601A12C044076AD4195EDE79247D0B9C7E87D7C (void);
// 0x000000C9 System.Void DevionGames.Swim::OnTriggerExit(UnityEngine.Collider)
extern void Swim_OnTriggerExit_m50C56775ED198AFA224A8746461C829CFFFC9435 (void);
// 0x000000CA System.Void DevionGames.Swim::.ctor()
extern void Swim__ctor_m1737AC2D452C80CA2E96E902A510B2B0E606D327 (void);
// 0x000000CB System.Void DevionGames.PushableObject::Start()
extern void PushableObject_Start_mF1AC1EF2BC959959D9DB6CD4ACA7615A5B346B4C (void);
// 0x000000CC System.Void DevionGames.PushableObject::StartMove(DevionGames.ThirdPersonController)
extern void PushableObject_StartMove_m972E6030D2C82B82828A6E6D4B452ADF72E3BE52 (void);
// 0x000000CD System.Void DevionGames.PushableObject::StopMove()
extern void PushableObject_StopMove_mC62078AD20F46429CE013D37ED8DF31736CF8DF4 (void);
// 0x000000CE System.Boolean DevionGames.PushableObject::Move(UnityEngine.Vector3)
extern void PushableObject_Move_mE195448AF71C90BD12E9D115C417326051B629AA (void);
// 0x000000CF System.Void DevionGames.PushableObject::OnCollisionStay(UnityEngine.Collision)
extern void PushableObject_OnCollisionStay_mD4E559F057918F19A4295B191F953B2DE2236851 (void);
// 0x000000D0 System.Void DevionGames.PushableObject::.ctor()
extern void PushableObject__ctor_m4E045B87B5B225740C25B1D0D0F73BA5B4072067 (void);
// 0x000000D1 System.Void DevionGames.SetLayerWeight::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void SetLayerWeight_OnStateEnter_m56693B786E8845A007287C83B772B7BB379E3DC0 (void);
// 0x000000D2 System.Void DevionGames.SetLayerWeight::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void SetLayerWeight_OnStateUpdate_mD225606CEB6D36CB4E7424DCD2678E4622A0B689 (void);
// 0x000000D3 System.Void DevionGames.SetLayerWeight::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void SetLayerWeight_OnStateExit_m3734414E0A2B889410888802B16D78BF5CE289C3 (void);
// 0x000000D4 System.Void DevionGames.SetLayerWeight::.ctor()
extern void SetLayerWeight__ctor_m9D6D8FCA639165B2D34156CB87D2A5CBCE9F0A9C (void);
// 0x000000D5 System.Void DevionGames.SwimTrigger::.ctor()
extern void SwimTrigger__ctor_m7991BAAB23F1024C02327A4C0D7F3F7007510707 (void);
// 0x000000D6 System.Void DevionGames.ThirdPersonController::Execute(DevionGames.IControllerGrounded,System.Object)
extern void ThirdPersonController_Execute_mBED7686CD45BB739E44881CBF390853E843593F8 (void);
// 0x000000D7 System.Void DevionGames.ThirdPersonController::Execute(DevionGames.IControllerAim,System.Object)
extern void ThirdPersonController_Execute_mB02B8B12BC685AFEDC5963360E861655386F0D56 (void);
// 0x000000D8 System.Void DevionGames.ThirdPersonController::ExecuteEvent(DevionGames.ThirdPersonController/EventFunction`1<T>,System.Object,System.Boolean)
// 0x000000D9 System.Boolean DevionGames.ThirdPersonController::ShouldSendEvent(DevionGames.IControllerEventHandler,System.Boolean)
// 0x000000DA System.Collections.Generic.List`1<DevionGames.MotionState> DevionGames.ThirdPersonController::get_Motions()
extern void ThirdPersonController_get_Motions_mEF21890C477BD0014D3E3EC34AC811AC98BE094C (void);
// 0x000000DB System.Void DevionGames.ThirdPersonController::set_Motions(System.Collections.Generic.List`1<DevionGames.MotionState>)
extern void ThirdPersonController_set_Motions_mD418ED8EDAC7DEAE02ED8E5B3D9CA5626D41A7EF (void);
// 0x000000DC System.String DevionGames.ThirdPersonController::get_ForwardInput()
extern void ThirdPersonController_get_ForwardInput_mACEA97EF6CF281F00F2A539BCB35D139C3E27B3F (void);
// 0x000000DD System.Void DevionGames.ThirdPersonController::set_ForwardInput(System.String)
extern void ThirdPersonController_set_ForwardInput_m5CFB4CA23D850BCD1A919C3E480C1E077C3A5B79 (void);
// 0x000000DE System.String DevionGames.ThirdPersonController::get_HorizontalInput()
extern void ThirdPersonController_get_HorizontalInput_mEFB42BE0D6ECCE92D99F380229E6C7B7B84D3646 (void);
// 0x000000DF System.Void DevionGames.ThirdPersonController::set_HorizontalInput(System.String)
extern void ThirdPersonController_set_HorizontalInput_m5D93E98309ADB1E0C385637574F3A65F05C59798 (void);
// 0x000000E0 System.Single DevionGames.ThirdPersonController::get_SpeedMultiplier()
extern void ThirdPersonController_get_SpeedMultiplier_mD56C4502292F7CBB94C008E830CCCD7AEF1188EE (void);
// 0x000000E1 System.Void DevionGames.ThirdPersonController::set_SpeedMultiplier(System.Single)
extern void ThirdPersonController_set_SpeedMultiplier_m3FFD97AF6FDCDA4FF0D449C147768470C5812F82 (void);
// 0x000000E2 DevionGames.AimType DevionGames.ThirdPersonController::get_AimType()
extern void ThirdPersonController_get_AimType_m1D9D021B380F37FFCBC12AB99C8DD46500EE5C11 (void);
// 0x000000E3 System.Void DevionGames.ThirdPersonController::set_AimType(DevionGames.AimType)
extern void ThirdPersonController_set_AimType_mE71A418944D98B659438A48C19E703AE3C19C4A1 (void);
// 0x000000E4 System.String DevionGames.ThirdPersonController::get_AimInput()
extern void ThirdPersonController_get_AimInput_m0B5800FFF22264E33A286EE92FA9596A90AA7529 (void);
// 0x000000E5 System.Void DevionGames.ThirdPersonController::set_AimInput(System.String)
extern void ThirdPersonController_set_AimInput_mBCAAB1DAA8CA0AD8A49095EC5EE2CF331C540F59 (void);
// 0x000000E6 System.Single DevionGames.ThirdPersonController::get_AimRotation()
extern void ThirdPersonController_get_AimRotation_mD70E74E452F380651211669EE4EC1CCE84B5AADF (void);
// 0x000000E7 System.Void DevionGames.ThirdPersonController::set_AimRotation(System.Single)
extern void ThirdPersonController_set_AimRotation_m69E27A7F3728A36FB597BE29548C4C6684825A7A (void);
// 0x000000E8 System.Single DevionGames.ThirdPersonController::get_RotationSpeed()
extern void ThirdPersonController_get_RotationSpeed_m8AF019DF9358455822CC4394F50C636A68644D83 (void);
// 0x000000E9 System.Void DevionGames.ThirdPersonController::set_RotationSpeed(System.Single)
extern void ThirdPersonController_set_RotationSpeed_m7DCFF13D8E09C69F609C923ACB915FD426FC5AF8 (void);
// 0x000000EA UnityEngine.Vector3 DevionGames.ThirdPersonController::get_AirSpeed()
extern void ThirdPersonController_get_AirSpeed_mB4D15B2871AB87BA2F6303FE7700DD91D6791525 (void);
// 0x000000EB System.Void DevionGames.ThirdPersonController::set_AirSpeed(UnityEngine.Vector3)
extern void ThirdPersonController_set_AirSpeed_mF2D5280DC7F10073330D8192DF25BB7D24E9A4F4 (void);
// 0x000000EC System.Single DevionGames.ThirdPersonController::get_AirDampening()
extern void ThirdPersonController_get_AirDampening_mE3170D13A836275CEEAA05499E7CB34433A5AC20 (void);
// 0x000000ED System.Void DevionGames.ThirdPersonController::set_AirDampening(System.Single)
extern void ThirdPersonController_set_AirDampening_m0D9606544C46BC6282806383BC4067D49CAE68AF (void);
// 0x000000EE System.Single DevionGames.ThirdPersonController::get_GroundDampening()
extern void ThirdPersonController_get_GroundDampening_m129DC904CD1462BA0B7B659AFD3DE4AA40BBFA30 (void);
// 0x000000EF System.Void DevionGames.ThirdPersonController::set_GroundDampening(System.Single)
extern void ThirdPersonController_set_GroundDampening_m62A6E291AA90D27877004ADB3EBD389E9231AEE0 (void);
// 0x000000F0 System.Single DevionGames.ThirdPersonController::get_StepOffset()
extern void ThirdPersonController_get_StepOffset_m58DAE60774E03A4CAF8760219C3F4B7010159114 (void);
// 0x000000F1 System.Void DevionGames.ThirdPersonController::set_StepOffset(System.Single)
extern void ThirdPersonController_set_StepOffset_mD43ED8407E09C06C6645F2B3EE2814D974809A2C (void);
// 0x000000F2 System.Single DevionGames.ThirdPersonController::get_SlopeLimit()
extern void ThirdPersonController_get_SlopeLimit_m06E84F89C064449C2FF47C17850F77EA95DEB060 (void);
// 0x000000F3 System.Void DevionGames.ThirdPersonController::set_SlopeLimit(System.Single)
extern void ThirdPersonController_set_SlopeLimit_m2B375AA83AF330EA2E24E931DB2E40E4F6AB0ED0 (void);
// 0x000000F4 UnityEngine.LayerMask DevionGames.ThirdPersonController::get_GroundLayer()
extern void ThirdPersonController_get_GroundLayer_mE1F8ACD7EE0C496DAFE9483E02CF001170B61872 (void);
// 0x000000F5 System.Void DevionGames.ThirdPersonController::set_GroundLayer(UnityEngine.LayerMask)
extern void ThirdPersonController_set_GroundLayer_mF77FD9E68F83E38876AA4DDBB7BFCE9456BD964B (void);
// 0x000000F6 System.Single DevionGames.ThirdPersonController::get_SkinWidth()
extern void ThirdPersonController_get_SkinWidth_m6CCB3699811CE1370A202F14A717A643347A55C1 (void);
// 0x000000F7 System.Void DevionGames.ThirdPersonController::set_SkinWidth(System.Single)
extern void ThirdPersonController_set_SkinWidth_m7F3BB70B4DF6964A48AB551C505E9B199D654546 (void);
// 0x000000F8 UnityEngine.PhysicMaterial DevionGames.ThirdPersonController::get_IdleFriction()
extern void ThirdPersonController_get_IdleFriction_m63F8AA5B808F6FB752A2B2390BEA77AD0D235D89 (void);
// 0x000000F9 System.Void DevionGames.ThirdPersonController::set_IdleFriction(UnityEngine.PhysicMaterial)
extern void ThirdPersonController_set_IdleFriction_m1E4BB1A493B02EC1B554AE8F8585E0CBFC5B6920 (void);
// 0x000000FA UnityEngine.PhysicMaterial DevionGames.ThirdPersonController::get_MovementFriction()
extern void ThirdPersonController_get_MovementFriction_m05B9A53240469980958B569E71F617176F5D5A55 (void);
// 0x000000FB System.Void DevionGames.ThirdPersonController::set_MovementFriction(UnityEngine.PhysicMaterial)
extern void ThirdPersonController_set_MovementFriction_mD6084BA4F7B84C62E6CAB7F5D78C904A6EF5C711 (void);
// 0x000000FC UnityEngine.PhysicMaterial DevionGames.ThirdPersonController::get_StepFriction()
extern void ThirdPersonController_get_StepFriction_m158E2BCA0D4E3B2E1D8A1E38AFF104240BCB6C27 (void);
// 0x000000FD System.Void DevionGames.ThirdPersonController::set_StepFriction(UnityEngine.PhysicMaterial)
extern void ThirdPersonController_set_StepFriction_m134C206111FBA26F100EC6FF53543B2E750092C2 (void);
// 0x000000FE UnityEngine.PhysicMaterial DevionGames.ThirdPersonController::get_AirFriction()
extern void ThirdPersonController_get_AirFriction_m9A57F9A09C4A07817C8262788F24DFF523A93154 (void);
// 0x000000FF System.Void DevionGames.ThirdPersonController::set_AirFriction(UnityEngine.PhysicMaterial)
extern void ThirdPersonController_set_AirFriction_m6F00197BA2BD83DF46D08A09D913A5C7E8A7F9F7 (void);
// 0x00000100 System.Single DevionGames.ThirdPersonController::get_ForwardDampTime()
extern void ThirdPersonController_get_ForwardDampTime_m23C569DA47626ADD61CCE18C08E9B15B0F68C64E (void);
// 0x00000101 System.Void DevionGames.ThirdPersonController::set_ForwardDampTime(System.Single)
extern void ThirdPersonController_set_ForwardDampTime_m8A925FAD99044753FB02B0E443C8739C0463BABC (void);
// 0x00000102 System.Single DevionGames.ThirdPersonController::get_HorizontalDampTime()
extern void ThirdPersonController_get_HorizontalDampTime_mC5EF620EDF4819AA823809AFF448AA199F8CAE0A (void);
// 0x00000103 System.Void DevionGames.ThirdPersonController::set_HorizontalDampTime(System.Single)
extern void ThirdPersonController_set_HorizontalDampTime_mDDCBEF514F1433150F82C94900C02EED4B9A2411 (void);
// 0x00000104 System.Boolean DevionGames.ThirdPersonController::get_IsGrounded()
extern void ThirdPersonController_get_IsGrounded_m92443FAD52F62A5A7D27EBF330078A48ACDF5CC9 (void);
// 0x00000105 System.Void DevionGames.ThirdPersonController::set_IsGrounded(System.Boolean)
extern void ThirdPersonController_set_IsGrounded_m71A6FB41C399527523BADE9D719199E0A8F81C2C (void);
// 0x00000106 System.Boolean DevionGames.ThirdPersonController::get_IsStepping()
extern void ThirdPersonController_get_IsStepping_m483AD98F36C8712F36B0B4775FC34070A27DD9D0 (void);
// 0x00000107 UnityEngine.Vector3 DevionGames.ThirdPersonController::get_RawInput()
extern void ThirdPersonController_get_RawInput_mA6E441407CE8DC30E4A8A024C254D568EDE25F80 (void);
// 0x00000108 System.Void DevionGames.ThirdPersonController::set_RawInput(UnityEngine.Vector3)
extern void ThirdPersonController_set_RawInput_m745F096DB195D2945A9A4B34B3E111D82793B938 (void);
// 0x00000109 UnityEngine.Vector3 DevionGames.ThirdPersonController::get_RelativeInput()
extern void ThirdPersonController_get_RelativeInput_m0546685CD4C1B2292DA27A027737A438574E2233 (void);
// 0x0000010A System.Boolean DevionGames.ThirdPersonController::get_IsMoving()
extern void ThirdPersonController_get_IsMoving_m31B5B25751480A77D97BB67EADFE1E6AF07C8AA0 (void);
// 0x0000010B System.Void DevionGames.ThirdPersonController::set_IsMoving(System.Boolean)
extern void ThirdPersonController_set_IsMoving_mC592B5FCA986E0DC014A572041BCA969191EC96E (void);
// 0x0000010C System.Boolean DevionGames.ThirdPersonController::get_IsAiming()
extern void ThirdPersonController_get_IsAiming_mCAD091F0336F0BCC3A00C72F8446D5276A4A74B3 (void);
// 0x0000010D System.Void DevionGames.ThirdPersonController::set_IsAiming(System.Boolean)
extern void ThirdPersonController_set_IsAiming_mD103532ED9160908A1F58D04AA17B981E9FE48A0 (void);
// 0x0000010E UnityEngine.Vector3 DevionGames.ThirdPersonController::get_Velocity()
extern void ThirdPersonController_get_Velocity_m424EB317131D0A0DFC11221C6B9B31E77BE0FAD3 (void);
// 0x0000010F System.Void DevionGames.ThirdPersonController::set_Velocity(UnityEngine.Vector3)
extern void ThirdPersonController_set_Velocity_m37253B0A4E796C30B84BE73E6A4206A0E4065023 (void);
// 0x00000110 UnityEngine.Quaternion DevionGames.ThirdPersonController::get_LookRotation()
extern void ThirdPersonController_get_LookRotation_mD8C65903EA392FD5A28E4D2AD5DE30ADCF4F33EA (void);
// 0x00000111 UnityEngine.Vector3 DevionGames.ThirdPersonController::get_RootMotionForce()
extern void ThirdPersonController_get_RootMotionForce_m908905A59AC3A544DEE749F1E5B90AB323B54A04 (void);
// 0x00000112 System.Void DevionGames.ThirdPersonController::Awake()
extern void ThirdPersonController_Awake_m3C0B359CCA4A7CEEE956B7A7C2E0CB0A0576F8E2 (void);
// 0x00000113 System.Void DevionGames.ThirdPersonController::OnEnable()
extern void ThirdPersonController_OnEnable_m32C301AAD7EB104928D3BEB341BB6F35AF933975 (void);
// 0x00000114 System.Void DevionGames.ThirdPersonController::OnDisable()
extern void ThirdPersonController_OnDisable_m4462CDE564C8CB41E80A5FBB69D950F4A1FA31DA (void);
// 0x00000115 System.Void DevionGames.ThirdPersonController::Update()
extern void ThirdPersonController_Update_mF96FF6BD5D7D7B8F093A40CBB7A64D71D6D3E3A5 (void);
// 0x00000116 System.Void DevionGames.ThirdPersonController::FixedUpdate()
extern void ThirdPersonController_FixedUpdate_m84B82193111BB00AAF5F93780B79AF62A8BA5AC1 (void);
// 0x00000117 System.Void DevionGames.ThirdPersonController::OnAnimatorMove()
extern void ThirdPersonController_OnAnimatorMove_m763438BC176F86F4D8D4A289500046EDB8927BC9 (void);
// 0x00000118 System.Void DevionGames.ThirdPersonController::UpdateVelocity()
extern void ThirdPersonController_UpdateVelocity_mA91AE0076944E09854C99E88FB2572B476DEACC7 (void);
// 0x00000119 System.Void DevionGames.ThirdPersonController::UpdateAnimator()
extern void ThirdPersonController_UpdateAnimator_m5DDE89B996B281A64FDF5E8823ED2582BAEED015 (void);
// 0x0000011A System.Single DevionGames.ThirdPersonController::Normalize(System.Single,System.Single,System.Single)
extern void ThirdPersonController_Normalize_mE72F0E9FA3C14AAFCB0E99138D39B3120A9BF381 (void);
// 0x0000011B System.Single DevionGames.ThirdPersonController::GetSignedAngle(UnityEngine.Quaternion,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void ThirdPersonController_GetSignedAngle_mD6794D25EB41E6F978E017914BD81D02D1504028 (void);
// 0x0000011C System.Void DevionGames.ThirdPersonController::UpdateRotation()
extern void ThirdPersonController_UpdateRotation_mEE706B72B4CB5107EE502D93669EFFDCC6F90DC2 (void);
// 0x0000011D System.Void DevionGames.ThirdPersonController::UpdateFrictionMaterial()
extern void ThirdPersonController_UpdateFrictionMaterial_m0BEAC893C798984C21B18B023CD6DF45AD953A72 (void);
// 0x0000011E System.Void DevionGames.ThirdPersonController::DeterminanteDefaultStates()
extern void ThirdPersonController_DeterminanteDefaultStates_mB82EE6DC8A1142154A266BBD28F4CC7BBCAE8410 (void);
// 0x0000011F System.Void DevionGames.ThirdPersonController::CheckDefaultAnimatorStates()
extern void ThirdPersonController_CheckDefaultAnimatorStates_m51E1C4825FD1685F6784AFAC28E4CF547E5932C7 (void);
// 0x00000120 System.Void DevionGames.ThirdPersonController::CheckGround()
extern void ThirdPersonController_CheckGround_m3586A065B84123401DE8689C1F488FDF672036B1 (void);
// 0x00000121 System.Void DevionGames.ThirdPersonController::CheckStep()
extern void ThirdPersonController_CheckStep_m9AE29681A952EF70A821A135EE67C50D8D654D2C (void);
// 0x00000122 System.Void DevionGames.ThirdPersonController::TryStopMotion(DevionGames.MotionState)
extern void ThirdPersonController_TryStopMotion_m2CE8DD9C76481E04D9FB6FDAD245E480C08215DF (void);
// 0x00000123 System.Void DevionGames.ThirdPersonController::TryStartMotion(DevionGames.MotionState)
extern void ThirdPersonController_TryStartMotion_m7CF4B2DFD2B01275855356AB828C98F769A8FD3F (void);
// 0x00000124 System.Void DevionGames.ThirdPersonController::SetControllerActive(System.Boolean)
extern void ThirdPersonController_SetControllerActive_mBCA172AEC596F46277BCEAB1E8E68A04066CA18E (void);
// 0x00000125 System.Void DevionGames.ThirdPersonController::SetMotionEnabled(System.Object[])
extern void ThirdPersonController_SetMotionEnabled_m3EDED81F46FFA2F5CE5E562A680B76CCD925B337 (void);
// 0x00000126 System.Void DevionGames.ThirdPersonController::SetMotionEnabled(System.String,System.Boolean)
extern void ThirdPersonController_SetMotionEnabled_m7FA57C9EDAB5E2D14F17F687EBB1679F0BAAC295 (void);
// 0x00000127 DevionGames.MotionState DevionGames.ThirdPersonController::GetMotion(System.String)
extern void ThirdPersonController_GetMotion_m2E92197B956BF9B95C32B1627D26549A351DBB58 (void);
// 0x00000128 System.Void DevionGames.ThirdPersonController::Footsteps(UnityEngine.AnimationEvent)
extern void ThirdPersonController_Footsteps_m2A3BE5D48E1C85CCDE6F2760E282EBB12B53B638 (void);
// 0x00000129 System.Void DevionGames.ThirdPersonController::PlaySound(UnityEngine.AudioClip,System.Single)
extern void ThirdPersonController_PlaySound_mCA7660BFF60BD19EBC1C368F735303191C99E8C3 (void);
// 0x0000012A System.Void DevionGames.ThirdPersonController::CopyProperties(DevionGames.ThirdPersonController)
extern void ThirdPersonController_CopyProperties_mA1772F32090D040389E17C8B1DCCB51F83420744 (void);
// 0x0000012B T DevionGames.ThirdPersonController::CopyComponent(T,UnityEngine.GameObject)
// 0x0000012C System.Void DevionGames.ThirdPersonController::.ctor()
extern void ThirdPersonController__ctor_m575BF3E1A14CC272C2CEF96945B25DA1952C85B3 (void);
// 0x0000012D System.Boolean DevionGames.ThirdPersonController::<Awake>b__143_0(UnityEngine.Animator)
extern void ThirdPersonController_U3CAwakeU3Eb__143_0_m69772D712CCC99CE0F597EFE68BC1E028F51ABB3 (void);
// 0x0000012E System.Void DevionGames.ThirdPersonController/EventFunction`1::.ctor(System.Object,System.IntPtr)
// 0x0000012F System.Void DevionGames.ThirdPersonController/EventFunction`1::Invoke(T,System.Object)
// 0x00000130 System.IAsyncResult DevionGames.ThirdPersonController/EventFunction`1::BeginInvoke(T,System.Object,System.AsyncCallback,System.Object)
// 0x00000131 System.Void DevionGames.ThirdPersonController/EventFunction`1::EndInvoke(System.IAsyncResult)
// 0x00000132 System.Void DevionGames.ThirdPersonController/<>c__DisplayClass155_0::.ctor()
extern void U3CU3Ec__DisplayClass155_0__ctor_m11530FFC547EB22B080C4702570C97E28E8C6CA9 (void);
// 0x00000133 System.Boolean DevionGames.ThirdPersonController/<>c__DisplayClass155_0::<DeterminanteDefaultStates>b__0(DevionGames.MotionState)
extern void U3CU3Ec__DisplayClass155_0_U3CDeterminanteDefaultStatesU3Eb__0_mF08DC12849D9CCD47FB3B798ED6EFE78B3820A1C (void);
static Il2CppMethodPointer s_methodPointers[307] = 
{
	Instantiate_OnStateUpdate_m36B64CE3E70FF40D43C32868E8427700AD0F6A02,
	Instantiate__ctor_mF1498BA1474FA6BDCF17B820DCF1888CF05BAC34,
	PlaySound_OnStateEnter_mFF6D33B1A1519D3DC46A5C8941CB998DF5871D30,
	PlaySound_OnStateUpdate_mD571D80F0F61D218A03E3BB2DDDD3C9D54A870FE,
	PlaySound_OnStateExit_m433D6DBB993C131988DC2861EA5257D6661F5E00,
	PlaySound_Play_m284B6A96DCFF9AD0FCC227EEB17F0214E6AEA797,
	PlaySound__ctor_mE2894569DE25F7E4176462EA3B82488AAB5AA262,
	CameraSettings_get_Name_m2F5F2F18B94E370F29020E0F6811BADD161702BD,
	CameraSettings_set_Name_mF74BAF475E1E9953A232E7C6D1F4548DD419BE6B,
	CameraSettings_get_InputName_m074BE02D8B2478E10045094BAB47A6199A008F89,
	CameraSettings_set_InputName_mBDCBEAD19D08B5543C00BAF41E7FD528FE69258C,
	CameraSettings_get_Activation_m75FE5269D56A5DB49099CE121D101063DA721EF7,
	CameraSettings_set_Activation_m632C11EC4C8ED23934998AB3E3D4497FCA4AC40E,
	CameraSettings_get_Offset_m3866C7B1D6194C365CC3E289444D9E9CE58F658B,
	CameraSettings_set_Offset_m330BA68BC431AA74E751B751EDB0A08D093E5D1A,
	CameraSettings_get_Distance_m1B9FCFBFCF18E533D08CE29CF3C1C2D622B67E7F,
	CameraSettings_set_Distance_m9D915936C4DEA68C489EE91EF59650B8AB5461E8,
	CameraSettings_get_Crosshair_m38400FF3C6A7E5C7EC8BD44F6448A8EF9D6C776D,
	CameraSettings_set_Crosshair_m628D3969E8F965F50B6DA2E8FA84749FB1EAA1A5,
	CameraSettings_get_TurnButton_mC142CB853F51B97DAEA3E19ED4AFF9D71E6DA16F,
	CameraSettings_set_TurnButton_mBB9E710963BD9CBFDE5A5715DC62926EA700C678,
	CameraSettings_get_TurnSpeed_m17C6AE46F8BCAF83B73E01689656B9C51DD84202,
	CameraSettings_set_TurnSpeed_m68D89B9A7A9E7117AB7809EEBE7880E5E7492721,
	CameraSettings_get_TurnSmoothing_m3683449822AE33204DD17B75EA18D3C83354B4D8,
	CameraSettings_set_TurnSmoothing_m4477BDE8AF44446A16A952C921CB1439638B4D02,
	CameraSettings_get_YawLimit_m791F93A81F117FDEE50BBD73A4EBAD7FAB93989B,
	CameraSettings_set_YawLimit_m2CACE52FF6DF2608CC0D6FE1EE1261E324F0A81C,
	CameraSettings_get_PitchLimit_mC5E51AB9E417115B568C0CDF0C6DC572A488DD34,
	CameraSettings_set_PitchLimit_mD0AFBF06E2C8B04A9A5B3200BE24AFD8819DB119,
	CameraSettings_get_VisibilityDelta_mA7A46E27215123B91C63143A685014F4BF554B3C,
	CameraSettings_set_VisibilityDelta_mFC55FD26E08B3D8180E6357A46DDC1450D8AC58A,
	CameraSettings_get_ZoomSpeed_mC1FB1A62066F4219FC23333D3070C51C4C771FA0,
	CameraSettings_set_ZoomSpeed_mA1CCFC97AC36BEA59F2782EE790E2392E7336723,
	CameraSettings_get_ZoomLimit_m36F7B9184B6708EC52BBCB66E49C6504D7245D93,
	CameraSettings_set_ZoomLimit_m83DF5CC2040B54A2F617C0FE5CBD580F3B2DAC9C,
	CameraSettings_get_ZoomSmoothing_m4FAC20ADCABCD683F2DBA67276409E7787A58B38,
	CameraSettings_set_ZoomSmoothing_mC199F5187A4D62B6D9B03B14A7FD94C2688A731C,
	CameraSettings_get_MoveSmoothing_mAB28154580A4E5D2945BB30006F1CF86E908C960,
	CameraSettings_set_MoveSmoothing_m9A43EF77E557FC8A6BA977949EAEFED8563C9535,
	CameraSettings_get_CursorMode_m7AEDBCCA92348B286FDC2AEC0412C02BF2E5F65E,
	CameraSettings_set_CursorMode_m35AF59286F39A5BFDFC23B15064333286F2D05C4,
	CameraSettings_get_ConsumeInputOverUI_mFD10B7749694328332B707912866C56DE0AAA87C,
	CameraSettings_set_ConsumeInputOverUI_m3A9FFC055E08003FFD5AA14E43EA3DC9E2AEDB0E,
	CameraSettings_get_CollisionLayer_m4F624E88B931C0E494EF47CDA9DCBBC099EB2953,
	CameraSettings_set_CollisionLayer_m02FF188E6B533EC51C9AE8624B8FA9FD30F2A6A5,
	CameraSettings_get_CollisionRadius_m5AFBA1BCE44FA3C1FBEC1F5F7892A85519932907,
	CameraSettings_set_CollisionRadius_mEDF27AEAA9A80553AB2C56739C8FA70CDCA6F8F0,
	CameraSettings_get_IsActive_m3CB334BA2F41488F4D5BEF6112EA8C061D88EE1E,
	CameraSettings_set_IsActive_m0826D626219FFDC147BC399403FF6218AB1D2A6D,
	CameraSettings_get_Zoom_m4774EDD7870F1088A1073BBBDE30B4E66F44A216,
	CameraSettings_set_Zoom_m543F9A9606367C14F641A68304D1BFEB6696A450,
	CameraSettings__ctor_mE62990BBBFCD7D3E914477A3306B71368DA853AA,
	FocusTarget_Start_mF8ACAD4B4B06E0D893755F4C05D10327A14B15AC,
	FocusTarget_Update_m527CC87535BE59B9A405418E9FC8E4BDD17ED772,
	FocusTarget_Focus_m8E71117CFBC78AF8C268D193F5AC7659A308CEC8,
	FocusTarget__ctor_m70339E68778E09FCC38408B752E7FAA042D62603,
	ThirdPersonCamera_get_Target_m135A7718937E473F35CD5576736F8EBF6E396980,
	ThirdPersonCamera_get_Presets_mB220A126E2968E4298431C663AB9C7D51B0DA319,
	ThirdPersonCamera_Start_mED52773B1B4079C894A1C8666D63952F4D5110B8,
	ThirdPersonCamera_OnEnable_m5CAB66441B940CDB322F1147E1ACDA978C9A6E5F,
	ThirdPersonCamera_OnDisable_mC3C76890E789464F60E8F330F8634B6EEE50C832,
	ThirdPersonCamera_OnSetControllerActive_m353ED80C26DF796678FB7B07C19D1D76F4E7A497,
	ThirdPersonCamera_LateUpdate_m9721845907F1441F6C8A8949EB9440D16ABD089F,
	ThirdPersonCamera_FixedUpdate_m5F8C7CAFEBCD439C28F037294958BE6EE4CDD905,
	ThirdPersonCamera_UpdateTransform_m6369B8526E00E22501A856A2C906AB03C238E10D,
	ThirdPersonCamera_UpdateInput_m272A78FA8280BF463260B1E669C7C247AD8F85FA,
	ThirdPersonCamera_ClampAngle_mEB3C5CE36CDAB23299A9AFAB856C50CB3BDA0547,
	ThirdPersonCamera_ApplyCrosshair_mA504A905D0310F650E539894E8AE7648443566EC,
	ThirdPersonCamera_CreateCrosshairUI_m1A21B6B226B3540D321AFE92DB4AD3DC3DA2E7ED,
	ThirdPersonCamera__ctor_m02A2D020EED2DE325F97F1962B62ECCF71A345EA,
	CharacterIK_Start_mBAF6688F04C077BD146E5299E265047A2856A3CC,
	CharacterIK_Update_m23E4BF47E7C8753AF25FE1E0F0E1C5004E408D6F,
	CharacterIK_OnSetControllerActive_m6730B36DF30D884FEF006192CB3EE0591045086A,
	CharacterIK_SetIK_m9905D5CC94A53B37A8F83E758C357055A58D68F9,
	CharacterIK_OnAnimatorIK_m235619FCCF9C3EA298DE8F74D75C31789828A6A7,
	CharacterIK__ctor_m8B1445F68D69B1614C6A3743E31BBC3B28E5C912,
	NULL,
	NULL,
	MotionState_get_FriendlyName_m2E37D82936656FE87D1B56F80C17FDC064A77DD3,
	MotionState_get_InputName_m6ED2F3C38784BA9132A99B4469CF2D254239B294,
	MotionState_set_InputName_m62C2FA34D18C533AEC4984392A716DC7FB18116D,
	MotionState_get_StartType_mC31B140194F959BAAB82089031DCCD3CA18F3C33,
	MotionState_set_StartType_mAA385808BFD6B881FBEDE4C290BA001AF5F256F1,
	MotionState_get_StopType_m322BBB22ADDC451E4CE4082BA3064DCC70513BCB,
	MotionState_set_StopType_mA7A9E318BF4272BB022619300AEB79B792E6C3DE,
	MotionState_get_ConsumeInputOverUI_m16294BF35B820EDC16086B1A918D97A93064CA42,
	MotionState_get_PauseItemUpdate_m5744E5C2811EC4119F79F4C6B6A0C21C4FCB200A,
	MotionState_get_TransitionDuration_m9A9ECDCFACC53685647EA5704ACE79B8D45056AC,
	MotionState_get_State_m68505E49A9692B31214C2CA6B9A9BD820F1B4027,
	MotionState_set_State_m26D42BC25AE67B1FA509B68E6898824ED4D5159F,
	MotionState_get_CameraPreset_m4A55C028C70F6119BADAAA5DB8CF7C5426D5F78A,
	MotionState_set_CameraPreset_m2DEBF67B9847A126A21073E7C35920990E1FA88B,
	MotionState_get_IsActive_m9186282C449725307BFE805F642CBFB6B2BFBC45,
	MotionState_get_Index_m9BBC0BC8A6A6778105ADCA94CF18F0A0ADCB2D15,
	MotionState_set_Index_m9127DF697416A51771189885D1E2DDCB55CD525F,
	MotionState_get_Layer_mE2E30DB7E89A3BF0C593B5E16311B50EB72D6CD1,
	MotionState_set_Layer_mE6620BFFE63CF0CEDE192AC906AFB87F28CEB8A4,
	MotionState_get_Controller_m0726C169E4762550E883C15E366C062A373CF59D,
	MotionState_set_Controller_m25813DCC6F47C18E0EB2F041AB6BEAE4BAF86A89,
	MotionState_Start_m2EA152613E852CEE0B88E6DC7B9A3FC9B977D9F2,
	MotionState_StopMotion_mC06D24B24D5CEBA0390A7FEFD0F0A8C39E094083,
	MotionState_StopMotion_m2876A004AE5EC7C8C87599629A71DE0C0E4717A8,
	MotionState_StartMotion_m7288F39BF6521D3A3F97C4282430685B04B2E63A,
	MotionState_IsPlaying_m9BE6C15D262AAFF324C2157F4B77AFA294CAC2CA,
	MotionState_CanStart_m7CBE8D33ADC3A4D94C6225C908DB066C4AA2AA60,
	MotionState_OnStart_mAA277DED77A6D3DD5E1D3A879CD9AFEC4141E3A6,
	MotionState_UpdateVelocity_m3FA2496BDF171AD5EF220221202AAE7AAB4EF171,
	MotionState_UpdateRotation_mD76ADDE0F08412308FD89B171BF8B1154F0BB4D2,
	MotionState_UpdateAnimator_m11FB1B75174F0495DE737F3986CCEC60439278BD,
	MotionState_UpdateAnimatorIK_mE5D69456189DF88DBFA1C3E8BC4B88E51147803E,
	MotionState_CheckGround_m9AB281E59548CF357053976B133387F2F6323064,
	MotionState_CheckStep_mA5E6F0B06971F22162D41330EDE482D13FF2AD02,
	MotionState_CanStop_mF01B2EEA242EC27B83C827D05D1DBD8A2FBABA24,
	MotionState_OnStop_mE81FF86754C3ACC418E590A32E9D564A2C0CB74D,
	MotionState_GetDestinationState_m8CC202AE9980D38D4D96CF7BA5ECA19BE67D7872,
	MotionState_MoveToTarget_m429F292A47C2BAE03BE73785E4A91148174643BB,
	MotionState_MoveToTargetInternal_m72F3F436692B580CD4928242CCBB777E42D9F73E,
	MotionState__ctor_m7D12BFEFD99B9DC8409C56FFC363F93148B68AC4,
	MotionState_U3CStopMotionU3Eb__55_0_m2EC938B7561925B6F1F570CCA17EDC47640CBAF2,
	MotionState_U3CStartMotionU3Eb__56_0_mCAB72567DA15AC8E57D4880F4C11C5365AF9A1B7,
	U3CMoveToTargetInternalU3Ed__70__ctor_mE0912DBF67EA22130A0FAC617B41761DFB0103EB,
	U3CMoveToTargetInternalU3Ed__70_System_IDisposable_Dispose_m023D24E65C4F951C4520F067C13E7B7359BA5575,
	U3CMoveToTargetInternalU3Ed__70_MoveNext_mC160CD9C016EF209CA9E806E359FF1920385BF94,
	U3CMoveToTargetInternalU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58C1F09B256FCDB31569F143F4E939C24B2CC96D,
	U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_Reset_m4FCAF97E4E9531B458C76E05942EFD943AB035A1,
	U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_get_Current_m015E37275E63AD25EC7633D4814097579FCE2700,
	MotionTrigger__ctor_m99A4BB05D312956038CBD765FB06CFA3B6A8D8F7,
	ChangeHeight_OnStart_mBE2AB8C65A894538CD5B0402CCE6088A64FA45B6,
	ChangeHeight_OnStop_m2F9CACD3F11147107FFD0072A70AE81AE38FC351,
	ChangeHeight__ctor_m9D5EA7291013DC36D3762845C0D6A29AFAEBF122,
	ChangeSpeed_OnStart_mC8FBC3ED8C60ABD34473ED755028F158B8361F7F,
	ChangeSpeed_OnStop_mAC9944D3ABE93311C68627D9741EFE25F5617F97,
	ChangeSpeed_CanStop_m66FBE793B98108D34DC59CA0C8FA761800722BBF,
	ChangeSpeed__ctor_mEAE5C5E02111F68AFC585D31C39CFA6EEF760449,
	Climb_OnStart_m413689D85F7F3C53753BA1663B87E3B9447B59DA,
	Climb_OnStop_m4C966990CDDBED2DAF75D49370063C7331A9E535,
	Climb_CanStart_m38150F14FF2ACE192ACB318026F12C13A32BFCE4,
	Climb_UpdateAnimatorIK_m0B3C9E7729167A9F0FF838E3B725258EBA6128BD,
	Climb_OnDrawGizmosSelected_m6DB1D44BE8FE05104E311E9997B1C8935F1DBE0A,
	Climb_DebugRay_mE87CFF76E5750707BD33C3B33D4F7A85A8AF8A8A,
	Climb_UpdateVelocity_m8E7F3F4A77716D66F71E959768AF7079B840A33F,
	Climb_SetIKWeight_mFA96105961A064ED4CEBF827A21CF943EBE4F491,
	Climb_UpdateRotation_m37A457F4C3008AC1D44B2939F3B2716E27D7127A,
	Climb_CheckGround_m4E49A4955F2079B730B62AFB2986AD28EE4FF44C,
	Climb_CheckStep_mD7C820CB5053D760A2F4EA5446AD576E86AC3C91,
	Climb__ctor_m6B5B28F82C4B4560A5972E794767BD69F0E593DC,
	Fall_OnStart_m7CA3AD39DD0DCDD00AF5EB3F93209B20C0782899,
	Fall_UpdateVelocity_m3A3954522CE352CE5B8324D4F9DC4899481901BF,
	Fall_UpdateAnimator_m281E0E927403E3826DA2A91BB4BDF335F596906E,
	Fall_CanStart_m1CAF87A6D903A48DEC6773DD947D3864B1E751F5,
	Fall_OnControllerLanded_mB052A5F7AB54387362C47AF2AFC31F17154E28CA,
	Fall_OnControllerGrounded_m24CE44325916D640D39005AA579C08FF6A9D3B58,
	Fall__ctor_m66A7BE743BF7F78B4BD6DF1F72FB735CCA871D37,
	Jump_OnStart_m265FF26E362B420CD8F16A409C244C59E3A78008,
	Jump_UpdateAnimator_m72A3A13EB3F2D487C8121CB64EB9BAD343F9D615,
	Jump_StartJump_m3674E2CC1559CABAC674707672DD46E68E891EC1,
	Jump_CheckGround_mE797CDD80464FBBC78D414CF1B35C935CCE68352,
	Jump_CheckStep_m1B906AC1068A9075B47F8FCA6B5C0CEAA0CE6264,
	Jump_OnControllerGrounded_m80387B520806C6D3A3F73DC0D34FEADC76D5062C,
	Jump_CanStart_m022E99C906EB3682F006BFEB11962DC5222BBA0D,
	Jump_CanStop_mADE3B195DD8146EA78393FEAA058CA64A1AAA687,
	Jump__ctor_m47E13118BE82F95E094B545955B4F00B4F2DE74D,
	Ladder_OnStart_m3030297160C2FB9E275B06B450C8885EDD956DA7,
	Ladder_OnStop_mE618431B5D65DF22EB12A6BEFF927A22E3747713,
	Ladder_CanStart_m87146BA4E66AA5BB7DB1EF2D5AD8B54203049465,
	Ladder_CanStop_mADDA032D7CB2DE9B45EB72E1DD96FE2E9EA5FA31,
	Ladder_UpdateVelocity_m50ED91BB0A4E180EF8AC3F73F89D5CE25223930E,
	Ladder_UpdateAnimator_m46C54DF0C5EDB98A3229E3554B42959F53357AF4,
	Ladder_CheckGround_m81E79395BD8EB7D6EA4A1F911BB667034F45B356,
	Ladder_UpdateRotation_mBB6FA81C8110C91348BFF4631CB9947823A888AA,
	Ladder_OnTriggerEnter_m5D56272F2E903A0C6B35A1F141EA8520EB26F21D,
	Ladder_OnTriggerExit_mE5620D722E1C8412E57CC2053E6B252485C92358,
	Ladder__ctor_m309E03A049C0D27D86D89BFF3CF83C4FFD1B1944,
	Ladder_U3COnStartU3Eb__3_0_mBC4D49695B7E552BC419F620728064B13CD93DF3,
	Push_CanStart_mCBF13130E6C762BACCE993BA03A3065FA49CEDF8,
	Push_CanStop_mACD4987B93E2BE2BC1AC2971A30DA6F71C1753C4,
	Push_OnStart_m40B0BE6235F95E7DAFAD2EDCE60342387F5D6679,
	Push_OnStop_mAFBDE280C7A0E7DF5E351ED34C6E2E8E00F21B81,
	Push_UpdateVelocity_m0294E2801C59CA2EFDBD0C0536B39E3F67BCABB5,
	Push_UpdateAnimatorIK_m54222D970F47B2AE0F800EC785E50148AE0037E1,
	Push_UpdateRotation_m10841DC32DDFC59ED1A1FE03082CD6CEF07BF80C,
	Push_CheckPush_m2C4AF8960CBFA66C3D1016B76B7EA8CD37B11CFD,
	Push__ctor_mD719C94C901B27B1B22AD9F84C7A66EFE3487EA2,
	Push_U3COnStartU3Eb__5_0_mDCFA0375A9026A356B13D8E7B2FF6DD0F030CB0D,
	RandomMotion_GetDestinationState_mE50F0CC119DAFEA1499426C7D55AC8FDB5CA77C8,
	RandomMotion__ctor_m95981E967D8C68A0D128A11557E2FF5C0EA91D37,
	SimpleMotion_OnStop_m0E5114C7FD925CEE4644D7B40FFAE7C2151B88A7,
	SimpleMotion_OnStart_m7FFA5BA266E6C3D4467CEEE37B2772E5289FE49F,
	SimpleMotion_CanStart_m7724CE681A1D388420880FC2F0D9D4F382E950B2,
	SimpleMotion__ctor_mF6B316BA85468044C2D4160738D0C828CDF9FCC7,
	Slide_CanStart_m0C72EBFE3518C8CDF46343B6AB45CC980C44C7D2,
	Slide__ctor_mA743ABA827FA73E769E238751EBDF81C17B68930,
	Swim_OnStart_mF0546141A4CF072DCC851013E90E73BEE84D4E0C,
	Swim_OnStop_m099152A263A3F84C92C6A3F9F403E238A675E3A4,
	Swim_CanStart_mD99A62FA03A20CF5FBB48EC4E79E8814B67B320A,
	Swim_CanStop_mDEA2522B7BD3F0BECDE1562A490B02C93AA8EC19,
	Swim_UpdateVelocity_m1081E2D3092D694361AC5A30F8DC8A045E9BE691,
	Swim_CheckGround_m95F5D7F6262491CEBED6EA13A73858CAEA29D4AC,
	Swim_UpdateAnimatorIK_m5F5A9C3416D93F25E002A78B2FA0F3C63D3FD798,
	Swim_OnTriggerEnter_m6601A12C044076AD4195EDE79247D0B9C7E87D7C,
	Swim_OnTriggerExit_m50C56775ED198AFA224A8746461C829CFFFC9435,
	Swim__ctor_m1737AC2D452C80CA2E96E902A510B2B0E606D327,
	PushableObject_Start_mF1AC1EF2BC959959D9DB6CD4ACA7615A5B346B4C,
	PushableObject_StartMove_m972E6030D2C82B82828A6E6D4B452ADF72E3BE52,
	PushableObject_StopMove_mC62078AD20F46429CE013D37ED8DF31736CF8DF4,
	PushableObject_Move_mE195448AF71C90BD12E9D115C417326051B629AA,
	PushableObject_OnCollisionStay_mD4E559F057918F19A4295B191F953B2DE2236851,
	PushableObject__ctor_m4E045B87B5B225740C25B1D0D0F73BA5B4072067,
	SetLayerWeight_OnStateEnter_m56693B786E8845A007287C83B772B7BB379E3DC0,
	SetLayerWeight_OnStateUpdate_mD225606CEB6D36CB4E7424DCD2678E4622A0B689,
	SetLayerWeight_OnStateExit_m3734414E0A2B889410888802B16D78BF5CE289C3,
	SetLayerWeight__ctor_m9D6D8FCA639165B2D34156CB87D2A5CBCE9F0A9C,
	SwimTrigger__ctor_m7991BAAB23F1024C02327A4C0D7F3F7007510707,
	ThirdPersonController_Execute_mBED7686CD45BB739E44881CBF390853E843593F8,
	ThirdPersonController_Execute_mB02B8B12BC685AFEDC5963360E861655386F0D56,
	NULL,
	NULL,
	ThirdPersonController_get_Motions_mEF21890C477BD0014D3E3EC34AC811AC98BE094C,
	ThirdPersonController_set_Motions_mD418ED8EDAC7DEAE02ED8E5B3D9CA5626D41A7EF,
	ThirdPersonController_get_ForwardInput_mACEA97EF6CF281F00F2A539BCB35D139C3E27B3F,
	ThirdPersonController_set_ForwardInput_m5CFB4CA23D850BCD1A919C3E480C1E077C3A5B79,
	ThirdPersonController_get_HorizontalInput_mEFB42BE0D6ECCE92D99F380229E6C7B7B84D3646,
	ThirdPersonController_set_HorizontalInput_m5D93E98309ADB1E0C385637574F3A65F05C59798,
	ThirdPersonController_get_SpeedMultiplier_mD56C4502292F7CBB94C008E830CCCD7AEF1188EE,
	ThirdPersonController_set_SpeedMultiplier_m3FFD97AF6FDCDA4FF0D449C147768470C5812F82,
	ThirdPersonController_get_AimType_m1D9D021B380F37FFCBC12AB99C8DD46500EE5C11,
	ThirdPersonController_set_AimType_mE71A418944D98B659438A48C19E703AE3C19C4A1,
	ThirdPersonController_get_AimInput_m0B5800FFF22264E33A286EE92FA9596A90AA7529,
	ThirdPersonController_set_AimInput_mBCAAB1DAA8CA0AD8A49095EC5EE2CF331C540F59,
	ThirdPersonController_get_AimRotation_mD70E74E452F380651211669EE4EC1CCE84B5AADF,
	ThirdPersonController_set_AimRotation_m69E27A7F3728A36FB597BE29548C4C6684825A7A,
	ThirdPersonController_get_RotationSpeed_m8AF019DF9358455822CC4394F50C636A68644D83,
	ThirdPersonController_set_RotationSpeed_m7DCFF13D8E09C69F609C923ACB915FD426FC5AF8,
	ThirdPersonController_get_AirSpeed_mB4D15B2871AB87BA2F6303FE7700DD91D6791525,
	ThirdPersonController_set_AirSpeed_mF2D5280DC7F10073330D8192DF25BB7D24E9A4F4,
	ThirdPersonController_get_AirDampening_mE3170D13A836275CEEAA05499E7CB34433A5AC20,
	ThirdPersonController_set_AirDampening_m0D9606544C46BC6282806383BC4067D49CAE68AF,
	ThirdPersonController_get_GroundDampening_m129DC904CD1462BA0B7B659AFD3DE4AA40BBFA30,
	ThirdPersonController_set_GroundDampening_m62A6E291AA90D27877004ADB3EBD389E9231AEE0,
	ThirdPersonController_get_StepOffset_m58DAE60774E03A4CAF8760219C3F4B7010159114,
	ThirdPersonController_set_StepOffset_mD43ED8407E09C06C6645F2B3EE2814D974809A2C,
	ThirdPersonController_get_SlopeLimit_m06E84F89C064449C2FF47C17850F77EA95DEB060,
	ThirdPersonController_set_SlopeLimit_m2B375AA83AF330EA2E24E931DB2E40E4F6AB0ED0,
	ThirdPersonController_get_GroundLayer_mE1F8ACD7EE0C496DAFE9483E02CF001170B61872,
	ThirdPersonController_set_GroundLayer_mF77FD9E68F83E38876AA4DDBB7BFCE9456BD964B,
	ThirdPersonController_get_SkinWidth_m6CCB3699811CE1370A202F14A717A643347A55C1,
	ThirdPersonController_set_SkinWidth_m7F3BB70B4DF6964A48AB551C505E9B199D654546,
	ThirdPersonController_get_IdleFriction_m63F8AA5B808F6FB752A2B2390BEA77AD0D235D89,
	ThirdPersonController_set_IdleFriction_m1E4BB1A493B02EC1B554AE8F8585E0CBFC5B6920,
	ThirdPersonController_get_MovementFriction_m05B9A53240469980958B569E71F617176F5D5A55,
	ThirdPersonController_set_MovementFriction_mD6084BA4F7B84C62E6CAB7F5D78C904A6EF5C711,
	ThirdPersonController_get_StepFriction_m158E2BCA0D4E3B2E1D8A1E38AFF104240BCB6C27,
	ThirdPersonController_set_StepFriction_m134C206111FBA26F100EC6FF53543B2E750092C2,
	ThirdPersonController_get_AirFriction_m9A57F9A09C4A07817C8262788F24DFF523A93154,
	ThirdPersonController_set_AirFriction_m6F00197BA2BD83DF46D08A09D913A5C7E8A7F9F7,
	ThirdPersonController_get_ForwardDampTime_m23C569DA47626ADD61CCE18C08E9B15B0F68C64E,
	ThirdPersonController_set_ForwardDampTime_m8A925FAD99044753FB02B0E443C8739C0463BABC,
	ThirdPersonController_get_HorizontalDampTime_mC5EF620EDF4819AA823809AFF448AA199F8CAE0A,
	ThirdPersonController_set_HorizontalDampTime_mDDCBEF514F1433150F82C94900C02EED4B9A2411,
	ThirdPersonController_get_IsGrounded_m92443FAD52F62A5A7D27EBF330078A48ACDF5CC9,
	ThirdPersonController_set_IsGrounded_m71A6FB41C399527523BADE9D719199E0A8F81C2C,
	ThirdPersonController_get_IsStepping_m483AD98F36C8712F36B0B4775FC34070A27DD9D0,
	ThirdPersonController_get_RawInput_mA6E441407CE8DC30E4A8A024C254D568EDE25F80,
	ThirdPersonController_set_RawInput_m745F096DB195D2945A9A4B34B3E111D82793B938,
	ThirdPersonController_get_RelativeInput_m0546685CD4C1B2292DA27A027737A438574E2233,
	ThirdPersonController_get_IsMoving_m31B5B25751480A77D97BB67EADFE1E6AF07C8AA0,
	ThirdPersonController_set_IsMoving_mC592B5FCA986E0DC014A572041BCA969191EC96E,
	ThirdPersonController_get_IsAiming_mCAD091F0336F0BCC3A00C72F8446D5276A4A74B3,
	ThirdPersonController_set_IsAiming_mD103532ED9160908A1F58D04AA17B981E9FE48A0,
	ThirdPersonController_get_Velocity_m424EB317131D0A0DFC11221C6B9B31E77BE0FAD3,
	ThirdPersonController_set_Velocity_m37253B0A4E796C30B84BE73E6A4206A0E4065023,
	ThirdPersonController_get_LookRotation_mD8C65903EA392FD5A28E4D2AD5DE30ADCF4F33EA,
	ThirdPersonController_get_RootMotionForce_m908905A59AC3A544DEE749F1E5B90AB323B54A04,
	ThirdPersonController_Awake_m3C0B359CCA4A7CEEE956B7A7C2E0CB0A0576F8E2,
	ThirdPersonController_OnEnable_m32C301AAD7EB104928D3BEB341BB6F35AF933975,
	ThirdPersonController_OnDisable_m4462CDE564C8CB41E80A5FBB69D950F4A1FA31DA,
	ThirdPersonController_Update_mF96FF6BD5D7D7B8F093A40CBB7A64D71D6D3E3A5,
	ThirdPersonController_FixedUpdate_m84B82193111BB00AAF5F93780B79AF62A8BA5AC1,
	ThirdPersonController_OnAnimatorMove_m763438BC176F86F4D8D4A289500046EDB8927BC9,
	ThirdPersonController_UpdateVelocity_mA91AE0076944E09854C99E88FB2572B476DEACC7,
	ThirdPersonController_UpdateAnimator_m5DDE89B996B281A64FDF5E8823ED2582BAEED015,
	ThirdPersonController_Normalize_mE72F0E9FA3C14AAFCB0E99138D39B3120A9BF381,
	ThirdPersonController_GetSignedAngle_mD6794D25EB41E6F978E017914BD81D02D1504028,
	ThirdPersonController_UpdateRotation_mEE706B72B4CB5107EE502D93669EFFDCC6F90DC2,
	ThirdPersonController_UpdateFrictionMaterial_m0BEAC893C798984C21B18B023CD6DF45AD953A72,
	ThirdPersonController_DeterminanteDefaultStates_mB82EE6DC8A1142154A266BBD28F4CC7BBCAE8410,
	ThirdPersonController_CheckDefaultAnimatorStates_m51E1C4825FD1685F6784AFAC28E4CF547E5932C7,
	ThirdPersonController_CheckGround_m3586A065B84123401DE8689C1F488FDF672036B1,
	ThirdPersonController_CheckStep_m9AE29681A952EF70A821A135EE67C50D8D654D2C,
	ThirdPersonController_TryStopMotion_m2CE8DD9C76481E04D9FB6FDAD245E480C08215DF,
	ThirdPersonController_TryStartMotion_m7CF4B2DFD2B01275855356AB828C98F769A8FD3F,
	ThirdPersonController_SetControllerActive_mBCA172AEC596F46277BCEAB1E8E68A04066CA18E,
	ThirdPersonController_SetMotionEnabled_m3EDED81F46FFA2F5CE5E562A680B76CCD925B337,
	ThirdPersonController_SetMotionEnabled_m7FA57C9EDAB5E2D14F17F687EBB1679F0BAAC295,
	ThirdPersonController_GetMotion_m2E92197B956BF9B95C32B1627D26549A351DBB58,
	ThirdPersonController_Footsteps_m2A3BE5D48E1C85CCDE6F2760E282EBB12B53B638,
	ThirdPersonController_PlaySound_mCA7660BFF60BD19EBC1C368F735303191C99E8C3,
	ThirdPersonController_CopyProperties_mA1772F32090D040389E17C8B1DCCB51F83420744,
	NULL,
	ThirdPersonController__ctor_m575BF3E1A14CC272C2CEF96945B25DA1952C85B3,
	ThirdPersonController_U3CAwakeU3Eb__143_0_m69772D712CCC99CE0F597EFE68BC1E028F51ABB3,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass155_0__ctor_m11530FFC547EB22B080C4702570C97E28E8C6CA9,
	U3CU3Ec__DisplayClass155_0_U3CDeterminanteDefaultStatesU3Eb__0_mF08DC12849D9CCD47FB3B798ED6EFE78B3820A1C,
};
static const int32_t s_InvokerIndices[307] = 
{
	910,
	2975,
	910,
	910,
	910,
	1450,
	2975,
	2912,
	2448,
	2912,
	2448,
	2897,
	2434,
	2968,
	2506,
	2948,
	2484,
	2912,
	2448,
	2912,
	2448,
	2948,
	2484,
	2948,
	2484,
	2968,
	2506,
	2968,
	2506,
	2948,
	2484,
	2948,
	2484,
	2968,
	2506,
	2948,
	2484,
	2948,
	2484,
	2897,
	2434,
	2941,
	2477,
	2904,
	2441,
	2948,
	2484,
	2941,
	2477,
	2948,
	2484,
	2975,
	2975,
	2975,
	2477,
	2975,
	2912,
	2912,
	2975,
	2975,
	2975,
	2477,
	2975,
	2975,
	2975,
	2975,
	858,
	2448,
	2975,
	2975,
	2975,
	2975,
	2477,
	2477,
	2434,
	2975,
	2477,
	2477,
	2912,
	2912,
	2448,
	2897,
	2434,
	2897,
	2434,
	2941,
	2941,
	2948,
	2912,
	2448,
	2912,
	2448,
	2941,
	2897,
	2434,
	2897,
	2434,
	2912,
	2448,
	2975,
	2975,
	2477,
	2975,
	2941,
	2941,
	2975,
	2055,
	2941,
	2941,
	2116,
	2941,
	2941,
	2941,
	2975,
	2912,
	349,
	261,
	2975,
	2133,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2941,
	2975,
	2975,
	2975,
	2941,
	2116,
	2975,
	849,
	2055,
	2484,
	2941,
	2941,
	2941,
	2975,
	2975,
	2055,
	2941,
	2941,
	2975,
	2477,
	2975,
	2975,
	2941,
	2975,
	2941,
	2941,
	2477,
	2941,
	2941,
	2975,
	2975,
	2975,
	2941,
	2941,
	2055,
	2941,
	2941,
	2941,
	2448,
	2448,
	2975,
	2975,
	2941,
	2941,
	2975,
	2975,
	2055,
	2116,
	2941,
	2171,
	2975,
	2975,
	2912,
	2975,
	2975,
	2975,
	2941,
	2975,
	2941,
	2975,
	2975,
	2975,
	2941,
	2941,
	2055,
	2941,
	2116,
	2448,
	2448,
	2975,
	2975,
	2448,
	2975,
	2195,
	2448,
	2975,
	910,
	910,
	910,
	2975,
	2975,
	4276,
	4276,
	-1,
	-1,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2948,
	2484,
	2897,
	2434,
	2912,
	2448,
	2948,
	2484,
	2948,
	2484,
	2970,
	2508,
	2948,
	2484,
	2948,
	2484,
	2948,
	2484,
	2948,
	2484,
	2904,
	2441,
	2948,
	2484,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2948,
	2484,
	2948,
	2484,
	2941,
	2477,
	2941,
	2970,
	2508,
	2970,
	2941,
	2477,
	2941,
	2477,
	2970,
	2508,
	2920,
	2970,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	858,
	857,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	2477,
	2448,
	1449,
	1898,
	2448,
	1450,
	2448,
	-1,
	2975,
	2133,
	-1,
	-1,
	-1,
	-1,
	2975,
	2133,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x060000D8, { 0, 3 } },
	{ 0x060000D9, { 3, 1 } },
	{ 0x0600012B, { 4, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)3, 22661 },
	{ (Il2CppRGCTXDataType)2, 288 },
	{ (Il2CppRGCTXDataType)3, 7245 },
	{ (Il2CppRGCTXDataType)2, 287 },
	{ (Il2CppRGCTXDataType)2, 289 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_ThirdPersonController_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_ThirdPersonController_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_ThirdPersonController_CodeGenModule = 
{
	"DevionGames.ThirdPersonController.dll",
	307,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	g_DevionGames_ThirdPersonController_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
