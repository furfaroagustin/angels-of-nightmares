﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
static void DevionGames_UIWidgets_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tDEE233FFFBD4EE0842B8451812576EFC59700FE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DialogBoxTrigger_tD484C903325C75A46D871DED91D6CEF2996F081A_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void U3CU3Ec_t399ADCA0A44AF0E2D4A2EE11057A4F858D79B3B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_filterMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_filter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_submit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_Chat_U3COnStartU3Eb__6_0_m3F9CA70497463E6A6E0A11EA176E9EBCE59CE02A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509_CustomAttributesCacheGenerator_m_MenuItemPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t2B062E3032D0F4F86D1F3EF7C1FF47F0A8D07A6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC47BA4AFB58EEBF4B837257267EAFD91DF07F47F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_title(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_mC24BE23C2C0B4C61FD24D23149B66CD070814D4F____buttons2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m65033F147B563EAFC7DC9A5C2D1124EB28176757____buttons2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m29679F5AD10130585BB848ED347461F89932B474____buttons3(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m9EE2AB39BFE7EA8D3551D237A37E43A155468460____buttons4(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_U3CU3En__0_m22421082B06171A00E7C8468E080BC9D2D8112D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_tC4745294364ED5350AB2CFA942BD913266E10F73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_1_tC2FB1FD91FC73BAF41977ADDC6E88606A7AAA079_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t114B0495587BDBFD508130A2D590A0BB5E43DAC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FloatingTextManager_t633B7A9A74D9407C5F86FFFCFB07C407670B2AA7_CustomAttributesCacheGenerator_m_Prefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_handle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_returnSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_horizontalAxis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_verticalAxis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE_CustomAttributesCacheGenerator_Notification_AddItem_mB6F6943FD716314F45BF3BD5E099DE7FFFA4C073____replacements1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE_CustomAttributesCacheGenerator_Notification_AddItem_m661D7EEB2F92E26A6F8013A135FC116D9FCCDF8B____replacements1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Time(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Icon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_NotificationSlot_DelayCrossFade_m6F7ABE7FFFB3561DE95092EDAD10CB573B0FCCD5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_0_0_0_var), NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5__ctor_m7561C710C476831299D12A048E7370021E44A700(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_IDisposable_Dispose_mC49A564E2431BEB17C9C1B8EC89C866C456269B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BBF68BDD827FB3FEC158DBEE89016274372D1E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_Reset_m4AB5F3841F027E448E456C6E561CE2D0BCD08DC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_get_Current_m2381C612381549E92122E9AD564C8777E90E48DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_progressbar(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_m_ProgressbarTitle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_progressLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_format(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Angle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Item(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void U3CU3Ec_t8691E922856D214CA0D00164135F8EC45737B550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tDE63FD402D5BE2E53CA1ABE21CC200FF165ED953_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_1_t793D8440B3601308C7DC139F2E09157AA689AC08_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A_0_0_0_var), NULL);
	}
}
static void AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator_m_MixerGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator_m_ExposedParameter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_instanceName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_showBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_color(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_tooltip(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_TooltipTrigger_DelayTooltip_m914A98EBA471C690E95541FDE6B1D3BE664B19F2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_0_0_0_var), NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14__ctor_m039406E2ADA4CA0AD21A0F06D03082D9DC04E5FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_IDisposable_Dispose_mF0870C95526BAE3BC8B1F2149DC8D405EDD4602C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DEC7C9FF58B39F959BE337BFBCE491F4BDF6CBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_Reset_mEA8201EEA14E7762C4E2F9AA327CCEADDCD1B583(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_get_Current_m797FF401C8FF76D65A3F038EC70C2F06F35DD844(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_m_Current(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_Spinner_Increase_mC322782BB67E030A046096639EC45C9D17A41C6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_0_0_0_var), NULL);
	}
}
static void Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_Spinner_Decrease_m85EC7B47F523CEC45A28D681B3CC10EEBD6CB5AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_0_0_0_var), NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15__ctor_m332B26987322491F88F1ACA5BEC12CDF110CB2C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_IDisposable_Dispose_m481F5174A264917BBCF83788D1C79981EAA63591(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13FE33844E4F95E9E5D24DE179CB014E77E0922D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_IEnumerator_Reset_m7248C18ABE14F91F640405744F9C560E91ACA18B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_IEnumerator_get_Current_mF3D7E2B8E04DEFCDB6A2AF9082B74E8D059DC47B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16__ctor_m54DDB1C4E9792F142928896771DAAB3C7AA55E34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_IDisposable_Dispose_m35A0C2A44B9BE0684A5BDCE1CA243F5F7092587E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8122851D8FBC2D8CAFC52AF57A2467203978278A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_IEnumerator_Reset_m7ACF8BAEC2101194888F88932511F29761EA369B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_IEnumerator_get_Current_mEEB8F0EA23ED965BFD6BE4A6E05C1F67F2483401(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A_CustomAttributesCacheGenerator_m_Key(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tab_tD073C15948C6C3576F9FF7CA4FD1F04A5227C0B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_UpdatePosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_PositionOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Title(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Icon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Background(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_SlotPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TweenRunner_1_t4A1F6E0AFCD398698E2E22035B027FFDDC903424_CustomAttributesCacheGenerator_TweenRunner_1_Start_m33EB15AEFC8731780A22BCD2C9010EA3772DC494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_mC661960B3BF53D55AF3BC1BEC8613D3017942444(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mDF81BC55CF9E02491B4C940C4C3997F85E1EDBDA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE34733C022FAAAFB444627992158F1FD446A854A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2352EA2A19633826992E3A6945CB1377876236A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD5716F3C053272FD22E7320BB13CE2C74655C9E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_DynamicContainer(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_SlotParent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_SlotPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UICursor_t7AD1AA5D27C7F7C8813931642AAE2E264E683B3A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12(tmp, il2cpp_codegen_type_get_object(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_0_0_0_var), il2cpp_codegen_type_get_object(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F_0_0_0_var), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x69\x64\x67\x65\x74\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x66\x69\x6E\x64\x20\x61\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x61\x20\x77\x69\x64\x67\x65\x74\x20\x77\x69\x74\x68\x20\x57\x69\x64\x67\x65\x74\x55\x74\x69\x6C\x69\x74\x79\x2E\x46\x69\x6E\x64\x3C\x54\x3E\x28\x6E\x61\x6D\x65\x29\x2E"), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_priority(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x69\x64\x67\x65\x74\x73\x20\x77\x69\x74\x68\x20\x68\x69\x67\x68\x65\x72\x20\x70\x72\x69\x6F\x72\x69\x74\x79\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x72\x65\x66\x65\x72\x65\x64\x20\x77\x68\x65\x6E\x20\x75\x73\x65\x64\x20\x77\x69\x74\x68\x20\x57\x69\x64\x67\x65\x74\x55\x74\x69\x6C\x69\x74\x79\x2E\x46\x69\x6E\x64\x3C\x54\x3E\x28\x6E\x61\x6D\x65\x29\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_KeyCode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x70\x65\x61\x72\x65\x6E\x63\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x20\x74\x6F\x20\x73\x68\x6F\x77\x20\x6F\x72\x20\x63\x6C\x6F\x73\x65\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_EaseType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x61\x73\x69\x6E\x67\x20\x65\x71\x75\x61\x74\x69\x6F\x6E\x20\x74\x79\x70\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Duration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_IgnoreTimeScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_ShowSound(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x75\x64\x69\x6F\x43\x6C\x69\x70\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x20\x73\x68\x6F\x77\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_CloseSound(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x75\x64\x69\x6F\x43\x6C\x69\x70\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x20\x63\x6C\x6F\x73\x65\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Focus(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x63\x75\x73\x20\x74\x68\x65\x20\x77\x69\x64\x67\x65\x74\x2E\x20\x54\x68\x69\x73\x20\x77\x69\x6C\x6C\x20\x62\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x77\x69\x64\x67\x65\x74\x20\x74\x6F\x20\x66\x72\x6F\x6E\x74\x20\x77\x68\x65\x6E\x20\x69\x74\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_DeactivateOnClose(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x64\x65\x61\x63\x74\x69\x76\x61\x74\x65\x73\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x68\x65\x6E\x20\x69\x74\x20\x67\x65\x74\x73\x20\x63\x6C\x6F\x73\x65\x64\x2E\x20\x54\x68\x69\x73\x20\x70\x72\x65\x76\x65\x74\x73\x20\x55\x70\x64\x61\x74\x65\x28\x29\x20\x74\x6F\x20\x62\x65\x20\x63\x61\x6C\x6C\x65\x64\x20\x65\x76\x65\x72\x79\x20\x66\x72\x61\x6D\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_ShowAndHideCursor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x73\x20\x43\x75\x72\x73\x6F\x72\x20\x77\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x77\x69\x6E\x64\x6F\x77\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x2E\x20\x48\x69\x64\x65\x73\x20\x69\x74\x20\x61\x67\x61\x69\x6E\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x77\x69\x6E\x64\x6F\x77\x20\x69\x73\x20\x63\x6C\x6F\x73\x65\x64\x20\x6F\x72\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x6D\x6F\x76\x65\x73\x2E"), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_CloseOnMove(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x6F\x73\x65\x20\x74\x68\x69\x73\x20\x77\x69\x64\x67\x65\x74\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x6D\x6F\x76\x65\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Deactivate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x74\x68\x65\x20\x6B\x65\x79\x20\x69\x73\x20\x70\x72\x65\x73\x73\x65\x64\x2C\x20\x73\x68\x6F\x77\x20\x61\x6E\x64\x20\x68\x69\x64\x65\x20\x63\x75\x72\x73\x6F\x72\x20\x66\x75\x6E\x74\x69\x6F\x6E\x61\x6C\x69\x74\x79\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x2E"), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_FocusPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x20\x61\x6C\x6C\x6F\x77\x73\x20\x74\x6F\x20\x66\x6F\x63\x75\x73\x20\x61\x6E\x64\x20\x72\x6F\x74\x61\x74\x65\x20\x70\x6C\x61\x79\x65\x72\x2E\x20\x54\x68\x69\x73\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x61\x6C\x69\x74\x79\x20\x6F\x6E\x6C\x79\x20\x77\x6F\x72\x6B\x73\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x69\x6E\x63\x6C\x75\x64\x65\x64\x20\x54\x68\x69\x72\x64\x50\x65\x72\x73\x6F\x6E\x43\x61\x6D\x65\x72\x61\x20\x61\x6E\x64\x20\x46\x6F\x63\x75\x73\x54\x61\x72\x67\x65\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x21"), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_UIWidget_OnDelayedStart_m29784EEA937B392528DF388ED658083EFFA584D7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_0_0_0_var), NULL);
	}
}
static void UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_UIWidget_U3CTweenTransformScaleU3Eb__48_0_m29A828C4EB884DCBFE7E10F8F1BEE1F195F63051(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43__ctor_mC964BF60C0BFC9443251FA4E80D6BEC534845B60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_IDisposable_Dispose_m2A142D07BA5F9D45043DFEFA39A96AE20A625D3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96BC0C790C4AFAA8E5FABB91BAB4E09E6A031D68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_Reset_mA6EFD5C346DD2AA0C17B009E2EF9192CD345D206(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_get_Current_m34A7FD6BEE90C1B43DA48518D95DF0530B61B437(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass47_0_tB88B6F21B5C85C60A95EB76F3E0D5C8C944E22FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tCC933BC556B14EF39DA5E9C53FA3F3E7FD23D1EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_1_tD334816FC28CE56AEC9CA4970E9F3957E14F6EFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__4_1_t7CE665A792E97572F0CE714B50934712E1D505E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__5_1_t72A5A1DB8035BE336C659FDB5AA586CA439FB413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_UIWidgets_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_UIWidgets_AttributeGenerators[129] = 
{
	U3CU3Ec__DisplayClass3_0_tDEE233FFFBD4EE0842B8451812576EFC59700FE2_CustomAttributesCacheGenerator,
	U3CU3Ec_t399ADCA0A44AF0E2D4A2EE11057A4F858D79B3B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t2B062E3032D0F4F86D1F3EF7C1FF47F0A8D07A6E_CustomAttributesCacheGenerator,
	U3CU3Ec_tC47BA4AFB58EEBF4B837257267EAFD91DF07F47F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_tC4745294364ED5350AB2CFA942BD913266E10F73_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_1_tC2FB1FD91FC73BAF41977ADDC6E88606A7AAA079_CustomAttributesCacheGenerator,
	U3CU3Ec_t114B0495587BDBFD508130A2D590A0BB5E43DAC6_CustomAttributesCacheGenerator,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator,
	U3CU3Ec_t8691E922856D214CA0D00164135F8EC45737B550_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tDE63FD402D5BE2E53CA1ABE21CC200FF165ED953_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_1_t793D8440B3601308C7DC139F2E09157AA689AC08_CustomAttributesCacheGenerator,
	AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator,
	Tab_tD073C15948C6C3576F9FF7CA4FD1F04A5227C0B8_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator,
	UICursor_t7AD1AA5D27C7F7C8813931642AAE2E264E683B3A_CustomAttributesCacheGenerator,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass47_0_tB88B6F21B5C85C60A95EB76F3E0D5C8C944E22FC_CustomAttributesCacheGenerator,
	U3CU3Ec_tCC933BC556B14EF39DA5E9C53FA3F3E7FD23D1EF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_1_tD334816FC28CE56AEC9CA4970E9F3957E14F6EFE_CustomAttributesCacheGenerator,
	U3CU3Ec__4_1_t7CE665A792E97572F0CE714B50934712E1D505E9_CustomAttributesCacheGenerator,
	U3CU3Ec__5_1_t72A5A1DB8035BE336C659FDB5AA586CA439FB413_CustomAttributesCacheGenerator,
	DialogBoxTrigger_tD484C903325C75A46D871DED91D6CEF2996F081A_CustomAttributesCacheGenerator_text,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_filterMask,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_filter,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_text,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_input,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_submit,
	ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509_CustomAttributesCacheGenerator_m_MenuItemPrefab,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_title,
	FloatingTextManager_t633B7A9A74D9407C5F86FFFCFB07C407670B2AA7_CustomAttributesCacheGenerator_m_Prefab,
	Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_handle,
	Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_radius,
	Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_returnSpeed,
	Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_horizontalAxis,
	Joystick_t1C81E1EBFA847236DAA425DBB2E7FB5A10DE551D_CustomAttributesCacheGenerator_verticalAxis,
	NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Text,
	NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Time,
	NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_m_Icon,
	Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_progressbar,
	Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_m_ProgressbarTitle,
	Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_progressLabel,
	Progressbar_t834F244139979DDC45232F4241CEEB3AE38E59CB_CustomAttributesCacheGenerator_format,
	RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Radius,
	RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Angle,
	RadialMenu_tBA478D3898ADBB76012AD97921B6F114DF587115_CustomAttributesCacheGenerator_m_Item,
	AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator_m_MixerGroup,
	AudioVolume_t14AD5A3AED10C99941994C40781F64243E149000_CustomAttributesCacheGenerator_m_ExposedParameter,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_instanceName,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_showBackground,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_width,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_color,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_tooltip,
	Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_m_Current,
	StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A_CustomAttributesCacheGenerator_m_Key,
	StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A_CustomAttributesCacheGenerator_m_Value,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_UpdatePosition,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_PositionOffset,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Title,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Text,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Icon,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_Background,
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859_CustomAttributesCacheGenerator_m_SlotPrefab,
	UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_DynamicContainer,
	UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_SlotParent,
	UIContainer_1_t5DBA5E9EF0544E53BCA7A40F1AFF5A0CA26C8C84_CustomAttributesCacheGenerator_m_SlotPrefab,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_name,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_priority,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_KeyCode,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_EaseType,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Duration,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_IgnoreTimeScale,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_ShowSound,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_CloseSound,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Focus,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_DeactivateOnClose,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_ShowAndHideCursor,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_CloseOnMove,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_Deactivate,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_m_FocusPlayer,
	Chat_tAA4DBF16F7B592DE6854041C7CCF89513A4950F6_CustomAttributesCacheGenerator_Chat_U3COnStartU3Eb__6_0_m3F9CA70497463E6A6E0A11EA176E9EBCE59CE02A,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_U3CU3En__0_m22421082B06171A00E7C8468E080BC9D2D8112D4,
	NotificationSlot_t053B2CF87165010B8F35D676B38D50E1ADA85FF1_CustomAttributesCacheGenerator_NotificationSlot_DelayCrossFade_m6F7ABE7FFFB3561DE95092EDAD10CB573B0FCCD5,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5__ctor_m7561C710C476831299D12A048E7370021E44A700,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_IDisposable_Dispose_mC49A564E2431BEB17C9C1B8EC89C866C456269B1,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BBF68BDD827FB3FEC158DBEE89016274372D1E0,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_Reset_m4AB5F3841F027E448E456C6E561CE2D0BCD08DC3,
	U3CDelayCrossFadeU3Ed__5_t4158EEA9DA909000415BECD633A01DC2A6CA5647_CustomAttributesCacheGenerator_U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_get_Current_m2381C612381549E92122E9AD564C8777E90E48DC,
	TooltipTrigger_t1C58ADA94BA9668AA414F87F9BA53011148D2977_CustomAttributesCacheGenerator_TooltipTrigger_DelayTooltip_m914A98EBA471C690E95541FDE6B1D3BE664B19F2,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14__ctor_m039406E2ADA4CA0AD21A0F06D03082D9DC04E5FB,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_IDisposable_Dispose_mF0870C95526BAE3BC8B1F2149DC8D405EDD4602C,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DEC7C9FF58B39F959BE337BFBCE491F4BDF6CBB,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_Reset_mEA8201EEA14E7762C4E2F9AA327CCEADDCD1B583,
	U3CDelayTooltipU3Ed__14_tEE30698821DC6AC5B0F1B523DC662DA96042038B_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_get_Current_m797FF401C8FF76D65A3F038EC70C2F06F35DD844,
	Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_Spinner_Increase_mC322782BB67E030A046096639EC45C9D17A41C6D,
	Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_CustomAttributesCacheGenerator_Spinner_Decrease_m85EC7B47F523CEC45A28D681B3CC10EEBD6CB5AE,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15__ctor_m332B26987322491F88F1ACA5BEC12CDF110CB2C2,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_IDisposable_Dispose_m481F5174A264917BBCF83788D1C79981EAA63591,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13FE33844E4F95E9E5D24DE179CB014E77E0922D,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_IEnumerator_Reset_m7248C18ABE14F91F640405744F9C560E91ACA18B,
	U3CIncreaseU3Ed__15_t504356D292576A4C14242F1F0D8FAA3A39D0208B_CustomAttributesCacheGenerator_U3CIncreaseU3Ed__15_System_Collections_IEnumerator_get_Current_mF3D7E2B8E04DEFCDB6A2AF9082B74E8D059DC47B,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16__ctor_m54DDB1C4E9792F142928896771DAAB3C7AA55E34,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_IDisposable_Dispose_m35A0C2A44B9BE0684A5BDCE1CA243F5F7092587E,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8122851D8FBC2D8CAFC52AF57A2467203978278A,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_IEnumerator_Reset_m7ACF8BAEC2101194888F88932511F29761EA369B,
	U3CDecreaseU3Ed__16_t16AD49D235467669DCC20C0F8BE44776A574E2C9_CustomAttributesCacheGenerator_U3CDecreaseU3Ed__16_System_Collections_IEnumerator_get_Current_mEEB8F0EA23ED965BFD6BE4A6E05C1F67F2483401,
	TweenRunner_1_t4A1F6E0AFCD398698E2E22035B027FFDDC903424_CustomAttributesCacheGenerator_TweenRunner_1_Start_m33EB15AEFC8731780A22BCD2C9010EA3772DC494,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_mC661960B3BF53D55AF3BC1BEC8613D3017942444,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mDF81BC55CF9E02491B4C940C4C3997F85E1EDBDA,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE34733C022FAAAFB444627992158F1FD446A854A,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m2352EA2A19633826992E3A6945CB1377876236A5,
	U3CStartU3Ed__2_t9F95A67E2DFFFDD465DBF11229B506811484BF17_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mD5716F3C053272FD22E7320BB13CE2C74655C9E6,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_UIWidget_OnDelayedStart_m29784EEA937B392528DF388ED658083EFFA584D7,
	UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_CustomAttributesCacheGenerator_UIWidget_U3CTweenTransformScaleU3Eb__48_0_m29A828C4EB884DCBFE7E10F8F1BEE1F195F63051,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43__ctor_mC964BF60C0BFC9443251FA4E80D6BEC534845B60,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_IDisposable_Dispose_m2A142D07BA5F9D45043DFEFA39A96AE20A625D3B,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96BC0C790C4AFAA8E5FABB91BAB4E09E6A031D68,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_Reset_mA6EFD5C346DD2AA0C17B009E2EF9192CD345D206,
	U3COnDelayedStartU3Ed__43_tFF151AB998B89CEDD84E545206BC0F90FF5E0504_CustomAttributesCacheGenerator_U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_get_Current_m34A7FD6BEE90C1B43DA48518D95DF0530B61B437,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_mC24BE23C2C0B4C61FD24D23149B66CD070814D4F____buttons2,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m65033F147B563EAFC7DC9A5C2D1124EB28176757____buttons2,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m29679F5AD10130585BB848ED347461F89932B474____buttons3,
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486_CustomAttributesCacheGenerator_DialogBox_Show_m9EE2AB39BFE7EA8D3551D237A37E43A155468460____buttons4,
	Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE_CustomAttributesCacheGenerator_Notification_AddItem_mB6F6943FD716314F45BF3BD5E099DE7FFFA4C073____replacements1,
	Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE_CustomAttributesCacheGenerator_Notification_AddItem_m661D7EEB2F92E26A6F8013A135FC116D9FCCDF8B____replacements1,
	DevionGames_UIWidgets_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
