﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399;
// System.Func`2<DevionGames.Graphs.FlowNode,System.Boolean>
struct Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5;
// System.Func`2<DevionGames.Graphs.Node,System.Boolean>
struct Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<DevionGames.Graphs.Port,System.Boolean>
struct Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC;
// System.Collections.Generic.IEnumerable`1<DevionGames.Graphs.FlowNode>
struct IEnumerable_1_t948AC183EEC50A49B37034FB7AF07E9C0DF765EB;
// System.Collections.Generic.IEnumerable`1<DevionGames.Graphs.Node>
struct IEnumerable_1_t531EC530DF2ADB77384595CBDB63AE2B816B1D52;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<DevionGames.Graphs.Port>
struct IEnumerable_1_t319FBB24758098096BB7746864468A2E23477ADC;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_t0043475CBB02FD67894529F3CAA818080A2F7A17;
// System.Collections.Generic.List`1<DevionGames.Graphs.Edge>
struct List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55;
// System.Collections.Generic.List`1<DevionGames.Graphs.Node>
struct List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9D216521E6A213FF8562D215598D336ABB5474F4;
// System.Collections.Generic.List`1<DevionGames.Graphs.Port>
struct List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD;
// System.Predicate`1<DevionGames.Graphs.Edge>
struct Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256;
// System.Predicate`1<DevionGames.Graphs.Node>
struct Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// System.Predicate`1<DevionGames.Graphs.Port>
struct Predicate_1_t37A5DE42085D332C641B55950592659162DD2774;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tB942A1033B750DCF04FE948413982D120FC69A4E;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_tDCA1A62E50C5B5A40FD6F44107088AF42F5671D2;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// DevionGames.Graphs.Edge[]
struct EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E;
// DevionGames.Graphs.FlowNode[]
struct FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// DevionGames.Graphs.Node[]
struct NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873;
// DevionGames.Graphs.Port[]
struct PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// DevionGames.Graphs.Add
struct Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// DevionGames.Graphs.Ceil
struct Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DevionGames.Graphs.Divide
struct Divide_t0E2802337E30A27C1C488271C61C2214772281FF;
// DevionGames.Graphs.Edge
struct Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81;
// DevionGames.Graphs.Evaluate
struct Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39;
// DevionGames.Graphs.EventNode
struct EventNode_t6C7BF4F88C4264940AED17FE36C8E8A0E398AD8F;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// DevionGames.Graphs.Floor
struct Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9;
// DevionGames.Graphs.FlowGraph
struct FlowGraph_t9A6068907F5E8BFFA7E6376D59860B7DBC8577E0;
// DevionGames.Graphs.FlowNode
struct FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195;
// DevionGames.Graphs.Formula
struct Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA;
// DevionGames.Graphs.FormulaGraph
struct FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15;
// DevionGames.Graphs.FormulaOutput
struct FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5;
// DevionGames.Graphs.Graph
struct Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IList
struct IList_tB15A9D6625D09661D6E47976BB626C703EC81910;
// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// DevionGames.Graphs.Multiply
struct Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F;
// DevionGames.Graphs.Node
struct Node_tD0AFD8B27090B24E967D338520DEF077662A61DF;
// DevionGames.Graphs.NodeStyleAttribute
struct NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672;
// DevionGames.Graphs.Port
struct Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5;
// DevionGames.Graphs.Pow
struct Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC;
// DevionGames.Graphs.Random
struct Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// DevionGames.Graphs.Round
struct Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A;
// DevionGames.Graphs.Sqrt
struct Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF;
// System.String
struct String_t;
// DevionGames.Graphs.Subtract
struct Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// DevionGames.Graphs.FlowNode/<>c
struct U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646;
// DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40;
// DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680;
// DevionGames.Graphs.Formula/<>c
struct U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F;
// DevionGames.Graphs.FormulaGraph/<>c
struct U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C;
// DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509;
// DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1
struct U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7;
// DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D;
// DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1
struct U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0;
// DevionGames.Graphs.Port/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_t37A5DE42085D332C641B55950592659162DD2774_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78;
IL2CPP_EXTERN_C String_t* _stringLiteral198AA065BF0F912BD6F5F93869BD5C361671F98B;
IL2CPP_EXTERN_C String_t* _stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral2C945D246C2B7897F000E1C591A686EB9EF010F0;
IL2CPP_EXTERN_C String_t* _stringLiteral41D96974DD8D9233DD07BA405DF8A3018998A84C;
IL2CPP_EXTERN_C String_t* _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C;
IL2CPP_EXTERN_C String_t* _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8;
IL2CPP_EXTERN_C String_t* _stringLiteral4EA2948BD4FABD44A63F69C545E6C11D9746FFC5;
IL2CPP_EXTERN_C String_t* _stringLiteral51C6279E31F7483126B79E3000116001A915B690;
IL2CPP_EXTERN_C String_t* _stringLiteral800887583098E85FC30F11258ADBB87A8CDBED7E;
IL2CPP_EXTERN_C String_t* _stringLiteral8192EF02E079142FD5CA69DC024E6EF19381C3B6;
IL2CPP_EXTERN_C String_t* _stringLiteralA15CF7E1CEFBD0C475E3A89A80B5393D417F8634;
IL2CPP_EXTERN_C String_t* _stringLiteralBCA7DDD073AD5DB21CC612ADB1833BF1A5D32261;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Count_m41D28F48221F99DB3016C0F5CF4B210291338546_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Any_TisFlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_m409E7D2E1AE678244328F78B52C2BAF3BF8E3B6E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Any_TisNode_tD0AFD8B27090B24E967D338520DEF077662A61DF_m36DE5710832D7F4C686483656AB39178B531F9FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mEEDE6D8C098C939874334B6A2448D278272F0556_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mC2EA6ED235C550D981F43C570F13379D5BE1CB1E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m96C2ED12F1680F282C4B654DB9B5EC1AFD99A142_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m04B7DEF1A5A31A1BEBF83465FB5BBCECE52C7CAE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_mC17802B326CCF03C5CDAAC9B94533C3E899F3DDE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m5207CA3F0B39504FFE8EA0E45B2C9C2D6294ABDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mCF93E3DFA395F0F2B4EA20436FE03EF4CFC46FB9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m5FB38C219330031C0EB797E49A904F84A284F2D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m62F1417B988555AB608ECA0C7DE6B76E2D7960AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m7981AB576A967AF513478AA362175476B4DA4CF9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mE83854FC821DCA2E3637C6AE7D6EC5222F587D88_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_IndexOf_m7CEA1AFEC4C2ECD458BDA3E4B1DC9610C9F23499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m3A4E207AFD5D554933D682752DE86150935C5565_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m6F74D1B3AF6AFC69F3E9C25B39D635EFD5100162_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m5C686920C2BB16A365651B4FD76DABA8F6AD82AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB894CE9F78F9B3D56F6EC500CB45DDA05137AF52_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m5609620CF6DC82315623CF4C9227475D860679E2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m8AB2C9BDA3A353EEA9DB64D20F5FFD876ACAB875_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Utility_GetCustomAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_mAF061D40815EFB9D8390D2732BCB25B7F5E0E036_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Utility_HasAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_m7F7D8E5E5D8629A2804D0E49B47CD218BA869FEB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Utility_HasAttribute_TisOutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_mEC474F5773AA45736B7EC27ECC883D70E91271C0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Enum_t23B90B40F60E677A8025267341651C94AE079CDA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* IList_tB15A9D6625D09661D6E47976BB626C703EC81910_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* List_1_t2F377D93C74B8090B226DCC307AB5BB600181453_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* String_t_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655;
struct FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E;
struct FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC;
struct NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t5F8794C2E7B8B35AE780253B97016DCDCF4F5206 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tDCA1A62E50C5B5A40FD6F44107088AF42F5671D2* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t0043475CBB02FD67894529F3CAA818080A2F7A17 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tB942A1033B750DCF04FE948413982D120FC69A4E * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___entries_1)); }
	inline EntryU5BU5D_tDCA1A62E50C5B5A40FD6F44107088AF42F5671D2* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tDCA1A62E50C5B5A40FD6F44107088AF42F5671D2** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tDCA1A62E50C5B5A40FD6F44107088AF42F5671D2* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___keys_7)); }
	inline KeyCollection_t0043475CBB02FD67894529F3CAA818080A2F7A17 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t0043475CBB02FD67894529F3CAA818080A2F7A17 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t0043475CBB02FD67894529F3CAA818080A2F7A17 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ___values_8)); }
	inline ValueCollection_tB942A1033B750DCF04FE948413982D120FC69A4E * get_values_8() const { return ___values_8; }
	inline ValueCollection_tB942A1033B750DCF04FE948413982D120FC69A4E ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tB942A1033B750DCF04FE948413982D120FC69A4E * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<DevionGames.Graphs.Edge>
struct List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55, ____items_1)); }
	inline EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* get__items_1() const { return ____items_1; }
	inline EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_StaticFields, ____emptyArray_5)); }
	inline EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(EdgeU5BU5D_t5ED568A6275A483B358A0AD464078CA595803F6B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<DevionGames.Graphs.Node>
struct List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB, ____items_1)); }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* get__items_1() const { return ____items_1; }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB_StaticFields, ____emptyArray_5)); }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t9D216521E6A213FF8562D215598D336ABB5474F4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4, ____items_1)); }
	inline ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<DevionGames.Graphs.Port>
struct List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD, ____items_1)); }
	inline PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* get__items_1() const { return ____items_1; }
	inline PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD_StaticFields, ____emptyArray_5)); }
	inline PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* get__emptyArray_5() const { return ____emptyArray_5; }
	inline PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(PortU5BU5D_t1DF05F37007F9715DAC47F18B2FE52A1F58D5505* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// DevionGames.Graphs.Edge
struct Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81  : public RuntimeObject
{
public:
	// System.String DevionGames.Graphs.Edge::nodeId
	String_t* ___nodeId_0;
	// System.String DevionGames.Graphs.Edge::fieldName
	String_t* ___fieldName_1;
	// DevionGames.Graphs.Port DevionGames.Graphs.Edge::port
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port_2;

public:
	inline static int32_t get_offset_of_nodeId_0() { return static_cast<int32_t>(offsetof(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81, ___nodeId_0)); }
	inline String_t* get_nodeId_0() const { return ___nodeId_0; }
	inline String_t** get_address_of_nodeId_0() { return &___nodeId_0; }
	inline void set_nodeId_0(String_t* value)
	{
		___nodeId_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodeId_0), (void*)value);
	}

	inline static int32_t get_offset_of_fieldName_1() { return static_cast<int32_t>(offsetof(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81, ___fieldName_1)); }
	inline String_t* get_fieldName_1() const { return ___fieldName_1; }
	inline String_t** get_address_of_fieldName_1() { return &___fieldName_1; }
	inline void set_fieldName_1(String_t* value)
	{
		___fieldName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldName_1), (void*)value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81, ___port_2)); }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * get_port_2() const { return ___port_2; }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 ** get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * value)
	{
		___port_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___port_2), (void*)value);
	}
};


// DevionGames.Graphs.Graph
struct Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9  : public RuntimeObject
{
public:
	// System.String DevionGames.Graphs.Graph::serializationData
	String_t* ___serializationData_0;
	// System.Collections.Generic.List`1<UnityEngine.Object> DevionGames.Graphs.Graph::serializedObjects
	List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___serializedObjects_1;
	// System.Collections.Generic.List`1<DevionGames.Graphs.Node> DevionGames.Graphs.Graph::nodes
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * ___nodes_2;

public:
	inline static int32_t get_offset_of_serializationData_0() { return static_cast<int32_t>(offsetof(Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9, ___serializationData_0)); }
	inline String_t* get_serializationData_0() const { return ___serializationData_0; }
	inline String_t** get_address_of_serializationData_0() { return &___serializationData_0; }
	inline void set_serializationData_0(String_t* value)
	{
		___serializationData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializationData_0), (void*)value);
	}

	inline static int32_t get_offset_of_serializedObjects_1() { return static_cast<int32_t>(offsetof(Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9, ___serializedObjects_1)); }
	inline List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * get_serializedObjects_1() const { return ___serializedObjects_1; }
	inline List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** get_address_of_serializedObjects_1() { return &___serializedObjects_1; }
	inline void set_serializedObjects_1(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * value)
	{
		___serializedObjects_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serializedObjects_1), (void*)value);
	}

	inline static int32_t get_offset_of_nodes_2() { return static_cast<int32_t>(offsetof(Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9, ___nodes_2)); }
	inline List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * get_nodes_2() const { return ___nodes_2; }
	inline List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB ** get_address_of_nodes_2() { return &___nodes_2; }
	inline void set_nodes_2(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * value)
	{
		___nodes_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_2), (void*)value);
	}
};


// DevionGames.Graphs.GraphUtility
struct GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DevionGames.Graphs.FlowNode/<>c
struct U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields
{
public:
	// DevionGames.Graphs.FlowNode/<>c DevionGames.Graphs.FlowNode/<>c::<>9
	U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * ___U3CU3E9_0;
	// System.Func`2<DevionGames.Graphs.Port,System.Boolean> DevionGames.Graphs.FlowNode/<>c::<>9__4_0
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * ___U3CU3E9__4_0_1;
	// System.Func`2<DevionGames.Graphs.Port,System.Boolean> DevionGames.Graphs.FlowNode/<>c::<>9__6_0
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * ___U3CU3E9__6_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields, ___U3CU3E9__6_0_2)); }
	inline Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * get_U3CU3E9__6_0_2() const { return ___U3CU3E9__6_0_2; }
	inline Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC ** get_address_of_U3CU3E9__6_0_2() { return &___U3CU3E9__6_0_2; }
	inline void set_U3CU3E9__6_0_2(Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * value)
	{
		___U3CU3E9__6_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_2), (void*)value);
	}
};


// DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40  : public RuntimeObject
{
public:
	// System.String DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::fieldName
	String_t* ___fieldName_0;

public:
	inline static int32_t get_offset_of_fieldName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40, ___fieldName_0)); }
	inline String_t* get_fieldName_0() const { return ___fieldName_0; }
	inline String_t** get_address_of_fieldName_0() { return &___fieldName_0; }
	inline void set_fieldName_0(String_t* value)
	{
		___fieldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldName_0), (void*)value);
	}
};


// DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680  : public RuntimeObject
{
public:
	// DevionGames.Graphs.Edge DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::edge
	Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___edge_0;

public:
	inline static int32_t get_offset_of_edge_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680, ___edge_0)); }
	inline Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * get_edge_0() const { return ___edge_0; }
	inline Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 ** get_address_of_edge_0() { return &___edge_0; }
	inline void set_edge_0(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * value)
	{
		___edge_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edge_0), (void*)value);
	}
};


// DevionGames.Graphs.Formula/<>c
struct U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields
{
public:
	// DevionGames.Graphs.Formula/<>c DevionGames.Graphs.Formula/<>c::<>9
	U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * ___U3CU3E9_0;
	// System.Predicate`1<DevionGames.Graphs.Node> DevionGames.Graphs.Formula/<>c::<>9__2_0
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_0_1), (void*)value);
	}
};


// DevionGames.Graphs.FormulaGraph/<>c
struct U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields
{
public:
	// DevionGames.Graphs.FormulaGraph/<>c DevionGames.Graphs.FormulaGraph/<>c::<>9
	U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * ___U3CU3E9_0;
	// System.Predicate`1<DevionGames.Graphs.Node> DevionGames.Graphs.FormulaGraph/<>c::<>9__1_0
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * ___U3CU3E9__1_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__1_0_1), (void*)value);
	}
};


// DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509  : public RuntimeObject
{
public:
	// DevionGames.Graphs.FlowNode[] DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::nodes
	FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* ___nodes_0;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509, ___nodes_0)); }
	inline FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* get_nodes_0() const { return ___nodes_0; }
	inline FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_0), (void*)value);
	}
};


// DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1
struct U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7  : public RuntimeObject
{
public:
	// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::x
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7, ___x_0)); }
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * get_x_0() const { return ___x_0; }
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___x_0), (void*)value);
	}
};


// DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D  : public RuntimeObject
{
public:
	// DevionGames.Graphs.Node[] DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::nodes
	NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* ___nodes_0;

public:
	inline static int32_t get_offset_of_nodes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D, ___nodes_0)); }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* get_nodes_0() const { return ___nodes_0; }
	inline NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49** get_address_of_nodes_0() { return &___nodes_0; }
	inline void set_nodes_0(NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* value)
	{
		___nodes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nodes_0), (void*)value);
	}
};


// DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1
struct U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0  : public RuntimeObject
{
public:
	// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::x
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x_0;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0, ___x_0)); }
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * get_x_0() const { return ___x_0; }
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF ** get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * value)
	{
		___x_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___x_0), (void*)value);
	}
};


// DevionGames.Graphs.Port/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8  : public RuntimeObject
{
public:
	// DevionGames.Graphs.Port DevionGames.Graphs.Port/<>c__DisplayClass18_0::port
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port_0;
	// DevionGames.Graphs.Port DevionGames.Graphs.Port/<>c__DisplayClass18_0::<>4__this
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_port_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8, ___port_0)); }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * get_port_0() const { return ___port_0; }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 ** get_address_of_port_0() { return &___port_0; }
	inline void set_port_0(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * value)
	{
		___port_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___port_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8, ___U3CU3E4__this_1)); }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<DevionGames.Graphs.Port>
struct Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72, ___list_0)); }
	inline List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * get_list_0() const { return ___list_0; }
	inline List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72, ___current_3)); }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * get_current_3() const { return ___current_3; }
	inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Reflection.FieldInfo
struct FieldInfo_t  : public MemberInfo_t
{
public:

public:
};


// DevionGames.Graphs.FlowGraph
struct FlowGraph_t9A6068907F5E8BFFA7E6376D59860B7DBC8577E0  : public Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9
{
public:

public:
};


// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean DevionGames.Graphs.InputAttribute::label
	bool ___label_0;
	// System.Boolean DevionGames.Graphs.InputAttribute::port
	bool ___port_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___label_0)); }
	inline bool get_label_0() const { return ___label_0; }
	inline bool* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(bool value)
	{
		___label_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___port_1)); }
	inline bool get_port_1() const { return ___port_1; }
	inline bool* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(bool value)
	{
		___port_1 = value;
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// DevionGames.Graphs.NodeStyleAttribute
struct NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.Graphs.NodeStyleAttribute::iconPath
	String_t* ___iconPath_0;
	// System.Boolean DevionGames.Graphs.NodeStyleAttribute::displayHeader
	bool ___displayHeader_1;
	// System.String DevionGames.Graphs.NodeStyleAttribute::category
	String_t* ___category_2;

public:
	inline static int32_t get_offset_of_iconPath_0() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___iconPath_0)); }
	inline String_t* get_iconPath_0() const { return ___iconPath_0; }
	inline String_t** get_address_of_iconPath_0() { return &___iconPath_0; }
	inline void set_iconPath_0(String_t* value)
	{
		___iconPath_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iconPath_0), (void*)value);
	}

	inline static int32_t get_offset_of_displayHeader_1() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___displayHeader_1)); }
	inline bool get_displayHeader_1() const { return ___displayHeader_1; }
	inline bool* get_address_of_displayHeader_1() { return &___displayHeader_1; }
	inline void set_displayHeader_1(bool value)
	{
		___displayHeader_1 = value;
	}

	inline static int32_t get_offset_of_category_2() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___category_2)); }
	inline String_t* get_category_2() const { return ___category_2; }
	inline String_t** get_address_of_category_2() { return &___category_2; }
	inline void set_category_2(String_t* value)
	{
		___category_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___category_2), (void*)value);
	}
};


// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// DevionGames.Graphs.FormulaGraph
struct FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15  : public FlowGraph_t9A6068907F5E8BFFA7E6376D59860B7DBC8577E0
{
public:

public:
};


// DevionGames.Graphs.Node
struct Node_tD0AFD8B27090B24E967D338520DEF077662A61DF  : public RuntimeObject
{
public:
	// System.String DevionGames.Graphs.Node::id
	String_t* ___id_0;
	// System.String DevionGames.Graphs.Node::name
	String_t* ___name_1;
	// UnityEngine.Vector2 DevionGames.Graphs.Node::position
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position_2;
	// DevionGames.Graphs.Graph DevionGames.Graphs.Node::graph
	Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_0), (void*)value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_1), (void*)value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF, ___position_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_position_2() const { return ___position_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_graph_3() { return static_cast<int32_t>(offsetof(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF, ___graph_3)); }
	inline Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * get_graph_3() const { return ___graph_3; }
	inline Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 ** get_address_of_graph_3() { return &___graph_3; }
	inline void set_graph_3(Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * value)
	{
		___graph_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graph_3), (void*)value);
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DevionGames.Graphs.PortCapacity
struct PortCapacity_tAEBF790AF56C20BDCD909913C0749DEB24C84E5E 
{
public:
	// System.Int32 DevionGames.Graphs.PortCapacity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PortCapacity_tAEBF790AF56C20BDCD909913C0749DEB24C84E5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.Graphs.PortDirection
struct PortDirection_tF19D420196B5D3DABD35D16B89E2C6E60EE61275 
{
public:
	// System.Int32 DevionGames.Graphs.PortDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PortDirection_tF19D420196B5D3DABD35D16B89E2C6E60EE61275, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DevionGames.Graphs.FlowNode
struct FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195  : public Node_tD0AFD8B27090B24E967D338520DEF077662A61DF
{
public:
	// System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::m_Ports
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * ___m_Ports_4;

public:
	inline static int32_t get_offset_of_m_Ports_4() { return static_cast<int32_t>(offsetof(FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195, ___m_Ports_4)); }
	inline List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * get_m_Ports_4() const { return ___m_Ports_4; }
	inline List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD ** get_address_of_m_Ports_4() { return &___m_Ports_4; }
	inline void set_m_Ports_4(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * value)
	{
		___m_Ports_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Ports_4), (void*)value);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// DevionGames.Graphs.Port
struct Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5  : public RuntimeObject
{
public:
	// DevionGames.Graphs.FlowNode DevionGames.Graphs.Port::node
	FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___node_0;
	// System.Type DevionGames.Graphs.Port::m_FieldType
	Type_t * ___m_FieldType_1;
	// System.String DevionGames.Graphs.Port::fieldName
	String_t* ___fieldName_2;
	// System.Boolean DevionGames.Graphs.Port::drawPort
	bool ___drawPort_3;
	// System.Boolean DevionGames.Graphs.Port::label
	bool ___label_4;
	// DevionGames.Graphs.PortCapacity DevionGames.Graphs.Port::capacity
	int32_t ___capacity_5;
	// DevionGames.Graphs.PortDirection DevionGames.Graphs.Port::direction
	int32_t ___direction_6;
	// System.Collections.Generic.List`1<DevionGames.Graphs.Edge> DevionGames.Graphs.Port::m_Connections
	List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * ___m_Connections_7;
	// System.String DevionGames.Graphs.Port::m_FieldTypeName
	String_t* ___m_FieldTypeName_8;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___node_0)); }
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * get_node_0() const { return ___node_0; }
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___node_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_FieldType_1() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___m_FieldType_1)); }
	inline Type_t * get_m_FieldType_1() const { return ___m_FieldType_1; }
	inline Type_t ** get_address_of_m_FieldType_1() { return &___m_FieldType_1; }
	inline void set_m_FieldType_1(Type_t * value)
	{
		___m_FieldType_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FieldType_1), (void*)value);
	}

	inline static int32_t get_offset_of_fieldName_2() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___fieldName_2)); }
	inline String_t* get_fieldName_2() const { return ___fieldName_2; }
	inline String_t** get_address_of_fieldName_2() { return &___fieldName_2; }
	inline void set_fieldName_2(String_t* value)
	{
		___fieldName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldName_2), (void*)value);
	}

	inline static int32_t get_offset_of_drawPort_3() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___drawPort_3)); }
	inline bool get_drawPort_3() const { return ___drawPort_3; }
	inline bool* get_address_of_drawPort_3() { return &___drawPort_3; }
	inline void set_drawPort_3(bool value)
	{
		___drawPort_3 = value;
	}

	inline static int32_t get_offset_of_label_4() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___label_4)); }
	inline bool get_label_4() const { return ___label_4; }
	inline bool* get_address_of_label_4() { return &___label_4; }
	inline void set_label_4(bool value)
	{
		___label_4 = value;
	}

	inline static int32_t get_offset_of_capacity_5() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___capacity_5)); }
	inline int32_t get_capacity_5() const { return ___capacity_5; }
	inline int32_t* get_address_of_capacity_5() { return &___capacity_5; }
	inline void set_capacity_5(int32_t value)
	{
		___capacity_5 = value;
	}

	inline static int32_t get_offset_of_direction_6() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___direction_6)); }
	inline int32_t get_direction_6() const { return ___direction_6; }
	inline int32_t* get_address_of_direction_6() { return &___direction_6; }
	inline void set_direction_6(int32_t value)
	{
		___direction_6 = value;
	}

	inline static int32_t get_offset_of_m_Connections_7() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___m_Connections_7)); }
	inline List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * get_m_Connections_7() const { return ___m_Connections_7; }
	inline List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 ** get_address_of_m_Connections_7() { return &___m_Connections_7; }
	inline void set_m_Connections_7(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * value)
	{
		___m_Connections_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Connections_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_FieldTypeName_8() { return static_cast<int32_t>(offsetof(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5, ___m_FieldTypeName_8)); }
	inline String_t* get_m_FieldTypeName_8() const { return ___m_FieldTypeName_8; }
	inline String_t** get_address_of_m_FieldTypeName_8() { return &___m_FieldTypeName_8; }
	inline void set_m_FieldTypeName_8(String_t* value)
	{
		___m_FieldTypeName_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FieldTypeName_8), (void*)value);
	}
};


// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Func`2<DevionGames.Graphs.FlowNode,System.Boolean>
struct Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<DevionGames.Graphs.Node,System.Boolean>
struct Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<DevionGames.Graphs.Port,System.Boolean>
struct Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<DevionGames.Graphs.Edge>
struct Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<DevionGames.Graphs.Node>
struct Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<DevionGames.Graphs.Port>
struct Predicate_1_t37A5DE42085D332C641B55950592659162DD2774  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.Graphs.Add
struct Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Add::a
	float ___a_5;
	// System.Single DevionGames.Graphs.Add::b
	float ___b_6;
	// System.Single DevionGames.Graphs.Add::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4, ___b_6)); }
	inline float get_b_6() const { return ___b_6; }
	inline float* get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(float value)
	{
		___b_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.Ceil
struct Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Ceil::value
	float ___value_5;
	// System.Single DevionGames.Graphs.Ceil::output
	float ___output_6;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}

	inline static int32_t get_offset_of_output_6() { return static_cast<int32_t>(offsetof(Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323, ___output_6)); }
	inline float get_output_6() const { return ___output_6; }
	inline float* get_address_of_output_6() { return &___output_6; }
	inline void set_output_6(float value)
	{
		___output_6 = value;
	}
};


// DevionGames.Graphs.Divide
struct Divide_t0E2802337E30A27C1C488271C61C2214772281FF  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Divide::a
	float ___a_5;
	// System.Single DevionGames.Graphs.Divide::b
	float ___b_6;
	// System.Single DevionGames.Graphs.Divide::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Divide_t0E2802337E30A27C1C488271C61C2214772281FF, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Divide_t0E2802337E30A27C1C488271C61C2214772281FF, ___b_6)); }
	inline float get_b_6() const { return ___b_6; }
	inline float* get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(float value)
	{
		___b_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Divide_t0E2802337E30A27C1C488271C61C2214772281FF, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.Evaluate
struct Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Evaluate::time
	float ___time_5;
	// UnityEngine.AnimationCurve DevionGames.Graphs.Evaluate::curve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___curve_6;
	// System.Single DevionGames.Graphs.Evaluate::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}

	inline static int32_t get_offset_of_curve_6() { return static_cast<int32_t>(offsetof(Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39, ___curve_6)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_curve_6() const { return ___curve_6; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_curve_6() { return &___curve_6; }
	inline void set_curve_6(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___curve_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___curve_6), (void*)value);
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.EventNode
struct EventNode_t6C7BF4F88C4264940AED17FE36C8E8A0E398AD8F  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:

public:
};


// DevionGames.Graphs.Floor
struct Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Floor::value
	float ___value_5;
	// System.Single DevionGames.Graphs.Floor::output
	float ___output_6;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}

	inline static int32_t get_offset_of_output_6() { return static_cast<int32_t>(offsetof(Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9, ___output_6)); }
	inline float get_output_6() const { return ___output_6; }
	inline float* get_address_of_output_6() { return &___output_6; }
	inline void set_output_6(float value)
	{
		___output_6 = value;
	}
};


// DevionGames.Graphs.Formula
struct Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// DevionGames.Graphs.FormulaGraph DevionGames.Graphs.Formula::m_Graph
	FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * ___m_Graph_4;

public:
	inline static int32_t get_offset_of_m_Graph_4() { return static_cast<int32_t>(offsetof(Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA, ___m_Graph_4)); }
	inline FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * get_m_Graph_4() const { return ___m_Graph_4; }
	inline FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 ** get_address_of_m_Graph_4() { return &___m_Graph_4; }
	inline void set_m_Graph_4(FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * value)
	{
		___m_Graph_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Graph_4), (void*)value);
	}
};


// DevionGames.Graphs.Multiply
struct Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Multiply::a
	float ___a_5;
	// System.Single DevionGames.Graphs.Multiply::b
	float ___b_6;
	// System.Single DevionGames.Graphs.Multiply::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F, ___b_6)); }
	inline float get_b_6() const { return ___b_6; }
	inline float* get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(float value)
	{
		___b_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// DevionGames.Graphs.Pow
struct Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Pow::f
	float ___f_5;
	// System.Single DevionGames.Graphs.Pow::p
	float ___p_6;
	// System.Single DevionGames.Graphs.Pow::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_f_5() { return static_cast<int32_t>(offsetof(Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC, ___f_5)); }
	inline float get_f_5() const { return ___f_5; }
	inline float* get_address_of_f_5() { return &___f_5; }
	inline void set_f_5(float value)
	{
		___f_5 = value;
	}

	inline static int32_t get_offset_of_p_6() { return static_cast<int32_t>(offsetof(Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC, ___p_6)); }
	inline float get_p_6() const { return ___p_6; }
	inline float* get_address_of_p_6() { return &___p_6; }
	inline void set_p_6(float value)
	{
		___p_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.Random
struct Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Random::a
	float ___a_5;
	// System.Single DevionGames.Graphs.Random::b
	float ___b_6;
	// System.Single DevionGames.Graphs.Random::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7, ___b_6)); }
	inline float get_b_6() const { return ___b_6; }
	inline float* get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(float value)
	{
		___b_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.Round
struct Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Round::value
	float ___value_5;
	// System.Single DevionGames.Graphs.Round::output
	float ___output_6;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}

	inline static int32_t get_offset_of_output_6() { return static_cast<int32_t>(offsetof(Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C, ___output_6)); }
	inline float get_output_6() const { return ___output_6; }
	inline float* get_address_of_output_6() { return &___output_6; }
	inline void set_output_6(float value)
	{
		___output_6 = value;
	}
};


// DevionGames.Graphs.Sqrt
struct Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Sqrt::value
	float ___value_5;
	// System.Single DevionGames.Graphs.Sqrt::output
	float ___output_6;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}

	inline static int32_t get_offset_of_output_6() { return static_cast<int32_t>(offsetof(Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF, ___output_6)); }
	inline float get_output_6() const { return ___output_6; }
	inline float* get_address_of_output_6() { return &___output_6; }
	inline void set_output_6(float value)
	{
		___output_6 = value;
	}
};


// DevionGames.Graphs.Subtract
struct Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71  : public FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195
{
public:
	// System.Single DevionGames.Graphs.Subtract::a
	float ___a_5;
	// System.Single DevionGames.Graphs.Subtract::b
	float ___b_6;
	// System.Single DevionGames.Graphs.Subtract::output
	float ___output_7;

public:
	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_b_6() { return static_cast<int32_t>(offsetof(Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71, ___b_6)); }
	inline float get_b_6() const { return ___b_6; }
	inline float* get_address_of_b_6() { return &___b_6; }
	inline void set_b_6(float value)
	{
		___b_6 = value;
	}

	inline static int32_t get_offset_of_output_7() { return static_cast<int32_t>(offsetof(Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71, ___output_7)); }
	inline float get_output_7() const { return ___output_7; }
	inline float* get_address_of_output_7() { return &___output_7; }
	inline void set_output_7(float value)
	{
		___output_7 = value;
	}
};


// DevionGames.Graphs.FormulaOutput
struct FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5  : public EventNode_t6C7BF4F88C4264940AED17FE36C8E8A0E398AD8F
{
public:
	// System.Single DevionGames.Graphs.FormulaOutput::result
	float ___result_5;

public:
	inline static int32_t get_offset_of_result_5() { return static_cast<int32_t>(offsetof(FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5, ___result_5)); }
	inline float get_result_5() const { return ___result_5; }
	inline float* get_address_of_result_5() { return &___result_5; }
	inline void set_result_5(float value)
	{
		___result_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// DevionGames.Graphs.FlowNode[]
struct FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * m_Items[1];

public:
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DevionGames.Graphs.Node[]
struct NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * m_Items[1];

public:
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FieldInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Collections.Generic.Dictionary`2<System.String,System.Object>[]
struct Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * m_Items[1];

public:
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T DevionGames.Graphs.FlowNode::GetInputValue<System.Single>(System.String,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_gshared (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, String_t* ___portName0, float ___defaultValue1, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared (Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * List_1_Find_mBE8A91B51D29EC296321E6070FCD76081576B31E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___match0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m73674D291C1D6030C21A39003E4743D110ACC6A2_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_RemoveAll_m3CA48D06F70CC498ADA7410EB930969256F43675_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___match0, const RuntimeMethod* method);
// System.Boolean DevionGames.Utility::HasAttribute<System.Object>(System.Reflection.MemberInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Utility_HasAttribute_TisRuntimeObject_mC0CC16EF1CA0F5F2220AC0FDA480652064A36A1C_gshared (MemberInfo_t * ___memberInfo0, const RuntimeMethod* method);
// !!0 DevionGames.Utility::GetCustomAttribute<System.Object>(System.Reflection.MemberInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Utility_GetCustomAttribute_TisRuntimeObject_m51624DD3D8D2BC8552BB3F1E0C8E6B9B9D0BE91D_gshared (MemberInfo_t * ___memberInfo0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_IndexOf_m4474628ACB239463EEF6742E298897E550641533_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m048C13E0F44BDC16F7CF01D14E918A84EE72C62C_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m1B599EE742A00E8D399B43E225AD4C6571FBC8DA_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerable_Any_TisRuntimeObject_m79E5BEA29E663B44BB9C1BDD62286D214D22E600_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);

// T DevionGames.Graphs.FlowNode::GetInputValue<System.Single>(System.String,T)
inline float FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, String_t* ___portName0, float ___defaultValue1, const RuntimeMethod* method)
{
	return ((  float (*) (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 *, String_t*, float, const RuntimeMethod*))FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_gshared)(__this, ___portName0, ___defaultValue1, method);
}
// System.Void DevionGames.Graphs.FlowNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.Graph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graph__ctor_mF13B8DA9134092BC46305E78DF7A4631353C0AFD (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<DevionGames.Graphs.Port,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3 (Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<DevionGames.Graphs.Port>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8 (RuntimeObject* ___source0, Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * ___predicate1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared)(___source0, ___predicate1, method);
}
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<DevionGames.Graphs.Port>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared)(___source0, method);
}
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Port>::.ctor()
inline void List_1__ctor_m6F74D1B3AF6AFC69F3E9C25B39D635EFD5100162 (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void DevionGames.Graphs.Node::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Node__ctor_m607E8DB7A61022982F71AE46459C355F082BA030 (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_0__ctor_mA1FED964843DC56317790239CA60118DE619C3E1 (U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<DevionGames.Graphs.Port>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1 (Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared)(__this, ___object0, ___method1, method);
}
// !0 System.Collections.Generic.List`1<DevionGames.Graphs.Port>::Find(System.Predicate`1<!0>)
inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1 (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 * ___match0, const RuntimeMethod* method)
{
	return ((  Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 *, const RuntimeMethod*))List_1_Find_mBE8A91B51D29EC296321E6070FCD76081576B31E_gshared)(__this, ___match0, method);
}
// !0 System.Collections.Generic.List`1<DevionGames.Graphs.Port>::get_Item(System.Int32)
inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Port>::Add(!0)
inline void List_1_Add_m5207CA3F0B39504FFE8EA0E45B2C9C2D6294ABDA (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<DevionGames.Graphs.Port>::GetEnumerator()
inline Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72  List_1_GetEnumerator_mE83854FC821DCA2E3637C6AE7D6EC5222F587D88 (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72  (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<DevionGames.Graphs.Port>::get_Current()
inline Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * Enumerator_get_Current_m96C2ED12F1680F282C4B654DB9B5EC1AFD99A142_inline (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 * __this, const RuntimeMethod* method)
{
	return ((  Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * (*) (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void DevionGames.Graphs.Port::DisconnectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port_DisconnectAll_m9A9345796CFB57DFE8DAE23B538E0D30E23886F0 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<DevionGames.Graphs.Port>::MoveNext()
inline bool Enumerator_MoveNext_mC2EA6ED235C550D981F43C570F13379D5BE1CB1E (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<DevionGames.Graphs.Port>::Dispose()
inline void Enumerator_Dispose_mEEDE6D8C098C939874334B6A2448D278272F0556 (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass15_0__ctor_m676586F14260D951119E7BF393C9CFF81BFAC368 (U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<DevionGames.Graphs.Edge> DevionGames.Graphs.Port::get_Connections()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::get_Item(System.Int32)
inline Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_inline (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Predicate`1<DevionGames.Graphs.Node>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9 (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared)(__this, ___object0, ___method1, method);
}
// !0 System.Collections.Generic.List`1<DevionGames.Graphs.Node>::Find(System.Predicate`1<!0>)
inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6 (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * ___match0, const RuntimeMethod* method)
{
	return ((  Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *, const RuntimeMethod*))List_1_Find_mBE8A91B51D29EC296321E6070FCD76081576B31E_gshared)(__this, ___match0, method);
}
// System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_Ports()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * FlowNode_get_Ports_m79D5C805ABCF819440DA1A96B204CDD1D1ED203C (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m8AB2C9BDA3A353EEA9DB64D20F5FFD876ACAB875 (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, int32_t ___index0, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, int32_t, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 *, const RuntimeMethod*))List_1_set_Item_m73674D291C1D6030C21A39003E4743D110ACC6A2_gshared)(__this, ___index0, ___value1, method);
}
// System.Int32 System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::get_Count()
inline int32_t List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_inline (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<DevionGames.Graphs.Port>::get_Count()
inline int32_t List_1_get_Count_m5C686920C2BB16A365651B4FD76DABA8F6AD82AB_inline (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// DevionGames.Graphs.Graph DevionGames.Graphs.Formula::GetGraph()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * Formula_GetGraph_m46FB2CCDE30D85D1770364AA7E7DABDDAA452382 (Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063 (ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowGraph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowGraph__ctor_mC5D4BA1D3523EE75C16FF16899853513F3416DF8 (FlowGraph_t9A6068907F5E8BFFA7E6376D59860B7DBC8577E0 * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::AddNode(DevionGames.Graphs.Graph,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GraphUtility_AddNode_m3D1B9E811B1DD86FE2785ABC80DC371EF2AA237F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, Type_t * ___type1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.EventNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventNode__ctor_mA5F5325FE2F3816BAC25F88D8BB33B2BFEA6B7AF (EventNode_t6C7BF4F88C4264940AED17FE36C8E8A0E398AD8F * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility::Load(DevionGames.Graphs.Graph)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_Load_m4C2361BA90120FD465B90AC31F423B405C220B9F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Object>::.ctor()
inline void List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4 (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Node>::.ctor()
inline void List_1__ctor_m3A4E207AFD5D554933D682752DE86150935C5565 (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Object System.Activator::CreateInstance(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Activator_CreateInstance_m1BACAB5F4FBF138CCCB537DDCB0683A2AC064295 (Type_t * ___type0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility::CreatePorts(DevionGames.Graphs.FlowNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_CreatePorts_m5D6C07B357EBCE3B7C322F94E1AC9696D7E5960B (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___node0, const RuntimeMethod* method);
// System.String DevionGames.Graphs.GraphUtility::NicifyVariableName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GraphUtility_NicifyVariableName_m9037A019C3386722DBA5ED850159659A2C4CDD1A (String_t* ___name0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Node>::Add(!0)
inline void List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void DevionGames.Graphs.GraphUtility::Save(DevionGames.Graphs.Graph)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean System.Char::IsUpper(System.Char)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Char_IsUpper_m72EAD892A02AD10D2050EA09BF3735DDE6921892 (Il2CppChar ___c0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.String System.Char::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8 (Il2CppChar* __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mF3C6BAD036536D3343CAD9AAD7F1D00950AD5FBE (U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowNode::DisconnectAllPorts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode_DisconnectAllPorts_m8FA2AC33D4D8E4D4074B258674528E9A596FF306 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<DevionGames.Graphs.Node>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * ___match0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *, const RuntimeMethod*))List_1_RemoveAll_m3CA48D06F70CC498ADA7410EB930969256F43675_gshared)(__this, ___match0, method);
}
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m6F79A73FE4250F82E7C386C679CA9A8EFE39B4EF (U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * __this, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean DevionGames.Utility::HasAttribute<DevionGames.Graphs.InputAttribute>(System.Reflection.MemberInfo)
inline bool Utility_HasAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_m7F7D8E5E5D8629A2804D0E49B47CD218BA869FEB (MemberInfo_t * ___memberInfo0, const RuntimeMethod* method)
{
	return ((  bool (*) (MemberInfo_t *, const RuntimeMethod*))Utility_HasAttribute_TisRuntimeObject_mC0CC16EF1CA0F5F2220AC0FDA480652064A36A1C_gshared)(___memberInfo0, method);
}
// !!0 DevionGames.Utility::GetCustomAttribute<DevionGames.Graphs.InputAttribute>(System.Reflection.MemberInfo)
inline InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * Utility_GetCustomAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_mAF061D40815EFB9D8390D2732BCB25B7F5E0E036 (MemberInfo_t * ___memberInfo0, const RuntimeMethod* method)
{
	return ((  InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * (*) (MemberInfo_t *, const RuntimeMethod*))Utility_GetCustomAttribute_TisRuntimeObject_m51624DD3D8D2BC8552BB3F1E0C8E6B9B9D0BE91D_gshared)(___memberInfo0, method);
}
// System.Void DevionGames.Graphs.Port::.ctor(DevionGames.Graphs.FlowNode,System.String,System.Type,DevionGames.Graphs.PortCapacity,DevionGames.Graphs.PortDirection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___node0, String_t* ___fieldName1, Type_t * ___fieldType2, int32_t ___capacity3, int32_t ___direction4, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowNode::AddPort(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method);
// System.Boolean DevionGames.Utility::HasAttribute<DevionGames.Graphs.OutputAttribute>(System.Reflection.MemberInfo)
inline bool Utility_HasAttribute_TisOutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_mEC474F5773AA45736B7EC27ECC883D70E91271C0 (MemberInfo_t * ___memberInfo0, const RuntimeMethod* method)
{
	return ((  bool (*) (MemberInfo_t *, const RuntimeMethod*))Utility_HasAttribute_TisRuntimeObject_mC0CC16EF1CA0F5F2220AC0FDA480652064A36A1C_gshared)(___memberInfo0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<DevionGames.Graphs.Node>::get_Count()
inline int32_t List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_inline (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<DevionGames.Graphs.Node>::get_Item(System.Int32)
inline Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_inline (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> DevionGames.Graphs.GraphUtility::SerializeNode(DevionGames.Graphs.Node,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * GraphUtility_SerializeNode_m28E8EB533906E6BD25710874F0EA9D1DA56327C0 (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___node0, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1)
inline void Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, String_t* ___key0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared)(__this, ___key0, ___value1, method);
}
// System.String DevionGames.MiniJSON::Serialize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* MiniJSON_Serialize_m0EAC236F1654F6BA91D0955E75321F084697656D (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility::SerializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC (RuntimeObject * ___obj0, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** ___dic1, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences2, const RuntimeMethod* method);
// System.Reflection.FieldInfo[] DevionGames.Utility::GetAllSerializedFields(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48 (Type_t * ___type0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility::SerializeValue(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1 (String_t* ___key0, RuntimeObject * ___value1, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** ___dic2, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences3, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, String_t*, const RuntimeMethod*))Dictionary_2_ContainsKey_m4F01DBE7409811CAB0BBA7AEFBAB4BC028D26FA6_gshared)(__this, ___key0, method);
}
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Object>::Contains(!0)
inline bool List_1_Contains_m7981AB576A967AF513478AA362175476B4DA4CF9 (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *, const RuntimeMethod*))List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Object>::Add(!0)
inline void List_1_Add_mCF93E3DFA395F0F2B4EA20436FE03EF4CFC46FB9 (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Object>::IndexOf(!0)
inline int32_t List_1_IndexOf_m7CEA1AFEC4C2ECD458BDA3E4B1DC9610C9F23499 (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___item0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *, const RuntimeMethod*))List_1_IndexOf_m4474628ACB239463EEF6742E298897E550641533_gshared)(__this, ___item0, method);
}
// System.Int32 UnityEngine.LayerMask::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_get_value_m6380C7449537F99361797225E179A9448A53DDF9 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsPrimitive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_get_IsPrimitive_m43E50D507C45CE3BD51C0DC07C8AB061AFD6B3C3 (Type_t * __this, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046 (Type_t * ___left0, Type_t * ___right1, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Object DevionGames.MiniJSON::Deserialize(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MiniJSON_Deserialize_mC78A07565C97B728803C8AE751D7583B7E5A4989 (String_t* ___json0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Node>::Clear()
inline void List_1_Clear_m5FB38C219330031C0EB797E49A904F84A284F2D3 (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, String_t* ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, String_t*, RuntimeObject **, const RuntimeMethod*))Dictionary_2_TryGetValue_m048C13E0F44BDC16F7CF01D14E918A84EE72C62C_gshared)(__this, ___key0, ___value1, method);
}
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
inline RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::DeserializeNode(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GraphUtility_DeserializeNode_m6E75890D459E8DD137AE264D5FA32E13BEC40570 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data0, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
inline int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0)
inline RuntimeObject * Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_mB1398A10D048A0246178C59F95003BD338CE7394_gshared)(__this, ___key0, method);
}
// System.Type DevionGames.Utility::GetType(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC (String_t* ___typeName0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility::DeserializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48 (RuntimeObject * ___source0, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data1, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences2, const RuntimeMethod* method);
// System.Object DevionGames.Graphs.GraphUtility::DeserializeValue(System.String,System.Object,System.Reflection.FieldInfo,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD (String_t* ___key0, RuntimeObject * ___source1, FieldInfo_t * ___field2, Type_t * ___type3, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data4, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences5, const RuntimeMethod* method);
// System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FieldInfo_SetValue_mA1EFB5DA5E4B930A617744E29E909FE9DEAA663C (FieldInfo_t * __this, RuntimeObject * ___obj0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Int32 System.Convert::ToInt32(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Convert_ToInt32_mFFEDED67681E3EC8705BCA890BBC206514431B4A (RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Object>::get_Count()
inline int32_t List_1_get_Count_mB894CE9F78F9B3D56F6EC500CB45DDA05137AF52_inline (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<UnityEngine.Object>::get_Item(System.Int32)
inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * List_1_get_Item_m5609620CF6DC82315623CF4C9227475D860679E2_inline (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * (*) (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LayerMask_set_value_mE825B6131A75814FCF2EA32ECBE2A205E6531585 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Object System.Enum::Parse(System.Type,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enum_Parse_m6601224637A9CF40F77358805956C2EE757EAF68 (Type_t * ___enumType0, String_t* ___value1, const RuntimeMethod* method);
// System.Type DevionGames.Utility::GetElementType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B (Type_t * ___type0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Count()
inline int32_t Dictionary_2_get_Count_m41D28F48221F99DB3016C0F5CF4B210291338546 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *, const RuntimeMethod*))Dictionary_2_get_Count_m1B599EE742A00E8D399B43E225AD4C6571FBC8DA_gshared)(__this, method);
}
// System.Boolean System.Type::get_IsArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_get_IsArray_m15FE83DD8FAF090F9BDA924753C7750AAEA7CFD1 (Type_t * __this, const RuntimeMethod* method);
// System.Array System.Array::CreateInstance(System.Type,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeArray * Array_CreateInstance_m57AC02F4475AF70CA317B48F09B3C29E3BA9C046 (Type_t * ___elementType0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90 (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Object System.Convert::ChangeType(System.Object,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Convert_ChangeType_mD726EC15920319382D858ECD7FD78339110D7FD4 (RuntimeObject * ___value0, Type_t * ___conversionType1, const RuntimeMethod* method);
// System.Void System.Attribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1 (Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71 * __this, const RuntimeMethod* method);
// System.Guid System.Guid::NewGuid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Guid_t  Guid_NewGuid_m5BD19325820690ED6ECA31D67BC2CD474DC4FDB0 (const RuntimeMethod* method);
// System.String System.Guid::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Guid_ToString_mA3AB7742FB0E04808F580868E82BDEB93187FB75 (Guid_t * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, String_t* ___iconPath0, bool ___displayHeader1, String_t* ___category2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::.ctor()
inline void List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void DevionGames.Graphs.Edge::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B (Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::Add(!0)
inline void List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0 (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void DevionGames.Graphs.Port/<>c__DisplayClass18_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass18_0__ctor_m180DB3EEA83BAF5B3727F12436285AD5EEFA327D (U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * __this, const RuntimeMethod* method);
// System.Void System.Predicate`1<DevionGames.Graphs.Edge>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC (Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared)(__this, ___object0, ___method1, method);
}
// System.Int32 System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::RemoveAll(System.Predicate`1<!0>)
inline int32_t List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 * ___match0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 *, const RuntimeMethod*))List_1_RemoveAll_m3CA48D06F70CC498ADA7410EB930969256F43675_gshared)(__this, ___match0, method);
}
// System.Void System.Collections.Generic.List`1<DevionGames.Graphs.Edge>::Clear()
inline void List_1_Clear_m62F1417B988555AB608ECA0C7DE6B76E2D7960AA (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FlowNode/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m22B16B9EF02763B8D48635AEFF43B1AD3D089375 (U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.Formula/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m9342E5C7F5E94A50D12343A0C4E500DBAB1264CE (U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.FormulaGraph/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEA18434FBED31CEAABB9A3DE9DD064F7E138682B (U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_1__ctor_mC51161D4D15A0B5F1E17AE8EC55CF6FC4E3381C4 (U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<DevionGames.Graphs.FlowNode,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mC17802B326CCF03C5CDAAC9B94533C3E899F3DDE (Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Boolean System.Linq.Enumerable::Any<DevionGames.Graphs.FlowNode>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline bool Enumerable_Any_TisFlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_m409E7D2E1AE678244328F78B52C2BAF3BF8E3B6E (RuntimeObject* ___source0, Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 * ___predicate1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 *, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m79E5BEA29E663B44BB9C1BDD62286D214D22E600_gshared)(___source0, ___predicate1, method);
}
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_1__ctor_m1DF92E78F67367063CE6255316D751DCAA3D76DE (U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * __this, const RuntimeMethod* method);
// System.Void System.Func`2<DevionGames.Graphs.Node,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m04B7DEF1A5A31A1BEBF83465FB5BBCECE52C7CAE (Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Boolean System.Linq.Enumerable::Any<DevionGames.Graphs.Node>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline bool Enumerable_Any_TisNode_tD0AFD8B27090B24E967D338520DEF077662A61DF_m36DE5710832D7F4C686483656AB39178B531F9FF (RuntimeObject* ___source0, Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED * ___predicate1, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject*, Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED *, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m79E5BEA29E663B44BB9C1BDD62286D214D22E600_gshared)(___source0, ___predicate1, method);
}
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Add::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Add_OnRequestValue_mD7164393B4970FDF303893B0B7A86A2FD1C31352 (Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return GetInputValue("a", a) + GetInputValue("b", b);
		float L_0 = __this->get_a_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_b_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4 = ((float)il2cpp_codegen_add((float)L_1, (float)L_3));
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		V_0 = L_5;
		goto IL_002c;
	}

IL_002c:
	{
		// }
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void DevionGames.Graphs.Add::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Add__ctor_mA02D0EE426DFDA33FB078F4C18A2AD7BFEA46BD8 (Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4 * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Ceil::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Ceil_OnRequestValue_m5517F0F2AD7C5F0FC8805C08B7ED71FA6FE859AA (Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return Mathf.Ceil(GetInputValue("value", value));
		float L_0 = __this->get_value_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2;
		L_2 = ceilf(L_1);
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		// }
		RuntimeObject * L_5 = V_0;
		return L_5;
	}
}
// System.Void DevionGames.Graphs.Ceil::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Ceil__ctor_mA7BFC45C9A0C72BBA00F2FD8796D63874D9EADF2 (Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323 * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Divide::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Divide_OnRequestValue_m63DD9454A9668215E87AC7BFE7BEAFBA7BACA569 (Divide_t0E2802337E30A27C1C488271C61C2214772281FF * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return GetInputValue("a", a) / GetInputValue("b", b);
		float L_0 = __this->get_a_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_b_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4 = ((float)((float)L_1/(float)L_3));
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		V_0 = L_5;
		goto IL_002c;
	}

IL_002c:
	{
		// }
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void DevionGames.Graphs.Divide::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Divide__ctor_m768E398CBC9CA2A31AA301164A4304BA77501A15 (Divide_t0E2802337E30A27C1C488271C61C2214772281FF * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.Edge::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B (Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Evaluate::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Evaluate_OnRequestValue_mA88B77FCF48A285E95133A367A493898A7D8DA20 (Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral51C6279E31F7483126B79E3000116001A915B690);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return curve.Evaluate(GetInputValue("time", time)) ;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_curve_6();
		float L_1 = __this->get_time_5();
		float L_2;
		L_2 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral51C6279E31F7483126B79E3000116001A915B690, L_1, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_3;
		L_3 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_0, L_2, /*hidden argument*/NULL);
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		V_0 = L_5;
		goto IL_0025;
	}

IL_0025:
	{
		// }
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void DevionGames.Graphs.Evaluate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Evaluate__ctor_m4CD5D7B75DD466CD7162EF27CA3817CCAF7708EE (Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AnimationCurve curve=new AnimationCurve();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E(L_0, /*hidden argument*/NULL);
		__this->set_curve_6(L_0);
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.EventNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventNode__ctor_mA5F5325FE2F3816BAC25F88D8BB33B2BFEA6B7AF (EventNode_t6C7BF4F88C4264940AED17FE36C8E8A0E398AD8F * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Floor::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Floor_OnRequestValue_mF8135CE73DE6D7B1A20D18ABF135B9D525CBEEB4 (Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return Mathf.Floor(GetInputValue("value", value));
		float L_0 = __this->get_value_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2;
		L_2 = floorf(L_1);
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		// }
		RuntimeObject * L_5 = V_0;
		return L_5;
	}
}
// System.Void DevionGames.Graphs.Floor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Floor__ctor_mF7F70D54AD4BF6E48FA82A476FB28F54210C87D0 (Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9 * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FlowGraph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowGraph__ctor_mC5D4BA1D3523EE75C16FF16899853513F3416DF8 (FlowGraph_t9A6068907F5E8BFFA7E6376D59860B7DBC8577E0 * __this, const RuntimeMethod* method)
{
	{
		Graph__ctor_mF13B8DA9134092BC46305E78DF7A4631353C0AFD(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_Ports()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * FlowNode_get_Ports_m79D5C805ABCF819440DA1A96B204CDD1D1ED203C (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * V_0 = NULL;
	{
		// return m_Ports;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_InputPorts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * FlowNode_get_InputPorts_m641161DFC39978BF5843693573B736DD902E5003 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * V_0 = NULL;
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * G_B2_0 = NULL;
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * G_B2_1 = NULL;
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * G_B1_0 = NULL;
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * G_B1_1 = NULL;
	{
		// return m_Ports.Where(x=>x.direction == PortDirection.Input).ToList();
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_1 = ((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->get_U3CU3E9__4_0_1();
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * L_3 = ((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_4 = (Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC *)il2cpp_codegen_object_new(Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC_il2cpp_TypeInfo_var);
		Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3_RuntimeMethod_var);
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_5 = L_4;
		((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->set_U3CU3E9__4_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0026:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8(G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8_RuntimeMethod_var);
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_7;
		L_7 = Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26(L_6, /*hidden argument*/Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26_RuntimeMethod_var);
		V_0 = L_7;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_8 = V_0;
		return L_8;
	}
}
// System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_OutputPorts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * FlowNode_get_OutputPorts_mCF2AD3A195A09DBACF4C9A47BF5E35AFC75B0185 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * V_0 = NULL;
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * G_B2_0 = NULL;
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * G_B2_1 = NULL;
	Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * G_B1_0 = NULL;
	List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * G_B1_1 = NULL;
	{
		// return m_Ports.Where(x => x.direction == PortDirection.Output).ToList();
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_1 = ((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->get_U3CU3E9__6_0_2();
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * L_3 = ((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_4 = (Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC *)il2cpp_codegen_object_new(Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC_il2cpp_TypeInfo_var);
		Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m0A8A8D1F6FE5DCE7095ED1D65FA8E4AE7727FCE3_RuntimeMethod_var);
		Func_2_t3F49FEC722D8C1D4408D0B5815D5AE778449F7EC * L_5 = L_4;
		((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->set_U3CU3E9__6_0_2(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0026:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8(G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Where_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_m8857A84EAA5B0A418BDA052B9083A64CB655E1C8_RuntimeMethod_var);
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_7;
		L_7 = Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26(L_6, /*hidden argument*/Enumerable_ToList_TisPort_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_mA478666254A33F54BECA05907EC8BD7B646B3F26_RuntimeMethod_var);
		V_0 = L_7;
		goto IL_0033;
	}

IL_0033:
	{
		// }
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_8 = V_0;
		return L_8;
	}
}
// System.Void DevionGames.Graphs.FlowNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m6F74D1B3AF6AFC69F3E9C25B39D635EFD5100162_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<Port> m_Ports= new List<Port>();
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = (List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD *)il2cpp_codegen_object_new(List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD_il2cpp_TypeInfo_var);
		List_1__ctor_m6F74D1B3AF6AFC69F3E9C25B39D635EFD5100162(L_0, /*hidden argument*/List_1__ctor_m6F74D1B3AF6AFC69F3E9C25B39D635EFD5100162_RuntimeMethod_var);
		__this->set_m_Ports_4(L_0);
		// public FlowNode()
		Node__ctor_m607E8DB7A61022982F71AE46459C355F082BA030(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// DevionGames.Graphs.Port DevionGames.Graphs.FlowNode::GetPort(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * FlowNode_GetPort_m05E579E4664F3400D3C4CB47FFC9881D665EA16C (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, String_t* ___fieldName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_t37A5DE42085D332C641B55950592659162DD2774_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * V_0 = NULL;
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_1 = NULL;
	{
		U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * L_0 = (U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass11_0__ctor_mA1FED964843DC56317790239CA60118DE619C3E1(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * L_1 = V_0;
		String_t* L_2 = ___fieldName0;
		L_1->set_fieldName_0(L_2);
		// return m_Ports.Find((port) => port.fieldName == fieldName);
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_3 = __this->get_m_Ports_4();
		U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * L_4 = V_0;
		Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 * L_5 = (Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 *)il2cpp_codegen_object_new(Predicate_1_t37A5DE42085D332C641B55950592659162DD2774_il2cpp_TypeInfo_var);
		Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1_RuntimeMethod_var);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_6;
		L_6 = List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1(L_3, L_5, /*hidden argument*/List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1_RuntimeMethod_var);
		V_1 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_7 = V_1;
		return L_7;
	}
}
// DevionGames.Graphs.Port DevionGames.Graphs.FlowNode::GetPort(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * FlowNode_GetPort_mD43CFCAB159FA796FE9B5561D57578CCED163749 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_0 = NULL;
	{
		// return m_Ports[index];
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		int32_t L_1 = ___index0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2;
		L_2 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		// }
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_3 = V_0;
		return L_3;
	}
}
// System.Void DevionGames.Graphs.FlowNode::AddPort(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m5207CA3F0B39504FFE8EA0E45B2C9C2D6294ABDA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Ports.Add(port);
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_1 = ___port0;
		List_1_Add_m5207CA3F0B39504FFE8EA0E45B2C9C2D6294ABDA(L_0, L_1, /*hidden argument*/List_1_Add_m5207CA3F0B39504FFE8EA0E45B2C9C2D6294ABDA_RuntimeMethod_var);
		// port.node = this;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2 = ___port0;
		L_2->set_node_0(__this);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.FlowNode::DisconnectAllPorts()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode_DisconnectAllPorts_m8FA2AC33D4D8E4D4074B258674528E9A596FF306 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mEEDE6D8C098C939874334B6A2448D278272F0556_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mC2EA6ED235C550D981F43C570F13379D5BE1CB1E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m96C2ED12F1680F282C4B654DB9B5EC1AFD99A142_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mE83854FC821DCA2E3637C6AE7D6EC5222F587D88_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (var port in this.m_Ports)
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72  L_1;
		L_1 = List_1_GetEnumerator_mE83854FC821DCA2E3637C6AE7D6EC5222F587D88(L_0, /*hidden argument*/List_1_GetEnumerator_mE83854FC821DCA2E3637C6AE7D6EC5222F587D88_RuntimeMethod_var);
		V_0 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0021;
		}

IL_0010:
		{
			// foreach (var port in this.m_Ports)
			Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2;
			L_2 = Enumerator_get_Current_m96C2ED12F1680F282C4B654DB9B5EC1AFD99A142_inline((Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m96C2ED12F1680F282C4B654DB9B5EC1AFD99A142_RuntimeMethod_var);
			V_1 = L_2;
			// port.DisconnectAll();
			Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_3 = V_1;
			Port_DisconnectAll_m9A9345796CFB57DFE8DAE23B538E0D30E23886F0(L_3, /*hidden argument*/NULL);
		}

IL_0021:
		{
			// foreach (var port in this.m_Ports)
			bool L_4;
			L_4 = Enumerator_MoveNext_mC2EA6ED235C550D981F43C570F13379D5BE1CB1E((Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mC2EA6ED235C550D981F43C570F13379D5BE1CB1E_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_0010;
			}
		}

IL_002a:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_002c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002c;
	}

FINALLY_002c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mEEDE6D8C098C939874334B6A2448D278272F0556((Enumerator_t650D16F89B0AF8DBE4897F3B989A50578DFD5E72 *)(&V_0), /*hidden argument*/Enumerator_Dispose_mEEDE6D8C098C939874334B6A2448D278272F0556_RuntimeMethod_var);
		IL2CPP_END_FINALLY(44)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(44)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.FlowNode::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlowNode_OnAfterDeserialize_mE0BB937EA97F69C1349E8CB75548A56475EE9A06 (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m5C686920C2BB16A365651B4FD76DABA8F6AD82AB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Item_m8AB2C9BDA3A353EEA9DB64D20F5FFD876ACAB875_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_t37A5DE42085D332C641B55950592659162DD2774_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8192EF02E079142FD5CA69DC024E6EF19381C3B6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBCA7DDD073AD5DB21CC612ADB1833BF1A5D32261);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * V_3 = NULL;
	FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	{
		// for (int i = 0; i < this.m_Ports.Count; i++)
		V_0 = 0;
		goto IL_010b;
	}

IL_0008:
	{
		// if (this.m_Ports[i].m_FieldTypeName == "String")
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_0 = __this->get_m_Ports_4();
		int32_t L_1 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2;
		L_2 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		String_t* L_3 = L_2->get_m_FieldTypeName_8();
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, _stringLiteralBCA7DDD073AD5DB21CC612ADB1833BF1A5D32261, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		// this.m_Ports[i].m_FieldTypeName = "System.String";
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_6 = __this->get_m_Ports_4();
		int32_t L_7 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_8;
		L_8 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		L_8->set_m_FieldTypeName_8(_stringLiteral8192EF02E079142FD5CA69DC024E6EF19381C3B6);
	}

IL_003e:
	{
		// this.m_Ports[i].node = this;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_9 = __this->get_m_Ports_4();
		int32_t L_10 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_11;
		L_11 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_9, L_10, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		L_11->set_node_0(__this);
		// for (var j = 0; j < this.m_Ports[i].Connections.Count; j++)
		V_2 = 0;
		goto IL_00e4;
	}

IL_0057:
	{
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_12 = (U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass15_0__ctor_m676586F14260D951119E7BF393C9CFF81BFAC368(L_12, /*hidden argument*/NULL);
		V_3 = L_12;
		// Edge edge = this.m_Ports[i].Connections[j];
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_13 = V_3;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_14 = __this->get_m_Ports_4();
		int32_t L_15 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_16;
		L_16 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_14, L_15, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_17;
		L_17 = Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2(L_16, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_19;
		L_19 = List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_inline(L_17, L_18, /*hidden argument*/List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_RuntimeMethod_var);
		L_13->set_edge_0(L_19);
		// FlowNode connected = graph.nodes.Find(x => x.id == edge.nodeId) as FlowNode;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_20 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)__this)->get_graph_3();
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_21 = L_20->get_nodes_2();
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_22 = V_3;
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_23 = (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *)il2cpp_codegen_object_new(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9(L_23, L_22, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_24;
		L_24 = List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6(L_21, L_23, /*hidden argument*/List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		V_4 = ((FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 *)IsInstClass((RuntimeObject*)L_24, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_il2cpp_TypeInfo_var));
		// edge.port = connected.Ports.Find(x => x.fieldName == edge.fieldName);
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_25 = V_3;
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_26 = L_25->get_edge_0();
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_27 = V_4;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_28;
		L_28 = FlowNode_get_Ports_m79D5C805ABCF819440DA1A96B204CDD1D1ED203C(L_27, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_29 = V_3;
		Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 * L_30 = (Predicate_1_t37A5DE42085D332C641B55950592659162DD2774 *)il2cpp_codegen_object_new(Predicate_1_t37A5DE42085D332C641B55950592659162DD2774_il2cpp_TypeInfo_var);
		Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1(L_30, L_29, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_mBF76A4D38D83816D733B881DE50AD769B03EC9C1_RuntimeMethod_var);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_31;
		L_31 = List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1(L_28, L_30, /*hidden argument*/List_1_Find_m721FBE36DD1BDD59D4056C4FA48DA0C9F26CA3D1_RuntimeMethod_var);
		L_26->set_port_2(L_31);
		// this.m_Ports[i].Connections[j] = edge;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_32 = __this->get_m_Ports_4();
		int32_t L_33 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_34;
		L_34 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_32, L_33, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_35;
		L_35 = Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2(L_34, /*hidden argument*/NULL);
		int32_t L_36 = V_2;
		U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * L_37 = V_3;
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_38 = L_37->get_edge_0();
		List_1_set_Item_m8AB2C9BDA3A353EEA9DB64D20F5FFD876ACAB875(L_35, L_36, L_38, /*hidden argument*/List_1_set_Item_m8AB2C9BDA3A353EEA9DB64D20F5FFD876ACAB875_RuntimeMethod_var);
		// for (var j = 0; j < this.m_Ports[i].Connections.Count; j++)
		int32_t L_39 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00e4:
	{
		// for (var j = 0; j < this.m_Ports[i].Connections.Count; j++)
		int32_t L_40 = V_2;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_41 = __this->get_m_Ports_4();
		int32_t L_42 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_43;
		L_43 = List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_inline(L_41, L_42, /*hidden argument*/List_1_get_Item_m7EAD5C43BBBCE0A97FCD751D0DEAD6373726046A_RuntimeMethod_var);
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_44;
		L_44 = Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2(L_43, /*hidden argument*/NULL);
		int32_t L_45;
		L_45 = List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_inline(L_44, /*hidden argument*/List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_RuntimeMethod_var);
		V_5 = (bool)((((int32_t)L_40) < ((int32_t)L_45))? 1 : 0);
		bool L_46 = V_5;
		if (L_46)
		{
			goto IL_0057;
		}
	}
	{
		// for (int i = 0; i < this.m_Ports.Count; i++)
		int32_t L_47 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_010b:
	{
		// for (int i = 0; i < this.m_Ports.Count; i++)
		int32_t L_48 = V_0;
		List_1_t25E056CC3B17717BF4774A67F588C4C0064AA5FD * L_49 = __this->get_m_Ports_4();
		int32_t L_50;
		L_50 = List_1_get_Count_m5C686920C2BB16A365651B4FD76DABA8F6AD82AB_inline(L_49, /*hidden argument*/List_1_get_Count_m5C686920C2BB16A365651B4FD76DABA8F6AD82AB_RuntimeMethod_var);
		V_6 = (bool)((((int32_t)L_48) < ((int32_t)L_50))? 1 : 0);
		bool L_51 = V_6;
		if (L_51)
		{
			goto IL_0008;
		}
	}
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DevionGames.Graphs.Graph DevionGames.Graphs.Formula::GetGraph()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * Formula_GetGraph_m46FB2CCDE30D85D1770364AA7E7DABDDAA452382 (Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA * __this, const RuntimeMethod* method)
{
	Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * V_0 = NULL;
	{
		// return this.m_Graph;
		FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * L_0 = __this->get_m_Graph_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_1 = V_0;
		return L_1;
	}
}
// System.Single DevionGames.Graphs.Formula::op_Implicit(DevionGames.Graphs.Formula)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Formula_op_Implicit_m7DA6C405115DE7683C94CB4963C772EC638F68FE (Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA * ___formula0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B);
		s_Il2CppMethodInitialized = true;
	}
	FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * V_0 = NULL;
	float V_1 = 0.0f;
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * G_B2_0 = NULL;
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * G_B2_1 = NULL;
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * G_B1_0 = NULL;
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * G_B1_1 = NULL;
	{
		// FormulaOutput output = formula.GetGraph().nodes.Find(x => x.GetType() == typeof(FormulaOutput)) as FormulaOutput;
		Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA * L_0 = ___formula0;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_1;
		L_1 = Formula_GetGraph_m46FB2CCDE30D85D1770364AA7E7DABDDAA452382(L_0, /*hidden argument*/NULL);
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_2 = L_1->get_nodes_2();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var);
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_3 = ((U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var))->get_U3CU3E9__2_0_1();
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var);
		U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * L_5 = ((U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_6 = (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *)il2cpp_codegen_object_new(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_7 = L_6;
		((U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var))->set_U3CU3E9__2_0_1(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
	}

IL_002b:
	{
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_8;
		L_8 = List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6(G_B2_1, G_B2_0, /*hidden argument*/List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		V_0 = ((FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 *)IsInstClass((RuntimeObject*)L_8, FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_il2cpp_TypeInfo_var));
		// return output.GetInputValue<float>("result", output.result);
		FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * L_9 = V_0;
		FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * L_10 = V_0;
		float L_11 = L_10->get_result_5();
		float L_12;
		L_12 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(L_9, _stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B, L_11, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		V_1 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		// }
		float L_13 = V_1;
		return L_13;
	}
}
// System.Void DevionGames.Graphs.Formula::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Formula__ctor_mEBD93C95EAF59860D1551CF3CCBFFDF15C342F56 (Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FormulaGraph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormulaGraph__ctor_mECDA736FBD6605313442A8ABFCBE7BDFFCA3E919 (FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public FormulaGraph() {
		FlowGraph__ctor_mC5D4BA1D3523EE75C16FF16899853513F3416DF8(__this, /*hidden argument*/NULL);
		// GraphUtility.AddNode(this, typeof(FormulaOutput));
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_2;
		L_2 = GraphUtility_AddNode_m3D1B9E811B1DD86FE2785ABC80DC371EF2AA237F(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Single DevionGames.Graphs.FormulaGraph::op_Implicit(DevionGames.Graphs.FormulaGraph)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FormulaGraph_op_Implicit_m9E3DBD69E33B87822AD0975C96BE9C0973C020E3 (FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * ___graph0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B);
		s_Il2CppMethodInitialized = true;
	}
	FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * V_0 = NULL;
	float V_1 = 0.0f;
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * G_B2_0 = NULL;
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * G_B2_1 = NULL;
	Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * G_B1_0 = NULL;
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * G_B1_1 = NULL;
	{
		// FormulaOutput output = graph.nodes.Find(x => x.GetType() == typeof(FormulaOutput)) as FormulaOutput;
		FormulaGraph_t9633850661E4E661A107DD4BE7C51220FD88AC15 * L_0 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_1 = ((Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 *)L_0)->get_nodes_2();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var);
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_2 = ((U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var))->get_U3CU3E9__1_0_1();
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_3 = L_2;
		G_B1_0 = L_3;
		G_B1_1 = L_1;
		if (L_3)
		{
			G_B2_0 = L_3;
			G_B2_1 = L_1;
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var);
		U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * L_4 = ((U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_5 = (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *)il2cpp_codegen_object_new(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_6 = L_5;
		((U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var))->set_U3CU3E9__1_0_1(L_6);
		G_B2_0 = L_6;
		G_B2_1 = G_B1_1;
	}

IL_0026:
	{
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_7;
		L_7 = List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6(G_B2_1, G_B2_0, /*hidden argument*/List_1_Find_m80E764AEB0BF68557A0A999DDDD880B986F408B6_RuntimeMethod_var);
		V_0 = ((FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 *)IsInstClass((RuntimeObject*)L_7, FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_il2cpp_TypeInfo_var));
		// return output.GetInputValue<float>("result", output.result);
		FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * L_8 = V_0;
		FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * L_9 = V_0;
		float L_10 = L_9->get_result_5();
		float L_11;
		L_11 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(L_8, _stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B, L_10, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		V_1 = L_11;
		goto IL_0045;
	}

IL_0045:
	{
		// }
		float L_12 = V_1;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.FormulaOutput::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * FormulaOutput_OnRequestValue_m5E4E152E713B280130D5D0E847665DF4DA94467A (FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return GetInputValue("result", result);
		float L_0 = __this->get_result_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral228835A6E22383C9BC1AAFD065E6D63FA30BF27B, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_2);
		V_0 = L_3;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		RuntimeObject * L_4 = V_0;
		return L_4;
	}
}
// System.Void DevionGames.Graphs.FormulaOutput::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormulaOutput__ctor_mAFCB23A3D06F0C69DE2899825C89B65461D4A7A1 (FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5 * __this, const RuntimeMethod* method)
{
	{
		EventNode__ctor_mA5F5325FE2F3816BAC25F88D8BB33B2BFEA6B7AF(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.Graph::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graph_OnBeforeSerialize_m9975C27B1F141DE195EA9C09FB35A78885C19105 (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Graph::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graph_OnAfterDeserialize_mDB70B72D05D24A32572B4D96296DDA52E60715F4 (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * __this, const RuntimeMethod* method)
{
	{
		// GraphUtility.Load(this);
		GraphUtility_Load_m4C2361BA90120FD465B90AC31F423B405C220B9F(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Graph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graph__ctor_mF13B8DA9134092BC46305E78DF7A4631353C0AFD (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m3A4E207AFD5D554933D682752DE86150935C5565_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Object> serializedObjects = new List<Object>();
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_0 = (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *)il2cpp_codegen_object_new(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_il2cpp_TypeInfo_var);
		List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4(L_0, /*hidden argument*/List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4_RuntimeMethod_var);
		__this->set_serializedObjects_1(L_0);
		// public List<Node> nodes = new List<Node>();
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_1 = (List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB *)il2cpp_codegen_object_new(List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB_il2cpp_TypeInfo_var);
		List_1__ctor_m3A4E207AFD5D554933D682752DE86150935C5565(L_1, /*hidden argument*/List_1__ctor_m3A4E207AFD5D554933D682752DE86150935C5565_RuntimeMethod_var);
		__this->set_nodes_2(L_1);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::AddNode(DevionGames.Graphs.Graph,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GraphUtility_AddNode_m3D1B9E811B1DD86FE2785ABC80DC371EF2AA237F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, Type_t * ___type1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_0 = NULL;
	bool V_1 = false;
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_2 = NULL;
	{
		// Node node = System.Activator.CreateInstance(type) as Node;
		Type_t * L_0 = ___type1;
		RuntimeObject * L_1;
		L_1 = Activator_CreateInstance_m1BACAB5F4FBF138CCCB537DDCB0683A2AC064295(L_0, /*hidden argument*/NULL);
		V_0 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)IsInstClass((RuntimeObject*)L_1, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_il2cpp_TypeInfo_var));
		// if (typeof(FlowNode).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		Type_t * L_4 = ___type1;
		bool L_5;
		L_5 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_3, L_4);
		V_1 = L_5;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		// CreatePorts(node as FlowNode);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_7 = V_0;
		GraphUtility_CreatePorts_m5D6C07B357EBCE3B7C322F94E1AC9696D7E5960B(((FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 *)IsInstClass((RuntimeObject*)L_7, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_002f:
	{
		// node.name = NicifyVariableName(type.Name);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_8 = V_0;
		Type_t * L_9 = ___type1;
		String_t* L_10;
		L_10 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_9);
		String_t* L_11;
		L_11 = GraphUtility_NicifyVariableName_m9037A019C3386722DBA5ED850159659A2C4CDD1A(L_10, /*hidden argument*/NULL);
		L_8->set_name_1(L_11);
		// node.graph = graph;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_12 = V_0;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_13 = ___graph0;
		L_12->set_graph_3(L_13);
		// graph.nodes.Add(node);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_14 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_15 = L_14->get_nodes_2();
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_16 = V_0;
		List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC(L_15, L_16, /*hidden argument*/List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC_RuntimeMethod_var);
		// Save(graph);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_17 = ___graph0;
		GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F(L_17, /*hidden argument*/NULL);
		// return node;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_18 = V_0;
		V_2 = L_18;
		goto IL_005f;
	}

IL_005f:
	{
		// }
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_19 = V_2;
		return L_19;
	}
}
// System.String DevionGames.Graphs.GraphUtility::NicifyVariableName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GraphUtility_NicifyVariableName_m9037A019C3386722DBA5ED850159659A2C4CDD1A (String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	Il2CppChar V_3 = 0x0;
	bool V_4 = false;
	String_t* V_5 = NULL;
	int32_t G_B4_0 = 0;
	{
		// string result = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// for (int i = 0; i < name.Length; i++)
		V_1 = 0;
		goto IL_004e;
	}

IL_000b:
	{
		// if (char.IsUpper(name[i]) == true && i != 0)
		String_t* L_0 = ___name0;
		int32_t L_1 = V_1;
		Il2CppChar L_2;
		L_2 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Char_IsUpper_m72EAD892A02AD10D2050EA09BF3735DDE6921892(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_4 = V_1;
		G_B4_0 = ((!(((uint32_t)L_4) <= ((uint32_t)0)))? 1 : 0);
		goto IL_0021;
	}

IL_0020:
	{
		G_B4_0 = 0;
	}

IL_0021:
	{
		V_2 = (bool)G_B4_0;
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		// result += " ";
		String_t* L_6 = V_0;
		String_t* L_7;
		L_7 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_6, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0033:
	{
		// result += name[i];
		String_t* L_8 = V_0;
		String_t* L_9 = ___name0;
		int32_t L_10 = V_1;
		Il2CppChar L_11;
		L_11 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70(L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		String_t* L_12;
		L_12 = Char_ToString_mE0DE433463C56FD30A4F0A50539553B17147C2F8((Il2CppChar*)(&V_3), /*hidden argument*/NULL);
		String_t* L_13;
		L_13 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_8, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		// for (int i = 0; i < name.Length; i++)
		int32_t L_14 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_004e:
	{
		// for (int i = 0; i < name.Length; i++)
		int32_t L_15 = V_1;
		String_t* L_16 = ___name0;
		int32_t L_17;
		L_17 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline(L_16, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_15) < ((int32_t)L_17))? 1 : 0);
		bool L_18 = V_4;
		if (L_18)
		{
			goto IL_000b;
		}
	}
	{
		// return result;
		String_t* L_19 = V_0;
		V_5 = L_19;
		goto IL_0062;
	}

IL_0062:
	{
		// }
		String_t* L_20 = V_5;
		return L_20;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::RemoveNodes(DevionGames.Graphs.Graph,DevionGames.Graphs.FlowNode[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_RemoveNodes_m5493AE1B5960EF5E3DBE6E0BB93578628C1AD2CC (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* ___nodes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * V_0 = NULL;
	int32_t V_1 = 0;
	FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * V_2 = NULL;
	bool V_3 = false;
	{
		U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * L_0 = (U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_mF3C6BAD036536D3343CAD9AAD7F1D00950AD5FBE(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * L_1 = V_0;
		FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* L_2 = ___nodes1;
		L_1->set_nodes_0(L_2);
		// for (int i = 0; i < nodes.Length; i++)
		V_1 = 0;
		goto IL_0028;
	}

IL_0012:
	{
		// FlowNode node = nodes[i];
		U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * L_3 = V_0;
		FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* L_4 = L_3->get_nodes_0();
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		// node.DisconnectAllPorts();
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_8 = V_2;
		FlowNode_DisconnectAllPorts_m8FA2AC33D4D8E4D4074B258674528E9A596FF306(L_8, /*hidden argument*/NULL);
		// for (int i = 0; i < nodes.Length; i++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0028:
	{
		// for (int i = 0; i < nodes.Length; i++)
		int32_t L_10 = V_1;
		U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * L_11 = V_0;
		FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* L_12 = L_11->get_nodes_0();
		V_3 = (bool)((((int32_t)L_10) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))))? 1 : 0);
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_0012;
		}
	}
	{
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x ));
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_14 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_15 = L_14->get_nodes_2();
		U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * L_16 = V_0;
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_17 = (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *)il2cpp_codegen_object_new(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9(L_17, L_16, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		int32_t L_18;
		L_18 = List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D(L_15, L_17, /*hidden argument*/List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D_RuntimeMethod_var);
		// Save(graph);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_19 = ___graph0;
		GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F(L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::RemoveNodes(DevionGames.Graphs.Graph,DevionGames.Graphs.Node[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_RemoveNodes_mF82276CB474A6052B82236BFDBE047B545C7F882 (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* ___nodes1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * V_0 = NULL;
	int32_t V_1 = 0;
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_2 = NULL;
	bool V_3 = false;
	{
		U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * L_0 = (U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m6F79A73FE4250F82E7C386C679CA9A8EFE39B4EF(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * L_1 = V_0;
		NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* L_2 = ___nodes1;
		L_1->set_nodes_0(L_2);
		// for (int i = 0; i < nodes.Length; i++)
		V_1 = 0;
		goto IL_0021;
	}

IL_0012:
	{
		// Node node = nodes[i];
		U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * L_3 = V_0;
		NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* L_4 = L_3->get_nodes_0();
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		// for (int i = 0; i < nodes.Length; i++)
		int32_t L_8 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0021:
	{
		// for (int i = 0; i < nodes.Length; i++)
		int32_t L_9 = V_1;
		U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * L_10 = V_0;
		NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* L_11 = L_10->get_nodes_0();
		V_3 = (bool)((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))? 1 : 0);
		bool L_12 = V_3;
		if (L_12)
		{
			goto IL_0012;
		}
	}
	{
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x));
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_13 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_14 = L_13->get_nodes_2();
		U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * L_15 = V_0;
		Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 * L_16 = (Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574 *)il2cpp_codegen_object_new(Predicate_1_tCB2F4C1EA26A023986102D2C496E99553EC53574_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9(L_16, L_15, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m16C1EB408A9FFBD498020857F9F89934AEF7A7E9_RuntimeMethod_var);
		int32_t L_17;
		L_17 = List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D(L_14, L_16, /*hidden argument*/List_1_RemoveAll_m28EC79CDADAC10B8F082FA9E3AC3ACD390C89D2D_RuntimeMethod_var);
		// Save(graph);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_18 = ___graph0;
		GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F(L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::CreatePorts(DevionGames.Graphs.FlowNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_CreatePorts_m5D6C07B357EBCE3B7C322F94E1AC9696D7E5960B (FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_GetCustomAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_mAF061D40815EFB9D8390D2732BCB25B7F5E0E036_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_HasAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_m7F7D8E5E5D8629A2804D0E49B47CD218BA869FEB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_HasAttribute_TisOutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_mEC474F5773AA45736B7EC27ECC883D70E91271C0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* V_0 = NULL;
	int32_t V_1 = 0;
	FieldInfo_t * V_2 = NULL;
	bool V_3 = false;
	InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * V_4 = NULL;
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_5 = NULL;
	bool V_6 = false;
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_7 = NULL;
	bool V_8 = false;
	{
		// FieldInfo[] fields = node.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_0 = ___node0;
		Type_t * L_1;
		L_1 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_0, /*hidden argument*/NULL);
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_2;
		L_2 = VirtFuncInvoker1< FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E*, int32_t >::Invoke(44 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_1, ((int32_t)52));
		V_0 = L_2;
		// for (int i = 0; i < fields.Length; i++)
		V_1 = 0;
		goto IL_009e;
	}

IL_0016:
	{
		// FieldInfo field = fields[i];
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_3 = V_0;
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		FieldInfo_t * L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		// if (field.HasAttribute<InputAttribute>())
		FieldInfo_t * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Utility_HasAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_m7F7D8E5E5D8629A2804D0E49B47CD218BA869FEB(L_7, /*hidden argument*/Utility_HasAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_m7F7D8E5E5D8629A2804D0E49B47CD218BA869FEB_RuntimeMethod_var);
		V_3 = L_8;
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_006c;
		}
	}
	{
		// InputAttribute inputAttribute = field.GetCustomAttribute<InputAttribute>();
		FieldInfo_t * L_10 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * L_11;
		L_11 = Utility_GetCustomAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_mAF061D40815EFB9D8390D2732BCB25B7F5E0E036(L_10, /*hidden argument*/Utility_GetCustomAttribute_TisInputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_mAF061D40815EFB9D8390D2732BCB25B7F5E0E036_RuntimeMethod_var);
		V_4 = L_11;
		// Port port = new Port(node, field.Name,field.FieldType, PortCapacity.Single, PortDirection.Input);
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_12 = ___node0;
		FieldInfo_t * L_13 = V_2;
		String_t* L_14;
		L_14 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_13);
		FieldInfo_t * L_15 = V_2;
		Type_t * L_16;
		L_16 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_15);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_17 = (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 *)il2cpp_codegen_object_new(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_il2cpp_TypeInfo_var);
		Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7(L_17, L_12, L_14, L_16, 0, 0, /*hidden argument*/NULL);
		V_5 = L_17;
		// port.drawPort = inputAttribute.port;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_18 = V_5;
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * L_19 = V_4;
		bool L_20 = L_19->get_port_1();
		L_18->set_drawPort_3(L_20);
		// port.label = inputAttribute.label;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_21 = V_5;
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * L_22 = V_4;
		bool L_23 = L_22->get_label_0();
		L_21->set_label_4(L_23);
		// node.AddPort(port);
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_24 = ___node0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_25 = V_5;
		FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580(L_24, L_25, /*hidden argument*/NULL);
		goto IL_0099;
	}

IL_006c:
	{
		// else if (field.HasAttribute<OutputAttribute>()) {
		FieldInfo_t * L_26 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		bool L_27;
		L_27 = Utility_HasAttribute_TisOutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_mEC474F5773AA45736B7EC27ECC883D70E91271C0(L_26, /*hidden argument*/Utility_HasAttribute_TisOutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_mEC474F5773AA45736B7EC27ECC883D70E91271C0_RuntimeMethod_var);
		V_6 = L_27;
		bool L_28 = V_6;
		if (!L_28)
		{
			goto IL_0099;
		}
	}
	{
		// Port port = new Port(node, field.Name,field.FieldType, PortCapacity.Multiple, PortDirection.Output);
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_29 = ___node0;
		FieldInfo_t * L_30 = V_2;
		String_t* L_31;
		L_31 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_30);
		FieldInfo_t * L_32 = V_2;
		Type_t * L_33;
		L_33 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_32);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_34 = (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 *)il2cpp_codegen_object_new(Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_il2cpp_TypeInfo_var);
		Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7(L_34, L_29, L_31, L_33, 1, 1, /*hidden argument*/NULL);
		V_7 = L_34;
		// node.AddPort(port);
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_35 = ___node0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_36 = V_7;
		FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580(L_35, L_36, /*hidden argument*/NULL);
	}

IL_0099:
	{
		// for (int i = 0; i < fields.Length; i++)
		int32_t L_37 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_009e:
	{
		// for (int i = 0; i < fields.Length; i++)
		int32_t L_38 = V_1;
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_39 = V_0;
		V_8 = (bool)((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_39)->max_length)))))? 1 : 0);
		bool L_40 = V_8;
		if (L_40)
		{
			goto IL_0016;
		}
	}
	{
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::Save(DevionGames.Graphs.Graph)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral800887583098E85FC30F11258ADBB87A8CDBED7E);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * V_0 = NULL;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_1 = NULL;
	List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * V_2 = NULL;
	Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655* V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	{
		// List<Node> nodes = graph.nodes;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_0 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_1 = L_0->get_nodes_2();
		V_0 = L_1;
		// Dictionary<string, object> graphData = new Dictionary<string, object>();
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_2 = (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)il2cpp_codegen_object_new(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63(L_2, /*hidden argument*/Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		V_1 = L_2;
		// List<UnityEngine.Object> objectReferences = new List<UnityEngine.Object>();
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_3 = (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 *)il2cpp_codegen_object_new(List_1_t9D216521E6A213FF8562D215598D336ABB5474F4_il2cpp_TypeInfo_var);
		List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4(L_3, /*hidden argument*/List_1__ctor_m8786AF4A4AE06CDD3B907914EE860C2748ED2EA4_RuntimeMethod_var);
		V_2 = L_3;
		// Dictionary<string, object>[] nodeData = new Dictionary<string, object>[nodes.Count];
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_4 = V_0;
		int32_t L_5;
		L_5 = List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_inline(L_4, /*hidden argument*/List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var);
		Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655* L_6 = (Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655*)(Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655*)SZArrayNew(Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655_il2cpp_TypeInfo_var, (uint32_t)L_5);
		V_3 = L_6;
		// for (int i = 0; i < nodes.Count; i++)
		V_4 = 0;
		goto IL_0040;
	}

IL_0025:
	{
		// nodeData[i] = SerializeNode(nodes[i], ref objectReferences);
		Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655* L_7 = V_3;
		int32_t L_8 = V_4;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_9 = V_0;
		int32_t L_10 = V_4;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_11;
		L_11 = List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_inline(L_9, L_10, /*hidden argument*/List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_RuntimeMethod_var);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_12;
		L_12 = GraphUtility_SerializeNode_m28E8EB533906E6BD25710874F0EA9D1DA56327C0(L_11, (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)(&V_2), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_8), (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)L_12);
		// for (int i = 0; i < nodes.Count; i++)
		int32_t L_13 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0040:
	{
		// for (int i = 0; i < nodes.Count; i++)
		int32_t L_14 = V_4;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_15 = V_0;
		int32_t L_16;
		L_16 = List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_inline(L_15, /*hidden argument*/List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var);
		V_5 = (bool)((((int32_t)L_14) < ((int32_t)L_16))? 1 : 0);
		bool L_17 = V_5;
		if (L_17)
		{
			goto IL_0025;
		}
	}
	{
		// graphData.Add("Nodes",nodeData);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_18 = V_1;
		Dictionary_2U5BU5D_tA1057ED686B28705A60F5B2BD0E1171C1F7CC655* L_19 = V_3;
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_18, _stringLiteral800887583098E85FC30F11258ADBB87A8CDBED7E, (RuntimeObject *)(RuntimeObject *)L_19, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		// graph.serializationData = MiniJSON.Serialize(graphData);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_20 = ___graph0;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_21 = V_1;
		String_t* L_22;
		L_22 = MiniJSON_Serialize_m0EAC236F1654F6BA91D0955E75321F084697656D(L_21, /*hidden argument*/NULL);
		L_20->set_serializationData_0(L_22);
		// graph.serializedObjects = objectReferences;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_23 = ___graph0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_24 = V_2;
		L_23->set_serializedObjects_1(L_24);
		// }
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Object> DevionGames.Graphs.GraphUtility::SerializeNode(DevionGames.Graphs.Node,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * GraphUtility_SerializeNode_m28E8EB533906E6BD25710874F0EA9D1DA56327C0 (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___node0, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA15CF7E1CEFBD0C475E3A89A80B5393D417F8634);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_0 = NULL;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_1 = NULL;
	{
		// Dictionary<string, object> data = new Dictionary<string, object>() {
		//     { "Type", node.GetType () },
		// };
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_0 = (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)il2cpp_codegen_object_new(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63(L_0, /*hidden argument*/Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_1 = L_0;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_2 = ___node0;
		Type_t * L_3;
		L_3 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_2, /*hidden argument*/NULL);
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_1, _stringLiteralA15CF7E1CEFBD0C475E3A89A80B5393D417F8634, L_3, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		V_0 = L_1;
		// SerializeFields(node, ref data, ref objectReferences);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_4 = ___node0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_5 = ___objectReferences1;
		GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC(L_4, (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)(&V_0), (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_5, /*hidden argument*/NULL);
		// return data;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_6 = V_0;
		V_1 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		// }
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_7 = V_1;
		return L_7;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::SerializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC (RuntimeObject * ___obj0, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** ___dic1, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	FieldInfo_t * V_4 = NULL;
	RuntimeObject * V_5 = NULL;
	bool V_6 = false;
	{
		// if (obj == null)
		RuntimeObject * L_0 = ___obj0;
		V_2 = (bool)((((RuntimeObject*)(RuntimeObject *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_2;
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		// return;
		goto IL_0050;
	}

IL_000c:
	{
		// Type type = obj.GetType();
		RuntimeObject * L_2 = ___obj0;
		Type_t * L_3;
		L_3 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// FieldInfo[] fields = type.GetAllSerializedFields();
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_5;
		L_5 = Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// for (int j = 0; j < fields.Length; j++)
		V_3 = 0;
		goto IL_0044;
	}

IL_001e:
	{
		// FieldInfo field = fields[j];
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_6 = V_1;
		int32_t L_7 = V_3;
		int32_t L_8 = L_7;
		FieldInfo_t * L_9 = (L_6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_8));
		V_4 = L_9;
		// object value = field.GetValue(obj);
		FieldInfo_t * L_10 = V_4;
		RuntimeObject * L_11 = ___obj0;
		RuntimeObject * L_12;
		L_12 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(19 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_10, L_11);
		V_5 = L_12;
		// SerializeValue(field.Name, value, ref dic, ref objectReferences);
		FieldInfo_t * L_13 = V_4;
		String_t* L_14;
		L_14 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_13);
		RuntimeObject * L_15 = V_5;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_16 = ___dic1;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_17 = ___objectReferences2;
		GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1(L_14, L_15, (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_16, (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_17, /*hidden argument*/NULL);
		// for (int j = 0; j < fields.Length; j++)
		int32_t L_18 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0044:
	{
		// for (int j = 0; j < fields.Length; j++)
		int32_t L_19 = V_3;
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_20 = V_1;
		V_6 = (bool)((((int32_t)L_19) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))? 1 : 0);
		bool L_21 = V_6;
		if (L_21)
		{
			goto IL_001e;
		}
	}

IL_0050:
	{
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::SerializeValue(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1 (String_t* ___key0, RuntimeObject * ___value1, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** ___dic2, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** ___objectReferences3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mCF93E3DFA395F0F2B4EA20436FE03EF4CFC46FB9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m7981AB576A967AF513478AA362175476B4DA4CF9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_IndexOf_m7CEA1AFEC4C2ECD458BDA3E4B1DC9610C9F23499_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Type_t * V_1 = NULL;
	bool V_2 = false;
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  V_6;
	memset((&V_6), 0, sizeof(V_6));
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	RuntimeObject* V_10 = NULL;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_11 = NULL;
	int32_t V_12 = 0;
	bool V_13 = false;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_14 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B19_0 = 0;
	{
		// if (value != null && !dic.ContainsKey(key))
		RuntimeObject * L_0 = ___value1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_1 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_2 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_1);
		String_t* L_3 = ___key0;
		bool L_4;
		L_4 = Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710_RuntimeMethod_var);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 0;
	}

IL_0012:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_01df;
		}
	}
	{
		// Type type = value.GetType();
		RuntimeObject * L_6 = ___value1;
		Type_t * L_7;
		L_7 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		// if (typeof(UnityEngine.Object).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_8 = { reinterpret_cast<intptr_t> (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9;
		L_9 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_8, /*hidden argument*/NULL);
		Type_t * L_10 = V_1;
		bool L_11;
		L_11 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_9, L_10);
		V_2 = L_11;
		bool L_12 = V_2;
		if (!L_12)
		{
			goto IL_0075;
		}
	}
	{
		// UnityEngine.Object unityObject = value as UnityEngine.Object;
		RuntimeObject * L_13 = ___value1;
		V_3 = ((Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)IsInstClass((RuntimeObject*)L_13, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var));
		// if (!objectReferences.Contains(unityObject))
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_14 = ___objectReferences3;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_15 = *((List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_14);
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_16 = V_3;
		bool L_17;
		L_17 = List_1_Contains_m7981AB576A967AF513478AA362175476B4DA4CF9(L_15, L_16, /*hidden argument*/List_1_Contains_m7981AB576A967AF513478AA362175476B4DA4CF9_RuntimeMethod_var);
		V_4 = (bool)((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_4;
		if (!L_18)
		{
			goto IL_0059;
		}
	}
	{
		// objectReferences.Add(unityObject);
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_19 = ___objectReferences3;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_20 = *((List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_19);
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_21 = V_3;
		List_1_Add_mCF93E3DFA395F0F2B4EA20436FE03EF4CFC46FB9(L_20, L_21, /*hidden argument*/List_1_Add_mCF93E3DFA395F0F2B4EA20436FE03EF4CFC46FB9_RuntimeMethod_var);
	}

IL_0059:
	{
		// dic.Add(key, objectReferences.IndexOf(unityObject));
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_22 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_23 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_22);
		String_t* L_24 = ___key0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_25 = ___objectReferences3;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_26 = *((List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_25);
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_27 = V_3;
		int32_t L_28;
		L_28 = List_1_IndexOf_m7CEA1AFEC4C2ECD458BDA3E4B1DC9610C9F23499(L_26, L_27, /*hidden argument*/List_1_IndexOf_m7CEA1AFEC4C2ECD458BDA3E4B1DC9610C9F23499_RuntimeMethod_var);
		int32_t L_29 = L_28;
		RuntimeObject * L_30 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_29);
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_23, L_24, L_30, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		goto IL_01de;
	}

IL_0075:
	{
		// else if (typeof(LayerMask).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_31 = { reinterpret_cast<intptr_t> (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32;
		L_32 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_31, /*hidden argument*/NULL);
		Type_t * L_33 = V_1;
		bool L_34;
		L_34 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_32, L_33);
		V_5 = L_34;
		bool L_35 = V_5;
		if (!L_35)
		{
			goto IL_00af;
		}
	}
	{
		// dic.Add(key, ((LayerMask)value).value);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_36 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_37 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_36);
		String_t* L_38 = ___key0;
		RuntimeObject * L_39 = ___value1;
		V_6 = ((*(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 *)((LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 *)UnBox(L_39, LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_il2cpp_TypeInfo_var))));
		int32_t L_40;
		L_40 = LayerMask_get_value_m6380C7449537F99361797225E179A9448A53DDF9((LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 *)(&V_6), /*hidden argument*/NULL);
		int32_t L_41 = L_40;
		RuntimeObject * L_42 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_41);
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_37, L_38, L_42, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		goto IL_01de;
	}

IL_00af:
	{
		// else if (typeof(Enum).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_43 = { reinterpret_cast<intptr_t> (Enum_t23B90B40F60E677A8025267341651C94AE079CDA_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44;
		L_44 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_43, /*hidden argument*/NULL);
		Type_t * L_45 = V_1;
		bool L_46;
		L_46 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_44, L_45);
		V_7 = L_46;
		bool L_47 = V_7;
		if (!L_47)
		{
			goto IL_00db;
		}
	}
	{
		// dic.Add(key, (Enum)value);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_48 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_49 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_48);
		String_t* L_50 = ___key0;
		RuntimeObject * L_51 = ___value1;
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_49, L_50, ((Enum_t23B90B40F60E677A8025267341651C94AE079CDA *)CastclassClass((RuntimeObject*)L_51, Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var)), /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		goto IL_01de;
	}

IL_00db:
	{
		// else if (type.IsPrimitive ||
		//          type == typeof(string) ||
		//          type == typeof(Vector2) ||
		//          type == typeof(Vector3) ||
		//          type == typeof(Vector4) ||
		//          type == typeof(Color))
		Type_t * L_52 = V_1;
		bool L_53;
		L_53 = Type_get_IsPrimitive_m43E50D507C45CE3BD51C0DC07C8AB061AFD6B3C3(L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_54 = V_1;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_55 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_56;
		L_56 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_55, /*hidden argument*/NULL);
		bool L_57;
		L_57 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_54, L_56, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_58 = V_1;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_59 = { reinterpret_cast<intptr_t> (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_60;
		L_60 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_59, /*hidden argument*/NULL);
		bool L_61;
		L_61 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_58, L_60, /*hidden argument*/NULL);
		if (L_61)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_62 = V_1;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_63 = { reinterpret_cast<intptr_t> (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_64;
		L_64 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_63, /*hidden argument*/NULL);
		bool L_65;
		L_65 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_62, L_64, /*hidden argument*/NULL);
		if (L_65)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_66 = V_1;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_67 = { reinterpret_cast<intptr_t> (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_68;
		L_68 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_67, /*hidden argument*/NULL);
		bool L_69;
		L_69 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_66, L_68, /*hidden argument*/NULL);
		if (L_69)
		{
			goto IL_013d;
		}
	}
	{
		Type_t * L_70 = V_1;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_71 = { reinterpret_cast<intptr_t> (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_72;
		L_72 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_71, /*hidden argument*/NULL);
		bool L_73;
		L_73 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_70, L_72, /*hidden argument*/NULL);
		G_B19_0 = ((int32_t)(L_73));
		goto IL_013e;
	}

IL_013d:
	{
		G_B19_0 = 1;
	}

IL_013e:
	{
		V_8 = (bool)G_B19_0;
		bool L_74 = V_8;
		if (!L_74)
		{
			goto IL_0155;
		}
	}
	{
		// dic.Add(key, value);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_75 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_76 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_75);
		String_t* L_77 = ___key0;
		RuntimeObject * L_78 = ___value1;
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_76, L_77, L_78, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		goto IL_01de;
	}

IL_0155:
	{
		// else if (typeof(IList).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_79 = { reinterpret_cast<intptr_t> (IList_tB15A9D6625D09661D6E47976BB626C703EC81910_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_80;
		L_80 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_79, /*hidden argument*/NULL);
		Type_t * L_81 = V_1;
		bool L_82;
		L_82 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_80, L_81);
		V_9 = L_82;
		bool L_83 = V_9;
		if (!L_83)
		{
			goto IL_01c0;
		}
	}
	{
		// IList list = (IList)value;
		RuntimeObject * L_84 = ___value1;
		V_10 = ((RuntimeObject*)Castclass((RuntimeObject*)L_84, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var));
		// Dictionary<string, object> s = new Dictionary<string, object>();
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_85 = (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)il2cpp_codegen_object_new(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63(L_85, /*hidden argument*/Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		V_11 = L_85;
		// for (int i = 0; i < list.Count; i++)
		V_12 = 0;
		goto IL_01a1;
	}

IL_0180:
	{
		// SerializeValue(i.ToString(), list[i], ref s, ref objectReferences);
		String_t* L_86;
		L_86 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_12), /*hidden argument*/NULL);
		RuntimeObject* L_87 = V_10;
		int32_t L_88 = V_12;
		RuntimeObject * L_89;
		L_89 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var, L_87, L_88);
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_90 = ___objectReferences3;
		GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1(L_86, L_89, (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)(&V_11), (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_90, /*hidden argument*/NULL);
		// for (int i = 0; i < list.Count; i++)
		int32_t L_91 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_91, (int32_t)1));
	}

IL_01a1:
	{
		// for (int i = 0; i < list.Count; i++)
		int32_t L_92 = V_12;
		RuntimeObject* L_93 = V_10;
		int32_t L_94;
		L_94 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var, L_93);
		V_13 = (bool)((((int32_t)L_92) < ((int32_t)L_94))? 1 : 0);
		bool L_95 = V_13;
		if (L_95)
		{
			goto IL_0180;
		}
	}
	{
		// dic.Add(key, s);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_96 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_97 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_96);
		String_t* L_98 = ___key0;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_99 = V_11;
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_97, L_98, L_99, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
		goto IL_01de;
	}

IL_01c0:
	{
		// Dictionary<string, object> data = new Dictionary<string, object>();
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_100 = (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)il2cpp_codegen_object_new(Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63(L_100, /*hidden argument*/Dictionary_2__ctor_mCD0C2F0325B7677B9BC340A60AA3FB9C7A88FF63_RuntimeMethod_var);
		V_14 = L_100;
		// SerializeFields(value, ref data, ref objectReferences);
		RuntimeObject * L_101 = ___value1;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 ** L_102 = ___objectReferences3;
		GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC(L_101, (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)(&V_14), (List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 **)L_102, /*hidden argument*/NULL);
		// dic.Add(key, data);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 ** L_103 = ___dic2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_104 = *((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 **)L_103);
		String_t* L_105 = ___key0;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_106 = V_14;
		Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F(L_104, L_105, L_106, /*hidden argument*/Dictionary_2_Add_m005F33425CDAEC23027518EC759F8F439AF34F3F_RuntimeMethod_var);
	}

IL_01de:
	{
	}

IL_01df:
	{
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::Load(DevionGames.Graphs.Graph)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_Load_m4C2361BA90120FD465B90AC31F423B405C220B9F (Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * ___graph0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m5FB38C219330031C0EB797E49A904F84A284F2D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral800887583098E85FC30F11258ADBB87A8CDBED7E);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * V_4 = NULL;
	int32_t V_5 = 0;
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_6 = NULL;
	bool V_7 = false;
	int32_t V_8 = 0;
	bool V_9 = false;
	{
		// if (string.IsNullOrEmpty(graph.serializationData)){
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_0 = ___graph0;
		String_t* L_1 = L_0->get_serializationData_0();
		bool L_2;
		L_2 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_0016;
		}
	}
	{
		// return;
		goto IL_00d5;
	}

IL_0016:
	{
		// Dictionary<string, object> data = MiniJSON.Deserialize(graph.serializationData) as Dictionary<string, object>;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_4 = ___graph0;
		String_t* L_5 = L_4->get_serializationData_0();
		RuntimeObject * L_6;
		L_6 = MiniJSON_Deserialize_mC78A07565C97B728803C8AE751D7583B7E5A4989(L_5, /*hidden argument*/NULL);
		V_0 = ((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)IsInstClass((RuntimeObject*)L_6, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var));
		// graph.nodes.Clear();
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_7 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_8 = L_7->get_nodes_2();
		List_1_Clear_m5FB38C219330031C0EB797E49A904F84A284F2D3(L_8, /*hidden argument*/List_1_Clear_m5FB38C219330031C0EB797E49A904F84A284F2D3_RuntimeMethod_var);
		// if (data.TryGetValue("Nodes", out obj))
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_9 = V_0;
		bool L_10;
		L_10 = Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06(L_9, _stringLiteral800887583098E85FC30F11258ADBB87A8CDBED7E, (RuntimeObject **)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06_RuntimeMethod_var);
		V_3 = L_10;
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_00d5;
		}
	}
	{
		// List<object> list = obj as List<object>;
		RuntimeObject * L_12 = V_1;
		V_4 = ((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)IsInstClass((RuntimeObject*)L_12, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_il2cpp_TypeInfo_var));
		// for (int i = 0; i < list.Count; i++)
		V_5 = 0;
		goto IL_008e;
	}

IL_0055:
	{
		// Node node = DeserializeNode(list[i] as Dictionary<string, object>, graph.serializedObjects);
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_13 = V_4;
		int32_t L_14 = V_5;
		RuntimeObject * L_15;
		L_15 = List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_inline(L_13, L_14, /*hidden argument*/List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_RuntimeMethod_var);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_16 = ___graph0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_17 = L_16->get_serializedObjects_1();
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_18;
		L_18 = GraphUtility_DeserializeNode_m6E75890D459E8DD137AE264D5FA32E13BEC40570(((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)IsInstClass((RuntimeObject*)L_15, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var)), L_17, /*hidden argument*/NULL);
		V_6 = L_18;
		// node.graph = graph;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_19 = V_6;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_20 = ___graph0;
		L_19->set_graph_3(L_20);
		// graph.nodes.Add(node);
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_21 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_22 = L_21->get_nodes_2();
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_23 = V_6;
		List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC(L_22, L_23, /*hidden argument*/List_1_Add_m74FBCDFCF16ED5926AC5BD2922AA64A4DE4DE4AC_RuntimeMethod_var);
		// for (int i = 0; i < list.Count; i++)
		int32_t L_24 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_008e:
	{
		// for (int i = 0; i < list.Count; i++)
		int32_t L_25 = V_5;
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_26 = V_4;
		int32_t L_27;
		L_27 = List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_inline(L_26, /*hidden argument*/List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_RuntimeMethod_var);
		V_7 = (bool)((((int32_t)L_25) < ((int32_t)L_27))? 1 : 0);
		bool L_28 = V_7;
		if (L_28)
		{
			goto IL_0055;
		}
	}
	{
		// for (int i = 0; i < graph.nodes.Count; i++) {
		V_8 = 0;
		goto IL_00bf;
	}

IL_00a4:
	{
		// graph.nodes[i].OnAfterDeserialize();
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_29 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_30 = L_29->get_nodes_2();
		int32_t L_31 = V_8;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_32;
		L_32 = List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_inline(L_30, L_31, /*hidden argument*/List_1_get_Item_m1E7FDEED80B12334D718869B3FFCEEB395AEC128_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(4 /* System.Void DevionGames.Graphs.Node::OnAfterDeserialize() */, L_32);
		// for (int i = 0; i < graph.nodes.Count; i++) {
		int32_t L_33 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00bf:
	{
		// for (int i = 0; i < graph.nodes.Count; i++) {
		int32_t L_34 = V_8;
		Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9 * L_35 = ___graph0;
		List_1_t2EE7EABC0B15D7981C1D0D3FF71731527CF3D5EB * L_36 = L_35->get_nodes_2();
		int32_t L_37;
		L_37 = List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_inline(L_36, /*hidden argument*/List_1_get_Count_m86EB29CE40B055F78335D3A8DAD8694AC60A046C_RuntimeMethod_var);
		V_9 = (bool)((((int32_t)L_34) < ((int32_t)L_37))? 1 : 0);
		bool L_38 = V_9;
		if (L_38)
		{
			goto IL_00a4;
		}
	}
	{
	}

IL_00d5:
	{
		// }
		return;
	}
}
// DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::DeserializeNode(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * GraphUtility_DeserializeNode_m6E75890D459E8DD137AE264D5FA32E13BEC40570 (Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data0, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA15CF7E1CEFBD0C475E3A89A80B5393D417F8634);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Type_t * V_1 = NULL;
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_2 = NULL;
	bool V_3 = false;
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * V_4 = NULL;
	int32_t G_B3_0 = 0;
	{
		// string typeString = (string)data["Type"];
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_0 = ___data0;
		RuntimeObject * L_1;
		L_1 = Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D(L_0, _stringLiteralA15CF7E1CEFBD0C475E3A89A80B5393D417F8634, /*hidden argument*/Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D_RuntimeMethod_var);
		V_0 = ((String_t*)CastclassSealed((RuntimeObject*)L_1, String_t_il2cpp_TypeInfo_var));
		// Type type = Utility.GetType(typeString);
		String_t* L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// if (type == null && !string.IsNullOrEmpty(typeString))
		Type_t * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_4, (Type_t *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		String_t* L_6 = V_0;
		bool L_7;
		L_7 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_6, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_7) == ((int32_t)0))? 1 : 0);
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		V_3 = (bool)G_B3_0;
		bool L_8 = V_3;
		if (!L_8)
		{
			goto IL_003b;
		}
	}
	{
		// type = Utility.GetType(typeString);
		String_t* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_10;
		L_10 = Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
	}

IL_003b:
	{
		// Node node = (Node)System.Activator.CreateInstance(type);
		Type_t * L_11 = V_1;
		RuntimeObject * L_12;
		L_12 = Activator_CreateInstance_m1BACAB5F4FBF138CCCB537DDCB0683A2AC064295(L_11, /*hidden argument*/NULL);
		V_2 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)CastclassClass((RuntimeObject*)L_12, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_il2cpp_TypeInfo_var));
		// DeserializeFields(node, data, objectReferences);
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_13 = V_2;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_14 = ___data0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_15 = ___objectReferences1;
		GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48(L_13, L_14, L_15, /*hidden argument*/NULL);
		// return node;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_16 = V_2;
		V_4 = L_16;
		goto IL_0055;
	}

IL_0055:
	{
		// }
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_17 = V_4;
		return L_17;
	}
}
// System.Void DevionGames.Graphs.GraphUtility::DeserializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48 (RuntimeObject * ___source0, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data1, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	FieldInfo_t * V_4 = NULL;
	RuntimeObject * V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	{
		// if (source == null){return;}
		RuntimeObject * L_0 = ___source0;
		V_2 = (bool)((((RuntimeObject*)(RuntimeObject *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_2;
		if (!L_1)
		{
			goto IL_000c;
		}
	}
	{
		// if (source == null){return;}
		goto IL_0067;
	}

IL_000c:
	{
		// Type type = source.GetType();
		RuntimeObject * L_2 = ___source0;
		Type_t * L_3;
		L_3 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// FieldInfo[] fields = type.GetAllSerializedFields();
		Type_t * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_5;
		L_5 = Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// for (int j = 0; j < fields.Length; j++)
		V_3 = 0;
		goto IL_005b;
	}

IL_001e:
	{
		// FieldInfo field = fields[j];
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_6 = V_1;
		int32_t L_7 = V_3;
		int32_t L_8 = L_7;
		FieldInfo_t * L_9 = (L_6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_8));
		V_4 = L_9;
		// object value = DeserializeValue(field.Name, source, field, field.FieldType, data, objectReferences);
		FieldInfo_t * L_10 = V_4;
		String_t* L_11;
		L_11 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_10);
		RuntimeObject * L_12 = ___source0;
		FieldInfo_t * L_13 = V_4;
		FieldInfo_t * L_14 = V_4;
		Type_t * L_15;
		L_15 = VirtFuncInvoker0< Type_t * >::Invoke(18 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_14);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_16 = ___data1;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_17 = ___objectReferences2;
		RuntimeObject * L_18;
		L_18 = GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD(L_11, L_12, L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		// if (value != null)
		RuntimeObject * L_19 = V_5;
		V_6 = (bool)((!(((RuntimeObject*)(RuntimeObject *)L_19) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_20 = V_6;
		if (!L_20)
		{
			goto IL_0056;
		}
	}
	{
		// field.SetValue(source, value);
		FieldInfo_t * L_21 = V_4;
		RuntimeObject * L_22 = ___source0;
		RuntimeObject * L_23 = V_5;
		FieldInfo_SetValue_mA1EFB5DA5E4B930A617744E29E909FE9DEAA663C(L_21, L_22, L_23, /*hidden argument*/NULL);
	}

IL_0056:
	{
		// for (int j = 0; j < fields.Length; j++)
		int32_t L_24 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_005b:
	{
		// for (int j = 0; j < fields.Length; j++)
		int32_t L_25 = V_3;
		FieldInfoU5BU5D_tD84513FCA07C63AAFE671A5B39E3ADD6E903938E* L_26 = V_1;
		V_7 = (bool)((((int32_t)L_25) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_26)->max_length)))))? 1 : 0);
		bool L_27 = V_7;
		if (L_27)
		{
			goto IL_001e;
		}
	}

IL_0067:
	{
		// }
		return;
	}
}
// System.Object DevionGames.Graphs.GraphUtility::DeserializeValue(System.String,System.Object,System.Reflection.FieldInfo,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD (String_t* ___key0, RuntimeObject * ___source1, FieldInfo_t * ___field2, Type_t * ___type3, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * ___data4, List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * ___objectReferences5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Count_m41D28F48221F99DB3016C0F5CF4B210291338546_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB894CE9F78F9B3D56F6EC500CB45DDA05137AF52_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m5609620CF6DC82315623CF4C9227475D860679E2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t2F377D93C74B8090B226DCC307AB5BB600181453_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4EA2948BD4FABD44A63F69C545E6C11D9746FFC5);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	bool V_4 = false;
	RuntimeObject * V_5 = NULL;
	bool V_6 = false;
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  V_7;
	memset((&V_7), 0, sizeof(V_7));
	bool V_8 = false;
	bool V_9 = false;
	bool V_10 = false;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_11 = NULL;
	Type_t * V_12 = NULL;
	RuntimeObject* V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	Type_t * V_16 = NULL;
	bool V_17 = false;
	bool V_18 = false;
	RuntimeArray * V_19 = NULL;
	Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * V_20 = NULL;
	RuntimeObject * V_21 = NULL;
	bool V_22 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B20_0 = 0;
	{
		// if (data.TryGetValue(key, out value))
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_0 = ___data4;
		String_t* L_1 = ___key0;
		bool L_2;
		L_2 = Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06(L_0, L_1, (RuntimeObject **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_mAD5155E7BDF9B0B146468C59E19A3190C07D1D06_RuntimeMethod_var);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0262;
		}
	}
	{
		// if (typeof(UnityEngine.Object).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_4 = { reinterpret_cast<intptr_t> (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5;
		L_5 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = ___type3;
		bool L_7;
		L_7 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_5, L_6);
		V_2 = L_7;
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		// int index = System.Convert.ToInt32(value);
		RuntimeObject * L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		int32_t L_10;
		L_10 = Convert_ToInt32_mFFEDED67681E3EC8705BCA890BBC206514431B4A(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		// if (index >= 0 && index < objectReferences.Count)
		int32_t L_11 = V_3;
		if ((((int32_t)L_11) < ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_12 = V_3;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_13 = ___objectReferences5;
		int32_t L_14;
		L_14 = List_1_get_Count_mB894CE9F78F9B3D56F6EC500CB45DDA05137AF52_inline(L_13, /*hidden argument*/List_1_get_Count_mB894CE9F78F9B3D56F6EC500CB45DDA05137AF52_RuntimeMethod_var);
		G_B5_0 = ((((int32_t)L_12) < ((int32_t)L_14))? 1 : 0);
		goto IL_0040;
	}

IL_003f:
	{
		G_B5_0 = 0;
	}

IL_0040:
	{
		V_4 = (bool)G_B5_0;
		bool L_15 = V_4;
		if (!L_15)
		{
			goto IL_0056;
		}
	}
	{
		// return objectReferences[index];
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_16 = ___objectReferences5;
		int32_t L_17 = V_3;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_18;
		L_18 = List_1_get_Item_m5609620CF6DC82315623CF4C9227475D860679E2_inline(L_16, L_17, /*hidden argument*/List_1_get_Item_m5609620CF6DC82315623CF4C9227475D860679E2_RuntimeMethod_var);
		V_5 = L_18;
		goto IL_0267;
	}

IL_0056:
	{
		goto IL_0261;
	}

IL_005c:
	{
		// else if (typeof(LayerMask) == type)
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_19 = { reinterpret_cast<intptr_t> (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20;
		L_20 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_19, /*hidden argument*/NULL);
		Type_t * L_21 = ___type3;
		bool L_22;
		L_22 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_20, L_21, /*hidden argument*/NULL);
		V_6 = L_22;
		bool L_23 = V_6;
		if (!L_23)
		{
			goto IL_0097;
		}
	}
	{
		// LayerMask mask = new LayerMask();
		il2cpp_codegen_initobj((&V_7), sizeof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 ));
		// mask.value = (int)value;
		RuntimeObject * L_24 = V_0;
		LayerMask_set_value_mE825B6131A75814FCF2EA32ECBE2A205E6531585((LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 *)(&V_7), ((*(int32_t*)((int32_t*)UnBox(L_24, Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		// return mask;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_25 = V_7;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_26 = L_25;
		RuntimeObject * L_27 = Box(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8_il2cpp_TypeInfo_var, &L_26);
		V_5 = L_27;
		goto IL_0267;
	}

IL_0097:
	{
		// else if (typeof(Enum).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_28 = { reinterpret_cast<intptr_t> (Enum_t23B90B40F60E677A8025267341651C94AE079CDA_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_29;
		L_29 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_28, /*hidden argument*/NULL);
		Type_t * L_30 = ___type3;
		bool L_31;
		L_31 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_29, L_30);
		V_8 = L_31;
		bool L_32 = V_8;
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		// return Enum.Parse(type, (string)value);
		Type_t * L_33 = ___type3;
		RuntimeObject * L_34 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		RuntimeObject * L_35;
		L_35 = Enum_Parse_m6601224637A9CF40F77358805956C2EE757EAF68(L_33, ((String_t*)CastclassSealed((RuntimeObject*)L_34, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_5 = L_35;
		goto IL_0267;
	}

IL_00c1:
	{
		// else if (type.IsPrimitive ||
		//          type == typeof(string) ||
		//          type == typeof(Vector2) ||
		//          type == typeof(Vector3) ||
		//          type == typeof(Vector4) ||
		//          type == typeof(Quaternion) ||
		//          type == typeof(Color))
		Type_t * L_36 = ___type3;
		bool L_37;
		L_37 = Type_get_IsPrimitive_m43E50D507C45CE3BD51C0DC07C8AB061AFD6B3C3(L_36, /*hidden argument*/NULL);
		if (L_37)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_38 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_39 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_40;
		L_40 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_39, /*hidden argument*/NULL);
		bool L_41;
		L_41 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_38, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_42 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_43 = { reinterpret_cast<intptr_t> (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44;
		L_44 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_43, /*hidden argument*/NULL);
		bool L_45;
		L_45 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_42, L_44, /*hidden argument*/NULL);
		if (L_45)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_46 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_47 = { reinterpret_cast<intptr_t> (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48;
		L_48 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_47, /*hidden argument*/NULL);
		bool L_49;
		L_49 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_46, L_48, /*hidden argument*/NULL);
		if (L_49)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_50 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_51 = { reinterpret_cast<intptr_t> (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_52;
		L_52 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_51, /*hidden argument*/NULL);
		bool L_53;
		L_53 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_50, L_52, /*hidden argument*/NULL);
		if (L_53)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_54 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_55 = { reinterpret_cast<intptr_t> (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_56;
		L_56 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_55, /*hidden argument*/NULL);
		bool L_57;
		L_57 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_54, L_56, /*hidden argument*/NULL);
		if (L_57)
		{
			goto IL_0135;
		}
	}
	{
		Type_t * L_58 = ___type3;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_59 = { reinterpret_cast<intptr_t> (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_60;
		L_60 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_59, /*hidden argument*/NULL);
		bool L_61;
		L_61 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_58, L_60, /*hidden argument*/NULL);
		G_B20_0 = ((int32_t)(L_61));
		goto IL_0136;
	}

IL_0135:
	{
		G_B20_0 = 1;
	}

IL_0136:
	{
		V_9 = (bool)G_B20_0;
		bool L_62 = V_9;
		if (!L_62)
		{
			goto IL_0145;
		}
	}
	{
		// return value;
		RuntimeObject * L_63 = V_0;
		V_5 = L_63;
		goto IL_0267;
	}

IL_0145:
	{
		// else if (typeof(IList).IsAssignableFrom(type))
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_64 = { reinterpret_cast<intptr_t> (IList_tB15A9D6625D09661D6E47976BB626C703EC81910_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_65;
		L_65 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_64, /*hidden argument*/NULL);
		Type_t * L_66 = ___type3;
		bool L_67;
		L_67 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_65, L_66);
		V_10 = L_67;
		bool L_68 = V_10;
		if (!L_68)
		{
			goto IL_020e;
		}
	}
	{
		// Dictionary<string, object> dic = value as Dictionary<string, object>;
		RuntimeObject * L_69 = V_0;
		V_11 = ((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)IsInstClass((RuntimeObject*)L_69, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var));
		// Type targetType = typeof(List<>).MakeGenericType(Utility.GetElementType(type));
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_70 = { reinterpret_cast<intptr_t> (List_1_t2F377D93C74B8090B226DCC307AB5BB600181453_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_71;
		L_71 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_70, /*hidden argument*/NULL);
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_72 = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_73 = L_72;
		Type_t * L_74 = ___type3;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_75;
		L_75 = Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B(L_74, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_73, L_75);
		(L_73)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_75);
		Type_t * L_76;
		L_76 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* >::Invoke(96 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_71, L_73);
		V_12 = L_76;
		// IList result = (IList)Activator.CreateInstance(targetType);
		Type_t * L_77 = V_12;
		RuntimeObject * L_78;
		L_78 = Activator_CreateInstance_m1BACAB5F4FBF138CCCB537DDCB0683A2AC064295(L_77, /*hidden argument*/NULL);
		V_13 = ((RuntimeObject*)Castclass((RuntimeObject*)L_78, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var));
		// int count = dic.Count;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_79 = V_11;
		int32_t L_80;
		L_80 = Dictionary_2_get_Count_m41D28F48221F99DB3016C0F5CF4B210291338546(L_79, /*hidden argument*/Dictionary_2_get_Count_m41D28F48221F99DB3016C0F5CF4B210291338546_RuntimeMethod_var);
		V_14 = L_80;
		// for (int i = 0; i < count; i++) {
		V_15 = 0;
		goto IL_01cf;
	}

IL_01a3:
	{
		// Type elementType = Utility.GetElementType(type);
		Type_t * L_81 = ___type3;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_82;
		L_82 = Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B(L_81, /*hidden argument*/NULL);
		V_16 = L_82;
		// result.Add(DeserializeValue(i.ToString(), source, field, elementType, dic, objectReferences));
		RuntimeObject* L_83 = V_13;
		String_t* L_84;
		L_84 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_15), /*hidden argument*/NULL);
		RuntimeObject * L_85 = ___source1;
		FieldInfo_t * L_86 = ___field2;
		Type_t * L_87 = V_16;
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_88 = V_11;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_89 = ___objectReferences5;
		RuntimeObject * L_90;
		L_90 = GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD(L_84, L_85, L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		int32_t L_91;
		L_91 = InterfaceFuncInvoker1< int32_t, RuntimeObject * >::Invoke(2 /* System.Int32 System.Collections.IList::Add(System.Object) */, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var, L_83, L_90);
		// for (int i = 0; i < count; i++) {
		int32_t L_92 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_92, (int32_t)1));
	}

IL_01cf:
	{
		// for (int i = 0; i < count; i++) {
		int32_t L_93 = V_15;
		int32_t L_94 = V_14;
		V_17 = (bool)((((int32_t)L_93) < ((int32_t)L_94))? 1 : 0);
		bool L_95 = V_17;
		if (L_95)
		{
			goto IL_01a3;
		}
	}
	{
		// if (type.IsArray)
		Type_t * L_96 = ___type3;
		bool L_97;
		L_97 = Type_get_IsArray_m15FE83DD8FAF090F9BDA924753C7750AAEA7CFD1(L_96, /*hidden argument*/NULL);
		V_18 = L_97;
		bool L_98 = V_18;
		if (!L_98)
		{
			goto IL_0208;
		}
	}
	{
		// Array array = Array.CreateInstance(Utility.GetElementType(type), count);
		Type_t * L_99 = ___type3;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_100;
		L_100 = Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B(L_99, /*hidden argument*/NULL);
		int32_t L_101 = V_14;
		RuntimeArray * L_102;
		L_102 = Array_CreateInstance_m57AC02F4475AF70CA317B48F09B3C29E3BA9C046(L_100, L_101, /*hidden argument*/NULL);
		V_19 = L_102;
		// result.CopyTo(array, 0);
		RuntimeObject* L_103 = V_13;
		RuntimeArray * L_104 = V_19;
		InterfaceActionInvoker2< RuntimeArray *, int32_t >::Invoke(0 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var, L_103, L_104, 0);
		// return array;
		RuntimeArray * L_105 = V_19;
		V_5 = L_105;
		goto IL_0267;
	}

IL_0208:
	{
		// return result;
		RuntimeObject* L_106 = V_13;
		V_5 = L_106;
		goto IL_0267;
	}

IL_020e:
	{
		// Dictionary<string,object> dic= value as Dictionary<string,object>;
		RuntimeObject * L_107 = V_0;
		V_20 = ((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)IsInstClass((RuntimeObject*)L_107, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var));
		// if (dic.ContainsKey("m_Type")) {
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_108 = V_20;
		bool L_109;
		L_109 = Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710(L_108, _stringLiteral4EA2948BD4FABD44A63F69C545E6C11D9746FFC5, /*hidden argument*/Dictionary_2_ContainsKey_m660B1C18318BE8EEC0B242140281274407F20710_RuntimeMethod_var);
		V_22 = L_109;
		bool L_110 = V_22;
		if (!L_110)
		{
			goto IL_0243;
		}
	}
	{
		// type = Utility.GetType((string)dic["m_Type"]);
		Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 * L_111 = V_20;
		RuntimeObject * L_112;
		L_112 = Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D(L_111, _stringLiteral4EA2948BD4FABD44A63F69C545E6C11D9746FFC5, /*hidden argument*/Dictionary_2_get_Item_m88AA4580D695AEA212B0DF17D8B55C98CF3B624D_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_113;
		L_113 = Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC(((String_t*)CastclassSealed((RuntimeObject*)L_112, String_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		___type3 = L_113;
	}

IL_0243:
	{
		// object instance = Activator.CreateInstance(type);
		Type_t * L_114 = ___type3;
		RuntimeObject * L_115;
		L_115 = Activator_CreateInstance_m1BACAB5F4FBF138CCCB537DDCB0683A2AC064295(L_114, /*hidden argument*/NULL);
		V_21 = L_115;
		// DeserializeFields(instance, value as Dictionary<string, object>, objectReferences);
		RuntimeObject * L_116 = V_21;
		RuntimeObject * L_117 = V_0;
		List_1_t9D216521E6A213FF8562D215598D336ABB5474F4 * L_118 = ___objectReferences5;
		GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48(L_116, ((Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399 *)IsInstClass((RuntimeObject*)L_117, Dictionary_2_t692011309BA94F599C6042A381FC9F8B3CB08399_il2cpp_TypeInfo_var)), L_118, /*hidden argument*/NULL);
		// return instance;
		RuntimeObject * L_119 = V_21;
		V_5 = L_119;
		goto IL_0267;
	}

IL_0261:
	{
	}

IL_0262:
	{
		// return null;
		V_5 = NULL;
		goto IL_0267;
	}

IL_0267:
	{
		// }
		RuntimeObject * L_120 = V_5;
		return L_120;
	}
}
// System.Object DevionGames.Graphs.GraphUtility::ConvertToArray(System.Collections.IList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC (RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	RuntimeObject * V_6 = NULL;
	int32_t G_B3_0 = 0;
	Type_t * G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	Type_t * G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	String_t* G_B10_0 = NULL;
	String_t* G_B10_1 = NULL;
	{
		// if (collection.GetType().IsGenericType && collection.GetType().GetGenericArguments().Length == 0)
		RuntimeObject* L_0 = ___collection0;
		Type_t * L_1;
		L_1 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_0, /*hidden argument*/NULL);
		bool L_2;
		L_2 = VirtFuncInvoker0< bool >::Invoke(76 /* System.Boolean System.Type::get_IsGenericType() */, L_1);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		RuntimeObject* L_3 = ___collection0;
		Type_t * L_4;
		L_4 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_3, /*hidden argument*/NULL);
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_5;
		L_5 = VirtFuncInvoker0< TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, L_4);
		G_B3_0 = ((((int32_t)(((RuntimeArray*)L_5)->max_length)) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B3_0 = 0;
	}

IL_0020:
	{
		V_2 = (bool)G_B3_0;
		bool L_6 = V_2;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		// type = collection.GetType().GetGenericArguments()[0];
		RuntimeObject* L_7 = ___collection0;
		Type_t * L_8;
		L_8 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_7, /*hidden argument*/NULL);
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_9;
		L_9 = VirtFuncInvoker0< TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* >::Invoke(100 /* System.Type[] System.Type::GetGenericArguments() */, L_8);
		int32_t L_10 = 0;
		Type_t * L_11 = (L_9)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		V_0 = L_11;
		goto IL_0072;
	}

IL_0034:
	{
		// else if (collection.Count > 0)
		RuntimeObject* L_12 = ___collection0;
		int32_t L_13;
		L_13 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var, L_12);
		V_3 = (bool)((((int32_t)L_13) > ((int32_t)0))? 1 : 0);
		bool L_14 = V_3;
		if (!L_14)
		{
			goto IL_0050;
		}
	}
	{
		// type = collection[0].GetType();
		RuntimeObject* L_15 = ___collection0;
		RuntimeObject * L_16;
		L_16 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var, L_15, 0);
		Type_t * L_17;
		L_17 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		goto IL_0072;
	}

IL_0050:
	{
		// throw new NotSupportedException("Failed to identify collection type for: " + collection.GetType());
		RuntimeObject* L_18 = ___collection0;
		Type_t * L_19;
		L_19 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_18, /*hidden argument*/NULL);
		Type_t * L_20 = L_19;
		G_B8_0 = L_20;
		G_B8_1 = ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral41D96974DD8D9233DD07BA405DF8A3018998A84C));
		if (L_20)
		{
			G_B9_0 = L_20;
			G_B9_1 = ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral41D96974DD8D9233DD07BA405DF8A3018998A84C));
			goto IL_0062;
		}
	}
	{
		G_B10_0 = ((String_t*)(NULL));
		G_B10_1 = G_B8_1;
		goto IL_0067;
	}

IL_0062:
	{
		String_t* L_21;
		L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, G_B9_0);
		G_B10_0 = L_21;
		G_B10_1 = G_B9_1;
	}

IL_0067:
	{
		String_t* L_22;
		L_22 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(G_B10_1, G_B10_0, /*hidden argument*/NULL);
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_23 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m40BC57BDA6E0E119B73700CC809A14B57DC65A90(L_23, L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC_RuntimeMethod_var)));
	}

IL_0072:
	{
		// var array = (object[])Array.CreateInstance(type, collection.Count);
		Type_t * L_24 = V_0;
		RuntimeObject* L_25 = ___collection0;
		int32_t L_26;
		L_26 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var, L_25);
		RuntimeArray * L_27;
		L_27 = Array_CreateInstance_m57AC02F4475AF70CA317B48F09B3C29E3BA9C046(L_24, L_26, /*hidden argument*/NULL);
		V_1 = ((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)Castclass((RuntimeObject*)L_27, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var));
		// for (int i = 0; i < array.Length; ++i)
		V_4 = 0;
		goto IL_009b;
	}

IL_0089:
	{
		// array[i] = collection[i];
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_28 = V_1;
		int32_t L_29 = V_4;
		RuntimeObject* L_30 = ___collection0;
		int32_t L_31 = V_4;
		RuntimeObject * L_32;
		L_32 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var, L_30, L_31);
		ArrayElementTypeCheck (L_28, L_32);
		(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_29), (RuntimeObject *)L_32);
		// for (int i = 0; i < array.Length; ++i)
		int32_t L_33 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_009b:
	{
		// for (int i = 0; i < array.Length; ++i)
		int32_t L_34 = V_4;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_35 = V_1;
		V_5 = (bool)((((int32_t)L_34) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_35)->max_length)))))? 1 : 0);
		bool L_36 = V_5;
		if (L_36)
		{
			goto IL_0089;
		}
	}
	{
		// return array;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_37 = V_1;
		V_6 = (RuntimeObject *)L_37;
		goto IL_00ad;
	}

IL_00ad:
	{
		// }
		RuntimeObject * L_38 = V_6;
		return L_38;
	}
}
// System.Object DevionGames.Graphs.GraphUtility::ConvertToArray(System.Collections.IList,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GraphUtility_ConvertToArray_m21F8FF4A9222C371AFE2EBDCB72E07E8D2A3C4DF (RuntimeObject* ___collection0, Type_t * ___arrayType1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	RuntimeObject * V_5 = NULL;
	{
		// var array = (object[])Array.CreateInstance(arrayType, collection.Count);
		Type_t * L_0 = ___arrayType1;
		RuntimeObject* L_1 = ___collection0;
		int32_t L_2;
		L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 System.Collections.ICollection::get_Count() */, ICollection_tC1E1DED86C0A66845675392606B302452210D5DA_il2cpp_TypeInfo_var, L_1);
		RuntimeArray * L_3;
		L_3 = Array_CreateInstance_m57AC02F4475AF70CA317B48F09B3C29E3BA9C046(L_0, L_2, /*hidden argument*/NULL);
		V_0 = ((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)Castclass((RuntimeObject*)L_3, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var));
		// for (int i = 0; i < array.Length; ++i)
		V_1 = 0;
		goto IL_003f;
	}

IL_0017:
	{
		// var obj = collection[i];
		RuntimeObject* L_4 = ___collection0;
		int32_t L_5 = V_1;
		RuntimeObject * L_6;
		L_6 = InterfaceFuncInvoker1< RuntimeObject *, int32_t >::Invoke(0 /* System.Object System.Collections.IList::get_Item(System.Int32) */, IList_tB15A9D6625D09661D6E47976BB626C703EC81910_il2cpp_TypeInfo_var, L_4, L_5);
		V_2 = L_6;
		// if (!arrayType.IsInstanceOfType(obj))
		Type_t * L_7 = ___arrayType1;
		RuntimeObject * L_8 = V_2;
		bool L_9;
		L_9 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(110 /* System.Boolean System.Type::IsInstanceOfType(System.Object) */, L_7, L_8);
		V_3 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_0036;
		}
	}
	{
		// obj = Convert.ChangeType(obj, arrayType);
		RuntimeObject * L_11 = V_2;
		Type_t * L_12 = ___arrayType1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_tDA947A979C1DAB4F09C461FAFD94FE194743A671_il2cpp_TypeInfo_var);
		RuntimeObject * L_13;
		L_13 = Convert_ChangeType_mD726EC15920319382D858ECD7FD78339110D7FD4(L_11, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
	}

IL_0036:
	{
		// array[i] = obj;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_14 = V_0;
		int32_t L_15 = V_1;
		RuntimeObject * L_16 = V_2;
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15), (RuntimeObject *)L_16);
		// for (int i = 0; i < array.Length; ++i)
		int32_t L_17 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_003f:
	{
		// for (int i = 0; i < array.Length; ++i)
		int32_t L_18 = V_1;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = V_0;
		V_4 = (bool)((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length)))))? 1 : 0);
		bool L_20 = V_4;
		if (L_20)
		{
			goto IL_0017;
		}
	}
	{
		// return array;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = V_0;
		V_5 = (RuntimeObject *)L_21;
		goto IL_0050;
	}

IL_0050:
	{
		// }
		RuntimeObject * L_22 = V_5;
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.InputAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_mDAD5BDE5412881B68D4BC6F98B4D321854C431AC (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * __this, const RuntimeMethod* method)
{
	{
		// public readonly bool label = true;
		__this->set_label_0((bool)1);
		// public readonly bool port = true;
		__this->set_port_1((bool)1);
		// public InputAttribute() { }
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		// public InputAttribute() { }
		return;
	}
}
// System.Void DevionGames.Graphs.InputAttribute::.ctor(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * __this, bool ___label0, bool ___port1, const RuntimeMethod* method)
{
	{
		// public readonly bool label = true;
		__this->set_label_0((bool)1);
		// public readonly bool port = true;
		__this->set_port_1((bool)1);
		// public InputAttribute(bool label, bool port) {
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		// this.label = label;
		bool L_0 = ___label0;
		__this->set_label_0(L_0);
		// this.port = port;
		bool L_1 = ___port1;
		__this->set_port_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Multiply::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Multiply_OnRequestValue_m12B9F16BB14F1A5F24A370B56310EBCF276ACF97 (Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return GetInputValue("a", a) * GetInputValue("b", b);
		float L_0 = __this->get_a_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_b_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4 = ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3));
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		V_0 = L_5;
		goto IL_002c;
	}

IL_002c:
	{
		// }
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void DevionGames.Graphs.Multiply::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Multiply__ctor_m4CDABF04EE5DFF6D888B8113BEFD69151B2C7C0C (Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.Node::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Node__ctor_m607E8DB7A61022982F71AE46459C355F082BA030 (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Guid_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// public Node()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// id = System.Guid.NewGuid().ToString();
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0;
		L_0 = Guid_NewGuid_m5BD19325820690ED6ECA31D67BC2CD474DC4FDB0(/*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1;
		L_1 = Guid_ToString_mA3AB7742FB0E04808F580868E82BDEB93187FB75((Guid_t *)(&V_0), /*hidden argument*/NULL);
		__this->set_id_0(L_1);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Node::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Node_OnAfterDeserialize_mEAF1B1B19243148A7CC0169F286508742D16401E (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * __this, const RuntimeMethod* method)
{
	{
		// public virtual void OnAfterDeserialize() { }
		return;
	}
}
// System.Void DevionGames.Graphs.Node::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Node_OnBeforeSerialize_mDD694FF6132DB887E9FD4EF15693528A44F7C707 (Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * __this, const RuntimeMethod* method)
{
	{
		// public virtual void OnBeforeSerialize() { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, bool ___displayHeader0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public NodeStyleAttribute(bool displayHeader) : this(string.Empty, displayHeader, string.Empty)
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		bool L_1 = ___displayHeader0;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, bool ___displayHeader0, String_t* ___category1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public NodeStyleAttribute(bool displayHeader, string category) : this(string.Empty, displayHeader, category)
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		bool L_1 = ___displayHeader0;
		String_t* L_2 = ___category1;
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_mC0692DD36473F4A4FA86CE04E2EB293DE110927B (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, String_t* ___iconPath0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public NodeStyleAttribute(string iconPath):this(iconPath, true, string.Empty) {
		String_t* L_0 = ___iconPath0;
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(__this, L_0, (bool)1, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, String_t* ___iconPath0, bool ___displayHeader1, String_t* ___category2, const RuntimeMethod* method)
{
	{
		// public NodeStyleAttribute(string iconPath, bool displayHeader, string category)
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		// this.iconPath = iconPath;
		String_t* L_0 = ___iconPath0;
		__this->set_iconPath_0(L_0);
		// this.displayHeader = displayHeader;
		bool L_1 = ___displayHeader1;
		__this->set_displayHeader_1(L_1);
		// this.category = category;
		String_t* L_2 = ___category2;
		__this->set_category_2(L_2);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.OutputAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * __this, const RuntimeMethod* method)
{
	{
		// public OutputAttribute() { }
		Attribute__ctor_m5C1862A7DFC2C25A4797A8C5F681FBB5CB53ECE1(__this, /*hidden argument*/NULL);
		// public OutputAttribute() { }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Type DevionGames.Graphs.Port::get_fieldType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Port_get_fieldType_m9E4E776EAECFC8FF152934C0C586A86AB4B32A16 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Type_t * V_1 = NULL;
	{
		// if (this.m_FieldType == null) {
		Type_t * L_0 = __this->get_m_FieldType_1();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_0, (Type_t *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		// this.m_FieldType = Utility.GetType(this.m_FieldTypeName);
		String_t* L_3 = __this->get_m_FieldTypeName_8();
		IL2CPP_RUNTIME_CLASS_INIT(Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_il2cpp_TypeInfo_var);
		Type_t * L_4;
		L_4 = Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC(L_3, /*hidden argument*/NULL);
		__this->set_m_FieldType_1(L_4);
	}

IL_0024:
	{
		// return this.m_FieldType;
		Type_t * L_5 = __this->get_m_FieldType_1();
		V_1 = L_5;
		goto IL_002d;
	}

IL_002d:
	{
		// }
		Type_t * L_6 = V_1;
		return L_6;
	}
}
// System.Collections.Generic.List`1<DevionGames.Graphs.Edge> DevionGames.Graphs.Port::get_Connections()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method)
{
	List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * V_0 = NULL;
	{
		// return this.m_Connections;
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_0 = __this->get_m_Connections_7();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_1 = V_0;
		return L_1;
	}
}
// System.Void DevionGames.Graphs.Port::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port__ctor_mFE59DCF3A05D932B3E0C947B0FE3AEB15EDAE77A (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool drawPort = true;
		__this->set_drawPort_3((bool)1);
		// public bool label = true;
		__this->set_label_4((bool)1);
		// public PortCapacity capacity = PortCapacity.Single;
		__this->set_capacity_5(0);
		// public PortDirection direction = PortDirection.Input;
		__this->set_direction_6(0);
		// public Port() {
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// m_Connections = new List<Edge>();
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_0 = (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *)il2cpp_codegen_object_new(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_il2cpp_TypeInfo_var);
		List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD(L_0, /*hidden argument*/List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD_RuntimeMethod_var);
		__this->set_m_Connections_7(L_0);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Port::.ctor(DevionGames.Graphs.FlowNode,System.String,System.Type,DevionGames.Graphs.PortCapacity,DevionGames.Graphs.PortDirection)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___node0, String_t* ___fieldName1, Type_t * ___fieldType2, int32_t ___capacity3, int32_t ___direction4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool drawPort = true;
		__this->set_drawPort_3((bool)1);
		// public bool label = true;
		__this->set_label_4((bool)1);
		// public PortCapacity capacity = PortCapacity.Single;
		__this->set_capacity_5(0);
		// public PortDirection direction = PortDirection.Input;
		__this->set_direction_6(0);
		// public Port(FlowNode node,string fieldName, Type fieldType,PortCapacity capacity, PortDirection direction )
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// m_Connections = new List<Edge>();
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_0 = (List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 *)il2cpp_codegen_object_new(List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55_il2cpp_TypeInfo_var);
		List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD(L_0, /*hidden argument*/List_1__ctor_mBDC5FFB97541A2E158BAC4748BEC9319C4FA2AAD_RuntimeMethod_var);
		__this->set_m_Connections_7(L_0);
		// this.node = node;
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_1 = ___node0;
		__this->set_node_0(L_1);
		// this.fieldName = fieldName;
		String_t* L_2 = ___fieldName1;
		__this->set_fieldName_2(L_2);
		// this.capacity = capacity;
		int32_t L_3 = ___capacity3;
		__this->set_capacity_5(L_3);
		// this.direction = direction;
		int32_t L_4 = ___direction4;
		__this->set_direction_6(L_4);
		// this.m_FieldTypeName = fieldType.FullName;
		Type_t * L_5 = ___fieldType2;
		String_t* L_6;
		L_6 = VirtFuncInvoker0< String_t* >::Invoke(25 /* System.String System.Type::get_FullName() */, L_5);
		__this->set_m_FieldTypeName_8(L_6);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Port::Connect(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port_Connect_mA37C1FF40FA49F61D2C5CFA23C3C0402B153E335 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_Connections.Add(new Edge()
		// {
		//     nodeId = port.node.id,
		//     port = port,
		//     fieldName = port.fieldName
		// });
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_0 = __this->get_m_Connections_7();
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_1 = (Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 *)il2cpp_codegen_object_new(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81_il2cpp_TypeInfo_var);
		Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B(L_1, /*hidden argument*/NULL);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_2 = L_1;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_3 = ___port0;
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_4 = L_3->get_node_0();
		String_t* L_5 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_4)->get_id_0();
		L_2->set_nodeId_0(L_5);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_6 = L_2;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_7 = ___port0;
		L_6->set_port_2(L_7);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_8 = L_6;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_9 = ___port0;
		String_t* L_10 = L_9->get_fieldName_2();
		L_8->set_fieldName_1(L_10);
		List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0(L_0, L_8, /*hidden argument*/List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0_RuntimeMethod_var);
		// port.m_Connections.Add(new Edge() {
		//     port = this,
		//     nodeId = node.id,
		//     fieldName = fieldName
		// });
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_11 = ___port0;
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_12 = L_11->get_m_Connections_7();
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_13 = (Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 *)il2cpp_codegen_object_new(Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81_il2cpp_TypeInfo_var);
		Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B(L_13, /*hidden argument*/NULL);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_14 = L_13;
		L_14->set_port_2(__this);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_15 = L_14;
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_16 = __this->get_node_0();
		String_t* L_17 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_16)->get_id_0();
		L_15->set_nodeId_0(L_17);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_18 = L_15;
		String_t* L_19 = __this->get_fieldName_2();
		L_18->set_fieldName_1(L_19);
		List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0(L_12, L_18, /*hidden argument*/List_1_Add_mBDDBFCA3C553FCC4B913B16969D2EB0231F732F0_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Port::Disconnect(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port_Disconnect_m939822E708D2C1C1461AF57D4F764B9EE60AB94E (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_0 = (U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass18_0__ctor_m180DB3EEA83BAF5B3727F12436285AD5EEFA327D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_1 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2 = ___port0;
		L_1->set_port_0(L_2);
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_3 = V_0;
		L_3->set_U3CU3E4__this_1(__this);
		// this.m_Connections.RemoveAll(x => x.nodeId == port.node.id && x.fieldName == port.fieldName);
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_4 = __this->get_m_Connections_7();
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_5 = V_0;
		Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 * L_6 = (Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 *)il2cpp_codegen_object_new(Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var);
		int32_t L_7;
		L_7 = List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE(L_4, L_6, /*hidden argument*/List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var);
		// port.Connections.RemoveAll(x =>x.nodeId == node.id && x.fieldName == fieldName);
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_8 = V_0;
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_9 = L_8->get_port_0();
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_10;
		L_10 = Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2(L_9, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * L_11 = V_0;
		Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 * L_12 = (Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 *)il2cpp_codegen_object_new(Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC(L_12, L_11, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var);
		int32_t L_13;
		L_13 = List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE(L_10, L_12, /*hidden argument*/List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DevionGames.Graphs.Port::DisconnectAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Port_DisconnectAll_m9A9345796CFB57DFE8DAE23B538E0D30E23886F0 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m62F1417B988555AB608ECA0C7DE6B76E2D7960AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * V_1 = NULL;
	bool V_2 = false;
	{
		// for (var i = 0; i < this.m_Connections.Count; i++)
		V_0 = 0;
		goto IL_0035;
	}

IL_0005:
	{
		// var port = this.m_Connections[i].port;
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_0 = __this->get_m_Connections_7();
		int32_t L_1 = V_0;
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_2;
		L_2 = List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_inline(L_0, L_1, /*hidden argument*/List_1_get_Item_m837952AFA6B2FEF650A603EF6229477293009ACC_RuntimeMethod_var);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_3 = L_2->get_port_2();
		V_1 = L_3;
		// port.m_Connections.RemoveAll(x => x.nodeId == node.id && x.fieldName == fieldName);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_4 = V_1;
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_5 = L_4->get_m_Connections_7();
		Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 * L_6 = (Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256 *)il2cpp_codegen_object_new(Predicate_1_tF671C92131F4362B981FF78AC2EFA4C84E6D3256_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC(L_6, __this, (intptr_t)((intptr_t)Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_m314693D9E5B091F1214690643CD1E26416BBD2BC_RuntimeMethod_var);
		int32_t L_7;
		L_7 = List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE(L_5, L_6, /*hidden argument*/List_1_RemoveAll_m93A6BAFD5A844C11BDEECF56C311D65B334289AE_RuntimeMethod_var);
		// for (var i = 0; i < this.m_Connections.Count; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0035:
	{
		// for (var i = 0; i < this.m_Connections.Count; i++)
		int32_t L_9 = V_0;
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_10 = __this->get_m_Connections_7();
		int32_t L_11;
		L_11 = List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_inline(L_10, /*hidden argument*/List_1_get_Count_mED6668CB6CBB0D1B39690EEB8C8EEBA5039A4B4F_RuntimeMethod_var);
		V_2 = (bool)((((int32_t)L_9) < ((int32_t)L_11))? 1 : 0);
		bool L_12 = V_2;
		if (L_12)
		{
			goto IL_0005;
		}
	}
	{
		// this.m_Connections.Clear();
		List_1_t91CF342E6AC8E18CA6B980CBB0D79B323FF67B55 * L_13 = __this->get_m_Connections_7();
		List_1_Clear_m62F1417B988555AB608ECA0C7DE6B76E2D7960AA(L_13, /*hidden argument*/List_1_Clear_m62F1417B988555AB608ECA0C7DE6B76E2D7960AA_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Boolean DevionGames.Graphs.Port::<DisconnectAll>b__19_0(DevionGames.Graphs.Edge)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89 (Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * __this, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___x0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// port.m_Connections.RemoveAll(x => x.nodeId == node.id && x.fieldName == fieldName);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_0 = ___x0;
		String_t* L_1 = L_0->get_nodeId_0();
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_2 = __this->get_node_0();
		String_t* L_3 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_2)->get_id_0();
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_5 = ___x0;
		String_t* L_6 = L_5->get_fieldName_1();
		String_t* L_7 = __this->get_fieldName_2();
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_8));
		goto IL_002c;
	}

IL_002b:
	{
		G_B3_0 = 0;
	}

IL_002c:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Pow::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Pow_OnRequestValue_m918133B94951E54F34D3F3475044283601DDAE86 (Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral198AA065BF0F912BD6F5F93869BD5C361671F98B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2C945D246C2B7897F000E1C591A686EB9EF010F0);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return Mathf.Pow(GetInputValue("f", f),GetInputValue("p",p));
		float L_0 = __this->get_f_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral2C945D246C2B7897F000E1C591A686EB9EF010F0, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_p_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral198AA065BF0F912BD6F5F93869BD5C361671F98B, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4;
		L_4 = powf(L_1, L_3);
		float L_5 = L_4;
		RuntimeObject * L_6 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_5);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		RuntimeObject * L_7 = V_0;
		return L_7;
	}
}
// System.Void DevionGames.Graphs.Pow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Pow__ctor_mBF0D82A1F3793DD04BF9E9B0BCC908CDE3ABE841 (Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Random::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Random_OnRequestValue_m69F5784D76FFADFB0825503946738B2146E59142 (Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return UnityEngine.Random.Range(GetInputValue("a", a), GetInputValue("b", b));
		float L_0 = __this->get_a_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_b_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4;
		L_4 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_1, L_3, /*hidden argument*/NULL);
		float L_5 = L_4;
		RuntimeObject * L_6 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_5);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		// }
		RuntimeObject * L_7 = V_0;
		return L_7;
	}
}
// System.Void DevionGames.Graphs.Random::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random__ctor_m746A033DFACA2417C81D672D6403D559B1808806 (Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7 * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Round::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Round_OnRequestValue_m25DA35698444213736F8C1EE0566D9FF1B259B07 (Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return Mathf.Round(GetInputValue("value", value));
		float L_0 = __this->get_value_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2;
		L_2 = bankers_roundf(L_1);
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		// }
		RuntimeObject * L_5 = V_0;
		return L_5;
	}
}
// System.Void DevionGames.Graphs.Round::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Round__ctor_m9D72E6F59BF85C9DC172786F0D32DAC5C1E4DD4A (Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Sqrt::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Sqrt_OnRequestValue_m69560EBE3810A61BC25E93279618E449EB3626AA (Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return Mathf.Sqrt(GetInputValue("value", value));
		float L_0 = __this->get_value_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral46F273EF641E07D271D91E0DC24A4392582671F8, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2;
		L_2 = sqrtf(L_1);
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		V_0 = L_4;
		goto IL_001f;
	}

IL_001f:
	{
		// }
		RuntimeObject * L_5 = V_0;
		return L_5;
	}
}
// System.Void DevionGames.Graphs.Sqrt::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sqrt__ctor_m059BE1D90DD04C3BD50DF2BFAE67D9782BD02B33 (Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Object DevionGames.Graphs.Subtract::OnRequestValue(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Subtract_OnRequestValue_mC8A574FDD03810050CB5725D51076CA0FA9C5BD9 (Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	{
		// return GetInputValue("a", a) - GetInputValue("b", b);
		float L_0 = __this->get_a_5();
		float L_1;
		L_1 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral0A04B971B03DA607CE6C455184037B660CA89F78, L_0, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_2 = __this->get_b_6();
		float L_3;
		L_3 = FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE(__this, _stringLiteral4609D79FE2FAD95C38B6DA64FC671E8594984D4C, L_2, /*hidden argument*/FlowNode_GetInputValue_TisSingle_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_mB0F444D07F3A43B05E1806D9C1C34F67EEAA07AE_RuntimeMethod_var);
		float L_4 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		V_0 = L_5;
		goto IL_002c;
	}

IL_002c:
	{
		// }
		RuntimeObject * L_6 = V_0;
		return L_6;
	}
}
// System.Void DevionGames.Graphs.Subtract::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Subtract__ctor_mEC0BBD326FE3D99DF2D1BA366D611E0449FDF065 (Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71 * __this, const RuntimeMethod* method)
{
	{
		FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FlowNode/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m167FAA2AF67ACF2FDCA39B9D488EABDCAC42A5CB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * L_0 = (U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 *)il2cpp_codegen_object_new(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m22B16B9EF02763B8D48635AEFF43B1AD3D089375(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void DevionGames.Graphs.FlowNode/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m22B16B9EF02763B8D48635AEFF43B1AD3D089375 (U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.FlowNode/<>c::<get_InputPorts>b__4_0(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12 (U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___x0, const RuntimeMethod* method)
{
	{
		// return m_Ports.Where(x=>x.direction == PortDirection.Input).ToList();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_0 = ___x0;
		int32_t L_1 = L_0->get_direction_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean DevionGames.Graphs.FlowNode/<>c::<get_OutputPorts>b__6_0(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024 (U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___x0, const RuntimeMethod* method)
{
	{
		// return m_Ports.Where(x => x.direction == PortDirection.Output).ToList();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_0 = ___x0;
		int32_t L_1 = L_0->get_direction_6();
		return (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_0__ctor_mA1FED964843DC56317790239CA60118DE619C3E1 (U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::<GetPort>b__0(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2 (U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___port0, const RuntimeMethod* method)
{
	{
		// return m_Ports.Find((port) => port.fieldName == fieldName);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_0 = ___port0;
		String_t* L_1 = L_0->get_fieldName_2();
		String_t* L_2 = __this->get_fieldName_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass15_0__ctor_m676586F14260D951119E7BF393C9CFF81BFAC368 (U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::<OnAfterDeserialize>b__0(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74 (U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x0, const RuntimeMethod* method)
{
	{
		// FlowNode connected = graph.nodes.Find(x => x.id == edge.nodeId) as FlowNode;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_0 = ___x0;
		String_t* L_1 = L_0->get_id_0();
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_2 = __this->get_edge_0();
		String_t* L_3 = L_2->get_nodeId_0();
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::<OnAfterDeserialize>b__1(DevionGames.Graphs.Port)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D (U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680 * __this, Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * ___x0, const RuntimeMethod* method)
{
	{
		// edge.port = connected.Ports.Find(x => x.fieldName == edge.fieldName);
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_0 = ___x0;
		String_t* L_1 = L_0->get_fieldName_2();
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_2 = __this->get_edge_0();
		String_t* L_3 = L_2->get_fieldName_1();
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.Formula/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m61839727659D0A26BEDCB6DD9E672088C93EE319 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * L_0 = (U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F *)il2cpp_codegen_object_new(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m9342E5C7F5E94A50D12343A0C4E500DBAB1264CE(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void DevionGames.Graphs.Formula/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m9342E5C7F5E94A50D12343A0C4E500DBAB1264CE (U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.Formula/<>c::<op_Implicit>b__2_0(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843 (U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// FormulaOutput output = formula.GetGraph().nodes.Find(x => x.GetType() == typeof(FormulaOutput)) as FormulaOutput;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_0 = ___x0;
		Type_t * L_1;
		L_1 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_0, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.FormulaGraph/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m14A0C8539147C4E2766C9207BE1DE8CBEAFD7EF8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * L_0 = (U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C *)il2cpp_codegen_object_new(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mEA18434FBED31CEAABB9A3DE9DD064F7E138682B(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void DevionGames.Graphs.FormulaGraph/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mEA18434FBED31CEAABB9A3DE9DD064F7E138682B (U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.FormulaGraph/<>c::<op_Implicit>b__1_0(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C (U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// FormulaOutput output = graph.nodes.Find(x => x.GetType() == typeof(FormulaOutput)) as FormulaOutput;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_0 = ___x0;
		Type_t * L_1;
		L_1 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_0, /*hidden argument*/NULL);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_mF3C6BAD036536D3343CAD9AAD7F1D00950AD5FBE (U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::<RemoveNodes>b__0(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7 (U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509 * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Any_TisFlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_m409E7D2E1AE678244328F78B52C2BAF3BF8E3B6E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_mC17802B326CCF03C5CDAAC9B94533C3E899F3DDE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * L_0 = (U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_1__ctor_mC51161D4D15A0B5F1E17AE8EC55CF6FC4E3381C4(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * L_1 = V_0;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_2 = ___x0;
		L_1->set_x_0(L_2);
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x ));
		FlowNodeU5BU5D_tC4718D4B7261B8A514CF57896FBFCC433278C7DC* L_3 = __this->get_nodes_0();
		U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * L_4 = V_0;
		Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 * L_5 = (Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5 *)il2cpp_codegen_object_new(Func_2_t0D6CF58981EEA58B047BA0B445C742C05F7B72C5_il2cpp_TypeInfo_var);
		Func_2__ctor_mC17802B326CCF03C5CDAAC9B94533C3E899F3DDE(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mC17802B326CCF03C5CDAAC9B94533C3E899F3DDE_RuntimeMethod_var);
		bool L_6;
		L_6 = Enumerable_Any_TisFlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_m409E7D2E1AE678244328F78B52C2BAF3BF8E3B6E((RuntimeObject*)(RuntimeObject*)L_3, L_5, /*hidden argument*/Enumerable_Any_TisFlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_m409E7D2E1AE678244328F78B52C2BAF3BF8E3B6E_RuntimeMethod_var);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_1__ctor_mC51161D4D15A0B5F1E17AE8EC55CF6FC4E3381C4 (U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::<RemoveNodes>b__1(DevionGames.Graphs.FlowNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E (U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7 * __this, FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * ___y0, const RuntimeMethod* method)
{
	{
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x ));
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_0 = ___y0;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_1 = __this->get_x_0();
		return (bool)((((RuntimeObject*)(FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 *)L_0) == ((RuntimeObject*)(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_1))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m6F79A73FE4250F82E7C386C679CA9A8EFE39B4EF (U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::<RemoveNodes>b__0(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C (U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Any_TisNode_tD0AFD8B27090B24E967D338520DEF077662A61DF_m36DE5710832D7F4C686483656AB39178B531F9FF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m04B7DEF1A5A31A1BEBF83465FB5BBCECE52C7CAE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * L_0 = (U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_1__ctor_m1DF92E78F67367063CE6255316D751DCAA3D76DE(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * L_1 = V_0;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_2 = ___x0;
		L_1->set_x_0(L_2);
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x));
		NodeU5BU5D_tEE515875A6C309E8E939BC1F5A5550AFCE108C49* L_3 = __this->get_nodes_0();
		U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * L_4 = V_0;
		Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED * L_5 = (Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED *)il2cpp_codegen_object_new(Func_2_t5229E18B5FD2A4E9DFF133590FCDEA2259BF78ED_il2cpp_TypeInfo_var);
		Func_2__ctor_m04B7DEF1A5A31A1BEBF83465FB5BBCECE52C7CAE(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m04B7DEF1A5A31A1BEBF83465FB5BBCECE52C7CAE_RuntimeMethod_var);
		bool L_6;
		L_6 = Enumerable_Any_TisNode_tD0AFD8B27090B24E967D338520DEF077662A61DF_m36DE5710832D7F4C686483656AB39178B531F9FF((RuntimeObject*)(RuntimeObject*)L_3, L_5, /*hidden argument*/Enumerable_Any_TisNode_tD0AFD8B27090B24E967D338520DEF077662A61DF_m36DE5710832D7F4C686483656AB39178B531F9FF_RuntimeMethod_var);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_1__ctor_m1DF92E78F67367063CE6255316D751DCAA3D76DE (U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::<RemoveNodes>b__1(DevionGames.Graphs.Node)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E (U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0 * __this, Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * ___y0, const RuntimeMethod* method)
{
	{
		// graph.nodes.RemoveAll(x => nodes.Any(y => y == x));
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_0 = ___y0;
		Node_tD0AFD8B27090B24E967D338520DEF077662A61DF * L_1 = __this->get_x_0();
		return (bool)((((RuntimeObject*)(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_0) == ((RuntimeObject*)(Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_1))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.Graphs.Port/<>c__DisplayClass18_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass18_0__ctor_m180DB3EEA83BAF5B3727F12436285AD5EEFA327D (U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.Graphs.Port/<>c__DisplayClass18_0::<Disconnect>b__0(DevionGames.Graphs.Edge)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9 (U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * __this, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___x0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// this.m_Connections.RemoveAll(x => x.nodeId == port.node.id && x.fieldName == port.fieldName);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_0 = ___x0;
		String_t* L_1 = L_0->get_nodeId_0();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2 = __this->get_port_0();
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_3 = L_2->get_node_0();
		String_t* L_4 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_3)->get_id_0();
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_6 = ___x0;
		String_t* L_7 = L_6->get_fieldName_1();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_8 = __this->get_port_0();
		String_t* L_9 = L_8->get_fieldName_2();
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, L_9, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_10));
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean DevionGames.Graphs.Port/<>c__DisplayClass18_0::<Disconnect>b__1(DevionGames.Graphs.Edge)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0 (U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8 * __this, Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * ___x0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// port.Connections.RemoveAll(x =>x.nodeId == node.id && x.fieldName == fieldName);
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_0 = ___x0;
		String_t* L_1 = L_0->get_nodeId_0();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_2 = __this->get_U3CU3E4__this_1();
		FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195 * L_3 = L_2->get_node_0();
		String_t* L_4 = ((Node_tD0AFD8B27090B24E967D338520DEF077662A61DF *)L_3)->get_id_0();
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Edge_t436E1FC54F7DC812689FC975E8E1E116F0055C81 * L_6 = ___x0;
		String_t* L_7 = L_6->get_fieldName_1();
		Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5 * L_8 = __this->get_U3CU3E4__this_1();
		String_t* L_9 = L_8->get_fieldName_2();
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, L_9, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_10));
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
