﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8;
// DevionGames.CompoundAttribute
struct CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// DevionGames.HeaderLineAttribute
struct HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8;
// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8;
// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677;
// DevionGames.StatSystem.StatPickerAttribute
struct StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.ComponentMenu::m_ComponentMenu
	String_t* ___m_ComponentMenu_0;

public:
	inline static int32_t get_offset_of_m_ComponentMenu_0() { return static_cast<int32_t>(offsetof(ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8, ___m_ComponentMenu_0)); }
	inline String_t* get_m_ComponentMenu_0() const { return ___m_ComponentMenu_0; }
	inline String_t** get_address_of_m_ComponentMenu_0() { return &___m_ComponentMenu_0; }
	inline void set_m_ComponentMenu_0(String_t* value)
	{
		___m_ComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentMenu_0), (void*)value);
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean DevionGames.Graphs.InputAttribute::label
	bool ___label_0;
	// System.Boolean DevionGames.Graphs.InputAttribute::port
	bool ___port_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___label_0)); }
	inline bool get_label_0() const { return ___label_0; }
	inline bool* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(bool value)
	{
		___label_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___port_1)); }
	inline bool get_port_1() const { return ___port_1; }
	inline bool* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(bool value)
	{
		___port_1 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C 
{
public:
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::className
	String_t* ___className_0;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpace
	String_t* ___nameSpace_1;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assembly
	String_t* ___assembly_2;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::classHasChanged
	bool ___classHasChanged_3;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpaceHasChanged
	bool ___nameSpaceHasChanged_4;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assemblyHasChanged
	bool ___assemblyHasChanged_5;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::autoUdpateAPI
	bool ___autoUdpateAPI_6;

public:
	inline static int32_t get_offset_of_className_0() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___className_0)); }
	inline String_t* get_className_0() const { return ___className_0; }
	inline String_t** get_address_of_className_0() { return &___className_0; }
	inline void set_className_0(String_t* value)
	{
		___className_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___className_0), (void*)value);
	}

	inline static int32_t get_offset_of_nameSpace_1() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpace_1)); }
	inline String_t* get_nameSpace_1() const { return ___nameSpace_1; }
	inline String_t** get_address_of_nameSpace_1() { return &___nameSpace_1; }
	inline void set_nameSpace_1(String_t* value)
	{
		___nameSpace_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameSpace_1), (void*)value);
	}

	inline static int32_t get_offset_of_assembly_2() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assembly_2)); }
	inline String_t* get_assembly_2() const { return ___assembly_2; }
	inline String_t** get_address_of_assembly_2() { return &___assembly_2; }
	inline void set_assembly_2(String_t* value)
	{
		___assembly_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assembly_2), (void*)value);
	}

	inline static int32_t get_offset_of_classHasChanged_3() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___classHasChanged_3)); }
	inline bool get_classHasChanged_3() const { return ___classHasChanged_3; }
	inline bool* get_address_of_classHasChanged_3() { return &___classHasChanged_3; }
	inline void set_classHasChanged_3(bool value)
	{
		___classHasChanged_3 = value;
	}

	inline static int32_t get_offset_of_nameSpaceHasChanged_4() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpaceHasChanged_4)); }
	inline bool get_nameSpaceHasChanged_4() const { return ___nameSpaceHasChanged_4; }
	inline bool* get_address_of_nameSpaceHasChanged_4() { return &___nameSpaceHasChanged_4; }
	inline void set_nameSpaceHasChanged_4(bool value)
	{
		___nameSpaceHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_assemblyHasChanged_5() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assemblyHasChanged_5)); }
	inline bool get_assemblyHasChanged_5() const { return ___assemblyHasChanged_5; }
	inline bool* get_address_of_assemblyHasChanged_5() { return &___assemblyHasChanged_5; }
	inline void set_assemblyHasChanged_5(bool value)
	{
		___assemblyHasChanged_5 = value;
	}

	inline static int32_t get_offset_of_autoUdpateAPI_6() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___autoUdpateAPI_6)); }
	inline bool get_autoUdpateAPI_6() const { return ___autoUdpateAPI_6; }
	inline bool* get_address_of_autoUdpateAPI_6() { return &___autoUdpateAPI_6; }
	inline void set_autoUdpateAPI_6(bool value)
	{
		___autoUdpateAPI_6 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_pinvoke
{
	char* ___className_0;
	char* ___nameSpace_1;
	char* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};
// Native definition for COM marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_com
{
	Il2CppChar* ___className_0;
	Il2CppChar* ___nameSpace_1;
	Il2CppChar* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};

// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.CompoundAttribute
struct CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.CompoundAttribute::propertyPath
	String_t* ___propertyPath_0;

public:
	inline static int32_t get_offset_of_propertyPath_0() { return static_cast<int32_t>(offsetof(CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5, ___propertyPath_0)); }
	inline String_t* get_propertyPath_0() const { return ___propertyPath_0; }
	inline String_t** get_address_of_propertyPath_0() { return &___propertyPath_0; }
	inline void set_propertyPath_0(String_t* value)
	{
		___propertyPath_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertyPath_0), (void*)value);
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// DevionGames.HeaderLineAttribute
struct HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.HeaderLineAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.InspectorLabelAttribute::label
	String_t* ___label_0;
	// System.String DevionGames.InspectorLabelAttribute::tooltip
	String_t* ___tooltip_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_tooltip_1() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___tooltip_1)); }
	inline String_t* get_tooltip_1() const { return ___tooltip_1; }
	inline String_t** get_address_of_tooltip_1() { return &___tooltip_1; }
	inline void set_tooltip_1(String_t* value)
	{
		___tooltip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData UnityEngine.Scripting.APIUpdating.MovedFromAttribute::data
	MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8, ___data_0)); }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  get_data_0() const { return ___data_0; }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C * get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___className_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___nameSpace_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___assembly_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DevionGames.StatSystem.StatPickerAttribute
struct StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void DevionGames.ComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3 (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.InputAttribute::.ctor(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * __this, bool ___label0, bool ___port1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.OutputAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.Boolean,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * __this, bool ___autoUpdateAPI0, String_t* ___sourceNamespace1, String_t* ___sourceAssembly2, String_t* ___sourceClassName3, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void DevionGames.HeaderLineAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void DevionGames.CompoundAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4 (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * __this, String_t* ___propertyPath0, const RuntimeMethod* method);
// System.Void DevionGames.StatSystem.StatPickerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57 (StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, String_t* ___tooltip1, const RuntimeMethod* method);
static void DevionGames_StatSystem_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void GetStatCurrentValue_t505491C52AAF3D0DBCF84F6495B473B57C8E5494_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x47\x65\x74\x20\x53\x74\x61\x74\x20\x43\x75\x72\x72\x65\x6E\x74\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void GetStatValue_t8735185F4FD569A1EB0D5E9F984B019D467FCABB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x47\x65\x74\x20\x53\x74\x61\x74\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void StatNode_t3477762477FF89703CE81E81ED6CD780173D58EF_CustomAttributesCacheGenerator_stat(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, true, false, NULL);
	}
}
static void StatNode_t3477762477FF89703CE81E81ED6CD780173D58EF_CustomAttributesCacheGenerator_statValue(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x64\x64\x20\x45\x66\x66\x65\x63\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator_m_Effect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x64\x64\x20\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
	}
}
static void AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_ModType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x64\x64\x20\x56\x61\x6C\x75\x65"), NULL);
	}
}
static void AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x70\x70\x6C\x79\x20\x44\x61\x6D\x61\x67\x65"), NULL);
	}
}
static void ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x43\x6F\x6D\x70\x61\x72\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_ValueType(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x79\x70\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Condition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x52\x65\x66\x72\x65\x73\x68"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
}
static void RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x52\x65\x6D\x6F\x76\x65\x20\x45\x66\x66\x65\x63\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator_m_Effect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x64\x64\x20\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x53\x75\x62\x74\x72\x61\x63\x74\x20\x56\x61\x6C\x75\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74"), NULL);
	}
}
static void SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Attribute_t5B1E9B3AC42D32F125D2FDC97C4BADF1EA640E80_CustomAttributesCacheGenerator_m_StartCurrentValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Attribute_t5B1E9B3AC42D32F125D2FDC97C4BADF1EA640E80_CustomAttributesCacheGenerator_Attribute_U3CInitializeU3Eb__6_0_m0BBCA00F943A9FBA8AE42E8FD86E20B3CBBC849C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x6D\x61\x67\x65\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x53\x74\x61\x74\x20\x53\x79\x73\x74\x65\x6D\x2F\x44\x61\x6D\x61\x67\x65\x20\x44\x61\x74\x61"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_sendingStat(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_maxAngle(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 360.0f, NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_damagePrefab(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x70\x6C\x61\x79\x44\x61\x6D\x61\x67\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_damageColor(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x70\x6C\x61\x79\x44\x61\x6D\x61\x67\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_criticalDamageColor(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x70\x6C\x61\x79\x44\x61\x6D\x61\x67\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_intensity(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x70\x6C\x61\x79\x44\x61\x6D\x61\x67\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_particleEffect(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x74\x69\x63\x6C\x65\x73"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_audioMixerGroup(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64\x73"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_volumeScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_enableShake(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x53\x68\x61\x6B\x65"), NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x64"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x53\x68\x61\x6B\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x53\x68\x61\x6B\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_amount(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x53\x68\x61\x6B\x65"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_enableKnockback(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x6E\x6F\x63\x6B\x62\x61\x63\x6B"), NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x64"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackChance(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x4B\x6E\x6F\x63\x6B\x62\x61\x63\x6B"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackStrength(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x4B\x6E\x6F\x63\x6B\x62\x61\x63\x6B"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackAcceleration(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x4B\x6E\x6F\x63\x6B\x62\x61\x63\x6B"), NULL);
	}
}
static void DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackDuration(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x65\x6E\x61\x62\x6C\x65\x4B\x6E\x6F\x63\x6B\x62\x61\x63\x6B"), NULL);
	}
}
static void Level_t44F3334F7BA87E4CCB905F4BB39A173CF489C51B_CustomAttributesCacheGenerator_m_Experience(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 * tmp = (StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 *)cache->attributes[1];
		StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57(tmp, NULL);
	}
}
static void Level_t44F3334F7BA87E4CCB905F4BB39A173CF489C51B_CustomAttributesCacheGenerator_Level_U3CInitializeU3Eb__1_0_mE2793F4F14D9C59BD7D7F25A213A75670D7FF277(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683____buttons2(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C____replacements1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_BaseValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_FormulaGraph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_Cap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_Callbacks(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void U3CU3Ec_t91EE94D36A523D3A4D2A9638E8406145FF8DB6DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t3AD81A2640D114FF2D15D12B620FE8973910CFC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_ValueType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Condition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Actions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_StatEffectName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_Repeat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_Actions(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_HandlerName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_Stats(CustomAttributesCache* cache)
{
	{
		StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 * tmp = (StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 *)cache->attributes[0];
		StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_StatOverrides(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_Effects(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_StatsHandler_Knockback_m35F1925FA3CBAA5C555AB778563D0904505EE453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_tE6EC045F994DFD92F2CD0DE08127E58CC0B545EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16__ctor_m329A5D30C415B9BC16139C55C8A93BCA9CADA4FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_IDisposable_Dispose_m19B8A236E9BBC6DE44B2F7BFEFE39D7C4AE91D85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC80C7DFD86C02829859923D00C6CE90D187846CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_IEnumerator_Reset_mC85E4C24234040A47E11AEF9943F6263705A0D0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_IEnumerator_get_Current_m81D4E93D7F6955995104EC73C52CA904DF63C84B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_t4DF05B4D3F37EBB72D57EB0DB1B377C6B72B6323_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t37EC0E7EDCBA6EFA740182E2EB5831990890E5DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_m_Database(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_StatsManager_DelayedLoading_mA76B40F29EB936EE5BB4E369EAB5E2DAFFE78061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_0_0_0_var), NULL);
	}
}
static void StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_StatsManager_RepeatSaving_m8939025EB8F979E7226934EFBCC988CCF2E0EB2A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__18_1_t8F7DFDE4DD5ABFA5BC1C55CB8FFFC7A51305C2AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tA101352EB3E98CBE2BBDF921F10C23C85E2980E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_tCBFD0E3AA3D551D7BF8CCEF73ABCE88F6097C91E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27__ctor_m7AD70427BFA5E4E78ED6D1793DEA46C70C9C18D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_IDisposable_Dispose_mCC1EF335F94627F7B4749AFC0033C996788FA255(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m530C66EE210D0708E9926EB9697F65CF71A7B32A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_Reset_mA3A2DE31353E38C08644726C9B7CB196EB8C9BFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_get_Current_m49D96F059D1AF1D6808DAEF1A67D39601B2AF2D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28__ctor_m43C748022A5E0D8F59EE964A5C05DB2407978720(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_IDisposable_Dispose_m5CCB625264F2C834B7889E1C3B9DDFCF6A7C6DBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEABC31FC27F603FB8556D196F5C635CE72DF6C31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_Reset_mC778C8BA155D7599A36976C2DD04F6BF5167DDAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_get_Current_m8F8CFDE00DF78F2A342D78E713428C0740AD7AF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t8C5A0D7437CBBE9E5BC8881684F61CB2E66A12F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatsHandler(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x44\x65\x66\x69\x6E\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_Stat(CustomAttributesCache* cache)
{
	{
		StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 * tmp = (StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 *)cache->attributes[0];
		StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_FreePoints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 * tmp = (StatPickerAttribute_tB96E497E7D8319776537CBC7727C73FCBBB7CA97 *)cache->attributes[1];
		StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatBar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatBarFade(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_CurrentValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_IncrementButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_UIStat_U3CStartU3Eb__11_0_mC9B417C247447A9DE1F134711F317B0D51314EC3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Default_t6E91B060253F1C77056ACCB8516B0BC0B2E27259_CustomAttributesCacheGenerator_debugMessages(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
}
static void UI_t37A0D4181FC3AB3CE2FA4ACF85CD68A3EC9604A3_CustomAttributesCacheGenerator_notificationName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UI_t37A0D4181FC3AB3CE2FA4ACF85CD68A3EC9604A3_CustomAttributesCacheGenerator_dialogBoxName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x20\x42\x6F\x78"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x69\x61\x6C\x6F\x67\x20\x62\x6F\x78\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_StatSystem_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_StatSystem_AttributeGenerators[129] = 
{
	GetStatCurrentValue_t505491C52AAF3D0DBCF84F6495B473B57C8E5494_CustomAttributesCacheGenerator,
	GetStatValue_t8735185F4FD569A1EB0D5E9F984B019D467FCABB_CustomAttributesCacheGenerator,
	AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator,
	AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator,
	AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator,
	ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator,
	Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator,
	RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator,
	RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator,
	SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator,
	NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator,
	U3CU3Ec_t91EE94D36A523D3A4D2A9638E8406145FF8DB6DF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t3AD81A2640D114FF2D15D12B620FE8973910CFC2_CustomAttributesCacheGenerator,
	StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_tE6EC045F994DFD92F2CD0DE08127E58CC0B545EA_CustomAttributesCacheGenerator,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_t4DF05B4D3F37EBB72D57EB0DB1B377C6B72B6323_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t37EC0E7EDCBA6EFA740182E2EB5831990890E5DE_CustomAttributesCacheGenerator,
	U3CU3Ec__18_1_t8F7DFDE4DD5ABFA5BC1C55CB8FFFC7A51305C2AA_CustomAttributesCacheGenerator,
	U3CU3Ec_tA101352EB3E98CBE2BBDF921F10C23C85E2980E2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_tCBFD0E3AA3D551D7BF8CCEF73ABCE88F6097C91E_CustomAttributesCacheGenerator,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t8C5A0D7437CBBE9E5BC8881684F61CB2E66A12F6_CustomAttributesCacheGenerator,
	StatNode_t3477762477FF89703CE81E81ED6CD780173D58EF_CustomAttributesCacheGenerator_stat,
	StatNode_t3477762477FF89703CE81E81ED6CD780173D58EF_CustomAttributesCacheGenerator_statValue,
	AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator_m_Target,
	AddEffect_t45124BC97851814E5FECD927AEDF94BD9E11BAFE_CustomAttributesCacheGenerator_m_Effect,
	AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_Target,
	AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_StatName,
	AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_Value,
	AddModifier_t10577C44BAD30F6B29933D1346DF4A0B184E8445_CustomAttributesCacheGenerator_m_ModType,
	AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_Target,
	AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_StatName,
	AddValue_t4A64ED7FFA04586DD59C6F2C0CE2D2488ED44C18_CustomAttributesCacheGenerator_m_Value,
	ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_Target,
	ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_StatName,
	ApplyDamage_t99599525A8A698EADC663CAC9C3DD5FBA371D80E_CustomAttributesCacheGenerator_m_Value,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Target,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_StatName,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_ValueType,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Condition,
	Compare_t7CE78CF615122A07A4307902FDE29E868CFD1A8E_CustomAttributesCacheGenerator_m_Value,
	Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator_m_Target,
	Refresh_t1302D8A138412D2FDBD81F9D93B0985D67C6BAC4_CustomAttributesCacheGenerator_m_StatName,
	RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator_m_Target,
	RemoveEffect_t870B6256041B42FAD799931803CB3823951A7C74_CustomAttributesCacheGenerator_m_Effect,
	RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator_m_Target,
	RemoveModifier_t6F12F9EDBB5850A4A35D7B68EA09DFA70AA3B65B_CustomAttributesCacheGenerator_m_StatName,
	SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_Target,
	SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_StatName,
	SubtractValue_t95F3A0E76E44A711317B64872827A4D8D0658FBB_CustomAttributesCacheGenerator_m_Value,
	Attribute_t5B1E9B3AC42D32F125D2FDC97C4BADF1EA640E80_CustomAttributesCacheGenerator_m_StartCurrentValue,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_sendingStat,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_maxAngle,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_damagePrefab,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_damageColor,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_criticalDamageColor,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_intensity,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_particleEffect,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_audioMixerGroup,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_volumeScale,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_enableShake,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_duration,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_speed,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_amount,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_enableKnockback,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackChance,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackStrength,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackAcceleration,
	DamageData_t7012C714D30297E8C52C7BBC06E951637F38B5C0_CustomAttributesCacheGenerator_knockbackDuration,
	Level_t44F3334F7BA87E4CCB905F4BB39A173CF489C51B_CustomAttributesCacheGenerator_m_Experience,
	Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_StatName,
	Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_BaseValue,
	Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_FormulaGraph,
	Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_Cap,
	Stat_t76D58A5055A01700CEC2A6A6DAF837947CFB5E61_CustomAttributesCacheGenerator_m_Callbacks,
	StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_ValueType,
	StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Condition,
	StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Value,
	StatCallback_tCBBEBCE62D272FAD01256B1C1D29371B3814CCCC_CustomAttributesCacheGenerator_m_Actions,
	StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_StatEffectName,
	StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_Repeat,
	StatEffect_t2A6D0D82E0E12332D40DCC5FE8FC2586F72A4010_CustomAttributesCacheGenerator_m_Actions,
	StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_HandlerName,
	StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_Stats,
	StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_StatOverrides,
	StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_m_Effects,
	StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_m_Database,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatsHandler,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_Stat,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_FreePoints,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatName,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatBar,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_StatBarFade,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_CurrentValue,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_Value,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_m_IncrementButton,
	Default_t6E91B060253F1C77056ACCB8516B0BC0B2E27259_CustomAttributesCacheGenerator_debugMessages,
	UI_t37A0D4181FC3AB3CE2FA4ACF85CD68A3EC9604A3_CustomAttributesCacheGenerator_notificationName,
	UI_t37A0D4181FC3AB3CE2FA4ACF85CD68A3EC9604A3_CustomAttributesCacheGenerator_dialogBoxName,
	Attribute_t5B1E9B3AC42D32F125D2FDC97C4BADF1EA640E80_CustomAttributesCacheGenerator_Attribute_U3CInitializeU3Eb__6_0_m0BBCA00F943A9FBA8AE42E8FD86E20B3CBBC849C,
	Level_t44F3334F7BA87E4CCB905F4BB39A173CF489C51B_CustomAttributesCacheGenerator_Level_U3CInitializeU3Eb__1_0_mE2793F4F14D9C59BD7D7F25A213A75670D7FF277,
	NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683,
	NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C,
	StatsHandler_t49AE58B2BE96642364D0AE6A2F73D1B1AC918194_CustomAttributesCacheGenerator_StatsHandler_Knockback_m35F1925FA3CBAA5C555AB778563D0904505EE453,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16__ctor_m329A5D30C415B9BC16139C55C8A93BCA9CADA4FF,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_IDisposable_Dispose_m19B8A236E9BBC6DE44B2F7BFEFE39D7C4AE91D85,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC80C7DFD86C02829859923D00C6CE90D187846CC,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_IEnumerator_Reset_mC85E4C24234040A47E11AEF9943F6263705A0D0C,
	U3CKnockbackU3Ed__16_t7FFCCBBDC0681CB6E5B9BC51553E75BD4058B4E0_CustomAttributesCacheGenerator_U3CKnockbackU3Ed__16_System_Collections_IEnumerator_get_Current_m81D4E93D7F6955995104EC73C52CA904DF63C84B,
	StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_StatsManager_DelayedLoading_mA76B40F29EB936EE5BB4E369EAB5E2DAFFE78061,
	StatsManager_t8B3E7EBDCF2C4BC788A5CE97CB34FBD842F282B8_CustomAttributesCacheGenerator_StatsManager_RepeatSaving_m8939025EB8F979E7226934EFBCC988CCF2E0EB2A,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27__ctor_m7AD70427BFA5E4E78ED6D1793DEA46C70C9C18D9,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_IDisposable_Dispose_mCC1EF335F94627F7B4749AFC0033C996788FA255,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m530C66EE210D0708E9926EB9697F65CF71A7B32A,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_Reset_mA3A2DE31353E38C08644726C9B7CB196EB8C9BFA,
	U3CDelayedLoadingU3Ed__27_t1C24855E83FB400497CD713C66C02292F5429D95_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_get_Current_m49D96F059D1AF1D6808DAEF1A67D39601B2AF2D0,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28__ctor_m43C748022A5E0D8F59EE964A5C05DB2407978720,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_IDisposable_Dispose_m5CCB625264F2C834B7889E1C3B9DDFCF6A7C6DBD,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEABC31FC27F603FB8556D196F5C635CE72DF6C31,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_Reset_mC778C8BA155D7599A36976C2DD04F6BF5167DDAF,
	U3CRepeatSavingU3Ed__28_t94F6F0D2EDE20F3735D84DFD61C7B13FF5A1608C_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_get_Current_m8F8CFDE00DF78F2A342D78E713428C0740AD7AF7,
	UIStat_t48A0646B8A0247B4D4795DA483B477D66DD451BA_CustomAttributesCacheGenerator_UIStat_U3CStartU3Eb__11_0_mC9B417C247447A9DE1F134711F317B0D51314EC3,
	NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683____buttons2,
	NotificationExtension_t33D2E04FAF630A3C6E75899EAB80E7B9822FAA98_CustomAttributesCacheGenerator_NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C____replacements1,
	DevionGames_StatSystem_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
