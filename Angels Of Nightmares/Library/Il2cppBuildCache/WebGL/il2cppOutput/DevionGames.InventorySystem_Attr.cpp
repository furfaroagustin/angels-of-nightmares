﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// DevionGames.InventorySystem.CategoryPickerAttribute
struct CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8;
// DevionGames.CompoundAttribute
struct CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// DevionGames.InventorySystem.CurrencyPickerAttribute
struct CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C;
// DevionGames.InventorySystem.EquipmentPickerAttribute
struct EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.IconAttribute
struct IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426;
// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2;
// DevionGames.InventorySystem.ItemGroupPickerAttribute
struct ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F;
// DevionGames.InventorySystem.ItemPickerAttribute
struct ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// DevionGames.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// DevionGames.InventorySystem.RarityPickerAttribute
struct RarityPickerAttribute_t358EC102DED10FE10A1C490FB8AF4FBAEF4A8131;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.ComponentMenu::m_ComponentMenu
	String_t* ___m_ComponentMenu_0;

public:
	inline static int32_t get_offset_of_m_ComponentMenu_0() { return static_cast<int32_t>(offsetof(ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8, ___m_ComponentMenu_0)); }
	inline String_t* get_m_ComponentMenu_0() const { return ___m_ComponentMenu_0; }
	inline String_t** get_address_of_m_ComponentMenu_0() { return &___m_ComponentMenu_0; }
	inline void set_m_ComponentMenu_0(String_t* value)
	{
		___m_ComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentMenu_0), (void*)value);
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.IconAttribute
struct IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type DevionGames.IconAttribute::type
	Type_t * ___type_0;
	// System.String DevionGames.IconAttribute::path
	String_t* ___path_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_0), (void*)value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_1), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C 
{
public:
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::className
	String_t* ___className_0;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpace
	String_t* ___nameSpace_1;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assembly
	String_t* ___assembly_2;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::classHasChanged
	bool ___classHasChanged_3;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpaceHasChanged
	bool ___nameSpaceHasChanged_4;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assemblyHasChanged
	bool ___assemblyHasChanged_5;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::autoUdpateAPI
	bool ___autoUdpateAPI_6;

public:
	inline static int32_t get_offset_of_className_0() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___className_0)); }
	inline String_t* get_className_0() const { return ___className_0; }
	inline String_t** get_address_of_className_0() { return &___className_0; }
	inline void set_className_0(String_t* value)
	{
		___className_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___className_0), (void*)value);
	}

	inline static int32_t get_offset_of_nameSpace_1() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpace_1)); }
	inline String_t* get_nameSpace_1() const { return ___nameSpace_1; }
	inline String_t** get_address_of_nameSpace_1() { return &___nameSpace_1; }
	inline void set_nameSpace_1(String_t* value)
	{
		___nameSpace_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameSpace_1), (void*)value);
	}

	inline static int32_t get_offset_of_assembly_2() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assembly_2)); }
	inline String_t* get_assembly_2() const { return ___assembly_2; }
	inline String_t** get_address_of_assembly_2() { return &___assembly_2; }
	inline void set_assembly_2(String_t* value)
	{
		___assembly_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assembly_2), (void*)value);
	}

	inline static int32_t get_offset_of_classHasChanged_3() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___classHasChanged_3)); }
	inline bool get_classHasChanged_3() const { return ___classHasChanged_3; }
	inline bool* get_address_of_classHasChanged_3() { return &___classHasChanged_3; }
	inline void set_classHasChanged_3(bool value)
	{
		___classHasChanged_3 = value;
	}

	inline static int32_t get_offset_of_nameSpaceHasChanged_4() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpaceHasChanged_4)); }
	inline bool get_nameSpaceHasChanged_4() const { return ___nameSpaceHasChanged_4; }
	inline bool* get_address_of_nameSpaceHasChanged_4() { return &___nameSpaceHasChanged_4; }
	inline void set_nameSpaceHasChanged_4(bool value)
	{
		___nameSpaceHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_assemblyHasChanged_5() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assemblyHasChanged_5)); }
	inline bool get_assemblyHasChanged_5() const { return ___assemblyHasChanged_5; }
	inline bool* get_address_of_assemblyHasChanged_5() { return &___assemblyHasChanged_5; }
	inline void set_assemblyHasChanged_5(bool value)
	{
		___assemblyHasChanged_5 = value;
	}

	inline static int32_t get_offset_of_autoUdpateAPI_6() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___autoUdpateAPI_6)); }
	inline bool get_autoUdpateAPI_6() const { return ___autoUdpateAPI_6; }
	inline bool* get_address_of_autoUdpateAPI_6() { return &___autoUdpateAPI_6; }
	inline void set_autoUdpateAPI_6(bool value)
	{
		___autoUdpateAPI_6 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_pinvoke
{
	char* ___className_0;
	char* ___nameSpace_1;
	char* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};
// Native definition for COM marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_com
{
	Il2CppChar* ___className_0;
	Il2CppChar* ___nameSpace_1;
	Il2CppChar* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};

// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.CompoundAttribute
struct CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.CompoundAttribute::propertyPath
	String_t* ___propertyPath_0;

public:
	inline static int32_t get_offset_of_propertyPath_0() { return static_cast<int32_t>(offsetof(CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5, ___propertyPath_0)); }
	inline String_t* get_propertyPath_0() const { return ___propertyPath_0; }
	inline String_t** get_address_of_propertyPath_0() { return &___propertyPath_0; }
	inline void set_propertyPath_0(String_t* value)
	{
		___propertyPath_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___propertyPath_0), (void*)value);
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.InspectorLabelAttribute::label
	String_t* ___label_0;
	// System.String DevionGames.InspectorLabelAttribute::tooltip
	String_t* ___tooltip_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_tooltip_1() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___tooltip_1)); }
	inline String_t* get_tooltip_1() const { return ___tooltip_1; }
	inline String_t** get_address_of_tooltip_1() { return &___tooltip_1; }
	inline void set_tooltip_1(String_t* value)
	{
		___tooltip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// DevionGames.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single DevionGames.MinMaxSliderAttribute::max
	float ___max_0;
	// System.Single DevionGames.MinMaxSliderAttribute::min
	float ___min_1;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744, ___max_0)); }
	inline float get_max_0() const { return ___max_0; }
	inline float* get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(float value)
	{
		___max_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744, ___min_1)); }
	inline float get_min_1() const { return ___min_1; }
	inline float* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(float value)
	{
		___min_1 = value;
	}
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData UnityEngine.Scripting.APIUpdating.MovedFromAttribute::data
	MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8, ___data_0)); }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  get_data_0() const { return ___data_0; }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C * get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___className_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___nameSpace_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___assembly_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// DevionGames.InventorySystem.PickerAttribute
struct PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Boolean DevionGames.InventorySystem.PickerAttribute::utility
	bool ___utility_0;

public:
	inline static int32_t get_offset_of_utility_0() { return static_cast<int32_t>(offsetof(PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156, ___utility_0)); }
	inline bool get_utility_0() const { return ___utility_0; }
	inline bool* get_address_of_utility_0() { return &___utility_0; }
	inline void set_utility_0(bool value)
	{
		___utility_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// DevionGames.InventorySystem.CategoryPickerAttribute
struct CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// DevionGames.InventorySystem.CurrencyPickerAttribute
struct CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// DevionGames.InventorySystem.EquipmentPickerAttribute
struct EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// DevionGames.InventorySystem.ItemGroupPickerAttribute
struct ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// DevionGames.InventorySystem.ItemPickerAttribute
struct ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// DevionGames.InventorySystem.RarityPickerAttribute
struct RarityPickerAttribute_t358EC102DED10FE10A1C490FB8AF4FBAEF4A8131  : public PickerAttribute_tF4FECB00A10E510F0F4B5C1D2D24704233A07156
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.CategoryPickerAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64 (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * __this, bool ___utility0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.CurrencyPickerAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D (CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 * __this, bool ___utility0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.EquipmentPickerAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1 (EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 * __this, bool ___utility0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.ItemPickerAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * __this, bool ___utility0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.ItemGroupPickerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030 (ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338 (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, int32_t ___lines0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.CategoryPickerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CategoryPickerAttribute__ctor_m27FFA39A126E099223A5122AB04E697EFF9E946B (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void DevionGames.MinMaxSliderAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52 (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.RarityPickerAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RarityPickerAttribute__ctor_m7DB777170B060BE32956BC1990EDD7FD24E65CE5 (RarityPickerAttribute_t358EC102DED10FE10A1C490FB8AF4FBAEF4A8131 * __this, bool ___utility0, const RuntimeMethod* method);
// System.Void DevionGames.CompoundAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4 (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * __this, String_t* ___propertyPath0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void DevionGames.ComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3 (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void DevionGames.IconAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * __this, String_t* ___path0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.Boolean,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * __this, bool ___autoUpdateAPI0, String_t* ___sourceNamespace1, String_t* ___sourceAssembly2, String_t* ___sourceClassName3, const RuntimeMethod* method);
// System.Void DevionGames.IconAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330 (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void DevionGames.EnumFlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818 (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, String_t* ___tooltip1, const RuntimeMethod* method);
static void DevionGames_InventorySystem_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_StartDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Repeat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_RepeatDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Data(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_AreaOfEffect_Start_m1BCF771F26D8968CFE154EFB7A768BEF77B6DF8C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tFC7D786C50C21B12305AC2AA098817BFF0D6D25D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5__ctor_mDD0FCA4B6533BB95B3546AC33074448CB76D9693(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_IDisposable_Dispose_m54A36E781AE43ABCDAB8A1519129451345E14150(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D5E1F2EC2C0F803F81E71054DA06B5F02A1EEE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m2E89927814E781AFD1A1114C71DC06BFF9538222(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m21DA2F9D0680E2C3450473D914ACD575EC2130AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CategoryAttribute_t7AA4E82C863C9C28715CF0598B213CE6DEF2D43A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_Parent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * tmp = (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 *)cache->attributes[1];
		CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64(tmp, true, NULL);
	}
}
static void Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_EditorColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_Cooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0_CustomAttributesCacheGenerator_currency(CustomAttributesCache* cache)
{
	{
		CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 * tmp = (CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 *)cache->attributes[0];
		CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D(tmp, true, NULL);
	}
}
static void EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_Database(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_Bones(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_VisibleItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentBone_t5B6414B4D660A89F4A8236212120ACBA39B0C141_CustomAttributesCacheGenerator_region(CustomAttributesCache* cache)
{
	{
		EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 * tmp = (EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 *)cache->attributes[0];
		EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t2255562419CC692D227A5C20A04A5B84F0D3A514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_tCA7FC89013F98C71746ABE0E8DE3975BB250884C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadClipSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x3A"), NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ResetClipSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_Projectile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x6A\x65\x63\x74\x69\x6C\x65\x3A"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileVisibility(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_FirePoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[1];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileItemWindow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VisibleItem_t295AAB6C2EE93CA6996D3A77186B78FE39D26AFC_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void VisibleItem_t295AAB6C2EE93CA6996D3A77186B78FE39D26AFC_CustomAttributesCacheGenerator_attachments(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D_CustomAttributesCacheGenerator_region(CustomAttributesCache* cache)
{
	{
		EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 * tmp = (EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 *)cache->attributes[0];
		EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1(tmp, true, NULL);
	}
}
static void Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D_CustomAttributesCacheGenerator_gameObject(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ActivationInputName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x20\x4E\x61\x6D\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x3A"), NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ActivationType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_UseInputName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x20\x4E\x61\x6D\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x3A"), NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_StartType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_StopType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKTarget(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x20\x49\x4B\x3A"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ItemID(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x20\x53\x74\x61\x74\x65\x73\x3A"), NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_IdleState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_UseState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var), NULL);
	}
}
static void ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_m_ItemGeneratorData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_m_MaxAmount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_ItemGenerator_U3CGenerateItemsU3Eb__3_0_m674BC7BF5C695E2D0CEB7586F350F7EF62C8A8A8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemGeneratorData_t7CD907831F3BCC5A75E4462BF29AFE4E3D9ACB02_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void ItemGeneratorData_t7CD907831F3BCC5A75E4462BF29AFE4E3D9ACB02_CustomAttributesCacheGenerator_chance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var), NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_From(CustomAttributesCache* cache)
{
	{
		ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F * tmp = (ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F *)cache->attributes[0];
		ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Filters(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MinStack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MaxStack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MinAmount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MaxAmount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Chance(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Modifiers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_m_Database(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_m_ChildDatabases(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_onDataLoaded(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_onDataSaved(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_GetBounds_m7490F78E6B2A3B1806058F81D20E63F53C816E53(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x4D\x61\x6E\x61\x67\x65\x72\x2E\x47\x65\x74\x42\x6F\x75\x6E\x64\x73\x20\x69\x73\x20\x6F\x62\x73\x6F\x6C\x65\x74\x65\x20\x55\x73\x65\x20\x55\x6E\x69\x74\x79\x55\x74\x69\x6C\x69\x74\x79\x2E\x47\x65\x74\x42\x6F\x75\x6E\x64\x73"), NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_DelayedLoading_mC2BAB7CBA7F1F2CF9F329B7DB66264EE52C056A3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_0_0_0_var), NULL);
	}
}
static void InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_RepeatSaving_m5D8702EC560A6F60DC5CF51077CDBB350BE26F0E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__23_1_tC6CA23302C37934961EBF727D8308D85E9DA16F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t050FF8165F5BE81574D32CCC7022F998D0BB9A3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37__ctor_m3325202AFC5623802BE84D44B0A7DDC9DE29095E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_IDisposable_Dispose_m71FFD192424C855DB335AC3AD8EB218444508066(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE895F35F30A0DC7759F22C9E344873FD0F449B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_Reset_m5E31818C9301990803658065761372C698261518(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_get_Current_m3B16221AB84E47B898C986B4DDC35AF4D0511C1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38__ctor_m6BABEB7DF2E5AA16D319ED98FCF12AD557763172(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_IDisposable_Dispose_m06C133222BF9EF721288B8864DD7BC84857CC580(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9434F8BE699510BBA853AE850EDA9590EE455611(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_Reset_m4E523C49553E9E05315E213B36ED6E4A9BE347A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_get_Current_m5BC6CCD95656D52496CF70BBEDFB46BBC364A187(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass41_0_tC45C74C65916B595C57D2D9C649DB478A4E288DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Items(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x69\x74\x65\x6D\x73"), NULL);
	}
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[2];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Amounts(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x61\x6D\x6F\x75\x6E\x74\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Modifiers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_onChange(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t51230A50220466A7180436503B8EBE585DB5D0D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_t6C1EC9F94DC0EBBB98B76C0B6C8E599875809823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemCollectionPopulator_tA52D2A2EE6808E9A5BC3DCF4156A0F73CAAD2F30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var), NULL);
	}
}
static void ItemCollectionPopulator_tA52D2A2EE6808E9A5BC3DCF4156A0F73CAAD2F30_CustomAttributesCacheGenerator_m_ItemGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F * tmp = (ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F *)cache->attributes[1];
		ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030(tmp, NULL);
	}
}
static void ItemCondition_t956DA12EF1F19DD6F7D8154ED039912DB064965A_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void ItemCondition_t956DA12EF1F19DD6F7D8154ED039912DB064965A_CustomAttributesCacheGenerator_category(CustomAttributesCache* cache)
{
	{
		CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * tmp = (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 *)cache->attributes[0];
		CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64(tmp, true, NULL);
	}
}
static void ItemContainerPopulator_t5937F1FF41D49170F0C9586A54928ADAFE52E248_CustomAttributesCacheGenerator_m_Entries(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068_CustomAttributesCacheGenerator_group(CustomAttributesCache* cache)
{
	{
		ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F * tmp = (ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F *)cache->attributes[0];
		ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_0_m3B3F48AA98F934853BDB4646CB7A0DC823F031EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_1_m77CBC2BC2BA3096537EEF5F026EED3C48B19664E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_2_m808DCA78EC4202BB44A393415ED0270388CB6730(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_3_mF0E9185A251DD88FFAE0ABDAE689984884A2B9E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_4_m96F50044707291B8066224E9D89807BE41574364(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_5_mF41FB56C477F6EE7E2939F43286360F76F89BAD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_GroupName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Items(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Amounts(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Modifiers(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentItem_t130209391E3888A08F2D69447EE07BFF77CA18D6_CustomAttributesCacheGenerator_m_OverrideEquipPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentItem_t130209391E3888A08F2D69447EE07BFF77CA18D6_CustomAttributesCacheGenerator_m_Region(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 * tmp = (EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 *)cache->attributes[1];
		EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1(tmp, true, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Id(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_ItemName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x71\x75\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x2E\x20\x49\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x6E\x61\x6D\x65\x20\x69\x6E\x20\x55\x49\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_UseItemNameAsDisplayName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x68\x65\x20\x4E\x61\x6D\x65\x20\x73\x65\x74\x74\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x6E\x61\x6D\x65\x20\x69\x6E\x20\x55\x49\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_DisplayName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x73\x20\x6E\x61\x6D\x65\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x69\x6E\x20\x55\x49\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Icon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x63\x6F\x6E\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x68\x6F\x77\x6E\x20\x69\x6E\x20\x76\x61\x72\x69\x6F\x75\x73\x20\x70\x6C\x61\x63\x65\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x55\x49\x2E\x20\x54\x6F\x6F\x6C\x74\x69\x70\x2C\x20\x76\x65\x6E\x64\x6F\x72\x20\x61\x6E\x64\x20\x6D\x61\x6E\x79\x20\x6D\x6F\x72\x65\x2E\x20"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Prefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x74\x6F\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x20\x77\x68\x65\x6E\x20\x61\x6E\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x64\x72\x61\x67\x65\x64\x20\x6F\x75\x74\x20\x6F\x66\x20\x61\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E\x20\x54\x68\x69\x73\x20\x70\x72\x65\x66\x61\x62\x20\x69\x73\x20\x61\x6C\x73\x6F\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x69\x6E\x20\x73\x63\x65\x6E\x65\x2C\x20\x73\x6F\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x63\x61\x6E\x20\x70\x69\x63\x6B\x75\x70\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Description(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x69\x73\x20\x75\x73\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E\x20\x54\x6F\x6F\x6C\x74\x69\x70\x2C\x20\x76\x65\x6E\x64\x6F\x72\x2C\x20\x73\x70\x65\x6C\x6C\x73\x2C\x20\x63\x72\x61\x66\x74\x69\x6E\x67\x2E\x2E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[2];
		MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338(tmp, 4LL, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Category(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x61\x74\x65\x67\x6F\x72\x79\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x62\x65\x6C\x6F\x6E\x67\x73\x20\x74\x6F\x2E\x20\x55\x73\x65\x64\x20\x74\x6F\x20\x73\x6F\x72\x74\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x63\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x69\x6E\x20\x65\x64\x69\x74\x6F\x72\x20\x6F\x72\x20\x61\x6C\x73\x6F\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x20\x69\x6E\x20\x74\x68\x65\x20\x55\x49\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72\x3A"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * tmp = (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 *)cache->attributes[3];
		CategoryPickerAttribute__ctor_m27FFA39A126E099223A5122AB04E697EFF9E946B(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsSellable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x73\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x73\x65\x6C\x6C\x61\x62\x6C\x65\x20\x74\x6F\x20\x61\x20\x76\x65\x6E\x64\x6F\x72\x3F\x20\x4D\x6F\x72\x65\x20\x6F\x70\x74\x69\x6F\x6E\x73\x20\x77\x69\x6C\x6C\x20\x61\x70\x70\x65\x61\x72\x20\x69\x66\x20\x69\x74\x20\x69\x73\x20\x73\x65\x6C\x6C\x61\x62\x6C\x65\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_BuyPrice(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x73\x20\x62\x75\x79\x20\x70\x72\x69\x63\x65\x2E\x20\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x72\x61\x72\x69\x74\x69\x65\x73\x20\x70\x72\x69\x63\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CanBuyBack(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x64\x64\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x76\x65\x6E\x64\x6F\x72\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x63\x61\x6E\x20\x62\x75\x79\x20\x69\x74\x20\x62\x61\x63\x6B\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_BuyCurrency(CustomAttributesCache* cache)
{
	{
		CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 * tmp = (CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 *)cache->attributes[0];
		CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D(tmp, true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x62\x75\x79\x20\x63\x75\x72\x72\x65\x6E\x63\x79\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x75\x73\x65\x20\x61\x20\x6C\x6F\x77\x65\x72\x20\x63\x75\x72\x72\x65\x6E\x63\x79\x2C\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x75\x74\x6F\x20\x63\x6F\x6E\x76\x65\x72\x74\x65\x64\x2E\x20\x31\x32\x30\x20\x43\x6F\x70\x70\x65\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x6F\x6E\x76\x65\x72\x74\x65\x64\x20\x74\x6F\x20\x31\x20\x53\x69\x6C\x76\x65\x72\x20\x61\x6E\x64\x20\x32\x30\x20\x43\x6F\x70\x70\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SellPrice(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x73\x20\x73\x65\x6C\x6C\x20\x70\x72\x69\x63\x65\x2E\x20\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x72\x61\x72\x69\x74\x69\x65\x73\x20\x70\x72\x69\x63\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SellCurrency(CustomAttributesCache* cache)
{
	{
		CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 * tmp = (CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 *)cache->attributes[0];
		CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D(tmp, true, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x6C\x6C\x20\x63\x75\x72\x72\x65\x6E\x63\x79\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x75\x73\x65\x20\x61\x20\x6C\x6F\x77\x65\x72\x20\x63\x75\x72\x72\x65\x6E\x63\x79\x2C\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x75\x74\x6F\x20\x63\x6F\x6E\x76\x65\x72\x74\x65\x64\x2E\x20\x31\x32\x30\x20\x43\x6F\x70\x70\x65\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x6F\x6E\x76\x65\x72\x74\x65\x64\x20\x74\x6F\x20\x31\x20\x53\x69\x6C\x76\x65\x72\x20\x61\x6E\x64\x20\x32\x30\x20\x43\x6F\x70\x70\x65\x72\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Stack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x73\x20\x73\x74\x61\x63\x6B\x20\x64\x65\x66\x69\x6E\x69\x74\x69\x6F\x6E\x2E\x20\x4E\x65\x77\x20\x63\x72\x65\x61\x74\x65\x64\x20\x69\x74\x65\x6D\x73\x20\x77\x69\x6C\x6C\x20\x68\x61\x76\x65\x20\x74\x68\x69\x73\x20\x73\x74\x61\x63\x6B\x2E\x20\x55\x73\x65\x20\x61\x20\x73\x74\x61\x63\x6B\x20\x6D\x6F\x64\x69\x66\x69\x65\x72\x20\x74\x6F\x20\x72\x61\x6E\x64\x6F\x6D\x69\x7A\x65\x20\x74\x68\x65\x20\x73\x74\x61\x63\x6B\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_MaxStack(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x74\x61\x63\x6B\x20\x61\x6D\x6F\x75\x6E\x74\x2E\x20\x49\x74\x65\x6D\x73\x20\x73\x74\x61\x63\x6B\x20\x63\x61\x6E\x27\x74\x20\x62\x65\x20\x68\x69\x67\x68\x65\x72\x20\x74\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x2E\x20\x49\x66\x20\x74\x68\x65\x20\x73\x74\x61\x63\x6B\x20\x69\x73\x20\x62\x69\x67\x67\x65\x72\x20\x74\x68\x65\x6E\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x73\x74\x61\x63\x6B\x2C\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x70\x6C\x69\x74\x74\x65\x64\x20\x69\x6E\x74\x6F\x20\x6D\x75\x6C\x74\x69\x70\x6C\x65\x20\x73\x74\x61\x63\x6B\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsDroppable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x64\x72\x6F\x70\x70\x61\x62\x6C\x65\x20\x66\x72\x6F\x6D\x20\x61\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x74\x6F\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_DropSound(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64\x20\x74\x68\x61\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x64\x72\x6F\x70\x70\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_OverridePrefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x79\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x69\x74\x65\x6D\x73\x20\x70\x72\x65\x66\x61\x62\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x77\x68\x65\x6E\x20\x64\x72\x6F\x70\x70\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsCraftable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x69\x6E\x65\x73\x20\x69\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x63\x72\x61\x66\x74\x61\x62\x6C\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x6C\x6F\x6E\x67\x20\x64\x6F\x65\x73\x20\x69\x74\x20\x74\x61\x6B\x65\x20\x74\x6F\x20\x63\x72\x61\x66\x74\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x2E\x20\x54\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x61\x6C\x73\x6F\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x62\x61\x72\x20\x69\x6E\x20\x63\x72\x61\x66\x74\x69\x6E\x67\x20\x55\x49\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_UseCraftingSkill(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x75\x6C\x64\x20\x61\x20\x73\x6B\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x63\x72\x61\x66\x74\x65\x64\x3F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SkillWindow(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x6B\x69\x6C\x6C\x20\x77\x69\x6E\x64\x6F\x77\x2E\x20\x49\x74\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x69\x66\x20\x75\x73\x65\x20\x63\x72\x61\x66\x74\x69\x6E\x67\x20\x73\x6B\x69\x6C\x6C\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x62\x65\x20\x61\x62\x6C\x65\x20\x74\x6F\x20\x66\x69\x6E\x64\x20\x74\x68\x65\x20\x73\x6B\x69\x6C\x6C\x2E\x20"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingSkill(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x61\x74\x20\x73\x6B\x69\x6C\x6C\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x63\x72\x61\x66\x74\x69\x6E\x67\x3F\x20\x54\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x70\x6C\x61\x79\x65\x72\x73\x20\x73\x6B\x69\x6C\x6C\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x65\x61\x72\x63\x68\x65\x64\x20\x69\x6E\x20\x73\x6B\x69\x6C\x6C\x20\x77\x69\x6E\x64\x6F\x77\x20\x73\x65\x74\x20\x61\x62\x6F\x76\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[2];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_RemoveIngredientsWhenFailed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6D\x6F\x76\x65\x20\x74\x68\x65\x20\x69\x6E\x67\x72\x65\x64\x69\x65\x6E\x74\x73\x20\x77\x68\x65\x6E\x20\x63\x72\x61\x66\x74\x69\x6E\x67\x20\x66\x61\x69\x6C\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_MinCraftingSkillValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x73\x6B\x69\x6C\x6C\x20\x74\x6F\x20\x63\x72\x61\x66\x74\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x2E\x20\x54\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x63\x61\x6E\x20\x6F\x6E\x6C\x79\x20\x63\x72\x61\x66\x74\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x69\x66\x20\x68\x69\x73\x20\x73\x6B\x69\x6C\x6C\x20\x69\x73\x20\x68\x69\x67\x68\x20\x65\x6E\x6F\x75\x67\x68\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingAnimatorState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x63\x72\x61\x66\x74\x65\x64\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x64\x6F\x6E\x27\x74\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x61\x6E\x79\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x2C\x20\x64\x65\x6C\x65\x74\x65\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x2E"), NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingModifier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_properties(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Ingredient_tF9A4CD3CF14706E1703D4F3B963CA8135CF680D4_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void U3CU3Ec__DisplayClass115_0_t1F03362DEAA585EA6E58F3955A9826CAC59F94E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass116_0_t4C9FB5FC569170ED2EF7A9B78C3CF9B1422D1765_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D_CustomAttributesCacheGenerator_m_FixedSuccessChance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D_CustomAttributesCacheGenerator_m_GainModifier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_m_UseCategoryCooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_m_Cooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_actions(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_UsableItem_SequenceCoroutine_m318A407A7B94147DD078E37D2D08C43C775EA045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_0_0_0_var), NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9__ctor_mD0D6AB8E654C1550E7FCD108F6B70B84E7BB1B05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_IDisposable_Dispose_m12DBD516F68B381F1311AF75F52DC4079F86D56C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m887024614CCE999B4A10D64456BC90F39F930864(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_m2DC9FF56B6AA56E737B6002C8AE95781725548AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x70\x6C\x65\x50\x72\x6F\x70\x65\x72\x74\x79\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x2F\x50\x72\x6F\x70\x65\x72\x74\x79"), NULL);
	}
}
static void PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_ApplyToAll(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_Properties(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_ModifierType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_Range(CustomAttributesCache* cache)
{
	{
		MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * tmp = (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 *)cache->attributes[0];
		MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52(tmp, -100.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RarityModifier_tEC87EA90A4403AD473C7458487DD9A4723A01686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x70\x6C\x65\x52\x61\x72\x69\x74\x79\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x2F\x52\x61\x72\x69\x74\x79"), NULL);
	}
}
static void RarityModifier_tEC87EA90A4403AD473C7458487DD9A4723A01686_CustomAttributesCacheGenerator_m_Rarities(CustomAttributesCache* cache)
{
	{
		RarityPickerAttribute_t358EC102DED10FE10A1C490FB8AF4FBAEF4A8131 * tmp = (RarityPickerAttribute_t358EC102DED10FE10A1C490FB8AF4FBAEF4A8131 *)cache->attributes[0];
		RarityPickerAttribute__ctor_m7DB777170B060BE32956BC1990EDD7FD24E65CE5(tmp, true, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x70\x6C\x65\x53\x6B\x69\x6C\x6C\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x2F\x53\x6B\x69\x6C\x6C"), NULL);
	}
}
static void SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator_m_Chance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator_m_Gain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x70\x6C\x65\x53\x74\x61\x63\x6B\x4D\x6F\x64\x69\x66\x69\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4D\x6F\x64\x69\x66\x69\x65\x72\x73\x2F\x53\x74\x61\x63\x6B"), NULL);
	}
}
static void StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator_m_Min(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator_m_Max(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_AutoDestruct(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestructDelay(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x41\x75\x74\x6F\x44\x65\x73\x74\x72\x75\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_StartDirection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_StartPositionOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_FollowTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_SelectBestTarget(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x46\x6F\x6C\x6C\x6F\x77\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_MaxDistance(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x46\x6F\x6C\x6C\x6F\x77\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_FieldOfView(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x46\x6F\x6C\x6C\x6F\x77\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_TurnSpeed(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x46\x6F\x6C\x6C\x6F\x77\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestroyOnCollision(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestroyDelay(CustomAttributesCache* cache)
{
	{
		CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 * tmp = (CompoundAttribute_t8844805F332AFA8C856C363FFA35959FA41BCED5 *)cache->attributes[0];
		CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x5F\x44\x65\x73\x74\x72\x6F\x79\x4F\x6E\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_Data(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_m_UseAsNamePrefix(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_color(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_chance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_multiplier(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x70\x65\x72\x74\x79\x20\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_m_PriceMultiplier(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x69\x63\x65\x20\x4D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator_NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator_NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1____replacements1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2F\x43\x6F\x6D\x70\x61\x72\x65\x20\x4E\x6F\x74\x69\x66\x79"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x69\x6E\x76\x6F\x6B\x65\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x6F\x6E"), NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64"), NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x66\x6F\x72\x20\x6D\x65\x74\x68\x6F\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Condition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Number(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_FailureNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2F\x49\x6E\x76\x6F\x6B\x65\x20\x4E\x6F\x74\x69\x66\x79"), NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x69\x6E\x76\x6F\x6B\x65\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x66\x6F\x72\x20\x6D\x65\x74\x68\x6F\x64\x2E"), NULL);
	}
}
static void InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_FailureNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanPickup_tDF63B07813DA480473C848A9D9D5E59029296C41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x43\x61\x6E\x20\x50\x69\x63\x6B\x75\x70"), NULL);
	}
}
static void CanPickup_tDF63B07813DA480473C848A9D9D5E59029296C41_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HasCategoryItem_t2B41CD4A11E61C8E5C121FE3975984124DBDEA8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x48\x61\x73\x20\x43\x61\x74\x65\x67\x6F\x72\x79\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void HasCategoryItem_t2B41CD4A11E61C8E5C121FE3975984124DBDEA8C_CustomAttributesCacheGenerator_requiredItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x48\x61\x73\x20\x47\x72\x6F\x75\x70\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator_m_RequiredGroupItem(CustomAttributesCache* cache)
{
	{
		ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F * tmp = (ItemGroupPickerAttribute_tD312A9769688F5251646DA5DD00A1FD33028635F *)cache->attributes[0];
		ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator_m_Window(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HasItem_t24EC295BBE0C5D691D9EABB060555BA1DF0CA69B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x48\x61\x73\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void HasItem_t24EC295BBE0C5D691D9EABB060555BA1DF0CA69B_CustomAttributesCacheGenerator_requiredItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4C\x6F\x63\x6B"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x69\x6E\x64\x6F\x77\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x2E"), NULL);
	}
}
static void Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator_m_State(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LockAll_tF3E2913D5EC3E5D2AE0A2F4DA07803A5B1BE764B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x4C\x6F\x63\x6B\x20\x41\x6C\x6C"), NULL);
	}
}
static void LockAll_tF3E2913D5EC3E5D2AE0A2F4DA07803A5B1BE764B_CustomAttributesCacheGenerator_m_State(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x50\x69\x63\x6B\x75\x70\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_0_0_0_var), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[3];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_DestroyWhenEmpty(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_Amount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_Pickup_U3COnStartU3Eb__4_0_m20B8279A641750A5962EEC43C0DB6F093D93C95D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Save_t62E1EC72E3C7C4408B7636B3E00B36AD15CFF038_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x53\x61\x76\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x53\x68\x6F\x77\x20\x57\x69\x6E\x64\x6F\x77"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x69\x6E\x64\x6F\x77\x20\x74\x6F\x20\x73\x68\x6F\x77\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_m_DestroyWhenEmpty(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_ShowWindow_U3COnSequenceStartU3Eb__5_0_mE8B5127A81299FDC5B31D9129E6BD811DDFF3A04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_ShowWindow_U3COnSequenceStartU3Eb__5_1_mFB0E22A61357DA278D83156338B29F01990D4DB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x52\x61\x79\x63\x61\x73\x74\x20\x4E\x6F\x74\x69\x66\x79"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var), NULL);
	}
}
static void RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator_m_SuccessNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator_m_FailureNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x43\x68\x65\x63\x6B\x20\x53\x6B\x69\x6C\x6C"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x77\x69\x6E\x64\x6F\x77\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_Skill(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[1];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_SuccessNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_FailureNotification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Close_tB332E730ACE478469DA9DD89D894365696022512_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x2F\x43\x6C\x6F\x73\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Close_tB332E730ACE478469DA9DD89D894365696022512_CustomAttributesCacheGenerator_m_WidgetName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x2F\x53\x68\x6F\x77\x20\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator_m_WidgetName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator_m_Notification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x2F\x53\x68\x6F\x77\x20\x50\x72\x6F\x67\x72\x65\x73\x73\x62\x61\x72"), NULL);
	}
}
static void ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_WidgetName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_Title(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_Duration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_RequiredIngredientsWindow(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x66\x74\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_ResultStorageWindow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_CraftingProgressbar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_CraftingTrigger_CraftItems_mF124FE6834334A72E006E5E84175D9156221722C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_0_0_0_var), NULL);
	}
}
static void CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_CraftingTrigger_CraftItem_m3FE76B80A298C4764A8874300B7E20F3EC5B3E1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_0_0_0_var), NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26__ctor_m05708EC26C38D37AB8EB955B751847FAB7A7A502(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_IDisposable_Dispose_m82915AB51DD42745B2BF0291F3903587E90F2878(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED7A8123703EA3554A6C3957508C98C877F48B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_Reset_m03ED5FA780BBAFC1BF44C998C90A7B700D99A7A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_get_Current_m8AB92C4E2BE021342710E731BEC348A687EAF066(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27__ctor_m6BE66FA9C0DD8A3C7BA790E9845C78A41C720F09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_IDisposable_Dispose_mF7F94E569233B993520A310A200FE681D03224E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m620CEE326E27BF4715D831A62854CAFC587F85CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_IEnumerator_Reset_m9944ED8FEEFABC00B312E796326D3892ADE1126D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_IEnumerator_get_Current_m448E7912969C96DD23A01894022C78EE4501FA94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_Size(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_AnimatorState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_DisplayType(CustomAttributesCache* cache)
{
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[0];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_Offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayNameType_tDBB888EA6507DED0F65D4D0F859CC298AB1200C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void DisplayTriggerTooltip_tFADDB707D0F1CCDC0EFC790986B7B2D6D02CCDBC_CustomAttributesCacheGenerator_m_Title(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DisplayTriggerTooltip_tFADDB707D0F1CCDC0EFC790986B7B2D6D02CCDBC_CustomAttributesCacheGenerator_m_Instruction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void TriggerTooltip_t453F0C4653DFAB1C55C8D727524F84C834B06005_CustomAttributesCacheGenerator_m_Title(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerTooltip_t453F0C4653DFAB1C55C8D727524F84C834B06005_CustomAttributesCacheGenerator_m_Instruction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyPriceFactor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6E\x64\x6F\x72"), NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellPriceFactor(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_PurchasedStorageWindow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_PaymentWindow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_RemoveItemAfterPurchase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuySellDialogName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x79\x20\x26\x20\x53\x65\x6C\x6C\x20\x44\x69\x61\x6C\x6F\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_DisplaySpinner(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogTitle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x79"), NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellDialogTitle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x6C"), NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellSingleDialogText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellMultipleDialogText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellDialogButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B_CustomAttributesCacheGenerator_m_Currency(CustomAttributesCache* cache)
{
	{
		CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 * tmp = (CurrencyPickerAttribute_t7E058621D9CCB6129EDC96B66E48F7EA6A0548C8 *)cache->attributes[0];
		CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D(tmp, true, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B_CustomAttributesCacheGenerator_m_HideEmptySlot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputButton_t254DE14BEF98A27DC304BA0898A7D78EA0CC427F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnAddItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnFailedToAddItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnRemoveItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnFailedToRemoveItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnTryUseItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnUseItem(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnDropItem(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_useButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[1];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x6F\x20\x75\x73\x65\x20\x69\x74\x65\x6D\x20\x69\x6E\x20\x73\x6C\x6F\x74\x2E"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_DynamicContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x73\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x61\x73\x20\x64\x79\x6E\x61\x6D\x69\x63\x2E\x20\x53\x6C\x6F\x74\x73\x20\x61\x72\x65\x20\x69\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x64\x20\x61\x74\x20\x72\x75\x6E\x74\x69\x6D\x65\x2E"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_SlotParent(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x72\x65\x6E\x74\x20\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x6F\x66\x20\x73\x6C\x6F\x74\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_SlotPrefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x6C\x6F\x74\x20\x70\x72\x65\x66\x61\x62\x2E\x20\x54\x68\x69\x73\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x63\x6F\x6E\x74\x61\x69\x6E\x20\x74\x68\x65\x20\x53\x6C\x6F\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x6F\x72\x20\x61\x20\x63\x68\x69\x6C\x64\x20\x63\x6C\x61\x73\x73\x20\x6F\x66\x20\x53\x6C\x6F\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_UseReferences(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x61\x73\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x2E\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x64\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x73\x20\x64\x6F\x6E\x27\x74\x20\x68\x6F\x6C\x64\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x69\x74\x73\x65\x6C\x66\x2C\x20\x74\x68\x65\x79\x20\x61\x72\x65\x20\x6F\x6E\x6C\x79\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x69\x6E\x67\x20\x61\x6E\x20\x69\x74\x65\x6D\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDragIn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x64\x72\x61\x67\x67\x65\x64\x20\x69\x6E\x74\x6F\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDragOut(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x64\x72\x61\x67\x67\x65\x64\x20\x6F\x75\x74\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDropItems(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x64\x72\x6F\x70\x70\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x74\x6F\x20\x67\x72\x6F\x75\x6E\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanReferenceItems(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanSellItems(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x73\x6F\x6C\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanUseItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x20\x69\x74\x65\x6D\x73\x20\x62\x65\x20\x75\x73\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x2E"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_UseContextMenu(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x63\x6F\x6E\x74\x65\x78\x74\x20\x6D\x65\x6E\x75\x20\x66\x6F\x72\x20\x69\x74\x65\x6D\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_ShowTooltips(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x6F\x77\x20\x69\x74\x65\x6D\x20\x74\x6F\x6F\x6C\x74\x69\x70\x73\x3F"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_MoveUsedItem(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x20\x6D\x6F\x76\x65\x20\x75\x73\x65\x64\x20\x69\x74\x65\x6D\x2E\x20\x4D\x6F\x76\x65\x20\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73\x20\x6E\x65\x65\x64\x73\x20\x74\x6F\x20\x62\x65\x20\x64\x65\x66\x69\x6E\x65\x64\x21"), NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnAddItem_m6F08BB8215855B35F3A4882D4C7199A01E88140F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnAddItem_mBEB15F3259A72C56DBDCEC44B655E7581862944B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnFailedToAddItem_mF136263709491EE7361ED20778D2ED92098B3AD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnFailedToAddItem_m5F5C7AE7262C71144C90C461FD403F761EE1DDF8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnRemoveItem_mAA53246093667369BD96800B5BE7337B49310595(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnRemoveItem_mD1A95A983E701FEF301966422CADED1F6B250712(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnFailedToRemoveItem_mFC55C620F984D08E5F652BAA8C9AE802CCD4466F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnFailedToRemoveItem_m070D8D92D2BED088C68AEBB705DC98A8851405FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnTryUseItem_m13E4D934B09A83CF7AACC3B3A203381F7CA2A5C6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnTryUseItem_m357C5DBF2E81B7D8E5BD6145B3FF59F710EB4147(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnUseItem_mA3FD14B23D326CFA04E5B49D1E2A32A1158B6265(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnUseItem_m6663E4FF2A9E58EFC9DBBA22B8CEACCDC067728C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnDropItem_m8682C5772C245FA7CBAC60D8003F3E7057F43F01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnDropItem_m7496A6F4ECCC526C2CE232496525DF8DBDE56A6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRefreshSlotsU3Eb__107_0_mF6C72F5C3896151C109389D48E435DC10859EE32(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRefreshSlotsU3Eb__107_1_m6144049BEF060A7BEC093D7A7706BF94A0F327D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_0_mCE609769E36DC6BA9AEF19D864F4EE26C44D8B61(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_1_mA462942C146F1C1325A8D9A2C67C34873D517BE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_2_m02F2650744A2810F336C05A2A398F9F462B74E47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_3_m24F684F75E88E23C1511354E2C4E257D1DC4D8AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_4_mA33BD64949C76B533C6C857BA3B7566879614A31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_5_m618FF67AA406E6EDB72AFBE5A98E4C87A159A5D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_6_mE1357A708738D310C3CB47D278809DBB3E9A9C6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t6618E1B854E2ED04713AA2137F2AE8DB61E32CDA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_UseKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Key(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_CooldownOverlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Cooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Description(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Ingredients(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_BuyPrice(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_ItemSlot_DelayTooltip_mCDF50D2E4203BBF294CCD3C362877EF229129E22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_0_0_0_var), NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23__ctor_mC3C9A902CAE14C3E35402B4ABC70E985052EC50A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_IDisposable_Dispose_mDBAAF22BCE80C94EFF75167DACF2A5C68702F4B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE227150E52923041CF13D04F059B3D1DF70AFA12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_get_Current_m646B7B909036413EDA2A5337E19D2265E39CCFE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SkillSlot_t6B62294F345027C0A207B6EF0229DB730D182FFD_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_ItemName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_UseRarityColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_UCDcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_Stack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_restrictions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_0_m5CD98F68009EA39E8EDB441D9887291DBEE66B4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_1_mAB216A805AF038A9A2BC7A513F96E17CA95F80D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_2_m8C54B7225F8484EA7180EBA03E79C8861BA89842(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE_0_0_0_var), NULL);
	}
}
static void Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Category_t7B7DCDC1EE4F4E2E5D41FF381172F3A48B641875_CustomAttributesCacheGenerator_m_Categories(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 * tmp = (CategoryPickerAttribute_tD830F31EF3FB01131992FE866E17365B455C2CB3 *)cache->attributes[1];
		CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64(tmp, true, NULL);
	}
}
static void Category_t7B7DCDC1EE4F4E2E5D41FF381172F3A48B641875_CustomAttributesCacheGenerator_invert(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EquipmentRegion_t7EEA2B0E41AC1E4ABD1645A2255501FC45A4723C_CustomAttributesCacheGenerator_region(CustomAttributesCache* cache)
{
	{
		EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 * tmp = (EquipmentPickerAttribute_tE6D7F84E7C3D9D03F11F0EB3F1506900F6D59487 *)cache->attributes[0];
		EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1(tmp, true, NULL);
	}
}
static void U3CU3Ec_tF76C364E3A8589B6E09D9368633D61CD5C5E3CBF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Default_t5B42050C9ECDC645E900D9651A60FD110CD6AB17_CustomAttributesCacheGenerator_queriesHitTriggers(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73"), NULL);
	}
}
static void Default_t5B42050C9ECDC645E900D9651A60FD110CD6AB17_CustomAttributesCacheGenerator_debugMessages(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
}
static void Input_t06F8D4714B40A6A3B0190254C0FD8CC375097E2F_CustomAttributesCacheGenerator_unstackEvent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x73\x74\x61\x63\x6B\x69\x6E\x67\x3A"), NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74"), NULL);
	}
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[2];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
}
static void Input_t06F8D4714B40A6A3B0190254C0FD8CC375097E2F_CustomAttributesCacheGenerator_unstackKeyCode(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x20\x43\x6F\x64\x65"), NULL);
	}
}
static void UnstackInput_t8393A683DE90275BF00CF1C2EC590A7F18248FD1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_containerFull(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x61\x69\x6E\x65\x72\x3A"), NULL);
	}
}
static void Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_selectItem(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x61\x66\x74\x69\x6E\x67\x3A"), NULL);
	}
}
static void Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_soldItem(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x6E\x64\x6F\x72\x3A"), NULL);
	}
}
static void Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_toFarAway(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72\x3A"), NULL);
	}
}
static void Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_skillGain(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6B\x69\x6C\x6C\x73\x3A"), NULL);
	}
}
static void UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_contextMenuName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x65\x78\x74\x20\x4D\x65\x6E\x75"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x43\x6F\x6E\x74\x65\x78\x74\x4D\x65\x6E\x75\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_tooltipName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x6F\x6C\x74\x69\x70"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x54\x6F\x6F\x6C\x74\x69\x70\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_sellPriceTooltipName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x69\x63\x65\x20\x54\x6F\x6F\x6C\x74\x69\x70"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x73\x65\x6C\x6C\x20\x70\x72\x69\x63\x65\x20\x74\x6F\x6F\x6C\x74\x69\x70\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_stackName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x63\x6B"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x53\x74\x61\x63\x6B\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_notificationName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x77\x69\x64\x67\x65\x74\x2E"), NULL);
	}
}
static void AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x41\x64\x64\x20\x49\x74\x65\x6D"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_Item(CustomAttributesCache* cache)
{
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[0];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_Amount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 200.0f, NULL);
	}
}
static void Cooldown_t6F12F61E4F1E47492965F9317FCF61065CC32192_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x43\x6F\x6F\x6C\x64\x6F\x77\x6E"), NULL);
	}
}
static void Cooldown_t6F12F61E4F1E47492965F9317FCF61065CC32192_CustomAttributesCacheGenerator_m_GlobalCooldown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReduceStack_tB667257C2C0845E10BF4755D22090B9239ED26AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x52\x65\x64\x75\x63\x65\x20\x53\x74\x61\x63\x6B"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void ReduceStack_tB667257C2C0845E10BF4755D22090B9239ED26AB_CustomAttributesCacheGenerator_m_Amount(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 200.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x79\x73\x74\x65\x6D\x2F\x52\x65\x6D\x6F\x76\x65\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_WindowName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_Item(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 * tmp = (ItemPickerAttribute_t46CB9A8FA32B876B7F650B6967B411AAA8F93328 *)cache->attributes[1];
		ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C(tmp, true, NULL);
	}
}
static void RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_Amount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 200.0f, NULL);
	}
}
static void ItemAction_t996F357750F6E481446619C0A4264B5634D13E36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void ItemAction_t996F357750F6E481446619C0A4264B5634D13E36_CustomAttributesCacheGenerator_item(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_InventorySystem_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_InventorySystem_AttributeGenerators[424] = 
{
	U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_CustomAttributesCacheGenerator,
	U3CU3Ec_tFC7D786C50C21B12305AC2AA098817BFF0D6D25D_CustomAttributesCacheGenerator,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator,
	CategoryAttribute_t7AA4E82C863C9C28715CF0598B213CE6DEF2D43A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t2255562419CC692D227A5C20A04A5B84F0D3A514_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_tCA7FC89013F98C71746ABE0E8DE3975BB250884C_CustomAttributesCacheGenerator,
	ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53_CustomAttributesCacheGenerator,
	U3CU3Ec__23_1_tC6CA23302C37934961EBF727D8308D85E9DA16F8_CustomAttributesCacheGenerator,
	U3CU3Ec_t050FF8165F5BE81574D32CCC7022F998D0BB9A3B_CustomAttributesCacheGenerator,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass41_0_tC45C74C65916B595C57D2D9C649DB478A4E288DA_CustomAttributesCacheGenerator,
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator,
	U3CU3Ec_t51230A50220466A7180436503B8EBE585DB5D0D2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_t6C1EC9F94DC0EBBB98B76C0B6C8E599875809823_CustomAttributesCacheGenerator,
	ItemCollectionPopulator_tA52D2A2EE6808E9A5BC3DCF4156A0F73CAAD2F30_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass115_0_t1F03362DEAA585EA6E58F3955A9826CAC59F94E1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass116_0_t4C9FB5FC569170ED2EF7A9B78C3CF9B1422D1765_CustomAttributesCacheGenerator,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator,
	PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator,
	RarityModifier_tEC87EA90A4403AD473C7458487DD9A4723A01686_CustomAttributesCacheGenerator,
	SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator,
	StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator,
	U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_CustomAttributesCacheGenerator,
	NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator,
	CanPickup_tDF63B07813DA480473C848A9D9D5E59029296C41_CustomAttributesCacheGenerator,
	HasCategoryItem_t2B41CD4A11E61C8E5C121FE3975984124DBDEA8C_CustomAttributesCacheGenerator,
	HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator,
	HasItem_t24EC295BBE0C5D691D9EABB060555BA1DF0CA69B_CustomAttributesCacheGenerator,
	Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator,
	LockAll_tF3E2913D5EC3E5D2AE0A2F4DA07803A5B1BE764B_CustomAttributesCacheGenerator,
	Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator,
	Save_t62E1EC72E3C7C4408B7636B3E00B36AD15CFF038_CustomAttributesCacheGenerator,
	ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator,
	RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator,
	CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator,
	Close_tB332E730ACE478469DA9DD89D894365696022512_CustomAttributesCacheGenerator,
	ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator,
	ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator,
	DisplayNameType_tDBB888EA6507DED0F65D4D0F859CC298AB1200C8_CustomAttributesCacheGenerator,
	Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D_CustomAttributesCacheGenerator,
	InputButton_t254DE14BEF98A27DC304BA0898A7D78EA0CC427F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A_CustomAttributesCacheGenerator,
	U3CU3Ec_t6618E1B854E2ED04713AA2137F2AE8DB61E32CDA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03_CustomAttributesCacheGenerator,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6_CustomAttributesCacheGenerator,
	U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_CustomAttributesCacheGenerator,
	Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C_CustomAttributesCacheGenerator,
	U3CU3Ec_tF76C364E3A8589B6E09D9368633D61CD5C5E3CBF_CustomAttributesCacheGenerator,
	UnstackInput_t8393A683DE90275BF00CF1C2EC590A7F18248FD1_CustomAttributesCacheGenerator,
	AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator,
	Cooldown_t6F12F61E4F1E47492965F9317FCF61065CC32192_CustomAttributesCacheGenerator,
	ReduceStack_tB667257C2C0845E10BF4755D22090B9239ED26AB_CustomAttributesCacheGenerator,
	RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator,
	ItemAction_t996F357750F6E481446619C0A4264B5634D13E36_CustomAttributesCacheGenerator,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_StartDelay,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Radius,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Repeat,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_RepeatDelay,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_m_Data,
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_Parent,
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_name,
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_EditorColor,
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_CustomAttributesCacheGenerator_m_Cooldown,
	CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0_CustomAttributesCacheGenerator_currency,
	EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_WindowName,
	EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_Database,
	EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_Bones,
	EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C_CustomAttributesCacheGenerator_m_VisibleItems,
	EquipmentBone_t5B6414B4D660A89F4A8236212120ACBA39B0C141_CustomAttributesCacheGenerator_region,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadState,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadClipSize,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ResetClipSize,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_Projectile,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileVisibility,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileSpeed,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_FirePoint,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ReloadPoint,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileItem,
	ShootableWeapon_t0E6D671E02CAC0B1003C484C77B950E297270FE8_CustomAttributesCacheGenerator_m_ProjectileItemWindow,
	VisibleItem_t295AAB6C2EE93CA6996D3A77186B78FE39D26AFC_CustomAttributesCacheGenerator_item,
	VisibleItem_t295AAB6C2EE93CA6996D3A77186B78FE39D26AFC_CustomAttributesCacheGenerator_attachments,
	Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D_CustomAttributesCacheGenerator_region,
	Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D_CustomAttributesCacheGenerator_gameObject,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ActivationInputName,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ActivationType,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_UseInputName,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_StartType,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_StopType,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKTarget,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKWeight,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_RightHandIKSpeed,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKTarget,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKWeight,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_LeftHandIKSpeed,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_ItemID,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_IdleState,
	Weapon_t2463BCD9A4D1BB807D1A8660F0078A8A51E44513_CustomAttributesCacheGenerator_m_UseState,
	EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7_CustomAttributesCacheGenerator_name,
	ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_m_ItemGeneratorData,
	ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_m_MaxAmount,
	ItemGeneratorData_t7CD907831F3BCC5A75E4462BF29AFE4E3D9ACB02_CustomAttributesCacheGenerator_item,
	ItemGeneratorData_t7CD907831F3BCC5A75E4462BF29AFE4E3D9ACB02_CustomAttributesCacheGenerator_chance,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_From,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Filters,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MinStack,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MaxStack,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MinAmount,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_MaxAmount,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Chance,
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80_CustomAttributesCacheGenerator_m_Modifiers,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_m_Database,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_m_ChildDatabases,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_onDataLoaded,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_onDataSaved,
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Items,
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Amounts,
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_m_Modifiers,
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_CustomAttributesCacheGenerator_onChange,
	ItemCollectionPopulator_tA52D2A2EE6808E9A5BC3DCF4156A0F73CAAD2F30_CustomAttributesCacheGenerator_m_ItemGroup,
	ItemCondition_t956DA12EF1F19DD6F7D8154ED039912DB064965A_CustomAttributesCacheGenerator_item,
	ItemCondition_t956DA12EF1F19DD6F7D8154ED039912DB064965A_CustomAttributesCacheGenerator_category,
	ItemContainerPopulator_t5937F1FF41D49170F0C9586A54928ADAFE52E248_CustomAttributesCacheGenerator_m_Entries,
	Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068_CustomAttributesCacheGenerator_group,
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_GroupName,
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Items,
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Amounts,
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84_CustomAttributesCacheGenerator_m_Modifiers,
	EquipmentItem_t130209391E3888A08F2D69447EE07BFF77CA18D6_CustomAttributesCacheGenerator_m_OverrideEquipPrefab,
	EquipmentItem_t130209391E3888A08F2D69447EE07BFF77CA18D6_CustomAttributesCacheGenerator_m_Region,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Id,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_ItemName,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_UseItemNameAsDisplayName,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_DisplayName,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Icon,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Prefab,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Description,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Category,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsSellable,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_BuyPrice,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CanBuyBack,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_BuyCurrency,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SellPrice,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SellCurrency,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_Stack,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_MaxStack,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsDroppable,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_DropSound,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_OverridePrefab,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_IsCraftable,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingDuration,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_UseCraftingSkill,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_SkillWindow,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingSkill,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_RemoveIngredientsWhenFailed,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_MinCraftingSkillValue,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingAnimatorState,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_m_CraftingModifier,
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_CustomAttributesCacheGenerator_properties,
	Ingredient_tF9A4CD3CF14706E1703D4F3B963CA8135CF680D4_CustomAttributesCacheGenerator_item,
	Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D_CustomAttributesCacheGenerator_m_FixedSuccessChance,
	Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D_CustomAttributesCacheGenerator_m_GainModifier,
	UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_m_UseCategoryCooldown,
	UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_m_Cooldown,
	UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_actions,
	PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_ApplyToAll,
	PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_Properties,
	PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_ModifierType,
	PropertyModifier_t4304C1306D0614F29C703E7773B3164FC864C862_CustomAttributesCacheGenerator_m_Range,
	RarityModifier_tEC87EA90A4403AD473C7458487DD9A4723A01686_CustomAttributesCacheGenerator_m_Rarities,
	SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator_m_Chance,
	SkillModifier_tE6D4035322989DA756956F10D7D04C0CEEDC6E65_CustomAttributesCacheGenerator_m_Gain,
	StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator_m_Min,
	StackModifier_t8CF8BBD86E730EF729F50E8883E652357868EC11_CustomAttributesCacheGenerator_m_Max,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_AutoDestruct,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestructDelay,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_StartDirection,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_StartPositionOffset,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_Speed,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_FollowTarget,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_SelectBestTarget,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_MaxDistance,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_FieldOfView,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_TurnSpeed,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestroyOnCollision,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_DestroyDelay,
	Projectile_t68318A7696694264C0E06C42CB783C9C43EC8118_CustomAttributesCacheGenerator_m_Data,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_name,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_m_UseAsNamePrefix,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_color,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_chance,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_multiplier,
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_CustomAttributesCacheGenerator_m_PriceMultiplier,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Target,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_ComponentName,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_MethodName,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Arguments,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Condition,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_Number,
	CompareNumberNotify_t7D3650CA8084C90737DE0A5DB0D7AEEB698A26AE_CustomAttributesCacheGenerator_m_FailureNotification,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_Target,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_ComponentName,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_MethodName,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_Arguments,
	InvokeNotify_t27DE87A337578483C91B48F3F81F61816FA09026_CustomAttributesCacheGenerator_m_FailureNotification,
	CanPickup_tDF63B07813DA480473C848A9D9D5E59029296C41_CustomAttributesCacheGenerator_m_WindowName,
	HasCategoryItem_t2B41CD4A11E61C8E5C121FE3975984124DBDEA8C_CustomAttributesCacheGenerator_requiredItems,
	HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator_m_RequiredGroupItem,
	HasGroupItem_t63CACFA3597EF56F858E0FFD2A060E9DBAA9514C_CustomAttributesCacheGenerator_m_Window,
	HasItem_t24EC295BBE0C5D691D9EABB060555BA1DF0CA69B_CustomAttributesCacheGenerator_requiredItems,
	Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator_m_WindowName,
	Lock_t2CFAA1C5EBDFB902ABD93EB7F08AE91F3FE00FCE_CustomAttributesCacheGenerator_m_State,
	LockAll_tF3E2913D5EC3E5D2AE0A2F4DA07803A5B1BE764B_CustomAttributesCacheGenerator_m_State,
	Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_WindowName,
	Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_DestroyWhenEmpty,
	Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_m_Amount,
	ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_m_WindowName,
	ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_m_DestroyWhenEmpty,
	RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator_m_SuccessNotification,
	RaycastNotify_tA70DB08D3DA02FD4ACFEB3172150933817642C87_CustomAttributesCacheGenerator_m_FailureNotification,
	CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_WindowName,
	CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_Skill,
	CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_SuccessNotification,
	CheckSkill_t5CCEDE303F0771FBEFD52D51BFEBA1DFC2C8349A_CustomAttributesCacheGenerator_m_FailureNotification,
	Close_tB332E730ACE478469DA9DD89D894365696022512_CustomAttributesCacheGenerator_m_WidgetName,
	ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator_m_WidgetName,
	ShowNotification_t80213C5EF9B1B3E4E61813FF47CCA340D295E777_CustomAttributesCacheGenerator_m_Notification,
	ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_WidgetName,
	ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_Title,
	ShowProgressbar_tACD4671548433D35414718162C0D6C9B64B13BD7_CustomAttributesCacheGenerator_m_Duration,
	CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_RequiredIngredientsWindow,
	CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_ResultStorageWindow,
	CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_m_CraftingProgressbar,
	DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_Sprite,
	DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_Size,
	DisplayCursor_tA03ECAB65FF98C6BB891DE0114FB5F256149277E_CustomAttributesCacheGenerator_m_AnimatorState,
	DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_DisplayType,
	DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_Color,
	DisplayName_tFDB259AF0855F10FC204876066936A95A38EC3FB_CustomAttributesCacheGenerator_m_Offset,
	DisplayTriggerTooltip_tFADDB707D0F1CCDC0EFC790986B7B2D6D02CCDBC_CustomAttributesCacheGenerator_m_Title,
	DisplayTriggerTooltip_tFADDB707D0F1CCDC0EFC790986B7B2D6D02CCDBC_CustomAttributesCacheGenerator_m_Instruction,
	TriggerTooltip_t453F0C4653DFAB1C55C8D727524F84C834B06005_CustomAttributesCacheGenerator_m_Title,
	TriggerTooltip_t453F0C4653DFAB1C55C8D727524F84C834B06005_CustomAttributesCacheGenerator_m_Instruction,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyPriceFactor,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellPriceFactor,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_PurchasedStorageWindow,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_PaymentWindow,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_RemoveItemAfterPurchase,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuySellDialogName,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_DisplaySpinner,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogTitle,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogText,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_BuyDialogButton,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellDialogTitle,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellSingleDialogText,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellMultipleDialogText,
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B_CustomAttributesCacheGenerator_m_SellDialogButton,
	CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B_CustomAttributesCacheGenerator_m_Currency,
	CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B_CustomAttributesCacheGenerator_m_HideEmptySlot,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnAddItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnFailedToAddItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnRemoveItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnFailedToRemoveItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnTryUseItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnUseItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_OnDropItem,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_useButton,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_DynamicContainer,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_SlotParent,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_SlotPrefab,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_UseReferences,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDragIn,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDragOut,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanDropItems,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanReferenceItems,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanSellItems,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_CanUseItems,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_UseContextMenu,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_ShowTooltips,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_m_MoveUsedItem,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_UseKey,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Key,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_CooldownOverlay,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Cooldown,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Description,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_Ingredients,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_m_BuyPrice,
	SkillSlot_t6B62294F345027C0A207B6EF0229DB730D182FFD_CustomAttributesCacheGenerator_m_Value,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_ItemName,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_UseRarityColor,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_UCDcon,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_m_Stack,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_restrictions,
	Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C_CustomAttributesCacheGenerator_item,
	Category_t7B7DCDC1EE4F4E2E5D41FF381172F3A48B641875_CustomAttributesCacheGenerator_m_Categories,
	Category_t7B7DCDC1EE4F4E2E5D41FF381172F3A48B641875_CustomAttributesCacheGenerator_invert,
	EquipmentRegion_t7EEA2B0E41AC1E4ABD1645A2255501FC45A4723C_CustomAttributesCacheGenerator_region,
	Default_t5B42050C9ECDC645E900D9651A60FD110CD6AB17_CustomAttributesCacheGenerator_queriesHitTriggers,
	Default_t5B42050C9ECDC645E900D9651A60FD110CD6AB17_CustomAttributesCacheGenerator_debugMessages,
	Input_t06F8D4714B40A6A3B0190254C0FD8CC375097E2F_CustomAttributesCacheGenerator_unstackEvent,
	Input_t06F8D4714B40A6A3B0190254C0FD8CC375097E2F_CustomAttributesCacheGenerator_unstackKeyCode,
	Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_containerFull,
	Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_selectItem,
	Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_soldItem,
	Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_toFarAway,
	Notifications_t04E8DD88D40AF65FC08CF7704D3079C1E255A4FE_CustomAttributesCacheGenerator_skillGain,
	UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_contextMenuName,
	UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_tooltipName,
	UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_sellPriceTooltipName,
	UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_stackName,
	UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1_CustomAttributesCacheGenerator_notificationName,
	AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_WindowName,
	AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_Item,
	AddItem_t572502E9BB4367626305BB061E242D28542AC2F4_CustomAttributesCacheGenerator_m_Amount,
	Cooldown_t6F12F61E4F1E47492965F9317FCF61065CC32192_CustomAttributesCacheGenerator_m_GlobalCooldown,
	ReduceStack_tB667257C2C0845E10BF4755D22090B9239ED26AB_CustomAttributesCacheGenerator_m_Amount,
	RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_WindowName,
	RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_Item,
	RemoveItem_tB3405ABBF4D410E84AE1E19F7178FA36FFBC960A_CustomAttributesCacheGenerator_m_Amount,
	ItemAction_t996F357750F6E481446619C0A4264B5634D13E36_CustomAttributesCacheGenerator_item,
	AreaOfEffect_t1331324D9811E9971FF266ADE06A0D3A13078EBF_CustomAttributesCacheGenerator_AreaOfEffect_Start_m1BCF771F26D8968CFE154EFB7A768BEF77B6DF8C,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5__ctor_mDD0FCA4B6533BB95B3546AC33074448CB76D9693,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_IDisposable_Dispose_m54A36E781AE43ABCDAB8A1519129451345E14150,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D5E1F2EC2C0F803F81E71054DA06B5F02A1EEE4,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m2E89927814E781AFD1A1114C71DC06BFF9538222,
	U3CStartU3Ed__5_t7663F35A59396C58ACF22E677A5850887AC174E2_CustomAttributesCacheGenerator_U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m21DA2F9D0680E2C3450473D914ACD575EC2130AA,
	ItemGenerator_tD2982597CC9F4D7396E228B9137918B788AB04F3_CustomAttributesCacheGenerator_ItemGenerator_U3CGenerateItemsU3Eb__3_0_m674BC7BF5C695E2D0CEB7586F350F7EF62C8A8A8,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_GetBounds_m7490F78E6B2A3B1806058F81D20E63F53C816E53,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_DelayedLoading_mC2BAB7CBA7F1F2CF9F329B7DB66264EE52C056A3,
	InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_CustomAttributesCacheGenerator_InventoryManager_RepeatSaving_m5D8702EC560A6F60DC5CF51077CDBB350BE26F0E,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37__ctor_m3325202AFC5623802BE84D44B0A7DDC9DE29095E,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_IDisposable_Dispose_m71FFD192424C855DB335AC3AD8EB218444508066,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE895F35F30A0DC7759F22C9E344873FD0F449B,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_Reset_m5E31818C9301990803658065761372C698261518,
	U3CDelayedLoadingU3Ed__37_tBD396783A5E29F4C5ACEB7505CAE3934EEC516C9_CustomAttributesCacheGenerator_U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_get_Current_m3B16221AB84E47B898C986B4DDC35AF4D0511C1C,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38__ctor_m6BABEB7DF2E5AA16D319ED98FCF12AD557763172,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_IDisposable_Dispose_m06C133222BF9EF721288B8864DD7BC84857CC580,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9434F8BE699510BBA853AE850EDA9590EE455611,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_Reset_m4E523C49553E9E05315E213B36ED6E4A9BE347A5,
	U3CRepeatSavingU3Ed__38_tFB73DA91EC558C7C01F897907B017EF4333540B0_CustomAttributesCacheGenerator_U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_get_Current_m5BC6CCD95656D52496CF70BBEDFB46BBC364A187,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_0_m3B3F48AA98F934853BDB4646CB7A0DC823F031EF,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_1_m77CBC2BC2BA3096537EEF5F026EED3C48B19664E,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_2_m808DCA78EC4202BB44A393415ED0270388CB6730,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_3_mF0E9185A251DD88FFAE0ABDAE689984884A2B9E2,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_4_m96F50044707291B8066224E9D89807BE41574364,
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906_CustomAttributesCacheGenerator_ItemDatabase_U3CMergeU3Eb__11_5_mF41FB56C477F6EE7E2939F43286360F76F89BAD6,
	UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB_CustomAttributesCacheGenerator_UsableItem_SequenceCoroutine_m318A407A7B94147DD078E37D2D08C43C775EA045,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9__ctor_mD0D6AB8E654C1550E7FCD108F6B70B84E7BB1B05,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_IDisposable_Dispose_m12DBD516F68B381F1311AF75F52DC4079F86D56C,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m887024614CCE999B4A10D64456BC90F39F930864,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863,
	U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348_CustomAttributesCacheGenerator_U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_m2DC9FF56B6AA56E737B6002C8AE95781725548AD,
	NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator_NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1,
	Pickup_t0831EAA72D7A43AAAB08A4B1BFAB7CE0B444FDA7_CustomAttributesCacheGenerator_Pickup_U3COnStartU3Eb__4_0_m20B8279A641750A5962EEC43C0DB6F093D93C95D,
	ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_ShowWindow_U3COnSequenceStartU3Eb__5_0_mE8B5127A81299FDC5B31D9129E6BD811DDFF3A04,
	ShowWindow_tC1E6A95A2670BFFE5382864E4D1008BE1F957AB8_CustomAttributesCacheGenerator_ShowWindow_U3COnSequenceStartU3Eb__5_1_mFB0E22A61357DA278D83156338B29F01990D4DB3,
	CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_CraftingTrigger_CraftItems_mF124FE6834334A72E006E5E84175D9156221722C,
	CraftingTrigger_tC7AFB8919125119DD2817750E7676F4F03DFA675_CustomAttributesCacheGenerator_CraftingTrigger_CraftItem_m3FE76B80A298C4764A8874300B7E20F3EC5B3E1F,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26__ctor_m05708EC26C38D37AB8EB955B751847FAB7A7A502,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_IDisposable_Dispose_m82915AB51DD42745B2BF0291F3903587E90F2878,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED7A8123703EA3554A6C3957508C98C877F48B1,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_Reset_m03ED5FA780BBAFC1BF44C998C90A7B700D99A7A2,
	U3CCraftItemsU3Ed__26_t9B23602DE5393B78853DFD5245AFFEEAF0BD6002_CustomAttributesCacheGenerator_U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_get_Current_m8AB92C4E2BE021342710E731BEC348A687EAF066,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27__ctor_m6BE66FA9C0DD8A3C7BA790E9845C78A41C720F09,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_IDisposable_Dispose_mF7F94E569233B993520A310A200FE681D03224E0,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m620CEE326E27BF4715D831A62854CAFC587F85CE,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_IEnumerator_Reset_m9944ED8FEEFABC00B312E796326D3892ADE1126D,
	U3CCraftItemU3Ed__27_t27BFFB3435455A999856B08B6DE5C5729E6771DD_CustomAttributesCacheGenerator_U3CCraftItemU3Ed__27_System_Collections_IEnumerator_get_Current_m448E7912969C96DD23A01894022C78EE4501FA94,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnAddItem_m6F08BB8215855B35F3A4882D4C7199A01E88140F,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnAddItem_mBEB15F3259A72C56DBDCEC44B655E7581862944B,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnFailedToAddItem_mF136263709491EE7361ED20778D2ED92098B3AD9,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnFailedToAddItem_m5F5C7AE7262C71144C90C461FD403F761EE1DDF8,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnRemoveItem_mAA53246093667369BD96800B5BE7337B49310595,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnRemoveItem_mD1A95A983E701FEF301966422CADED1F6B250712,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnFailedToRemoveItem_mFC55C620F984D08E5F652BAA8C9AE802CCD4466F,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnFailedToRemoveItem_m070D8D92D2BED088C68AEBB705DC98A8851405FC,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnTryUseItem_m13E4D934B09A83CF7AACC3B3A203381F7CA2A5C6,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnTryUseItem_m357C5DBF2E81B7D8E5BD6145B3FF59F710EB4147,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnUseItem_mA3FD14B23D326CFA04E5B49D1E2A32A1158B6265,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnUseItem_m6663E4FF2A9E58EFC9DBBA22B8CEACCDC067728C,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_add_OnDropItem_m8682C5772C245FA7CBAC60D8003F3E7057F43F01,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_remove_OnDropItem_m7496A6F4ECCC526C2CE232496525DF8DBDE56A6F,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRefreshSlotsU3Eb__107_0_mF6C72F5C3896151C109389D48E435DC10859EE32,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRefreshSlotsU3Eb__107_1_m6144049BEF060A7BEC093D7A7706BF94A0F327D2,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_0_mCE609769E36DC6BA9AEF19D864F4EE26C44D8B61,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_1_mA462942C146F1C1325A8D9A2C67C34873D517BE7,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_2_m02F2650744A2810F336C05A2A398F9F462B74E47,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_3_m24F684F75E88E23C1511354E2C4E257D1DC4D8AA,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_4_mA33BD64949C76B533C6C857BA3B7566879614A31,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_5_m618FF67AA406E6EDB72AFBE5A98E4C87A159A5D1,
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5_CustomAttributesCacheGenerator_ItemContainer_U3CRegisterCallbacksU3Eb__118_6_mE1357A708738D310C3CB47D278809DBB3E9A9C6E,
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_CustomAttributesCacheGenerator_ItemSlot_DelayTooltip_mCDF50D2E4203BBF294CCD3C362877EF229129E22,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23__ctor_mC3C9A902CAE14C3E35402B4ABC70E985052EC50A,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_IDisposable_Dispose_mDBAAF22BCE80C94EFF75167DACF2A5C68702F4B7,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE227150E52923041CF13D04F059B3D1DF70AFA12,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40,
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48_CustomAttributesCacheGenerator_U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_get_Current_m646B7B909036413EDA2A5337E19D2265E39CCFE0,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_0_m5CD98F68009EA39E8EDB441D9887291DBEE66B4E,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_1_mAB216A805AF038A9A2BC7A513F96E17CA95F80D2,
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97_CustomAttributesCacheGenerator_Slot_U3CStartU3Eb__21_2_m8C54B7225F8484EA7180EBA03E79C8861BA89842,
	NotificationExtension_t02C04D788DCCD2A70C0C40A0D865D3458D38A87A_CustomAttributesCacheGenerator_NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1____replacements1,
	DevionGames_InventorySystem_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
