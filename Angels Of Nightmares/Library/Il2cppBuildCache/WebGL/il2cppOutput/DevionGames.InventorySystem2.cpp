﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4>
struct VirtActionInvoker4
{
	typedef void (*Action)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t606AA85440D2767289243C98586DE3C541D183D0;
// System.Func`2<UnityEngine.Collider,System.Boolean>
struct Func_2_tA51C8146EA8F24475C3727C2FB08F4A1B9AD9D83;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62;
// System.Collections.Generic.List`1<DevionGames.Action>
struct List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42;
// System.Collections.Generic.List`1<DevionGames.BaseTrigger>
struct List_1_tD4FB55FD3222A9EDA27BDB2EC7AB011A358F2190;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.Item>
struct List_1_t2772AD0C301E5E39A5101F3577A9F6D8CDE4D91B;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemModifierList>
struct List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<DevionGames.ObjectProperty>
struct List_1_t44F9CAAA96CC3DAE67BE460143C5EB821214ADCE;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.Restriction>
struct List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E;
// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_tEB4537E121ED7128292F5E49486823EB846576FE;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot>
struct List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE;
// System.Collections.Generic.List`1<DevionGames.UIWidgets.StringPairSlot>
struct List_1_t400A86024C0EA3AAD0CA723BB0317E0B2F22532D;
// System.Collections.Generic.List`1<DevionGames.UIWidgets.UIWidget>
struct List_1_t0EEC4246600C77B5E7CF41453C8A2C10EEA04EED;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.VisibleItem>
struct List_1_t2BAC24224203EB60F9A70D6B629A6F700EB81057;
// System.Collections.Generic.List`1<DevionGames.CallbackHandler/Entry>
struct List_1_tA6B1EE7CD310767E07B8EDC08D5F5C1506C31A82;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentHandler/EquipmentBone>
struct List_1_t95949B58FE659DBC2F71E2008085864DE4DD496E;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.Item/Ingredient>
struct List_1_tE9004C4523F5A08490EA3F73C43C507E7194574F;
// System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemContainer/MoveItemCondition>
struct List_1_t960FB04E8119A41E33517256BFE9A9027EC1B9F4;
// System.Predicate`1<System.String>
struct Predicate_1_t782ED4F640A61B7ACA1DD787F7C4993C746207B3;
// DevionGames.UIWidgets.TweenRunner`1<DevionGames.UIWidgets.FloatTween>
struct TweenRunner_1_t0CF4BBD8446B3BA1A85660580655539A7B35E50C;
// DevionGames.UIWidgets.TweenRunner`1<DevionGames.UIWidgets.Vector3Tween>
struct TweenRunner_1_t43CAACE55716A741A154B38CAC7381374111B29C;
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123;
// UnityEngine.AnimatorStateInfo[]
struct AnimatorStateInfoU5BU5D_t5493CE60099D0DB79ADFDE73948BBC5832E28572;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
// DevionGames.InventorySystem.CurrencyConversion[]
struct CurrencyConversionU5BU5D_t6F495C7F9CFC6A6FBB9CFDF9AF3B00CEAAED2975;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// DevionGames.IAction[]
struct IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5;
// DevionGames.InventorySystem.IGenerator[]
struct IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1;
// DevionGames.ITriggerEventHandler[]
struct ITriggerEventHandlerU5BU5D_tED0AC395E4838FDB2E2A59DA004F78787F1282DF;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// DevionGames.InventorySystem.Item[]
struct ItemU5BU5D_t7735267E2EAA8DEAFA53553BCD5512DA27749997;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.ScriptableObject[]
struct ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208;
// UnityEngine.UI.Scrollbar[]
struct ScrollbarU5BU5D_t81EAF2E6C2496DFC879A8478FD3FA6AFA53A7CF4;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// DevionGames.ActionTemplate
struct ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// DevionGames.InventorySystem.Category
struct Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// DevionGames.UIWidgets.ContextMenu
struct ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// DevionGames.InventorySystem.Currency
struct Currency_tC5AA8C667005127F940597BF06CF49D395F55308;
// DevionGames.InventorySystem.CurrencyConversion
struct CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0;
// DevionGames.InventorySystem.CurrencySlot
struct CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// DevionGames.UIWidgets.DialogBox
struct DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486;
// DevionGames.InventorySystem.EquipmentHandler
struct EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C;
// DevionGames.InventorySystem.EquipmentRegion
struct EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// DevionGames.InventorySystem.IGenerator
struct IGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// DevionGames.InventorySystem.Item
struct Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5;
// DevionGames.InventorySystem.ItemCollection
struct ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803;
// DevionGames.InventorySystem.ItemContainer
struct ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5;
// DevionGames.InventorySystem.ItemDatabase
struct ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906;
// DevionGames.InventorySystem.ItemGroup
struct ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84;
// DevionGames.InventorySystem.ItemGroupGenerator
struct ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80;
// DevionGames.InventorySystem.ItemModifierList
struct ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19;
// DevionGames.InventorySystem.ItemSlot
struct ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// DevionGames.UIWidgets.Notification
struct Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// DevionGames.PlayerInfo
struct PlayerInfo_t2F08B1BD9401335FD1DF02191A55DA4FC68E9748;
// DevionGames.InventorySystem.Rarity
struct Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// DevionGames.SelectableObject
struct SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3;
// DevionGames.Sequence
struct Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5;
// DevionGames.InventorySystem.Skill
struct Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D;
// DevionGames.InventorySystem.Slot
struct Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97;
// DevionGames.UIWidgets.Spinner
struct Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// DevionGames.InventorySystem.Stack
struct Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C;
// System.String
struct String_t;
// DevionGames.UIWidgets.StringPairSlot
struct StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// DevionGames.UIWidgets.Tooltip
struct Tooltip_t84784622E964293493EBDE375C05FB4BD736F859;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// DevionGames.InventorySystem.Trigger
struct Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723;
// System.Type
struct Type_t;
// DevionGames.InventorySystem.Configuration.UI
struct UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1;
// DevionGames.UIWidgets.UIWidget
struct UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// DevionGames.InventorySystem.UsableItem
struct UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB;
// DevionGames.InventorySystem.VendorTrigger
struct VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0
struct U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0
struct U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0
struct U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0
struct U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0
struct U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0
struct U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0
struct U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A;
// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0
struct U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B;
// DevionGames.InventorySystem.ItemContainer/AddItemDelegate
struct AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D;
// DevionGames.InventorySystem.ItemContainer/DropItemDelegate
struct DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D;
// DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate
struct FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC;
// DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate
struct FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397;
// DevionGames.InventorySystem.ItemContainer/MoveItemCondition
struct MoveItemCondition_tC1339BB8BB4F28B301FB7B505F06EB4D130263B6;
// DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate
struct RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029;
// DevionGames.InventorySystem.ItemContainer/UseItemDelegate
struct UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B;
// ItemContainerCallbackTester/<>c
struct U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4;
// DevionGames.InventorySystem.ItemContainerPopulator/Entry
struct Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1
struct U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2
struct U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3
struct U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4
struct U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9;
// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5
struct U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482;
// DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53;
// DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23
struct U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48;
// DevionGames.InventorySystem.ItemSlot/DragObject
struct DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82;
// DevionGames.InventorySystem.LoadSaveMenu/<>c
struct U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE;
// DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6;
// DevionGames.InventorySystem.Projectile/<>c
struct U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E;
// DevionGames.UIWidgets.Spinner/SpinnerEvent
struct SpinnerEvent_t3E9EC0B8E5BF9928D00230B8C3F8945BB85CCA3A;
// DevionGames.UIWidgets.Spinner/SpinnerTextEvent
struct SpinnerTextEvent_t421ED044855D0F19B55CDC413D30DE08E0E45D63;
// DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9
struct U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348;
// DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0
struct U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063;
// DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D;
// DevionGames.InventorySystem.VisibleItem/Attachment
struct Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D;

IL2CPP_EXTERN_C RuntimeClass* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral5FD3DCB5900804C3AA9796D311531F3F35786E5F;
IL2CPP_EXTERN_C String_t* _stringLiteral6751E43009A5D229BF6DFB516C94CAD79085EEFD;
IL2CPP_EXTERN_C String_t* _stringLiteral68B8DEBED212A277911E2A4827C5A0E30BF70D94;
IL2CPP_EXTERN_C String_t* _stringLiteral9B89EEF4B61C471E75E330C2A4B3D6F43C4E1D80;
IL2CPP_EXTERN_C String_t* _stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE;
IL2CPP_EXTERN_C String_t* _stringLiteralAE128C797A08DBCB47C70B7C8D835C48499DA55B;
IL2CPP_EXTERN_C String_t* _stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1;
IL2CPP_EXTERN_C String_t* _stringLiteralDCA36CD63E715AA87DDBAECD8BCC4400878520B1;
IL2CPP_EXTERN_C String_t* _stringLiteralE11FFF0DDA0500DDDD1C0EA92C63BAE06CA56A5B;
IL2CPP_EXTERN_C String_t* _stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_mEB6CBE76B608B9EDBA8E49E84569D09A444243F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_m6768E89F745979BABEE8FB850B0134DDF6A7EAB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mA1779277BB07CE3D2CB8E340CEA85C40C6B52354_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisTrigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_m71F7EE5A1C76E4BCD5D1B61B9CD06CFDACC5E0FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponents_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_mB05DD18198B23D82EDD9CD9ED0055E90780B1215_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponents_TisIGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32_m8E4BF18040DAB58B180D5B23B7B72B23ACCB4C39_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Currency_tC5AA8C667005127F940597BF06CF49D395F55308_0_0_0_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486;
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_t9A79A7EED5838CECDC32AB2E0A4C369A6D9DF123* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.ScriptableObject>
struct List_1_tEB4537E121ED7128292F5E49486823EB846576FE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tEB4537E121ED7128292F5E49486823EB846576FE, ____items_1)); }
	inline ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* get__items_1() const { return ____items_1; }
	inline ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tEB4537E121ED7128292F5E49486823EB846576FE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tEB4537E121ED7128292F5E49486823EB846576FE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tEB4537E121ED7128292F5E49486823EB846576FE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tEB4537E121ED7128292F5E49486823EB846576FE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tEB4537E121ED7128292F5E49486823EB846576FE_StaticFields, ____emptyArray_5)); }
	inline ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ScriptableObjectU5BU5D_tA5117515D714DD945669F5BAE310FC6F26311208* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// DevionGames.InventorySystem.CurrencyConversion
struct CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0  : public RuntimeObject
{
public:
	// System.Single DevionGames.InventorySystem.CurrencyConversion::factor
	float ___factor_0;
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.CurrencyConversion::currency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___currency_1;

public:
	inline static int32_t get_offset_of_factor_0() { return static_cast<int32_t>(offsetof(CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0, ___factor_0)); }
	inline float get_factor_0() const { return ___factor_0; }
	inline float* get_address_of_factor_0() { return &___factor_0; }
	inline void set_factor_0(float value)
	{
		___factor_0 = value;
	}

	inline static int32_t get_offset_of_currency_1() { return static_cast<int32_t>(offsetof(CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0, ___currency_1)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_currency_1() const { return ___currency_1; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_currency_1() { return &___currency_1; }
	inline void set_currency_1(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___currency_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currency_1), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0
struct U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA  : public RuntimeObject
{
public:
	// System.String DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::idOrName
	String_t* ___idOrName_0;

public:
	inline static int32_t get_offset_of_idOrName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA, ___idOrName_0)); }
	inline String_t* get_idOrName_0() const { return ___idOrName_0; }
	inline String_t** get_address_of_idOrName_0() { return &___idOrName_0; }
	inline void set_idOrName_0(String_t* value)
	{
		___idOrName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idOrName_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0
struct U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC  : public RuntimeObject
{
public:
	// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::inherit
	bool ___inherit_0;
	// System.Type DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::type
	Type_t * ___type_1;

public:
	inline static int32_t get_offset_of_inherit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC, ___inherit_0)); }
	inline bool get_inherit_0() const { return ___inherit_0; }
	inline bool* get_address_of_inherit_0() { return &___inherit_0; }
	inline void set_inherit_0(bool value)
	{
		___inherit_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_1), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0
struct U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B  : public RuntimeObject
{
public:
	// System.Type DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0
struct U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0::item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52, ___item_0)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_item_0() const { return ___item_0; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0
struct U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.CurrencyConversion DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0::conversion
	CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0 * ___conversion_0;

public:
	inline static int32_t get_offset_of_conversion_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03, ___conversion_0)); }
	inline CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0 * get_conversion_0() const { return ___conversion_0; }
	inline CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0 ** get_address_of_conversion_0() { return &___conversion_0; }
	inline void set_conversion_0(CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0 * value)
	{
		___conversion_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___conversion_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0
struct U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0::defaultCurrency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___defaultCurrency_0;

public:
	inline static int32_t get_offset_of_defaultCurrency_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A, ___defaultCurrency_0)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_defaultCurrency_0() const { return ___defaultCurrency_0; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_defaultCurrency_0() { return &___defaultCurrency_0; }
	inline void set_defaultCurrency_0(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___defaultCurrency_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultCurrency_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0
struct U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0::item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A, ___item_0)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_item_0() const { return ___item_0; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0
struct U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0::payCurrency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___payCurrency_0;

public:
	inline static int32_t get_offset_of_payCurrency_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B, ___payCurrency_0)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_payCurrency_0() const { return ___payCurrency_0; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_payCurrency_0() { return &___payCurrency_0; }
	inline void set_payCurrency_0(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___payCurrency_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___payCurrency_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainer/MoveItemCondition
struct MoveItemCondition_tC1339BB8BB4F28B301FB7B505F06EB4D130263B6  : public RuntimeObject
{
public:
	// System.String DevionGames.InventorySystem.ItemContainer/MoveItemCondition::window
	String_t* ___window_0;
	// System.Boolean DevionGames.InventorySystem.ItemContainer/MoveItemCondition::requiresVisibility
	bool ___requiresVisibility_1;

public:
	inline static int32_t get_offset_of_window_0() { return static_cast<int32_t>(offsetof(MoveItemCondition_tC1339BB8BB4F28B301FB7B505F06EB4D130263B6, ___window_0)); }
	inline String_t* get_window_0() const { return ___window_0; }
	inline String_t** get_address_of_window_0() { return &___window_0; }
	inline void set_window_0(String_t* value)
	{
		___window_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___window_0), (void*)value);
	}

	inline static int32_t get_offset_of_requiresVisibility_1() { return static_cast<int32_t>(offsetof(MoveItemCondition_tC1339BB8BB4F28B301FB7B505F06EB4D130263B6, ___requiresVisibility_1)); }
	inline bool get_requiresVisibility_1() const { return ___requiresVisibility_1; }
	inline bool* get_address_of_requiresVisibility_1() { return &___requiresVisibility_1; }
	inline void set_requiresVisibility_1(bool value)
	{
		___requiresVisibility_1 = value;
	}
};


// ItemContainerCallbackTester/<>c
struct U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields
{
public:
	// ItemContainerCallbackTester/<>c ItemContainerCallbackTester/<>c::<>9
	U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * ___U3CU3E9_0;
	// DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate ItemContainerCallbackTester/<>c::<>9__0_0
	RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * ___U3CU3E9__0_0_1;
	// DevionGames.InventorySystem.ItemContainer/AddItemDelegate ItemContainerCallbackTester/<>c::<>9__0_1
	AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * ___U3CU3E9__0_1_2;
	// DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate ItemContainerCallbackTester/<>c::<>9__0_2
	FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * ___U3CU3E9__0_2_3;
	// DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate ItemContainerCallbackTester/<>c::<>9__0_3
	FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * ___U3CU3E9__0_3_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields, ___U3CU3E9__0_0_1)); }
	inline RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields, ___U3CU3E9__0_1_2)); }
	inline AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * get_U3CU3E9__0_1_2() const { return ___U3CU3E9__0_1_2; }
	inline AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D ** get_address_of_U3CU3E9__0_1_2() { return &___U3CU3E9__0_1_2; }
	inline void set_U3CU3E9__0_1_2(AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * value)
	{
		___U3CU3E9__0_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_1_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields, ___U3CU3E9__0_2_3)); }
	inline FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * get_U3CU3E9__0_2_3() const { return ___U3CU3E9__0_2_3; }
	inline FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC ** get_address_of_U3CU3E9__0_2_3() { return &___U3CU3E9__0_2_3; }
	inline void set_U3CU3E9__0_2_3(FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * value)
	{
		___U3CU3E9__0_2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields, ___U3CU3E9__0_3_4)); }
	inline FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * get_U3CU3E9__0_3_4() const { return ___U3CU3E9__0_3_4; }
	inline FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 ** get_address_of_U3CU3E9__0_3_4() { return &___U3CU3E9__0_3_4; }
	inline void set_U3CU3E9__0_3_4(FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * value)
	{
		___U3CU3E9__0_3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_3_4), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemContainerPopulator/Entry
struct Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068  : public RuntimeObject
{
public:
	// System.String DevionGames.InventorySystem.ItemContainerPopulator/Entry::name
	String_t* ___name_0;
	// DevionGames.InventorySystem.ItemGroup DevionGames.InventorySystem.ItemContainerPopulator/Entry::group
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * ___group_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_group_1() { return static_cast<int32_t>(offsetof(Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068, ___group_1)); }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * get_group_1() const { return ___group_1; }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 ** get_address_of_group_1() { return &___group_1; }
	inline void set_group_1(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * value)
	{
		___group_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___group_1), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940  : public RuntimeObject
{
public:
	// System.String DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0::y
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339, ___y_0)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_y_0() const { return ___y_0; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1
struct U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1::y
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08, ___y_0)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_y_0() const { return ___y_0; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2
struct U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2::y
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E, ___y_0)); }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * get_y_0() const { return ___y_0; }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3
struct U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Category DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3::y
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942, ___y_0)); }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * get_y_0() const { return ___y_0; }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4
struct U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4::y
	EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9, ___y_0)); }
	inline EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * get_y_0() const { return ___y_0; }
	inline EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5
struct U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.ItemGroup DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5::y
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * ___y_0;

public:
	inline static int32_t get_offset_of_y_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482, ___y_0)); }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * get_y_0() const { return ___y_0; }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 ** get_address_of_y_0() { return &___y_0; }
	inline void set_y_0(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * value)
	{
		___y_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___y_0), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53  : public RuntimeObject
{
public:
	// System.Int32 DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::i
	int32_t ___i_0;
	// DevionGames.InventorySystem.ItemGroupGenerator DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::<>4__this
	ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53, ___U3CU3E4__this_1)); }
	inline ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23
struct U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48  : public RuntimeObject
{
public:
	// System.Int32 DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::delay
	float ___delay_2;
	// DevionGames.InventorySystem.ItemSlot DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::<>4__this
	ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * ___U3CU3E4__this_3;
	// System.Single DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::<time>5__1
	float ___U3CtimeU3E5__1_4;
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::<currency>5__2
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___U3CcurrencyU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___U3CU3E4__this_3)); }
	inline ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtimeU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___U3CtimeU3E5__1_4)); }
	inline float get_U3CtimeU3E5__1_4() const { return ___U3CtimeU3E5__1_4; }
	inline float* get_address_of_U3CtimeU3E5__1_4() { return &___U3CtimeU3E5__1_4; }
	inline void set_U3CtimeU3E5__1_4(float value)
	{
		___U3CtimeU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrencyU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48, ___U3CcurrencyU3E5__2_5)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_U3CcurrencyU3E5__2_5() const { return ___U3CcurrencyU3E5__2_5; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_U3CcurrencyU3E5__2_5() { return &___U3CcurrencyU3E5__2_5; }
	inline void set_U3CcurrencyU3E5__2_5(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___U3CcurrencyU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcurrencyU3E5__2_5), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemSlot/DragObject
struct DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.ItemSlot/DragObject::container
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___container_0;
	// DevionGames.InventorySystem.Slot DevionGames.InventorySystem.ItemSlot/DragObject::slot
	Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot_1;
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemSlot/DragObject::item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82, ___container_0)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_container_0() const { return ___container_0; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___container_0), (void*)value);
	}

	inline static int32_t get_offset_of_slot_1() { return static_cast<int32_t>(offsetof(DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82, ___slot_1)); }
	inline Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * get_slot_1() const { return ___slot_1; }
	inline Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 ** get_address_of_slot_1() { return &___slot_1; }
	inline void set_slot_1(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * value)
	{
		___slot_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slot_1), (void*)value);
	}

	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82, ___item_2)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_item_2() const { return ___item_2; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_2), (void*)value);
	}
};


// DevionGames.InventorySystem.LoadSaveMenu/<>c
struct U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_StaticFields
{
public:
	// DevionGames.InventorySystem.LoadSaveMenu/<>c DevionGames.InventorySystem.LoadSaveMenu/<>c::<>9
	U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * ___U3CU3E9_0;
	// System.Predicate`1<System.String> DevionGames.InventorySystem.LoadSaveMenu/<>c::<>9__3_0
	Predicate_1_t782ED4F640A61B7ACA1DD787F7C4993C746207B3 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Predicate_1_t782ED4F640A61B7ACA1DD787F7C4993C746207B3 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Predicate_1_t782ED4F640A61B7ACA1DD787F7C4993C746207B3 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Predicate_1_t782ED4F640A61B7ACA1DD787F7C4993C746207B3 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__3_0_1), (void*)value);
	}
};


// DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6  : public RuntimeObject
{
public:
	// System.String DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}
};


// DevionGames.InventorySystem.Projectile/<>c
struct U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_StaticFields
{
public:
	// DevionGames.InventorySystem.Projectile/<>c DevionGames.InventorySystem.Projectile/<>c::<>9
	U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Collider,System.Boolean> DevionGames.InventorySystem.Projectile/<>c::<>9__18_0
	Func_2_tA51C8146EA8F24475C3727C2FB08F4A1B9AD9D83 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_tA51C8146EA8F24475C3727C2FB08F4A1B9AD9D83 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_tA51C8146EA8F24475C3727C2FB08F4A1B9AD9D83 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_tA51C8146EA8F24475C3727C2FB08F4A1B9AD9D83 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__18_0_1), (void*)value);
	}
};


// DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9
struct U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348  : public RuntimeObject
{
public:
	// System.Int32 DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DevionGames.InventorySystem.UsableItem DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::<>4__this
	UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348, ___U3CU3E4__this_2)); }
	inline UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0
struct U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item_0;
	// DevionGames.InventorySystem.VendorTrigger DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::<>4__this
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063, ___item_0)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_item_0() const { return ___item_0; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063, ___U3CU3E4__this_1)); }
	inline VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item_0;
	// DevionGames.InventorySystem.VendorTrigger DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::<>4__this
	VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D, ___item_0)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_item_0() const { return ___item_0; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D, ___U3CU3E4__this_1)); }
	inline VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// DevionGames.ActionStatus
struct ActionStatus_tC476CE714B655D15E03E5318377FFB06C7170EA5 
{
public:
	// System.Int32 DevionGames.ActionStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionStatus_tC476CE714B655D15E03E5318377FFB06C7170EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.CursorLockMode
struct CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// DevionGames.InventorySystem.InputButton
struct InputButton_t254DE14BEF98A27DC304BA0898A7D78EA0CC427F 
{
public:
	// System.Int32 DevionGames.InventorySystem.InputButton::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputButton_t254DE14BEF98A27DC304BA0898A7D78EA0CC427F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DevionGames.BaseTrigger/TriggerInputType
struct TriggerInputType_t625D91275DC8F4E0D10B0709F654548DA96BEB97 
{
public:
	// System.Int32 DevionGames.BaseTrigger/TriggerInputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriggerInputType_t625D91275DC8F4E0D10B0709F654548DA96BEB97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.UIWidgets.EasingEquations/EaseType
struct EaseType_t27288851DEE2775DAE02A5E03C1DF75A07B0A94E 
{
public:
	// System.Int32 DevionGames.UIWidgets.EasingEquations/EaseType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EaseType_t27288851DEE2775DAE02A5E03C1DF75A07B0A94E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Projectile/LookType
struct LookType_tDB9AE08965347688DDBB4D3AB8BFF7B3830E42F0 
{
public:
	// System.Int32 DevionGames.InventorySystem.Projectile/LookType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LookType_tDB9AE08965347688DDBB4D3AB8BFF7B3830E42F0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Projectile/StartDirection
struct StartDirection_tA442D389291B07DB3821CCD65E4339FAFFA8A972 
{
public:
	// System.Int32 DevionGames.InventorySystem.Projectile/StartDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StartDirection_tA442D389291B07DB3821CCD65E4339FAFFA8A972, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.PropertyModifier/PropertyModifierType
struct PropertyModifierType_t6F0133B3DC53E0A49C1450E40A1A9851710512DA 
{
public:
	// System.Int32 DevionGames.InventorySystem.PropertyModifier/PropertyModifierType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyModifierType_t6F0133B3DC53E0A49C1450E40A1A9851710512DA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Configuration.SavingLoading/SavingProvider
struct SavingProvider_tBD92AAF99D0CAFCED96839F0FBF5415019F900E8 
{
public:
	// System.Int32 DevionGames.InventorySystem.Configuration.SavingLoading/SavingProvider::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SavingProvider_tBD92AAF99D0CAFCED96839F0FBF5415019F900E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.ShootableWeapon/ProjectileVisibility
struct ProjectileVisibility_tA0201D5C6EAAB61613EF1F4B386E1949B1D8184A 
{
public:
	// System.Int32 DevionGames.InventorySystem.ShootableWeapon/ProjectileVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProjectileVisibility_tA0201D5C6EAAB61613EF1F4B386E1949B1D8184A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Trigger/FailureCause
struct FailureCause_tF9277C16E79E5F661D0B95582FCA99BCA02AE84A 
{
public:
	// System.Int32 DevionGames.InventorySystem.Trigger/FailureCause::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FailureCause_tF9277C16E79E5F661D0B95582FCA99BCA02AE84A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.VisibleItem/Attachment
struct Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D  : public RuntimeObject
{
public:
	// DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.VisibleItem/Attachment::region
	EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * ___region_0;
	// UnityEngine.GameObject DevionGames.InventorySystem.VisibleItem/Attachment::prefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___prefab_1;
	// UnityEngine.Vector3 DevionGames.InventorySystem.VisibleItem/Attachment::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_2;
	// UnityEngine.Vector3 DevionGames.InventorySystem.VisibleItem/Attachment::rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rotation_3;
	// UnityEngine.Vector3 DevionGames.InventorySystem.VisibleItem/Attachment::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_4;
	// UnityEngine.GameObject DevionGames.InventorySystem.VisibleItem/Attachment::gameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameObject_5;

public:
	inline static int32_t get_offset_of_region_0() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___region_0)); }
	inline EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * get_region_0() const { return ___region_0; }
	inline EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 ** get_address_of_region_0() { return &___region_0; }
	inline void set_region_0(EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * value)
	{
		___region_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___region_0), (void*)value);
	}

	inline static int32_t get_offset_of_prefab_1() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___prefab_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_prefab_1() const { return ___prefab_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_prefab_1() { return &___prefab_1; }
	inline void set_prefab_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___prefab_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prefab_1), (void*)value);
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___position_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_2() const { return ___position_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___rotation_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rotation_3() const { return ___rotation_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rotation_3 = value;
	}

	inline static int32_t get_offset_of_scale_4() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___scale_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_4() const { return ___scale_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_4() { return &___scale_4; }
	inline void set_scale_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_4 = value;
	}

	inline static int32_t get_offset_of_gameObject_5() { return static_cast<int32_t>(offsetof(Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D, ___gameObject_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameObject_5() const { return ___gameObject_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameObject_5() { return &___gameObject_5; }
	inline void set_gameObject_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameObject_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameObject_5), (void*)value);
	}
};


// DevionGames.InventorySystem.Weapon/ActivationType
struct ActivationType_t0CD47FF20778F15CBB5FA3B4A17FA6F9AD239C0D 
{
public:
	// System.Int32 DevionGames.InventorySystem.Weapon/ActivationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActivationType_t0CD47FF20778F15CBB5FA3B4A17FA6F9AD239C0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Weapon/StartType
struct StartType_t6F5FBAF1ABA7CB1D2F53CAB84649F9B6CCCAFD10 
{
public:
	// System.Int32 DevionGames.InventorySystem.Weapon/StartType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StartType_t6F5FBAF1ABA7CB1D2F53CAB84649F9B6CCCAFD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.InventorySystem.Weapon/StopType
struct StopType_t9DB728F6EC8C5F075209DAAE0EC008576AB7CA78 
{
public:
	// System.Int32 DevionGames.InventorySystem.Weapon/StopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StopType_t9DB728F6EC8C5F075209DAAE0EC008576AB7CA78, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// DevionGames.Sequence
struct Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5  : public RuntimeObject
{
public:
	// DevionGames.ActionStatus DevionGames.Sequence::m_Status
	int32_t ___m_Status_0;
	// System.Int32 DevionGames.Sequence::m_ActionIndex
	int32_t ___m_ActionIndex_1;
	// DevionGames.ActionStatus DevionGames.Sequence::m_ActionStatus
	int32_t ___m_ActionStatus_2;
	// DevionGames.IAction[] DevionGames.Sequence::m_AllActions
	IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* ___m_AllActions_3;
	// DevionGames.IAction[] DevionGames.Sequence::m_Actions
	IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* ___m_Actions_4;

public:
	inline static int32_t get_offset_of_m_Status_0() { return static_cast<int32_t>(offsetof(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5, ___m_Status_0)); }
	inline int32_t get_m_Status_0() const { return ___m_Status_0; }
	inline int32_t* get_address_of_m_Status_0() { return &___m_Status_0; }
	inline void set_m_Status_0(int32_t value)
	{
		___m_Status_0 = value;
	}

	inline static int32_t get_offset_of_m_ActionIndex_1() { return static_cast<int32_t>(offsetof(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5, ___m_ActionIndex_1)); }
	inline int32_t get_m_ActionIndex_1() const { return ___m_ActionIndex_1; }
	inline int32_t* get_address_of_m_ActionIndex_1() { return &___m_ActionIndex_1; }
	inline void set_m_ActionIndex_1(int32_t value)
	{
		___m_ActionIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_ActionStatus_2() { return static_cast<int32_t>(offsetof(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5, ___m_ActionStatus_2)); }
	inline int32_t get_m_ActionStatus_2() const { return ___m_ActionStatus_2; }
	inline int32_t* get_address_of_m_ActionStatus_2() { return &___m_ActionStatus_2; }
	inline void set_m_ActionStatus_2(int32_t value)
	{
		___m_ActionStatus_2 = value;
	}

	inline static int32_t get_offset_of_m_AllActions_3() { return static_cast<int32_t>(offsetof(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5, ___m_AllActions_3)); }
	inline IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* get_m_AllActions_3() const { return ___m_AllActions_3; }
	inline IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5** get_address_of_m_AllActions_3() { return &___m_AllActions_3; }
	inline void set_m_AllActions_3(IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* value)
	{
		___m_AllActions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AllActions_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Actions_4() { return static_cast<int32_t>(offsetof(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5, ___m_Actions_4)); }
	inline IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* get_m_Actions_4() const { return ___m_Actions_4; }
	inline IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5** get_address_of_m_Actions_4() { return &___m_Actions_4; }
	inline void set_m_Actions_4(IActionU5BU5D_tDCA3205085887DBDA2C58B66BA166EF5E6A83CD5* value)
	{
		___m_Actions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Actions_4), (void*)value);
	}
};


// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DevionGames.InventorySystem.Category
struct Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// DevionGames.InventorySystem.Category DevionGames.InventorySystem.Category::m_Parent
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * ___m_Parent_4;
	// System.String DevionGames.InventorySystem.Category::name
	String_t* ___name_5;
	// UnityEngine.Color DevionGames.InventorySystem.Category::m_EditorColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_EditorColor_6;
	// System.Single DevionGames.InventorySystem.Category::m_Cooldown
	float ___m_Cooldown_7;

public:
	inline static int32_t get_offset_of_m_Parent_4() { return static_cast<int32_t>(offsetof(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C, ___m_Parent_4)); }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * get_m_Parent_4() const { return ___m_Parent_4; }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C ** get_address_of_m_Parent_4() { return &___m_Parent_4; }
	inline void set_m_Parent_4(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * value)
	{
		___m_Parent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Parent_4), (void*)value);
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_EditorColor_6() { return static_cast<int32_t>(offsetof(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C, ___m_EditorColor_6)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_EditorColor_6() const { return ___m_EditorColor_6; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_EditorColor_6() { return &___m_EditorColor_6; }
	inline void set_m_EditorColor_6(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_EditorColor_6 = value;
	}

	inline static int32_t get_offset_of_m_Cooldown_7() { return static_cast<int32_t>(offsetof(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C, ___m_Cooldown_7)); }
	inline float get_m_Cooldown_7() const { return ___m_Cooldown_7; }
	inline float* get_address_of_m_Cooldown_7() { return &___m_Cooldown_7; }
	inline void set_m_Cooldown_7(float value)
	{
		___m_Cooldown_7 = value;
	}
};


// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DevionGames.InventorySystem.EquipmentRegion
struct EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String DevionGames.InventorySystem.EquipmentRegion::name
	String_t* ___name_4;

public:
	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_4), (void*)value);
	}
};


// DevionGames.InventorySystem.Item
struct Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String DevionGames.InventorySystem.Item::m_Id
	String_t* ___m_Id_4;
	// System.String DevionGames.InventorySystem.Item::m_ItemName
	String_t* ___m_ItemName_5;
	// System.Boolean DevionGames.InventorySystem.Item::m_UseItemNameAsDisplayName
	bool ___m_UseItemNameAsDisplayName_6;
	// System.String DevionGames.InventorySystem.Item::m_DisplayName
	String_t* ___m_DisplayName_7;
	// UnityEngine.Sprite DevionGames.InventorySystem.Item::m_Icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Icon_8;
	// UnityEngine.GameObject DevionGames.InventorySystem.Item::m_Prefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_Prefab_9;
	// System.String DevionGames.InventorySystem.Item::m_Description
	String_t* ___m_Description_10;
	// DevionGames.InventorySystem.Category DevionGames.InventorySystem.Item::m_Category
	Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * ___m_Category_11;
	// DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.Item::m_Rarity
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * ___m_Rarity_13;
	// System.Boolean DevionGames.InventorySystem.Item::m_IsSellable
	bool ___m_IsSellable_14;
	// System.Int32 DevionGames.InventorySystem.Item::m_BuyPrice
	int32_t ___m_BuyPrice_15;
	// System.Boolean DevionGames.InventorySystem.Item::m_CanBuyBack
	bool ___m_CanBuyBack_16;
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::m_BuyCurrency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___m_BuyCurrency_17;
	// System.Int32 DevionGames.InventorySystem.Item::m_SellPrice
	int32_t ___m_SellPrice_18;
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::m_SellCurrency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___m_SellCurrency_19;
	// System.Int32 DevionGames.InventorySystem.Item::m_Stack
	int32_t ___m_Stack_20;
	// System.Int32 DevionGames.InventorySystem.Item::m_MaxStack
	int32_t ___m_MaxStack_21;
	// System.Boolean DevionGames.InventorySystem.Item::m_IsDroppable
	bool ___m_IsDroppable_22;
	// UnityEngine.AudioClip DevionGames.InventorySystem.Item::m_DropSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___m_DropSound_23;
	// UnityEngine.GameObject DevionGames.InventorySystem.Item::m_OverridePrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_OverridePrefab_24;
	// System.Boolean DevionGames.InventorySystem.Item::m_IsCraftable
	bool ___m_IsCraftable_25;
	// System.Single DevionGames.InventorySystem.Item::m_CraftingDuration
	float ___m_CraftingDuration_26;
	// System.Boolean DevionGames.InventorySystem.Item::m_UseCraftingSkill
	bool ___m_UseCraftingSkill_27;
	// System.String DevionGames.InventorySystem.Item::m_SkillWindow
	String_t* ___m_SkillWindow_28;
	// DevionGames.InventorySystem.Skill DevionGames.InventorySystem.Item::m_CraftingSkill
	Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D * ___m_CraftingSkill_29;
	// System.Boolean DevionGames.InventorySystem.Item::m_RemoveIngredientsWhenFailed
	bool ___m_RemoveIngredientsWhenFailed_30;
	// System.Single DevionGames.InventorySystem.Item::m_MinCraftingSkillValue
	float ___m_MinCraftingSkillValue_31;
	// System.String DevionGames.InventorySystem.Item::m_CraftingAnimatorState
	String_t* ___m_CraftingAnimatorState_32;
	// DevionGames.InventorySystem.ItemModifierList DevionGames.InventorySystem.Item::m_CraftingModifier
	ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * ___m_CraftingModifier_33;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Item/Ingredient> DevionGames.InventorySystem.Item::ingredients
	List_1_tE9004C4523F5A08490EA3F73C43C507E7194574F * ___ingredients_34;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.Item::m_Slots
	List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * ___m_Slots_35;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.Item::m_ReferencedSlots
	List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * ___m_ReferencedSlots_36;
	// System.Collections.Generic.List`1<DevionGames.ObjectProperty> DevionGames.InventorySystem.Item::properties
	List_1_t44F9CAAA96CC3DAE67BE460143C5EB821214ADCE * ___properties_37;

public:
	inline static int32_t get_offset_of_m_Id_4() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Id_4)); }
	inline String_t* get_m_Id_4() const { return ___m_Id_4; }
	inline String_t** get_address_of_m_Id_4() { return &___m_Id_4; }
	inline void set_m_Id_4(String_t* value)
	{
		___m_Id_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemName_5() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_ItemName_5)); }
	inline String_t* get_m_ItemName_5() const { return ___m_ItemName_5; }
	inline String_t** get_address_of_m_ItemName_5() { return &___m_ItemName_5; }
	inline void set_m_ItemName_5(String_t* value)
	{
		___m_ItemName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseItemNameAsDisplayName_6() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_UseItemNameAsDisplayName_6)); }
	inline bool get_m_UseItemNameAsDisplayName_6() const { return ___m_UseItemNameAsDisplayName_6; }
	inline bool* get_address_of_m_UseItemNameAsDisplayName_6() { return &___m_UseItemNameAsDisplayName_6; }
	inline void set_m_UseItemNameAsDisplayName_6(bool value)
	{
		___m_UseItemNameAsDisplayName_6 = value;
	}

	inline static int32_t get_offset_of_m_DisplayName_7() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_DisplayName_7)); }
	inline String_t* get_m_DisplayName_7() const { return ___m_DisplayName_7; }
	inline String_t** get_address_of_m_DisplayName_7() { return &___m_DisplayName_7; }
	inline void set_m_DisplayName_7(String_t* value)
	{
		___m_DisplayName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayName_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Icon_8() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Icon_8)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Icon_8() const { return ___m_Icon_8; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Icon_8() { return &___m_Icon_8; }
	inline void set_m_Icon_8(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Icon_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Icon_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Prefab_9() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Prefab_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_Prefab_9() const { return ___m_Prefab_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_Prefab_9() { return &___m_Prefab_9; }
	inline void set_m_Prefab_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_Prefab_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Prefab_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Description_10() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Description_10)); }
	inline String_t* get_m_Description_10() const { return ___m_Description_10; }
	inline String_t** get_address_of_m_Description_10() { return &___m_Description_10; }
	inline void set_m_Description_10(String_t* value)
	{
		___m_Description_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Description_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Category_11() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Category_11)); }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * get_m_Category_11() const { return ___m_Category_11; }
	inline Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C ** get_address_of_m_Category_11() { return &___m_Category_11; }
	inline void set_m_Category_11(Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * value)
	{
		___m_Category_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Category_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Rarity_13() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Rarity_13)); }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * get_m_Rarity_13() const { return ___m_Rarity_13; }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F ** get_address_of_m_Rarity_13() { return &___m_Rarity_13; }
	inline void set_m_Rarity_13(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * value)
	{
		___m_Rarity_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Rarity_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsSellable_14() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_IsSellable_14)); }
	inline bool get_m_IsSellable_14() const { return ___m_IsSellable_14; }
	inline bool* get_address_of_m_IsSellable_14() { return &___m_IsSellable_14; }
	inline void set_m_IsSellable_14(bool value)
	{
		___m_IsSellable_14 = value;
	}

	inline static int32_t get_offset_of_m_BuyPrice_15() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_BuyPrice_15)); }
	inline int32_t get_m_BuyPrice_15() const { return ___m_BuyPrice_15; }
	inline int32_t* get_address_of_m_BuyPrice_15() { return &___m_BuyPrice_15; }
	inline void set_m_BuyPrice_15(int32_t value)
	{
		___m_BuyPrice_15 = value;
	}

	inline static int32_t get_offset_of_m_CanBuyBack_16() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_CanBuyBack_16)); }
	inline bool get_m_CanBuyBack_16() const { return ___m_CanBuyBack_16; }
	inline bool* get_address_of_m_CanBuyBack_16() { return &___m_CanBuyBack_16; }
	inline void set_m_CanBuyBack_16(bool value)
	{
		___m_CanBuyBack_16 = value;
	}

	inline static int32_t get_offset_of_m_BuyCurrency_17() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_BuyCurrency_17)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_m_BuyCurrency_17() const { return ___m_BuyCurrency_17; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_m_BuyCurrency_17() { return &___m_BuyCurrency_17; }
	inline void set_m_BuyCurrency_17(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___m_BuyCurrency_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuyCurrency_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellPrice_18() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_SellPrice_18)); }
	inline int32_t get_m_SellPrice_18() const { return ___m_SellPrice_18; }
	inline int32_t* get_address_of_m_SellPrice_18() { return &___m_SellPrice_18; }
	inline void set_m_SellPrice_18(int32_t value)
	{
		___m_SellPrice_18 = value;
	}

	inline static int32_t get_offset_of_m_SellCurrency_19() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_SellCurrency_19)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_m_SellCurrency_19() const { return ___m_SellCurrency_19; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_m_SellCurrency_19() { return &___m_SellCurrency_19; }
	inline void set_m_SellCurrency_19(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___m_SellCurrency_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellCurrency_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Stack_20() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Stack_20)); }
	inline int32_t get_m_Stack_20() const { return ___m_Stack_20; }
	inline int32_t* get_address_of_m_Stack_20() { return &___m_Stack_20; }
	inline void set_m_Stack_20(int32_t value)
	{
		___m_Stack_20 = value;
	}

	inline static int32_t get_offset_of_m_MaxStack_21() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_MaxStack_21)); }
	inline int32_t get_m_MaxStack_21() const { return ___m_MaxStack_21; }
	inline int32_t* get_address_of_m_MaxStack_21() { return &___m_MaxStack_21; }
	inline void set_m_MaxStack_21(int32_t value)
	{
		___m_MaxStack_21 = value;
	}

	inline static int32_t get_offset_of_m_IsDroppable_22() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_IsDroppable_22)); }
	inline bool get_m_IsDroppable_22() const { return ___m_IsDroppable_22; }
	inline bool* get_address_of_m_IsDroppable_22() { return &___m_IsDroppable_22; }
	inline void set_m_IsDroppable_22(bool value)
	{
		___m_IsDroppable_22 = value;
	}

	inline static int32_t get_offset_of_m_DropSound_23() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_DropSound_23)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_m_DropSound_23() const { return ___m_DropSound_23; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_m_DropSound_23() { return &___m_DropSound_23; }
	inline void set_m_DropSound_23(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___m_DropSound_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DropSound_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverridePrefab_24() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_OverridePrefab_24)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_OverridePrefab_24() const { return ___m_OverridePrefab_24; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_OverridePrefab_24() { return &___m_OverridePrefab_24; }
	inline void set_m_OverridePrefab_24(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_OverridePrefab_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverridePrefab_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsCraftable_25() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_IsCraftable_25)); }
	inline bool get_m_IsCraftable_25() const { return ___m_IsCraftable_25; }
	inline bool* get_address_of_m_IsCraftable_25() { return &___m_IsCraftable_25; }
	inline void set_m_IsCraftable_25(bool value)
	{
		___m_IsCraftable_25 = value;
	}

	inline static int32_t get_offset_of_m_CraftingDuration_26() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_CraftingDuration_26)); }
	inline float get_m_CraftingDuration_26() const { return ___m_CraftingDuration_26; }
	inline float* get_address_of_m_CraftingDuration_26() { return &___m_CraftingDuration_26; }
	inline void set_m_CraftingDuration_26(float value)
	{
		___m_CraftingDuration_26 = value;
	}

	inline static int32_t get_offset_of_m_UseCraftingSkill_27() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_UseCraftingSkill_27)); }
	inline bool get_m_UseCraftingSkill_27() const { return ___m_UseCraftingSkill_27; }
	inline bool* get_address_of_m_UseCraftingSkill_27() { return &___m_UseCraftingSkill_27; }
	inline void set_m_UseCraftingSkill_27(bool value)
	{
		___m_UseCraftingSkill_27 = value;
	}

	inline static int32_t get_offset_of_m_SkillWindow_28() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_SkillWindow_28)); }
	inline String_t* get_m_SkillWindow_28() const { return ___m_SkillWindow_28; }
	inline String_t** get_address_of_m_SkillWindow_28() { return &___m_SkillWindow_28; }
	inline void set_m_SkillWindow_28(String_t* value)
	{
		___m_SkillWindow_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SkillWindow_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_CraftingSkill_29() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_CraftingSkill_29)); }
	inline Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D * get_m_CraftingSkill_29() const { return ___m_CraftingSkill_29; }
	inline Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D ** get_address_of_m_CraftingSkill_29() { return &___m_CraftingSkill_29; }
	inline void set_m_CraftingSkill_29(Skill_tD8B6D5BDA0E3AFDAAFDD5445D97C7BF29D4DA57D * value)
	{
		___m_CraftingSkill_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CraftingSkill_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_RemoveIngredientsWhenFailed_30() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_RemoveIngredientsWhenFailed_30)); }
	inline bool get_m_RemoveIngredientsWhenFailed_30() const { return ___m_RemoveIngredientsWhenFailed_30; }
	inline bool* get_address_of_m_RemoveIngredientsWhenFailed_30() { return &___m_RemoveIngredientsWhenFailed_30; }
	inline void set_m_RemoveIngredientsWhenFailed_30(bool value)
	{
		___m_RemoveIngredientsWhenFailed_30 = value;
	}

	inline static int32_t get_offset_of_m_MinCraftingSkillValue_31() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_MinCraftingSkillValue_31)); }
	inline float get_m_MinCraftingSkillValue_31() const { return ___m_MinCraftingSkillValue_31; }
	inline float* get_address_of_m_MinCraftingSkillValue_31() { return &___m_MinCraftingSkillValue_31; }
	inline void set_m_MinCraftingSkillValue_31(float value)
	{
		___m_MinCraftingSkillValue_31 = value;
	}

	inline static int32_t get_offset_of_m_CraftingAnimatorState_32() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_CraftingAnimatorState_32)); }
	inline String_t* get_m_CraftingAnimatorState_32() const { return ___m_CraftingAnimatorState_32; }
	inline String_t** get_address_of_m_CraftingAnimatorState_32() { return &___m_CraftingAnimatorState_32; }
	inline void set_m_CraftingAnimatorState_32(String_t* value)
	{
		___m_CraftingAnimatorState_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CraftingAnimatorState_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_CraftingModifier_33() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_CraftingModifier_33)); }
	inline ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * get_m_CraftingModifier_33() const { return ___m_CraftingModifier_33; }
	inline ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 ** get_address_of_m_CraftingModifier_33() { return &___m_CraftingModifier_33; }
	inline void set_m_CraftingModifier_33(ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * value)
	{
		___m_CraftingModifier_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CraftingModifier_33), (void*)value);
	}

	inline static int32_t get_offset_of_ingredients_34() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___ingredients_34)); }
	inline List_1_tE9004C4523F5A08490EA3F73C43C507E7194574F * get_ingredients_34() const { return ___ingredients_34; }
	inline List_1_tE9004C4523F5A08490EA3F73C43C507E7194574F ** get_address_of_ingredients_34() { return &___ingredients_34; }
	inline void set_ingredients_34(List_1_tE9004C4523F5A08490EA3F73C43C507E7194574F * value)
	{
		___ingredients_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ingredients_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_Slots_35() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_Slots_35)); }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * get_m_Slots_35() const { return ___m_Slots_35; }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE ** get_address_of_m_Slots_35() { return &___m_Slots_35; }
	inline void set_m_Slots_35(List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * value)
	{
		___m_Slots_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Slots_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReferencedSlots_36() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___m_ReferencedSlots_36)); }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * get_m_ReferencedSlots_36() const { return ___m_ReferencedSlots_36; }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE ** get_address_of_m_ReferencedSlots_36() { return &___m_ReferencedSlots_36; }
	inline void set_m_ReferencedSlots_36(List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * value)
	{
		___m_ReferencedSlots_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReferencedSlots_36), (void*)value);
	}

	inline static int32_t get_offset_of_properties_37() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5, ___properties_37)); }
	inline List_1_t44F9CAAA96CC3DAE67BE460143C5EB821214ADCE * get_properties_37() const { return ___properties_37; }
	inline List_1_t44F9CAAA96CC3DAE67BE460143C5EB821214ADCE ** get_address_of_properties_37() { return &___properties_37; }
	inline void set_properties_37(List_1_t44F9CAAA96CC3DAE67BE460143C5EB821214ADCE * value)
	{
		___properties_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___properties_37), (void*)value);
	}
};

struct Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_StaticFields
{
public:
	// DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.Item::m_DefaultRarity
	Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * ___m_DefaultRarity_12;

public:
	inline static int32_t get_offset_of_m_DefaultRarity_12() { return static_cast<int32_t>(offsetof(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5_StaticFields, ___m_DefaultRarity_12)); }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * get_m_DefaultRarity_12() const { return ___m_DefaultRarity_12; }
	inline Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F ** get_address_of_m_DefaultRarity_12() { return &___m_DefaultRarity_12; }
	inline void set_m_DefaultRarity_12(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * value)
	{
		___m_DefaultRarity_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultRarity_12), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemGroup
struct ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String DevionGames.InventorySystem.ItemGroup::m_GroupName
	String_t* ___m_GroupName_4;
	// DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.ItemGroup::m_Items
	ItemU5BU5D_t7735267E2EAA8DEAFA53553BCD5512DA27749997* ___m_Items_5;
	// System.Int32[] DevionGames.InventorySystem.ItemGroup::m_Amounts
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___m_Amounts_6;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemModifierList> DevionGames.InventorySystem.ItemGroup::m_Modifiers
	List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * ___m_Modifiers_7;

public:
	inline static int32_t get_offset_of_m_GroupName_4() { return static_cast<int32_t>(offsetof(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84, ___m_GroupName_4)); }
	inline String_t* get_m_GroupName_4() const { return ___m_GroupName_4; }
	inline String_t** get_address_of_m_GroupName_4() { return &___m_GroupName_4; }
	inline void set_m_GroupName_4(String_t* value)
	{
		___m_GroupName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GroupName_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Items_5() { return static_cast<int32_t>(offsetof(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84, ___m_Items_5)); }
	inline ItemU5BU5D_t7735267E2EAA8DEAFA53553BCD5512DA27749997* get_m_Items_5() const { return ___m_Items_5; }
	inline ItemU5BU5D_t7735267E2EAA8DEAFA53553BCD5512DA27749997** get_address_of_m_Items_5() { return &___m_Items_5; }
	inline void set_m_Items_5(ItemU5BU5D_t7735267E2EAA8DEAFA53553BCD5512DA27749997* value)
	{
		___m_Items_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Items_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Amounts_6() { return static_cast<int32_t>(offsetof(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84, ___m_Amounts_6)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_m_Amounts_6() const { return ___m_Amounts_6; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_m_Amounts_6() { return &___m_Amounts_6; }
	inline void set_m_Amounts_6(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___m_Amounts_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Amounts_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Modifiers_7() { return static_cast<int32_t>(offsetof(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84, ___m_Modifiers_7)); }
	inline List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * get_m_Modifiers_7() const { return ___m_Modifiers_7; }
	inline List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 ** get_address_of_m_Modifiers_7() { return &___m_Modifiers_7; }
	inline void set_m_Modifiers_7(List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * value)
	{
		___m_Modifiers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Modifiers_7), (void*)value);
	}
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// DevionGames.InventorySystem.Rarity
struct Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String DevionGames.InventorySystem.Rarity::name
	String_t* ___name_4;
	// System.Boolean DevionGames.InventorySystem.Rarity::m_UseAsNamePrefix
	bool ___m_UseAsNamePrefix_5;
	// UnityEngine.Color DevionGames.InventorySystem.Rarity::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_6;
	// System.Int32 DevionGames.InventorySystem.Rarity::chance
	int32_t ___chance_7;
	// System.Single DevionGames.InventorySystem.Rarity::multiplier
	float ___multiplier_8;
	// System.Single DevionGames.InventorySystem.Rarity::m_PriceMultiplier
	float ___m_PriceMultiplier_9;

public:
	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseAsNamePrefix_5() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___m_UseAsNamePrefix_5)); }
	inline bool get_m_UseAsNamePrefix_5() const { return ___m_UseAsNamePrefix_5; }
	inline bool* get_address_of_m_UseAsNamePrefix_5() { return &___m_UseAsNamePrefix_5; }
	inline void set_m_UseAsNamePrefix_5(bool value)
	{
		___m_UseAsNamePrefix_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___color_6)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_6() const { return ___color_6; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_chance_7() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___chance_7)); }
	inline int32_t get_chance_7() const { return ___chance_7; }
	inline int32_t* get_address_of_chance_7() { return &___chance_7; }
	inline void set_chance_7(int32_t value)
	{
		___chance_7 = value;
	}

	inline static int32_t get_offset_of_multiplier_8() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___multiplier_8)); }
	inline float get_multiplier_8() const { return ___multiplier_8; }
	inline float* get_address_of_multiplier_8() { return &___multiplier_8; }
	inline void set_multiplier_8(float value)
	{
		___multiplier_8 = value;
	}

	inline static int32_t get_offset_of_m_PriceMultiplier_9() { return static_cast<int32_t>(offsetof(Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F, ___m_PriceMultiplier_9)); }
	inline float get_m_PriceMultiplier_9() const { return ___m_PriceMultiplier_9; }
	inline float* get_address_of_m_PriceMultiplier_9() { return &___m_PriceMultiplier_9; }
	inline void set_m_PriceMultiplier_9(float value)
	{
		___m_PriceMultiplier_9 = value;
	}
};


// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DevionGames.InventorySystem.Configuration.Settings
struct Settings_t7DEDA545EE89F4EB2CA5D9DA714FD17EB1171FB6  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/AddItemDelegate
struct AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/DropItemDelegate
struct DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate
struct FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate
struct FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate
struct RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.ItemContainer/UseItemDelegate
struct UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B  : public MulticastDelegate_t
{
public:

public:
};


// DevionGames.InventorySystem.Currency
struct Currency_tC5AA8C667005127F940597BF06CF49D395F55308  : public Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5
{
public:
	// DevionGames.InventorySystem.CurrencyConversion[] DevionGames.InventorySystem.Currency::currencyConversions
	CurrencyConversionU5BU5D_t6F495C7F9CFC6A6FBB9CFDF9AF3B00CEAAED2975* ___currencyConversions_38;

public:
	inline static int32_t get_offset_of_currencyConversions_38() { return static_cast<int32_t>(offsetof(Currency_tC5AA8C667005127F940597BF06CF49D395F55308, ___currencyConversions_38)); }
	inline CurrencyConversionU5BU5D_t6F495C7F9CFC6A6FBB9CFDF9AF3B00CEAAED2975* get_currencyConversions_38() const { return ___currencyConversions_38; }
	inline CurrencyConversionU5BU5D_t6F495C7F9CFC6A6FBB9CFDF9AF3B00CEAAED2975** get_address_of_currencyConversions_38() { return &___currencyConversions_38; }
	inline void set_currencyConversions_38(CurrencyConversionU5BU5D_t6F495C7F9CFC6A6FBB9CFDF9AF3B00CEAAED2975* value)
	{
		___currencyConversions_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currencyConversions_38), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// DevionGames.InventorySystem.Configuration.UI
struct UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1  : public Settings_t7DEDA545EE89F4EB2CA5D9DA714FD17EB1171FB6
{
public:
	// System.String DevionGames.InventorySystem.Configuration.UI::contextMenuName
	String_t* ___contextMenuName_4;
	// System.String DevionGames.InventorySystem.Configuration.UI::tooltipName
	String_t* ___tooltipName_5;
	// System.String DevionGames.InventorySystem.Configuration.UI::sellPriceTooltipName
	String_t* ___sellPriceTooltipName_6;
	// System.String DevionGames.InventorySystem.Configuration.UI::stackName
	String_t* ___stackName_7;
	// System.String DevionGames.InventorySystem.Configuration.UI::notificationName
	String_t* ___notificationName_8;
	// DevionGames.UIWidgets.Notification DevionGames.InventorySystem.Configuration.UI::m_Notification
	Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE * ___m_Notification_9;
	// DevionGames.UIWidgets.Tooltip DevionGames.InventorySystem.Configuration.UI::m_Tooltip
	Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * ___m_Tooltip_10;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Configuration.UI::m_SellPriceTooltip
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_SellPriceTooltip_11;
	// DevionGames.InventorySystem.Stack DevionGames.InventorySystem.Configuration.UI::m_Stack
	Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C * ___m_Stack_12;
	// DevionGames.UIWidgets.ContextMenu DevionGames.InventorySystem.Configuration.UI::m_ContextMenu
	ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509 * ___m_ContextMenu_13;

public:
	inline static int32_t get_offset_of_contextMenuName_4() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___contextMenuName_4)); }
	inline String_t* get_contextMenuName_4() const { return ___contextMenuName_4; }
	inline String_t** get_address_of_contextMenuName_4() { return &___contextMenuName_4; }
	inline void set_contextMenuName_4(String_t* value)
	{
		___contextMenuName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contextMenuName_4), (void*)value);
	}

	inline static int32_t get_offset_of_tooltipName_5() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___tooltipName_5)); }
	inline String_t* get_tooltipName_5() const { return ___tooltipName_5; }
	inline String_t** get_address_of_tooltipName_5() { return &___tooltipName_5; }
	inline void set_tooltipName_5(String_t* value)
	{
		___tooltipName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltipName_5), (void*)value);
	}

	inline static int32_t get_offset_of_sellPriceTooltipName_6() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___sellPriceTooltipName_6)); }
	inline String_t* get_sellPriceTooltipName_6() const { return ___sellPriceTooltipName_6; }
	inline String_t** get_address_of_sellPriceTooltipName_6() { return &___sellPriceTooltipName_6; }
	inline void set_sellPriceTooltipName_6(String_t* value)
	{
		___sellPriceTooltipName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sellPriceTooltipName_6), (void*)value);
	}

	inline static int32_t get_offset_of_stackName_7() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___stackName_7)); }
	inline String_t* get_stackName_7() const { return ___stackName_7; }
	inline String_t** get_address_of_stackName_7() { return &___stackName_7; }
	inline void set_stackName_7(String_t* value)
	{
		___stackName_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stackName_7), (void*)value);
	}

	inline static int32_t get_offset_of_notificationName_8() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___notificationName_8)); }
	inline String_t* get_notificationName_8() const { return ___notificationName_8; }
	inline String_t** get_address_of_notificationName_8() { return &___notificationName_8; }
	inline void set_notificationName_8(String_t* value)
	{
		___notificationName_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___notificationName_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Notification_9() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___m_Notification_9)); }
	inline Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE * get_m_Notification_9() const { return ___m_Notification_9; }
	inline Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE ** get_address_of_m_Notification_9() { return &___m_Notification_9; }
	inline void set_m_Notification_9(Notification_t5700FC6B04B5D950CBBFBB5FD70E08465C3015AE * value)
	{
		___m_Notification_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Notification_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tooltip_10() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___m_Tooltip_10)); }
	inline Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * get_m_Tooltip_10() const { return ___m_Tooltip_10; }
	inline Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 ** get_address_of_m_Tooltip_10() { return &___m_Tooltip_10; }
	inline void set_m_Tooltip_10(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * value)
	{
		___m_Tooltip_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tooltip_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellPriceTooltip_11() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___m_SellPriceTooltip_11)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_SellPriceTooltip_11() const { return ___m_SellPriceTooltip_11; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_SellPriceTooltip_11() { return &___m_SellPriceTooltip_11; }
	inline void set_m_SellPriceTooltip_11(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_SellPriceTooltip_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellPriceTooltip_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Stack_12() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___m_Stack_12)); }
	inline Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C * get_m_Stack_12() const { return ___m_Stack_12; }
	inline Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C ** get_address_of_m_Stack_12() { return &___m_Stack_12; }
	inline void set_m_Stack_12(Stack_t803866555FCD8EFE120D6DDB7BF4C5B820895C8C * value)
	{
		___m_Stack_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_ContextMenu_13() { return static_cast<int32_t>(offsetof(UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1, ___m_ContextMenu_13)); }
	inline ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509 * get_m_ContextMenu_13() const { return ___m_ContextMenu_13; }
	inline ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509 ** get_address_of_m_ContextMenu_13() { return &___m_ContextMenu_13; }
	inline void set_m_ContextMenu_13(ContextMenu_t0C804F1A33B8505E6C2DA01B979575B024632509 * value)
	{
		___m_ContextMenu_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ContextMenu_13), (void*)value);
	}
};


// DevionGames.InventorySystem.UsableItem
struct UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB  : public Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5
{
public:
	// System.Boolean DevionGames.InventorySystem.UsableItem::m_UseCategoryCooldown
	bool ___m_UseCategoryCooldown_38;
	// System.Single DevionGames.InventorySystem.UsableItem::m_Cooldown
	float ___m_Cooldown_39;
	// System.Collections.Generic.List`1<DevionGames.Action> DevionGames.InventorySystem.UsableItem::actions
	List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * ___actions_40;
	// DevionGames.Sequence DevionGames.InventorySystem.UsableItem::m_ActionSequence
	Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * ___m_ActionSequence_41;
	// System.Collections.IEnumerator DevionGames.InventorySystem.UsableItem::m_ActionBehavior
	RuntimeObject* ___m_ActionBehavior_42;

public:
	inline static int32_t get_offset_of_m_UseCategoryCooldown_38() { return static_cast<int32_t>(offsetof(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB, ___m_UseCategoryCooldown_38)); }
	inline bool get_m_UseCategoryCooldown_38() const { return ___m_UseCategoryCooldown_38; }
	inline bool* get_address_of_m_UseCategoryCooldown_38() { return &___m_UseCategoryCooldown_38; }
	inline void set_m_UseCategoryCooldown_38(bool value)
	{
		___m_UseCategoryCooldown_38 = value;
	}

	inline static int32_t get_offset_of_m_Cooldown_39() { return static_cast<int32_t>(offsetof(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB, ___m_Cooldown_39)); }
	inline float get_m_Cooldown_39() const { return ___m_Cooldown_39; }
	inline float* get_address_of_m_Cooldown_39() { return &___m_Cooldown_39; }
	inline void set_m_Cooldown_39(float value)
	{
		___m_Cooldown_39 = value;
	}

	inline static int32_t get_offset_of_actions_40() { return static_cast<int32_t>(offsetof(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB, ___actions_40)); }
	inline List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * get_actions_40() const { return ___actions_40; }
	inline List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 ** get_address_of_actions_40() { return &___actions_40; }
	inline void set_actions_40(List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * value)
	{
		___actions_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actions_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionSequence_41() { return static_cast<int32_t>(offsetof(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB, ___m_ActionSequence_41)); }
	inline Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * get_m_ActionSequence_41() const { return ___m_ActionSequence_41; }
	inline Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 ** get_address_of_m_ActionSequence_41() { return &___m_ActionSequence_41; }
	inline void set_m_ActionSequence_41(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * value)
	{
		___m_ActionSequence_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionSequence_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionBehavior_42() { return static_cast<int32_t>(offsetof(UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB, ___m_ActionBehavior_42)); }
	inline RuntimeObject* get_m_ActionBehavior_42() const { return ___m_ActionBehavior_42; }
	inline RuntimeObject** get_address_of_m_ActionBehavior_42() { return &___m_ActionBehavior_42; }
	inline void set_m_ActionBehavior_42(RuntimeObject* value)
	{
		___m_ActionBehavior_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionBehavior_42), (void*)value);
	}
};


// DevionGames.CallbackHandler
struct CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<DevionGames.CallbackHandler/Entry> DevionGames.CallbackHandler::delegates
	List_1_tA6B1EE7CD310767E07B8EDC08D5F5C1506C31A82 * ___delegates_4;

public:
	inline static int32_t get_offset_of_delegates_4() { return static_cast<int32_t>(offsetof(CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D, ___delegates_4)); }
	inline List_1_tA6B1EE7CD310767E07B8EDC08D5F5C1506C31A82 * get_delegates_4() const { return ___delegates_4; }
	inline List_1_tA6B1EE7CD310767E07B8EDC08D5F5C1506C31A82 ** get_address_of_delegates_4() { return &___delegates_4; }
	inline void set_delegates_4(List_1_tA6B1EE7CD310767E07B8EDC08D5F5C1506C31A82 * value)
	{
		___delegates_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_4), (void*)value);
	}
};


// DevionGames.InventorySystem.EquipmentHandler
struct EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String DevionGames.InventorySystem.EquipmentHandler::m_WindowName
	String_t* ___m_WindowName_4;
	// DevionGames.InventorySystem.ItemDatabase DevionGames.InventorySystem.EquipmentHandler::m_Database
	ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906 * ___m_Database_5;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentHandler/EquipmentBone> DevionGames.InventorySystem.EquipmentHandler::m_Bones
	List_1_t95949B58FE659DBC2F71E2008085864DE4DD496E * ___m_Bones_6;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.VisibleItem> DevionGames.InventorySystem.EquipmentHandler::m_VisibleItems
	List_1_t2BAC24224203EB60F9A70D6B629A6F700EB81057 * ___m_VisibleItems_7;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.EquipmentHandler::m_EquipmentContainer
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_EquipmentContainer_8;

public:
	inline static int32_t get_offset_of_m_WindowName_4() { return static_cast<int32_t>(offsetof(EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C, ___m_WindowName_4)); }
	inline String_t* get_m_WindowName_4() const { return ___m_WindowName_4; }
	inline String_t** get_address_of_m_WindowName_4() { return &___m_WindowName_4; }
	inline void set_m_WindowName_4(String_t* value)
	{
		___m_WindowName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WindowName_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Database_5() { return static_cast<int32_t>(offsetof(EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C, ___m_Database_5)); }
	inline ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906 * get_m_Database_5() const { return ___m_Database_5; }
	inline ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906 ** get_address_of_m_Database_5() { return &___m_Database_5; }
	inline void set_m_Database_5(ItemDatabase_t0D371ABDEF93E9B91A2D4B13E603AFF774235906 * value)
	{
		___m_Database_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Database_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Bones_6() { return static_cast<int32_t>(offsetof(EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C, ___m_Bones_6)); }
	inline List_1_t95949B58FE659DBC2F71E2008085864DE4DD496E * get_m_Bones_6() const { return ___m_Bones_6; }
	inline List_1_t95949B58FE659DBC2F71E2008085864DE4DD496E ** get_address_of_m_Bones_6() { return &___m_Bones_6; }
	inline void set_m_Bones_6(List_1_t95949B58FE659DBC2F71E2008085864DE4DD496E * value)
	{
		___m_Bones_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Bones_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_VisibleItems_7() { return static_cast<int32_t>(offsetof(EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C, ___m_VisibleItems_7)); }
	inline List_1_t2BAC24224203EB60F9A70D6B629A6F700EB81057 * get_m_VisibleItems_7() const { return ___m_VisibleItems_7; }
	inline List_1_t2BAC24224203EB60F9A70D6B629A6F700EB81057 ** get_address_of_m_VisibleItems_7() { return &___m_VisibleItems_7; }
	inline void set_m_VisibleItems_7(List_1_t2BAC24224203EB60F9A70D6B629A6F700EB81057 * value)
	{
		___m_VisibleItems_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VisibleItems_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_EquipmentContainer_8() { return static_cast<int32_t>(offsetof(EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C, ___m_EquipmentContainer_8)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_EquipmentContainer_8() const { return ___m_EquipmentContainer_8; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_EquipmentContainer_8() { return &___m_EquipmentContainer_8; }
	inline void set_m_EquipmentContainer_8(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_EquipmentContainer_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EquipmentContainer_8), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemCollection
struct ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean DevionGames.InventorySystem.ItemCollection::saveable
	bool ___saveable_4;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Item> DevionGames.InventorySystem.ItemCollection::m_Items
	List_1_t2772AD0C301E5E39A5101F3577A9F6D8CDE4D91B * ___m_Items_5;
	// System.Collections.Generic.List`1<System.Int32> DevionGames.InventorySystem.ItemCollection::m_Amounts
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Amounts_6;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemModifierList> DevionGames.InventorySystem.ItemCollection::m_Modifiers
	List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * ___m_Modifiers_7;
	// UnityEngine.Events.UnityEvent DevionGames.InventorySystem.ItemCollection::onChange
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onChange_8;
	// System.Boolean DevionGames.InventorySystem.ItemCollection::m_Initialized
	bool ___m_Initialized_9;

public:
	inline static int32_t get_offset_of_saveable_4() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___saveable_4)); }
	inline bool get_saveable_4() const { return ___saveable_4; }
	inline bool* get_address_of_saveable_4() { return &___saveable_4; }
	inline void set_saveable_4(bool value)
	{
		___saveable_4 = value;
	}

	inline static int32_t get_offset_of_m_Items_5() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___m_Items_5)); }
	inline List_1_t2772AD0C301E5E39A5101F3577A9F6D8CDE4D91B * get_m_Items_5() const { return ___m_Items_5; }
	inline List_1_t2772AD0C301E5E39A5101F3577A9F6D8CDE4D91B ** get_address_of_m_Items_5() { return &___m_Items_5; }
	inline void set_m_Items_5(List_1_t2772AD0C301E5E39A5101F3577A9F6D8CDE4D91B * value)
	{
		___m_Items_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Items_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Amounts_6() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___m_Amounts_6)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Amounts_6() const { return ___m_Amounts_6; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Amounts_6() { return &___m_Amounts_6; }
	inline void set_m_Amounts_6(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Amounts_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Amounts_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Modifiers_7() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___m_Modifiers_7)); }
	inline List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * get_m_Modifiers_7() const { return ___m_Modifiers_7; }
	inline List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 ** get_address_of_m_Modifiers_7() { return &___m_Modifiers_7; }
	inline void set_m_Modifiers_7(List_1_t675D36843B35C1E573632F1CF0BE3C57122DEA63 * value)
	{
		___m_Modifiers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Modifiers_7), (void*)value);
	}

	inline static int32_t get_offset_of_onChange_8() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___onChange_8)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onChange_8() const { return ___onChange_8; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onChange_8() { return &___onChange_8; }
	inline void set_onChange_8(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onChange_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onChange_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Initialized_9() { return static_cast<int32_t>(offsetof(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803, ___m_Initialized_9)); }
	inline bool get_m_Initialized_9() const { return ___m_Initialized_9; }
	inline bool* get_address_of_m_Initialized_9() { return &___m_Initialized_9; }
	inline void set_m_Initialized_9(bool value)
	{
		___m_Initialized_9 = value;
	}
};


// DevionGames.InventorySystem.ItemGroupGenerator
struct ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// DevionGames.InventorySystem.ItemGroup DevionGames.InventorySystem.ItemGroupGenerator::m_From
	ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * ___m_From_4;
	// System.Collections.Generic.List`1<UnityEngine.ScriptableObject> DevionGames.InventorySystem.ItemGroupGenerator::m_Filters
	List_1_tEB4537E121ED7128292F5E49486823EB846576FE * ___m_Filters_5;
	// System.Int32 DevionGames.InventorySystem.ItemGroupGenerator::m_MinStack
	int32_t ___m_MinStack_6;
	// System.Int32 DevionGames.InventorySystem.ItemGroupGenerator::m_MaxStack
	int32_t ___m_MaxStack_7;
	// System.Int32 DevionGames.InventorySystem.ItemGroupGenerator::m_MinAmount
	int32_t ___m_MinAmount_8;
	// System.Int32 DevionGames.InventorySystem.ItemGroupGenerator::m_MaxAmount
	int32_t ___m_MaxAmount_9;
	// System.Single DevionGames.InventorySystem.ItemGroupGenerator::m_Chance
	float ___m_Chance_10;
	// DevionGames.InventorySystem.ItemModifierList DevionGames.InventorySystem.ItemGroupGenerator::m_Modifiers
	ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * ___m_Modifiers_11;

public:
	inline static int32_t get_offset_of_m_From_4() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_From_4)); }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * get_m_From_4() const { return ___m_From_4; }
	inline ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 ** get_address_of_m_From_4() { return &___m_From_4; }
	inline void set_m_From_4(ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * value)
	{
		___m_From_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_From_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Filters_5() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_Filters_5)); }
	inline List_1_tEB4537E121ED7128292F5E49486823EB846576FE * get_m_Filters_5() const { return ___m_Filters_5; }
	inline List_1_tEB4537E121ED7128292F5E49486823EB846576FE ** get_address_of_m_Filters_5() { return &___m_Filters_5; }
	inline void set_m_Filters_5(List_1_tEB4537E121ED7128292F5E49486823EB846576FE * value)
	{
		___m_Filters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Filters_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_MinStack_6() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_MinStack_6)); }
	inline int32_t get_m_MinStack_6() const { return ___m_MinStack_6; }
	inline int32_t* get_address_of_m_MinStack_6() { return &___m_MinStack_6; }
	inline void set_m_MinStack_6(int32_t value)
	{
		___m_MinStack_6 = value;
	}

	inline static int32_t get_offset_of_m_MaxStack_7() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_MaxStack_7)); }
	inline int32_t get_m_MaxStack_7() const { return ___m_MaxStack_7; }
	inline int32_t* get_address_of_m_MaxStack_7() { return &___m_MaxStack_7; }
	inline void set_m_MaxStack_7(int32_t value)
	{
		___m_MaxStack_7 = value;
	}

	inline static int32_t get_offset_of_m_MinAmount_8() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_MinAmount_8)); }
	inline int32_t get_m_MinAmount_8() const { return ___m_MinAmount_8; }
	inline int32_t* get_address_of_m_MinAmount_8() { return &___m_MinAmount_8; }
	inline void set_m_MinAmount_8(int32_t value)
	{
		___m_MinAmount_8 = value;
	}

	inline static int32_t get_offset_of_m_MaxAmount_9() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_MaxAmount_9)); }
	inline int32_t get_m_MaxAmount_9() const { return ___m_MaxAmount_9; }
	inline int32_t* get_address_of_m_MaxAmount_9() { return &___m_MaxAmount_9; }
	inline void set_m_MaxAmount_9(int32_t value)
	{
		___m_MaxAmount_9 = value;
	}

	inline static int32_t get_offset_of_m_Chance_10() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_Chance_10)); }
	inline float get_m_Chance_10() const { return ___m_Chance_10; }
	inline float* get_address_of_m_Chance_10() { return &___m_Chance_10; }
	inline void set_m_Chance_10(float value)
	{
		___m_Chance_10 = value;
	}

	inline static int32_t get_offset_of_m_Modifiers_11() { return static_cast<int32_t>(offsetof(ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80, ___m_Modifiers_11)); }
	inline ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * get_m_Modifiers_11() const { return ___m_Modifiers_11; }
	inline ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 ** get_address_of_m_Modifiers_11() { return &___m_Modifiers_11; }
	inline void set_m_Modifiers_11(ItemModifierList_tD35A31EABE8F2D63AB808181E016523D256C4C19 * value)
	{
		___m_Modifiers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Modifiers_11), (void*)value);
	}
};


// DevionGames.UIWidgets.Spinner
struct Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single DevionGames.UIWidgets.Spinner::changeDelay
	float ___changeDelay_4;
	// System.Single DevionGames.UIWidgets.Spinner::m_Current
	float ___m_Current_5;
	// System.Single DevionGames.UIWidgets.Spinner::step
	float ___step_6;
	// System.Single DevionGames.UIWidgets.Spinner::min
	float ___min_7;
	// System.Single DevionGames.UIWidgets.Spinner::max
	float ___max_8;
	// DevionGames.UIWidgets.Spinner/SpinnerEvent DevionGames.UIWidgets.Spinner::onChange
	SpinnerEvent_t3E9EC0B8E5BF9928D00230B8C3F8945BB85CCA3A * ___onChange_9;
	// DevionGames.UIWidgets.Spinner/SpinnerTextEvent DevionGames.UIWidgets.Spinner::m_OnChange
	SpinnerTextEvent_t421ED044855D0F19B55CDC413D30DE08E0E45D63 * ___m_OnChange_10;
	// System.Collections.IEnumerator DevionGames.UIWidgets.Spinner::coroutine
	RuntimeObject* ___coroutine_11;

public:
	inline static int32_t get_offset_of_changeDelay_4() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___changeDelay_4)); }
	inline float get_changeDelay_4() const { return ___changeDelay_4; }
	inline float* get_address_of_changeDelay_4() { return &___changeDelay_4; }
	inline void set_changeDelay_4(float value)
	{
		___changeDelay_4 = value;
	}

	inline static int32_t get_offset_of_m_Current_5() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___m_Current_5)); }
	inline float get_m_Current_5() const { return ___m_Current_5; }
	inline float* get_address_of_m_Current_5() { return &___m_Current_5; }
	inline void set_m_Current_5(float value)
	{
		___m_Current_5 = value;
	}

	inline static int32_t get_offset_of_step_6() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___step_6)); }
	inline float get_step_6() const { return ___step_6; }
	inline float* get_address_of_step_6() { return &___step_6; }
	inline void set_step_6(float value)
	{
		___step_6 = value;
	}

	inline static int32_t get_offset_of_min_7() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___min_7)); }
	inline float get_min_7() const { return ___min_7; }
	inline float* get_address_of_min_7() { return &___min_7; }
	inline void set_min_7(float value)
	{
		___min_7 = value;
	}

	inline static int32_t get_offset_of_max_8() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___max_8)); }
	inline float get_max_8() const { return ___max_8; }
	inline float* get_address_of_max_8() { return &___max_8; }
	inline void set_max_8(float value)
	{
		___max_8 = value;
	}

	inline static int32_t get_offset_of_onChange_9() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___onChange_9)); }
	inline SpinnerEvent_t3E9EC0B8E5BF9928D00230B8C3F8945BB85CCA3A * get_onChange_9() const { return ___onChange_9; }
	inline SpinnerEvent_t3E9EC0B8E5BF9928D00230B8C3F8945BB85CCA3A ** get_address_of_onChange_9() { return &___onChange_9; }
	inline void set_onChange_9(SpinnerEvent_t3E9EC0B8E5BF9928D00230B8C3F8945BB85CCA3A * value)
	{
		___onChange_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onChange_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnChange_10() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___m_OnChange_10)); }
	inline SpinnerTextEvent_t421ED044855D0F19B55CDC413D30DE08E0E45D63 * get_m_OnChange_10() const { return ___m_OnChange_10; }
	inline SpinnerTextEvent_t421ED044855D0F19B55CDC413D30DE08E0E45D63 ** get_address_of_m_OnChange_10() { return &___m_OnChange_10; }
	inline void set_m_OnChange_10(SpinnerTextEvent_t421ED044855D0F19B55CDC413D30DE08E0E45D63 * value)
	{
		___m_OnChange_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnChange_10), (void*)value);
	}

	inline static int32_t get_offset_of_coroutine_11() { return static_cast<int32_t>(offsetof(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE, ___coroutine_11)); }
	inline RuntimeObject* get_coroutine_11() const { return ___coroutine_11; }
	inline RuntimeObject** get_address_of_coroutine_11() { return &___coroutine_11; }
	inline void set_coroutine_11(RuntimeObject* value)
	{
		___coroutine_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___coroutine_11), (void*)value);
	}
};


// DevionGames.BaseTrigger
struct BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F  : public CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D
{
public:
	// System.Single DevionGames.BaseTrigger::useDistance
	float ___useDistance_5;
	// DevionGames.BaseTrigger/TriggerInputType DevionGames.BaseTrigger::triggerType
	int32_t ___triggerType_6;
	// UnityEngine.KeyCode DevionGames.BaseTrigger::key
	int32_t ___key_7;
	// DevionGames.ITriggerEventHandler[] DevionGames.BaseTrigger::m_TriggerEvents
	ITriggerEventHandlerU5BU5D_tED0AC395E4838FDB2E2A59DA004F78787F1282DF* ___m_TriggerEvents_8;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> DevionGames.BaseTrigger::m_CallbackHandlers
	Dictionary_2_t606AA85440D2767289243C98586DE3C541D183D0 * ___m_CallbackHandlers_11;
	// System.Boolean DevionGames.BaseTrigger::m_CheckBlocking
	bool ___m_CheckBlocking_12;
	// System.Boolean DevionGames.BaseTrigger::m_Started
	bool ___m_Started_13;
	// System.Boolean DevionGames.BaseTrigger::m_InRange
	bool ___m_InRange_14;
	// System.Boolean DevionGames.BaseTrigger::m_InUse
	bool ___m_InUse_15;

public:
	inline static int32_t get_offset_of_useDistance_5() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___useDistance_5)); }
	inline float get_useDistance_5() const { return ___useDistance_5; }
	inline float* get_address_of_useDistance_5() { return &___useDistance_5; }
	inline void set_useDistance_5(float value)
	{
		___useDistance_5 = value;
	}

	inline static int32_t get_offset_of_triggerType_6() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___triggerType_6)); }
	inline int32_t get_triggerType_6() const { return ___triggerType_6; }
	inline int32_t* get_address_of_triggerType_6() { return &___triggerType_6; }
	inline void set_triggerType_6(int32_t value)
	{
		___triggerType_6 = value;
	}

	inline static int32_t get_offset_of_key_7() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___key_7)); }
	inline int32_t get_key_7() const { return ___key_7; }
	inline int32_t* get_address_of_key_7() { return &___key_7; }
	inline void set_key_7(int32_t value)
	{
		___key_7 = value;
	}

	inline static int32_t get_offset_of_m_TriggerEvents_8() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_TriggerEvents_8)); }
	inline ITriggerEventHandlerU5BU5D_tED0AC395E4838FDB2E2A59DA004F78787F1282DF* get_m_TriggerEvents_8() const { return ___m_TriggerEvents_8; }
	inline ITriggerEventHandlerU5BU5D_tED0AC395E4838FDB2E2A59DA004F78787F1282DF** get_address_of_m_TriggerEvents_8() { return &___m_TriggerEvents_8; }
	inline void set_m_TriggerEvents_8(ITriggerEventHandlerU5BU5D_tED0AC395E4838FDB2E2A59DA004F78787F1282DF* value)
	{
		___m_TriggerEvents_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TriggerEvents_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallbackHandlers_11() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_CallbackHandlers_11)); }
	inline Dictionary_2_t606AA85440D2767289243C98586DE3C541D183D0 * get_m_CallbackHandlers_11() const { return ___m_CallbackHandlers_11; }
	inline Dictionary_2_t606AA85440D2767289243C98586DE3C541D183D0 ** get_address_of_m_CallbackHandlers_11() { return &___m_CallbackHandlers_11; }
	inline void set_m_CallbackHandlers_11(Dictionary_2_t606AA85440D2767289243C98586DE3C541D183D0 * value)
	{
		___m_CallbackHandlers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CallbackHandlers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CheckBlocking_12() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_CheckBlocking_12)); }
	inline bool get_m_CheckBlocking_12() const { return ___m_CheckBlocking_12; }
	inline bool* get_address_of_m_CheckBlocking_12() { return &___m_CheckBlocking_12; }
	inline void set_m_CheckBlocking_12(bool value)
	{
		___m_CheckBlocking_12 = value;
	}

	inline static int32_t get_offset_of_m_Started_13() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_Started_13)); }
	inline bool get_m_Started_13() const { return ___m_Started_13; }
	inline bool* get_address_of_m_Started_13() { return &___m_Started_13; }
	inline void set_m_Started_13(bool value)
	{
		___m_Started_13 = value;
	}

	inline static int32_t get_offset_of_m_InRange_14() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_InRange_14)); }
	inline bool get_m_InRange_14() const { return ___m_InRange_14; }
	inline bool* get_address_of_m_InRange_14() { return &___m_InRange_14; }
	inline void set_m_InRange_14(bool value)
	{
		___m_InRange_14 = value;
	}

	inline static int32_t get_offset_of_m_InUse_15() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F, ___m_InUse_15)); }
	inline bool get_m_InUse_15() const { return ___m_InUse_15; }
	inline bool* get_address_of_m_InUse_15() { return &___m_InUse_15; }
	inline void set_m_InUse_15(bool value)
	{
		___m_InUse_15 = value;
	}
};

struct BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_StaticFields
{
public:
	// DevionGames.BaseTrigger DevionGames.BaseTrigger::currentUsedTrigger
	BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F * ___currentUsedTrigger_9;
	// System.Collections.Generic.List`1<DevionGames.BaseTrigger> DevionGames.BaseTrigger::m_TriggerInRange
	List_1_tD4FB55FD3222A9EDA27BDB2EC7AB011A358F2190 * ___m_TriggerInRange_10;

public:
	inline static int32_t get_offset_of_currentUsedTrigger_9() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_StaticFields, ___currentUsedTrigger_9)); }
	inline BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F * get_currentUsedTrigger_9() const { return ___currentUsedTrigger_9; }
	inline BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F ** get_address_of_currentUsedTrigger_9() { return &___currentUsedTrigger_9; }
	inline void set_currentUsedTrigger_9(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F * value)
	{
		___currentUsedTrigger_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentUsedTrigger_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_TriggerInRange_10() { return static_cast<int32_t>(offsetof(BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_StaticFields, ___m_TriggerInRange_10)); }
	inline List_1_tD4FB55FD3222A9EDA27BDB2EC7AB011A358F2190 * get_m_TriggerInRange_10() const { return ___m_TriggerInRange_10; }
	inline List_1_tD4FB55FD3222A9EDA27BDB2EC7AB011A358F2190 ** get_address_of_m_TriggerInRange_10() { return &___m_TriggerInRange_10; }
	inline void set_m_TriggerInRange_10(List_1_tD4FB55FD3222A9EDA27BDB2EC7AB011A358F2190 * value)
	{
		___m_TriggerInRange_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TriggerInRange_10), (void*)value);
	}
};


// DevionGames.SelectableObject
struct SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3  : public CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D
{
public:
	// UnityEngine.Transform DevionGames.SelectableObject::m_Transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3, ___m_Transform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Transform_6), (void*)value);
	}
};

struct SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_StaticFields
{
public:
	// DevionGames.SelectableObject DevionGames.SelectableObject::current
	SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * ___current_5;

public:
	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_StaticFields, ___current_5)); }
	inline SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * get_current_5() const { return ___current_5; }
	inline SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_5), (void*)value);
	}
};


// DevionGames.InventorySystem.Slot
struct Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97  : public CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D
{
public:
	// UnityEngine.UI.Text DevionGames.InventorySystem.Slot::m_ItemName
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_ItemName_5;
	// System.Boolean DevionGames.InventorySystem.Slot::m_UseRarityColor
	bool ___m_UseRarityColor_6;
	// UnityEngine.UI.Image DevionGames.InventorySystem.Slot::m_?con
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_UCDcon_7;
	// UnityEngine.UI.Text DevionGames.InventorySystem.Slot::m_Stack
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Stack_8;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Restriction> DevionGames.InventorySystem.Slot::restrictions
	List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * ___restrictions_9;
	// DevionGames.InventorySystem.Item DevionGames.InventorySystem.Slot::m_Item
	Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___m_Item_10;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Slot::m_Container
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_Container_11;
	// System.Int32 DevionGames.InventorySystem.Slot::m_Index
	int32_t ___m_Index_12;

public:
	inline static int32_t get_offset_of_m_ItemName_5() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_ItemName_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_ItemName_5() const { return ___m_ItemName_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_ItemName_5() { return &___m_ItemName_5; }
	inline void set_m_ItemName_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_ItemName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseRarityColor_6() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_UseRarityColor_6)); }
	inline bool get_m_UseRarityColor_6() const { return ___m_UseRarityColor_6; }
	inline bool* get_address_of_m_UseRarityColor_6() { return &___m_UseRarityColor_6; }
	inline void set_m_UseRarityColor_6(bool value)
	{
		___m_UseRarityColor_6 = value;
	}

	inline static int32_t get_offset_of_m_UCDcon_7() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_UCDcon_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_UCDcon_7() const { return ___m_UCDcon_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_UCDcon_7() { return &___m_UCDcon_7; }
	inline void set_m_UCDcon_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_UCDcon_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UCDcon_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Stack_8() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_Stack_8)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Stack_8() const { return ___m_Stack_8; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Stack_8() { return &___m_Stack_8; }
	inline void set_m_Stack_8(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Stack_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_8), (void*)value);
	}

	inline static int32_t get_offset_of_restrictions_9() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___restrictions_9)); }
	inline List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * get_restrictions_9() const { return ___restrictions_9; }
	inline List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E ** get_address_of_restrictions_9() { return &___restrictions_9; }
	inline void set_restrictions_9(List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * value)
	{
		___restrictions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restrictions_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Item_10() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_Item_10)); }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * get_m_Item_10() const { return ___m_Item_10; }
	inline Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 ** get_address_of_m_Item_10() { return &___m_Item_10; }
	inline void set_m_Item_10(Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * value)
	{
		___m_Item_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Item_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_Container_11() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_Container_11)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_Container_11() const { return ___m_Container_11; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_Container_11() { return &___m_Container_11; }
	inline void set_m_Container_11(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_Container_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Container_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Index_12() { return static_cast<int32_t>(offsetof(Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97, ___m_Index_12)); }
	inline int32_t get_m_Index_12() const { return ___m_Index_12; }
	inline int32_t* get_address_of_m_Index_12() { return &___m_Index_12; }
	inline void set_m_Index_12(int32_t value)
	{
		___m_Index_12 = value;
	}
};


// DevionGames.UIWidgets.UIWidget
struct UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE  : public CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D
{
public:
	// System.String DevionGames.UIWidgets.UIWidget::name
	String_t* ___name_5;
	// System.Int32 DevionGames.UIWidgets.UIWidget::priority
	int32_t ___priority_6;
	// UnityEngine.KeyCode DevionGames.UIWidgets.UIWidget::m_KeyCode
	int32_t ___m_KeyCode_7;
	// DevionGames.UIWidgets.EasingEquations/EaseType DevionGames.UIWidgets.UIWidget::m_EaseType
	int32_t ___m_EaseType_8;
	// System.Single DevionGames.UIWidgets.UIWidget::m_Duration
	float ___m_Duration_9;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_10;
	// UnityEngine.AudioClip DevionGames.UIWidgets.UIWidget::m_ShowSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___m_ShowSound_11;
	// UnityEngine.AudioClip DevionGames.UIWidgets.UIWidget::m_CloseSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___m_CloseSound_12;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_Focus
	bool ___m_Focus_13;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_DeactivateOnClose
	bool ___m_DeactivateOnClose_14;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_ShowAndHideCursor
	bool ___m_ShowAndHideCursor_15;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_CloseOnMove
	bool ___m_CloseOnMove_16;
	// UnityEngine.KeyCode DevionGames.UIWidgets.UIWidget::m_Deactivate
	int32_t ___m_Deactivate_17;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_FocusPlayer
	bool ___m_FocusPlayer_18;
	// UnityEngine.Transform DevionGames.UIWidgets.UIWidget::m_CameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_CameraTransform_21;
	// UnityEngine.MonoBehaviour DevionGames.UIWidgets.UIWidget::m_CameraController
	MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___m_CameraController_22;
	// UnityEngine.MonoBehaviour DevionGames.UIWidgets.UIWidget::m_ThirdPersonController
	MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * ___m_ThirdPersonController_23;
	// UnityEngine.RectTransform DevionGames.UIWidgets.UIWidget::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_26;
	// UnityEngine.CanvasGroup DevionGames.UIWidgets.UIWidget::m_CanvasGroup
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___m_CanvasGroup_27;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_IsShowing
	bool ___m_IsShowing_28;
	// DevionGames.UIWidgets.TweenRunner`1<DevionGames.UIWidgets.FloatTween> DevionGames.UIWidgets.UIWidget::m_AlphaTweenRunner
	TweenRunner_1_t0CF4BBD8446B3BA1A85660580655539A7B35E50C * ___m_AlphaTweenRunner_29;
	// DevionGames.UIWidgets.TweenRunner`1<DevionGames.UIWidgets.Vector3Tween> DevionGames.UIWidgets.UIWidget::m_ScaleTweenRunner
	TweenRunner_1_t43CAACE55716A741A154B38CAC7381374111B29C * ___m_ScaleTweenRunner_30;
	// UnityEngine.UI.Scrollbar[] DevionGames.UIWidgets.UIWidget::m_Scrollbars
	ScrollbarU5BU5D_t81EAF2E6C2496DFC879A8478FD3FA6AFA53A7CF4* ___m_Scrollbars_31;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_IsLocked
	bool ___m_IsLocked_32;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_5), (void*)value);
	}

	inline static int32_t get_offset_of_priority_6() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___priority_6)); }
	inline int32_t get_priority_6() const { return ___priority_6; }
	inline int32_t* get_address_of_priority_6() { return &___priority_6; }
	inline void set_priority_6(int32_t value)
	{
		___priority_6 = value;
	}

	inline static int32_t get_offset_of_m_KeyCode_7() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_KeyCode_7)); }
	inline int32_t get_m_KeyCode_7() const { return ___m_KeyCode_7; }
	inline int32_t* get_address_of_m_KeyCode_7() { return &___m_KeyCode_7; }
	inline void set_m_KeyCode_7(int32_t value)
	{
		___m_KeyCode_7 = value;
	}

	inline static int32_t get_offset_of_m_EaseType_8() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_EaseType_8)); }
	inline int32_t get_m_EaseType_8() const { return ___m_EaseType_8; }
	inline int32_t* get_address_of_m_EaseType_8() { return &___m_EaseType_8; }
	inline void set_m_EaseType_8(int32_t value)
	{
		___m_EaseType_8 = value;
	}

	inline static int32_t get_offset_of_m_Duration_9() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_Duration_9)); }
	inline float get_m_Duration_9() const { return ___m_Duration_9; }
	inline float* get_address_of_m_Duration_9() { return &___m_Duration_9; }
	inline void set_m_Duration_9(float value)
	{
		___m_Duration_9 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_10() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_IgnoreTimeScale_10)); }
	inline bool get_m_IgnoreTimeScale_10() const { return ___m_IgnoreTimeScale_10; }
	inline bool* get_address_of_m_IgnoreTimeScale_10() { return &___m_IgnoreTimeScale_10; }
	inline void set_m_IgnoreTimeScale_10(bool value)
	{
		___m_IgnoreTimeScale_10 = value;
	}

	inline static int32_t get_offset_of_m_ShowSound_11() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_ShowSound_11)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_m_ShowSound_11() const { return ___m_ShowSound_11; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_m_ShowSound_11() { return &___m_ShowSound_11; }
	inline void set_m_ShowSound_11(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___m_ShowSound_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShowSound_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CloseSound_12() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_CloseSound_12)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_m_CloseSound_12() const { return ___m_CloseSound_12; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_m_CloseSound_12() { return &___m_CloseSound_12; }
	inline void set_m_CloseSound_12(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___m_CloseSound_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CloseSound_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Focus_13() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_Focus_13)); }
	inline bool get_m_Focus_13() const { return ___m_Focus_13; }
	inline bool* get_address_of_m_Focus_13() { return &___m_Focus_13; }
	inline void set_m_Focus_13(bool value)
	{
		___m_Focus_13 = value;
	}

	inline static int32_t get_offset_of_m_DeactivateOnClose_14() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_DeactivateOnClose_14)); }
	inline bool get_m_DeactivateOnClose_14() const { return ___m_DeactivateOnClose_14; }
	inline bool* get_address_of_m_DeactivateOnClose_14() { return &___m_DeactivateOnClose_14; }
	inline void set_m_DeactivateOnClose_14(bool value)
	{
		___m_DeactivateOnClose_14 = value;
	}

	inline static int32_t get_offset_of_m_ShowAndHideCursor_15() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_ShowAndHideCursor_15)); }
	inline bool get_m_ShowAndHideCursor_15() const { return ___m_ShowAndHideCursor_15; }
	inline bool* get_address_of_m_ShowAndHideCursor_15() { return &___m_ShowAndHideCursor_15; }
	inline void set_m_ShowAndHideCursor_15(bool value)
	{
		___m_ShowAndHideCursor_15 = value;
	}

	inline static int32_t get_offset_of_m_CloseOnMove_16() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_CloseOnMove_16)); }
	inline bool get_m_CloseOnMove_16() const { return ___m_CloseOnMove_16; }
	inline bool* get_address_of_m_CloseOnMove_16() { return &___m_CloseOnMove_16; }
	inline void set_m_CloseOnMove_16(bool value)
	{
		___m_CloseOnMove_16 = value;
	}

	inline static int32_t get_offset_of_m_Deactivate_17() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_Deactivate_17)); }
	inline int32_t get_m_Deactivate_17() const { return ___m_Deactivate_17; }
	inline int32_t* get_address_of_m_Deactivate_17() { return &___m_Deactivate_17; }
	inline void set_m_Deactivate_17(int32_t value)
	{
		___m_Deactivate_17 = value;
	}

	inline static int32_t get_offset_of_m_FocusPlayer_18() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_FocusPlayer_18)); }
	inline bool get_m_FocusPlayer_18() const { return ___m_FocusPlayer_18; }
	inline bool* get_address_of_m_FocusPlayer_18() { return &___m_FocusPlayer_18; }
	inline void set_m_FocusPlayer_18(bool value)
	{
		___m_FocusPlayer_18 = value;
	}

	inline static int32_t get_offset_of_m_CameraTransform_21() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_CameraTransform_21)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_CameraTransform_21() const { return ___m_CameraTransform_21; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_CameraTransform_21() { return &___m_CameraTransform_21; }
	inline void set_m_CameraTransform_21(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_CameraTransform_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraTransform_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraController_22() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_CameraController_22)); }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * get_m_CameraController_22() const { return ___m_CameraController_22; }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A ** get_address_of_m_CameraController_22() { return &___m_CameraController_22; }
	inline void set_m_CameraController_22(MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * value)
	{
		___m_CameraController_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraController_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ThirdPersonController_23() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_ThirdPersonController_23)); }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * get_m_ThirdPersonController_23() const { return ___m_ThirdPersonController_23; }
	inline MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A ** get_address_of_m_ThirdPersonController_23() { return &___m_ThirdPersonController_23; }
	inline void set_m_ThirdPersonController_23(MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * value)
	{
		___m_ThirdPersonController_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ThirdPersonController_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_RectTransform_26() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_RectTransform_26)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_26() const { return ___m_RectTransform_26; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_26() { return &___m_RectTransform_26; }
	inline void set_m_RectTransform_26(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasGroup_27() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_CanvasGroup_27)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_m_CanvasGroup_27() const { return ___m_CanvasGroup_27; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_m_CanvasGroup_27() { return &___m_CanvasGroup_27; }
	inline void set_m_CanvasGroup_27(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___m_CanvasGroup_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroup_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsShowing_28() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_IsShowing_28)); }
	inline bool get_m_IsShowing_28() const { return ___m_IsShowing_28; }
	inline bool* get_address_of_m_IsShowing_28() { return &___m_IsShowing_28; }
	inline void set_m_IsShowing_28(bool value)
	{
		___m_IsShowing_28 = value;
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_29() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_AlphaTweenRunner_29)); }
	inline TweenRunner_1_t0CF4BBD8446B3BA1A85660580655539A7B35E50C * get_m_AlphaTweenRunner_29() const { return ___m_AlphaTweenRunner_29; }
	inline TweenRunner_1_t0CF4BBD8446B3BA1A85660580655539A7B35E50C ** get_address_of_m_AlphaTweenRunner_29() { return &___m_AlphaTweenRunner_29; }
	inline void set_m_AlphaTweenRunner_29(TweenRunner_1_t0CF4BBD8446B3BA1A85660580655539A7B35E50C * value)
	{
		___m_AlphaTweenRunner_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AlphaTweenRunner_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_ScaleTweenRunner_30() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_ScaleTweenRunner_30)); }
	inline TweenRunner_1_t43CAACE55716A741A154B38CAC7381374111B29C * get_m_ScaleTweenRunner_30() const { return ___m_ScaleTweenRunner_30; }
	inline TweenRunner_1_t43CAACE55716A741A154B38CAC7381374111B29C ** get_address_of_m_ScaleTweenRunner_30() { return &___m_ScaleTweenRunner_30; }
	inline void set_m_ScaleTweenRunner_30(TweenRunner_1_t43CAACE55716A741A154B38CAC7381374111B29C * value)
	{
		___m_ScaleTweenRunner_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ScaleTweenRunner_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_Scrollbars_31() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_Scrollbars_31)); }
	inline ScrollbarU5BU5D_t81EAF2E6C2496DFC879A8478FD3FA6AFA53A7CF4* get_m_Scrollbars_31() const { return ___m_Scrollbars_31; }
	inline ScrollbarU5BU5D_t81EAF2E6C2496DFC879A8478FD3FA6AFA53A7CF4** get_address_of_m_Scrollbars_31() { return &___m_Scrollbars_31; }
	inline void set_m_Scrollbars_31(ScrollbarU5BU5D_t81EAF2E6C2496DFC879A8478FD3FA6AFA53A7CF4* value)
	{
		___m_Scrollbars_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Scrollbars_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsLocked_32() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE, ___m_IsLocked_32)); }
	inline bool get_m_IsLocked_32() const { return ___m_IsLocked_32; }
	inline bool* get_address_of_m_IsLocked_32() { return &___m_IsLocked_32; }
	inline void set_m_IsLocked_32(bool value)
	{
		___m_IsLocked_32 = value;
	}
};

struct UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_StaticFields
{
public:
	// UnityEngine.CursorLockMode DevionGames.UIWidgets.UIWidget::m_PreviousCursorLockMode
	int32_t ___m_PreviousCursorLockMode_19;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_PreviousCursorVisibility
	bool ___m_PreviousCursorVisibility_20;
	// System.Boolean DevionGames.UIWidgets.UIWidget::m_PreviousCameraControllerEnabled
	bool ___m_PreviousCameraControllerEnabled_24;
	// System.Collections.Generic.List`1<DevionGames.UIWidgets.UIWidget> DevionGames.UIWidgets.UIWidget::m_CurrentVisibleWidgets
	List_1_t0EEC4246600C77B5E7CF41453C8A2C10EEA04EED * ___m_CurrentVisibleWidgets_25;

public:
	inline static int32_t get_offset_of_m_PreviousCursorLockMode_19() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_StaticFields, ___m_PreviousCursorLockMode_19)); }
	inline int32_t get_m_PreviousCursorLockMode_19() const { return ___m_PreviousCursorLockMode_19; }
	inline int32_t* get_address_of_m_PreviousCursorLockMode_19() { return &___m_PreviousCursorLockMode_19; }
	inline void set_m_PreviousCursorLockMode_19(int32_t value)
	{
		___m_PreviousCursorLockMode_19 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCursorVisibility_20() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_StaticFields, ___m_PreviousCursorVisibility_20)); }
	inline bool get_m_PreviousCursorVisibility_20() const { return ___m_PreviousCursorVisibility_20; }
	inline bool* get_address_of_m_PreviousCursorVisibility_20() { return &___m_PreviousCursorVisibility_20; }
	inline void set_m_PreviousCursorVisibility_20(bool value)
	{
		___m_PreviousCursorVisibility_20 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCameraControllerEnabled_24() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_StaticFields, ___m_PreviousCameraControllerEnabled_24)); }
	inline bool get_m_PreviousCameraControllerEnabled_24() const { return ___m_PreviousCameraControllerEnabled_24; }
	inline bool* get_address_of_m_PreviousCameraControllerEnabled_24() { return &___m_PreviousCameraControllerEnabled_24; }
	inline void set_m_PreviousCameraControllerEnabled_24(bool value)
	{
		___m_PreviousCameraControllerEnabled_24 = value;
	}

	inline static int32_t get_offset_of_m_CurrentVisibleWidgets_25() { return static_cast<int32_t>(offsetof(UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE_StaticFields, ___m_CurrentVisibleWidgets_25)); }
	inline List_1_t0EEC4246600C77B5E7CF41453C8A2C10EEA04EED * get_m_CurrentVisibleWidgets_25() const { return ___m_CurrentVisibleWidgets_25; }
	inline List_1_t0EEC4246600C77B5E7CF41453C8A2C10EEA04EED ** get_address_of_m_CurrentVisibleWidgets_25() { return &___m_CurrentVisibleWidgets_25; }
	inline void set_m_CurrentVisibleWidgets_25(List_1_t0EEC4246600C77B5E7CF41453C8A2C10EEA04EED * value)
	{
		___m_CurrentVisibleWidgets_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentVisibleWidgets_25), (void*)value);
	}
};


// DevionGames.BehaviorTrigger
struct BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495  : public BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F
{
public:
	// DevionGames.ActionTemplate DevionGames.BehaviorTrigger::actionTemplate
	ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A * ___actionTemplate_16;
	// System.Collections.Generic.List`1<DevionGames.Action> DevionGames.BehaviorTrigger::actions
	List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * ___actions_17;
	// System.Boolean DevionGames.BehaviorTrigger::m_Interruptable
	bool ___m_Interruptable_18;
	// DevionGames.Sequence DevionGames.BehaviorTrigger::m_ActionBehavior
	Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * ___m_ActionBehavior_19;
	// UnityEngine.AnimatorStateInfo[] DevionGames.BehaviorTrigger::m_LayerStateMap
	AnimatorStateInfoU5BU5D_t5493CE60099D0DB79ADFDE73948BBC5832E28572* ___m_LayerStateMap_20;
	// DevionGames.PlayerInfo DevionGames.BehaviorTrigger::m_PlayerInfo
	PlayerInfo_t2F08B1BD9401335FD1DF02191A55DA4FC68E9748 * ___m_PlayerInfo_21;

public:
	inline static int32_t get_offset_of_actionTemplate_16() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___actionTemplate_16)); }
	inline ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A * get_actionTemplate_16() const { return ___actionTemplate_16; }
	inline ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A ** get_address_of_actionTemplate_16() { return &___actionTemplate_16; }
	inline void set_actionTemplate_16(ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A * value)
	{
		___actionTemplate_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actionTemplate_16), (void*)value);
	}

	inline static int32_t get_offset_of_actions_17() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___actions_17)); }
	inline List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * get_actions_17() const { return ___actions_17; }
	inline List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 ** get_address_of_actions_17() { return &___actions_17; }
	inline void set_actions_17(List_1_tBE8172797A4FE279DB75F18EFA27D4A8BD1B6A42 * value)
	{
		___actions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___actions_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interruptable_18() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___m_Interruptable_18)); }
	inline bool get_m_Interruptable_18() const { return ___m_Interruptable_18; }
	inline bool* get_address_of_m_Interruptable_18() { return &___m_Interruptable_18; }
	inline void set_m_Interruptable_18(bool value)
	{
		___m_Interruptable_18 = value;
	}

	inline static int32_t get_offset_of_m_ActionBehavior_19() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___m_ActionBehavior_19)); }
	inline Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * get_m_ActionBehavior_19() const { return ___m_ActionBehavior_19; }
	inline Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 ** get_address_of_m_ActionBehavior_19() { return &___m_ActionBehavior_19; }
	inline void set_m_ActionBehavior_19(Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * value)
	{
		___m_ActionBehavior_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionBehavior_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_LayerStateMap_20() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___m_LayerStateMap_20)); }
	inline AnimatorStateInfoU5BU5D_t5493CE60099D0DB79ADFDE73948BBC5832E28572* get_m_LayerStateMap_20() const { return ___m_LayerStateMap_20; }
	inline AnimatorStateInfoU5BU5D_t5493CE60099D0DB79ADFDE73948BBC5832E28572** get_address_of_m_LayerStateMap_20() { return &___m_LayerStateMap_20; }
	inline void set_m_LayerStateMap_20(AnimatorStateInfoU5BU5D_t5493CE60099D0DB79ADFDE73948BBC5832E28572* value)
	{
		___m_LayerStateMap_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LayerStateMap_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerInfo_21() { return static_cast<int32_t>(offsetof(BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495, ___m_PlayerInfo_21)); }
	inline PlayerInfo_t2F08B1BD9401335FD1DF02191A55DA4FC68E9748 * get_m_PlayerInfo_21() const { return ___m_PlayerInfo_21; }
	inline PlayerInfo_t2F08B1BD9401335FD1DF02191A55DA4FC68E9748 ** get_address_of_m_PlayerInfo_21() { return &___m_PlayerInfo_21; }
	inline void set_m_PlayerInfo_21(PlayerInfo_t2F08B1BD9401335FD1DF02191A55DA4FC68E9748 * value)
	{
		___m_PlayerInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerInfo_21), (void*)value);
	}
};


// DevionGames.InventorySystem.CurrencySlot
struct CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B  : public Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97
{
public:
	// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.CurrencySlot::m_Currency
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___m_Currency_13;
	// System.Boolean DevionGames.InventorySystem.CurrencySlot::m_HideEmptySlot
	bool ___m_HideEmptySlot_14;

public:
	inline static int32_t get_offset_of_m_Currency_13() { return static_cast<int32_t>(offsetof(CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B, ___m_Currency_13)); }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * get_m_Currency_13() const { return ___m_Currency_13; }
	inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 ** get_address_of_m_Currency_13() { return &___m_Currency_13; }
	inline void set_m_Currency_13(Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * value)
	{
		___m_Currency_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Currency_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_HideEmptySlot_14() { return static_cast<int32_t>(offsetof(CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B, ___m_HideEmptySlot_14)); }
	inline bool get_m_HideEmptySlot_14() const { return ___m_HideEmptySlot_14; }
	inline bool* get_address_of_m_HideEmptySlot_14() { return &___m_HideEmptySlot_14; }
	inline void set_m_HideEmptySlot_14(bool value)
	{
		___m_HideEmptySlot_14 = value;
	}
};


// DevionGames.InventorySystem.ItemContainer
struct ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5  : public UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE
{
public:
	// DevionGames.InventorySystem.ItemContainer/AddItemDelegate DevionGames.InventorySystem.ItemContainer::OnAddItem
	AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * ___OnAddItem_33;
	// DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate DevionGames.InventorySystem.ItemContainer::OnFailedToAddItem
	FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * ___OnFailedToAddItem_34;
	// DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate DevionGames.InventorySystem.ItemContainer::OnRemoveItem
	RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * ___OnRemoveItem_35;
	// DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate DevionGames.InventorySystem.ItemContainer::OnFailedToRemoveItem
	FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * ___OnFailedToRemoveItem_36;
	// DevionGames.InventorySystem.ItemContainer/UseItemDelegate DevionGames.InventorySystem.ItemContainer::OnTryUseItem
	UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * ___OnTryUseItem_37;
	// DevionGames.InventorySystem.ItemContainer/UseItemDelegate DevionGames.InventorySystem.ItemContainer::OnUseItem
	UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * ___OnUseItem_38;
	// DevionGames.InventorySystem.ItemContainer/DropItemDelegate DevionGames.InventorySystem.ItemContainer::OnDropItem
	DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * ___OnDropItem_39;
	// DevionGames.InventorySystem.InputButton DevionGames.InventorySystem.ItemContainer::useButton
	int32_t ___useButton_40;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_DynamicContainer
	bool ___m_DynamicContainer_41;
	// UnityEngine.Transform DevionGames.InventorySystem.ItemContainer::m_SlotParent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_SlotParent_42;
	// UnityEngine.GameObject DevionGames.InventorySystem.ItemContainer::m_SlotPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_SlotPrefab_43;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_UseReferences
	bool ___m_UseReferences_44;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanDragIn
	bool ___m_CanDragIn_45;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanDragOut
	bool ___m_CanDragOut_46;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanDropItems
	bool ___m_CanDropItems_47;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanReferenceItems
	bool ___m_CanReferenceItems_48;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanSellItems
	bool ___m_CanSellItems_49;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_CanUseItems
	bool ___m_CanUseItems_50;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_UseContextMenu
	bool ___m_UseContextMenu_51;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_ShowTooltips
	bool ___m_ShowTooltips_52;
	// System.Boolean DevionGames.InventorySystem.ItemContainer::m_MoveUsedItem
	bool ___m_MoveUsedItem_53;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemContainer/MoveItemCondition> DevionGames.InventorySystem.ItemContainer::moveItemConditions
	List_1_t960FB04E8119A41E33517256BFE9A9027EC1B9F4 * ___moveItemConditions_54;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Restriction> DevionGames.InventorySystem.ItemContainer::restrictions
	List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * ___restrictions_55;
	// System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.ItemContainer::m_Slots
	List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * ___m_Slots_56;
	// DevionGames.InventorySystem.ItemCollection DevionGames.InventorySystem.ItemContainer::m_Collection
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * ___m_Collection_57;

public:
	inline static int32_t get_offset_of_OnAddItem_33() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnAddItem_33)); }
	inline AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * get_OnAddItem_33() const { return ___OnAddItem_33; }
	inline AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D ** get_address_of_OnAddItem_33() { return &___OnAddItem_33; }
	inline void set_OnAddItem_33(AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * value)
	{
		___OnAddItem_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAddItem_33), (void*)value);
	}

	inline static int32_t get_offset_of_OnFailedToAddItem_34() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnFailedToAddItem_34)); }
	inline FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * get_OnFailedToAddItem_34() const { return ___OnFailedToAddItem_34; }
	inline FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC ** get_address_of_OnFailedToAddItem_34() { return &___OnFailedToAddItem_34; }
	inline void set_OnFailedToAddItem_34(FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * value)
	{
		___OnFailedToAddItem_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFailedToAddItem_34), (void*)value);
	}

	inline static int32_t get_offset_of_OnRemoveItem_35() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnRemoveItem_35)); }
	inline RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * get_OnRemoveItem_35() const { return ___OnRemoveItem_35; }
	inline RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 ** get_address_of_OnRemoveItem_35() { return &___OnRemoveItem_35; }
	inline void set_OnRemoveItem_35(RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * value)
	{
		___OnRemoveItem_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRemoveItem_35), (void*)value);
	}

	inline static int32_t get_offset_of_OnFailedToRemoveItem_36() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnFailedToRemoveItem_36)); }
	inline FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * get_OnFailedToRemoveItem_36() const { return ___OnFailedToRemoveItem_36; }
	inline FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 ** get_address_of_OnFailedToRemoveItem_36() { return &___OnFailedToRemoveItem_36; }
	inline void set_OnFailedToRemoveItem_36(FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * value)
	{
		___OnFailedToRemoveItem_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnFailedToRemoveItem_36), (void*)value);
	}

	inline static int32_t get_offset_of_OnTryUseItem_37() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnTryUseItem_37)); }
	inline UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * get_OnTryUseItem_37() const { return ___OnTryUseItem_37; }
	inline UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B ** get_address_of_OnTryUseItem_37() { return &___OnTryUseItem_37; }
	inline void set_OnTryUseItem_37(UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * value)
	{
		___OnTryUseItem_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnTryUseItem_37), (void*)value);
	}

	inline static int32_t get_offset_of_OnUseItem_38() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnUseItem_38)); }
	inline UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * get_OnUseItem_38() const { return ___OnUseItem_38; }
	inline UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B ** get_address_of_OnUseItem_38() { return &___OnUseItem_38; }
	inline void set_OnUseItem_38(UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * value)
	{
		___OnUseItem_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnUseItem_38), (void*)value);
	}

	inline static int32_t get_offset_of_OnDropItem_39() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___OnDropItem_39)); }
	inline DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * get_OnDropItem_39() const { return ___OnDropItem_39; }
	inline DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D ** get_address_of_OnDropItem_39() { return &___OnDropItem_39; }
	inline void set_OnDropItem_39(DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * value)
	{
		___OnDropItem_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDropItem_39), (void*)value);
	}

	inline static int32_t get_offset_of_useButton_40() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___useButton_40)); }
	inline int32_t get_useButton_40() const { return ___useButton_40; }
	inline int32_t* get_address_of_useButton_40() { return &___useButton_40; }
	inline void set_useButton_40(int32_t value)
	{
		___useButton_40 = value;
	}

	inline static int32_t get_offset_of_m_DynamicContainer_41() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_DynamicContainer_41)); }
	inline bool get_m_DynamicContainer_41() const { return ___m_DynamicContainer_41; }
	inline bool* get_address_of_m_DynamicContainer_41() { return &___m_DynamicContainer_41; }
	inline void set_m_DynamicContainer_41(bool value)
	{
		___m_DynamicContainer_41 = value;
	}

	inline static int32_t get_offset_of_m_SlotParent_42() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_SlotParent_42)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_SlotParent_42() const { return ___m_SlotParent_42; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_SlotParent_42() { return &___m_SlotParent_42; }
	inline void set_m_SlotParent_42(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_SlotParent_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlotParent_42), (void*)value);
	}

	inline static int32_t get_offset_of_m_SlotPrefab_43() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_SlotPrefab_43)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_SlotPrefab_43() const { return ___m_SlotPrefab_43; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_SlotPrefab_43() { return &___m_SlotPrefab_43; }
	inline void set_m_SlotPrefab_43(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_SlotPrefab_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlotPrefab_43), (void*)value);
	}

	inline static int32_t get_offset_of_m_UseReferences_44() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_UseReferences_44)); }
	inline bool get_m_UseReferences_44() const { return ___m_UseReferences_44; }
	inline bool* get_address_of_m_UseReferences_44() { return &___m_UseReferences_44; }
	inline void set_m_UseReferences_44(bool value)
	{
		___m_UseReferences_44 = value;
	}

	inline static int32_t get_offset_of_m_CanDragIn_45() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanDragIn_45)); }
	inline bool get_m_CanDragIn_45() const { return ___m_CanDragIn_45; }
	inline bool* get_address_of_m_CanDragIn_45() { return &___m_CanDragIn_45; }
	inline void set_m_CanDragIn_45(bool value)
	{
		___m_CanDragIn_45 = value;
	}

	inline static int32_t get_offset_of_m_CanDragOut_46() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanDragOut_46)); }
	inline bool get_m_CanDragOut_46() const { return ___m_CanDragOut_46; }
	inline bool* get_address_of_m_CanDragOut_46() { return &___m_CanDragOut_46; }
	inline void set_m_CanDragOut_46(bool value)
	{
		___m_CanDragOut_46 = value;
	}

	inline static int32_t get_offset_of_m_CanDropItems_47() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanDropItems_47)); }
	inline bool get_m_CanDropItems_47() const { return ___m_CanDropItems_47; }
	inline bool* get_address_of_m_CanDropItems_47() { return &___m_CanDropItems_47; }
	inline void set_m_CanDropItems_47(bool value)
	{
		___m_CanDropItems_47 = value;
	}

	inline static int32_t get_offset_of_m_CanReferenceItems_48() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanReferenceItems_48)); }
	inline bool get_m_CanReferenceItems_48() const { return ___m_CanReferenceItems_48; }
	inline bool* get_address_of_m_CanReferenceItems_48() { return &___m_CanReferenceItems_48; }
	inline void set_m_CanReferenceItems_48(bool value)
	{
		___m_CanReferenceItems_48 = value;
	}

	inline static int32_t get_offset_of_m_CanSellItems_49() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanSellItems_49)); }
	inline bool get_m_CanSellItems_49() const { return ___m_CanSellItems_49; }
	inline bool* get_address_of_m_CanSellItems_49() { return &___m_CanSellItems_49; }
	inline void set_m_CanSellItems_49(bool value)
	{
		___m_CanSellItems_49 = value;
	}

	inline static int32_t get_offset_of_m_CanUseItems_50() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_CanUseItems_50)); }
	inline bool get_m_CanUseItems_50() const { return ___m_CanUseItems_50; }
	inline bool* get_address_of_m_CanUseItems_50() { return &___m_CanUseItems_50; }
	inline void set_m_CanUseItems_50(bool value)
	{
		___m_CanUseItems_50 = value;
	}

	inline static int32_t get_offset_of_m_UseContextMenu_51() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_UseContextMenu_51)); }
	inline bool get_m_UseContextMenu_51() const { return ___m_UseContextMenu_51; }
	inline bool* get_address_of_m_UseContextMenu_51() { return &___m_UseContextMenu_51; }
	inline void set_m_UseContextMenu_51(bool value)
	{
		___m_UseContextMenu_51 = value;
	}

	inline static int32_t get_offset_of_m_ShowTooltips_52() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_ShowTooltips_52)); }
	inline bool get_m_ShowTooltips_52() const { return ___m_ShowTooltips_52; }
	inline bool* get_address_of_m_ShowTooltips_52() { return &___m_ShowTooltips_52; }
	inline void set_m_ShowTooltips_52(bool value)
	{
		___m_ShowTooltips_52 = value;
	}

	inline static int32_t get_offset_of_m_MoveUsedItem_53() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_MoveUsedItem_53)); }
	inline bool get_m_MoveUsedItem_53() const { return ___m_MoveUsedItem_53; }
	inline bool* get_address_of_m_MoveUsedItem_53() { return &___m_MoveUsedItem_53; }
	inline void set_m_MoveUsedItem_53(bool value)
	{
		___m_MoveUsedItem_53 = value;
	}

	inline static int32_t get_offset_of_moveItemConditions_54() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___moveItemConditions_54)); }
	inline List_1_t960FB04E8119A41E33517256BFE9A9027EC1B9F4 * get_moveItemConditions_54() const { return ___moveItemConditions_54; }
	inline List_1_t960FB04E8119A41E33517256BFE9A9027EC1B9F4 ** get_address_of_moveItemConditions_54() { return &___moveItemConditions_54; }
	inline void set_moveItemConditions_54(List_1_t960FB04E8119A41E33517256BFE9A9027EC1B9F4 * value)
	{
		___moveItemConditions_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moveItemConditions_54), (void*)value);
	}

	inline static int32_t get_offset_of_restrictions_55() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___restrictions_55)); }
	inline List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * get_restrictions_55() const { return ___restrictions_55; }
	inline List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E ** get_address_of_restrictions_55() { return &___restrictions_55; }
	inline void set_restrictions_55(List_1_t9C96B6F73205151DD23ADA49C058A16CE678A17E * value)
	{
		___restrictions_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restrictions_55), (void*)value);
	}

	inline static int32_t get_offset_of_m_Slots_56() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_Slots_56)); }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * get_m_Slots_56() const { return ___m_Slots_56; }
	inline List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE ** get_address_of_m_Slots_56() { return &___m_Slots_56; }
	inline void set_m_Slots_56(List_1_t12B40C54ACBA7B2E773700295E0172BC093ED8CE * value)
	{
		___m_Slots_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Slots_56), (void*)value);
	}

	inline static int32_t get_offset_of_m_Collection_57() { return static_cast<int32_t>(offsetof(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5, ___m_Collection_57)); }
	inline ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * get_m_Collection_57() const { return ___m_Collection_57; }
	inline ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 ** get_address_of_m_Collection_57() { return &___m_Collection_57; }
	inline void set_m_Collection_57(ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * value)
	{
		___m_Collection_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Collection_57), (void*)value);
	}
};


// DevionGames.InventorySystem.ItemSlot
struct ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1  : public Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97
{
public:
	// UnityEngine.KeyCode DevionGames.InventorySystem.ItemSlot::m_UseKey
	int32_t ___m_UseKey_13;
	// UnityEngine.UI.Text DevionGames.InventorySystem.ItemSlot::m_Key
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Key_14;
	// UnityEngine.UI.Image DevionGames.InventorySystem.ItemSlot::m_CooldownOverlay
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_CooldownOverlay_15;
	// UnityEngine.UI.Text DevionGames.InventorySystem.ItemSlot::m_Cooldown
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Cooldown_16;
	// UnityEngine.UI.Text DevionGames.InventorySystem.ItemSlot::m_Description
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Description_17;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.ItemSlot::m_Ingredients
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_Ingredients_18;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.ItemSlot::m_BuyPrice
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_BuyPrice_19;
	// System.Boolean DevionGames.InventorySystem.ItemSlot::m_IsCooldown
	bool ___m_IsCooldown_20;
	// System.Single DevionGames.InventorySystem.ItemSlot::cooldownDuration
	float ___cooldownDuration_21;
	// System.Single DevionGames.InventorySystem.ItemSlot::cooldownInitTime
	float ___cooldownInitTime_22;
	// UnityEngine.Coroutine DevionGames.InventorySystem.ItemSlot::m_DelayTooltipCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___m_DelayTooltipCoroutine_24;
	// UnityEngine.UI.ScrollRect DevionGames.InventorySystem.ItemSlot::m_ParentScrollRect
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___m_ParentScrollRect_25;
	// System.Boolean DevionGames.InventorySystem.ItemSlot::m_IsMouseKey
	bool ___m_IsMouseKey_26;

public:
	inline static int32_t get_offset_of_m_UseKey_13() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_UseKey_13)); }
	inline int32_t get_m_UseKey_13() const { return ___m_UseKey_13; }
	inline int32_t* get_address_of_m_UseKey_13() { return &___m_UseKey_13; }
	inline void set_m_UseKey_13(int32_t value)
	{
		___m_UseKey_13 = value;
	}

	inline static int32_t get_offset_of_m_Key_14() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_Key_14)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Key_14() const { return ___m_Key_14; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Key_14() { return &___m_Key_14; }
	inline void set_m_Key_14(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Key_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Key_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_CooldownOverlay_15() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_CooldownOverlay_15)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_CooldownOverlay_15() const { return ___m_CooldownOverlay_15; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_CooldownOverlay_15() { return &___m_CooldownOverlay_15; }
	inline void set_m_CooldownOverlay_15(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_CooldownOverlay_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CooldownOverlay_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_Cooldown_16() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_Cooldown_16)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Cooldown_16() const { return ___m_Cooldown_16; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Cooldown_16() { return &___m_Cooldown_16; }
	inline void set_m_Cooldown_16(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Cooldown_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Cooldown_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_Description_17() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_Description_17)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Description_17() const { return ___m_Description_17; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Description_17() { return &___m_Description_17; }
	inline void set_m_Description_17(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Description_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Description_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ingredients_18() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_Ingredients_18)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_Ingredients_18() const { return ___m_Ingredients_18; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_Ingredients_18() { return &___m_Ingredients_18; }
	inline void set_m_Ingredients_18(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_Ingredients_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Ingredients_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_BuyPrice_19() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_BuyPrice_19)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_BuyPrice_19() const { return ___m_BuyPrice_19; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_BuyPrice_19() { return &___m_BuyPrice_19; }
	inline void set_m_BuyPrice_19(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_BuyPrice_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuyPrice_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsCooldown_20() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_IsCooldown_20)); }
	inline bool get_m_IsCooldown_20() const { return ___m_IsCooldown_20; }
	inline bool* get_address_of_m_IsCooldown_20() { return &___m_IsCooldown_20; }
	inline void set_m_IsCooldown_20(bool value)
	{
		___m_IsCooldown_20 = value;
	}

	inline static int32_t get_offset_of_cooldownDuration_21() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___cooldownDuration_21)); }
	inline float get_cooldownDuration_21() const { return ___cooldownDuration_21; }
	inline float* get_address_of_cooldownDuration_21() { return &___cooldownDuration_21; }
	inline void set_cooldownDuration_21(float value)
	{
		___cooldownDuration_21 = value;
	}

	inline static int32_t get_offset_of_cooldownInitTime_22() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___cooldownInitTime_22)); }
	inline float get_cooldownInitTime_22() const { return ___cooldownInitTime_22; }
	inline float* get_address_of_cooldownInitTime_22() { return &___cooldownInitTime_22; }
	inline void set_cooldownInitTime_22(float value)
	{
		___cooldownInitTime_22 = value;
	}

	inline static int32_t get_offset_of_m_DelayTooltipCoroutine_24() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_DelayTooltipCoroutine_24)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_m_DelayTooltipCoroutine_24() const { return ___m_DelayTooltipCoroutine_24; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_m_DelayTooltipCoroutine_24() { return &___m_DelayTooltipCoroutine_24; }
	inline void set_m_DelayTooltipCoroutine_24(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___m_DelayTooltipCoroutine_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DelayTooltipCoroutine_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentScrollRect_25() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_ParentScrollRect_25)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_m_ParentScrollRect_25() const { return ___m_ParentScrollRect_25; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_m_ParentScrollRect_25() { return &___m_ParentScrollRect_25; }
	inline void set_m_ParentScrollRect_25(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___m_ParentScrollRect_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentScrollRect_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_IsMouseKey_26() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1, ___m_IsMouseKey_26)); }
	inline bool get_m_IsMouseKey_26() const { return ___m_IsMouseKey_26; }
	inline bool* get_address_of_m_IsMouseKey_26() { return &___m_IsMouseKey_26; }
	inline void set_m_IsMouseKey_26(bool value)
	{
		___m_IsMouseKey_26 = value;
	}
};

struct ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_StaticFields
{
public:
	// DevionGames.InventorySystem.ItemSlot/DragObject DevionGames.InventorySystem.ItemSlot::m_DragObject
	DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82 * ___m_DragObject_23;

public:
	inline static int32_t get_offset_of_m_DragObject_23() { return static_cast<int32_t>(offsetof(ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1_StaticFields, ___m_DragObject_23)); }
	inline DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82 * get_m_DragObject_23() const { return ___m_DragObject_23; }
	inline DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82 ** get_address_of_m_DragObject_23() { return &___m_DragObject_23; }
	inline void set_m_DragObject_23(DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82 * value)
	{
		___m_DragObject_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DragObject_23), (void*)value);
	}
};


// DevionGames.UIWidgets.Tooltip
struct Tooltip_t84784622E964293493EBDE375C05FB4BD736F859  : public UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE
{
public:
	// System.Boolean DevionGames.UIWidgets.Tooltip::m_UpdatePosition
	bool ___m_UpdatePosition_33;
	// UnityEngine.Vector2 DevionGames.UIWidgets.Tooltip::m_PositionOffset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_PositionOffset_34;
	// UnityEngine.UI.Text DevionGames.UIWidgets.Tooltip::m_Title
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Title_35;
	// UnityEngine.UI.Text DevionGames.UIWidgets.Tooltip::m_Text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Text_36;
	// UnityEngine.UI.Image DevionGames.UIWidgets.Tooltip::m_Icon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_Icon_37;
	// UnityEngine.UI.Image DevionGames.UIWidgets.Tooltip::m_Background
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_Background_38;
	// DevionGames.UIWidgets.StringPairSlot DevionGames.UIWidgets.Tooltip::m_SlotPrefab
	StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A * ___m_SlotPrefab_39;
	// UnityEngine.Canvas DevionGames.UIWidgets.Tooltip::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_40;
	// UnityEngine.Transform DevionGames.UIWidgets.Tooltip::m_SlotParent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_SlotParent_41;
	// System.Single DevionGames.UIWidgets.Tooltip::m_Width
	float ___m_Width_42;
	// System.Collections.Generic.List`1<DevionGames.UIWidgets.StringPairSlot> DevionGames.UIWidgets.Tooltip::m_SlotCache
	List_1_t400A86024C0EA3AAD0CA723BB0317E0B2F22532D * ___m_SlotCache_43;
	// System.Boolean DevionGames.UIWidgets.Tooltip::_updatePosition
	bool ____updatePosition_44;

public:
	inline static int32_t get_offset_of_m_UpdatePosition_33() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_UpdatePosition_33)); }
	inline bool get_m_UpdatePosition_33() const { return ___m_UpdatePosition_33; }
	inline bool* get_address_of_m_UpdatePosition_33() { return &___m_UpdatePosition_33; }
	inline void set_m_UpdatePosition_33(bool value)
	{
		___m_UpdatePosition_33 = value;
	}

	inline static int32_t get_offset_of_m_PositionOffset_34() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_PositionOffset_34)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_PositionOffset_34() const { return ___m_PositionOffset_34; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_PositionOffset_34() { return &___m_PositionOffset_34; }
	inline void set_m_PositionOffset_34(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_PositionOffset_34 = value;
	}

	inline static int32_t get_offset_of_m_Title_35() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Title_35)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Title_35() const { return ___m_Title_35; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Title_35() { return &___m_Title_35; }
	inline void set_m_Title_35(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Title_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Title_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_36() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Text_36)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Text_36() const { return ___m_Text_36; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Text_36() { return &___m_Text_36; }
	inline void set_m_Text_36(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Text_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Icon_37() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Icon_37)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_Icon_37() const { return ___m_Icon_37; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_Icon_37() { return &___m_Icon_37; }
	inline void set_m_Icon_37(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_Icon_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Icon_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_Background_38() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Background_38)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_Background_38() const { return ___m_Background_38; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_Background_38() { return &___m_Background_38; }
	inline void set_m_Background_38(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_Background_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Background_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_SlotPrefab_39() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_SlotPrefab_39)); }
	inline StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A * get_m_SlotPrefab_39() const { return ___m_SlotPrefab_39; }
	inline StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A ** get_address_of_m_SlotPrefab_39() { return &___m_SlotPrefab_39; }
	inline void set_m_SlotPrefab_39(StringPairSlot_t535AB5309B2DCC524AE8A5B7EDBF7FB06650250A * value)
	{
		___m_SlotPrefab_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlotPrefab_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_40() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Canvas_40)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_40() const { return ___m_Canvas_40; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_40() { return &___m_Canvas_40; }
	inline void set_m_Canvas_40(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_SlotParent_41() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_SlotParent_41)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_SlotParent_41() const { return ___m_SlotParent_41; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_SlotParent_41() { return &___m_SlotParent_41; }
	inline void set_m_SlotParent_41(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_SlotParent_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlotParent_41), (void*)value);
	}

	inline static int32_t get_offset_of_m_Width_42() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_Width_42)); }
	inline float get_m_Width_42() const { return ___m_Width_42; }
	inline float* get_address_of_m_Width_42() { return &___m_Width_42; }
	inline void set_m_Width_42(float value)
	{
		___m_Width_42 = value;
	}

	inline static int32_t get_offset_of_m_SlotCache_43() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ___m_SlotCache_43)); }
	inline List_1_t400A86024C0EA3AAD0CA723BB0317E0B2F22532D * get_m_SlotCache_43() const { return ___m_SlotCache_43; }
	inline List_1_t400A86024C0EA3AAD0CA723BB0317E0B2F22532D ** get_address_of_m_SlotCache_43() { return &___m_SlotCache_43; }
	inline void set_m_SlotCache_43(List_1_t400A86024C0EA3AAD0CA723BB0317E0B2F22532D * value)
	{
		___m_SlotCache_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlotCache_43), (void*)value);
	}

	inline static int32_t get_offset_of__updatePosition_44() { return static_cast<int32_t>(offsetof(Tooltip_t84784622E964293493EBDE375C05FB4BD736F859, ____updatePosition_44)); }
	inline bool get__updatePosition_44() const { return ____updatePosition_44; }
	inline bool* get_address_of__updatePosition_44() { return &____updatePosition_44; }
	inline void set__updatePosition_44(bool value)
	{
		____updatePosition_44 = value;
	}
};


// DevionGames.InventorySystem.Trigger
struct Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723  : public BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495
{
public:

public:
};

struct Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_StaticFields
{
public:
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Trigger::currentUsedWindow
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___currentUsedWindow_22;

public:
	inline static int32_t get_offset_of_currentUsedWindow_22() { return static_cast<int32_t>(offsetof(Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_StaticFields, ___currentUsedWindow_22)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_currentUsedWindow_22() const { return ___currentUsedWindow_22; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_currentUsedWindow_22() { return &___currentUsedWindow_22; }
	inline void set_currentUsedWindow_22(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___currentUsedWindow_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentUsedWindow_22), (void*)value);
	}
};


// DevionGames.InventorySystem.VendorTrigger
struct VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B  : public Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723
{
public:
	// System.Single DevionGames.InventorySystem.VendorTrigger::m_BuyPriceFactor
	float ___m_BuyPriceFactor_23;
	// System.Single DevionGames.InventorySystem.VendorTrigger::m_SellPriceFactor
	float ___m_SellPriceFactor_24;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_PurchasedStorageWindow
	String_t* ___m_PurchasedStorageWindow_25;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_PaymentWindow
	String_t* ___m_PaymentWindow_26;
	// System.Boolean DevionGames.InventorySystem.VendorTrigger::m_RemoveItemAfterPurchase
	bool ___m_RemoveItemAfterPurchase_27;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_BuySellDialogName
	String_t* ___m_BuySellDialogName_28;
	// System.Boolean DevionGames.InventorySystem.VendorTrigger::m_DisplaySpinner
	bool ___m_DisplaySpinner_29;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_BuyDialogTitle
	String_t* ___m_BuyDialogTitle_30;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_BuyDialogText
	String_t* ___m_BuyDialogText_31;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_BuyDialogButton
	String_t* ___m_BuyDialogButton_32;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_SellDialogTitle
	String_t* ___m_SellDialogTitle_33;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_SellSingleDialogText
	String_t* ___m_SellSingleDialogText_34;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_SellMultipleDialogText
	String_t* ___m_SellMultipleDialogText_35;
	// System.String DevionGames.InventorySystem.VendorTrigger::m_SellDialogButton
	String_t* ___m_SellDialogButton_36;
	// DevionGames.UIWidgets.DialogBox DevionGames.InventorySystem.VendorTrigger::m_BuySellDialog
	DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486 * ___m_BuySellDialog_37;
	// DevionGames.UIWidgets.Spinner DevionGames.InventorySystem.VendorTrigger::m_AmountSpinner
	Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * ___m_AmountSpinner_38;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.VendorTrigger::m_PriceInfo
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_PriceInfo_39;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.VendorTrigger::m_PurchasedStorageContainer
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_PurchasedStorageContainer_40;
	// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.VendorTrigger::m_PaymentContainer
	ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * ___m_PaymentContainer_41;

public:
	inline static int32_t get_offset_of_m_BuyPriceFactor_23() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuyPriceFactor_23)); }
	inline float get_m_BuyPriceFactor_23() const { return ___m_BuyPriceFactor_23; }
	inline float* get_address_of_m_BuyPriceFactor_23() { return &___m_BuyPriceFactor_23; }
	inline void set_m_BuyPriceFactor_23(float value)
	{
		___m_BuyPriceFactor_23 = value;
	}

	inline static int32_t get_offset_of_m_SellPriceFactor_24() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_SellPriceFactor_24)); }
	inline float get_m_SellPriceFactor_24() const { return ___m_SellPriceFactor_24; }
	inline float* get_address_of_m_SellPriceFactor_24() { return &___m_SellPriceFactor_24; }
	inline void set_m_SellPriceFactor_24(float value)
	{
		___m_SellPriceFactor_24 = value;
	}

	inline static int32_t get_offset_of_m_PurchasedStorageWindow_25() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_PurchasedStorageWindow_25)); }
	inline String_t* get_m_PurchasedStorageWindow_25() const { return ___m_PurchasedStorageWindow_25; }
	inline String_t** get_address_of_m_PurchasedStorageWindow_25() { return &___m_PurchasedStorageWindow_25; }
	inline void set_m_PurchasedStorageWindow_25(String_t* value)
	{
		___m_PurchasedStorageWindow_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PurchasedStorageWindow_25), (void*)value);
	}

	inline static int32_t get_offset_of_m_PaymentWindow_26() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_PaymentWindow_26)); }
	inline String_t* get_m_PaymentWindow_26() const { return ___m_PaymentWindow_26; }
	inline String_t** get_address_of_m_PaymentWindow_26() { return &___m_PaymentWindow_26; }
	inline void set_m_PaymentWindow_26(String_t* value)
	{
		___m_PaymentWindow_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PaymentWindow_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_RemoveItemAfterPurchase_27() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_RemoveItemAfterPurchase_27)); }
	inline bool get_m_RemoveItemAfterPurchase_27() const { return ___m_RemoveItemAfterPurchase_27; }
	inline bool* get_address_of_m_RemoveItemAfterPurchase_27() { return &___m_RemoveItemAfterPurchase_27; }
	inline void set_m_RemoveItemAfterPurchase_27(bool value)
	{
		___m_RemoveItemAfterPurchase_27 = value;
	}

	inline static int32_t get_offset_of_m_BuySellDialogName_28() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuySellDialogName_28)); }
	inline String_t* get_m_BuySellDialogName_28() const { return ___m_BuySellDialogName_28; }
	inline String_t** get_address_of_m_BuySellDialogName_28() { return &___m_BuySellDialogName_28; }
	inline void set_m_BuySellDialogName_28(String_t* value)
	{
		___m_BuySellDialogName_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuySellDialogName_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplaySpinner_29() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_DisplaySpinner_29)); }
	inline bool get_m_DisplaySpinner_29() const { return ___m_DisplaySpinner_29; }
	inline bool* get_address_of_m_DisplaySpinner_29() { return &___m_DisplaySpinner_29; }
	inline void set_m_DisplaySpinner_29(bool value)
	{
		___m_DisplaySpinner_29 = value;
	}

	inline static int32_t get_offset_of_m_BuyDialogTitle_30() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuyDialogTitle_30)); }
	inline String_t* get_m_BuyDialogTitle_30() const { return ___m_BuyDialogTitle_30; }
	inline String_t** get_address_of_m_BuyDialogTitle_30() { return &___m_BuyDialogTitle_30; }
	inline void set_m_BuyDialogTitle_30(String_t* value)
	{
		___m_BuyDialogTitle_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuyDialogTitle_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_BuyDialogText_31() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuyDialogText_31)); }
	inline String_t* get_m_BuyDialogText_31() const { return ___m_BuyDialogText_31; }
	inline String_t** get_address_of_m_BuyDialogText_31() { return &___m_BuyDialogText_31; }
	inline void set_m_BuyDialogText_31(String_t* value)
	{
		___m_BuyDialogText_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuyDialogText_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_BuyDialogButton_32() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuyDialogButton_32)); }
	inline String_t* get_m_BuyDialogButton_32() const { return ___m_BuyDialogButton_32; }
	inline String_t** get_address_of_m_BuyDialogButton_32() { return &___m_BuyDialogButton_32; }
	inline void set_m_BuyDialogButton_32(String_t* value)
	{
		___m_BuyDialogButton_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuyDialogButton_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellDialogTitle_33() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_SellDialogTitle_33)); }
	inline String_t* get_m_SellDialogTitle_33() const { return ___m_SellDialogTitle_33; }
	inline String_t** get_address_of_m_SellDialogTitle_33() { return &___m_SellDialogTitle_33; }
	inline void set_m_SellDialogTitle_33(String_t* value)
	{
		___m_SellDialogTitle_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellDialogTitle_33), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellSingleDialogText_34() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_SellSingleDialogText_34)); }
	inline String_t* get_m_SellSingleDialogText_34() const { return ___m_SellSingleDialogText_34; }
	inline String_t** get_address_of_m_SellSingleDialogText_34() { return &___m_SellSingleDialogText_34; }
	inline void set_m_SellSingleDialogText_34(String_t* value)
	{
		___m_SellSingleDialogText_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellSingleDialogText_34), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellMultipleDialogText_35() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_SellMultipleDialogText_35)); }
	inline String_t* get_m_SellMultipleDialogText_35() const { return ___m_SellMultipleDialogText_35; }
	inline String_t** get_address_of_m_SellMultipleDialogText_35() { return &___m_SellMultipleDialogText_35; }
	inline void set_m_SellMultipleDialogText_35(String_t* value)
	{
		___m_SellMultipleDialogText_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellMultipleDialogText_35), (void*)value);
	}

	inline static int32_t get_offset_of_m_SellDialogButton_36() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_SellDialogButton_36)); }
	inline String_t* get_m_SellDialogButton_36() const { return ___m_SellDialogButton_36; }
	inline String_t** get_address_of_m_SellDialogButton_36() { return &___m_SellDialogButton_36; }
	inline void set_m_SellDialogButton_36(String_t* value)
	{
		___m_SellDialogButton_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SellDialogButton_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_BuySellDialog_37() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_BuySellDialog_37)); }
	inline DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486 * get_m_BuySellDialog_37() const { return ___m_BuySellDialog_37; }
	inline DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486 ** get_address_of_m_BuySellDialog_37() { return &___m_BuySellDialog_37; }
	inline void set_m_BuySellDialog_37(DialogBox_tCDDCA8B7367F90C32C6AFE47F1D2E0135DE0D486 * value)
	{
		___m_BuySellDialog_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BuySellDialog_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_AmountSpinner_38() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_AmountSpinner_38)); }
	inline Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * get_m_AmountSpinner_38() const { return ___m_AmountSpinner_38; }
	inline Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE ** get_address_of_m_AmountSpinner_38() { return &___m_AmountSpinner_38; }
	inline void set_m_AmountSpinner_38(Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * value)
	{
		___m_AmountSpinner_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AmountSpinner_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_PriceInfo_39() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_PriceInfo_39)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_PriceInfo_39() const { return ___m_PriceInfo_39; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_PriceInfo_39() { return &___m_PriceInfo_39; }
	inline void set_m_PriceInfo_39(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_PriceInfo_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PriceInfo_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_PurchasedStorageContainer_40() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_PurchasedStorageContainer_40)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_PurchasedStorageContainer_40() const { return ___m_PurchasedStorageContainer_40; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_PurchasedStorageContainer_40() { return &___m_PurchasedStorageContainer_40; }
	inline void set_m_PurchasedStorageContainer_40(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_PurchasedStorageContainer_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PurchasedStorageContainer_40), (void*)value);
	}

	inline static int32_t get_offset_of_m_PaymentContainer_41() { return static_cast<int32_t>(offsetof(VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B, ___m_PaymentContainer_41)); }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * get_m_PaymentContainer_41() const { return ___m_PaymentContainer_41; }
	inline ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 ** get_address_of_m_PaymentContainer_41() { return &___m_PaymentContainer_41; }
	inline void set_m_PaymentContainer_41(ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * value)
	{
		___m_PaymentContainer_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PaymentContainer_41), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DevionGames.InventorySystem.IGenerator[]
struct IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * m_Items[1];

public:
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared (RuntimeObject * ___original0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* GameObject_GetComponents_TisRuntimeObject_mAB26971A1F37F81EEEF20F7897AA6FAE3B33779E_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Item::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Boolean DevionGames.InventorySystem.Slot::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644 (Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.Item DevionGames.InventorySystem.Slot::get_ObservedItem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438 (Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Item::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean System.Type::op_Equality(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046 (Type_t * ___left0, Type_t * ___right1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.Item::get_Rarity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void ItemContainerCallbackTester/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m933F59E5DF1ED926E1DAC9D75D0E33959989E6A0 (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Slot::get_Container()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D (Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * __this, const RuntimeMethod* method);
// System.String DevionGames.UIWidgets.UIWidget::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UIWidget_get_Name_mE16241740E8F042B088BD63CA5D11C7F90EBF90B (UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE * __this, const RuntimeMethod* method);
// System.Int32 DevionGames.InventorySystem.Slot::get_Index()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Slot_get_Index_mC035A81C33D92B432421E15673C429D96FEB405D (Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.ItemGroup::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B (ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Rarity::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21 (Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Category::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F (Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.EquipmentRegion::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* EquipmentRegion_get_Name_mE9156A7BE449EA2F9B3BDD27CCCDFE8741A6FC07 (EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.Category DevionGames.InventorySystem.Item::get_Category()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * Item_get_Category_mA2301BF94DD3C9FCE88171937B00B79DB07844E0 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.ScriptableObject>::get_Item(System.Int32)
inline ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_inline (List_1_tEB4537E121ED7128292F5E49486823EB846576FE * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * (*) (List_1_tEB4537E121ED7128292F5E49486823EB846576FE *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Boolean DevionGames.UIWidgets.UIWidget::get_IgnoreTimeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIWidget_get_IgnoreTimeScale_mF470882883F5E3AC0417210E5CC8E2F2D01FFECE (UIWidget_tE0E117D4D704EE21B3C18D7FB83D1830C9F77DBE * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84 (const RuntimeMethod* method);
// DevionGames.InventorySystem.Configuration.UI DevionGames.InventorySystem.InventoryManager::get_UI()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649 (const RuntimeMethod* method);
// DevionGames.UIWidgets.Tooltip DevionGames.InventorySystem.Configuration.UI::get_tooltip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * UI_get_tooltip_mFED99C380C15D406CD8D1B2C243D2FCF6AE4F46F (UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * __this, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Item::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Item_get_DisplayName_m4DE5829073A0A2EA30EA47CBD49B513704C12909 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// UnityEngine.Color DevionGames.InventorySystem.Rarity::get_Color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Rarity_get_Color_m9D433820F051AD1EEEFADCFE348BAADA21066420 (Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * __this, const RuntimeMethod* method);
// System.String DevionGames.UnityTools::ColorString(System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityTools_ColorString_m9335B52921AFB6D29680AA7475F4A538C520864B (String_t* ___value0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, const RuntimeMethod* method);
// System.String DevionGames.InventorySystem.Item::get_Description()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Item_get_Description_mB7F9BB622A01FD5720F1573FA9716698D415833F (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// UnityEngine.Sprite DevionGames.InventorySystem.Item::get_Icon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * Item_get_Icon_m5458CA78A43074CA889D686A51B3E8ADB864514C (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> DevionGames.InventorySystem.Item::GetPropertyInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62 * Item_GetPropertyInfo_m9375BAAD43D6C4B78059B15451509F462D127453 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Configuration.UI::get_sellPriceTooltip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347 (UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * __this, const RuntimeMethod* method);
// System.Boolean DevionGames.InventorySystem.Item::get_IsSellable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Item_get_IsSellable_m3DEAC715603E2BA6AEAD713758DFFD0FD577806B (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Int32 DevionGames.InventorySystem.Item::get_SellPrice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::get_SellCurrency()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * Item_get_SellCurrency_m52419C8985F5C719A86C5EB23928E6D0A6665421 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<DevionGames.InventorySystem.Currency>(!!0)
inline Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07 (Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___original0, const RuntimeMethod* method)
{
	return ((  Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * (*) (Currency_tC5AA8C667005127F940597BF06CF49D395F55308 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m4039C8E65629D33E1EC84D2505BF1D5DDC936622_gshared)(___original0, method);
}
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m1E6B4B8589A5ABDFCCF7D8419EA65EA392E063D2 (U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C (String_t* ___value0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.InventoryManager::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryManager_Load_m1A8274A328F990EB2E9D244FA357B5E5A093C7FD (String_t* ___key0, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.Projectile/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAE34D66987FBBB74F62935C8E358292FFD8E1453 (U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<DevionGames.SelectableObject>()
inline SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * Component_GetComponent_TisSelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_mEB6CBE76B608B9EDBA8E49E84569D09A444243F7 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void DevionGames.Sequence::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sequence_Start_m849C4AD223E00ED601AD87EC022D65D39110CD40 (Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * __this, const RuntimeMethod* method);
// System.Boolean DevionGames.Sequence::Tick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Sequence_Tick_m0EC5BA3677022D4D6CC8DAF2238DC32FB5E69429 (Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * __this, const RuntimeMethod* method);
// DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::get_BuyCurrency()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * Item_get_BuyCurrency_m2A371F5A4BBA33885093CC75E3B9984F97BC12AE (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Int32 DevionGames.InventorySystem.Item::get_BuyPrice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Item_get_BuyPrice_m46EB74805728696A53C379A637EEF269A45395B1 (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD (float ___f0, const RuntimeMethod* method);
// System.Single DevionGames.UIWidgets.Spinner::get_current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Spinner_get_current_mE89E80DE712D9BF4487556FF3709CC252FD603CE (Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * __this, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.VendorTrigger::BuyItem(DevionGames.InventorySystem.Item,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VendorTrigger_BuyItem_mBDE414BF84F09670EECB23C15BC94D0701288BC8 (VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, bool ___showDialog2, const RuntimeMethod* method);
// System.Void DevionGames.InventorySystem.VendorTrigger::SellItem(DevionGames.InventorySystem.Item,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VendorTrigger_SellItem_m7D80333E0630E8CEEE68ADD4A58B37294A26920F (VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, bool ___showDialog2, const RuntimeMethod* method);
// UnityEngine.Transform DevionGames.InventorySystem.EquipmentHandler::GetBone(DevionGames.InventorySystem.EquipmentRegion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * EquipmentHandler_GetBone_m510D1291A594A6CAF82F886525CE3E72E1E3731E (EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C * __this, EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * ___region0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared)(___original0, ___parent1, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<DevionGames.InventorySystem.Trigger>()
inline Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * GameObject_GetComponent_TisTrigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_m71F7EE5A1C76E4BCD5D1B61B9CD06CFDACC5E0FD (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponents<DevionGames.InventorySystem.IGenerator>()
inline IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* GameObject_GetComponents_TisIGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32_m8E4BF18040DAB58B180D5B23B7B72B23ACCB4C39 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponents_TisRuntimeObject_mAB26971A1F37F81EEEF20F7897AA6FAE3B33779E_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<DevionGames.InventorySystem.ItemCollection>()
inline ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * GameObject_GetComponent_TisItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_m6768E89F745979BABEE8FB850B0134DDF6A7EAB7 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * GameObject_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mA1779277BB07CE3D2CB8E340CEA85C40C6B52354 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// !!0[] UnityEngine.GameObject::GetComponents<UnityEngine.Collider>()
inline ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* GameObject_GetComponents_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_mB05DD18198B23D82EDD9CD9ED0055E90780B1215 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponents_TisRuntimeObject_mAB26971A1F37F81EEEF20F7897AA6FAE3B33779E_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB (const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass102_0__ctor_m108CEB315C1D77B0F72AA76D244842D6F1CF1C9F (U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__0_mBDAB476C9D67ECDE4AD3C6A3B2484ED417B31E31 (U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	{
		// items.AddRange(this.m_Collection.Where(x => x.Id == idOrName));
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		String_t* L_1;
		L_1 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_idOrName_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__1(DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__1_mED1368D649E7DED7D196F9813DF74093414C824F (U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA * __this, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___x0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// items.AddRange(this.m_Slots.Where(x => !x.IsEmpty && x.ObservedItem.Id == idOrName).Select(y => y.ObservedItem));
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_0 = ___x0;
		bool L_1;
		L_1 = Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_2 = ___x0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_3;
		L_3 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_3, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_idOrName_0();
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = 0;
	}

IL_0021:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__3(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__3_m729D5B7D3BF765F343BC8745A2DE0B934B66F53B (U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	{
		// items.AddRange(this.m_Collection.Where(x => x.Name == idOrName));
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_idOrName_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__4(DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__4_m9C9DF7F81EAD43AC830AC1C27D7C773DFDE31595 (U3CU3Ec__DisplayClass102_0_t0C7765DC3445ECCDB2A6B4A4286D424F3AF171FA * __this, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___x0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// items.AddRange(this.m_Slots.Where(x => !x.IsEmpty && x.ObservedItem.Name == idOrName).Select(y => y.ObservedItem));
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_0 = ___x0;
		bool L_1;
		L_1 = Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_2 = ___x0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_3;
		L_3 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_3, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_idOrName_0();
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0021;
	}

IL_0020:
	{
		G_B3_0 = 0;
	}

IL_0021:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass104_0__ctor_m2D4AB0C53651DC47779AFC73EF092FDEDF06F928 (U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::<GetItems>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__0_mF0841795DEE68EFFF6B94EAFDFFA23AE33DA1DBE (U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		// items.AddRange(this.m_Collection.Where(x => (!inherit && x.GetType() == type) || (inherit && type.IsAssignableFrom(x.GetType()))));
		bool L_0 = __this->get_inherit_0();
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_1 = ___x0;
		Type_t * L_2;
		L_2 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_1, /*hidden argument*/NULL);
		Type_t * L_3 = __this->get_type_1();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_2, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0039;
		}
	}

IL_001b:
	{
		bool L_5 = __this->get_inherit_0();
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		Type_t * L_6 = __this->get_type_1();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_7 = ___x0;
		Type_t * L_8;
		L_8 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_6, L_8);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_0037;
	}

IL_0036:
	{
		G_B5_0 = 0;
	}

IL_0037:
	{
		G_B7_0 = G_B5_0;
		goto IL_003a;
	}

IL_0039:
	{
		G_B7_0 = 1;
	}

IL_003a:
	{
		return (bool)G_B7_0;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::<GetItems>b__1(DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__1_m3820BF183A18195071D48E250E91998BE4C7AD9A (U3CU3Ec__DisplayClass104_0_t55FEAF2203352C983DA2F34B4AA482D1D216E1EC * __this, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B6_0 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	{
		// items.AddRange(this.m_Slots.Where(x => !x.IsEmpty && ((!inherit && x.ObservedItem.GetType() == type) || (inherit && type.IsAssignableFrom(x.ObservedItem.GetType())) )).Select(y => y.ObservedItem));
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_0 = ___x0;
		bool L_1;
		L_1 = Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_004e;
		}
	}
	{
		bool L_2 = __this->get_inherit_0();
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_3 = ___x0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_4;
		L_4 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_3, /*hidden argument*/NULL);
		Type_t * L_5;
		L_5 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_4, /*hidden argument*/NULL);
		Type_t * L_6 = __this->get_type_1();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_5, L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_004b;
		}
	}

IL_0028:
	{
		bool L_8 = __this->get_inherit_0();
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		Type_t * L_9 = __this->get_type_1();
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_10 = ___x0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_11;
		L_11 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_10, /*hidden argument*/NULL);
		Type_t * L_12;
		L_12 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_11, /*hidden argument*/NULL);
		bool L_13;
		L_13 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_9, L_12);
		G_B6_0 = ((int32_t)(L_13));
		goto IL_0049;
	}

IL_0048:
	{
		G_B6_0 = 0;
	}

IL_0049:
	{
		G_B8_0 = G_B6_0;
		goto IL_004c;
	}

IL_004b:
	{
		G_B8_0 = 1;
	}

IL_004c:
	{
		G_B10_0 = G_B8_0;
		goto IL_004f;
	}

IL_004e:
	{
		G_B10_0 = 0;
	}

IL_004f:
	{
		return (bool)G_B10_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass109_0__ctor_m09A2B3A8E3D9EC8DE701B5F4AC42ED39426AEC82 (U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0::<GetSlots>b__0(DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass109_0_U3CGetSlotsU3Eb__0_m1C807F6F95479080823BE5C6506FACA76128C795 (U3CU3Ec__DisplayClass109_0_t99E9219951A4C7FD629A642E9F54E1DAD990C90B * __this, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return this.m_Slots.Where(x => x.GetType() == type).ToArray(); ;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_0 = ___x0;
		Type_t * L_1;
		L_1 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_0, /*hidden argument*/NULL);
		Type_t * L_2 = __this->get_type_0();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Type_op_Equality_mA438719A1FDF103C7BBBB08AEF564E7FAEEA0046(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass113_0__ctor_mA019AC50A788A394195BFFB149D7BB01088E1740 (U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0::<CanStack>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass113_0_U3CCanStackU3Eb__0_m9067D6C95F15D9F823B907B14349BD398AE1B4AC (U3CU3Ec__DisplayClass113_0_t957CCDE644E84DCBBD2DD9398131B7BEC32F9F52 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		// Item[] items = this.m_Collection.Where(x => x != null && x.Id == item.Id && x.Rarity == item.Rarity).ToArray();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_2 = ___x0;
		String_t* L_3;
		L_3 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_2, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_4 = __this->get_item_0();
		String_t* L_5;
		L_5 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_4, /*hidden argument*/NULL);
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_7 = ___x0;
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_8;
		L_8 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_7, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_9 = __this->get_item_0();
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_10;
		L_10 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_8, L_10, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_003a;
	}

IL_0039:
	{
		G_B4_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B4_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass115_0__ctor_m05FA62D60598F38F92EB40836F86FD6706ED543D (U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0::<TryConvertCurrency>b__0(DevionGames.InventorySystem.Currency)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass115_0_U3CTryConvertCurrencyU3Eb__0_m3A812DE5CC1E70DBC85E164BCD0263D1A294D6A4 (U3CU3Ec__DisplayClass115_0_t3A417C040D21E0160BC9838D8C77C5806EC16B03 * __this, Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___x0, const RuntimeMethod* method)
{
	{
		// Currency converted = currencies.Where(x => x.Name == conversion.currency.Name).FirstOrDefault();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_0 = ___x0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		CurrencyConversion_t56D4374FBE1FF446D4F47FBA62C5015BCAFDB9D0 * L_2 = __this->get_conversion_0();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_3 = L_2->get_currency_1();
		String_t* L_4;
		L_4 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass80_0__ctor_mE8D7F64D411CBAA882074E7457CC70CC3FF971DC (U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0::<set_Collection>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass80_0_U3Cset_CollectionU3Eb__0_m6095F4B55A92EB379D2DFD8CEB923F8CFCC3BA36 (U3CU3Ec__DisplayClass80_0_tE1B38081E3B47A2BB855039CBF99909D28B3FD8A * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Currency_tC5AA8C667005127F940597BF06CF49D395F55308_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		// Currency currency = m_Collection.Where(x => typeof(Currency).IsAssignableFrom(x.GetType()) && x.Id == defaultCurrency.Id).FirstOrDefault() as Currency;
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (Currency_tC5AA8C667005127F940597BF06CF49D395F55308_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_2 = ___x0;
		Type_t * L_3;
		L_3 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = VirtFuncInvoker1< bool, Type_t * >::Invoke(111 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_1, L_3);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_5 = ___x0;
		String_t* L_6;
		L_6 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_5, /*hidden argument*/NULL);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_7 = __this->get_defaultCurrency_0();
		String_t* L_8;
		L_8 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_8, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_9));
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 0;
	}

IL_0030:
	{
		return (bool)G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass89_0__ctor_m1A71997BCF90B26DA38001C9925544D93A2D7B54 (U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0::<StackItem>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass89_0_U3CStackItemU3Eb__0_mF52A8D11865660EBD84951A4400D666A53868D5C (U3CU3Ec__DisplayClass89_0_t108C7F8F3D278378370E5DC36E80226A2EBF193A * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		// Item[] items = this.m_Collection.Where(x => x != null && x.Id == item.Id && x.Rarity == item.Rarity).ToArray();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_2 = ___x0;
		String_t* L_3;
		L_3 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_2, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_4 = __this->get_item_0();
		String_t* L_5;
		L_5 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_4, /*hidden argument*/NULL);
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_7 = ___x0;
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_8;
		L_8 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_7, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_9 = __this->get_item_0();
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_10;
		L_10 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_8, L_10, /*hidden argument*/NULL);
		G_B4_0 = ((int32_t)(L_11));
		goto IL_003a;
	}

IL_0039:
	{
		G_B4_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B4_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass96_0__ctor_m75FF4066883B13125127A4D0460B9A4C24E37DC0 (U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0::<RemoveItem>b__0(DevionGames.InventorySystem.CurrencySlot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass96_0_U3CRemoveItemU3Eb__0_m0A60574A1E1614D6FCACD6B1672E13141DBEE4FF (U3CU3Ec__DisplayClass96_0_t17A32C5F2BD4C21E4982ED478A39836351DE5F4B * __this, CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B * ___x0, const RuntimeMethod* method)
{
	{
		// CurrencySlot slot = GetSlots<CurrencySlot>().Where(x => x.ObservedItem.Id == payCurrency.Id).FirstOrDefault();
		CurrencySlot_t93445A3613021EEB17BA0D21F545546205D6861B * L_0 = ___x0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_1;
		L_1 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_1, /*hidden argument*/NULL);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_3 = __this->get_payCurrency_0();
		String_t* L_4;
		L_4 = Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddItemDelegate__ctor_mF2268FBBCFA478A58C45517C1834CF8741BEA035 (AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::Invoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddItemDelegate_Invoke_m17AC8582C1C6B83700F3A8565DF5E2429D2C5639 (AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___slot1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___slot1);
					else
						GenericVirtActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___slot1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0, ___slot1);
					else
						VirtActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0, ___slot1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___slot1);
					else
						GenericVirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___slot1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0, ___slot1);
					else
						VirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0, ___slot1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___slot1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/AddItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AddItemDelegate_BeginInvoke_m15DEB3D486173B0D743BAC50AFE5259C303D6D9D (AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___item0;
	__d_args[1] = ___slot1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddItemDelegate_EndInvoke_m226FB3D152835C80CC2FDEA27756D804500C4BCF (AddItemDelegate_t87D8932BA2756F33B9C6AAC289453CA56953260D * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropItemDelegate__ctor_m8D123DDC38F650FAABEF63459C433CB1401643E2 (DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::Invoke(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropItemDelegate_Invoke_m38B05498F02933153863260A9B367FC55FDCB8CA (DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___droppedInstance1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___droppedInstance1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___droppedInstance1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(targetMethod, ___item0, ___droppedInstance1);
					else
						GenericVirtActionInvoker1< GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(targetMethod, ___item0, ___droppedInstance1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0, ___droppedInstance1);
					else
						VirtActionInvoker1< GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0, ___droppedInstance1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___droppedInstance1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(targetMethod, targetThis, ___item0, ___droppedInstance1);
					else
						GenericVirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(targetMethod, targetThis, ___item0, ___droppedInstance1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0, ___droppedInstance1);
					else
						VirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0, ___droppedInstance1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, ___droppedInstance1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___droppedInstance1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/DropItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,UnityEngine.GameObject,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DropItemDelegate_BeginInvoke_m4F122299CACE8F277D03AA67BBBE857FB8E99A34 (DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___droppedInstance1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___item0;
	__d_args[1] = ___droppedInstance1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropItemDelegate_EndInvoke_m79D54AB9FDDEA9E80C3461AD87A54B6B88949E36 (DropItemDelegate_t4461465CF0D0064F8BE58545916C7DE2759D831D * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToAddItemDelegate__ctor_mD9E18790126B624D7F7A9BB7CCDF3CB30802F599 (FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::Invoke(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToAddItemDelegate_Invoke_m94F697D87B83FC1B3E34A8B5A692CFDA52A21861 (FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___item0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___item0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(targetMethod, targetThis, ___item0);
					else
						GenericVirtActionInvoker1< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(targetMethod, targetThis, ___item0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0);
					else
						VirtActionInvoker1< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FailedToAddItemDelegate_BeginInvoke_m7194A1E8ADF1D6099EF4E5BA25259CF8A9A60100 (FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___item0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToAddItemDelegate_EndInvoke_mD384609F467F1242B69BBACFAE1C181D44189548 (FailedToAddItemDelegate_t976037A25C7D83C535F2B7F142B5BBC4FEDCBFDC * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToRemoveItemDelegate__ctor_m622BB21F6F97E0EA79643BAB94171FAE3C2CDBAD (FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::Invoke(DevionGames.InventorySystem.Item,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToRemoveItemDelegate_Invoke_m8619D6D8F748159DF1414F9564F2E8B356C109F1 (FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___amount1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< int32_t >::Invoke(targetMethod, ___item0, ___amount1);
					else
						GenericVirtActionInvoker1< int32_t >::Invoke(targetMethod, ___item0, ___amount1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0, ___amount1);
					else
						VirtActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0, ___amount1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t >::Invoke(targetMethod, targetThis, ___item0, ___amount1);
					else
						GenericVirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t >::Invoke(targetMethod, targetThis, ___item0, ___amount1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0, ___amount1);
					else
						VirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0, ___amount1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___amount1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.Int32,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FailedToRemoveItemDelegate_BeginInvoke_mE77F6610C7253ED04B62D97D35B2FABBF393BF55 (FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___item0;
	__d_args[1] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___amount1);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FailedToRemoveItemDelegate_EndInvoke_m8AB2E1F3632FC60922E844770AF1B575AFAD6D6A (FailedToRemoveItemDelegate_t9229B009FAE5F58D27AE60F20D9F228D223FB397 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/MoveItemCondition::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MoveItemCondition__ctor_mE201579B85E5F4CE2808AFBAD2E1E2005BE8B13F (MoveItemCondition_tC1339BB8BB4F28B301FB7B505F06EB4D130263B6 * __this, const RuntimeMethod* method)
{
	{
		// public bool requiresVisibility = true;
		__this->set_requiresVisibility_1((bool)1);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RemoveItemDelegate__ctor_m2905BD9846BC2731CF6996336F0CE07888995A69 (RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::Invoke(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RemoveItemDelegate_Invoke_m2ECEC5C5550F7D87FA9A91773D0C2D9A3FDE7954 (RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot2, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, ___slot2, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___amount1, ___slot2, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___amount1, ___slot2);
					else
						GenericVirtActionInvoker2< int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___amount1, ___slot2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0, ___amount1, ___slot2);
					else
						VirtActionInvoker2< int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0, ___amount1, ___slot2);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, ___slot2, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker3< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___amount1, ___slot2);
					else
						GenericVirtActionInvoker3< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___amount1, ___slot2);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker3< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0, ___amount1, ___slot2);
					else
						VirtActionInvoker3< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0, ___amount1, ___slot2);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, ___amount1, ___slot2, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, int32_t, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___amount1, ___slot2, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* RemoveItemDelegate_BeginInvoke_m322D3DDC0FA1B4549A78FAFB7CA152F0B5B4F534 (RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot2, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___item0;
	__d_args[1] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___amount1);
	__d_args[2] = ___slot2;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RemoveItemDelegate_EndInvoke_m4A9B4D2CAD1806CFBFE7BC4345BE2024751E10CE (RemoveItemDelegate_t46A8D5B3703CA20190A816A5225211238627C029 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UseItemDelegate__ctor_m93D0DA58F472A45F5917457C5BC91A668B9B0B0F (UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::Invoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UseItemDelegate_Invoke_m6407B74DD0985B6249634E9E9FD907673466838A (UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot1, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___slot1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___slot1);
					else
						GenericVirtActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, ___item0, ___slot1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___item0, ___slot1);
					else
						VirtActionInvoker1< Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___item0, ___slot1);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___slot1);
					else
						GenericVirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(targetMethod, targetThis, ___item0, ___slot1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___item0, ___slot1);
					else
						VirtActionInvoker2< Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___item0, ___slot1);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___item0, ___slot1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 *, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___item0, ___slot1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult DevionGames.InventorySystem.ItemContainer/UseItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UseItemDelegate_BeginInvoke_m591AF4B80D624E5768332949B7ADF353785FEA5B (UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot1, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___item0;
	__d_args[1] = ___slot1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);;
}
// System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UseItemDelegate_EndInvoke_m3B26098D85EA5622BA629929E36B1ACC0542CA32 (UseItemDelegate_t71E73E2E77111F03C16F54E16B60B1F65F56BC1B * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ItemContainerCallbackTester/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE15630DCF665B958BBC4902FB3FA3F5F7A9D2FA3 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * L_0 = (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 *)il2cpp_codegen_object_new(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m933F59E5DF1ED926E1DAC9D75D0E33959989E6A0(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void ItemContainerCallbackTester/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m933F59E5DF1ED926E1DAC9D75D0E33959989E6A0 (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_0(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__0_0_mDC226B2EF59870C30E06385F95F55CDDF6BE44FC (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6751E43009A5D229BF6DFB516C94CAD79085EEFD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE128C797A08DBCB47C70B7C8D835C48499DA55B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// Debug.Log("[" + Time.time + "]" + "OnRemoveItem: " + item.Name + " Amount: " + amount + " Container: " + slot.Container.Name + " Slot: " + slot.Index);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		ArrayElementTypeCheck (L_1, _stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		float L_3;
		L_3 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4;
		L_4 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_2;
		ArrayElementTypeCheck (L_5, _stringLiteral6751E43009A5D229BF6DFB516C94CAD79085EEFD);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral6751E43009A5D229BF6DFB516C94CAD79085EEFD);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_7 = ___item0;
		String_t* L_8;
		L_8 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_7, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_6;
		ArrayElementTypeCheck (L_9, _stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		String_t* L_11;
		L_11 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___amount1), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)L_11);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12 = L_10;
		ArrayElementTypeCheck (L_12, _stringLiteralAE128C797A08DBCB47C70B7C8D835C48499DA55B);
		(L_12)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralAE128C797A08DBCB47C70B7C8D835C48499DA55B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = L_12;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_14 = ___slot2;
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_15;
		L_15 = Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D(L_14, /*hidden argument*/NULL);
		String_t* L_16;
		L_16 = UIWidget_get_Name_mE16241740E8F042B088BD63CA5D11C7F90EBF90B(L_15, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (String_t*)L_16);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17 = L_13;
		ArrayElementTypeCheck (L_17, _stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_18 = L_17;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_19 = ___slot2;
		int32_t L_20;
		L_20 = Slot_get_Index_mC035A81C33D92B432421E15673C429D96FEB405D(L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		String_t* L_21;
		L_21 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)L_21);
		String_t* L_22;
		L_22 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_22, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_1(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__0_1_m708DB13EFC48E9873C0F0AC74270CFE8E3E0E59D (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9B89EEF4B61C471E75E330C2A4B3D6F43C4E1D80);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE11FFF0DDA0500DDDD1C0EA92C63BAE06CA56A5B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		// Debug.Log("[" + Time.time + "]" + "OnAddItem: " + item.Name + " Amount: " + item.Stack + "Container: " + slot.Container.Name + " Slot: " + slot.Index);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_1 = L_0;
		ArrayElementTypeCheck (L_1, _stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralD9691C4FD8A1F6B09DB1147CA32B442772FB46A1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_2 = L_1;
		float L_3;
		L_3 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4;
		L_4 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_2, L_4);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_4);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_5 = L_2;
		ArrayElementTypeCheck (L_5, _stringLiteral9B89EEF4B61C471E75E330C2A4B3D6F43C4E1D80);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral9B89EEF4B61C471E75E330C2A4B3D6F43C4E1D80);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_6 = L_5;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_7 = ___item0;
		String_t* L_8;
		L_8 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_7, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_8);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_9 = L_6;
		ArrayElementTypeCheck (L_9, _stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_10 = L_9;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_11 = ___item0;
		int32_t L_12;
		L_12 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 DevionGames.InventorySystem.Item::get_Stack() */, L_11);
		V_1 = L_12;
		String_t* L_13;
		L_13 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)L_13);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14 = L_10;
		ArrayElementTypeCheck (L_14, _stringLiteralE11FFF0DDA0500DDDD1C0EA92C63BAE06CA56A5B);
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteralE11FFF0DDA0500DDDD1C0EA92C63BAE06CA56A5B);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_15 = L_14;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_16 = ___slot1;
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_17;
		L_17 = Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D(L_16, /*hidden argument*/NULL);
		String_t* L_18;
		L_18 = UIWidget_get_Name_mE16241740E8F042B088BD63CA5D11C7F90EBF90B(L_17, /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (String_t*)L_18);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = L_15;
		ArrayElementTypeCheck (L_19, _stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral9C43D53720165C5E51F384C8D4B3A86FEE56EBAE);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_20 = L_19;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_21 = ___slot1;
		int32_t L_22;
		L_22 = Slot_get_Index_mC035A81C33D92B432421E15673C429D96FEB405D(L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		String_t* L_23;
		L_23 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_1), /*hidden argument*/NULL);
		ArrayElementTypeCheck (L_20, L_23);
		(L_20)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)L_23);
		String_t* L_24;
		L_24 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_24, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_2(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__0_2_mE6978D97020C48DEC7277BBE683DE79E500F5E5E (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDCA36CD63E715AA87DDBAECD8BCC4400878520B1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("OnFailedToAddItem: " + item.Name);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___item0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralDCA36CD63E715AA87DDBAECD8BCC4400878520B1, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_2, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_3(DevionGames.InventorySystem.Item,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CAwakeU3Eb__0_3_m76E2A6AF9DFADC710C802AB13979BF7CEC5B7CC8 (U3CU3Ec_t257709D1AD4A1F790350A0D7DBC9EFD58E2385C4 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___item0, int32_t ___amount1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5FD3DCB5900804C3AA9796D311531F3F35786E5F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("OnFailedToRemoveItem: " + item.Name + " Amount: " + amount);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___item0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___amount1), /*hidden argument*/NULL);
		String_t* L_3;
		L_3 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral5FD3DCB5900804C3AA9796D311531F3F35786E5F, L_1, _stringLiteralED765622EB1DDA1E8F1E27279E98D09E50FEEA79, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_3, /*hidden argument*/NULL);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemContainerPopulator/Entry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Entry__ctor_m45CCEFBA047A44BA862051D9E41682E0CE7D5B9A (Entry_tEE8EA3A0B07EF66CD775D1748C94347B9778D068 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68B8DEBED212A277911E2A4827C5A0E30BF70D94);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string name = "Inventory";
		__this->set_name_0(_stringLiteral68B8DEBED212A277911E2A4827C5A0E30BF70D94);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_mDFA45F3813076EFE9C1C15C593F8EE23B9595E60 (U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0::<GetItemGroup>b__0(DevionGames.InventorySystem.ItemGroup)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass10_0_U3CGetItemGroupU3Eb__0_mD5336BEA23CBD87A0405598E9C5500320C41AA73 (U3CU3Ec__DisplayClass10_0_t884265069C93F87B376E8673D6B5FCB36E779940 * __this, ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * ___x0, const RuntimeMethod* method)
{
	{
		// return itemGroups.First(x => x.Name == name);
		ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * L_0 = ___x0;
		String_t* L_1;
		L_1 = ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_name_0();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_0__ctor_m02D9EA5414D3D7771C112C640679955B6FACEED3 (U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0::<Merge>b__6(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_0_U3CMergeU3Eb__6_m6E4398A632AF5281525451DA84FEB2FAC092D816 (U3CU3Ec__DisplayClass11_0_tEF36E93589FC9ED4F9F196F710486A659F429339 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___z0, const RuntimeMethod* method)
{
	{
		// items.AddRange(database.items.Where(y => !items.Any(z => z.Name == y.Name)));
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___z0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_1__ctor_m3CDDC3F336D033E80AE18380C8DB444B501E1FB0 (U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1::<Merge>b__7(DevionGames.InventorySystem.Currency)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_1_U3CMergeU3Eb__7_m4A06110E76F0AFDFDDF63DF70CD57758A8C5B936 (U3CU3Ec__DisplayClass11_1_t0A35475FB75E665681DCF3237A0887CE2054AF08 * __this, Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * ___z0, const RuntimeMethod* method)
{
	{
		// currencies.AddRange(database.currencies.Where(y => !currencies.Any(z => z.Name == y.Name)));
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_0 = ___z0;
		String_t* L_1;
		L_1 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_0, /*hidden argument*/NULL);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_2__ctor_mBEA76D40F3FB5FB4161CDCFDB17A34172AF7D7F0 (U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2::<Merge>b__8(DevionGames.InventorySystem.Rarity)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_2_U3CMergeU3Eb__8_m3BE2DAA1450D7210C235221F1DFE0F5CFB952854 (U3CU3Ec__DisplayClass11_2_tB6A61A0E401FB8FEEB397E903E8E80D051D7833E * __this, Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * ___z0, const RuntimeMethod* method)
{
	{
		// raritys.AddRange(database.raritys.Where(y => !raritys.Any(z => z.Name == y.Name)));
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_0 = ___z0;
		String_t* L_1;
		L_1 = Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21(L_0, /*hidden argument*/NULL);
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_3__ctor_m9C281F43BD87D90C7617E368A682AEABB8E7EF84 (U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3::<Merge>b__9(DevionGames.InventorySystem.Category)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_3_U3CMergeU3Eb__9_m03575315A10CFFD04FBFCFB9F7C6BD58EC3370CA (U3CU3Ec__DisplayClass11_3_t052A55DA4955B67AC9280E96025B81C458F29942 * __this, Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * ___z0, const RuntimeMethod* method)
{
	{
		// categories.AddRange(database.categories.Where(y => !categories.Any(z => z.Name == y.Name)));
		Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * L_0 = ___z0;
		String_t* L_1;
		L_1 = Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F(L_0, /*hidden argument*/NULL);
		Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_4__ctor_m7D20B898349551DFE3B704D9BF98064B834DF9C7 (U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4::<Merge>b__10(DevionGames.InventorySystem.EquipmentRegion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_4_U3CMergeU3Eb__10_m8F16DA14EA9489646CE196D106818187836C4533 (U3CU3Ec__DisplayClass11_4_t0022E4F49CCB714FDB091B684A33F1523290C9D9 * __this, EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * ___z0, const RuntimeMethod* method)
{
	{
		// equipments.AddRange(database.equipments.Where(y => !equipments.Any(z => z.Name == y.Name)));
		EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * L_0 = ___z0;
		String_t* L_1;
		L_1 = EquipmentRegion_get_Name_mE9156A7BE449EA2F9B3BDD27CCCDFE8741A6FC07(L_0, /*hidden argument*/NULL);
		EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = EquipmentRegion_get_Name_mE9156A7BE449EA2F9B3BDD27CCCDFE8741A6FC07(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass11_5__ctor_mF78EC76B71BF3B03892829A68B4478EB4CE85A77 (U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5::<Merge>b__11(DevionGames.InventorySystem.ItemGroup)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass11_5_U3CMergeU3Eb__11_m6FCD5A6F233BB10323DA645180E16C611E5D7A1C (U3CU3Ec__DisplayClass11_5_t7E9FDB562E5CE43C13965420094F38BE84A35482 * __this, ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * ___z0, const RuntimeMethod* method)
{
	{
		// itemGroups.AddRange(database.itemGroups.Where(y => !itemGroups.Any(z => z.Name == y.Name)));
		ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * L_0 = ___z0;
		String_t* L_1;
		L_1 = ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B(L_0, /*hidden argument*/NULL);
		ItemGroup_t1F3068F9FB5A19AF76954807A4EBC6005FB20B84 * L_2 = __this->get_y_0();
		String_t* L_3;
		L_3 = ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B(L_2, /*hidden argument*/NULL);
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_m6232303D57B0FECFF722519E957D0ACA81A52419 (U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::<GenerateItems>b__0(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__0_m61D18D2567B3EB8CF59726B611E5049FE8970034 (U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		// items = items.Where(x => x.Category != null &&( x.Category.Name == (this.m_Filters[i] as Category).Name)).ToList();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * L_1;
		L_1 = Item_get_Category_mA2301BF94DD3C9FCE88171937B00B79DB07844E0(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_3 = ___x0;
		Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C * L_4;
		L_4 = Item_get_Category_mA2301BF94DD3C9FCE88171937B00B79DB07844E0(L_3, /*hidden argument*/NULL);
		String_t* L_5;
		L_5 = Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F(L_4, /*hidden argument*/NULL);
		ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 * L_6 = __this->get_U3CU3E4__this_1();
		List_1_tEB4537E121ED7128292F5E49486823EB846576FE * L_7 = L_6->get_m_Filters_5();
		int32_t L_8 = __this->get_i_0();
		ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * L_9;
		L_9 = List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_inline(L_7, L_8, /*hidden argument*/List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_RuntimeMethod_var);
		String_t* L_10;
		L_10 = Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F(((Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C *)IsInstClass((RuntimeObject*)L_9, Category_t752DFDBD5C9A0970A882C39EFBA5DF91E159050C_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_11;
		L_11 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_5, L_10, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_11));
		goto IL_0041;
	}

IL_0040:
	{
		G_B3_0 = 0;
	}

IL_0041:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::<GenerateItems>b__1(DevionGames.InventorySystem.Item)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__1_m45DB1312E7BDEE24A58170DC0D53CEB1DCD4F791 (U3CU3Ec__DisplayClass9_0_t0FE42EBB97C83EB588EE927574E03693FCFDAF53 * __this, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// items = items.Where(x => x.Rarity.Name == (this.m_Filters[i] as Rarity).Name).ToList();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = ___x0;
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_1;
		L_1 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_0, /*hidden argument*/NULL);
		String_t* L_2;
		L_2 = Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21(L_1, /*hidden argument*/NULL);
		ItemGroupGenerator_tB5460AE737222FD406538EFC4C91DC3699961E80 * L_3 = __this->get_U3CU3E4__this_1();
		List_1_tEB4537E121ED7128292F5E49486823EB846576FE * L_4 = L_3->get_m_Filters_5();
		int32_t L_5 = __this->get_i_0();
		ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A * L_6;
		L_6 = List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_inline(L_4, L_5, /*hidden argument*/List_1_get_Item_m70E0BC04C0D22BDD88373E630BC5BE48A457C735_RuntimeMethod_var);
		String_t* L_7;
		L_7 = Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21(((Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F *)IsInstClass((RuntimeObject*)L_6, Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		bool L_8;
		L_8 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_2, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayTooltipU3Ed__23__ctor_mC3C9A902CAE14C3E35402B4ABC70E985052EC50A (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayTooltipU3Ed__23_System_IDisposable_Dispose_mDBAAF22BCE80C94EFF75167DACF2A5C68702F4B7 (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CDelayTooltipU3Ed__23_MoveNext_mEA8769B233EF0085F559D38AD27DD6F5980CE16C (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	float G_B10_0 = 0.0f;
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * G_B10_1 = NULL;
	float G_B9_0 = 0.0f;
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * G_B9_1 = NULL;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * G_B11_2 = NULL;
	int32_t G_B17_0 = 0;
	int32_t G_B22_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_001d;
			}
			case 2:
			{
				goto IL_001f;
			}
		}
	}
	{
		goto IL_0021;
	}

IL_001b:
	{
		goto IL_0023;
	}

IL_001d:
	{
		goto IL_004b;
	}

IL_001f:
	{
		goto IL_0095;
	}

IL_0021:
	{
		return (bool)0;
	}

IL_0023:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float time = 0.0f;
		__this->set_U3CtimeU3E5__1_4((0.0f));
		// yield return true;
		bool L_2 = ((bool)1);
		RuntimeObject * L_3 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_2);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_004b:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_009d;
	}

IL_0054:
	{
		// time += Container.IgnoreTimeScale?Time.unscaledDeltaTime: Time.deltaTime;
		float L_4 = __this->get_U3CtimeU3E5__1_4();
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_5 = __this->get_U3CU3E4__this_3();
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_6;
		L_6 = Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D(L_5, /*hidden argument*/NULL);
		bool L_7;
		L_7 = UIWidget_get_IgnoreTimeScale_mF470882883F5E3AC0417210E5CC8E2F2D01FFECE(L_6, /*hidden argument*/NULL);
		G_B9_0 = L_4;
		G_B9_1 = __this;
		if (L_7)
		{
			G_B10_0 = L_4;
			G_B10_1 = __this;
			goto IL_0075;
		}
	}
	{
		float L_8;
		L_8 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		G_B11_0 = L_8;
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		goto IL_007a;
	}

IL_0075:
	{
		float L_9;
		L_9 = Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84(/*hidden argument*/NULL);
		G_B11_0 = L_9;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
	}

IL_007a:
	{
		G_B11_2->set_U3CtimeU3E5__1_4(((float)il2cpp_codegen_add((float)G_B11_1, (float)G_B11_0)));
		// yield return true;
		bool L_10 = ((bool)1);
		RuntimeObject * L_11 = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &L_10);
		__this->set_U3CU3E2__current_1(L_11);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0095:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_009d:
	{
		// while (time < delay)
		float L_12 = __this->get_U3CtimeU3E5__1_4();
		float L_13 = __this->get_delay_2();
		V_1 = (bool)((((float)L_12) < ((float)L_13))? 1 : 0);
		bool L_14 = V_1;
		if (L_14)
		{
			goto IL_0054;
		}
	}
	{
		// if (InventoryManager.UI.tooltip != null && ObservedItem != null)
		IL2CPP_RUNTIME_CLASS_INIT(InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_15;
		L_15 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * L_16;
		L_16 = UI_get_tooltip_mFED99C380C15D406CD8D1B2C243D2FCF6AE4F46F(L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_17;
		L_17 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_16, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00d4;
		}
	}
	{
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_18 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_19;
		L_19 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_19, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		G_B17_0 = ((int32_t)(L_20));
		goto IL_00d5;
	}

IL_00d4:
	{
		G_B17_0 = 0;
	}

IL_00d5:
	{
		V_2 = (bool)G_B17_0;
		bool L_21 = V_2;
		if (!L_21)
		{
			goto IL_0211;
		}
	}
	{
		// InventoryManager.UI.tooltip.Show(UnityTools.ColorString(ObservedItem.DisplayName, ObservedItem.Rarity.Color), ObservedItem.Description, ObservedItem.Icon, ObservedItem.GetPropertyInfo());
		IL2CPP_RUNTIME_CLASS_INIT(InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_22;
		L_22 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		Tooltip_t84784622E964293493EBDE375C05FB4BD736F859 * L_23;
		L_23 = UI_get_tooltip_mFED99C380C15D406CD8D1B2C243D2FCF6AE4F46F(L_22, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_24 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_25;
		L_25 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_24, /*hidden argument*/NULL);
		String_t* L_26;
		L_26 = Item_get_DisplayName_m4DE5829073A0A2EA30EA47CBD49B513704C12909(L_25, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_27 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_28;
		L_28 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_27, /*hidden argument*/NULL);
		Rarity_t27C084AA4A4B83C0C354A5D14105F41305A6E76F * L_29;
		L_29 = Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6(L_28, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_30;
		L_30 = Rarity_get_Color_m9D433820F051AD1EEEFADCFE348BAADA21066420(L_29, /*hidden argument*/NULL);
		String_t* L_31;
		L_31 = UnityTools_ColorString_m9335B52921AFB6D29680AA7475F4A538C520864B(L_26, L_30, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_32 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_33;
		L_33 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_32, /*hidden argument*/NULL);
		String_t* L_34;
		L_34 = Item_get_Description_mB7F9BB622A01FD5720F1573FA9716698D415833F(L_33, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_35 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_36;
		L_36 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_35, /*hidden argument*/NULL);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_37;
		L_37 = Item_get_Icon_m5458CA78A43074CA889D686A51B3E8ADB864514C(L_36, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_38 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_39;
		L_39 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_38, /*hidden argument*/NULL);
		List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62 * L_40;
		L_40 = Item_GetPropertyInfo_m9375BAAD43D6C4B78059B15451509F462D127453(L_39, /*hidden argument*/NULL);
		VirtActionInvoker4< String_t*, String_t*, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *, List_1_t2E0A11B5F89515389093E72BC4B6064918E20E62 * >::Invoke(15 /* System.Void DevionGames.UIWidgets.Tooltip::Show(System.String,System.String,UnityEngine.Sprite,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>) */, L_23, L_31, L_34, L_37, L_40);
		// if (InventoryManager.UI.sellPriceTooltip != null && ObservedItem.IsSellable && ObservedItem.SellPrice > 0)
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_41;
		L_41 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_42;
		L_42 = UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347(L_41, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_43;
		L_43 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_42, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0180;
		}
	}
	{
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_44 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_45;
		L_45 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_44, /*hidden argument*/NULL);
		bool L_46;
		L_46 = Item_get_IsSellable_m3DEAC715603E2BA6AEAD713758DFFD0FD577806B(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0180;
		}
	}
	{
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_47 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_48;
		L_48 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_47, /*hidden argument*/NULL);
		int32_t L_49;
		L_49 = Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603(L_48, /*hidden argument*/NULL);
		G_B22_0 = ((((int32_t)L_49) > ((int32_t)0))? 1 : 0);
		goto IL_0181;
	}

IL_0180:
	{
		G_B22_0 = 0;
	}

IL_0181:
	{
		V_3 = (bool)G_B22_0;
		bool L_50 = V_3;
		if (!L_50)
		{
			goto IL_0210;
		}
	}
	{
		// InventoryManager.UI.sellPriceTooltip.RemoveItems();
		IL2CPP_RUNTIME_CLASS_INIT(InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_51;
		L_51 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_52;
		L_52 = UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347(L_51, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(24 /* System.Void DevionGames.InventorySystem.ItemContainer::RemoveItems(System.Boolean) */, L_52, (bool)0);
		// Currency currency = Instantiate(ObservedItem.SellCurrency);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_53 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_54;
		L_54 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_53, /*hidden argument*/NULL);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_55;
		L_55 = Item_get_SellCurrency_m52419C8985F5C719A86C5EB23928E6D0A6665421(L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_56;
		L_56 = Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07(L_55, /*hidden argument*/Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		__this->set_U3CcurrencyU3E5__2_5(L_56);
		// currency.Stack = ObservedItem.SellPrice*ObservedItem.Stack;
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_57 = __this->get_U3CcurrencyU3E5__2_5();
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_58 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_59;
		L_59 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_58, /*hidden argument*/NULL);
		int32_t L_60;
		L_60 = Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603(L_59, /*hidden argument*/NULL);
		ItemSlot_tC950DB00EA25D0424715E0C8131A0C469BE3C2C1 * L_61 = __this->get_U3CU3E4__this_3();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_62;
		L_62 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_61, /*hidden argument*/NULL);
		int32_t L_63;
		L_63 = VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 DevionGames.InventorySystem.Item::get_Stack() */, L_62);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void DevionGames.InventorySystem.Item::set_Stack(System.Int32) */, L_57, ((int32_t)il2cpp_codegen_multiply((int32_t)L_60, (int32_t)L_63)));
		// InventoryManager.UI.sellPriceTooltip.StackOrAdd(currency);
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_64;
		L_64 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_65;
		L_65 = UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347(L_64, /*hidden argument*/NULL);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_66 = __this->get_U3CcurrencyU3E5__2_5();
		bool L_67;
		L_67 = VirtFuncInvoker1< bool, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(14 /* System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrAdd(DevionGames.InventorySystem.Item) */, L_65, L_66);
		// InventoryManager.UI.sellPriceTooltip.Show();
		UI_t1B8ECE331C03A9D55930FA32696F166F485B3DB1 * L_68;
		L_68 = InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649(/*hidden argument*/NULL);
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_69;
		L_69 = UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347(L_68, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(8 /* System.Void DevionGames.UIWidgets.UIWidget::Show() */, L_69);
		__this->set_U3CcurrencyU3E5__2_5((Currency_tC5AA8C667005127F940597BF06CF49D395F55308 *)NULL);
	}

IL_0210:
	{
	}

IL_0211:
	{
		// }
		return (bool)0;
	}
}
// System.Object DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayTooltipU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE227150E52923041CF13D04F059B3D1DF70AFA12 (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40 (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40_RuntimeMethod_var)));
	}
}
// System.Object DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_get_Current_m646B7B909036413EDA2A5337E19D2265E39CCFE0 (U3CDelayTooltipU3Ed__23_t07638390B03CF50B017E20986BCA2C0DBB4ACF48 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.ItemSlot/DragObject::.ctor(DevionGames.InventorySystem.Slot)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DragObject__ctor_m53544633AA81943EDC3A6B5202B1EC8D01E2EDD9 (DragObject_tA8683B9D647B8A92BBE1C385C395DDD0B51F0C82 * __this, Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * ___slot0, const RuntimeMethod* method)
{
	{
		// public DragObject(Slot slot) {
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.slot = slot;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_0 = ___slot0;
		__this->set_slot_1(L_0);
		// this.container = slot.Container;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_1 = ___slot0;
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_2;
		L_2 = Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D(L_1, /*hidden argument*/NULL);
		__this->set_container_0(L_2);
		// this.item = slot.ObservedItem;
		Slot_tA8B0E44E393863CD3062AC2A00C662583D84DF97 * L_3 = ___slot0;
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_4;
		L_4 = Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438(L_3, /*hidden argument*/NULL);
		__this->set_item_2(L_4);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mD28322611AA857CE92686FB0AF29DFC61C834539 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * L_0 = (U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE *)il2cpp_codegen_object_new(U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m1E6B4B8589A5ABDFCCF7D8419EA65EA392E063D2(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m1E6B4B8589A5ABDFCCF7D8419EA65EA392E063D2 (U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.LoadSaveMenu/<>c::<UpdateLoadingStates>b__3_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CUpdateLoadingStatesU3Eb__3_0_mA9313CB46D208854AC5BA7F4708B2461D8D5F765 (U3CU3Ec_tAA0890F4F5A4EB925878DDD1F8FCEE6A01E929AE * __this, String_t* ___x0, const RuntimeMethod* method)
{
	{
		// keys.RemoveAll(x => string.IsNullOrEmpty(x));
		String_t* L_0 = ___x0;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m9AFBB5335B441B94E884B8A9D4A27AD60E3D7F7C(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0__ctor_m3A2560E9B5E817FC27123B8E0810600A18A27DBF (U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0::<UpdateLoadingStates>b__1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass3_0_U3CUpdateLoadingStatesU3Eb__1_mA3F22CDB8B8853D8F30E121C17E1D9340137F869 (U3CU3Ec__DisplayClass3_0_tEDF8AA44FCAF7C42317238E2A299C9DF5F68AAB6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// button.onClick.AddListener(()=>{InventoryManager.Load(key); });
		String_t* L_0 = __this->get_key_0();
		IL2CPP_RUNTIME_CLASS_INIT(InventoryManager_tCCAEED8EDFCECFC8135F0B43170F54429A007616_il2cpp_TypeInfo_var);
		InventoryManager_Load_m1A8274A328F990EB2E9D244FA357B5E5A093C7FD(L_0, /*hidden argument*/NULL);
		// button.onClick.AddListener(()=>{InventoryManager.Load(key); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.Projectile/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m6690FDA4E8AD1A72FDEB11F4442DBE7D8A3F0A18 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * L_0 = (U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E *)il2cpp_codegen_object_new(U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mAE34D66987FBBB74F62935C8E358292FFD8E1453(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void DevionGames.InventorySystem.Projectile/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mAE34D66987FBBB74F62935C8E358292FFD8E1453 (U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.Projectile/<>c::<GetTarget>b__18_0(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CGetTargetU3Eb__18_0_m82749985C0FEC325E7953876524E65224292FB8D (U3CU3Ec_tDE2C5AFF5A0C97F8AC5D02B8E68080F47624C87E * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_mEB6CBE76B608B9EDBA8E49E84569D09A444243F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Collider[] selectables = colliders.Where(x => x.GetComponent<SelectableObject>() != null).ToArray();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___x0;
		SelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3 * L_1;
		L_1 = Component_GetComponent_TisSelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_mEB6CBE76B608B9EDBA8E49E84569D09A444243F7(L_0, /*hidden argument*/Component_GetComponent_TisSelectableObject_t29EA3BFF0068A135CC8255CEB3EB5D0DE5E13BB3_mEB6CBE76B608B9EDBA8E49E84569D09A444243F7_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSequenceCoroutineU3Ed__9__ctor_mD0D6AB8E654C1550E7FCD108F6B70B84E7BB1B05 (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSequenceCoroutineU3Ed__9_System_IDisposable_Dispose_m12DBD516F68B381F1311AF75F52DC4079F86D56C (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSequenceCoroutineU3Ed__9_MoveNext_m242BC4E9F62F2AA66CBD209A64F38ACC8BECC5A5 (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		goto IL_0016;
	}

IL_0012:
	{
		goto IL_0018;
	}

IL_0014:
	{
		goto IL_0044;
	}

IL_0016:
	{
		return (bool)0;
	}

IL_0018:
	{
		__this->set_U3CU3E1__state_0((-1));
		// this.m_ActionSequence.Start();
		UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB * L_3 = __this->get_U3CU3E4__this_2();
		Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * L_4 = L_3->get_m_ActionSequence_41();
		Sequence_Start_m849C4AD223E00ED601AD87EC022D65D39110CD40(L_4, /*hidden argument*/NULL);
		goto IL_004c;
	}

IL_0033:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0044:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_004c:
	{
		// while (this.m_ActionSequence.Tick()) {
		UsableItem_tE049F2C53B3660BDCB8049E39CD1D6290078AEEB * L_5 = __this->get_U3CU3E4__this_2();
		Sequence_tD24D8E473A20349E8062F4A6CBA9D1B2C5501CC5 * L_6 = L_5->get_m_ActionSequence_41();
		bool L_7;
		L_7 = Sequence_Tick_m0EC5BA3677022D4D6CC8DAF2238DC32FB5E69429(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		bool L_8 = V_1;
		if (L_8)
		{
			goto IL_0033;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSequenceCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m887024614CCE999B4A10D64456BC90F39F930864 (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863 (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863_RuntimeMethod_var)));
	}
}
// System.Object DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_m2DC9FF56B6AA56E737B6002C8AE95781725548AD (U3CSequenceCoroutineU3Ed__9_t4FABDC9B720B4F74CA0B665928C348AA923F9348 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass29_0__ctor_m69D42811E74A45807862D31B1B2A830C3766D91E (U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::<BuyItem>b__0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__0_m3E4E578EE14C745E917B0945F8A99A4816F03BEC (U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * V_0 = NULL;
	{
		// Currency price = Instantiate(item.BuyCurrency);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = __this->get_item_0();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_1;
		L_1 = Item_get_BuyCurrency_m2A371F5A4BBA33885093CC75E3B9984F97BC12AE(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_2;
		L_2 = Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07(L_1, /*hidden argument*/Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		V_0 = L_2;
		// price.Stack = Mathf.RoundToInt(this.m_BuyPriceFactor * item.BuyPrice * value);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_3 = V_0;
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_4 = __this->get_U3CU3E4__this_1();
		float L_5 = L_4->get_m_BuyPriceFactor_23();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_6 = __this->get_item_0();
		int32_t L_7;
		L_7 = Item_get_BuyPrice_m46EB74805728696A53C379A637EEF269A45395B1(L_6, /*hidden argument*/NULL);
		float L_8 = ___value0;
		int32_t L_9;
		L_9 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_5, (float)((float)((float)L_7)))), (float)L_8)), /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void DevionGames.InventorySystem.Item::set_Stack(System.Int32) */, L_3, L_9);
		// this.m_PriceInfo.RemoveItems();
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_10 = __this->get_U3CU3E4__this_1();
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_11 = L_10->get_m_PriceInfo_39();
		VirtActionInvoker1< bool >::Invoke(24 /* System.Void DevionGames.InventorySystem.ItemContainer::RemoveItems(System.Boolean) */, L_11, (bool)0);
		// this.m_PriceInfo.StackOrAdd(price);
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_12 = __this->get_U3CU3E4__this_1();
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_13 = L_12->get_m_PriceInfo_39();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_14 = V_0;
		bool L_15;
		L_15 = VirtFuncInvoker1< bool, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(14 /* System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrAdd(DevionGames.InventorySystem.Item) */, L_13, L_14);
		// });
		return;
	}
}
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::<BuyItem>b__1(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__1_m067E57B859F20A200EA30FF708AAC00D845880E2 (U3CU3Ec__DisplayClass29_0_t42965A75D7C7D246A17413B5520745BD013DA063 * __this, int32_t ___result0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (result == 0){
		int32_t L_0 = ___result0;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		// BuyItem(item, Mathf.RoundToInt(this.m_AmountSpinner.current), false);
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_2 = __this->get_U3CU3E4__this_1();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_3 = __this->get_item_0();
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_4 = __this->get_U3CU3E4__this_1();
		Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * L_5 = L_4->get_m_AmountSpinner_38();
		float L_6;
		L_6 = Spinner_get_current_mE89E80DE712D9BF4487556FF3709CC252FD603CE(L_5, /*hidden argument*/NULL);
		int32_t L_7;
		L_7 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(L_6, /*hidden argument*/NULL);
		VendorTrigger_BuyItem_mBDE414BF84F09670EECB23C15BC94D0701288BC8(L_2, L_3, L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }, this.m_BuyDialogButton, "Cancel");
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0__ctor_m1ECEAC40554536940ACB202A3E0E8C68A8ECED44 (U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::<SellItem>b__0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__0_mFB6B707D8C5596F790B68146050394B343394D1D (U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * V_0 = NULL;
	{
		// Currency price = Instantiate(item.SellCurrency);
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_0 = __this->get_item_0();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_1;
		L_1 = Item_get_SellCurrency_m52419C8985F5C719A86C5EB23928E6D0A6665421(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_2;
		L_2 = Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07(L_1, /*hidden argument*/Object_Instantiate_TisCurrency_tC5AA8C667005127F940597BF06CF49D395F55308_mCC29FA85624D98A4ECB27BF2379D984EDD70FE07_RuntimeMethod_var);
		V_0 = L_2;
		// price.Stack = Mathf.RoundToInt(this.m_SellPriceFactor * item.SellPrice * value);
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_3 = V_0;
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_4 = __this->get_U3CU3E4__this_1();
		float L_5 = L_4->get_m_SellPriceFactor_24();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_6 = __this->get_item_0();
		int32_t L_7;
		L_7 = Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603(L_6, /*hidden argument*/NULL);
		float L_8 = ___value0;
		int32_t L_9;
		L_9 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_5, (float)((float)((float)L_7)))), (float)L_8)), /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void DevionGames.InventorySystem.Item::set_Stack(System.Int32) */, L_3, L_9);
		// this.m_PriceInfo.RemoveItems();
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_10 = __this->get_U3CU3E4__this_1();
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_11 = L_10->get_m_PriceInfo_39();
		VirtActionInvoker1< bool >::Invoke(24 /* System.Void DevionGames.InventorySystem.ItemContainer::RemoveItems(System.Boolean) */, L_11, (bool)0);
		// this.m_PriceInfo.StackOrAdd(price);
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_12 = __this->get_U3CU3E4__this_1();
		ItemContainer_tC175C0901538B686331F32293B143845C5EE7FE5 * L_13 = L_12->get_m_PriceInfo_39();
		Currency_tC5AA8C667005127F940597BF06CF49D395F55308 * L_14 = V_0;
		bool L_15;
		L_15 = VirtFuncInvoker1< bool, Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * >::Invoke(14 /* System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrAdd(DevionGames.InventorySystem.Item) */, L_13, L_14);
		// });
		return;
	}
}
// System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::<SellItem>b__1(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__1_m890270DE06975B621F379787F02D1F12DFE6C206 (U3CU3Ec__DisplayClass30_0_tAF77B6EFE7479742BE540791B4EC9DD147FB2E9D * __this, int32_t ___result0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (result == 0)
		int32_t L_0 = ___result0;
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		// SellItem(item, Mathf.RoundToInt(this.m_AmountSpinner.current), false);
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_2 = __this->get_U3CU3E4__this_1();
		Item_tF63972D038D2EA801C4060C79BEDC2D1B16ACFA5 * L_3 = __this->get_item_0();
		VendorTrigger_tE55721F20F1BC76CC7698DAFA0D29C74DD31653B * L_4 = __this->get_U3CU3E4__this_1();
		Spinner_tA9317947BF61B90F945DF5E5FBB48035CB6117DE * L_5 = L_4->get_m_AmountSpinner_38();
		float L_6;
		L_6 = Spinner_get_current_mE89E80DE712D9BF4487556FF3709CC252FD603CE(L_5, /*hidden argument*/NULL);
		int32_t L_7;
		L_7 = Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD(L_6, /*hidden argument*/NULL);
		VendorTrigger_SellItem_m7D80333E0630E8CEEE68ADD4A58B37294A26920F(L_2, L_3, L_7, (bool)0, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }, this.m_SellDialogButton, "Cancel");
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.GameObject DevionGames.InventorySystem.VisibleItem/Attachment::Instantiate(DevionGames.InventorySystem.EquipmentHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Attachment_Instantiate_m7D9EE00E6517589518F548E0A0883EB44F0D25F9 (Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D * __this, EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C * ___handler0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_m6768E89F745979BABEE8FB850B0134DDF6A7EAB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mA1779277BB07CE3D2CB8E340CEA85C40C6B52354_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisTrigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_m71F7EE5A1C76E4BCD5D1B61B9CD06CFDACC5E0FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponents_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_mB05DD18198B23D82EDD9CD9ED0055E90780B1215_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponents_TisIGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32_m8E4BF18040DAB58B180D5B23B7B72B23ACCB4C39_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * V_0 = NULL;
	IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* V_1 = NULL;
	ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * V_2 = NULL;
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * V_3 = NULL;
	ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* V_4 = NULL;
	bool V_5 = false;
	int32_t V_6 = 0;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	int32_t V_10 = 0;
	bool V_11 = false;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_12 = NULL;
	{
		// gameObject = GameObject.Instantiate(prefab, handler.GetBone(region));
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_prefab_1();
		EquipmentHandler_t550D1EFFF1CD1203C1090E9396C4A15FF68BB43C * L_1 = ___handler0;
		EquipmentRegion_t5FBCACA0D8DCFF214258D92C3A955359248C0BD7 * L_2 = __this->get_region_0();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = EquipmentHandler_GetBone_m510D1291A594A6CAF82F886525CE3E72E1E3731E(L_1, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8(L_0, L_3, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
		__this->set_gameObject_5(L_4);
		// gameObject.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_gameObject_5();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// Trigger trigger = gameObject.GetComponent<Trigger>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_gameObject_5();
		Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * L_7;
		L_7 = GameObject_GetComponent_TisTrigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_m71F7EE5A1C76E4BCD5D1B61B9CD06CFDACC5E0FD(L_6, /*hidden argument*/GameObject_GetComponent_TisTrigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723_m71F7EE5A1C76E4BCD5D1B61B9CD06CFDACC5E0FD_RuntimeMethod_var);
		V_0 = L_7;
		// if (trigger != null) {
		Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * L_8 = V_0;
		bool L_9;
		L_9 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_8, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_5 = L_9;
		bool L_10 = V_5;
		if (!L_10)
		{
			goto IL_004d;
		}
	}
	{
		// Destroy(trigger);
		Trigger_tBA181BEF3B0A0ED1036F269763BF35A3A3388723 * L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_11, /*hidden argument*/NULL);
	}

IL_004d:
	{
		// IGenerator[] generators = gameObject.GetComponents<IGenerator>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_gameObject_5();
		IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* L_13;
		L_13 = GameObject_GetComponents_TisIGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32_m8E4BF18040DAB58B180D5B23B7B72B23ACCB4C39(L_12, /*hidden argument*/GameObject_GetComponents_TisIGenerator_t7192786E761966E71C83DB1E4EE9175929E96F32_m8E4BF18040DAB58B180D5B23B7B72B23ACCB4C39_RuntimeMethod_var);
		V_1 = L_13;
		// for (int i = 0; i < generators.Length; i++) {
		V_6 = 0;
		goto IL_0075;
	}

IL_005e:
	{
		// Destroy((generators[i] as Component));
		IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* L_14 = V_1;
		int32_t L_15 = V_6;
		int32_t L_16 = L_15;
		RuntimeObject* L_17 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(((Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *)IsInstClass((RuntimeObject*)L_17, Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		// for (int i = 0; i < generators.Length; i++) {
		int32_t L_18 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0075:
	{
		// for (int i = 0; i < generators.Length; i++) {
		int32_t L_19 = V_6;
		IGeneratorU5BU5D_t8E494A9C967EF0EE9B733698E6BE1A0639B74FE1* L_20 = V_1;
		V_7 = (bool)((((int32_t)L_19) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))? 1 : 0);
		bool L_21 = V_7;
		if (L_21)
		{
			goto IL_005e;
		}
	}
	{
		// ItemCollection collection = gameObject.GetComponent<ItemCollection>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_22 = __this->get_gameObject_5();
		ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * L_23;
		L_23 = GameObject_GetComponent_TisItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_m6768E89F745979BABEE8FB850B0134DDF6A7EAB7(L_22, /*hidden argument*/GameObject_GetComponent_TisItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803_m6768E89F745979BABEE8FB850B0134DDF6A7EAB7_RuntimeMethod_var);
		V_2 = L_23;
		// if (collection != null){
		ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * L_24 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_24, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_8 = L_25;
		bool L_26 = V_8;
		if (!L_26)
		{
			goto IL_00a4;
		}
	}
	{
		// Destroy(collection);
		ItemCollection_tDDA41B3AB5392E768B928B63AB1F8F9832118803 * L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_27, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		// Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_28 = __this->get_gameObject_5();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_29;
		L_29 = GameObject_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mA1779277BB07CE3D2CB8E340CEA85C40C6B52354(L_28, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_mA1779277BB07CE3D2CB8E340CEA85C40C6B52354_RuntimeMethod_var);
		V_3 = L_29;
		// if (rigidbody != null) {
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_30 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_31;
		L_31 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_30, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_9 = L_31;
		bool L_32 = V_9;
		if (!L_32)
		{
			goto IL_00c6;
		}
	}
	{
		// Destroy(rigidbody);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_33 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_33, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		// Collider[] colliders = gameObject.GetComponents<Collider>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34 = __this->get_gameObject_5();
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_35;
		L_35 = GameObject_GetComponents_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_mB05DD18198B23D82EDD9CD9ED0055E90780B1215(L_34, /*hidden argument*/GameObject_GetComponents_TisCollider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_mB05DD18198B23D82EDD9CD9ED0055E90780B1215_RuntimeMethod_var);
		V_4 = L_35;
		// for (int i = 0; i < colliders.Length; i++) {
		V_10 = 0;
		goto IL_00eb;
	}

IL_00d8:
	{
		// Destroy(colliders[i]);
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_36 = V_4;
		int32_t L_37 = V_10;
		int32_t L_38 = L_37;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_39 = (L_36)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_38));
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_39, /*hidden argument*/NULL);
		// for (int i = 0; i < colliders.Length; i++) {
		int32_t L_40 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1));
	}

IL_00eb:
	{
		// for (int i = 0; i < colliders.Length; i++) {
		int32_t L_41 = V_10;
		ColliderU5BU5D_t5124940293343DB3D31DA41C593709905906E486* L_42 = V_4;
		V_11 = (bool)((((int32_t)L_41) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length)))))? 1 : 0);
		bool L_43 = V_11;
		if (L_43)
		{
			goto IL_00d8;
		}
	}
	{
		// gameObject.transform.localPosition = position;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_44 = __this->get_gameObject_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_45;
		L_45 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_44, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_46 = __this->get_position_2();
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_45, L_46, /*hidden argument*/NULL);
		// gameObject.transform.localEulerAngles = rotation;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_47 = __this->get_gameObject_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_48;
		L_48 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_47, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_49 = __this->get_rotation_3();
		Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B(L_48, L_49, /*hidden argument*/NULL);
		// gameObject.transform.localScale = scale;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_50 = __this->get_gameObject_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_51;
		L_51 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_50, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = __this->get_scale_4();
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_51, L_52, /*hidden argument*/NULL);
		// return gameObject;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_53 = __this->get_gameObject_5();
		V_12 = L_53;
		goto IL_0148;
	}

IL_0148:
	{
		// }
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_54 = V_12;
		return L_54;
	}
}
// System.Void DevionGames.InventorySystem.VisibleItem/Attachment::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Attachment__ctor_mD76902D6EE601C99BF3CF5292A7D6F332C26C2C0 (Attachment_t75D91711297F30BB523CB6541CEF934DEE47BF0D * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 scale = Vector3.one;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB(/*hidden argument*/NULL);
		__this->set_scale_4(L_0);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
