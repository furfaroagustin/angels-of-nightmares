﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 DevionGames.PlayerInfo DevionGames.BaseTrigger::get_PlayerInfo()
// 0x00000002 System.String[] DevionGames.BaseTrigger::get_Callbacks()
extern void BaseTrigger_get_Callbacks_mC64DB930EB107C8CF28A773C850FDEE5B5FC8C24 (void);
// 0x00000003 System.Boolean DevionGames.BaseTrigger::get_InRange()
extern void BaseTrigger_get_InRange_mD0C2197559CD4AA414D901BA116106CEEC562E4F (void);
// 0x00000004 System.Void DevionGames.BaseTrigger::set_InRange(System.Boolean)
extern void BaseTrigger_set_InRange_mC9F0D95111F03B57CF9C5524CC0F2E9256827394 (void);
// 0x00000005 System.Boolean DevionGames.BaseTrigger::get_InUse()
extern void BaseTrigger_get_InUse_mD360CDBE3AFC612823AD801438C887749D5EAA1D (void);
// 0x00000006 System.Void DevionGames.BaseTrigger::set_InUse(System.Boolean)
extern void BaseTrigger_set_InUse_m63BB8D722FE055D30819F4B475D724B4D38C887F (void);
// 0x00000007 System.Void DevionGames.BaseTrigger::Start()
extern void BaseTrigger_Start_m7D76B0F929650B8675034A72F0B717E8AE52A3BB (void);
// 0x00000008 System.Void DevionGames.BaseTrigger::OnDisable()
extern void BaseTrigger_OnDisable_mC3209EE9E8B06FED13EBBB3356E87856FFADA1B8 (void);
// 0x00000009 System.Void DevionGames.BaseTrigger::OnEnable()
extern void BaseTrigger_OnEnable_m7A001B497CA663AB7CBDA8D92D78904A79427123 (void);
// 0x0000000A System.Void DevionGames.BaseTrigger::Update()
extern void BaseTrigger_Update_mE0CDBCD464FA149DB766371C60E4BB8204CC80E8 (void);
// 0x0000000B System.Void DevionGames.BaseTrigger::OnDestroy()
extern void BaseTrigger_OnDestroy_mB7C3075B973687BD39781D9A5D4B7A39DEC8BF91 (void);
// 0x0000000C System.Void DevionGames.BaseTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void BaseTrigger_OnTriggerEnter_mA1FF21B8C8DFC6C3B2757F2A7FE0CCC289FEC13C (void);
// 0x0000000D System.Void DevionGames.BaseTrigger::OnTriggerExit(UnityEngine.Collider)
extern void BaseTrigger_OnTriggerExit_mAFC7150F8E615FC342411F68A08C50B4F8072A82 (void);
// 0x0000000E System.Void DevionGames.BaseTrigger::OnPointerTriggerClick(System.Int32)
extern void BaseTrigger_OnPointerTriggerClick_m97BEB9BA3FEBA31B675458EE47A196CA4C2EBA01 (void);
// 0x0000000F System.Boolean DevionGames.BaseTrigger::Use()
extern void BaseTrigger_Use_m62E0A8F63EB628107B894F7E53B9D175E77C559B (void);
// 0x00000010 System.Boolean DevionGames.BaseTrigger::CanUse()
extern void BaseTrigger_CanUse_m7AA6E83991A2011AE63C1B304E03F1CC635CF941 (void);
// 0x00000011 System.Void DevionGames.BaseTrigger::OnWentOutOfRange()
extern void BaseTrigger_OnWentOutOfRange_m261BB40395A49630782271D0097EFFB0A1230F0C (void);
// 0x00000012 System.Void DevionGames.BaseTrigger::NotifyWentOutOfRange()
extern void BaseTrigger_NotifyWentOutOfRange_mA20C61F657EE345AA4E758CC3C0A7FA93F844C20 (void);
// 0x00000013 System.Void DevionGames.BaseTrigger::OnCameInRange()
extern void BaseTrigger_OnCameInRange_m232CA4AC9A80FAE7A162E205396494CFF022A200 (void);
// 0x00000014 System.Void DevionGames.BaseTrigger::NotifyCameInRange()
extern void BaseTrigger_NotifyCameInRange_mCBBD0A19365BFEF9F00673A641BD1E7C66A7D600 (void);
// 0x00000015 System.Void DevionGames.BaseTrigger::OnTriggerUsed()
extern void BaseTrigger_OnTriggerUsed_m539790192971C9BFC658F5F2E351A3B8EECE4F31 (void);
// 0x00000016 System.Void DevionGames.BaseTrigger::NotifyUsed()
extern void BaseTrigger_NotifyUsed_mF072F076A0D4FD6D3B3EAEE53949D16CD0B01E06 (void);
// 0x00000017 System.Void DevionGames.BaseTrigger::OnTriggerUnUsed()
extern void BaseTrigger_OnTriggerUnUsed_mA6E2C856EE075B62F51C90C0CB966E098984302A (void);
// 0x00000018 System.Void DevionGames.BaseTrigger::NotifyUnUsed()
extern void BaseTrigger_NotifyUnUsed_m9CCADFD57AA1BB5F3ACA074AD6EE775E52173DEB (void);
// 0x00000019 System.Void DevionGames.BaseTrigger::DisplayInUse()
extern void BaseTrigger_DisplayInUse_m4FA8E7C3548136D790AA582F30BCA0D1E4DD02FA (void);
// 0x0000001A System.Void DevionGames.BaseTrigger::DisplayOutOfRange()
extern void BaseTrigger_DisplayOutOfRange_mD4B9361F6940053673ADA2CF2EED72244680BAD9 (void);
// 0x0000001B System.Void DevionGames.BaseTrigger::CreateTriggerCollider()
extern void BaseTrigger_CreateTriggerCollider_m2985FFABA906DFCB9DC99013A56704E7F958F90F (void);
// 0x0000001C System.Boolean DevionGames.BaseTrigger::IsBestTrigger()
extern void BaseTrigger_IsBestTrigger_m31638B732D8AA18CE4E8164105282C9BBF7A7AC4 (void);
// 0x0000001D System.Void DevionGames.BaseTrigger::Execute(DevionGames.ITriggerUsedHandler,UnityEngine.GameObject)
extern void BaseTrigger_Execute_mB03D63B14E1B71596F4EFA10B472DAA767585E42 (void);
// 0x0000001E System.Void DevionGames.BaseTrigger::Execute(DevionGames.ITriggerUnUsedHandler,UnityEngine.GameObject)
extern void BaseTrigger_Execute_mB66465591A390A7BE60A54E3BD1A639C41A390C3 (void);
// 0x0000001F System.Void DevionGames.BaseTrigger::Execute(DevionGames.ITriggerCameInRange,UnityEngine.GameObject)
extern void BaseTrigger_Execute_mC05568B86956BC181B29BF100926C14B1053D161 (void);
// 0x00000020 System.Void DevionGames.BaseTrigger::Execute(DevionGames.ITriggerWentOutOfRange,UnityEngine.GameObject)
extern void BaseTrigger_Execute_mDDCE56B5DB318ECD5DF34405B212F0650D42363B (void);
// 0x00000021 System.Void DevionGames.BaseTrigger::ExecuteEvent(DevionGames.BaseTrigger/EventFunction`1<T>,System.Boolean)
// 0x00000022 System.Void DevionGames.BaseTrigger::ExecuteEvent(DevionGames.BaseTrigger/PointerEventFunction`1<T>,UnityEngine.EventSystems.PointerEventData,System.Boolean)
// 0x00000023 System.Boolean DevionGames.BaseTrigger::ShouldSendEvent(DevionGames.ITriggerEventHandler,System.Boolean)
// 0x00000024 System.Void DevionGames.BaseTrigger::RegisterCallbacks()
extern void BaseTrigger_RegisterCallbacks_m93A3889C762B81BCF512A8BA41B6C7D27743046B (void);
// 0x00000025 System.Void DevionGames.BaseTrigger::.ctor()
extern void BaseTrigger__ctor_mE5E2AA708111BC2ABCEC59F84EEC3D2969DC9D07 (void);
// 0x00000026 System.Void DevionGames.BaseTrigger::.cctor()
extern void BaseTrigger__cctor_m913D770449084248393DB2F5A8DDAC1AD6E5FD6A (void);
// 0x00000027 System.Void DevionGames.BaseTrigger/EventFunction`1::.ctor(System.Object,System.IntPtr)
// 0x00000028 System.Void DevionGames.BaseTrigger/EventFunction`1::Invoke(T,UnityEngine.GameObject)
// 0x00000029 System.IAsyncResult DevionGames.BaseTrigger/EventFunction`1::BeginInvoke(T,UnityEngine.GameObject,System.AsyncCallback,System.Object)
// 0x0000002A System.Void DevionGames.BaseTrigger/EventFunction`1::EndInvoke(System.IAsyncResult)
// 0x0000002B System.Void DevionGames.BaseTrigger/PointerEventFunction`1::.ctor(System.Object,System.IntPtr)
// 0x0000002C System.Void DevionGames.BaseTrigger/PointerEventFunction`1::Invoke(T,UnityEngine.EventSystems.PointerEventData)
// 0x0000002D System.IAsyncResult DevionGames.BaseTrigger/PointerEventFunction`1::BeginInvoke(T,UnityEngine.EventSystems.PointerEventData,System.AsyncCallback,System.Object)
// 0x0000002E System.Void DevionGames.BaseTrigger/PointerEventFunction`1::EndInvoke(System.IAsyncResult)
// 0x0000002F System.Void DevionGames.ITriggerUsedHandler::OnTriggerUsed(UnityEngine.GameObject)
// 0x00000030 System.Void DevionGames.ITriggerUnUsedHandler::OnTriggerUnUsed(UnityEngine.GameObject)
// 0x00000031 System.Void DevionGames.ITriggerCameInRange::OnCameInRange(UnityEngine.GameObject)
// 0x00000032 System.Void DevionGames.ITriggerWentOutOfRange::OnWentOutOfRange(UnityEngine.GameObject)
// 0x00000033 System.Void DevionGames.ITriggerPointerEnter::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
// 0x00000034 System.Void DevionGames.ITriggerPointerExit::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
// 0x00000035 DevionGames.PlayerInfo DevionGames.BehaviorTrigger::get_PlayerInfo()
extern void BehaviorTrigger_get_PlayerInfo_mE34FF1915D61F354FC090711157F85B526CF39C2 (void);
// 0x00000036 System.Void DevionGames.BehaviorTrigger::Start()
extern void BehaviorTrigger_Start_m96A283387E9B71CC47956EE160AA8B8DA1753D5C (void);
// 0x00000037 System.Void DevionGames.BehaviorTrigger::Update()
extern void BehaviorTrigger_Update_m54A700916071A6052C59A3932DE07B1E096A18F5 (void);
// 0x00000038 System.Void DevionGames.BehaviorTrigger::OnDisable()
extern void BehaviorTrigger_OnDisable_m8B79F19AD41CCB8A1FE665E53970032273014654 (void);
// 0x00000039 System.Void DevionGames.BehaviorTrigger::OnDestroy()
extern void BehaviorTrigger_OnDestroy_m72F8908451E91A300A47E7D73359B4AA9BC410D0 (void);
// 0x0000003A System.Void DevionGames.BehaviorTrigger::NotifyInterrupted()
extern void BehaviorTrigger_NotifyInterrupted_m9D122309121EDE50AA5E3193C819832429F27022 (void);
// 0x0000003B System.Void DevionGames.BehaviorTrigger::OnTriggerInterrupted()
extern void BehaviorTrigger_OnTriggerInterrupted_mDE8B0CD53D7F5AB225C298B3528999CED04C12FE (void);
// 0x0000003C System.Void DevionGames.BehaviorTrigger::OnTriggerUsed()
extern void BehaviorTrigger_OnTriggerUsed_mC998E2EAC00D77C1549A1B6F958FE27AB7EC30A3 (void);
// 0x0000003D System.Void DevionGames.BehaviorTrigger::OnTriggerUnUsed()
extern void BehaviorTrigger_OnTriggerUnUsed_mE4F5D4A958C8605D6DF94C7D9407879B97241F3D (void);
// 0x0000003E System.Boolean DevionGames.BehaviorTrigger::Use()
extern void BehaviorTrigger_Use_mB7731D131894E725370EFB82069E2FE67E8F02B3 (void);
// 0x0000003F System.Void DevionGames.BehaviorTrigger::CacheAnimatorStates()
extern void BehaviorTrigger_CacheAnimatorStates_m2720947D242ADEE1D0523EB00138481E8C6618AC (void);
// 0x00000040 System.Void DevionGames.BehaviorTrigger::LoadCachedAnimatorStates()
extern void BehaviorTrigger_LoadCachedAnimatorStates_m9FD82763FBC1482D6A4D63CF6CED9DCDF4C9D552 (void);
// 0x00000041 System.Void DevionGames.BehaviorTrigger::.ctor()
extern void BehaviorTrigger__ctor_m29D4C9C6463C70E92ED88B8DBA6AFEB339A42D2E (void);
// 0x00000042 System.Void DevionGames.BehaviorTrigger/<>c::.cctor()
extern void U3CU3Ec__cctor_m8052C0027353DFE1B8A17B0A9D71ED93726D70B1 (void);
// 0x00000043 System.Void DevionGames.BehaviorTrigger/<>c::.ctor()
extern void U3CU3Ec__ctor_m6C1B7E0D3904C207D4E8BDF7002C0A2B193BE746 (void);
// 0x00000044 System.Boolean DevionGames.BehaviorTrigger/<>c::<Start>b__8_0(DevionGames.Action)
extern void U3CU3Ec_U3CStartU3Eb__8_0_m18BD8B51C635110F7FB4467865D2E8803F61A0B7 (void);
// 0x00000045 System.Boolean DevionGames.Action::get_enabled()
extern void Action_get_enabled_m3E685F616574AD92346C8B3DF2DACC0ADF5B039A (void);
// 0x00000046 System.Void DevionGames.Action::set_enabled(System.Boolean)
extern void Action_set_enabled_mDC0492849F3495BA212693C56C99A6C7860B4593 (void);
// 0x00000047 System.Boolean DevionGames.Action::get_isActiveAndEnabled()
extern void Action_get_isActiveAndEnabled_m2C3B004316FA34965062F4070ECF041521277701 (void);
// 0x00000048 System.Void DevionGames.Action::.ctor()
extern void Action__ctor_m7119657D6D723178E7BD4CDA51A9BD485D7D07D3 (void);
// 0x00000049 System.Void DevionGames.Action::Initialize(UnityEngine.GameObject,DevionGames.PlayerInfo,DevionGames.Blackboard)
extern void Action_Initialize_m4773A734FABBDC9455F05A5F63CE44A7F4E122AD (void);
// 0x0000004A DevionGames.ActionStatus DevionGames.Action::OnUpdate()
// 0x0000004B System.Void DevionGames.Action::Update()
extern void Action_Update_mB1CD4E4E3B22FBFAC9235DA278F0969727D17AB5 (void);
// 0x0000004C System.Void DevionGames.Action::OnStart()
extern void Action_OnStart_mD73399B0D148CF794CA3FDFDB6922A33956CC66B (void);
// 0x0000004D System.Void DevionGames.Action::OnEnd()
extern void Action_OnEnd_mEF505768D02E7A75E2F5126C164172A19ABC06F4 (void);
// 0x0000004E System.Void DevionGames.Action::OnSequenceStart()
extern void Action_OnSequenceStart_m620ECAACB80A3025A8CA7ED39B6F2755765F7EB3 (void);
// 0x0000004F System.Void DevionGames.Action::OnSequenceEnd()
extern void Action_OnSequenceEnd_m30CE37C3F44F3AE810A27E6E43F31A08B83A7603 (void);
// 0x00000050 System.Void DevionGames.Action::OnInterrupt()
extern void Action_OnInterrupt_m3709A86DF3A0478A8413E02DF2148C20B120776B (void);
// 0x00000051 UnityEngine.GameObject DevionGames.Action::GetTarget(DevionGames.TargetType)
extern void Action_GetTarget_m851A94DDC949456AADC16F46DEC3A18CF6E3AC58 (void);
// 0x00000052 System.Void DevionGames.ActionTemplate::.ctor()
extern void ActionTemplate__ctor_mC5BEFD46C90E52FD3E9E9E96014F9FF498B630E4 (void);
// 0x00000053 System.Void DevionGames.Actions::.ctor()
extern void Actions__ctor_m00BFB49A689897CEF81A41F31B670E43ED47C6BE (void);
// 0x00000054 System.Void DevionGames.ApplyRootMotion::OnStart()
extern void ApplyRootMotion_OnStart_m7D9A7FF0D95E17CED6DED2623AECD518FEC546E5 (void);
// 0x00000055 DevionGames.ActionStatus DevionGames.ApplyRootMotion::OnUpdate()
extern void ApplyRootMotion_OnUpdate_mE6DEBD8EAD396123C3AB8EE32B2698BD813AFDEC (void);
// 0x00000056 System.Void DevionGames.ApplyRootMotion::.ctor()
extern void ApplyRootMotion__ctor_m800ACCFDCE0DC8D56066D4DA7B2432E4E4C2CD78 (void);
// 0x00000057 System.Void DevionGames.CrossFade::OnStart()
extern void CrossFade_OnStart_mAB6693AA5BBBF5C774667CF5F6223EB537D05F91 (void);
// 0x00000058 DevionGames.ActionStatus DevionGames.CrossFade::OnUpdate()
extern void CrossFade_OnUpdate_m0777A9663FB55DA5BE262270DF2CB3D03D057BC4 (void);
// 0x00000059 System.Void DevionGames.CrossFade::.ctor()
extern void CrossFade__ctor_mDBCB791C23B9115C3D76EEBC54D3D016BE891B51 (void);
// 0x0000005A System.Void DevionGames.IsInTransition::OnStart()
extern void IsInTransition_OnStart_m0EC807F1DB2F6B09D93D2DDE2EE1697C696CB8D2 (void);
// 0x0000005B DevionGames.ActionStatus DevionGames.IsInTransition::OnUpdate()
extern void IsInTransition_OnUpdate_m32B5E3941DE60039707EF3AEDBE70DA416E3D427 (void);
// 0x0000005C System.Void DevionGames.IsInTransition::.ctor()
extern void IsInTransition__ctor_mAE7ADDDEA3BAD3E7F20BB22A1872D9DD884B74D5 (void);
// 0x0000005D System.Void DevionGames.IsName::OnStart()
extern void IsName_OnStart_mB3477C9B00DBC7B21ECF56C00725DF5BA3B17118 (void);
// 0x0000005E DevionGames.ActionStatus DevionGames.IsName::OnUpdate()
extern void IsName_OnUpdate_mD975D367F993145B9D8E676268D2645F7CC01D83 (void);
// 0x0000005F System.Void DevionGames.IsName::.ctor()
extern void IsName__ctor_m9A19E2172ECADFC58218DCAB1770411FA3E28BBE (void);
// 0x00000060 System.Void DevionGames.SetBool::OnStart()
extern void SetBool_OnStart_m82BDD0856A3C4B9060DF3FF37A34B5A624279672 (void);
// 0x00000061 DevionGames.ActionStatus DevionGames.SetBool::OnUpdate()
extern void SetBool_OnUpdate_mA6FADBB071A5B5000066ABBB7230397CA291C245 (void);
// 0x00000062 System.Void DevionGames.SetBool::.ctor()
extern void SetBool__ctor_mAAFB065606C9D13028BEEDB60D6C5F569247158B (void);
// 0x00000063 System.Void DevionGames.SetFloat::OnSequenceStart()
extern void SetFloat_OnSequenceStart_m52784B6B30D99E53A1BA6A96607832CAA63CAD9D (void);
// 0x00000064 DevionGames.ActionStatus DevionGames.SetFloat::OnUpdate()
extern void SetFloat_OnUpdate_m0187B1C0B41F1A693C54AFD18370321654F9F988 (void);
// 0x00000065 System.Void DevionGames.SetFloat::Update()
extern void SetFloat_Update_m1360C28930AFE3D0F2A962075857BCD60DFC2924 (void);
// 0x00000066 System.Void DevionGames.SetFloat::.ctor()
extern void SetFloat__ctor_m4B5FF44DEE645941CC35E5BCBA7F959D62FFFD5A (void);
// 0x00000067 System.Void DevionGames.SetInt::OnStart()
extern void SetInt_OnStart_m1B3C520B95A099AF91F8C608346FA15DD0EA9D1A (void);
// 0x00000068 DevionGames.ActionStatus DevionGames.SetInt::OnUpdate()
extern void SetInt_OnUpdate_m89AE4A2FE14E618A3431979B397441D46477FDEE (void);
// 0x00000069 System.Void DevionGames.SetInt::.ctor()
extern void SetInt__ctor_m5D00E8A68C4B8F0F35B0B965035CDC9A4FEDD747 (void);
// 0x0000006A System.Void DevionGames.SetRandomFloat::OnStart()
extern void SetRandomFloat_OnStart_m4FD413ECB73CBB3ED06CAB660D6540CC505AC78A (void);
// 0x0000006B DevionGames.ActionStatus DevionGames.SetRandomFloat::OnUpdate()
extern void SetRandomFloat_OnUpdate_m646D5B912DDB71B16009115BCF6B11E5B052E05E (void);
// 0x0000006C System.Void DevionGames.SetRandomFloat::Update()
extern void SetRandomFloat_Update_m1569A69218044E92BA9CC484A13CAFD9CA17D690 (void);
// 0x0000006D System.Void DevionGames.SetRandomFloat::.ctor()
extern void SetRandomFloat__ctor_mA17BB56C8E61E66F77D74083116638DC9CB40A70 (void);
// 0x0000006E System.Void DevionGames.SetTrigger::OnStart()
extern void SetTrigger_OnStart_mD980D2452AB84D2B3C676BFF20104ADA81822C42 (void);
// 0x0000006F DevionGames.ActionStatus DevionGames.SetTrigger::OnUpdate()
extern void SetTrigger_OnUpdate_m1FAA595EC9449096409543F0EDB55C951E5339AC (void);
// 0x00000070 System.Void DevionGames.SetTrigger::.ctor()
extern void SetTrigger__ctor_mD0C7A14024DA06FCB73CA74D74B1B07EC78EB537 (void);
// 0x00000071 DevionGames.ActionStatus DevionGames.Play::OnUpdate()
extern void Play_OnUpdate_mEAED35577B941330792402C146118D39FEB5261D (void);
// 0x00000072 System.Void DevionGames.Play::.ctor()
extern void Play__ctor_m2EB9071A249324B98FDDF76203465C43AAA57616 (void);
// 0x00000073 DevionGames.ActionStatus DevionGames.DeleteVariable::OnUpdate()
extern void DeleteVariable_OnUpdate_m7868C1C871F2F2A32D3ED2468D387D0E6B40361C (void);
// 0x00000074 System.Void DevionGames.DeleteVariable::.ctor()
extern void DeleteVariable__ctor_m1B5275222D87174A1D2EA7F5293F73CBA4A79E38 (void);
// 0x00000075 System.Void DevionGames.InvokeWithVariable::OnStart()
extern void InvokeWithVariable_OnStart_m52BDB5E25A4612D82BC92DA03445DF8225F28D04 (void);
// 0x00000076 DevionGames.ActionStatus DevionGames.InvokeWithVariable::OnUpdate()
extern void InvokeWithVariable_OnUpdate_m1D34AB41452642162629370F9B66DB57175A650C (void);
// 0x00000077 System.Void DevionGames.InvokeWithVariable::.ctor()
extern void InvokeWithVariable__ctor_m50C1FA175699BA5B1DB4335D9DCA52244798C180 (void);
// 0x00000078 DevionGames.ActionStatus DevionGames.SetBoolVariable::OnUpdate()
extern void SetBoolVariable_OnUpdate_mAB50E1FACF37EB4B50D49FBE569BF668648E7B10 (void);
// 0x00000079 System.Void DevionGames.SetBoolVariable::.ctor()
extern void SetBoolVariable__ctor_mAE6A1C462FDBA890EE234121CE750045FB4B4995 (void);
// 0x0000007A DevionGames.ActionStatus DevionGames.SetFloatVariable::OnUpdate()
extern void SetFloatVariable_OnUpdate_m37442F9C791118F975C3D2360B200923C89D14C9 (void);
// 0x0000007B System.Void DevionGames.SetFloatVariable::Update()
extern void SetFloatVariable_Update_mBE5BB44EEFEA6B256F787636F3CB44E87E2B24FF (void);
// 0x0000007C System.Void DevionGames.SetFloatVariable::.ctor()
extern void SetFloatVariable__ctor_mB3A04C0C86107E4273C0A04694CD90180C42B97B (void);
// 0x0000007D System.Void DevionGames.CompareNumber::OnStart()
extern void CompareNumber_OnStart_m393C676BCE77AFBD3443F902C33DB67BE7CB405E (void);
// 0x0000007E DevionGames.ActionStatus DevionGames.CompareNumber::OnUpdate()
extern void CompareNumber_OnUpdate_m3332284829A50A0229A754AEDD4309A69EC5C0AC (void);
// 0x0000007F System.Void DevionGames.CompareNumber::.ctor()
extern void CompareNumber__ctor_m31E0999E6D1207AB7B0BD7C49BB3B880C09C1CBE (void);
// 0x00000080 System.Void DevionGames.Invoke::OnStart()
extern void Invoke_OnStart_mD3BF777E342589A89E3FA81BBDB94734E6F6FA8E (void);
// 0x00000081 DevionGames.ActionStatus DevionGames.Invoke::OnUpdate()
extern void Invoke_OnUpdate_m893585F86A072290A343E2E4A786FA16CC7F1AE6 (void);
// 0x00000082 System.Void DevionGames.Invoke::.ctor()
extern void Invoke__ctor_mD9B020526D8CB0099DA7023AFAE47ABE68276CAF (void);
// 0x00000083 System.Void DevionGames.SetEnabled::OnStart()
extern void SetEnabled_OnStart_m5E39798C0392DC9F7BE5F4970D53523675276793 (void);
// 0x00000084 DevionGames.ActionStatus DevionGames.SetEnabled::OnUpdate()
extern void SetEnabled_OnUpdate_mBADE2C408E6B59ABA543D907426DC219DA5AD089 (void);
// 0x00000085 System.Void DevionGames.SetEnabled::OnInterrupt()
extern void SetEnabled_OnInterrupt_m62F571455ECB3346CD0646847E094B55043C9F95 (void);
// 0x00000086 System.Void DevionGames.SetEnabled::.ctor()
extern void SetEnabled__ctor_m3ACB5E0F4369FFEACC05D7B6ECB8B92739586894 (void);
// 0x00000087 DevionGames.ActionStatus DevionGames.Log::OnUpdate()
extern void Log_OnUpdate_mAA2A6183AE1F760F8A3DD2811D510D19A5120A99 (void);
// 0x00000088 System.Void DevionGames.Log::.ctor()
extern void Log__ctor_mDACC5F7C20FCF0949B184CEF298A15C2769299EA (void);
// 0x00000089 System.Void DevionGames.BroadcastMessage::OnStart()
extern void BroadcastMessage_OnStart_m92D627BD09407A2BA1EE208C6292C9333BB162E3 (void);
// 0x0000008A DevionGames.ActionStatus DevionGames.BroadcastMessage::OnUpdate()
extern void BroadcastMessage_OnUpdate_m87FE8505473862B47088B2FD577C77248EF69436 (void);
// 0x0000008B System.Void DevionGames.BroadcastMessage::.ctor()
extern void BroadcastMessage__ctor_mCBC1909E7855ACD79FE48B6589F0188CDC018097 (void);
// 0x0000008C DevionGames.ActionStatus DevionGames.CompareTag::OnUpdate()
extern void CompareTag_OnUpdate_mCDF649EC7D09E3F18AF22C3070C291588E855D7B (void);
// 0x0000008D System.Void DevionGames.CompareTag::.ctor()
extern void CompareTag__ctor_m5AE4402D3B2F131546B66EFEB2AE5E0F1166A219 (void);
// 0x0000008E DevionGames.ActionStatus DevionGames.Destroy::OnUpdate()
extern void Destroy_OnUpdate_mA7F981477BC92602F101369729BCA6E7D80E132D (void);
// 0x0000008F System.Void DevionGames.Destroy::.ctor()
extern void Destroy__ctor_m13E428566C2B023B06CB193B5482D587F0522570 (void);
// 0x00000090 System.Void DevionGames.Instantiate::OnStart()
extern void Instantiate_OnStart_mAAFD0B1D6DF9C83E328C9CAFE2914F7A1B4639CF (void);
// 0x00000091 DevionGames.ActionStatus DevionGames.Instantiate::OnUpdate()
extern void Instantiate_OnUpdate_m6287DF7420B6EDCC4B4B65F17EAD31AA81DC3BBB (void);
// 0x00000092 UnityEngine.Transform DevionGames.Instantiate::FindBone(UnityEngine.Transform,System.String)
extern void Instantiate_FindBone_m7280E568549B5685D2BA9D3785B3F91A9F429F72 (void);
// 0x00000093 System.Void DevionGames.Instantiate::.ctor()
extern void Instantiate__ctor_mF2FB020437A2879E14C59D14177FC13FAE2CF9A5 (void);
// 0x00000094 DevionGames.ActionStatus DevionGames.InstantiateAtMouse::OnUpdate()
extern void InstantiateAtMouse_OnUpdate_m8F4BDECD92EEDD1F0B4DE88275D3D73B67D14A5B (void);
// 0x00000095 System.Void DevionGames.InstantiateAtMouse::.ctor()
extern void InstantiateAtMouse__ctor_m37AFCEEA15F99C8DF551B751FC46040FE8A0D5B1 (void);
// 0x00000096 System.Void DevionGames.SendMessage::OnStart()
extern void SendMessage_OnStart_m37BD25D4A5648329D30610730A0398AEF38BC941 (void);
// 0x00000097 DevionGames.ActionStatus DevionGames.SendMessage::OnUpdate()
extern void SendMessage_OnUpdate_mE662F7C0839088E5026F1A305825822FBD654EED (void);
// 0x00000098 System.Void DevionGames.SendMessage::.ctor()
extern void SendMessage__ctor_mAC8D23083DD4ED95D60D8A09B7E90338090F11C7 (void);
// 0x00000099 System.Void DevionGames.SendMessageUpwards::OnStart()
extern void SendMessageUpwards_OnStart_m8EA1AD784BEBFD8F8BE3FE8D7F769063D2295851 (void);
// 0x0000009A DevionGames.ActionStatus DevionGames.SendMessageUpwards::OnUpdate()
extern void SendMessageUpwards_OnUpdate_mCFDF6553301B2D9C618796689C0B19D1BFAF1CB8 (void);
// 0x0000009B System.Void DevionGames.SendMessageUpwards::.ctor()
extern void SendMessageUpwards__ctor_m91D8EE08B279A049821C79B9DCE12D5A935FAD86 (void);
// 0x0000009C DevionGames.ActionStatus DevionGames.SetName::OnUpdate()
extern void SetName_OnUpdate_m2993B35BE38BC47782EC03411EF117858EC0B9F6 (void);
// 0x0000009D System.Void DevionGames.SetName::.ctor()
extern void SetName__ctor_m1B9EE7B2FBCD7AF20719F555AD79A50CF2CE4D6D (void);
// 0x0000009E System.Void DevionGames.OverlapSphere::OnStart()
extern void OverlapSphere_OnStart_m83E0CB9A1C05863396DA5B59DB55FEC651839E81 (void);
// 0x0000009F DevionGames.ActionStatus DevionGames.OverlapSphere::OnUpdate()
extern void OverlapSphere_OnUpdate_mFF523BD988DC2701F8EBEC21917B38E256651BA4 (void);
// 0x000000A0 System.Void DevionGames.OverlapSphere::.ctor()
extern void OverlapSphere__ctor_m0D1E8ECAC792244B5CAFA6C4604E1F158F2DBAC5 (void);
// 0x000000A1 UnityEngine.Vector3 DevionGames.PhysicsUtility::GetDirection(UnityEngine.Transform,DevionGames.Direction)
extern void PhysicsUtility_GetDirection_mBD2913B5CDCC25C3769D25BD7ECA458D6C903AC1 (void);
// 0x000000A2 System.Void DevionGames.Raycast::OnStart()
extern void Raycast_OnStart_m1DEE292098130404306E662DE09895C76D4FEDD0 (void);
// 0x000000A3 DevionGames.ActionStatus DevionGames.Raycast::OnUpdate()
extern void Raycast_OnUpdate_mE6B89D2B28D97889487674A39B597A19A84D0B07 (void);
// 0x000000A4 System.Boolean DevionGames.Raycast::DoRaycast()
extern void Raycast_DoRaycast_m7759B992D986A7C137297083920A1B5F96E8AFC3 (void);
// 0x000000A5 System.Void DevionGames.Raycast::.ctor()
extern void Raycast__ctor_m1238E1DA46419AB7322AC5F186126630CE919426 (void);
// 0x000000A6 System.Void DevionGames.SphereCast::OnStart()
extern void SphereCast_OnStart_m9B7C623646129ADE45911C089A0A4C1819EC0BC6 (void);
// 0x000000A7 DevionGames.ActionStatus DevionGames.SphereCast::OnUpdate()
extern void SphereCast_OnUpdate_m1A363B7B69C665561E788F3E717C0C9F23FB78F1 (void);
// 0x000000A8 System.Void DevionGames.SphereCast::.ctor()
extern void SphereCast__ctor_m2D9E2EE7E9873423AF288B05D69577188712D2DE (void);
// 0x000000A9 System.Void DevionGames.SetConstraints::OnStart()
extern void SetConstraints_OnStart_mC13922B7BB7C582B606D0E65AB890AECDDFD2BEA (void);
// 0x000000AA DevionGames.ActionStatus DevionGames.SetConstraints::OnUpdate()
extern void SetConstraints_OnUpdate_m567F6559B3D4732DB6BFDC6DEA1D3FA3613F776E (void);
// 0x000000AB System.Void DevionGames.SetConstraints::OnInterrupt()
extern void SetConstraints_OnInterrupt_m17E776AFFC1AA60B4A328EB82F66B73C285F23EC (void);
// 0x000000AC System.Void DevionGames.SetConstraints::.ctor()
extern void SetConstraints__ctor_m576BAF262D673D7F8DF02B1E63A177DF3913BD3C (void);
// 0x000000AD DevionGames.ActionStatus DevionGames.LoadScene::OnUpdate()
extern void LoadScene_OnUpdate_mD59DE29E5660421C051E76F82ABB6664D9345A21 (void);
// 0x000000AE System.Void DevionGames.LoadScene::.ctor()
extern void LoadScene__ctor_m3907F6D99C0C2CFA9C326D7496EEC97FD0A9B070 (void);
// 0x000000AF System.Void DevionGames.LookAtMouse::OnStart()
extern void LookAtMouse_OnStart_mB1D2F84491B3FF86FD391BEF886AB14D724E3E78 (void);
// 0x000000B0 DevionGames.ActionStatus DevionGames.LookAtMouse::OnUpdate()
extern void LookAtMouse_OnUpdate_m49C8AA9B88C550F2048B0D4FFC3D3A03651BE4C6 (void);
// 0x000000B1 System.Void DevionGames.LookAtMouse::.ctor()
extern void LookAtMouse__ctor_mC764BFF90AB3BF097017915AA533EB44B345B4C6 (void);
// 0x000000B2 System.Void DevionGames.LookAtPosition::OnStart()
extern void LookAtPosition_OnStart_m4F256863D6DC188E5CEB96B2DA21773F5CAEB0B6 (void);
// 0x000000B3 DevionGames.ActionStatus DevionGames.LookAtPosition::OnUpdate()
extern void LookAtPosition_OnUpdate_mF841810B1F10D8E76850292B896A428A9D870CD4 (void);
// 0x000000B4 System.Void DevionGames.LookAtPosition::.ctor()
extern void LookAtPosition__ctor_m86CEAF5B1B915D75BACE12CB4A3B3684DF3993DB (void);
// 0x000000B5 System.Void DevionGames.LookAtTrigger::OnStart()
extern void LookAtTrigger_OnStart_m552897C9BA41DD1158A380D39594E36FAC57F6D4 (void);
// 0x000000B6 DevionGames.ActionStatus DevionGames.LookAtTrigger::OnUpdate()
extern void LookAtTrigger_OnUpdate_mC8884629868570751BB78E9DAD865B9C386C4F43 (void);
// 0x000000B7 System.Void DevionGames.LookAtTrigger::.ctor()
extern void LookAtTrigger__ctor_m0FFE8F8F4C3FFE096D861F7E5E278B635EB87AED (void);
// 0x000000B8 System.Void DevionGames.LookForward::OnStart()
extern void LookForward_OnStart_m0BFA7C18491AC2101736B18C020B03415EA37B0D (void);
// 0x000000B9 DevionGames.ActionStatus DevionGames.LookForward::OnUpdate()
extern void LookForward_OnUpdate_m56591D27723F8BDEC2442383746DBCE6DD01D247 (void);
// 0x000000BA System.Void DevionGames.LookForward::.ctor()
extern void LookForward__ctor_mEC5A8F3D192107CC079D5816404C39EBBD9F8520 (void);
// 0x000000BB System.Void DevionGames.MoveTowards::OnStart()
extern void MoveTowards_OnStart_mB268E77197D94B3DC6926F163738FD363E650D2B (void);
// 0x000000BC DevionGames.ActionStatus DevionGames.MoveTowards::OnUpdate()
extern void MoveTowards_OnUpdate_mB1306BD264FB43A44F9B22C746067F8717375971 (void);
// 0x000000BD System.Void DevionGames.MoveTowards::LookAtPosition(UnityEngine.Vector3)
extern void MoveTowards_LookAtPosition_m59AD764AA8C8F0786B7145A9864F64B23DD60EAB (void);
// 0x000000BE System.Void DevionGames.MoveTowards::.ctor()
extern void MoveTowards__ctor_m6655153964FABC9A2380B8823B633480612406ED (void);
// 0x000000BF System.Void DevionGames.SetPosition::OnStart()
extern void SetPosition_OnStart_m73E4A01019EACCC282AE8D7ED08191164E971049 (void);
// 0x000000C0 DevionGames.ActionStatus DevionGames.SetPosition::OnUpdate()
extern void SetPosition_OnUpdate_m7C67715CEC3FCCAC6D0B79E8583861A8FEA3DEAB (void);
// 0x000000C1 System.Void DevionGames.SetPosition::.ctor()
extern void SetPosition__ctor_m6E1AC390926612CCD39A59BA931F177CFE6C61E2 (void);
// 0x000000C2 System.Void DevionGames.SetPositionToTarget::OnStart()
extern void SetPositionToTarget_OnStart_m52AB477416B67464004DD15EE4F55F6814C72748 (void);
// 0x000000C3 DevionGames.ActionStatus DevionGames.SetPositionToTarget::OnUpdate()
extern void SetPositionToTarget_OnUpdate_mA3196CB40FBBA1734A50B2CB08045D18C598345F (void);
// 0x000000C4 UnityEngine.Transform DevionGames.SetPositionToTarget::GetClosest(UnityEngine.Transform,UnityEngine.Transform[])
extern void SetPositionToTarget_GetClosest_mA8DE8B3C8DDC0F72FD6BF8B3C9649B9E144B2DDF (void);
// 0x000000C5 System.Void DevionGames.SetPositionToTarget::.ctor()
extern void SetPositionToTarget__ctor_mC3ADDFEE55017CFAEA9374EF37FB5BA0F52DE32A (void);
// 0x000000C6 System.Void DevionGames.SetPositionToTarget/<>c::.cctor()
extern void U3CU3Ec__cctor_mBB80EF0D16482050C013176FD914F80A913AB69C (void);
// 0x000000C7 System.Void DevionGames.SetPositionToTarget/<>c::.ctor()
extern void U3CU3Ec__ctor_m5F753DC6569637BA4062BB2F0A8C240BCDE7967C (void);
// 0x000000C8 UnityEngine.Transform DevionGames.SetPositionToTarget/<>c::<OnUpdate>b__7_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3COnUpdateU3Eb__7_0_mA5B15D9D68E2D467BC8A580EAAA9E5768B4BCB31 (void);
// 0x000000C9 System.Void DevionGames.Wait::OnStart()
extern void Wait_OnStart_mEC87E7564BA22E481A4982D69775CF0A90E1CC06 (void);
// 0x000000CA DevionGames.ActionStatus DevionGames.Wait::OnUpdate()
extern void Wait_OnUpdate_mD86A2BFDC926204E2C73AC924807321F69E3EFE6 (void);
// 0x000000CB System.Void DevionGames.Wait::.ctor()
extern void Wait__ctor_m91FE16DBEAF758CC33904ACAFA7A3F6AE8F0A7DF (void);
// 0x000000CC System.Void DevionGames.IAction::Initialize(UnityEngine.GameObject,DevionGames.PlayerInfo,DevionGames.Blackboard)
// 0x000000CD System.Boolean DevionGames.IAction::get_isActiveAndEnabled()
// 0x000000CE System.Void DevionGames.IAction::OnSequenceStart()
// 0x000000CF System.Void DevionGames.IAction::OnStart()
// 0x000000D0 DevionGames.ActionStatus DevionGames.IAction::OnUpdate()
// 0x000000D1 System.Void DevionGames.IAction::Update()
// 0x000000D2 System.Void DevionGames.IAction::OnEnd()
// 0x000000D3 System.Void DevionGames.IAction::OnSequenceEnd()
// 0x000000D4 System.Void DevionGames.IAction::OnInterrupt()
// 0x000000D5 DevionGames.ActionStatus DevionGames.Sequence::get_Status()
extern void Sequence_get_Status_m4691D26F020F3394AC62465B1BCCACC095009163 (void);
// 0x000000D6 System.Void DevionGames.Sequence::.ctor(UnityEngine.GameObject,DevionGames.PlayerInfo,DevionGames.Blackboard,DevionGames.IAction[])
extern void Sequence__ctor_m44F6FC7B33DE0A595E7D8EDD34190B3A3176C129 (void);
// 0x000000D7 System.Void DevionGames.Sequence::Start()
extern void Sequence_Start_m849C4AD223E00ED601AD87EC022D65D39110CD40 (void);
// 0x000000D8 System.Void DevionGames.Sequence::Stop()
extern void Sequence_Stop_m3E3E51E0E184B962435C57BA1B0332DF05EAF725 (void);
// 0x000000D9 System.Void DevionGames.Sequence::Interrupt()
extern void Sequence_Interrupt_m8AF6446DD2A164611B175C1D09AEBC01E071A0DE (void);
// 0x000000DA System.Void DevionGames.Sequence::Update()
extern void Sequence_Update_m4BDB78C95AB15C92B06BB1CAB437935F98F154AB (void);
// 0x000000DB System.Boolean DevionGames.Sequence::Tick()
extern void Sequence_Tick_m0EC5BA3677022D4D6CC8DAF2238DC32FB5E69429 (void);
// 0x000000DC System.Void DevionGames.Sequence/<>c::.cctor()
extern void U3CU3Ec__cctor_m9F19245DC042EA08DF512E7D8EFC1A590D79183F (void);
// 0x000000DD System.Void DevionGames.Sequence/<>c::.ctor()
extern void U3CU3Ec__ctor_m3F2505F38DD061B43CA48136E9290B8A8F055F31 (void);
// 0x000000DE System.Boolean DevionGames.Sequence/<>c::<Start>b__8_0(DevionGames.IAction)
extern void U3CU3Ec_U3CStartU3Eb__8_0_m9833B085A21CBF3709E9F967E2BEECA8A9DEF42F (void);
// 0x000000DF System.Void DevionGames.TriggerRaycaster::Initialize()
extern void TriggerRaycaster_Initialize_mCBB8C03C0080AB308E85ED0FA7CC92789B311786 (void);
// 0x000000E0 System.Void DevionGames.TriggerRaycaster::Start()
extern void TriggerRaycaster_Start_mA82CF29DE4C70E6B5069DC825B533F854EDD540D (void);
// 0x000000E1 System.Void DevionGames.TriggerRaycaster::Update()
extern void TriggerRaycaster_Update_m3C4A9FDA749FAF32E59EBF120EDA8EE6280FA6B9 (void);
// 0x000000E2 System.Boolean DevionGames.TriggerRaycaster::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void TriggerRaycaster_Raycast_mBE14B6EBCEDA15DA628024F66BB1A87FE3BBA29D (void);
// 0x000000E3 System.Boolean DevionGames.TriggerRaycaster::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern void TriggerRaycaster_Raycast_m095926216B6D288A9F01C7F496A3E50A8F398902 (void);
// 0x000000E4 System.Boolean DevionGames.TriggerRaycaster::IsPointerOverTrigger()
extern void TriggerRaycaster_IsPointerOverTrigger_mFF9B18D0FB2D7BC2FEAD7C17C3546B17AFFFCA38 (void);
// 0x000000E5 System.Void DevionGames.TriggerRaycaster::.ctor()
extern void TriggerRaycaster__ctor_m6D4C192C454AB5F3927C17FFEDA8C904C0745DE7 (void);
// 0x000000E6 System.Void DevionGames.TriggerRaycaster::.cctor()
extern void TriggerRaycaster__cctor_mC0D06B9C5D7CD828FD1DB8054467C5D84FE0079B (void);
static Il2CppMethodPointer s_methodPointers[230] = 
{
	NULL,
	BaseTrigger_get_Callbacks_mC64DB930EB107C8CF28A773C850FDEE5B5FC8C24,
	BaseTrigger_get_InRange_mD0C2197559CD4AA414D901BA116106CEEC562E4F,
	BaseTrigger_set_InRange_mC9F0D95111F03B57CF9C5524CC0F2E9256827394,
	BaseTrigger_get_InUse_mD360CDBE3AFC612823AD801438C887749D5EAA1D,
	BaseTrigger_set_InUse_m63BB8D722FE055D30819F4B475D724B4D38C887F,
	BaseTrigger_Start_m7D76B0F929650B8675034A72F0B717E8AE52A3BB,
	BaseTrigger_OnDisable_mC3209EE9E8B06FED13EBBB3356E87856FFADA1B8,
	BaseTrigger_OnEnable_m7A001B497CA663AB7CBDA8D92D78904A79427123,
	BaseTrigger_Update_mE0CDBCD464FA149DB766371C60E4BB8204CC80E8,
	BaseTrigger_OnDestroy_mB7C3075B973687BD39781D9A5D4B7A39DEC8BF91,
	BaseTrigger_OnTriggerEnter_mA1FF21B8C8DFC6C3B2757F2A7FE0CCC289FEC13C,
	BaseTrigger_OnTriggerExit_mAFC7150F8E615FC342411F68A08C50B4F8072A82,
	BaseTrigger_OnPointerTriggerClick_m97BEB9BA3FEBA31B675458EE47A196CA4C2EBA01,
	BaseTrigger_Use_m62E0A8F63EB628107B894F7E53B9D175E77C559B,
	BaseTrigger_CanUse_m7AA6E83991A2011AE63C1B304E03F1CC635CF941,
	BaseTrigger_OnWentOutOfRange_m261BB40395A49630782271D0097EFFB0A1230F0C,
	BaseTrigger_NotifyWentOutOfRange_mA20C61F657EE345AA4E758CC3C0A7FA93F844C20,
	BaseTrigger_OnCameInRange_m232CA4AC9A80FAE7A162E205396494CFF022A200,
	BaseTrigger_NotifyCameInRange_mCBBD0A19365BFEF9F00673A641BD1E7C66A7D600,
	BaseTrigger_OnTriggerUsed_m539790192971C9BFC658F5F2E351A3B8EECE4F31,
	BaseTrigger_NotifyUsed_mF072F076A0D4FD6D3B3EAEE53949D16CD0B01E06,
	BaseTrigger_OnTriggerUnUsed_mA6E2C856EE075B62F51C90C0CB966E098984302A,
	BaseTrigger_NotifyUnUsed_m9CCADFD57AA1BB5F3ACA074AD6EE775E52173DEB,
	BaseTrigger_DisplayInUse_m4FA8E7C3548136D790AA582F30BCA0D1E4DD02FA,
	BaseTrigger_DisplayOutOfRange_mD4B9361F6940053673ADA2CF2EED72244680BAD9,
	BaseTrigger_CreateTriggerCollider_m2985FFABA906DFCB9DC99013A56704E7F958F90F,
	BaseTrigger_IsBestTrigger_m31638B732D8AA18CE4E8164105282C9BBF7A7AC4,
	BaseTrigger_Execute_mB03D63B14E1B71596F4EFA10B472DAA767585E42,
	BaseTrigger_Execute_mB66465591A390A7BE60A54E3BD1A639C41A390C3,
	BaseTrigger_Execute_mC05568B86956BC181B29BF100926C14B1053D161,
	BaseTrigger_Execute_mDDCE56B5DB318ECD5DF34405B212F0650D42363B,
	NULL,
	NULL,
	NULL,
	BaseTrigger_RegisterCallbacks_m93A3889C762B81BCF512A8BA41B6C7D27743046B,
	BaseTrigger__ctor_mE5E2AA708111BC2ABCEC59F84EEC3D2969DC9D07,
	BaseTrigger__cctor_m913D770449084248393DB2F5A8DDAC1AD6E5FD6A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BehaviorTrigger_get_PlayerInfo_mE34FF1915D61F354FC090711157F85B526CF39C2,
	BehaviorTrigger_Start_m96A283387E9B71CC47956EE160AA8B8DA1753D5C,
	BehaviorTrigger_Update_m54A700916071A6052C59A3932DE07B1E096A18F5,
	BehaviorTrigger_OnDisable_m8B79F19AD41CCB8A1FE665E53970032273014654,
	BehaviorTrigger_OnDestroy_m72F8908451E91A300A47E7D73359B4AA9BC410D0,
	BehaviorTrigger_NotifyInterrupted_m9D122309121EDE50AA5E3193C819832429F27022,
	BehaviorTrigger_OnTriggerInterrupted_mDE8B0CD53D7F5AB225C298B3528999CED04C12FE,
	BehaviorTrigger_OnTriggerUsed_mC998E2EAC00D77C1549A1B6F958FE27AB7EC30A3,
	BehaviorTrigger_OnTriggerUnUsed_mE4F5D4A958C8605D6DF94C7D9407879B97241F3D,
	BehaviorTrigger_Use_mB7731D131894E725370EFB82069E2FE67E8F02B3,
	BehaviorTrigger_CacheAnimatorStates_m2720947D242ADEE1D0523EB00138481E8C6618AC,
	BehaviorTrigger_LoadCachedAnimatorStates_m9FD82763FBC1482D6A4D63CF6CED9DCDF4C9D552,
	BehaviorTrigger__ctor_m29D4C9C6463C70E92ED88B8DBA6AFEB339A42D2E,
	U3CU3Ec__cctor_m8052C0027353DFE1B8A17B0A9D71ED93726D70B1,
	U3CU3Ec__ctor_m6C1B7E0D3904C207D4E8BDF7002C0A2B193BE746,
	U3CU3Ec_U3CStartU3Eb__8_0_m18BD8B51C635110F7FB4467865D2E8803F61A0B7,
	Action_get_enabled_m3E685F616574AD92346C8B3DF2DACC0ADF5B039A,
	Action_set_enabled_mDC0492849F3495BA212693C56C99A6C7860B4593,
	Action_get_isActiveAndEnabled_m2C3B004316FA34965062F4070ECF041521277701,
	Action__ctor_m7119657D6D723178E7BD4CDA51A9BD485D7D07D3,
	Action_Initialize_m4773A734FABBDC9455F05A5F63CE44A7F4E122AD,
	NULL,
	Action_Update_mB1CD4E4E3B22FBFAC9235DA278F0969727D17AB5,
	Action_OnStart_mD73399B0D148CF794CA3FDFDB6922A33956CC66B,
	Action_OnEnd_mEF505768D02E7A75E2F5126C164172A19ABC06F4,
	Action_OnSequenceStart_m620ECAACB80A3025A8CA7ED39B6F2755765F7EB3,
	Action_OnSequenceEnd_m30CE37C3F44F3AE810A27E6E43F31A08B83A7603,
	Action_OnInterrupt_m3709A86DF3A0478A8413E02DF2148C20B120776B,
	Action_GetTarget_m851A94DDC949456AADC16F46DEC3A18CF6E3AC58,
	ActionTemplate__ctor_mC5BEFD46C90E52FD3E9E9E96014F9FF498B630E4,
	Actions__ctor_m00BFB49A689897CEF81A41F31B670E43ED47C6BE,
	ApplyRootMotion_OnStart_m7D9A7FF0D95E17CED6DED2623AECD518FEC546E5,
	ApplyRootMotion_OnUpdate_mE6DEBD8EAD396123C3AB8EE32B2698BD813AFDEC,
	ApplyRootMotion__ctor_m800ACCFDCE0DC8D56066D4DA7B2432E4E4C2CD78,
	CrossFade_OnStart_mAB6693AA5BBBF5C774667CF5F6223EB537D05F91,
	CrossFade_OnUpdate_m0777A9663FB55DA5BE262270DF2CB3D03D057BC4,
	CrossFade__ctor_mDBCB791C23B9115C3D76EEBC54D3D016BE891B51,
	IsInTransition_OnStart_m0EC807F1DB2F6B09D93D2DDE2EE1697C696CB8D2,
	IsInTransition_OnUpdate_m32B5E3941DE60039707EF3AEDBE70DA416E3D427,
	IsInTransition__ctor_mAE7ADDDEA3BAD3E7F20BB22A1872D9DD884B74D5,
	IsName_OnStart_mB3477C9B00DBC7B21ECF56C00725DF5BA3B17118,
	IsName_OnUpdate_mD975D367F993145B9D8E676268D2645F7CC01D83,
	IsName__ctor_m9A19E2172ECADFC58218DCAB1770411FA3E28BBE,
	SetBool_OnStart_m82BDD0856A3C4B9060DF3FF37A34B5A624279672,
	SetBool_OnUpdate_mA6FADBB071A5B5000066ABBB7230397CA291C245,
	SetBool__ctor_mAAFB065606C9D13028BEEDB60D6C5F569247158B,
	SetFloat_OnSequenceStart_m52784B6B30D99E53A1BA6A96607832CAA63CAD9D,
	SetFloat_OnUpdate_m0187B1C0B41F1A693C54AFD18370321654F9F988,
	SetFloat_Update_m1360C28930AFE3D0F2A962075857BCD60DFC2924,
	SetFloat__ctor_m4B5FF44DEE645941CC35E5BCBA7F959D62FFFD5A,
	SetInt_OnStart_m1B3C520B95A099AF91F8C608346FA15DD0EA9D1A,
	SetInt_OnUpdate_m89AE4A2FE14E618A3431979B397441D46477FDEE,
	SetInt__ctor_m5D00E8A68C4B8F0F35B0B965035CDC9A4FEDD747,
	SetRandomFloat_OnStart_m4FD413ECB73CBB3ED06CAB660D6540CC505AC78A,
	SetRandomFloat_OnUpdate_m646D5B912DDB71B16009115BCF6B11E5B052E05E,
	SetRandomFloat_Update_m1569A69218044E92BA9CC484A13CAFD9CA17D690,
	SetRandomFloat__ctor_mA17BB56C8E61E66F77D74083116638DC9CB40A70,
	SetTrigger_OnStart_mD980D2452AB84D2B3C676BFF20104ADA81822C42,
	SetTrigger_OnUpdate_m1FAA595EC9449096409543F0EDB55C951E5339AC,
	SetTrigger__ctor_mD0C7A14024DA06FCB73CA74D74B1B07EC78EB537,
	Play_OnUpdate_mEAED35577B941330792402C146118D39FEB5261D,
	Play__ctor_m2EB9071A249324B98FDDF76203465C43AAA57616,
	DeleteVariable_OnUpdate_m7868C1C871F2F2A32D3ED2468D387D0E6B40361C,
	DeleteVariable__ctor_m1B5275222D87174A1D2EA7F5293F73CBA4A79E38,
	InvokeWithVariable_OnStart_m52BDB5E25A4612D82BC92DA03445DF8225F28D04,
	InvokeWithVariable_OnUpdate_m1D34AB41452642162629370F9B66DB57175A650C,
	InvokeWithVariable__ctor_m50C1FA175699BA5B1DB4335D9DCA52244798C180,
	SetBoolVariable_OnUpdate_mAB50E1FACF37EB4B50D49FBE569BF668648E7B10,
	SetBoolVariable__ctor_mAE6A1C462FDBA890EE234121CE750045FB4B4995,
	SetFloatVariable_OnUpdate_m37442F9C791118F975C3D2360B200923C89D14C9,
	SetFloatVariable_Update_mBE5BB44EEFEA6B256F787636F3CB44E87E2B24FF,
	SetFloatVariable__ctor_mB3A04C0C86107E4273C0A04694CD90180C42B97B,
	CompareNumber_OnStart_m393C676BCE77AFBD3443F902C33DB67BE7CB405E,
	CompareNumber_OnUpdate_m3332284829A50A0229A754AEDD4309A69EC5C0AC,
	CompareNumber__ctor_m31E0999E6D1207AB7B0BD7C49BB3B880C09C1CBE,
	Invoke_OnStart_mD3BF777E342589A89E3FA81BBDB94734E6F6FA8E,
	Invoke_OnUpdate_m893585F86A072290A343E2E4A786FA16CC7F1AE6,
	Invoke__ctor_mD9B020526D8CB0099DA7023AFAE47ABE68276CAF,
	SetEnabled_OnStart_m5E39798C0392DC9F7BE5F4970D53523675276793,
	SetEnabled_OnUpdate_mBADE2C408E6B59ABA543D907426DC219DA5AD089,
	SetEnabled_OnInterrupt_m62F571455ECB3346CD0646847E094B55043C9F95,
	SetEnabled__ctor_m3ACB5E0F4369FFEACC05D7B6ECB8B92739586894,
	Log_OnUpdate_mAA2A6183AE1F760F8A3DD2811D510D19A5120A99,
	Log__ctor_mDACC5F7C20FCF0949B184CEF298A15C2769299EA,
	BroadcastMessage_OnStart_m92D627BD09407A2BA1EE208C6292C9333BB162E3,
	BroadcastMessage_OnUpdate_m87FE8505473862B47088B2FD577C77248EF69436,
	BroadcastMessage__ctor_mCBC1909E7855ACD79FE48B6589F0188CDC018097,
	CompareTag_OnUpdate_mCDF649EC7D09E3F18AF22C3070C291588E855D7B,
	CompareTag__ctor_m5AE4402D3B2F131546B66EFEB2AE5E0F1166A219,
	Destroy_OnUpdate_mA7F981477BC92602F101369729BCA6E7D80E132D,
	Destroy__ctor_m13E428566C2B023B06CB193B5482D587F0522570,
	Instantiate_OnStart_mAAFD0B1D6DF9C83E328C9CAFE2914F7A1B4639CF,
	Instantiate_OnUpdate_m6287DF7420B6EDCC4B4B65F17EAD31AA81DC3BBB,
	Instantiate_FindBone_m7280E568549B5685D2BA9D3785B3F91A9F429F72,
	Instantiate__ctor_mF2FB020437A2879E14C59D14177FC13FAE2CF9A5,
	InstantiateAtMouse_OnUpdate_m8F4BDECD92EEDD1F0B4DE88275D3D73B67D14A5B,
	InstantiateAtMouse__ctor_m37AFCEEA15F99C8DF551B751FC46040FE8A0D5B1,
	SendMessage_OnStart_m37BD25D4A5648329D30610730A0398AEF38BC941,
	SendMessage_OnUpdate_mE662F7C0839088E5026F1A305825822FBD654EED,
	SendMessage__ctor_mAC8D23083DD4ED95D60D8A09B7E90338090F11C7,
	SendMessageUpwards_OnStart_m8EA1AD784BEBFD8F8BE3FE8D7F769063D2295851,
	SendMessageUpwards_OnUpdate_mCFDF6553301B2D9C618796689C0B19D1BFAF1CB8,
	SendMessageUpwards__ctor_m91D8EE08B279A049821C79B9DCE12D5A935FAD86,
	SetName_OnUpdate_m2993B35BE38BC47782EC03411EF117858EC0B9F6,
	SetName__ctor_m1B9EE7B2FBCD7AF20719F555AD79A50CF2CE4D6D,
	OverlapSphere_OnStart_m83E0CB9A1C05863396DA5B59DB55FEC651839E81,
	OverlapSphere_OnUpdate_mFF523BD988DC2701F8EBEC21917B38E256651BA4,
	OverlapSphere__ctor_m0D1E8ECAC792244B5CAFA6C4604E1F158F2DBAC5,
	PhysicsUtility_GetDirection_mBD2913B5CDCC25C3769D25BD7ECA458D6C903AC1,
	Raycast_OnStart_m1DEE292098130404306E662DE09895C76D4FEDD0,
	Raycast_OnUpdate_mE6B89D2B28D97889487674A39B597A19A84D0B07,
	Raycast_DoRaycast_m7759B992D986A7C137297083920A1B5F96E8AFC3,
	Raycast__ctor_m1238E1DA46419AB7322AC5F186126630CE919426,
	SphereCast_OnStart_m9B7C623646129ADE45911C089A0A4C1819EC0BC6,
	SphereCast_OnUpdate_m1A363B7B69C665561E788F3E717C0C9F23FB78F1,
	SphereCast__ctor_m2D9E2EE7E9873423AF288B05D69577188712D2DE,
	SetConstraints_OnStart_mC13922B7BB7C582B606D0E65AB890AECDDFD2BEA,
	SetConstraints_OnUpdate_m567F6559B3D4732DB6BFDC6DEA1D3FA3613F776E,
	SetConstraints_OnInterrupt_m17E776AFFC1AA60B4A328EB82F66B73C285F23EC,
	SetConstraints__ctor_m576BAF262D673D7F8DF02B1E63A177DF3913BD3C,
	LoadScene_OnUpdate_mD59DE29E5660421C051E76F82ABB6664D9345A21,
	LoadScene__ctor_m3907F6D99C0C2CFA9C326D7496EEC97FD0A9B070,
	LookAtMouse_OnStart_mB1D2F84491B3FF86FD391BEF886AB14D724E3E78,
	LookAtMouse_OnUpdate_m49C8AA9B88C550F2048B0D4FFC3D3A03651BE4C6,
	LookAtMouse__ctor_mC764BFF90AB3BF097017915AA533EB44B345B4C6,
	LookAtPosition_OnStart_m4F256863D6DC188E5CEB96B2DA21773F5CAEB0B6,
	LookAtPosition_OnUpdate_mF841810B1F10D8E76850292B896A428A9D870CD4,
	LookAtPosition__ctor_m86CEAF5B1B915D75BACE12CB4A3B3684DF3993DB,
	LookAtTrigger_OnStart_m552897C9BA41DD1158A380D39594E36FAC57F6D4,
	LookAtTrigger_OnUpdate_mC8884629868570751BB78E9DAD865B9C386C4F43,
	LookAtTrigger__ctor_m0FFE8F8F4C3FFE096D861F7E5E278B635EB87AED,
	LookForward_OnStart_m0BFA7C18491AC2101736B18C020B03415EA37B0D,
	LookForward_OnUpdate_m56591D27723F8BDEC2442383746DBCE6DD01D247,
	LookForward__ctor_mEC5A8F3D192107CC079D5816404C39EBBD9F8520,
	MoveTowards_OnStart_mB268E77197D94B3DC6926F163738FD363E650D2B,
	MoveTowards_OnUpdate_mB1306BD264FB43A44F9B22C746067F8717375971,
	MoveTowards_LookAtPosition_m59AD764AA8C8F0786B7145A9864F64B23DD60EAB,
	MoveTowards__ctor_m6655153964FABC9A2380B8823B633480612406ED,
	SetPosition_OnStart_m73E4A01019EACCC282AE8D7ED08191164E971049,
	SetPosition_OnUpdate_m7C67715CEC3FCCAC6D0B79E8583861A8FEA3DEAB,
	SetPosition__ctor_m6E1AC390926612CCD39A59BA931F177CFE6C61E2,
	SetPositionToTarget_OnStart_m52AB477416B67464004DD15EE4F55F6814C72748,
	SetPositionToTarget_OnUpdate_mA3196CB40FBBA1734A50B2CB08045D18C598345F,
	SetPositionToTarget_GetClosest_mA8DE8B3C8DDC0F72FD6BF8B3C9649B9E144B2DDF,
	SetPositionToTarget__ctor_mC3ADDFEE55017CFAEA9374EF37FB5BA0F52DE32A,
	U3CU3Ec__cctor_mBB80EF0D16482050C013176FD914F80A913AB69C,
	U3CU3Ec__ctor_m5F753DC6569637BA4062BB2F0A8C240BCDE7967C,
	U3CU3Ec_U3COnUpdateU3Eb__7_0_mA5B15D9D68E2D467BC8A580EAAA9E5768B4BCB31,
	Wait_OnStart_mEC87E7564BA22E481A4982D69775CF0A90E1CC06,
	Wait_OnUpdate_mD86A2BFDC926204E2C73AC924807321F69E3EFE6,
	Wait__ctor_m91FE16DBEAF758CC33904ACAFA7A3F6AE8F0A7DF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Sequence_get_Status_m4691D26F020F3394AC62465B1BCCACC095009163,
	Sequence__ctor_m44F6FC7B33DE0A595E7D8EDD34190B3A3176C129,
	Sequence_Start_m849C4AD223E00ED601AD87EC022D65D39110CD40,
	Sequence_Stop_m3E3E51E0E184B962435C57BA1B0332DF05EAF725,
	Sequence_Interrupt_m8AF6446DD2A164611B175C1D09AEBC01E071A0DE,
	Sequence_Update_m4BDB78C95AB15C92B06BB1CAB437935F98F154AB,
	Sequence_Tick_m0EC5BA3677022D4D6CC8DAF2238DC32FB5E69429,
	U3CU3Ec__cctor_m9F19245DC042EA08DF512E7D8EFC1A590D79183F,
	U3CU3Ec__ctor_m3F2505F38DD061B43CA48136E9290B8A8F055F31,
	U3CU3Ec_U3CStartU3Eb__8_0_m9833B085A21CBF3709E9F967E2BEECA8A9DEF42F,
	TriggerRaycaster_Initialize_mCBB8C03C0080AB308E85ED0FA7CC92789B311786,
	TriggerRaycaster_Start_mA82CF29DE4C70E6B5069DC825B533F854EDD540D,
	TriggerRaycaster_Update_m3C4A9FDA749FAF32E59EBF120EDA8EE6280FA6B9,
	TriggerRaycaster_Raycast_mBE14B6EBCEDA15DA628024F66BB1A87FE3BBA29D,
	TriggerRaycaster_Raycast_m095926216B6D288A9F01C7F496A3E50A8F398902,
	TriggerRaycaster_IsPointerOverTrigger_mFF9B18D0FB2D7BC2FEAD7C17C3546B17AFFFCA38,
	TriggerRaycaster__ctor_m6D4C192C454AB5F3927C17FFEDA8C904C0745DE7,
	TriggerRaycaster__cctor_mC0D06B9C5D7CD828FD1DB8054467C5D84FE0079B,
};
static const int32_t s_InvokerIndices[230] = 
{
	2912,
	2912,
	2941,
	2477,
	2941,
	2477,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	2434,
	2941,
	2941,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2941,
	4276,
	4276,
	4276,
	4276,
	-1,
	-1,
	-1,
	2975,
	2975,
	4649,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2912,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2941,
	2975,
	2975,
	2975,
	4649,
	2975,
	2133,
	2941,
	2477,
	2941,
	2975,
	931,
	2897,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	1894,
	2975,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	1088,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	4214,
	2975,
	2897,
	2941,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2508,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	1088,
	2975,
	4649,
	2975,
	1898,
	2975,
	2897,
	2975,
	931,
	2941,
	2975,
	2975,
	2897,
	2975,
	2975,
	2975,
	2975,
	2897,
	627,
	2975,
	2975,
	2975,
	2975,
	2941,
	4649,
	2975,
	2133,
	4649,
	2975,
	2975,
	3330,
	3581,
	4636,
	2975,
	4649,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x06000021, { 0, 4 } },
	{ 0x06000022, { 4, 4 } },
	{ 0x06000023, { 8, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[9] = 
{
	{ (Il2CppRGCTXDataType)3, 21542 },
	{ (Il2CppRGCTXDataType)2, 63 },
	{ (Il2CppRGCTXDataType)3, 7243 },
	{ (Il2CppRGCTXDataType)1, 63 },
	{ (Il2CppRGCTXDataType)3, 21543 },
	{ (Il2CppRGCTXDataType)2, 64 },
	{ (Il2CppRGCTXDataType)3, 15024 },
	{ (Il2CppRGCTXDataType)1, 64 },
	{ (Il2CppRGCTXDataType)2, 62 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_Triggers_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_Triggers_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_Triggers_CodeGenModule = 
{
	"DevionGames.Triggers.dll",
	230,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	9,
	s_rgctxValues,
	NULL,
	g_DevionGames_Triggers_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
