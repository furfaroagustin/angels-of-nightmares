﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Type WindowsRuntimeExtension::GetBaseType(System.Type)
extern void WindowsRuntimeExtension_GetBaseType_mE6ACF7E58CB0D935D01E896D429EAF4B58DDBDEF (void);
// 0x00000002 System.Reflection.Assembly WindowsRuntimeExtension::GetAssembly(System.Type)
extern void WindowsRuntimeExtension_GetAssembly_m3CAE53A6F8F7706260CC43FDCB1B8EA3FA795D6E (void);
// 0x00000003 System.Boolean WindowsRuntimeExtension::IsValueType(System.Type)
extern void WindowsRuntimeExtension_IsValueType_m9F0ED94D95D39AE9356387E07522334ABBE0D477 (void);
// 0x00000004 System.Boolean WindowsRuntimeExtension::IsGenericType(System.Type)
extern void WindowsRuntimeExtension_IsGenericType_mC31B6B3F7CA5148D1A5895401883948F5FBF1619 (void);
// 0x00000005 System.Boolean WindowsRuntimeExtension::ParametersMatch(System.Reflection.ParameterInfo[],System.Type[])
extern void WindowsRuntimeExtension_ParametersMatch_m2ACF810B60431BF319CAEC7D8A6D0E0853AA9708 (void);
// 0x00000006 System.Boolean WindowsRuntimeExtension::MatchBindingFlags(System.Boolean,System.Boolean,System.Boolean,System.Reflection.BindingFlags,System.Boolean)
extern void WindowsRuntimeExtension_MatchBindingFlags_m420BC851B0FC38308C8983FF213E7F86978E6F17 (void);
// 0x00000007 System.Void DevionGames.AnimationEventSender::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern void AnimationEventSender_OnStateMachineEnter_m3024F864EED5574D2AF1BE59B1E6DB5A0653AAD2 (void);
// 0x00000008 System.Void DevionGames.AnimationEventSender::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern void AnimationEventSender_OnStateMachineExit_mABF0DE2EE0ECD6FB407F828C4ACBC773E832544A (void);
// 0x00000009 System.Void DevionGames.AnimationEventSender::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AnimationEventSender_OnStateEnter_mE66210AFDE3ADB38C1CD286B0C0430D1759D433C (void);
// 0x0000000A System.Void DevionGames.AnimationEventSender::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AnimationEventSender_OnStateUpdate_m8ABBFC3C4AE42ECC9407392E3660C7E18FDE3298 (void);
// 0x0000000B System.Void DevionGames.AnimationEventSender::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void AnimationEventSender_OnStateExit_m81ADCCBB68A02AC00CFBBBF6D4F38F9A70FFFA60 (void);
// 0x0000000C System.Void DevionGames.AnimationEventSender::SendEvent(UnityEngine.Animator)
extern void AnimationEventSender_SendEvent_m8664651F0085CE8306EF251D521C25DE7286DA37 (void);
// 0x0000000D System.Void DevionGames.AnimationEventSender::.ctor()
extern void AnimationEventSender__ctor_m451453AB5C89374328CB4EEA2839B758BB97441A (void);
// 0x0000000E DevionGames.ArgumentType DevionGames.ArgumentVariable::get_ArgumentType()
extern void ArgumentVariable_get_ArgumentType_mBD12B96B3BC86619A2668689F733040C3AD92263 (void);
// 0x0000000F System.Void DevionGames.ArgumentVariable::set_ArgumentType(DevionGames.ArgumentType)
extern void ArgumentVariable_set_ArgumentType_m956F7B89C6AB5E0664CE26E1E66A6EBAF424D364 (void);
// 0x00000010 System.Boolean DevionGames.ArgumentVariable::get_IsNone()
extern void ArgumentVariable_get_IsNone_m0EA7262EB485DEE8011F3843C93CD67E5AEB377A (void);
// 0x00000011 System.Object DevionGames.ArgumentVariable::GetValue()
extern void ArgumentVariable_GetValue_mBAE495F444E558E1C64641A071949EB0ADFE60D9 (void);
// 0x00000012 System.String DevionGames.ArgumentVariable::GetPropertyValuePath(DevionGames.ArgumentType)
extern void ArgumentVariable_GetPropertyValuePath_m10F268A42BC3A41B5EE96AC5FBB1424994F5B3E7 (void);
// 0x00000013 System.Void DevionGames.ArgumentVariable::.ctor()
extern void ArgumentVariable__ctor_m7E3C07A465C4ABABFB1D5884FA93480090A8FB37 (void);
// 0x00000014 System.String DevionGames.CategoryAttribute::get_Category()
extern void CategoryAttribute_get_Category_m5E616988DFEB4339B40F43FACAEAE3B62CB80C61 (void);
// 0x00000015 System.Void DevionGames.CategoryAttribute::.ctor(System.String)
extern void CategoryAttribute__ctor_m84BE4F1EB081CEE2C16249663AE79F5F84439A5C (void);
// 0x00000016 System.String DevionGames.ComponentMenu::get_componentMenu()
extern void ComponentMenu_get_componentMenu_mAF2EE494DD38FE27035083AD18E676BE9C078EAD (void);
// 0x00000017 System.Void DevionGames.ComponentMenu::.ctor(System.String)
extern void ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3 (void);
// 0x00000018 System.Void DevionGames.CompoundAttribute::.ctor(System.String)
extern void CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4 (void);
// 0x00000019 System.Void DevionGames.EnumFlagsAttribute::.ctor()
extern void EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818 (void);
// 0x0000001A System.Void DevionGames.ExcludeFromCreation::.ctor()
extern void ExcludeFromCreation__ctor_mCE92E999886A8742ECC711969904CAD04D20E038 (void);
// 0x0000001B System.Void DevionGames.HeaderLineAttribute::.ctor(System.String)
extern void HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC (void);
// 0x0000001C System.Void DevionGames.IconAttribute::.ctor(System.Type)
extern void IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330 (void);
// 0x0000001D System.Void DevionGames.IconAttribute::.ctor(System.String)
extern void IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD (void);
// 0x0000001E System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
extern void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (void);
// 0x0000001F System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String,System.String)
extern void InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF (void);
// 0x00000020 System.Void DevionGames.MinMaxSliderAttribute::.ctor(System.Single,System.Single)
extern void MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52 (void);
// 0x00000021 System.Void DevionGames.AudioEventListener::Awake()
extern void AudioEventListener_Awake_m9432FCC2F1AF4497EE6B80EB631285314BF64D7A (void);
// 0x00000022 System.Void DevionGames.AudioEventListener::PlayAudio(UnityEngine.AnimationEvent)
extern void AudioEventListener_PlayAudio_mD7BA13D42CE35C16521B890737E562B801E79B64 (void);
// 0x00000023 System.Void DevionGames.AudioEventListener::.ctor()
extern void AudioEventListener__ctor_m953712EC0045A6705EF4E286573AEFBC975BF439 (void);
// 0x00000024 UnityEngine.AudioSource DevionGames.AudioEventListener/AudioGroup::get_audioSource()
extern void AudioGroup_get_audioSource_mE43B8E290F8B4931DC7C44F8AC0237B4F6B03DB5 (void);
// 0x00000025 System.Void DevionGames.AudioEventListener/AudioGroup::set_audioSource(UnityEngine.AudioSource)
extern void AudioGroup_set_audioSource_m1B24F7BB72F968658A04E0F573CC2E6A6B9BAAD7 (void);
// 0x00000026 System.Void DevionGames.AudioEventListener/AudioGroup::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern void AudioGroup_PlayOneShot_m1F23D0A6B9074B5C9BC5FA586F68A005709A342A (void);
// 0x00000027 System.Void DevionGames.AudioEventListener/AudioGroup::.ctor()
extern void AudioGroup__ctor_m798D39DE14D4F2844AC1B347BCDFF254B4D3431D (void);
// 0x00000028 System.Void DevionGames.AudioEventListener/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m65094D917C56C1970F422F40FAB7D625AE6C7934 (void);
// 0x00000029 System.Boolean DevionGames.AudioEventListener/<>c__DisplayClass2_0::<PlayAudio>b__0(DevionGames.AudioEventListener/AudioGroup)
extern void U3CU3Ec__DisplayClass2_0_U3CPlayAudioU3Eb__0_m667F4D771465488DE976CD02559B7B154ABEE429 (void);
// 0x0000002A UnityEngine.AudioClip DevionGames.AudioPlaylist::get_Item(System.Int32)
extern void AudioPlaylist_get_Item_mC58282B14C6CBEB3F4856FD9A644D6222B50E78F (void);
// 0x0000002B System.Int32 DevionGames.AudioPlaylist::get_Count()
extern void AudioPlaylist_get_Count_m76AE369B79FC7429ECDC6FB9C48A89DADED71E63 (void);
// 0x0000002C System.Void DevionGames.AudioPlaylist::.ctor()
extern void AudioPlaylist__ctor_mDE731354783EC2170560CA95A5C2F2183A15CB83 (void);
// 0x0000002D System.Collections.ArrayList DevionGames.ArrayListVariable::get_Value()
extern void ArrayListVariable_get_Value_mFE6EFAC812D9BFC26BFAF313E75690C3943AA0A5 (void);
// 0x0000002E System.Void DevionGames.ArrayListVariable::set_Value(System.Collections.ArrayList)
extern void ArrayListVariable_set_Value_m2B47AE7F0CF3EFC418F0E7DFE5B1CD121F888BD2 (void);
// 0x0000002F System.Object DevionGames.ArrayListVariable::get_RawValue()
extern void ArrayListVariable_get_RawValue_m4E6A456E83E93C8F657DC65BD38F929E171AE97B (void);
// 0x00000030 System.Void DevionGames.ArrayListVariable::set_RawValue(System.Object)
extern void ArrayListVariable_set_RawValue_m8C63FBCECE83634F235350892B6D74B744A2B235 (void);
// 0x00000031 System.Type DevionGames.ArrayListVariable::get_type()
extern void ArrayListVariable_get_type_m71509A553C514611BEDBA94E4C085D975C06007B (void);
// 0x00000032 System.Void DevionGames.ArrayListVariable::.ctor()
extern void ArrayListVariable__ctor_mA27817C12868653C142186CDAEF1D20AC851AC44 (void);
// 0x00000033 System.Void DevionGames.ArrayListVariable::.ctor(System.String)
extern void ArrayListVariable__ctor_mF6D8C7A267B09ED9433943154177C0913D75F03C (void);
// 0x00000034 DevionGames.ArrayListVariable DevionGames.ArrayListVariable::op_Implicit(System.Collections.ArrayList)
extern void ArrayListVariable_op_Implicit_m5BF9FE5C3B188ADCC9A08AB13DD1542C275A706D (void);
// 0x00000035 System.Collections.ArrayList DevionGames.ArrayListVariable::op_Implicit(DevionGames.ArrayListVariable)
extern void ArrayListVariable_op_Implicit_m7DD6038F492FAC0F8A7659EAA2ADE4B080523D93 (void);
// 0x00000036 T DevionGames.Blackboard::GetValue(System.String)
// 0x00000037 T DevionGames.Blackboard::GetValue(DevionGames.Variable)
// 0x00000038 System.Void DevionGames.Blackboard::SetValue(System.String,System.Object)
// 0x00000039 System.Void DevionGames.Blackboard::SetValue(System.String,System.Object,System.Type)
extern void Blackboard_SetValue_mC1D4129E18F01A7FABAB12C57DF94026046F2C4F (void);
// 0x0000003A System.Boolean DevionGames.Blackboard::DeleteVariable(System.String)
extern void Blackboard_DeleteVariable_m79797EBE8ED6B85CDD93E01B85B18BA0BDBFCA7E (void);
// 0x0000003B DevionGames.Variable DevionGames.Blackboard::GetVariable(System.String)
extern void Blackboard_GetVariable_m39930581EE9595405048B096B4A9E28B52AC5899 (void);
// 0x0000003C System.Void DevionGames.Blackboard::AddVariable(DevionGames.Variable)
extern void Blackboard_AddVariable_m0DD1A4CF28914EED9614988CD9DB8CE0F0FB8161 (void);
// 0x0000003D DevionGames.Variable DevionGames.Blackboard::AddVariable(System.String,System.Object)
// 0x0000003E DevionGames.Variable DevionGames.Blackboard::AddVariable(System.String,System.Object,System.Type)
extern void Blackboard_AddVariable_m9A3A5A6E28034F01DC2656F19443141343584DAF (void);
// 0x0000003F System.Void DevionGames.Blackboard::.ctor()
extern void Blackboard__ctor_mE6ADBA43C560F7A8FE73C33014FE2CF7E2120D07 (void);
// 0x00000040 System.Void DevionGames.Blackboard/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m2BE4D85F14DD1B24B5CB6B7B58E239983FF2535D (void);
// 0x00000041 System.Boolean DevionGames.Blackboard/<>c__DisplayClass5_0::<DeleteVariable>b__0(DevionGames.Variable)
extern void U3CU3Ec__DisplayClass5_0_U3CDeleteVariableU3Eb__0_m1D789D9A6A8CC29A1D1C665730F9DCBDA27AE6B5 (void);
// 0x00000042 System.Void DevionGames.Blackboard/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m0D7DC7A912FF0B4C3F80E64A6D5D701012138AD8 (void);
// 0x00000043 System.Boolean DevionGames.Blackboard/<>c__DisplayClass6_0::<GetVariable>b__0(DevionGames.Variable)
extern void U3CU3Ec__DisplayClass6_0_U3CGetVariableU3Eb__0_m82F2E86DD1DCA5858B4676F1E2268997C9D8F59A (void);
// 0x00000044 System.Boolean DevionGames.BoolVariable::get_Value()
extern void BoolVariable_get_Value_m332ECE2B796C9D5410199F7AE056BEB9E85C8DAB (void);
// 0x00000045 System.Void DevionGames.BoolVariable::set_Value(System.Boolean)
extern void BoolVariable_set_Value_mD05AFF1E9DC1E2523AE6C1D0200CFCD9D2A5432B (void);
// 0x00000046 System.Object DevionGames.BoolVariable::get_RawValue()
extern void BoolVariable_get_RawValue_mCAC036B036E2FD98C16966C6BEF0C759836EC465 (void);
// 0x00000047 System.Void DevionGames.BoolVariable::set_RawValue(System.Object)
extern void BoolVariable_set_RawValue_m5B2B9D8E85394AFDACB23FF8F667A5BB58FA1683 (void);
// 0x00000048 System.Type DevionGames.BoolVariable::get_type()
extern void BoolVariable_get_type_m54DDF5197DA2252363F090F513458EF1DACE2F27 (void);
// 0x00000049 System.Void DevionGames.BoolVariable::.ctor()
extern void BoolVariable__ctor_m6174AF60ADEE6BA67E5BF390EC6CEB6E068DCF3A (void);
// 0x0000004A System.Void DevionGames.BoolVariable::.ctor(System.String)
extern void BoolVariable__ctor_m8398FC2DE8D1D5841DA97D89F62E23B822CD9F63 (void);
// 0x0000004B DevionGames.BoolVariable DevionGames.BoolVariable::op_Implicit(System.Boolean)
extern void BoolVariable_op_Implicit_mDF5931559A3092BE7558FB3AF3B5D58D5DAE442C (void);
// 0x0000004C System.Boolean DevionGames.BoolVariable::op_Implicit(DevionGames.BoolVariable)
extern void BoolVariable_op_Implicit_m5AAF4104A968E7B1DD231FA0D6E88BBC17E8CE3A (void);
// 0x0000004D UnityEngine.Color DevionGames.ColorVariable::get_Value()
extern void ColorVariable_get_Value_m015C3BFC907798781473443678169B8B5CB2E05D (void);
// 0x0000004E System.Void DevionGames.ColorVariable::set_Value(UnityEngine.Color)
extern void ColorVariable_set_Value_m0EB68E424C43C661C9757C4C6EF14C2420D62258 (void);
// 0x0000004F System.Object DevionGames.ColorVariable::get_RawValue()
extern void ColorVariable_get_RawValue_mF7C0CE35F9867A7616BD4961EE297E813557E3DA (void);
// 0x00000050 System.Void DevionGames.ColorVariable::set_RawValue(System.Object)
extern void ColorVariable_set_RawValue_m02674AF1ECF919F4FBB460CF8AC24B74859195B5 (void);
// 0x00000051 System.Type DevionGames.ColorVariable::get_type()
extern void ColorVariable_get_type_m576644C950562CCBC03E0FF1D9785BAB5D505082 (void);
// 0x00000052 System.Void DevionGames.ColorVariable::.ctor()
extern void ColorVariable__ctor_m559C57C93AEBBFC93F829A910AE93BC9C2AB1AF8 (void);
// 0x00000053 System.Void DevionGames.ColorVariable::.ctor(System.String)
extern void ColorVariable__ctor_mE4169ACA33372D039367FD35A705B6CF2F3B483F (void);
// 0x00000054 DevionGames.ColorVariable DevionGames.ColorVariable::op_Implicit(UnityEngine.Color)
extern void ColorVariable_op_Implicit_m0329B90B1AEBDECC45A2122B4A3ADDE9CAF92C80 (void);
// 0x00000055 UnityEngine.Color DevionGames.ColorVariable::op_Implicit(DevionGames.ColorVariable)
extern void ColorVariable_op_Implicit_m74AD9BBDF27ED1F276AB74367F850BF9CA6C803A (void);
// 0x00000056 System.Single DevionGames.FloatVariable::get_Value()
extern void FloatVariable_get_Value_mA1B7ABDFC1AE103F9C3C12B96460D284C1261DA5 (void);
// 0x00000057 System.Void DevionGames.FloatVariable::set_Value(System.Single)
extern void FloatVariable_set_Value_m6A213705CD85CA612DDA0EBF7919327DB7047ACA (void);
// 0x00000058 System.Object DevionGames.FloatVariable::get_RawValue()
extern void FloatVariable_get_RawValue_m0324E7A777F9B1B92D4A1291AD6EAE967B3A8669 (void);
// 0x00000059 System.Void DevionGames.FloatVariable::set_RawValue(System.Object)
extern void FloatVariable_set_RawValue_mCBAF1297B54FBC43DBC85058C6091505B0FF9A10 (void);
// 0x0000005A System.Type DevionGames.FloatVariable::get_type()
extern void FloatVariable_get_type_mFCC4FC8F7520D4B77C252195588DA35B1E513D90 (void);
// 0x0000005B System.Void DevionGames.FloatVariable::.ctor()
extern void FloatVariable__ctor_mD91FA9784B44B20F8F86CED745376622EAFCFC53 (void);
// 0x0000005C System.Void DevionGames.FloatVariable::.ctor(System.String)
extern void FloatVariable__ctor_mD7238551CDE9398472D9460F7CF2E8464232CF53 (void);
// 0x0000005D DevionGames.FloatVariable DevionGames.FloatVariable::op_Implicit(System.Single)
extern void FloatVariable_op_Implicit_mA53C094916B1A2C71B90CA9BFF638530CAED58E9 (void);
// 0x0000005E System.Single DevionGames.FloatVariable::op_Implicit(DevionGames.FloatVariable)
extern void FloatVariable_op_Implicit_mA43124B62982BF153986EABC309254ADDF90AD68 (void);
// 0x0000005F UnityEngine.GameObject DevionGames.GameObjectVariable::get_Value()
extern void GameObjectVariable_get_Value_m53A2A6D5F8AFE604B2180559EA0D4A3BFB5F058B (void);
// 0x00000060 System.Void DevionGames.GameObjectVariable::set_Value(UnityEngine.GameObject)
extern void GameObjectVariable_set_Value_m546A5744ED95FB12855FBD6306BAFB3939DFC860 (void);
// 0x00000061 System.Object DevionGames.GameObjectVariable::get_RawValue()
extern void GameObjectVariable_get_RawValue_m0FA2E40F9DAC771117819A1E0F6E5320ADCCC0EE (void);
// 0x00000062 System.Void DevionGames.GameObjectVariable::set_RawValue(System.Object)
extern void GameObjectVariable_set_RawValue_m517A8EF599964CA46D01EFDB5C8BBBB7E3D9F798 (void);
// 0x00000063 System.Type DevionGames.GameObjectVariable::get_type()
extern void GameObjectVariable_get_type_m9D7AB2140C111A7D99137F2CA22A79703F12CA9B (void);
// 0x00000064 System.Void DevionGames.GameObjectVariable::.ctor()
extern void GameObjectVariable__ctor_m1571F4F20290F5021FDB041E65EAF756CDAC6B0F (void);
// 0x00000065 System.Void DevionGames.GameObjectVariable::.ctor(System.String)
extern void GameObjectVariable__ctor_m6AFEFA274831D491A760A1B4C6DE30994B0D21AF (void);
// 0x00000066 DevionGames.GameObjectVariable DevionGames.GameObjectVariable::op_Implicit(UnityEngine.GameObject)
extern void GameObjectVariable_op_Implicit_mAED845D8E325093AA75489EE6490226BE6696B96 (void);
// 0x00000067 UnityEngine.GameObject DevionGames.GameObjectVariable::op_Implicit(DevionGames.GameObjectVariable)
extern void GameObjectVariable_op_Implicit_m641BF9F16C956CFFA55B03990F93CFF16B1C03C0 (void);
// 0x00000068 System.Int32 DevionGames.IntVariable::get_Value()
extern void IntVariable_get_Value_m1FF293E0E5BA7FDAD08240FD215552990D795F49 (void);
// 0x00000069 System.Void DevionGames.IntVariable::set_Value(System.Int32)
extern void IntVariable_set_Value_m67C4A3DB666A78419AB600FF88C6A7F92E8F97BB (void);
// 0x0000006A System.Object DevionGames.IntVariable::get_RawValue()
extern void IntVariable_get_RawValue_mEBA6A0D5209FC1956E9F6AE3BE79F854B6265E4F (void);
// 0x0000006B System.Void DevionGames.IntVariable::set_RawValue(System.Object)
extern void IntVariable_set_RawValue_mAC37C745DE93E594223E32113520E77BEA5A46BD (void);
// 0x0000006C System.Type DevionGames.IntVariable::get_type()
extern void IntVariable_get_type_m2699893ABEABEE7F2A69A520BA4F32F58BBE0E94 (void);
// 0x0000006D System.Void DevionGames.IntVariable::.ctor()
extern void IntVariable__ctor_mA6DB20DA5D632A35D3F35E1F9EE73C78CC4C47AD (void);
// 0x0000006E System.Void DevionGames.IntVariable::.ctor(System.String)
extern void IntVariable__ctor_mA25B604E94831B321545CAD1A1F847E2FEE935B4 (void);
// 0x0000006F DevionGames.IntVariable DevionGames.IntVariable::op_Implicit(System.Int32)
extern void IntVariable_op_Implicit_m678DFA5E56A89C6F297A90175414652551D5190F (void);
// 0x00000070 System.Int32 DevionGames.IntVariable::op_Implicit(DevionGames.IntVariable)
extern void IntVariable_op_Implicit_m8CCE943400051FD0A1C27C2E4D8460BF6389A871 (void);
// 0x00000071 UnityEngine.Object DevionGames.ObjectVariable::get_Value()
extern void ObjectVariable_get_Value_m37083B1F21D8973732237B30AE8A2F4986B182D0 (void);
// 0x00000072 System.Void DevionGames.ObjectVariable::set_Value(UnityEngine.Object)
extern void ObjectVariable_set_Value_m98C993762C89B6989717615DCA00A025C169BD80 (void);
// 0x00000073 System.Object DevionGames.ObjectVariable::get_RawValue()
extern void ObjectVariable_get_RawValue_mE0E094C4AB0CD3C93F57EF2A202AB12A5546B9A8 (void);
// 0x00000074 System.Void DevionGames.ObjectVariable::set_RawValue(System.Object)
extern void ObjectVariable_set_RawValue_m3654FA6EDBCC4B81C6BBF82C66D610E025E4F179 (void);
// 0x00000075 System.Type DevionGames.ObjectVariable::get_type()
extern void ObjectVariable_get_type_m19DA2057045F8ADA72BC27BDA1C70956C8A625F9 (void);
// 0x00000076 System.Void DevionGames.ObjectVariable::.ctor()
extern void ObjectVariable__ctor_mB4D033335A1D22BD4A851FE2FF903E10B2DA6E2E (void);
// 0x00000077 System.Void DevionGames.ObjectVariable::.ctor(System.String)
extern void ObjectVariable__ctor_m51B3DF6CE5F4600594C1B4DF6DC1883A94FF7442 (void);
// 0x00000078 DevionGames.ObjectVariable DevionGames.ObjectVariable::op_Implicit(UnityEngine.Object)
extern void ObjectVariable_op_Implicit_m7E61B9B263DB84F9812B6748CC3F9A1D781259F4 (void);
// 0x00000079 UnityEngine.Object DevionGames.ObjectVariable::op_Implicit(DevionGames.ObjectVariable)
extern void ObjectVariable_op_Implicit_m7FE9F61AAD1A08A0AA05F6D8295BB17A09A495A5 (void);
// 0x0000007A System.Void DevionGames.SharedAttribute::.ctor()
extern void SharedAttribute__ctor_m738BE8BEB37C6EAD0C8841893DF1DEC881EACF4E (void);
// 0x0000007B System.String DevionGames.StringVariable::get_Value()
extern void StringVariable_get_Value_mCEE8C988AA9077131FC0CF1BE23AC3549F7092B7 (void);
// 0x0000007C System.Void DevionGames.StringVariable::set_Value(System.String)
extern void StringVariable_set_Value_m4FD334C170C3B80AABEEFD836B6AEC75150D8D67 (void);
// 0x0000007D System.Object DevionGames.StringVariable::get_RawValue()
extern void StringVariable_get_RawValue_m658AE8F29B78638D469F3760A8840F97B332EE33 (void);
// 0x0000007E System.Void DevionGames.StringVariable::set_RawValue(System.Object)
extern void StringVariable_set_RawValue_m2F7EFADB83883C85755A3AC094EFB97A865C8043 (void);
// 0x0000007F System.Type DevionGames.StringVariable::get_type()
extern void StringVariable_get_type_m37194FECCEB626340FAE9A3A6563B569AA180630 (void);
// 0x00000080 System.Void DevionGames.StringVariable::.ctor()
extern void StringVariable__ctor_m71872475CE29E267605F93BF0E2F23F1DE289CF7 (void);
// 0x00000081 System.Void DevionGames.StringVariable::.ctor(System.String)
extern void StringVariable__ctor_m15109BA1698AD27E314FC2DF8250E454BF5463C6 (void);
// 0x00000082 DevionGames.StringVariable DevionGames.StringVariable::op_Implicit(System.String)
extern void StringVariable_op_Implicit_m85C7CC038FC5D4053853C20FCC3CB1B2856D0D87 (void);
// 0x00000083 System.String DevionGames.StringVariable::op_Implicit(DevionGames.StringVariable)
extern void StringVariable_op_Implicit_mC5C837810ACB60BA0B122A56BB3B766B12319D38 (void);
// 0x00000084 System.String DevionGames.Variable::get_name()
extern void Variable_get_name_mE7CD1F19A9A1B8F180C3C129D748466FD3D255D7 (void);
// 0x00000085 System.Void DevionGames.Variable::set_name(System.String)
extern void Variable_set_name_mF56BB1728B5D6294614EC1A95ECD73AB6A7D7792 (void);
// 0x00000086 System.Boolean DevionGames.Variable::get_isShared()
extern void Variable_get_isShared_m7CDE99AC0F54969D9887DA1DE4DBDD4EE37EFB32 (void);
// 0x00000087 System.Void DevionGames.Variable::set_isShared(System.Boolean)
extern void Variable_set_isShared_m83CF7CB67F2137D49932729CD16FE25449FAFA8F (void);
// 0x00000088 System.Boolean DevionGames.Variable::get_isNone()
extern void Variable_get_isNone_mD765CC99DEB9D7589A728D052A909282EE2F51A2 (void);
// 0x00000089 System.Type DevionGames.Variable::get_type()
// 0x0000008A System.Object DevionGames.Variable::get_RawValue()
// 0x0000008B System.Void DevionGames.Variable::set_RawValue(System.Object)
// 0x0000008C System.Void DevionGames.Variable::.ctor()
extern void Variable__ctor_m3DF269A5FCC98AB6BF3F79F54FE3543E5C15BD42 (void);
// 0x0000008D System.Void DevionGames.Variable::.ctor(System.String)
extern void Variable__ctor_m1687146BB81F5AF850FA31F3D85CDAFB304F9911 (void);
// 0x0000008E UnityEngine.Vector2 DevionGames.Vector2Variable::get_Value()
extern void Vector2Variable_get_Value_mCB200EBE7B8517387A7688197BF863EABA3F5001 (void);
// 0x0000008F System.Void DevionGames.Vector2Variable::set_Value(UnityEngine.Vector2)
extern void Vector2Variable_set_Value_m79B547518A2A03D8314F9A2D6AB021F2E76D2030 (void);
// 0x00000090 System.Object DevionGames.Vector2Variable::get_RawValue()
extern void Vector2Variable_get_RawValue_m241313F422B5551AECC8CDAF0C5C4C35E32639C6 (void);
// 0x00000091 System.Void DevionGames.Vector2Variable::set_RawValue(System.Object)
extern void Vector2Variable_set_RawValue_mA4DF84B2C802CEF5A1883A7134C26F2DECE2F22F (void);
// 0x00000092 System.Type DevionGames.Vector2Variable::get_type()
extern void Vector2Variable_get_type_mC1038ACFC133A0708C4A4528DC1A711CD1830F40 (void);
// 0x00000093 System.Void DevionGames.Vector2Variable::.ctor()
extern void Vector2Variable__ctor_m1A8FFF5773652B0C90F4E2662AA950435F358A20 (void);
// 0x00000094 System.Void DevionGames.Vector2Variable::.ctor(System.String)
extern void Vector2Variable__ctor_m4C0919173D47A604EE87A251968B717D94F78420 (void);
// 0x00000095 DevionGames.Vector2Variable DevionGames.Vector2Variable::op_Implicit(UnityEngine.Vector2)
extern void Vector2Variable_op_Implicit_mD33CA39C14E78B5F3055A0DC121469F23AA02C3C (void);
// 0x00000096 UnityEngine.Vector2 DevionGames.Vector2Variable::op_Implicit(DevionGames.Vector2Variable)
extern void Vector2Variable_op_Implicit_m8283DC146B400A203D90001F289501B504808D9F (void);
// 0x00000097 UnityEngine.Vector3 DevionGames.Vector3Variable::get_Value()
extern void Vector3Variable_get_Value_mC5220AD3091F933D8662F8EBE4AFD44464C425BD (void);
// 0x00000098 System.Void DevionGames.Vector3Variable::set_Value(UnityEngine.Vector3)
extern void Vector3Variable_set_Value_m9707A1A009731DB5DE9F14765C4A3C131966ED0D (void);
// 0x00000099 System.Object DevionGames.Vector3Variable::get_RawValue()
extern void Vector3Variable_get_RawValue_m9D25377B82D4704A51AEAAB9DC173F60CCCCDDB6 (void);
// 0x0000009A System.Void DevionGames.Vector3Variable::set_RawValue(System.Object)
extern void Vector3Variable_set_RawValue_m535A6DC0C283D54204E8C85D49021667D240BFBE (void);
// 0x0000009B System.Type DevionGames.Vector3Variable::get_type()
extern void Vector3Variable_get_type_m1B1CCE67F472CF3A2C1AC4F698674D7AB60A9855 (void);
// 0x0000009C System.Void DevionGames.Vector3Variable::.ctor()
extern void Vector3Variable__ctor_mEAD567AD69468BD46E72258CEFF69E952FB91D67 (void);
// 0x0000009D System.Void DevionGames.Vector3Variable::.ctor(System.String)
extern void Vector3Variable__ctor_m30820CD501E201FAB010BBF72FAF93E0FC9663AC (void);
// 0x0000009E DevionGames.Vector3Variable DevionGames.Vector3Variable::op_Implicit(UnityEngine.Vector3)
extern void Vector3Variable_op_Implicit_m4D1CB12225B5B805CE1FD3AF2BFAC04286889C1A (void);
// 0x0000009F UnityEngine.Vector3 DevionGames.Vector3Variable::op_Implicit(DevionGames.Vector3Variable)
extern void Vector3Variable_op_Implicit_mB4BCCCA58D2A22AB2BA8C8A9EE82D71A22FE68A9 (void);
// 0x000000A0 System.Void DevionGames.CallbackEventData::.ctor()
extern void CallbackEventData__ctor_m103D4E02BCA85FB3E04CEA8524E42A9BAADDA333 (void);
// 0x000000A1 System.Void DevionGames.CallbackEventData::AddData(System.String,System.Object)
extern void CallbackEventData_AddData_mA4E495602ACF2FD723683BDBB3E371434960CA3C (void);
// 0x000000A2 System.Object DevionGames.CallbackEventData::GetData(System.String)
extern void CallbackEventData_GetData_mE16063313FC3A76BBBC40F9FAF898E14795D1770 (void);
// 0x000000A3 System.String[] DevionGames.CallbackHandler::get_Callbacks()
// 0x000000A4 System.Void DevionGames.CallbackHandler::Execute(System.String,DevionGames.CallbackEventData)
extern void CallbackHandler_Execute_m977B75E8AABBCF185D4C1CB477EBA4D84C68AD31 (void);
// 0x000000A5 System.Void DevionGames.CallbackHandler::RegisterListener(System.String,UnityEngine.Events.UnityAction`1<DevionGames.CallbackEventData>)
extern void CallbackHandler_RegisterListener_m55292FC1E70AF8C33813BF095ED1743563CEB3A6 (void);
// 0x000000A6 System.Void DevionGames.CallbackHandler::RemoveListener(System.String,UnityEngine.Events.UnityAction`1<DevionGames.CallbackEventData>)
extern void CallbackHandler_RemoveListener_m3570400B68B31861CDC5A8DFF406BD682E462979 (void);
// 0x000000A7 System.Void DevionGames.CallbackHandler::.ctor()
extern void CallbackHandler__ctor_m38F9E3848D97076B2176CE3A250B7A06FA54402C (void);
// 0x000000A8 System.Void DevionGames.CallbackHandler/Entry::.ctor()
extern void Entry__ctor_mCDE5846E8210775A2FAADD13ACB1D160D62D7F68 (void);
// 0x000000A9 System.Void DevionGames.CallbackHandler/CallbackEvent::.ctor()
extern void CallbackEvent__ctor_mC44831337F8F4357BF97F7AF60D28AD739DBD5BB (void);
// 0x000000AA System.Void DevionGames.CameraEffects::Awake()
extern void CameraEffects_Awake_m7A8580FFBBCF266208F6A1210DBDB7E2E0B93840 (void);
// 0x000000AB System.Void DevionGames.CameraEffects::Shake(System.Single,System.Single,System.Nullable`1<UnityEngine.Vector3>,UnityEngine.Camera,System.Boolean,UnityEngine.AnimationCurve)
extern void CameraEffects_Shake_m59E63858224C18B9492984C3DCAFFA178E5BCA63 (void);
// 0x000000AC System.Void DevionGames.CameraEffects::LateUpdate()
extern void CameraEffects_LateUpdate_m74A49329FCD9859ED68BCCCA764C7D496FE7E6FA (void);
// 0x000000AD System.Void DevionGames.CameraEffects::ResetCamera()
extern void CameraEffects_ResetCamera_mD79732D1495C96B10A667B713DDBBFBF02618D6C (void);
// 0x000000AE System.Void DevionGames.CameraEffects::.ctor()
extern void CameraEffects__ctor_mAFF07E8066A65EE0759394674087CCC362815CEB (void);
// 0x000000AF System.Void DevionGames.CoroutineHandler::.ctor()
extern void CoroutineHandler__ctor_mF5C507E7C02299B8CF8B1301F1B65175CE76B8A2 (void);
// 0x000000B0 System.Void DevionGames.CoroutineQueue::.ctor(UnityEngine.MonoBehaviour)
extern void CoroutineQueue__ctor_mD42E0C96CBDCA3BECCE44B059FF9931D534BED7B (void);
// 0x000000B1 System.Void DevionGames.CoroutineQueue::Start()
extern void CoroutineQueue_Start_m5F152C6F896357B134331A796672AA1ABE02D8DB (void);
// 0x000000B2 System.Void DevionGames.CoroutineQueue::Stop()
extern void CoroutineQueue_Stop_m59583908A8FA22629E6723BCDAF087A9D2B3C4A6 (void);
// 0x000000B3 System.Void DevionGames.CoroutineQueue::EnqueueAction(System.Collections.IEnumerator)
extern void CoroutineQueue_EnqueueAction_mDE9AB402C6EFB6D399FE021E2D8801C640FFB23B (void);
// 0x000000B4 System.Collections.IEnumerator DevionGames.CoroutineQueue::Process()
extern void CoroutineQueue_Process_mA4F1B1CA22EBA0AC12050CE9D6CB58193071F76D (void);
// 0x000000B5 System.Void DevionGames.CoroutineQueue/<Process>d__7::.ctor(System.Int32)
extern void U3CProcessU3Ed__7__ctor_mA35CD4DE0A2227D3286F26FD712B83DABCFEBFA3 (void);
// 0x000000B6 System.Void DevionGames.CoroutineQueue/<Process>d__7::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__7_System_IDisposable_Dispose_m2839E313A558FE75EC388F0735F6E5BB247B5CBF (void);
// 0x000000B7 System.Boolean DevionGames.CoroutineQueue/<Process>d__7::MoveNext()
extern void U3CProcessU3Ed__7_MoveNext_m7F1CFCB6D49702368BF5344EDEEB891727021F20 (void);
// 0x000000B8 System.Object DevionGames.CoroutineQueue/<Process>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED87547CE0E935996285759923C65E910ADC90DC (void);
// 0x000000B9 System.Void DevionGames.CoroutineQueue/<Process>d__7::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__7_System_Collections_IEnumerator_Reset_m70CD4B91FCDAAF12A0674773E8D497436327CF60 (void);
// 0x000000BA System.Object DevionGames.CoroutineQueue/<Process>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__7_System_Collections_IEnumerator_get_Current_m2B21F63871756331688DBDD7C68F246498644548 (void);
// 0x000000BB System.Void DevionGames.DontDestroyOnLoad::Awake()
extern void DontDestroyOnLoad_Awake_m1BE38B436A6D7D5F86FCC5D12D9A8831919AF7A1 (void);
// 0x000000BC System.Void DevionGames.DontDestroyOnLoad::.ctor()
extern void DontDestroyOnLoad__ctor_m902572EEB23F8126AF1FBA4972B9F0E9F2C3CA09 (void);
// 0x000000BD System.Void DevionGames.EventHandler::.cctor()
extern void EventHandler__cctor_m1C087E3F6722E7ADCF20607B6FA2CA0773480668 (void);
// 0x000000BE System.Void DevionGames.EventHandler::Execute(System.String)
extern void EventHandler_Execute_mC9AEB7977041146F8430BA0A2533A962F11B722F (void);
// 0x000000BF System.Void DevionGames.EventHandler::Execute(System.Object,System.String)
extern void EventHandler_Execute_m3F598B00C6285C501830C69892DDA29578B6A48A (void);
// 0x000000C0 System.Void DevionGames.EventHandler::Execute(System.String,T1)
// 0x000000C1 System.Void DevionGames.EventHandler::Execute(System.Object,System.String,T1)
// 0x000000C2 System.Void DevionGames.EventHandler::Execute(System.String,T1,T2)
// 0x000000C3 System.Void DevionGames.EventHandler::Execute(System.Object,System.String,T1,T2)
// 0x000000C4 System.Void DevionGames.EventHandler::Execute(System.String,T1,T2,T3)
// 0x000000C5 System.Void DevionGames.EventHandler::Execute(System.Object,System.String,T1,T2,T3)
// 0x000000C6 System.Void DevionGames.EventHandler::Register(System.String,System.Action)
extern void EventHandler_Register_mCCE2DF1E7BF59E4EC7600F2F38E006E1F0E06D08 (void);
// 0x000000C7 System.Void DevionGames.EventHandler::Register(System.Object,System.String,System.Action)
extern void EventHandler_Register_m7887E2866B3323D8521D4F688ED985E9E6A4E197 (void);
// 0x000000C8 System.Void DevionGames.EventHandler::Register(System.String,System.Action`1<T1>)
// 0x000000C9 System.Void DevionGames.EventHandler::Register(System.Object,System.String,System.Action`1<T1>)
// 0x000000CA System.Void DevionGames.EventHandler::Register(System.String,System.Action`2<T1,T2>)
// 0x000000CB System.Void DevionGames.EventHandler::Register(System.Object,System.String,System.Action`2<T1,T2>)
// 0x000000CC System.Void DevionGames.EventHandler::Register(System.String,System.Action`3<T1,T2,T3>)
// 0x000000CD System.Void DevionGames.EventHandler::Register(System.Object,System.String,System.Action`3<T1,T2,T3>)
// 0x000000CE System.Void DevionGames.EventHandler::Unregister(System.String,System.Action)
extern void EventHandler_Unregister_m445BD7DFB081C8EEC51FD356F5F1EC4CE2FC32C5 (void);
// 0x000000CF System.Void DevionGames.EventHandler::Unregister(System.Object,System.String,System.Action)
extern void EventHandler_Unregister_m64A893E7AF43A68A836879B3F06904EA2F395BBD (void);
// 0x000000D0 System.Void DevionGames.EventHandler::Unregister(System.String,System.Action`1<T1>)
// 0x000000D1 System.Void DevionGames.EventHandler::Unregister(System.Object,System.String,System.Action`1<T1>)
// 0x000000D2 System.Void DevionGames.EventHandler::Unregister(System.String,System.Action`2<T1,T2>)
// 0x000000D3 System.Void DevionGames.EventHandler::Unregister(System.Object,System.String,System.Action`2<T1,T2>)
// 0x000000D4 System.Void DevionGames.EventHandler::Unregister(System.String,System.Action`3<T1,T2,T3>)
// 0x000000D5 System.Void DevionGames.EventHandler::Unregister(System.Object,System.String,System.Action`3<T1,T2,T3>)
// 0x000000D6 System.Void DevionGames.EventHandler::Register(System.String,System.Delegate)
extern void EventHandler_Register_mF5C710D25E7F28EB64A61F5FB1A38C7EBF017C40 (void);
// 0x000000D7 System.Void DevionGames.EventHandler::Register(System.Object,System.String,System.Delegate)
extern void EventHandler_Register_mC985925E6C3667D9B606F6B18215E905FB256EF9 (void);
// 0x000000D8 System.Void DevionGames.EventHandler::Unregister(System.String,System.Delegate)
extern void EventHandler_Unregister_m9A5AE7046FF18608D5E6E94893794632E82E0D44 (void);
// 0x000000D9 System.Void DevionGames.EventHandler::Unregister(System.Object,System.String,System.Delegate)
extern void EventHandler_Unregister_mFF5654B39FE8D8D171D93EAB64EB41C5885A33BB (void);
// 0x000000DA System.Delegate DevionGames.EventHandler::GetDelegate(System.String)
extern void EventHandler_GetDelegate_m4A5D1D11A804D1E1D77F0A1324BED1BAA00943C5 (void);
// 0x000000DB System.Delegate DevionGames.EventHandler::GetDelegate(System.Object,System.String)
extern void EventHandler_GetDelegate_m8EAB69B7F7093E06A85CA419B151777D81F51E32 (void);
// 0x000000DC System.Void DevionGames.EventHandler::.ctor()
extern void EventHandler__ctor_m998EE8D7FFDEB7156A2FBB7A7108E64D57E97F7F (void);
// 0x000000DD System.Int32 DevionGames.IIdentity::get_ID()
// 0x000000DE System.Void DevionGames.IIdentity::set_ID(System.Int32)
// 0x000000DF System.String DevionGames.INameable::get_Name()
// 0x000000E0 System.Void DevionGames.INameable::set_Name(System.String)
// 0x000000E1 System.Boolean DevionGames.ISelectable::get_enabled()
// 0x000000E2 UnityEngine.Vector3 DevionGames.ISelectable::get_position()
// 0x000000E3 System.Void DevionGames.ISelectable::OnSelect()
// 0x000000E4 System.Void DevionGames.ISelectable::OnDeselect()
// 0x000000E5 System.Void DevionGames.IJsonSerializable::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000E6 System.Void DevionGames.IJsonSerializable::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
// 0x000000E7 System.String DevionGames.JsonSerializer::Serialize(DevionGames.IJsonSerializable[])
extern void JsonSerializer_Serialize_mA0040BF435F812CBD17F5D2773B87127901F3E3F (void);
// 0x000000E8 System.Void DevionGames.JsonSerializer::Deserialize(System.String,DevionGames.IJsonSerializable[])
extern void JsonSerializer_Deserialize_m8A5ADBADE2B4A2A8BC6BDFC954FAC8C2A57E7C5C (void);
// 0x000000E9 System.Collections.Generic.List`1<T> DevionGames.JsonSerializer::Deserialize(System.String)
// 0x000000EA System.String DevionGames.JsonSerializer::Serialize(DevionGames.IJsonSerializable)
extern void JsonSerializer_Serialize_mEE0C3E0547ADB4A9D57FE3533B3E0E500791AEA0 (void);
// 0x000000EB System.Void DevionGames.JsonSerializer::Deserialize(System.String,DevionGames.IJsonSerializable)
extern void JsonSerializer_Deserialize_m222767516257F841D1E140C9263B2145E393BB74 (void);
// 0x000000EC System.Object DevionGames.MiniJSON::Deserialize(System.String)
extern void MiniJSON_Deserialize_mC78A07565C97B728803C8AE751D7583B7E5A4989 (void);
// 0x000000ED System.String DevionGames.MiniJSON::Serialize(System.Object)
extern void MiniJSON_Serialize_m0EAC236F1654F6BA91D0955E75321F084697656D (void);
// 0x000000EE System.Boolean DevionGames.MiniJSON/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mECF1CFB9E4D5DBBA923A834C01FBEEFEA2B27EE1 (void);
// 0x000000EF System.Void DevionGames.MiniJSON/Parser::.ctor(System.String)
extern void Parser__ctor_m9E21D017E4747A4045DC07B29A764E7411BF449F (void);
// 0x000000F0 System.Object DevionGames.MiniJSON/Parser::Parse(System.String)
extern void Parser_Parse_m6A83FEC068F710E9DB5F7D6E50CBE434C2E0ABFD (void);
// 0x000000F1 System.Void DevionGames.MiniJSON/Parser::Dispose()
extern void Parser_Dispose_m2AFDED3B409174744C14C82FB55315FC25183264 (void);
// 0x000000F2 System.Collections.Generic.Dictionary`2<System.String,System.Object> DevionGames.MiniJSON/Parser::ParseObject()
extern void Parser_ParseObject_mA45820E6DC2BADFBE31B079E08795EEE018B2E89 (void);
// 0x000000F3 System.Collections.Generic.List`1<System.Object> DevionGames.MiniJSON/Parser::ParseArray()
extern void Parser_ParseArray_m23BD84E2D9D97A3253919389BC7D6D5845A52A3B (void);
// 0x000000F4 System.Object DevionGames.MiniJSON/Parser::ParseValue()
extern void Parser_ParseValue_mAA9616FDB99E62E648953BA0F555EED59C294795 (void);
// 0x000000F5 System.Object DevionGames.MiniJSON/Parser::ParseByToken(DevionGames.MiniJSON/Parser/TOKEN)
extern void Parser_ParseByToken_m643F5C2C8468B0850F138A844EF088B2547D2793 (void);
// 0x000000F6 System.String DevionGames.MiniJSON/Parser::ParseString()
extern void Parser_ParseString_mFD5FED77AE3FB01681A47B921AC47FD1B7EA9CB4 (void);
// 0x000000F7 System.Object DevionGames.MiniJSON/Parser::ParseNumber()
extern void Parser_ParseNumber_mFD6EFB7880D7DCF8B83E1635A250E86FA02B8BD1 (void);
// 0x000000F8 UnityEngine.Quaternion DevionGames.MiniJSON/Parser::ToQuaternion(System.String)
extern void Parser_ToQuaternion_mA1ED062403850989FFA1C75AF1512E3E8233B382 (void);
// 0x000000F9 UnityEngine.Vector4 DevionGames.MiniJSON/Parser::ToVector4(System.String)
extern void Parser_ToVector4_m1ABB680D64963C605FF8BE6F878A3FC2417262CA (void);
// 0x000000FA UnityEngine.Vector3 DevionGames.MiniJSON/Parser::ToVector3(System.String)
extern void Parser_ToVector3_mEADF1FA0067DB25EF2CD48F91381E90615D00567 (void);
// 0x000000FB UnityEngine.Vector2 DevionGames.MiniJSON/Parser::ToVector2(System.String)
extern void Parser_ToVector2_m390A4978445E5959A32772A1F7A2802FD015204D (void);
// 0x000000FC UnityEngine.Color DevionGames.MiniJSON/Parser::ToColor(System.String)
extern void Parser_ToColor_mC3FA3801EAF7A42BF30ED3CD8AE20F0BA64418A8 (void);
// 0x000000FD System.Void DevionGames.MiniJSON/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mF9C4A1E38EE3D8D232D2DC0D2BA149DEC8EA7F00 (void);
// 0x000000FE System.Char DevionGames.MiniJSON/Parser::get_PeekChar()
extern void Parser_get_PeekChar_mDDCA1D7C1FD96741D077A8E68E7770D61D8D8315 (void);
// 0x000000FF System.Char DevionGames.MiniJSON/Parser::get_NextChar()
extern void Parser_get_NextChar_mE2E741E78DD5EF0F3CD5D2823F5FA3EA68F4864D (void);
// 0x00000100 System.String DevionGames.MiniJSON/Parser::get_NextWord()
extern void Parser_get_NextWord_mBD502DD147AADCCD19927A2388F0830574D48931 (void);
// 0x00000101 DevionGames.MiniJSON/Parser/TOKEN DevionGames.MiniJSON/Parser::get_NextToken()
extern void Parser_get_NextToken_mE6664A7BD810EB40C908E2A5AE2B374F8F586367 (void);
// 0x00000102 System.Void DevionGames.MiniJSON/Serializer::.ctor()
extern void Serializer__ctor_mD528CF21C57D1C4DE43B3DCCDA0B04F000E88AD2 (void);
// 0x00000103 System.String DevionGames.MiniJSON/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mE20B6AFB56DB0959B654675580B767967F05866E (void);
// 0x00000104 System.Void DevionGames.MiniJSON/Serializer::SerializeValue(System.Object,System.Int32)
extern void Serializer_SerializeValue_m95C4388683E1A911F366629C73DDFD049EF61F07 (void);
// 0x00000105 System.Void DevionGames.MiniJSON/Serializer::SerializeObject(System.Collections.IDictionary,System.Int32)
extern void Serializer_SerializeObject_m48BD0BAEC64340715EDC1F1DD92AB21DEE6BC534 (void);
// 0x00000106 System.Void DevionGames.MiniJSON/Serializer::SerializeArray(System.Collections.IList,System.Int32)
extern void Serializer_SerializeArray_mD070FE3C3CE50FD6D76FAFBE9F6D7DA00707C4E6 (void);
// 0x00000107 System.Void DevionGames.MiniJSON/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m911D7DACEB983B330B8E9E9D916CDA770225B639 (void);
// 0x00000108 System.Void DevionGames.MiniJSON/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mE7199EA4878583CFDD5484147F5DAF04E2EC693D (void);
// 0x00000109 System.Void DevionGames.LookAtMainCamera::Start()
extern void LookAtMainCamera_Start_mF924C7E9DE9870FA2C9ADF998A1B3E69607C835B (void);
// 0x0000010A System.Void DevionGames.LookAtMainCamera::Update()
extern void LookAtMainCamera_Update_m53CBDB27D41652D8FE5D061DDFCDF4D79D3C8494 (void);
// 0x0000010B System.Collections.IEnumerator DevionGames.LookAtMainCamera::SearchCamera()
extern void LookAtMainCamera_SearchCamera_m6FA58F13F9317393645F50E5496431BE77B0993B (void);
// 0x0000010C System.Void DevionGames.LookAtMainCamera::.ctor()
extern void LookAtMainCamera__ctor_mA1EAFCDCA283CBAD6233C9952BB60E30B5F44D43 (void);
// 0x0000010D System.Void DevionGames.LookAtMainCamera/<SearchCamera>d__6::.ctor(System.Int32)
extern void U3CSearchCameraU3Ed__6__ctor_m05514C886D4BDBB39C51CAF3FA8ABD053B9F8EAC (void);
// 0x0000010E System.Void DevionGames.LookAtMainCamera/<SearchCamera>d__6::System.IDisposable.Dispose()
extern void U3CSearchCameraU3Ed__6_System_IDisposable_Dispose_m18B8EAE4684219FCB4D38506E56021D26E4BD8CE (void);
// 0x0000010F System.Boolean DevionGames.LookAtMainCamera/<SearchCamera>d__6::MoveNext()
extern void U3CSearchCameraU3Ed__6_MoveNext_m04308AC6164FB0730FD9AE33BFC2E106D1A16D6C (void);
// 0x00000110 System.Object DevionGames.LookAtMainCamera/<SearchCamera>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSearchCameraU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88445D17B8BB4050E4C069580E8637BD7971F47A (void);
// 0x00000111 System.Void DevionGames.LookAtMainCamera/<SearchCamera>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_Reset_m9E922F1F4C94E1398E5CFD22EF6A07F03A9BF7C8 (void);
// 0x00000112 System.Object DevionGames.LookAtMainCamera/<SearchCamera>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_get_Current_mFD0288E149200F0FD3B5715E4E00F236506B7782 (void);
// 0x00000113 System.Void DevionGames.MoveForward::Start()
extern void MoveForward_Start_mD6B6A4B01FCE24887297DCD57D5FAC9EF0B94942 (void);
// 0x00000114 System.Void DevionGames.MoveForward::FixedUpdate()
extern void MoveForward_FixedUpdate_m02871A13C36E5951EB3F8E8DE3612BE8048C213B (void);
// 0x00000115 System.Void DevionGames.MoveForward::.ctor()
extern void MoveForward__ctor_m688707D09CC0688B5B181DBCE6019BAF29B0CE29 (void);
// 0x00000116 System.Void DevionGames.MoveTo::Start()
extern void MoveTo_Start_mEE605C609529C147B27928DD04929094F3DB69AC (void);
// 0x00000117 System.Void DevionGames.MoveTo::Update()
extern void MoveTo_Update_m26E450C3D432BD61053A48B0D115E55C6E7DF9E6 (void);
// 0x00000118 System.Void DevionGames.MoveTo::.ctor()
extern void MoveTo__ctor_m61258B00F90817B6A436C7EF231890501C6E8F8C (void);
// 0x00000119 System.String DevionGames.NamedVariable::get_Name()
extern void NamedVariable_get_Name_mB1B05159CFD28BA66C8F550375E709B061370F35 (void);
// 0x0000011A System.Void DevionGames.NamedVariable::set_Name(System.String)
extern void NamedVariable_set_Name_mB7FF12474431E697C37281C8136ED3ACB0EB4084 (void);
// 0x0000011B System.String DevionGames.NamedVariable::get_Description()
extern void NamedVariable_get_Description_m9C53F0D71B95A7EA12CBE50A68F2740CEACDEC2D (void);
// 0x0000011C System.Void DevionGames.NamedVariable::set_Description(System.String)
extern void NamedVariable_set_Description_mA1012D042EC37718286252FC8D9E7FB0268BEEB1 (void);
// 0x0000011D DevionGames.NamedVariableType DevionGames.NamedVariable::get_VariableType()
extern void NamedVariable_get_VariableType_mDF1A4194B6EC84515268C73DA0BE406E4814FD87 (void);
// 0x0000011E System.Void DevionGames.NamedVariable::set_VariableType(DevionGames.NamedVariableType)
extern void NamedVariable_set_VariableType_m5E8D3355D0075984B9C2DE2FEDA2CB336B942309 (void);
// 0x0000011F System.Type DevionGames.NamedVariable::get_ValueType()
extern void NamedVariable_get_ValueType_m59DF0FA7B1F08A9640CE8B60D8864399D15423A2 (void);
// 0x00000120 System.String[] DevionGames.NamedVariable::get_VariableTypeNames()
extern void NamedVariable_get_VariableTypeNames_mDB7E697BEE2748C30A75872CDA5C26DA39F69F1E (void);
// 0x00000121 System.Object DevionGames.NamedVariable::GetValue()
extern void NamedVariable_GetValue_mA3BC0A2540ECDEE87309ABF351416621D031FABC (void);
// 0x00000122 System.Void DevionGames.NamedVariable::SetValue(System.Object)
extern void NamedVariable_SetValue_m980997E62410434EA5E9FE2EE3FE6F73E4C1787A (void);
// 0x00000123 System.String DevionGames.NamedVariable::get_PropertyPath()
extern void NamedVariable_get_PropertyPath_mA72116852B01683CC6EE5654DD6FE351683F61C5 (void);
// 0x00000124 System.Void DevionGames.NamedVariable::.ctor()
extern void NamedVariable__ctor_m59AFD128613287C47562D278F1C640E877D77DCF (void);
// 0x00000125 System.String DevionGames.ObjectProperty::get_Name()
extern void ObjectProperty_get_Name_m5AB1BCDC2216FC182720615EAE3551E1199DF232 (void);
// 0x00000126 System.Void DevionGames.ObjectProperty::set_Name(System.String)
extern void ObjectProperty_set_Name_mAF9740B014A461FB4FF620FAF587D2BA2B073BCC (void);
// 0x00000127 System.Type DevionGames.ObjectProperty::get_SerializedType()
extern void ObjectProperty_get_SerializedType_m99A5E6A06E67E9F1EAD559B65CFD4217741CDE53 (void);
// 0x00000128 System.Object DevionGames.ObjectProperty::GetValue()
extern void ObjectProperty_GetValue_mC5A96CC3D724D54BE0FF296AFE745F06768F716A (void);
// 0x00000129 System.Void DevionGames.ObjectProperty::SetValue(System.Object)
extern void ObjectProperty_SetValue_m71B8393BB54C121AFA7F9516BEF266CAFA6F67CC (void);
// 0x0000012A System.String DevionGames.ObjectProperty::GetPropertyName(System.Type)
extern void ObjectProperty_GetPropertyName_mEC5F0B3B118A2598E31B87A13050267F2B2D7BCD (void);
// 0x0000012B System.String DevionGames.ObjectProperty::ToString(System.String)
extern void ObjectProperty_ToString_mFCED8D0E0901C40F68D9FBF4A9E5CFEC78CABFB6 (void);
// 0x0000012C System.Type[] DevionGames.ObjectProperty::get_SupportedTypes()
extern void ObjectProperty_get_SupportedTypes_mE3D86A9F079FC5C4A8C172C822D5E187DD4879D9 (void);
// 0x0000012D System.String[] DevionGames.ObjectProperty::get_DisplayNames()
extern void ObjectProperty_get_DisplayNames_m7517213FDD438E91561A80CE50AD9EB67E8276CE (void);
// 0x0000012E System.Void DevionGames.ObjectProperty::.ctor()
extern void ObjectProperty__ctor_mB2C319288BF72C2C7E033E487370807D78A98949 (void);
// 0x0000012F System.Collections.IEnumerator DevionGames.PlayAudioClip::Start()
extern void PlayAudioClip_Start_m76002B53FCC6F1E3B598DE5FF7898B38593FE0B2 (void);
// 0x00000130 System.Void DevionGames.PlayAudioClip::.ctor()
extern void PlayAudioClip__ctor_mD1B79D1717E52DBF15540E77493F379D0EF6E692 (void);
// 0x00000131 System.Void DevionGames.PlayAudioClip/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mEF45D7D81D28D0AD72ADE751ADF86D4F214D0CEE (void);
// 0x00000132 System.Void DevionGames.PlayAudioClip/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m36730405D9679212EDBA4DF350C053A8EF8B4062 (void);
// 0x00000133 System.Boolean DevionGames.PlayAudioClip/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mBF50C82120DE3C36EF092188B9E2220C7FF8DACC (void);
// 0x00000134 System.Object DevionGames.PlayAudioClip/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m862A8A274013AD886436DFF6E0E832D3CDF8584F (void);
// 0x00000135 System.Void DevionGames.PlayAudioClip/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m933C41702153F8200693D84ACFA4E211767628BA (void);
// 0x00000136 System.Object DevionGames.PlayAudioClip/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA428A2D99B93A9A2EE31DBF3B6027A222715320D (void);
// 0x00000137 System.Void DevionGames.PlayerInfo::.ctor(System.String)
extern void PlayerInfo__ctor_m36A300EA30D61D83F022B43FACF0B5681B50A8D7 (void);
// 0x00000138 UnityEngine.GameObject DevionGames.PlayerInfo::get_gameObject()
extern void PlayerInfo_get_gameObject_mCDC9229223B46F65319EF98553E3AC88FBA66192 (void);
// 0x00000139 UnityEngine.Transform DevionGames.PlayerInfo::get_transform()
extern void PlayerInfo_get_transform_m506963C45530B19D4FEC4C1E348604F46E58CC39 (void);
// 0x0000013A UnityEngine.Collider DevionGames.PlayerInfo::get_collider()
extern void PlayerInfo_get_collider_m85917D3BC5BF4F56E82EE592DD4A8EF21591C79E (void);
// 0x0000013B UnityEngine.Collider2D DevionGames.PlayerInfo::get_collider2D()
extern void PlayerInfo_get_collider2D_m74B061ACCA931A5D38B6314729FDDFB5A7486851 (void);
// 0x0000013C UnityEngine.Animator DevionGames.PlayerInfo::get_animator()
extern void PlayerInfo_get_animator_mC90B904FD5F892750E5D3559BE22A791EF3774B1 (void);
// 0x0000013D UnityEngine.Bounds DevionGames.PlayerInfo::get_bounds()
extern void PlayerInfo_get_bounds_m92571E2218491825149E7ADBFD1B62D96D3AB428 (void);
// 0x0000013E System.Void DevionGames.PropertyBinding::Start()
extern void PropertyBinding_Start_m81E90CF45638C8FD3D2B094F66139EC799CF9A99 (void);
// 0x0000013F System.Void DevionGames.PropertyBinding::Update()
extern void PropertyBinding_Update_m3E4C42EAB7BCA6F7C55A78892B80E4D4B3EE1A5B (void);
// 0x00000140 System.Void DevionGames.PropertyBinding::LateUpdate()
extern void PropertyBinding_LateUpdate_m3041E2B2D7E66CF688AAE696778D8CAC3A795963 (void);
// 0x00000141 System.Void DevionGames.PropertyBinding::FixedUpdate()
extern void PropertyBinding_FixedUpdate_mE1AE66D9FDA57365C9BDF3EE3C96FA76FC8281AB (void);
// 0x00000142 System.Collections.IEnumerator DevionGames.PropertyBinding::IntervalUpdate()
extern void PropertyBinding_IntervalUpdate_mF7DB2D8DB3B459788E759EED9F73261399142692 (void);
// 0x00000143 System.Void DevionGames.PropertyBinding::UpdateTarget()
extern void PropertyBinding_UpdateTarget_mCB3D5E0E8DC93BD950965465AE97EC0CA8D839E0 (void);
// 0x00000144 System.Void DevionGames.PropertyBinding::.ctor()
extern void PropertyBinding__ctor_mEB0047074BD9AA8422495DC076267CB4297C4FD6 (void);
// 0x00000145 UnityEngine.Component DevionGames.PropertyBinding/PropertyRef::get_component()
extern void PropertyRef_get_component_mDA66F92BEA0D5FF5D3CC12B7563C9F2ADF8592C0 (void);
// 0x00000146 System.String DevionGames.PropertyBinding/PropertyRef::get_propertyPath()
extern void PropertyRef_get_propertyPath_m42D396F55CF08FBF7DADC8DF5DBE0D82EC4E9697 (void);
// 0x00000147 System.Object DevionGames.PropertyBinding/PropertyRef::GetValue()
extern void PropertyRef_GetValue_mE4F59D47453AE94DC2001E468AC359E47E848FEA (void);
// 0x00000148 System.Boolean DevionGames.PropertyBinding/PropertyRef::SetValue(System.Object)
extern void PropertyRef_SetValue_m135EB1AB70F97E3E94E993144F1C4DDF83B43CC6 (void);
// 0x00000149 System.Boolean DevionGames.PropertyBinding/PropertyRef::CacheProperty()
extern void PropertyRef_CacheProperty_m1DBB883A938AB5FD747C8A8EFD3A618B26F02107 (void);
// 0x0000014A System.Void DevionGames.PropertyBinding/PropertyRef::.ctor()
extern void PropertyRef__ctor_m913CD13E033BDECCA9B096D69814E24644AEB595 (void);
// 0x0000014B System.Void DevionGames.PropertyBinding/<IntervalUpdate>d__8::.ctor(System.Int32)
extern void U3CIntervalUpdateU3Ed__8__ctor_mE569C36E5154F28C9464783AB286F1E5D95562AC (void);
// 0x0000014C System.Void DevionGames.PropertyBinding/<IntervalUpdate>d__8::System.IDisposable.Dispose()
extern void U3CIntervalUpdateU3Ed__8_System_IDisposable_Dispose_m938B36B9E9E68F9FE94E5F6031E53984C29619D6 (void);
// 0x0000014D System.Boolean DevionGames.PropertyBinding/<IntervalUpdate>d__8::MoveNext()
extern void U3CIntervalUpdateU3Ed__8_MoveNext_mCB18228CAB537933E04FBC9F2BCF7CBC745AEACC (void);
// 0x0000014E System.Object DevionGames.PropertyBinding/<IntervalUpdate>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIntervalUpdateU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69063FDBF5B4831E0C85C71F09F5244D00998219 (void);
// 0x0000014F System.Void DevionGames.PropertyBinding/<IntervalUpdate>d__8::System.Collections.IEnumerator.Reset()
extern void U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_Reset_m158FF84C726C4B2D5EE0C60A67E4A41B6595A38C (void);
// 0x00000150 System.Object DevionGames.PropertyBinding/<IntervalUpdate>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_get_Current_mEA80CCCA18AB0E4401F2CC817C77857E79112045 (void);
// 0x00000151 System.Void DevionGames.SaveUIComponent::Awake()
extern void SaveUIComponent_Awake_mAFE026354C38B1190D5F942CD40B07BBF353B0F1 (void);
// 0x00000152 System.Void DevionGames.SaveUIComponent::SaveFloat(System.Single)
extern void SaveUIComponent_SaveFloat_mDF05346AF9A6C4CCF1D32EA3873E62C06D27D0EB (void);
// 0x00000153 System.Single DevionGames.SaveUIComponent::LoadFloat(System.Single)
extern void SaveUIComponent_LoadFloat_m932BEF92B3EDEF3A3463BB7F3A3684BA0C30164D (void);
// 0x00000154 System.Void DevionGames.SaveUIComponent::SaveInt(System.Int32)
extern void SaveUIComponent_SaveInt_mD59DB6A1B116A2D806D4F2DFAEDA0FBE20FDE0EB (void);
// 0x00000155 System.Int32 DevionGames.SaveUIComponent::LoadInt(System.Int32)
extern void SaveUIComponent_LoadInt_m3015EE758718251772CB74B6CCC896F89216A7A1 (void);
// 0x00000156 System.Void DevionGames.SaveUIComponent::SaveString(System.String)
extern void SaveUIComponent_SaveString_m08D3A82C30EFAC91D3312612A67FC41F205CA14D (void);
// 0x00000157 System.String DevionGames.SaveUIComponent::LoadString(System.String)
extern void SaveUIComponent_LoadString_mB9757D06EF34727F70404A972849E0252B60D441 (void);
// 0x00000158 System.Void DevionGames.SaveUIComponent::SaveBool(System.Boolean)
extern void SaveUIComponent_SaveBool_m02363995C5FD0958AAC0A0B777F68C42B42718BE (void);
// 0x00000159 System.Boolean DevionGames.SaveUIComponent::LoadBool(System.Boolean)
extern void SaveUIComponent_LoadBool_mD2CD7C2337FDAC49F5E5043FE3761FE68DF5BFC6 (void);
// 0x0000015A System.Void DevionGames.SaveUIComponent::OnValidate()
extern void SaveUIComponent_OnValidate_m1EB8EFAB390B009E9468EFF051D7B5AD963DDC3B (void);
// 0x0000015B System.Void DevionGames.SaveUIComponent::.ctor()
extern void SaveUIComponent__ctor_mB3FE1C719D07D7A2873F1A42F1B7D2B118B8E82C (void);
// 0x0000015C UnityEngine.Vector3 DevionGames.SelectableObject::get_position()
extern void SelectableObject_get_position_m5175DBA2D06D0FB2B3A819D8D0F0551B7B7FC4F7 (void);
// 0x0000015D System.String[] DevionGames.SelectableObject::get_Callbacks()
extern void SelectableObject_get_Callbacks_m5427B5B0F58943BF078830E59F92355297188893 (void);
// 0x0000015E System.Void DevionGames.SelectableObject::Awake()
extern void SelectableObject_Awake_mC4B94D44BFA88AF36A95D4869CE71730AF07BDB2 (void);
// 0x0000015F System.Void DevionGames.SelectableObject::Start()
extern void SelectableObject_Start_m800CCD90AF2309DE1AF50BBF14CC6DC334186EB8 (void);
// 0x00000160 System.Void DevionGames.SelectableObject::OnSelect()
extern void SelectableObject_OnSelect_mF2B15842F9903F81F4637BE61F413C8BC359D620 (void);
// 0x00000161 System.Void DevionGames.SelectableObject::OnDeselect()
extern void SelectableObject_OnDeselect_mFDBCD4FA9D25D79F9093BEC52E94B8255E11B878 (void);
// 0x00000162 System.Void DevionGames.SelectableObject::OnDestroy()
extern void SelectableObject_OnDestroy_mC5C5BDC7B1BDA308E3834AC23A50F70F620C9792 (void);
// 0x00000163 System.Void DevionGames.SelectableObject::.ctor()
extern void SelectableObject__ctor_mB97342D85B2894F200C3EC7FF0B54A221FE419CF (void);
// 0x00000164 System.Boolean DevionGames.SelectableObject::DevionGames.ISelectable.get_enabled()
extern void SelectableObject_DevionGames_ISelectable_get_enabled_m00062F17363403BC0B43D5852069EC7BC0C81413 (void);
// 0x00000165 System.Void DevionGames.SelectableObjectName::Start()
extern void SelectableObjectName_Start_m4B5E0D01F140AD01DAF863876054348F5F99D185 (void);
// 0x00000166 System.Void DevionGames.SelectableObjectName::Update()
extern void SelectableObjectName_Update_mD829B5C3E085E6967299027720E783583F0DCF6A (void);
// 0x00000167 System.Void DevionGames.SelectableObjectName::.ctor()
extern void SelectableObjectName__ctor_m65E62BA557054C158F811B640C781CD4C9498CB1 (void);
// 0x00000168 System.String[] DevionGames.SelectionHandler::get_Callbacks()
extern void SelectionHandler_get_Callbacks_m0A3B8C870F5273546093F68C73DF3D1DFFFC67C2 (void);
// 0x00000169 System.Void DevionGames.SelectionHandler::Start()
extern void SelectionHandler_Start_m7EEE663D63DDF6D4CB4FA665A79B06728BD62A3D (void);
// 0x0000016A System.Void DevionGames.SelectionHandler::CustomUpdate()
extern void SelectionHandler_CustomUpdate_mD3293975B972B1EA362FE03EAA6675E6C15999F2 (void);
// 0x0000016B System.Void DevionGames.SelectionHandler::Update()
extern void SelectionHandler_Update_m876D4FD371D197BCF4370CA2695D19AC8458248F (void);
// 0x0000016C System.Boolean DevionGames.SelectionHandler::TrySelect(UnityEngine.Ray)
extern void SelectionHandler_TrySelect_m1D37BD0E0189B345E9C6F1763715E9ADECD3C68D (void);
// 0x0000016D System.Void DevionGames.SelectionHandler::Select(DevionGames.ISelectable)
extern void SelectionHandler_Select_mC8156B81B12567D0CE50F95AC1ABDCFF9B8EE1F2 (void);
// 0x0000016E System.Void DevionGames.SelectionHandler::Deselect()
extern void SelectionHandler_Deselect_m8EB4557FEBBF0C2DC4759BCEF2FE92E9C08CE1A1 (void);
// 0x0000016F DevionGames.ISelectable DevionGames.SelectionHandler::GetBestSelectable(System.Collections.Generic.IEnumerable`1<DevionGames.ISelectable>)
extern void SelectionHandler_GetBestSelectable_m49716D029E39A18B370FCC979561FCE550064C15 (void);
// 0x00000170 System.Void DevionGames.SelectionHandler::.ctor()
extern void SelectionHandler__ctor_m57683EAA1E9336236A73B2611425E3315D4A07EE (void);
// 0x00000171 System.Void DevionGames.SelectionHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_mE0F5E1191886E2BD2C53EF4EF4B89643BE9D5A7F (void);
// 0x00000172 System.Void DevionGames.SelectionHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_m05D278ED51668A833F88B459E7BDF940777CDC96 (void);
// 0x00000173 DevionGames.ISelectable DevionGames.SelectionHandler/<>c::<Update>b__18_0(UnityEngine.RaycastHit)
extern void U3CU3Ec_U3CUpdateU3Eb__18_0_mD3124141D66FD419F997B75BCB196A2A38819E4F (void);
// 0x00000174 System.Boolean DevionGames.SelectionHandler/<>c::<Update>b__18_1(DevionGames.ISelectable)
extern void U3CU3Ec_U3CUpdateU3Eb__18_1_m5CD999EC3F470EEF00E107E2051EA1FF9A104BC0 (void);
// 0x00000175 System.String[] DevionGames.SetCursorLockState::get_Callbacks()
extern void SetCursorLockState_get_Callbacks_mA96DBE6A6E33D577180D2317CAA708A189858A90 (void);
// 0x00000176 System.Void DevionGames.SetCursorLockState::Update()
extern void SetCursorLockState_Update_m8979183533008CB6D34FD9BBA8680DAF3A417B4A (void);
// 0x00000177 System.Void DevionGames.SetCursorLockState::.ctor()
extern void SetCursorLockState__ctor_mAC9FBD0F3C1F5C10371B384AE6681CEE1374F0E3 (void);
// 0x00000178 System.Void DevionGames.SimpleMinimap::Start()
extern void SimpleMinimap_Start_m5AA95357A330718AD811887A009D326A4E82E659 (void);
// 0x00000179 System.Void DevionGames.SimpleMinimap::Update()
extern void SimpleMinimap_Update_mC4FFD1BBDC82AF34A21B4F44ABA5D999BBC9CA67 (void);
// 0x0000017A System.Void DevionGames.SimpleMinimap::.ctor()
extern void SimpleMinimap__ctor_mD458A021ED64D84B9D1A758EFAB75B9FEFC0B800 (void);
// 0x0000017B System.Void DevionGames.SingleInstance::Awake()
extern void SingleInstance_Awake_m36F22DE7EBFB81C12CAE0EAED86890FBEB7843FC (void);
// 0x0000017C System.Collections.Generic.List`1<UnityEngine.GameObject> DevionGames.SingleInstance::GetInstanceObjects()
extern void SingleInstance_GetInstanceObjects_m1411E97ED72361656A6819348A8167F1F8488751 (void);
// 0x0000017D System.Void DevionGames.SingleInstance::.ctor()
extern void SingleInstance__ctor_mEE8E06661154B077A7D15FED40C59DD383AA9628 (void);
// 0x0000017E System.Void DevionGames.SingleInstance::.cctor()
extern void SingleInstance__cctor_m5AD86C4A6E93F0C198B8D98F5EC5D3553B228E29 (void);
// 0x0000017F System.Void DevionGames.SingleInstance/<>c::.cctor()
extern void U3CU3Ec__cctor_mD36BDDBDB840EE359E9B7AAE0EF7A6B9887B8879 (void);
// 0x00000180 System.Void DevionGames.SingleInstance/<>c::.ctor()
extern void U3CU3Ec__ctor_mF9CDE6BA7A32788543E76E76BF7828A1AAB72E10 (void);
// 0x00000181 System.Boolean DevionGames.SingleInstance/<>c::<GetInstanceObjects>b__2_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CGetInstanceObjectsU3Eb__2_0_m602557116183F00C23ED1CD20740DDCC92FF8CB7 (void);
// 0x00000182 System.Collections.IEnumerator DevionGames.TimedDestroy::Start()
extern void TimedDestroy_Start_mEE92664FA2142DA2498F6CA5D17A402CB0EF473D (void);
// 0x00000183 System.Void DevionGames.TimedDestroy::.ctor()
extern void TimedDestroy__ctor_mAA2D1CCB0F16B25360FF396D5D50450C88753BDA (void);
// 0x00000184 System.Void DevionGames.TimedDestroy/<Start>d__2::.ctor(System.Int32)
extern void U3CStartU3Ed__2__ctor_m7225B152290395438805F0521BA532F23E6B00E1 (void);
// 0x00000185 System.Void DevionGames.TimedDestroy/<Start>d__2::System.IDisposable.Dispose()
extern void U3CStartU3Ed__2_System_IDisposable_Dispose_mAE86168E862DE97101A48AFA3587AADA5E43F452 (void);
// 0x00000186 System.Boolean DevionGames.TimedDestroy/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m6494F59687CA972FBE5F523571F420D94BA909AF (void);
// 0x00000187 System.Object DevionGames.TimedDestroy/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD36C0E919D3C31F79BBCC4389881C2BED18A196C (void);
// 0x00000188 System.Void DevionGames.TimedDestroy/<Start>d__2::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mE031A47C4C3F5D56B6551FCC569394B5E232A9CD (void);
// 0x00000189 System.Object DevionGames.TimedDestroy/<Start>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m3E74EBF644324A5D42FAF8353F61C09FC047608A (void);
// 0x0000018A System.Void DevionGames.TimedEnable::OnEnable()
extern void TimedEnable_OnEnable_m516AFD10DD3C1231F03DACDB7135E7A2EAA6F6CC (void);
// 0x0000018B System.Collections.IEnumerator DevionGames.TimedEnable::WaitAndSetEnabled()
extern void TimedEnable_WaitAndSetEnabled_m5518E3F9CECA33EB77D46E4D85B003C30614045C (void);
// 0x0000018C System.Void DevionGames.TimedEnable::.ctor()
extern void TimedEnable__ctor_m27BE87FDAEE2D0BDF39FC9209DEE5AC2FB69D70D (void);
// 0x0000018D System.Void DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::.ctor(System.Int32)
extern void U3CWaitAndSetEnabledU3Ed__4__ctor_mC300D871D35BC85F42A31B653EC7EB79ECC920CA (void);
// 0x0000018E System.Void DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::System.IDisposable.Dispose()
extern void U3CWaitAndSetEnabledU3Ed__4_System_IDisposable_Dispose_mA75D4D3AC769C6B475A840A7950BBBC00120B27F (void);
// 0x0000018F System.Boolean DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::MoveNext()
extern void U3CWaitAndSetEnabledU3Ed__4_MoveNext_mDF45F77E9D4E16CBC8B50F7CAEF0045B3A8028E9 (void);
// 0x00000190 System.Object DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndSetEnabledU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB87EB7D92D24427B6B9AFDACCC861D3682416F68 (void);
// 0x00000191 System.Void DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_Reset_m1A49C73502745B49FD532606CA5CFFCA5E644573 (void);
// 0x00000192 System.Object DevionGames.TimedEnable/<WaitAndSetEnabled>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_get_Current_m06302F4BD4A6E352124BFED7190F24F7B4069423 (void);
// 0x00000193 DevionGames.CoroutineHandler DevionGames.UnityTools::get_Handler()
extern void UnityTools_get_Handler_mFF9B9477D3F507C0999C7DBEF92FE6C165FD8809 (void);
// 0x00000194 System.Void DevionGames.UnityTools::PlaySound(UnityEngine.AudioClip,System.Single,UnityEngine.Audio.AudioMixerGroup)
extern void UnityTools_PlaySound_mD3B4B43903581B68713DAB83C2D67A1D1D77C54B (void);
// 0x00000195 System.Boolean DevionGames.UnityTools::IsPointerOverUI()
extern void UnityTools_IsPointerOverUI_mD2D60DB893A1AEC2AB2E19E0C3EBF351A12E7595 (void);
// 0x00000196 System.String DevionGames.UnityTools::ColorToHex(UnityEngine.Color32)
extern void UnityTools_ColorToHex_mB31E9A44C57C1D8A257CF0D1B058CCD97235E7F2 (void);
// 0x00000197 UnityEngine.Color DevionGames.UnityTools::HexToColor(System.String)
extern void UnityTools_HexToColor_m2542D2DE0F305F210A44077BD2CA3269E37B16B5 (void);
// 0x00000198 System.String DevionGames.UnityTools::ColorString(System.String,UnityEngine.Color)
extern void UnityTools_ColorString_m9335B52921AFB6D29680AA7475F4A538C520864B (void);
// 0x00000199 System.String DevionGames.UnityTools::Replace(System.String,System.String,System.String)
extern void UnityTools_Replace_m24693C3EE97AE04AE9BC0348382A7A36E9584E90 (void);
// 0x0000019A System.Boolean DevionGames.UnityTools::IsNumeric(System.Object)
extern void UnityTools_IsNumeric_mABC9475A7E47F3FD45B59BAFC141FAB5FF4284A6 (void);
// 0x0000019B System.Boolean DevionGames.UnityTools::IsInteger(System.Type)
extern void UnityTools_IsInteger_m73A21999147BC2DFF761FB2672A72A7829DE7F44 (void);
// 0x0000019C System.Boolean DevionGames.UnityTools::IsFloat(System.Type)
extern void UnityTools_IsFloat_mB94371FBCBC59C1053D4C9AD7FAFCE0E389AF153 (void);
// 0x0000019D UnityEngine.GameObject DevionGames.UnityTools::FindChild(UnityEngine.GameObject,System.String,System.Boolean)
extern void UnityTools_FindChild_m3A4D6BC66B989B7F4BE684488D74F9EBF3E1DB97 (void);
// 0x0000019E System.Void DevionGames.UnityTools::Stretch(UnityEngine.RectTransform,UnityEngine.RectOffset)
extern void UnityTools_Stretch_mED921D4043429CE5C026BEE8BFCCD0F7F3730C0D (void);
// 0x0000019F System.Void DevionGames.UnityTools::Stretch(UnityEngine.RectTransform)
extern void UnityTools_Stretch_mB9ECC09B408195B08506E68C49F33FCEC9B91464 (void);
// 0x000001A0 System.Void DevionGames.UnityTools::SetActiveObjectsOfType(System.Boolean)
// 0x000001A1 System.Void DevionGames.UnityTools::IgnoreCollision(UnityEngine.GameObject,UnityEngine.GameObject)
extern void UnityTools_IgnoreCollision_m5D0648DA362AAEF8B62231E791BE11A6A8937545 (void);
// 0x000001A2 UnityEngine.Bounds DevionGames.UnityTools::GetBounds(UnityEngine.GameObject)
extern void UnityTools_GetBounds_mD328546150E05762EA5FCC17FDD95C6026EE8B23 (void);
// 0x000001A3 System.String DevionGames.UnityTools::KeyToCaption(UnityEngine.KeyCode)
extern void UnityTools_KeyToCaption_m981CF5FB3650987E282277DCF8CEA8BFBF4A23BC (void);
// 0x000001A4 System.Void DevionGames.UnityTools::CheckIsEnum(System.Boolean)
// 0x000001A5 System.Boolean DevionGames.UnityTools::HasFlag(T,T)
// 0x000001A6 UnityEngine.Coroutine DevionGames.UnityTools::StartCoroutine(System.Collections.IEnumerator)
extern void UnityTools_StartCoroutine_m1E22198CE829DE7B24CFE6B20CAAA0F4113B250B (void);
// 0x000001A7 UnityEngine.Coroutine DevionGames.UnityTools::StartCoroutine(System.String,System.Object)
extern void UnityTools_StartCoroutine_m5D8F448E6FB9413DFA61297DC8050A2E511C6D78 (void);
// 0x000001A8 UnityEngine.Coroutine DevionGames.UnityTools::StartCoroutine(System.String)
extern void UnityTools_StartCoroutine_m7B4ACEB8BA2A80CE4F3B8914CAAE3C73504D73D4 (void);
// 0x000001A9 System.Void DevionGames.UnityTools::StopCoroutine(System.Collections.IEnumerator)
extern void UnityTools_StopCoroutine_mF60F6A13C8058414C40AAAD2009384A2141F4122 (void);
// 0x000001AA System.Void DevionGames.UnityTools::StopCoroutine(System.String)
extern void UnityTools_StopCoroutine_mD8B00EB510E28C2DCAA829F5F66E7B725682784D (void);
// 0x000001AB System.Void DevionGames.UnityTools::StopCoroutine(UnityEngine.Coroutine)
extern void UnityTools_StopCoroutine_m1AB7FC8496584FD113311A51FD113F2F32861C26 (void);
// 0x000001AC System.Void DevionGames.UnityTools::StopAllCoroutines()
extern void UnityTools_StopAllCoroutines_m28972BDAEF18463F69E509E395ACFDFD4EE2DBF9 (void);
// 0x000001AD System.Void DevionGames.Utility::.cctor()
extern void Utility__cctor_m7EB290B0CD7C93A16B6FC8A57EF15BD480C9F3FF (void);
// 0x000001AE System.Type DevionGames.Utility::GetType(System.String)
extern void Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC (void);
// 0x000001AF System.Type DevionGames.Utility::GetElementType(System.Type)
extern void Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B (void);
// 0x000001B0 System.Reflection.MethodInfo[] DevionGames.Utility::GetAllMethods(System.Type)
extern void Utility_GetAllMethods_mA26E4013A1C68B62ED790F49F6C0D3DBBAAE9D21 (void);
// 0x000001B1 System.Reflection.FieldInfo DevionGames.Utility::GetSerializedField(System.Type,System.String)
extern void Utility_GetSerializedField_m3DFCAF7A79CDB3857A8B3B453E1AE088F665408A (void);
// 0x000001B2 System.Reflection.FieldInfo[] DevionGames.Utility::GetAllSerializedFields(System.Type)
extern void Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48 (void);
// 0x000001B3 System.Reflection.FieldInfo[] DevionGames.Utility::GetSerializedFields(System.Type)
extern void Utility_GetSerializedFields_m1D6F2A8FD044D8FF49D5FD9B27E0AB8BA0B2DCB8 (void);
// 0x000001B4 System.Collections.Generic.IEnumerable`1<System.Type> DevionGames.Utility::BaseTypesAndSelf(System.Type)
extern void Utility_BaseTypesAndSelf_mAD2EBB949D685E2D62DED747B3900570BAAD4FF7 (void);
// 0x000001B5 System.Collections.Generic.IEnumerable`1<System.Type> DevionGames.Utility::BaseTypes(System.Type)
extern void Utility_BaseTypes_mD100E7396D2DA9A100981F3005331402805CB914 (void);
// 0x000001B6 System.Object[] DevionGames.Utility::GetCustomAttributes(System.Reflection.MemberInfo,System.Boolean)
extern void Utility_GetCustomAttributes_m1BA602C31499B43DBB54A027A77907E562DAE270 (void);
// 0x000001B7 T[] DevionGames.Utility::GetCustomAttributes(System.Reflection.MemberInfo)
// 0x000001B8 T DevionGames.Utility::GetCustomAttribute(System.Reflection.MemberInfo)
// 0x000001B9 System.Boolean DevionGames.Utility::HasAttribute(System.Reflection.MemberInfo)
// 0x000001BA System.Boolean DevionGames.Utility::HasAttribute(System.Reflection.MemberInfo,System.Type)
extern void Utility_HasAttribute_mD6E30A724B936B8A733B759E7BB8644DB2DF7637 (void);
// 0x000001BB System.Boolean DevionGames.Utility::Contains(UnityEngine.LayerMask,System.Int32)
extern void Utility_Contains_m5D2130FBCFC37784529F87A9FB57C0113DE69999 (void);
// 0x000001BC System.Reflection.Assembly[] DevionGames.Utility::GetLoadedAssemblies()
extern void Utility_GetLoadedAssemblies_mEB7893AB1263758AC5263ECC66CA72F747C85176 (void);
// 0x000001BD System.Void DevionGames.Utility/<>c::.cctor()
extern void U3CU3Ec__cctor_m03A3EF69A054EA73BF3D5DC3445952B486770F74 (void);
// 0x000001BE System.Void DevionGames.Utility/<>c::.ctor()
extern void U3CU3Ec__ctor_m09A91CA5366576DA19FD4B03F7D4DB0B7FCAA907 (void);
// 0x000001BF System.Boolean DevionGames.Utility/<>c::<GetElementType>b__7_0(System.Type)
extern void U3CU3Ec_U3CGetElementTypeU3Eb__7_0_m8047473A8563AE9FF5596A7EEF7DED8B58E3DC63 (void);
// 0x000001C0 System.Type DevionGames.Utility/<>c::<GetElementType>b__7_1(System.Type)
extern void U3CU3Ec_U3CGetElementTypeU3Eb__7_1_mD66FA9A2FDA4B6EA3540BD9C21825D39C5099896 (void);
// 0x000001C1 System.Int32 DevionGames.Utility/<>c::<GetAllSerializedFields>b__10_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetAllSerializedFieldsU3Eb__10_0_m8E0C3AA2F406B422373814D1466C58980ED5693A (void);
// 0x000001C2 System.Boolean DevionGames.Utility/<>c::<GetSerializedFields>b__11_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetSerializedFieldsU3Eb__11_0_m4CAEA39D5FED24E3E7BC99758767CCF8DBDB4154 (void);
// 0x000001C3 System.Int32 DevionGames.Utility/<>c::<GetSerializedFields>b__11_1(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetSerializedFieldsU3Eb__11_1_m06D9CD67DCA805D61C4FEE5B70167664FA84CF53 (void);
// 0x000001C4 System.Void DevionGames.Utility/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mEA93F88E2D52C8E80687A375CDF39BE252E796A6 (void);
// 0x000001C5 System.Boolean DevionGames.Utility/<>c__DisplayClass9_0::<GetSerializedField>b__0(System.Reflection.FieldInfo)
extern void U3CU3Ec__DisplayClass9_0_U3CGetSerializedFieldU3Eb__0_mF979CC08C9D819384398D21D02BEF8CE18195457 (void);
// 0x000001C6 System.Void DevionGames.Utility/<BaseTypesAndSelf>d__12::.ctor(System.Int32)
extern void U3CBaseTypesAndSelfU3Ed__12__ctor_mC16CA045DB26529930070E385A119F5FC28687D8 (void);
// 0x000001C7 System.Void DevionGames.Utility/<BaseTypesAndSelf>d__12::System.IDisposable.Dispose()
extern void U3CBaseTypesAndSelfU3Ed__12_System_IDisposable_Dispose_m266119160E53905B8BA612E023AEBDC2F1B800E0 (void);
// 0x000001C8 System.Boolean DevionGames.Utility/<BaseTypesAndSelf>d__12::MoveNext()
extern void U3CBaseTypesAndSelfU3Ed__12_MoveNext_mB90BD002079EFFF0326E8E7072847E67159D4D77 (void);
// 0x000001C9 System.Type DevionGames.Utility/<BaseTypesAndSelf>d__12::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m18C4E6212AD8F107EBAC79DAD65CA1156E82DAC1 (void);
// 0x000001CA System.Void DevionGames.Utility/<BaseTypesAndSelf>d__12::System.Collections.IEnumerator.Reset()
extern void U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_Reset_m93A48609980842C0527D5EF5AEDBF49ECC60484C (void);
// 0x000001CB System.Object DevionGames.Utility/<BaseTypesAndSelf>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_get_Current_m632B11DDF786C85B0002A8C4916C13FF49A1F7C3 (void);
// 0x000001CC System.Collections.Generic.IEnumerator`1<System.Type> DevionGames.Utility/<BaseTypesAndSelf>d__12::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m0E1C888F34B391FF5A69ED92DF7FADA9AFFD611F (void);
// 0x000001CD System.Collections.IEnumerator DevionGames.Utility/<BaseTypesAndSelf>d__12::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m07AA84023DF74C6E52C7DA01FC413312FA9B7F8E (void);
// 0x000001CE System.Void DevionGames.Utility/<BaseTypes>d__13::.ctor(System.Int32)
extern void U3CBaseTypesU3Ed__13__ctor_mE5EFE42FB994CC40FAA0D05432A1D4E87D246034 (void);
// 0x000001CF System.Void DevionGames.Utility/<BaseTypes>d__13::System.IDisposable.Dispose()
extern void U3CBaseTypesU3Ed__13_System_IDisposable_Dispose_m90A8E9CA4A62940322702B943C802246F7198F07 (void);
// 0x000001D0 System.Boolean DevionGames.Utility/<BaseTypes>d__13::MoveNext()
extern void U3CBaseTypesU3Ed__13_MoveNext_m54435BD974A4519F0EC10D6F2618244F4E20565D (void);
// 0x000001D1 System.Type DevionGames.Utility/<BaseTypes>d__13::System.Collections.Generic.IEnumerator<System.Type>.get_Current()
extern void U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m478F8E1330BFB7CD178E7001520F4E03ACBC54C7 (void);
// 0x000001D2 System.Void DevionGames.Utility/<BaseTypes>d__13::System.Collections.IEnumerator.Reset()
extern void U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_Reset_m656B190DF87DC9078C1AD524603995BF40E749D7 (void);
// 0x000001D3 System.Object DevionGames.Utility/<BaseTypes>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_get_Current_m989A6A05C7EFB1780FE9588AE15B7020A3D3DDB1 (void);
// 0x000001D4 System.Collections.Generic.IEnumerator`1<System.Type> DevionGames.Utility/<BaseTypes>d__13::System.Collections.Generic.IEnumerable<System.Type>.GetEnumerator()
extern void U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mFF576A29276D4CBABC0891FB8734F1D5AE81B00A (void);
// 0x000001D5 System.Collections.IEnumerator DevionGames.Utility/<BaseTypes>d__13::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBaseTypesU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB6B2F041696200200F5DEA07D880E2F7C57DA539 (void);
// 0x000001D6 System.Void DevionGames.UtilityBehavior::QuitApplication()
extern void UtilityBehavior_QuitApplication_mF8D4B67159389D3811F1375EEB91509391CB70A9 (void);
// 0x000001D7 System.Void DevionGames.UtilityBehavior::LoadScene(System.String)
extern void UtilityBehavior_LoadScene_mB132C6A6A10F5026FEBFD1F4301E0D08CA25E48A (void);
// 0x000001D8 System.Void DevionGames.UtilityBehavior::Instantiate(UnityEngine.GameObject)
extern void UtilityBehavior_Instantiate_mA73BB77B1600ACD5F43262749F141A98BD714613 (void);
// 0x000001D9 System.Void DevionGames.UtilityBehavior::.ctor()
extern void UtilityBehavior__ctor_m1535B0FDCF9455A8B4922CDDFAE9D772EF7DC228 (void);
static Il2CppMethodPointer s_methodPointers[473] = 
{
	WindowsRuntimeExtension_GetBaseType_mE6ACF7E58CB0D935D01E896D429EAF4B58DDBDEF,
	WindowsRuntimeExtension_GetAssembly_m3CAE53A6F8F7706260CC43FDCB1B8EA3FA795D6E,
	WindowsRuntimeExtension_IsValueType_m9F0ED94D95D39AE9356387E07522334ABBE0D477,
	WindowsRuntimeExtension_IsGenericType_mC31B6B3F7CA5148D1A5895401883948F5FBF1619,
	WindowsRuntimeExtension_ParametersMatch_m2ACF810B60431BF319CAEC7D8A6D0E0853AA9708,
	WindowsRuntimeExtension_MatchBindingFlags_m420BC851B0FC38308C8983FF213E7F86978E6F17,
	AnimationEventSender_OnStateMachineEnter_m3024F864EED5574D2AF1BE59B1E6DB5A0653AAD2,
	AnimationEventSender_OnStateMachineExit_mABF0DE2EE0ECD6FB407F828C4ACBC773E832544A,
	AnimationEventSender_OnStateEnter_mE66210AFDE3ADB38C1CD286B0C0430D1759D433C,
	AnimationEventSender_OnStateUpdate_m8ABBFC3C4AE42ECC9407392E3660C7E18FDE3298,
	AnimationEventSender_OnStateExit_m81ADCCBB68A02AC00CFBBBF6D4F38F9A70FFFA60,
	AnimationEventSender_SendEvent_m8664651F0085CE8306EF251D521C25DE7286DA37,
	AnimationEventSender__ctor_m451453AB5C89374328CB4EEA2839B758BB97441A,
	ArgumentVariable_get_ArgumentType_mBD12B96B3BC86619A2668689F733040C3AD92263,
	ArgumentVariable_set_ArgumentType_m956F7B89C6AB5E0664CE26E1E66A6EBAF424D364,
	ArgumentVariable_get_IsNone_m0EA7262EB485DEE8011F3843C93CD67E5AEB377A,
	ArgumentVariable_GetValue_mBAE495F444E558E1C64641A071949EB0ADFE60D9,
	ArgumentVariable_GetPropertyValuePath_m10F268A42BC3A41B5EE96AC5FBB1424994F5B3E7,
	ArgumentVariable__ctor_m7E3C07A465C4ABABFB1D5884FA93480090A8FB37,
	CategoryAttribute_get_Category_m5E616988DFEB4339B40F43FACAEAE3B62CB80C61,
	CategoryAttribute__ctor_m84BE4F1EB081CEE2C16249663AE79F5F84439A5C,
	ComponentMenu_get_componentMenu_mAF2EE494DD38FE27035083AD18E676BE9C078EAD,
	ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3,
	CompoundAttribute__ctor_m72834167039F9894AE5C88D9A353D16E9AE8BFA4,
	EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818,
	ExcludeFromCreation__ctor_mCE92E999886A8742ECC711969904CAD04D20E038,
	HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC,
	IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330,
	IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD,
	InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C,
	InspectorLabelAttribute__ctor_mB7200A2D81B1D0AE64E416356378CA8CE0041EEF,
	MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52,
	AudioEventListener_Awake_m9432FCC2F1AF4497EE6B80EB631285314BF64D7A,
	AudioEventListener_PlayAudio_mD7BA13D42CE35C16521B890737E562B801E79B64,
	AudioEventListener__ctor_m953712EC0045A6705EF4E286573AEFBC975BF439,
	AudioGroup_get_audioSource_mE43B8E290F8B4931DC7C44F8AC0237B4F6B03DB5,
	AudioGroup_set_audioSource_m1B24F7BB72F968658A04E0F573CC2E6A6B9BAAD7,
	AudioGroup_PlayOneShot_m1F23D0A6B9074B5C9BC5FA586F68A005709A342A,
	AudioGroup__ctor_m798D39DE14D4F2844AC1B347BCDFF254B4D3431D,
	U3CU3Ec__DisplayClass2_0__ctor_m65094D917C56C1970F422F40FAB7D625AE6C7934,
	U3CU3Ec__DisplayClass2_0_U3CPlayAudioU3Eb__0_m667F4D771465488DE976CD02559B7B154ABEE429,
	AudioPlaylist_get_Item_mC58282B14C6CBEB3F4856FD9A644D6222B50E78F,
	AudioPlaylist_get_Count_m76AE369B79FC7429ECDC6FB9C48A89DADED71E63,
	AudioPlaylist__ctor_mDE731354783EC2170560CA95A5C2F2183A15CB83,
	ArrayListVariable_get_Value_mFE6EFAC812D9BFC26BFAF313E75690C3943AA0A5,
	ArrayListVariable_set_Value_m2B47AE7F0CF3EFC418F0E7DFE5B1CD121F888BD2,
	ArrayListVariable_get_RawValue_m4E6A456E83E93C8F657DC65BD38F929E171AE97B,
	ArrayListVariable_set_RawValue_m8C63FBCECE83634F235350892B6D74B744A2B235,
	ArrayListVariable_get_type_m71509A553C514611BEDBA94E4C085D975C06007B,
	ArrayListVariable__ctor_mA27817C12868653C142186CDAEF1D20AC851AC44,
	ArrayListVariable__ctor_mF6D8C7A267B09ED9433943154177C0913D75F03C,
	ArrayListVariable_op_Implicit_m5BF9FE5C3B188ADCC9A08AB13DD1542C275A706D,
	ArrayListVariable_op_Implicit_m7DD6038F492FAC0F8A7659EAA2ADE4B080523D93,
	NULL,
	NULL,
	NULL,
	Blackboard_SetValue_mC1D4129E18F01A7FABAB12C57DF94026046F2C4F,
	Blackboard_DeleteVariable_m79797EBE8ED6B85CDD93E01B85B18BA0BDBFCA7E,
	Blackboard_GetVariable_m39930581EE9595405048B096B4A9E28B52AC5899,
	Blackboard_AddVariable_m0DD1A4CF28914EED9614988CD9DB8CE0F0FB8161,
	NULL,
	Blackboard_AddVariable_m9A3A5A6E28034F01DC2656F19443141343584DAF,
	Blackboard__ctor_mE6ADBA43C560F7A8FE73C33014FE2CF7E2120D07,
	U3CU3Ec__DisplayClass5_0__ctor_m2BE4D85F14DD1B24B5CB6B7B58E239983FF2535D,
	U3CU3Ec__DisplayClass5_0_U3CDeleteVariableU3Eb__0_m1D789D9A6A8CC29A1D1C665730F9DCBDA27AE6B5,
	U3CU3Ec__DisplayClass6_0__ctor_m0D7DC7A912FF0B4C3F80E64A6D5D701012138AD8,
	U3CU3Ec__DisplayClass6_0_U3CGetVariableU3Eb__0_m82F2E86DD1DCA5858B4676F1E2268997C9D8F59A,
	BoolVariable_get_Value_m332ECE2B796C9D5410199F7AE056BEB9E85C8DAB,
	BoolVariable_set_Value_mD05AFF1E9DC1E2523AE6C1D0200CFCD9D2A5432B,
	BoolVariable_get_RawValue_mCAC036B036E2FD98C16966C6BEF0C759836EC465,
	BoolVariable_set_RawValue_m5B2B9D8E85394AFDACB23FF8F667A5BB58FA1683,
	BoolVariable_get_type_m54DDF5197DA2252363F090F513458EF1DACE2F27,
	BoolVariable__ctor_m6174AF60ADEE6BA67E5BF390EC6CEB6E068DCF3A,
	BoolVariable__ctor_m8398FC2DE8D1D5841DA97D89F62E23B822CD9F63,
	BoolVariable_op_Implicit_mDF5931559A3092BE7558FB3AF3B5D58D5DAE442C,
	BoolVariable_op_Implicit_m5AAF4104A968E7B1DD231FA0D6E88BBC17E8CE3A,
	ColorVariable_get_Value_m015C3BFC907798781473443678169B8B5CB2E05D,
	ColorVariable_set_Value_m0EB68E424C43C661C9757C4C6EF14C2420D62258,
	ColorVariable_get_RawValue_mF7C0CE35F9867A7616BD4961EE297E813557E3DA,
	ColorVariable_set_RawValue_m02674AF1ECF919F4FBB460CF8AC24B74859195B5,
	ColorVariable_get_type_m576644C950562CCBC03E0FF1D9785BAB5D505082,
	ColorVariable__ctor_m559C57C93AEBBFC93F829A910AE93BC9C2AB1AF8,
	ColorVariable__ctor_mE4169ACA33372D039367FD35A705B6CF2F3B483F,
	ColorVariable_op_Implicit_m0329B90B1AEBDECC45A2122B4A3ADDE9CAF92C80,
	ColorVariable_op_Implicit_m74AD9BBDF27ED1F276AB74367F850BF9CA6C803A,
	FloatVariable_get_Value_mA1B7ABDFC1AE103F9C3C12B96460D284C1261DA5,
	FloatVariable_set_Value_m6A213705CD85CA612DDA0EBF7919327DB7047ACA,
	FloatVariable_get_RawValue_m0324E7A777F9B1B92D4A1291AD6EAE967B3A8669,
	FloatVariable_set_RawValue_mCBAF1297B54FBC43DBC85058C6091505B0FF9A10,
	FloatVariable_get_type_mFCC4FC8F7520D4B77C252195588DA35B1E513D90,
	FloatVariable__ctor_mD91FA9784B44B20F8F86CED745376622EAFCFC53,
	FloatVariable__ctor_mD7238551CDE9398472D9460F7CF2E8464232CF53,
	FloatVariable_op_Implicit_mA53C094916B1A2C71B90CA9BFF638530CAED58E9,
	FloatVariable_op_Implicit_mA43124B62982BF153986EABC309254ADDF90AD68,
	GameObjectVariable_get_Value_m53A2A6D5F8AFE604B2180559EA0D4A3BFB5F058B,
	GameObjectVariable_set_Value_m546A5744ED95FB12855FBD6306BAFB3939DFC860,
	GameObjectVariable_get_RawValue_m0FA2E40F9DAC771117819A1E0F6E5320ADCCC0EE,
	GameObjectVariable_set_RawValue_m517A8EF599964CA46D01EFDB5C8BBBB7E3D9F798,
	GameObjectVariable_get_type_m9D7AB2140C111A7D99137F2CA22A79703F12CA9B,
	GameObjectVariable__ctor_m1571F4F20290F5021FDB041E65EAF756CDAC6B0F,
	GameObjectVariable__ctor_m6AFEFA274831D491A760A1B4C6DE30994B0D21AF,
	GameObjectVariable_op_Implicit_mAED845D8E325093AA75489EE6490226BE6696B96,
	GameObjectVariable_op_Implicit_m641BF9F16C956CFFA55B03990F93CFF16B1C03C0,
	IntVariable_get_Value_m1FF293E0E5BA7FDAD08240FD215552990D795F49,
	IntVariable_set_Value_m67C4A3DB666A78419AB600FF88C6A7F92E8F97BB,
	IntVariable_get_RawValue_mEBA6A0D5209FC1956E9F6AE3BE79F854B6265E4F,
	IntVariable_set_RawValue_mAC37C745DE93E594223E32113520E77BEA5A46BD,
	IntVariable_get_type_m2699893ABEABEE7F2A69A520BA4F32F58BBE0E94,
	IntVariable__ctor_mA6DB20DA5D632A35D3F35E1F9EE73C78CC4C47AD,
	IntVariable__ctor_mA25B604E94831B321545CAD1A1F847E2FEE935B4,
	IntVariable_op_Implicit_m678DFA5E56A89C6F297A90175414652551D5190F,
	IntVariable_op_Implicit_m8CCE943400051FD0A1C27C2E4D8460BF6389A871,
	ObjectVariable_get_Value_m37083B1F21D8973732237B30AE8A2F4986B182D0,
	ObjectVariable_set_Value_m98C993762C89B6989717615DCA00A025C169BD80,
	ObjectVariable_get_RawValue_mE0E094C4AB0CD3C93F57EF2A202AB12A5546B9A8,
	ObjectVariable_set_RawValue_m3654FA6EDBCC4B81C6BBF82C66D610E025E4F179,
	ObjectVariable_get_type_m19DA2057045F8ADA72BC27BDA1C70956C8A625F9,
	ObjectVariable__ctor_mB4D033335A1D22BD4A851FE2FF903E10B2DA6E2E,
	ObjectVariable__ctor_m51B3DF6CE5F4600594C1B4DF6DC1883A94FF7442,
	ObjectVariable_op_Implicit_m7E61B9B263DB84F9812B6748CC3F9A1D781259F4,
	ObjectVariable_op_Implicit_m7FE9F61AAD1A08A0AA05F6D8295BB17A09A495A5,
	SharedAttribute__ctor_m738BE8BEB37C6EAD0C8841893DF1DEC881EACF4E,
	StringVariable_get_Value_mCEE8C988AA9077131FC0CF1BE23AC3549F7092B7,
	StringVariable_set_Value_m4FD334C170C3B80AABEEFD836B6AEC75150D8D67,
	StringVariable_get_RawValue_m658AE8F29B78638D469F3760A8840F97B332EE33,
	StringVariable_set_RawValue_m2F7EFADB83883C85755A3AC094EFB97A865C8043,
	StringVariable_get_type_m37194FECCEB626340FAE9A3A6563B569AA180630,
	StringVariable__ctor_m71872475CE29E267605F93BF0E2F23F1DE289CF7,
	StringVariable__ctor_m15109BA1698AD27E314FC2DF8250E454BF5463C6,
	StringVariable_op_Implicit_m85C7CC038FC5D4053853C20FCC3CB1B2856D0D87,
	StringVariable_op_Implicit_mC5C837810ACB60BA0B122A56BB3B766B12319D38,
	Variable_get_name_mE7CD1F19A9A1B8F180C3C129D748466FD3D255D7,
	Variable_set_name_mF56BB1728B5D6294614EC1A95ECD73AB6A7D7792,
	Variable_get_isShared_m7CDE99AC0F54969D9887DA1DE4DBDD4EE37EFB32,
	Variable_set_isShared_m83CF7CB67F2137D49932729CD16FE25449FAFA8F,
	Variable_get_isNone_mD765CC99DEB9D7589A728D052A909282EE2F51A2,
	NULL,
	NULL,
	NULL,
	Variable__ctor_m3DF269A5FCC98AB6BF3F79F54FE3543E5C15BD42,
	Variable__ctor_m1687146BB81F5AF850FA31F3D85CDAFB304F9911,
	Vector2Variable_get_Value_mCB200EBE7B8517387A7688197BF863EABA3F5001,
	Vector2Variable_set_Value_m79B547518A2A03D8314F9A2D6AB021F2E76D2030,
	Vector2Variable_get_RawValue_m241313F422B5551AECC8CDAF0C5C4C35E32639C6,
	Vector2Variable_set_RawValue_mA4DF84B2C802CEF5A1883A7134C26F2DECE2F22F,
	Vector2Variable_get_type_mC1038ACFC133A0708C4A4528DC1A711CD1830F40,
	Vector2Variable__ctor_m1A8FFF5773652B0C90F4E2662AA950435F358A20,
	Vector2Variable__ctor_m4C0919173D47A604EE87A251968B717D94F78420,
	Vector2Variable_op_Implicit_mD33CA39C14E78B5F3055A0DC121469F23AA02C3C,
	Vector2Variable_op_Implicit_m8283DC146B400A203D90001F289501B504808D9F,
	Vector3Variable_get_Value_mC5220AD3091F933D8662F8EBE4AFD44464C425BD,
	Vector3Variable_set_Value_m9707A1A009731DB5DE9F14765C4A3C131966ED0D,
	Vector3Variable_get_RawValue_m9D25377B82D4704A51AEAAB9DC173F60CCCCDDB6,
	Vector3Variable_set_RawValue_m535A6DC0C283D54204E8C85D49021667D240BFBE,
	Vector3Variable_get_type_m1B1CCE67F472CF3A2C1AC4F698674D7AB60A9855,
	Vector3Variable__ctor_mEAD567AD69468BD46E72258CEFF69E952FB91D67,
	Vector3Variable__ctor_m30820CD501E201FAB010BBF72FAF93E0FC9663AC,
	Vector3Variable_op_Implicit_m4D1CB12225B5B805CE1FD3AF2BFAC04286889C1A,
	Vector3Variable_op_Implicit_mB4BCCCA58D2A22AB2BA8C8A9EE82D71A22FE68A9,
	CallbackEventData__ctor_m103D4E02BCA85FB3E04CEA8524E42A9BAADDA333,
	CallbackEventData_AddData_mA4E495602ACF2FD723683BDBB3E371434960CA3C,
	CallbackEventData_GetData_mE16063313FC3A76BBBC40F9FAF898E14795D1770,
	NULL,
	CallbackHandler_Execute_m977B75E8AABBCF185D4C1CB477EBA4D84C68AD31,
	CallbackHandler_RegisterListener_m55292FC1E70AF8C33813BF095ED1743563CEB3A6,
	CallbackHandler_RemoveListener_m3570400B68B31861CDC5A8DFF406BD682E462979,
	CallbackHandler__ctor_m38F9E3848D97076B2176CE3A250B7A06FA54402C,
	Entry__ctor_mCDE5846E8210775A2FAADD13ACB1D160D62D7F68,
	CallbackEvent__ctor_mC44831337F8F4357BF97F7AF60D28AD739DBD5BB,
	CameraEffects_Awake_m7A8580FFBBCF266208F6A1210DBDB7E2E0B93840,
	CameraEffects_Shake_m59E63858224C18B9492984C3DCAFFA178E5BCA63,
	CameraEffects_LateUpdate_m74A49329FCD9859ED68BCCCA764C7D496FE7E6FA,
	CameraEffects_ResetCamera_mD79732D1495C96B10A667B713DDBBFBF02618D6C,
	CameraEffects__ctor_mAFF07E8066A65EE0759394674087CCC362815CEB,
	CoroutineHandler__ctor_mF5C507E7C02299B8CF8B1301F1B65175CE76B8A2,
	CoroutineQueue__ctor_mD42E0C96CBDCA3BECCE44B059FF9931D534BED7B,
	CoroutineQueue_Start_m5F152C6F896357B134331A796672AA1ABE02D8DB,
	CoroutineQueue_Stop_m59583908A8FA22629E6723BCDAF087A9D2B3C4A6,
	CoroutineQueue_EnqueueAction_mDE9AB402C6EFB6D399FE021E2D8801C640FFB23B,
	CoroutineQueue_Process_mA4F1B1CA22EBA0AC12050CE9D6CB58193071F76D,
	U3CProcessU3Ed__7__ctor_mA35CD4DE0A2227D3286F26FD712B83DABCFEBFA3,
	U3CProcessU3Ed__7_System_IDisposable_Dispose_m2839E313A558FE75EC388F0735F6E5BB247B5CBF,
	U3CProcessU3Ed__7_MoveNext_m7F1CFCB6D49702368BF5344EDEEB891727021F20,
	U3CProcessU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED87547CE0E935996285759923C65E910ADC90DC,
	U3CProcessU3Ed__7_System_Collections_IEnumerator_Reset_m70CD4B91FCDAAF12A0674773E8D497436327CF60,
	U3CProcessU3Ed__7_System_Collections_IEnumerator_get_Current_m2B21F63871756331688DBDD7C68F246498644548,
	DontDestroyOnLoad_Awake_m1BE38B436A6D7D5F86FCC5D12D9A8831919AF7A1,
	DontDestroyOnLoad__ctor_m902572EEB23F8126AF1FBA4972B9F0E9F2C3CA09,
	EventHandler__cctor_m1C087E3F6722E7ADCF20607B6FA2CA0773480668,
	EventHandler_Execute_mC9AEB7977041146F8430BA0A2533A962F11B722F,
	EventHandler_Execute_m3F598B00C6285C501830C69892DDA29578B6A48A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EventHandler_Register_mCCE2DF1E7BF59E4EC7600F2F38E006E1F0E06D08,
	EventHandler_Register_m7887E2866B3323D8521D4F688ED985E9E6A4E197,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EventHandler_Unregister_m445BD7DFB081C8EEC51FD356F5F1EC4CE2FC32C5,
	EventHandler_Unregister_m64A893E7AF43A68A836879B3F06904EA2F395BBD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EventHandler_Register_mF5C710D25E7F28EB64A61F5FB1A38C7EBF017C40,
	EventHandler_Register_mC985925E6C3667D9B606F6B18215E905FB256EF9,
	EventHandler_Unregister_m9A5AE7046FF18608D5E6E94893794632E82E0D44,
	EventHandler_Unregister_mFF5654B39FE8D8D171D93EAB64EB41C5885A33BB,
	EventHandler_GetDelegate_m4A5D1D11A804D1E1D77F0A1324BED1BAA00943C5,
	EventHandler_GetDelegate_m8EAB69B7F7093E06A85CA419B151777D81F51E32,
	EventHandler__ctor_m998EE8D7FFDEB7156A2FBB7A7108E64D57E97F7F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonSerializer_Serialize_mA0040BF435F812CBD17F5D2773B87127901F3E3F,
	JsonSerializer_Deserialize_m8A5ADBADE2B4A2A8BC6BDFC954FAC8C2A57E7C5C,
	NULL,
	JsonSerializer_Serialize_mEE0C3E0547ADB4A9D57FE3533B3E0E500791AEA0,
	JsonSerializer_Deserialize_m222767516257F841D1E140C9263B2145E393BB74,
	MiniJSON_Deserialize_mC78A07565C97B728803C8AE751D7583B7E5A4989,
	MiniJSON_Serialize_m0EAC236F1654F6BA91D0955E75321F084697656D,
	Parser_IsWordBreak_mECF1CFB9E4D5DBBA923A834C01FBEEFEA2B27EE1,
	Parser__ctor_m9E21D017E4747A4045DC07B29A764E7411BF449F,
	Parser_Parse_m6A83FEC068F710E9DB5F7D6E50CBE434C2E0ABFD,
	Parser_Dispose_m2AFDED3B409174744C14C82FB55315FC25183264,
	Parser_ParseObject_mA45820E6DC2BADFBE31B079E08795EEE018B2E89,
	Parser_ParseArray_m23BD84E2D9D97A3253919389BC7D6D5845A52A3B,
	Parser_ParseValue_mAA9616FDB99E62E648953BA0F555EED59C294795,
	Parser_ParseByToken_m643F5C2C8468B0850F138A844EF088B2547D2793,
	Parser_ParseString_mFD5FED77AE3FB01681A47B921AC47FD1B7EA9CB4,
	Parser_ParseNumber_mFD6EFB7880D7DCF8B83E1635A250E86FA02B8BD1,
	Parser_ToQuaternion_mA1ED062403850989FFA1C75AF1512E3E8233B382,
	Parser_ToVector4_m1ABB680D64963C605FF8BE6F878A3FC2417262CA,
	Parser_ToVector3_mEADF1FA0067DB25EF2CD48F91381E90615D00567,
	Parser_ToVector2_m390A4978445E5959A32772A1F7A2802FD015204D,
	Parser_ToColor_mC3FA3801EAF7A42BF30ED3CD8AE20F0BA64418A8,
	Parser_EatWhitespace_mF9C4A1E38EE3D8D232D2DC0D2BA149DEC8EA7F00,
	Parser_get_PeekChar_mDDCA1D7C1FD96741D077A8E68E7770D61D8D8315,
	Parser_get_NextChar_mE2E741E78DD5EF0F3CD5D2823F5FA3EA68F4864D,
	Parser_get_NextWord_mBD502DD147AADCCD19927A2388F0830574D48931,
	Parser_get_NextToken_mE6664A7BD810EB40C908E2A5AE2B374F8F586367,
	Serializer__ctor_mD528CF21C57D1C4DE43B3DCCDA0B04F000E88AD2,
	Serializer_Serialize_mE20B6AFB56DB0959B654675580B767967F05866E,
	Serializer_SerializeValue_m95C4388683E1A911F366629C73DDFD049EF61F07,
	Serializer_SerializeObject_m48BD0BAEC64340715EDC1F1DD92AB21DEE6BC534,
	Serializer_SerializeArray_mD070FE3C3CE50FD6D76FAFBE9F6D7DA00707C4E6,
	Serializer_SerializeString_m911D7DACEB983B330B8E9E9D916CDA770225B639,
	Serializer_SerializeOther_mE7199EA4878583CFDD5484147F5DAF04E2EC693D,
	LookAtMainCamera_Start_mF924C7E9DE9870FA2C9ADF998A1B3E69607C835B,
	LookAtMainCamera_Update_m53CBDB27D41652D8FE5D061DDFCDF4D79D3C8494,
	LookAtMainCamera_SearchCamera_m6FA58F13F9317393645F50E5496431BE77B0993B,
	LookAtMainCamera__ctor_mA1EAFCDCA283CBAD6233C9952BB60E30B5F44D43,
	U3CSearchCameraU3Ed__6__ctor_m05514C886D4BDBB39C51CAF3FA8ABD053B9F8EAC,
	U3CSearchCameraU3Ed__6_System_IDisposable_Dispose_m18B8EAE4684219FCB4D38506E56021D26E4BD8CE,
	U3CSearchCameraU3Ed__6_MoveNext_m04308AC6164FB0730FD9AE33BFC2E106D1A16D6C,
	U3CSearchCameraU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88445D17B8BB4050E4C069580E8637BD7971F47A,
	U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_Reset_m9E922F1F4C94E1398E5CFD22EF6A07F03A9BF7C8,
	U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_get_Current_mFD0288E149200F0FD3B5715E4E00F236506B7782,
	MoveForward_Start_mD6B6A4B01FCE24887297DCD57D5FAC9EF0B94942,
	MoveForward_FixedUpdate_m02871A13C36E5951EB3F8E8DE3612BE8048C213B,
	MoveForward__ctor_m688707D09CC0688B5B181DBCE6019BAF29B0CE29,
	MoveTo_Start_mEE605C609529C147B27928DD04929094F3DB69AC,
	MoveTo_Update_m26E450C3D432BD61053A48B0D115E55C6E7DF9E6,
	MoveTo__ctor_m61258B00F90817B6A436C7EF231890501C6E8F8C,
	NamedVariable_get_Name_mB1B05159CFD28BA66C8F550375E709B061370F35,
	NamedVariable_set_Name_mB7FF12474431E697C37281C8136ED3ACB0EB4084,
	NamedVariable_get_Description_m9C53F0D71B95A7EA12CBE50A68F2740CEACDEC2D,
	NamedVariable_set_Description_mA1012D042EC37718286252FC8D9E7FB0268BEEB1,
	NamedVariable_get_VariableType_mDF1A4194B6EC84515268C73DA0BE406E4814FD87,
	NamedVariable_set_VariableType_m5E8D3355D0075984B9C2DE2FEDA2CB336B942309,
	NamedVariable_get_ValueType_m59DF0FA7B1F08A9640CE8B60D8864399D15423A2,
	NamedVariable_get_VariableTypeNames_mDB7E697BEE2748C30A75872CDA5C26DA39F69F1E,
	NamedVariable_GetValue_mA3BC0A2540ECDEE87309ABF351416621D031FABC,
	NamedVariable_SetValue_m980997E62410434EA5E9FE2EE3FE6F73E4C1787A,
	NamedVariable_get_PropertyPath_mA72116852B01683CC6EE5654DD6FE351683F61C5,
	NamedVariable__ctor_m59AFD128613287C47562D278F1C640E877D77DCF,
	ObjectProperty_get_Name_m5AB1BCDC2216FC182720615EAE3551E1199DF232,
	ObjectProperty_set_Name_mAF9740B014A461FB4FF620FAF587D2BA2B073BCC,
	ObjectProperty_get_SerializedType_m99A5E6A06E67E9F1EAD559B65CFD4217741CDE53,
	ObjectProperty_GetValue_mC5A96CC3D724D54BE0FF296AFE745F06768F716A,
	ObjectProperty_SetValue_m71B8393BB54C121AFA7F9516BEF266CAFA6F67CC,
	ObjectProperty_GetPropertyName_mEC5F0B3B118A2598E31B87A13050267F2B2D7BCD,
	ObjectProperty_ToString_mFCED8D0E0901C40F68D9FBF4A9E5CFEC78CABFB6,
	ObjectProperty_get_SupportedTypes_mE3D86A9F079FC5C4A8C172C822D5E187DD4879D9,
	ObjectProperty_get_DisplayNames_m7517213FDD438E91561A80CE50AD9EB67E8276CE,
	ObjectProperty__ctor_mB2C319288BF72C2C7E033E487370807D78A98949,
	PlayAudioClip_Start_m76002B53FCC6F1E3B598DE5FF7898B38593FE0B2,
	PlayAudioClip__ctor_mD1B79D1717E52DBF15540E77493F379D0EF6E692,
	U3CStartU3Ed__4__ctor_mEF45D7D81D28D0AD72ADE751ADF86D4F214D0CEE,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m36730405D9679212EDBA4DF350C053A8EF8B4062,
	U3CStartU3Ed__4_MoveNext_mBF50C82120DE3C36EF092188B9E2220C7FF8DACC,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m862A8A274013AD886436DFF6E0E832D3CDF8584F,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m933C41702153F8200693D84ACFA4E211767628BA,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA428A2D99B93A9A2EE31DBF3B6027A222715320D,
	PlayerInfo__ctor_m36A300EA30D61D83F022B43FACF0B5681B50A8D7,
	PlayerInfo_get_gameObject_mCDC9229223B46F65319EF98553E3AC88FBA66192,
	PlayerInfo_get_transform_m506963C45530B19D4FEC4C1E348604F46E58CC39,
	PlayerInfo_get_collider_m85917D3BC5BF4F56E82EE592DD4A8EF21591C79E,
	PlayerInfo_get_collider2D_m74B061ACCA931A5D38B6314729FDDFB5A7486851,
	PlayerInfo_get_animator_mC90B904FD5F892750E5D3559BE22A791EF3774B1,
	PlayerInfo_get_bounds_m92571E2218491825149E7ADBFD1B62D96D3AB428,
	PropertyBinding_Start_m81E90CF45638C8FD3D2B094F66139EC799CF9A99,
	PropertyBinding_Update_m3E4C42EAB7BCA6F7C55A78892B80E4D4B3EE1A5B,
	PropertyBinding_LateUpdate_m3041E2B2D7E66CF688AAE696778D8CAC3A795963,
	PropertyBinding_FixedUpdate_mE1AE66D9FDA57365C9BDF3EE3C96FA76FC8281AB,
	PropertyBinding_IntervalUpdate_mF7DB2D8DB3B459788E759EED9F73261399142692,
	PropertyBinding_UpdateTarget_mCB3D5E0E8DC93BD950965465AE97EC0CA8D839E0,
	PropertyBinding__ctor_mEB0047074BD9AA8422495DC076267CB4297C4FD6,
	PropertyRef_get_component_mDA66F92BEA0D5FF5D3CC12B7563C9F2ADF8592C0,
	PropertyRef_get_propertyPath_m42D396F55CF08FBF7DADC8DF5DBE0D82EC4E9697,
	PropertyRef_GetValue_mE4F59D47453AE94DC2001E468AC359E47E848FEA,
	PropertyRef_SetValue_m135EB1AB70F97E3E94E993144F1C4DDF83B43CC6,
	PropertyRef_CacheProperty_m1DBB883A938AB5FD747C8A8EFD3A618B26F02107,
	PropertyRef__ctor_m913CD13E033BDECCA9B096D69814E24644AEB595,
	U3CIntervalUpdateU3Ed__8__ctor_mE569C36E5154F28C9464783AB286F1E5D95562AC,
	U3CIntervalUpdateU3Ed__8_System_IDisposable_Dispose_m938B36B9E9E68F9FE94E5F6031E53984C29619D6,
	U3CIntervalUpdateU3Ed__8_MoveNext_mCB18228CAB537933E04FBC9F2BCF7CBC745AEACC,
	U3CIntervalUpdateU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69063FDBF5B4831E0C85C71F09F5244D00998219,
	U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_Reset_m158FF84C726C4B2D5EE0C60A67E4A41B6595A38C,
	U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_get_Current_mEA80CCCA18AB0E4401F2CC817C77857E79112045,
	SaveUIComponent_Awake_mAFE026354C38B1190D5F942CD40B07BBF353B0F1,
	SaveUIComponent_SaveFloat_mDF05346AF9A6C4CCF1D32EA3873E62C06D27D0EB,
	SaveUIComponent_LoadFloat_m932BEF92B3EDEF3A3463BB7F3A3684BA0C30164D,
	SaveUIComponent_SaveInt_mD59DB6A1B116A2D806D4F2DFAEDA0FBE20FDE0EB,
	SaveUIComponent_LoadInt_m3015EE758718251772CB74B6CCC896F89216A7A1,
	SaveUIComponent_SaveString_m08D3A82C30EFAC91D3312612A67FC41F205CA14D,
	SaveUIComponent_LoadString_mB9757D06EF34727F70404A972849E0252B60D441,
	SaveUIComponent_SaveBool_m02363995C5FD0958AAC0A0B777F68C42B42718BE,
	SaveUIComponent_LoadBool_mD2CD7C2337FDAC49F5E5043FE3761FE68DF5BFC6,
	SaveUIComponent_OnValidate_m1EB8EFAB390B009E9468EFF051D7B5AD963DDC3B,
	SaveUIComponent__ctor_mB3FE1C719D07D7A2873F1A42F1B7D2B118B8E82C,
	SelectableObject_get_position_m5175DBA2D06D0FB2B3A819D8D0F0551B7B7FC4F7,
	SelectableObject_get_Callbacks_m5427B5B0F58943BF078830E59F92355297188893,
	SelectableObject_Awake_mC4B94D44BFA88AF36A95D4869CE71730AF07BDB2,
	SelectableObject_Start_m800CCD90AF2309DE1AF50BBF14CC6DC334186EB8,
	SelectableObject_OnSelect_mF2B15842F9903F81F4637BE61F413C8BC359D620,
	SelectableObject_OnDeselect_mFDBCD4FA9D25D79F9093BEC52E94B8255E11B878,
	SelectableObject_OnDestroy_mC5C5BDC7B1BDA308E3834AC23A50F70F620C9792,
	SelectableObject__ctor_mB97342D85B2894F200C3EC7FF0B54A221FE419CF,
	SelectableObject_DevionGames_ISelectable_get_enabled_m00062F17363403BC0B43D5852069EC7BC0C81413,
	SelectableObjectName_Start_m4B5E0D01F140AD01DAF863876054348F5F99D185,
	SelectableObjectName_Update_mD829B5C3E085E6967299027720E783583F0DCF6A,
	SelectableObjectName__ctor_m65E62BA557054C158F811B640C781CD4C9498CB1,
	SelectionHandler_get_Callbacks_m0A3B8C870F5273546093F68C73DF3D1DFFFC67C2,
	SelectionHandler_Start_m7EEE663D63DDF6D4CB4FA665A79B06728BD62A3D,
	SelectionHandler_CustomUpdate_mD3293975B972B1EA362FE03EAA6675E6C15999F2,
	SelectionHandler_Update_m876D4FD371D197BCF4370CA2695D19AC8458248F,
	SelectionHandler_TrySelect_m1D37BD0E0189B345E9C6F1763715E9ADECD3C68D,
	SelectionHandler_Select_mC8156B81B12567D0CE50F95AC1ABDCFF9B8EE1F2,
	SelectionHandler_Deselect_m8EB4557FEBBF0C2DC4759BCEF2FE92E9C08CE1A1,
	SelectionHandler_GetBestSelectable_m49716D029E39A18B370FCC979561FCE550064C15,
	SelectionHandler__ctor_m57683EAA1E9336236A73B2611425E3315D4A07EE,
	U3CU3Ec__cctor_mE0F5E1191886E2BD2C53EF4EF4B89643BE9D5A7F,
	U3CU3Ec__ctor_m05D278ED51668A833F88B459E7BDF940777CDC96,
	U3CU3Ec_U3CUpdateU3Eb__18_0_mD3124141D66FD419F997B75BCB196A2A38819E4F,
	U3CU3Ec_U3CUpdateU3Eb__18_1_m5CD999EC3F470EEF00E107E2051EA1FF9A104BC0,
	SetCursorLockState_get_Callbacks_mA96DBE6A6E33D577180D2317CAA708A189858A90,
	SetCursorLockState_Update_m8979183533008CB6D34FD9BBA8680DAF3A417B4A,
	SetCursorLockState__ctor_mAC9FBD0F3C1F5C10371B384AE6681CEE1374F0E3,
	SimpleMinimap_Start_m5AA95357A330718AD811887A009D326A4E82E659,
	SimpleMinimap_Update_mC4FFD1BBDC82AF34A21B4F44ABA5D999BBC9CA67,
	SimpleMinimap__ctor_mD458A021ED64D84B9D1A758EFAB75B9FEFC0B800,
	SingleInstance_Awake_m36F22DE7EBFB81C12CAE0EAED86890FBEB7843FC,
	SingleInstance_GetInstanceObjects_m1411E97ED72361656A6819348A8167F1F8488751,
	SingleInstance__ctor_mEE8E06661154B077A7D15FED40C59DD383AA9628,
	SingleInstance__cctor_m5AD86C4A6E93F0C198B8D98F5EC5D3553B228E29,
	U3CU3Ec__cctor_mD36BDDBDB840EE359E9B7AAE0EF7A6B9887B8879,
	U3CU3Ec__ctor_mF9CDE6BA7A32788543E76E76BF7828A1AAB72E10,
	U3CU3Ec_U3CGetInstanceObjectsU3Eb__2_0_m602557116183F00C23ED1CD20740DDCC92FF8CB7,
	TimedDestroy_Start_mEE92664FA2142DA2498F6CA5D17A402CB0EF473D,
	TimedDestroy__ctor_mAA2D1CCB0F16B25360FF396D5D50450C88753BDA,
	U3CStartU3Ed__2__ctor_m7225B152290395438805F0521BA532F23E6B00E1,
	U3CStartU3Ed__2_System_IDisposable_Dispose_mAE86168E862DE97101A48AFA3587AADA5E43F452,
	U3CStartU3Ed__2_MoveNext_m6494F59687CA972FBE5F523571F420D94BA909AF,
	U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD36C0E919D3C31F79BBCC4389881C2BED18A196C,
	U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mE031A47C4C3F5D56B6551FCC569394B5E232A9CD,
	U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m3E74EBF644324A5D42FAF8353F61C09FC047608A,
	TimedEnable_OnEnable_m516AFD10DD3C1231F03DACDB7135E7A2EAA6F6CC,
	TimedEnable_WaitAndSetEnabled_m5518E3F9CECA33EB77D46E4D85B003C30614045C,
	TimedEnable__ctor_m27BE87FDAEE2D0BDF39FC9209DEE5AC2FB69D70D,
	U3CWaitAndSetEnabledU3Ed__4__ctor_mC300D871D35BC85F42A31B653EC7EB79ECC920CA,
	U3CWaitAndSetEnabledU3Ed__4_System_IDisposable_Dispose_mA75D4D3AC769C6B475A840A7950BBBC00120B27F,
	U3CWaitAndSetEnabledU3Ed__4_MoveNext_mDF45F77E9D4E16CBC8B50F7CAEF0045B3A8028E9,
	U3CWaitAndSetEnabledU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB87EB7D92D24427B6B9AFDACCC861D3682416F68,
	U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_Reset_m1A49C73502745B49FD532606CA5CFFCA5E644573,
	U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_get_Current_m06302F4BD4A6E352124BFED7190F24F7B4069423,
	UnityTools_get_Handler_mFF9B9477D3F507C0999C7DBEF92FE6C165FD8809,
	UnityTools_PlaySound_mD3B4B43903581B68713DAB83C2D67A1D1D77C54B,
	UnityTools_IsPointerOverUI_mD2D60DB893A1AEC2AB2E19E0C3EBF351A12E7595,
	UnityTools_ColorToHex_mB31E9A44C57C1D8A257CF0D1B058CCD97235E7F2,
	UnityTools_HexToColor_m2542D2DE0F305F210A44077BD2CA3269E37B16B5,
	UnityTools_ColorString_m9335B52921AFB6D29680AA7475F4A538C520864B,
	UnityTools_Replace_m24693C3EE97AE04AE9BC0348382A7A36E9584E90,
	UnityTools_IsNumeric_mABC9475A7E47F3FD45B59BAFC141FAB5FF4284A6,
	UnityTools_IsInteger_m73A21999147BC2DFF761FB2672A72A7829DE7F44,
	UnityTools_IsFloat_mB94371FBCBC59C1053D4C9AD7FAFCE0E389AF153,
	UnityTools_FindChild_m3A4D6BC66B989B7F4BE684488D74F9EBF3E1DB97,
	UnityTools_Stretch_mED921D4043429CE5C026BEE8BFCCD0F7F3730C0D,
	UnityTools_Stretch_mB9ECC09B408195B08506E68C49F33FCEC9B91464,
	NULL,
	UnityTools_IgnoreCollision_m5D0648DA362AAEF8B62231E791BE11A6A8937545,
	UnityTools_GetBounds_mD328546150E05762EA5FCC17FDD95C6026EE8B23,
	UnityTools_KeyToCaption_m981CF5FB3650987E282277DCF8CEA8BFBF4A23BC,
	NULL,
	NULL,
	UnityTools_StartCoroutine_m1E22198CE829DE7B24CFE6B20CAAA0F4113B250B,
	UnityTools_StartCoroutine_m5D8F448E6FB9413DFA61297DC8050A2E511C6D78,
	UnityTools_StartCoroutine_m7B4ACEB8BA2A80CE4F3B8914CAAE3C73504D73D4,
	UnityTools_StopCoroutine_mF60F6A13C8058414C40AAAD2009384A2141F4122,
	UnityTools_StopCoroutine_mD8B00EB510E28C2DCAA829F5F66E7B725682784D,
	UnityTools_StopCoroutine_m1AB7FC8496584FD113311A51FD113F2F32861C26,
	UnityTools_StopAllCoroutines_m28972BDAEF18463F69E509E395ACFDFD4EE2DBF9,
	Utility__cctor_m7EB290B0CD7C93A16B6FC8A57EF15BD480C9F3FF,
	Utility_GetType_mAD63B0BE5B48E1F55B3F87FD74EC23CC91B61FDC,
	Utility_GetElementType_mA52FF5E518F65DD849F9ADE9DA43E72E8BBB7A1B,
	Utility_GetAllMethods_mA26E4013A1C68B62ED790F49F6C0D3DBBAAE9D21,
	Utility_GetSerializedField_m3DFCAF7A79CDB3857A8B3B453E1AE088F665408A,
	Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48,
	Utility_GetSerializedFields_m1D6F2A8FD044D8FF49D5FD9B27E0AB8BA0B2DCB8,
	Utility_BaseTypesAndSelf_mAD2EBB949D685E2D62DED747B3900570BAAD4FF7,
	Utility_BaseTypes_mD100E7396D2DA9A100981F3005331402805CB914,
	Utility_GetCustomAttributes_m1BA602C31499B43DBB54A027A77907E562DAE270,
	NULL,
	NULL,
	NULL,
	Utility_HasAttribute_mD6E30A724B936B8A733B759E7BB8644DB2DF7637,
	Utility_Contains_m5D2130FBCFC37784529F87A9FB57C0113DE69999,
	Utility_GetLoadedAssemblies_mEB7893AB1263758AC5263ECC66CA72F747C85176,
	U3CU3Ec__cctor_m03A3EF69A054EA73BF3D5DC3445952B486770F74,
	U3CU3Ec__ctor_m09A91CA5366576DA19FD4B03F7D4DB0B7FCAA907,
	U3CU3Ec_U3CGetElementTypeU3Eb__7_0_m8047473A8563AE9FF5596A7EEF7DED8B58E3DC63,
	U3CU3Ec_U3CGetElementTypeU3Eb__7_1_mD66FA9A2FDA4B6EA3540BD9C21825D39C5099896,
	U3CU3Ec_U3CGetAllSerializedFieldsU3Eb__10_0_m8E0C3AA2F406B422373814D1466C58980ED5693A,
	U3CU3Ec_U3CGetSerializedFieldsU3Eb__11_0_m4CAEA39D5FED24E3E7BC99758767CCF8DBDB4154,
	U3CU3Ec_U3CGetSerializedFieldsU3Eb__11_1_m06D9CD67DCA805D61C4FEE5B70167664FA84CF53,
	U3CU3Ec__DisplayClass9_0__ctor_mEA93F88E2D52C8E80687A375CDF39BE252E796A6,
	U3CU3Ec__DisplayClass9_0_U3CGetSerializedFieldU3Eb__0_mF979CC08C9D819384398D21D02BEF8CE18195457,
	U3CBaseTypesAndSelfU3Ed__12__ctor_mC16CA045DB26529930070E385A119F5FC28687D8,
	U3CBaseTypesAndSelfU3Ed__12_System_IDisposable_Dispose_m266119160E53905B8BA612E023AEBDC2F1B800E0,
	U3CBaseTypesAndSelfU3Ed__12_MoveNext_mB90BD002079EFFF0326E8E7072847E67159D4D77,
	U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m18C4E6212AD8F107EBAC79DAD65CA1156E82DAC1,
	U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_Reset_m93A48609980842C0527D5EF5AEDBF49ECC60484C,
	U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_get_Current_m632B11DDF786C85B0002A8C4916C13FF49A1F7C3,
	U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m0E1C888F34B391FF5A69ED92DF7FADA9AFFD611F,
	U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m07AA84023DF74C6E52C7DA01FC413312FA9B7F8E,
	U3CBaseTypesU3Ed__13__ctor_mE5EFE42FB994CC40FAA0D05432A1D4E87D246034,
	U3CBaseTypesU3Ed__13_System_IDisposable_Dispose_m90A8E9CA4A62940322702B943C802246F7198F07,
	U3CBaseTypesU3Ed__13_MoveNext_m54435BD974A4519F0EC10D6F2618244F4E20565D,
	U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m478F8E1330BFB7CD178E7001520F4E03ACBC54C7,
	U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_Reset_m656B190DF87DC9078C1AD524603995BF40E749D7,
	U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_get_Current_m989A6A05C7EFB1780FE9588AE15B7020A3D3DDB1,
	U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mFF576A29276D4CBABC0891FB8734F1D5AE81B00A,
	U3CBaseTypesU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB6B2F041696200200F5DEA07D880E2F7C57DA539,
	UtilityBehavior_QuitApplication_mF8D4B67159389D3811F1375EEB91509391CB70A9,
	UtilityBehavior_LoadScene_mB132C6A6A10F5026FEBFD1F4301E0D08CA25E48A,
	UtilityBehavior_Instantiate_mA73BB77B1600ACD5F43262749F141A98BD714613,
	UtilityBehavior__ctor_m1535B0FDCF9455A8B4922CDDFAE9D772EF7DC228,
};
static const int32_t s_InvokerIndices[473] = 
{
	4481,
	4481,
	4518,
	4518,
	4148,
	3329,
	1439,
	1439,
	910,
	910,
	910,
	2448,
	2975,
	2897,
	2434,
	2941,
	2912,
	4478,
	2975,
	2912,
	2448,
	2912,
	2448,
	2448,
	2975,
	2975,
	2448,
	2448,
	2448,
	2448,
	1444,
	1477,
	2975,
	2448,
	2975,
	2912,
	2448,
	1450,
	2975,
	2975,
	2133,
	1894,
	2897,
	2975,
	2912,
	2448,
	2912,
	2448,
	2912,
	2975,
	2448,
	4481,
	4481,
	-1,
	-1,
	-1,
	931,
	2133,
	1898,
	2448,
	-1,
	756,
	2975,
	2975,
	2133,
	2975,
	2133,
	2941,
	2477,
	2912,
	2448,
	2912,
	2975,
	2448,
	4488,
	4518,
	2862,
	2395,
	2912,
	2448,
	2912,
	2975,
	2448,
	4467,
	4366,
	2948,
	2484,
	2912,
	2448,
	2912,
	2975,
	2448,
	4489,
	4532,
	2912,
	2448,
	2912,
	2448,
	2912,
	2975,
	2448,
	4481,
	4481,
	2897,
	2434,
	2912,
	2448,
	2912,
	2975,
	2448,
	4478,
	4412,
	2912,
	2448,
	2912,
	2448,
	2912,
	2975,
	2448,
	4481,
	4481,
	2975,
	2912,
	2448,
	2912,
	2448,
	2912,
	2975,
	2448,
	4481,
	4481,
	2912,
	2448,
	2941,
	2477,
	2941,
	2912,
	2912,
	2448,
	2975,
	2448,
	2968,
	2506,
	2912,
	2448,
	2912,
	2975,
	2448,
	4491,
	4543,
	2970,
	2508,
	2912,
	2448,
	2912,
	2975,
	2448,
	4492,
	4547,
	2975,
	1444,
	1898,
	2912,
	1444,
	1444,
	1444,
	2975,
	2975,
	2975,
	2975,
	3216,
	2975,
	2975,
	2975,
	2975,
	2448,
	2975,
	2975,
	2448,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2975,
	4649,
	4566,
	4276,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4276,
	3926,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4276,
	3926,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4276,
	3926,
	4276,
	3926,
	4481,
	4069,
	2975,
	2897,
	2434,
	2912,
	2448,
	2941,
	2970,
	2975,
	2975,
	2448,
	2448,
	4481,
	4276,
	-1,
	4481,
	4276,
	4481,
	4481,
	4514,
	2448,
	4481,
	2975,
	2912,
	2912,
	2912,
	1894,
	2912,
	2912,
	1916,
	2293,
	2289,
	2281,
	1616,
	2975,
	2896,
	2896,
	2912,
	2897,
	2975,
	4481,
	1439,
	1439,
	1439,
	2448,
	2448,
	2975,
	2975,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2912,
	2448,
	2912,
	2448,
	2897,
	2434,
	2912,
	2912,
	2912,
	2448,
	2912,
	2975,
	2912,
	2448,
	2912,
	2912,
	2448,
	4481,
	1898,
	4622,
	4622,
	2975,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2448,
	2912,
	2912,
	2912,
	2912,
	2912,
	2859,
	2975,
	2975,
	2975,
	2975,
	2912,
	2975,
	2975,
	2912,
	2912,
	2912,
	2133,
	2941,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2484,
	2248,
	2434,
	1771,
	2448,
	1898,
	2477,
	2163,
	2975,
	2975,
	2970,
	2912,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2941,
	2975,
	2975,
	2975,
	2912,
	2975,
	2975,
	2975,
	2147,
	2448,
	2975,
	1898,
	2975,
	4649,
	2975,
	1899,
	2133,
	2912,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	4622,
	2975,
	4649,
	4649,
	2975,
	2133,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	4622,
	3936,
	4636,
	4468,
	4366,
	4061,
	3763,
	4518,
	4518,
	4518,
	3765,
	4276,
	4566,
	-1,
	4276,
	4362,
	4478,
	-1,
	-1,
	4481,
	4069,
	4481,
	4566,
	4566,
	4566,
	4649,
	4649,
	4481,
	4481,
	4481,
	4069,
	4481,
	4481,
	4481,
	4481,
	4070,
	-1,
	-1,
	-1,
	4148,
	4142,
	4622,
	4649,
	2975,
	2133,
	1898,
	1784,
	2133,
	1784,
	2975,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2912,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2912,
	2912,
	2975,
	2448,
	2448,
	2975,
};
static const Il2CppTokenRangePair s_rgctxIndices[17] = 
{
	{ 0x06000036, { 0, 1 } },
	{ 0x06000037, { 1, 1 } },
	{ 0x06000038, { 2, 1 } },
	{ 0x0600003D, { 3, 1 } },
	{ 0x060000C0, { 4, 2 } },
	{ 0x060000C1, { 6, 2 } },
	{ 0x060000C2, { 8, 2 } },
	{ 0x060000C3, { 10, 2 } },
	{ 0x060000C4, { 12, 2 } },
	{ 0x060000C5, { 14, 2 } },
	{ 0x060000E9, { 16, 5 } },
	{ 0x060001A0, { 21, 2 } },
	{ 0x060001A4, { 23, 1 } },
	{ 0x060001A5, { 24, 2 } },
	{ 0x060001B7, { 26, 6 } },
	{ 0x060001B8, { 32, 2 } },
	{ 0x060001B9, { 34, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[35] = 
{
	{ (Il2CppRGCTXDataType)3, 21551 },
	{ (Il2CppRGCTXDataType)2, 67 },
	{ (Il2CppRGCTXDataType)3, 21547 },
	{ (Il2CppRGCTXDataType)1, 65 },
	{ (Il2CppRGCTXDataType)2, 986 },
	{ (Il2CppRGCTXDataType)3, 380 },
	{ (Il2CppRGCTXDataType)2, 985 },
	{ (Il2CppRGCTXDataType)3, 379 },
	{ (Il2CppRGCTXDataType)2, 1057 },
	{ (Il2CppRGCTXDataType)3, 592 },
	{ (Il2CppRGCTXDataType)2, 1056 },
	{ (Il2CppRGCTXDataType)3, 591 },
	{ (Il2CppRGCTXDataType)2, 1087 },
	{ (Il2CppRGCTXDataType)3, 681 },
	{ (Il2CppRGCTXDataType)2, 1086 },
	{ (Il2CppRGCTXDataType)3, 680 },
	{ (Il2CppRGCTXDataType)2, 2968 },
	{ (Il2CppRGCTXDataType)3, 10526 },
	{ (Il2CppRGCTXDataType)1, 184 },
	{ (Il2CppRGCTXDataType)2, 184 },
	{ (Il2CppRGCTXDataType)3, 10527 },
	{ (Il2CppRGCTXDataType)3, 22406 },
	{ (Il2CppRGCTXDataType)2, 295 },
	{ (Il2CppRGCTXDataType)1, 294 },
	{ (Il2CppRGCTXDataType)3, 22719 },
	{ (Il2CppRGCTXDataType)2, 293 },
	{ (Il2CppRGCTXDataType)2, 2986 },
	{ (Il2CppRGCTXDataType)3, 10548 },
	{ (Il2CppRGCTXDataType)1, 314 },
	{ (Il2CppRGCTXDataType)2, 314 },
	{ (Il2CppRGCTXDataType)3, 10549 },
	{ (Il2CppRGCTXDataType)3, 10550 },
	{ (Il2CppRGCTXDataType)1, 313 },
	{ (Il2CppRGCTXDataType)2, 313 },
	{ (Il2CppRGCTXDataType)1, 312 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_Utilities_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_Utilities_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_Utilities_CodeGenModule = 
{
	"DevionGames.Utilities.dll",
	473,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	17,
	s_rgctxIndices,
	35,
	s_rgctxValues,
	NULL,
	g_DevionGames_Utilities_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
