﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean Cooldown::get_IsCoolDown()
extern void Cooldown_get_IsCoolDown_m73568573CDF89BD7B2414CD2F16E2981E7F097E1 (void);
// 0x00000002 System.Void Cooldown::Update()
extern void Cooldown_Update_mFD0E719ACD5054DF57D591A38F3C6BF91F50197F (void);
// 0x00000003 System.Void Cooldown::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void Cooldown_OnPointerClick_mB1E8A16C67F7BCE34199934BA1411FEC51479F73 (void);
// 0x00000004 System.Void Cooldown::DoCooldown(System.Single,System.Single)
extern void Cooldown_DoCooldown_mF8F31907E54EBE051374B5A30F5EECE27B4DD571 (void);
// 0x00000005 System.Void Cooldown::DoGlobalCooldown(System.Single)
extern void Cooldown_DoGlobalCooldown_mE5563FDBF30727988093DF9F72B24E9619241C97 (void);
// 0x00000006 System.Void Cooldown::.ctor()
extern void Cooldown__ctor_m7404B43FFB012AB3C54BA4A3991D3F5943D00AFB (void);
// 0x00000007 System.Void ContextMenuTrigger::Start()
extern void ContextMenuTrigger_Start_m170AA3DA4511EDBBAB31543A0D85CF7F0D2476C7 (void);
// 0x00000008 System.Void ContextMenuTrigger::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ContextMenuTrigger_OnPointerDown_m0B288937B9E2A8C561287AB6E8ECEE7DFB784B08 (void);
// 0x00000009 System.Void ContextMenuTrigger::.ctor()
extern void ContextMenuTrigger__ctor_m0CF6940F1CD5F456F29D1EB2D6BA39F71F07E3D0 (void);
// 0x0000000A System.Void ContextMenuTrigger/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mEC535FD7763138804E7E0092A43FC6E682DCC478 (void);
// 0x0000000B System.Void ContextMenuTrigger/<>c__DisplayClass3_0::<OnPointerDown>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3COnPointerDownU3Eb__0_m7819F8DA6AB371A7917D4D143321C92D74667511 (void);
// 0x0000000C System.Void DialogBoxTrigger::Start()
extern void DialogBoxTrigger_Start_m84D6AE20B404E699643ABF1968FED21244E3D8B6 (void);
// 0x0000000D System.Void DialogBoxTrigger::Show()
extern void DialogBoxTrigger_Show_m73155C5B98109262E20263FD39842C159FA37FC9 (void);
// 0x0000000E System.Void DialogBoxTrigger::ShowWithCallback()
extern void DialogBoxTrigger_ShowWithCallback_mCCA1C4B788F2C3BA90418061F7D22FD37320973B (void);
// 0x0000000F System.Void DialogBoxTrigger::OnDialogResult(System.Int32)
extern void DialogBoxTrigger_OnDialogResult_m25280F538EEC55301BE84308FEF894CDA855C7E5 (void);
// 0x00000010 System.Void DialogBoxTrigger::.ctor()
extern void DialogBoxTrigger__ctor_mC2C39738E3FF4DBE8E3282D6337C358DE06E33E1 (void);
// 0x00000011 System.Void NotificationTrigger::Start()
extern void NotificationTrigger_Start_m073E7887BABD374DAEB286DD507F7228573F3C83 (void);
// 0x00000012 System.Void NotificationTrigger::AddRandomNotification()
extern void NotificationTrigger_AddRandomNotification_m4F49DD95F4591365BD79C01953B85DB8AB21D90E (void);
// 0x00000013 System.Void NotificationTrigger::AddNotification(UnityEngine.UI.InputField)
extern void NotificationTrigger_AddNotification_m12C2CC7C75E1E35E0B90BAAA5B2347BBC99971F5 (void);
// 0x00000014 System.Void NotificationTrigger::AddNotification(System.Single)
extern void NotificationTrigger_AddNotification_m608FE8D6D7061B69193E022A6E9CC95452C83030 (void);
// 0x00000015 System.Void NotificationTrigger::.ctor()
extern void NotificationTrigger__ctor_m9AA3653E5A3635E79B55FCC26DFA5D899030715E (void);
// 0x00000016 System.Void RadialMenuTrigger::Start()
extern void RadialMenuTrigger_Start_m66A735291156902C99F4C8C0EEC103B9F7EC325A (void);
// 0x00000017 System.Void RadialMenuTrigger::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RadialMenuTrigger_OnPointerDown_mA0E9853FA88A661D2407416BB752E9839FE65960 (void);
// 0x00000018 System.Void RadialMenuTrigger::.ctor()
extern void RadialMenuTrigger__ctor_mA6651BCBD32B93E382D610623B56165B63D052FF (void);
// 0x00000019 System.Void RadialMenuTrigger/<>c::.cctor()
extern void U3CU3Ec__cctor_m531831D00E7AC4531B9A0F9F79ACAD1C8069D527 (void);
// 0x0000001A System.Void RadialMenuTrigger/<>c::.ctor()
extern void U3CU3Ec__ctor_m3874D5CF04172132EC9F262D3970EF4B266CE225 (void);
// 0x0000001B System.Void RadialMenuTrigger/<>c::<OnPointerDown>b__3_0(System.Int32)
extern void U3CU3Ec_U3COnPointerDownU3Eb__3_0_m41DE04D1852B78BB6B412A02C0D21B6607A0FB0B (void);
// 0x0000001C System.Void DevionGames.UIWidgets.AnimateRawImage::Start()
extern void AnimateRawImage_Start_m012EF1F43B7782FF9AE363156C883ADCAD635439 (void);
// 0x0000001D System.Void DevionGames.UIWidgets.AnimateRawImage::Update()
extern void AnimateRawImage_Update_mA52CDC0E999ADA67DFB9412A93107054C5F9DABD (void);
// 0x0000001E System.Void DevionGames.UIWidgets.AnimateRawImage::.ctor()
extern void AnimateRawImage__ctor_mB004EAB319D6EF7469BAC1850D264D2DB5D71531 (void);
// 0x0000001F System.Void DevionGames.UIWidgets.Chat::OnStart()
extern void Chat_OnStart_mFF6CD1A4C28863996872CF3F46F2B7FD6AE85B87 (void);
// 0x00000020 System.Void DevionGames.UIWidgets.Chat::Submit(System.String)
extern void Chat_Submit_m15E66E4BDABEF8DA3D13868005BB065D6A3BB069 (void);
// 0x00000021 System.Void DevionGames.UIWidgets.Chat::OnSubmit(System.String)
extern void Chat_OnSubmit_mA7B784F7CAFB74DF8490BF0D4767714AFA253CD7 (void);
// 0x00000022 System.String DevionGames.UIWidgets.Chat::ApplyFilter(System.String)
extern void Chat_ApplyFilter_mDE0624BDC906F737114673F4F45FEE1CFFE82B28 (void);
// 0x00000023 System.Void DevionGames.UIWidgets.Chat::.ctor()
extern void Chat__ctor_m05E5B630B65AB241D7C2C8EC890D0477D48DF9FD (void);
// 0x00000024 System.Void DevionGames.UIWidgets.Chat::<OnStart>b__6_0()
extern void Chat_U3COnStartU3Eb__6_0_m3F9CA70497463E6A6E0A11EA176E9EBCE59CE02A (void);
// 0x00000025 System.Void DevionGames.UIWidgets.ContextMenu::Show()
extern void ContextMenu_Show_mFFBCF2896931C0EBEE4AB5F69CAE6E1444D22277 (void);
// 0x00000026 System.Void DevionGames.UIWidgets.ContextMenu::Update()
extern void ContextMenu_Update_mFA3D8E0ECC19EFA5DC96FF0FA67305BD70231248 (void);
// 0x00000027 System.Void DevionGames.UIWidgets.ContextMenu::Clear()
extern void ContextMenu_Clear_m3727DEC3FFDBFAB8DB453FDE6E0841A908348958 (void);
// 0x00000028 DevionGames.UIWidgets.MenuItem DevionGames.UIWidgets.ContextMenu::AddMenuItem(System.String,UnityEngine.Events.UnityAction)
extern void ContextMenu_AddMenuItem_m47C561A3F529F55CE0A401B0EBCE5CDBBB43783B (void);
// 0x00000029 System.Void DevionGames.UIWidgets.ContextMenu::.ctor()
extern void ContextMenu__ctor_mB85995A02987AC50C1DBED6FB233CCECE8F7742B (void);
// 0x0000002A System.Void DevionGames.UIWidgets.ContextMenu/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mAA49C078B22AE5A9955338DE9BBCBE5C05DA51B3 (void);
// 0x0000002B System.Void DevionGames.UIWidgets.ContextMenu/<>c__DisplayClass5_0::<AddMenuItem>b__1()
extern void U3CU3Ec__DisplayClass5_0_U3CAddMenuItemU3Eb__1_m8B3F76895CDE6FC0804A140A80CDBCDA88CB1FF1 (void);
// 0x0000002C System.Void DevionGames.UIWidgets.ContextMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m87B27DF16FA6DC0402043E0B3377E8BBCC4D701C (void);
// 0x0000002D System.Void DevionGames.UIWidgets.ContextMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_m3303C0188F91DA0DE4262AF4C1143412E6BA0817 (void);
// 0x0000002E System.Boolean DevionGames.UIWidgets.ContextMenu/<>c::<AddMenuItem>b__5_0(DevionGames.UIWidgets.MenuItem)
extern void U3CU3Ec_U3CAddMenuItemU3Eb__5_0_m7BCF9CFE0D7796A4FA4BD627D56080D2C8594861 (void);
// 0x0000002F System.Void DevionGames.UIWidgets.DialogBox::OnAwake()
extern void DialogBox_OnAwake_mFCE03E18FE368AA420A3C121C7C9DE2E1384A057 (void);
// 0x00000030 System.Void DevionGames.UIWidgets.DialogBox::Show(DevionGames.UIWidgets.NotificationOptions,UnityEngine.Events.UnityAction`1<System.Int32>,System.String[])
extern void DialogBox_Show_mC24BE23C2C0B4C61FD24D23149B66CD070814D4F (void);
// 0x00000031 System.Void DevionGames.UIWidgets.DialogBox::Show(System.String,System.String,System.String[])
extern void DialogBox_Show_m65033F147B563EAFC7DC9A5C2D1124EB28176757 (void);
// 0x00000032 System.Void DevionGames.UIWidgets.DialogBox::Show(System.String,System.String,UnityEngine.Events.UnityAction`1<System.Int32>,System.String[])
extern void DialogBox_Show_m29679F5AD10130585BB848ED347461F89932B474 (void);
// 0x00000033 System.Void DevionGames.UIWidgets.DialogBox::Show(System.String,System.String,UnityEngine.Sprite,UnityEngine.Events.UnityAction`1<System.Int32>,System.String[])
extern void DialogBox_Show_m9EE2AB39BFE7EA8D3551D237A37E43A155468460 (void);
// 0x00000034 UnityEngine.UI.Button DevionGames.UIWidgets.DialogBox::AddButton(System.String)
extern void DialogBox_AddButton_m90C82A8819FDE717FF6BF4AF20A7E369D696EA3E (void);
// 0x00000035 System.Void DevionGames.UIWidgets.DialogBox::.ctor()
extern void DialogBox__ctor_mFB4BB7615E9E5BFF1657F70B985A3D0E17EE84B5 (void);
// 0x00000036 System.Void DevionGames.UIWidgets.DialogBox::<>n__0()
extern void DialogBox_U3CU3En__0_m22421082B06171A00E7C8468E080BC9D2D8112D4 (void);
// 0x00000037 System.Void DevionGames.UIWidgets.DialogBox/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mECC5C6B45C792556D15FF978DF85CEC19370F5B8 (void);
// 0x00000038 System.Void DevionGames.UIWidgets.DialogBox/<>c__DisplayClass11_1::.ctor()
extern void U3CU3Ec__DisplayClass11_1__ctor_m16D09220D81C5EF732282F017E81F796F265C681 (void);
// 0x00000039 System.Void DevionGames.UIWidgets.DialogBox/<>c__DisplayClass11_1::<Show>b__0()
extern void U3CU3Ec__DisplayClass11_1_U3CShowU3Eb__0_m18E49568C6729F62EA15EA8D3B1B13E374E3917C (void);
// 0x0000003A System.Void DevionGames.UIWidgets.DialogBox/<>c::.cctor()
extern void U3CU3Ec__cctor_mD5FF6584BBAEDAE48DC6C4DE48C7BE9163C2B505 (void);
// 0x0000003B System.Void DevionGames.UIWidgets.DialogBox/<>c::.ctor()
extern void U3CU3Ec__ctor_m1B21A29526FFC89B2BCADB28E31A877C049B0343 (void);
// 0x0000003C System.Boolean DevionGames.UIWidgets.DialogBox/<>c::<AddButton>b__12_0(UnityEngine.UI.Button)
extern void U3CU3Ec_U3CAddButtonU3Eb__12_0_mB7719FB5D5C09B9560C76F7E17EB3676C46BD201 (void);
// 0x0000003D System.Void DevionGames.UIWidgets.DragPanel::Awake()
extern void DragPanel_Awake_mCEC3EFDE078650597701B6143894F570E7603E3F (void);
// 0x0000003E System.Void DevionGames.UIWidgets.DragPanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnPointerDown_mFF9F34CF85A195252D2F9F1BDCC7F899DD2CCFD1 (void);
// 0x0000003F System.Void DevionGames.UIWidgets.DragPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_OnDrag_m12F44736E5964D022E0A56E5BBFF04242CBF56BA (void);
// 0x00000040 UnityEngine.Vector2 DevionGames.UIWidgets.DragPanel::ClampToWindow(UnityEngine.EventSystems.PointerEventData)
extern void DragPanel_ClampToWindow_m4666F4A71EB3BF5BAA87076957E251AF40C5EC8F (void);
// 0x00000041 System.Void DevionGames.UIWidgets.DragPanel::.ctor()
extern void DragPanel__ctor_m5C5C6358B652E132FA6EEE11757ED3C5A1853250 (void);
// 0x00000042 System.Void DevionGames.UIWidgets.FloatingText::Awake()
extern void FloatingText_Awake_m8EEDC0A55D5E79F62FB309D2BC50517CE1BD1755 (void);
// 0x00000043 System.Void DevionGames.UIWidgets.FloatingText::LateUpdate()
extern void FloatingText_LateUpdate_mA93D4F71DDB20ADBEDC3835B0A2F56F8ED82BDE8 (void);
// 0x00000044 System.Void DevionGames.UIWidgets.FloatingText::SetText(UnityEngine.Transform,System.String,UnityEngine.Color,UnityEngine.Vector3)
extern void FloatingText_SetText_m877938FE22AE975B6999ED5DA322F06DB084B089 (void);
// 0x00000045 System.Void DevionGames.UIWidgets.FloatingText::.ctor()
extern void FloatingText__ctor_mB469FE8D67D72956FA7712D1D710EF59F76AD609 (void);
// 0x00000046 System.Void DevionGames.UIWidgets.FloatingTextManager::Awake()
extern void FloatingTextManager_Awake_mA1E68F55D1F2DC5E844A4C480F4C3582169CC21F (void);
// 0x00000047 System.Void DevionGames.UIWidgets.FloatingTextManager::Add(UnityEngine.GameObject,System.String,UnityEngine.Color,UnityEngine.Vector3)
extern void FloatingTextManager_Add_m06E6F93434AC0F0F182BB6D8BB9B5EDFDA557CC3 (void);
// 0x00000048 System.Void DevionGames.UIWidgets.FloatingTextManager::Remove(UnityEngine.GameObject)
extern void FloatingTextManager_Remove_m0439BD708813F4CF97666D5E8471A249B3B180D8 (void);
// 0x00000049 System.Void DevionGames.UIWidgets.FloatingTextManager::.ctor()
extern void FloatingTextManager__ctor_mB1650C2D78EC15E26686EA60CFA162A97BE21ED8 (void);
// 0x0000004A System.Void DevionGames.UIWidgets.FloatingTextManager::.cctor()
extern void FloatingTextManager__cctor_mF422DD5587CA1C377F35A361889F92516BA91054 (void);
// 0x0000004B System.Void DevionGames.UIWidgets.HorizontalCompass::Start()
extern void HorizontalCompass_Start_m9D9365B85813440343BCA779263F548008D27300 (void);
// 0x0000004C System.Void DevionGames.UIWidgets.HorizontalCompass::Update()
extern void HorizontalCompass_Update_m93423DFB5CB12DA83AD5E2DC604C2BE4F127BFC7 (void);
// 0x0000004D System.Void DevionGames.UIWidgets.HorizontalCompass::.ctor()
extern void HorizontalCompass__ctor_m71F8F6F5B8B264A2B1BA67B7424956856407F498 (void);
// 0x0000004E System.Boolean DevionGames.UIWidgets.IValidation`1::Validate(T)
// 0x0000004F UnityEngine.Vector2 DevionGames.UIWidgets.Joystick::get_position()
extern void Joystick_get_position_m390650DB815A6124E96EDB0BD0AB2CEC88950D54 (void);
// 0x00000050 System.Void DevionGames.UIWidgets.Joystick::Start()
extern void Joystick_Start_mAF35CEE5345DA532B2C55D15F8C10BD74CD98979 (void);
// 0x00000051 System.Void DevionGames.UIWidgets.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnDrag_mB58C2EC401953F2CB2A13F0CFA60260A146DEBAA (void);
// 0x00000052 System.Void DevionGames.UIWidgets.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerDown_m9021414382BBC67E7B9FD1E12972D8D5BF6F5A2C (void);
// 0x00000053 System.Void DevionGames.UIWidgets.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void Joystick_OnPointerUp_mC516EBA9BFDD22057F59A6279A3C50CDC1D396D6 (void);
// 0x00000054 System.Void DevionGames.UIWidgets.Joystick::Update()
extern void Joystick_Update_m933849F08D227A79FE6901247F5CC020088366F5 (void);
// 0x00000055 System.Void DevionGames.UIWidgets.Joystick::.ctor()
extern void Joystick__ctor_m9495DE01496E7A44DFC162A646379CF9C5CA8950 (void);
// 0x00000056 System.Void DevionGames.UIWidgets.Joystick/JoystickEvent::.ctor()
extern void JoystickEvent__ctor_mF0E1BD657D265382052866039D0E773C231EBAA8 (void);
// 0x00000057 UnityEngine.Events.UnityEvent DevionGames.UIWidgets.MenuItem::get_onTrigger()
extern void MenuItem_get_onTrigger_mA321E3B33EFBB90EA3C55828ABBD1A2562E8BF76 (void);
// 0x00000058 System.Void DevionGames.UIWidgets.MenuItem::set_onTrigger(UnityEngine.Events.UnityEvent)
extern void MenuItem_set_onTrigger_mCDF6B7AD05496985504D9EE1366D1A3A44B51BE1 (void);
// 0x00000059 System.Void DevionGames.UIWidgets.MenuItem::Press()
extern void MenuItem_Press_m0DF9DC9268BE82E2B9EC1D377589F5A4608B2A83 (void);
// 0x0000005A System.Void DevionGames.UIWidgets.MenuItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void MenuItem_OnPointerClick_m38F5FD8BEE3EC1513ADE018CF7DFF7422DE462B4 (void);
// 0x0000005B System.Void DevionGames.UIWidgets.MenuItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void MenuItem_OnPointerEnter_mC70E7E41D196D71AA3C0DBDAE71D290EA56FC5ED (void);
// 0x0000005C System.Void DevionGames.UIWidgets.MenuItem::.ctor()
extern void MenuItem__ctor_m8A9DEBBABD7EDE931DB7ECF916BF2E3A0F65FC50 (void);
// 0x0000005D System.Boolean DevionGames.UIWidgets.Notification::AddItem(DevionGames.UIWidgets.NotificationOptions,System.String[])
extern void Notification_AddItem_mB6F6943FD716314F45BF3BD5E099DE7FFFA4C073 (void);
// 0x0000005E System.Boolean DevionGames.UIWidgets.Notification::AddItem(System.String,System.String[])
extern void Notification_AddItem_m661D7EEB2F92E26A6F8013A135FC116D9FCCDF8B (void);
// 0x0000005F System.Boolean DevionGames.UIWidgets.Notification::CanAddItem(DevionGames.UIWidgets.NotificationOptions,DevionGames.UIWidgets.UISlot`1<DevionGames.UIWidgets.NotificationOptions>&,System.Boolean)
extern void Notification_CanAddItem_m1B60DA3020A1805E349CF0EA056DFA573022978F (void);
// 0x00000060 System.Void DevionGames.UIWidgets.Notification::.ctor()
extern void Notification__ctor_m60989F008960E012D4F10C8FD897A9EE1873BBF5 (void);
// 0x00000061 System.Void DevionGames.UIWidgets.NotificationOptions::.ctor(DevionGames.UIWidgets.NotificationOptions)
extern void NotificationOptions__ctor_mCC89E14FDC37EC8CCAE24ECBFFDF4AE2E19BA04E (void);
// 0x00000062 System.Void DevionGames.UIWidgets.NotificationOptions::.ctor()
extern void NotificationOptions__ctor_m1A4308800A2DA93BB9419B90E3666B8424EE1067 (void);
// 0x00000063 System.Void DevionGames.UIWidgets.NotificationSlot::Repaint()
extern void NotificationSlot_Repaint_m1D58D29FC1D3E1C000AD3AB7F71E69C672905809 (void);
// 0x00000064 System.Void DevionGames.UIWidgets.NotificationSlot::DelayCrossFade(UnityEngine.UI.Graphic,DevionGames.UIWidgets.NotificationOptions)
extern void NotificationSlot_DelayCrossFade_m057D06679446D003369D6582B06181A5988D9893 (void);
// 0x00000065 System.Collections.IEnumerator DevionGames.UIWidgets.NotificationSlot::DelayCrossFade(UnityEngine.UI.Graphic,System.Single,System.Single,System.Boolean)
extern void NotificationSlot_DelayCrossFade_m6F7ABE7FFFB3561DE95092EDAD10CB573B0FCCD5 (void);
// 0x00000066 System.Void DevionGames.UIWidgets.NotificationSlot::.ctor()
extern void NotificationSlot__ctor_m932A6ED57D98D60BB9177C6A75E975204DC38518 (void);
// 0x00000067 System.Void DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::.ctor(System.Int32)
extern void U3CDelayCrossFadeU3Ed__5__ctor_m7561C710C476831299D12A048E7370021E44A700 (void);
// 0x00000068 System.Void DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::System.IDisposable.Dispose()
extern void U3CDelayCrossFadeU3Ed__5_System_IDisposable_Dispose_mC49A564E2431BEB17C9C1B8EC89C866C456269B1 (void);
// 0x00000069 System.Boolean DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::MoveNext()
extern void U3CDelayCrossFadeU3Ed__5_MoveNext_mE93ABBCAD5AE0C55ED51209D9E589D7DA59B703F (void);
// 0x0000006A System.Object DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayCrossFadeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BBF68BDD827FB3FEC158DBEE89016274372D1E0 (void);
// 0x0000006B System.Void DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::System.Collections.IEnumerator.Reset()
extern void U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_Reset_m4AB5F3841F027E448E456C6E561CE2D0BCD08DC3 (void);
// 0x0000006C System.Object DevionGames.UIWidgets.NotificationSlot/<DelayCrossFade>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_get_Current_m2381C612381549E92122E9AD564C8777E90E48DC (void);
// 0x0000006D System.Void DevionGames.UIWidgets.Progressbar::OnStart()
extern void Progressbar_OnStart_m669E838CA384436217BC260D255CCC0C95426082 (void);
// 0x0000006E System.Void DevionGames.UIWidgets.Progressbar::SetProgress(System.Single)
extern void Progressbar_SetProgress_m4A94EDF6AF68785BB2C19BBAE8F7DD11EE2A1E43 (void);
// 0x0000006F System.Void DevionGames.UIWidgets.Progressbar::Show()
extern void Progressbar_Show_m5A879C13A1EB6C573FD344AA2F71B9A3D220FF0A (void);
// 0x00000070 System.Void DevionGames.UIWidgets.Progressbar::Show(System.String)
extern void Progressbar_Show_mC7891DA8BC2EED84F0469C26E6872C51FAA101C6 (void);
// 0x00000071 System.Void DevionGames.UIWidgets.Progressbar::.ctor()
extern void Progressbar__ctor_m259FF7CCC442FEA33812C1455A368EC0DD570656 (void);
// 0x00000072 System.Void DevionGames.UIWidgets.RadialMenu::Update()
extern void RadialMenu_Update_mD82AAA9253F732A68CE031739246BAC7A3BD1C4F (void);
// 0x00000073 System.Void DevionGames.UIWidgets.RadialMenu::Show(UnityEngine.GameObject,UnityEngine.Sprite[],UnityEngine.Events.UnityAction`1<System.Int32>)
extern void RadialMenu_Show_m9F278454880A13C4D38666ED4FD258DE98D57EF7 (void);
// 0x00000074 System.Void DevionGames.UIWidgets.RadialMenu::Close()
extern void RadialMenu_Close_m8AE726F8A3767756E4944F9760CD9F5DBE991A9E (void);
// 0x00000075 System.Void DevionGames.UIWidgets.RadialMenu::Show()
extern void RadialMenu_Show_m3F83A4BD09B3D15F16C468F87CF2ECCAB2DE4324 (void);
// 0x00000076 DevionGames.UIWidgets.MenuItem DevionGames.UIWidgets.RadialMenu::AddMenuItem(UnityEngine.Sprite)
extern void RadialMenu_AddMenuItem_mEA5246B804E4AC49521F0B061032162DC1F1FC76 (void);
// 0x00000077 System.Void DevionGames.UIWidgets.RadialMenu::.ctor()
extern void RadialMenu__ctor_mF7E6C2F018219AC87B20CA34BFF937DF069E4EDD (void);
// 0x00000078 System.Void DevionGames.UIWidgets.RadialMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_m9D05263F50BA419457200EF786E4842F94461D0C (void);
// 0x00000079 System.Void DevionGames.UIWidgets.RadialMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_m9896A3FECF0201ED05AAF56C17CAB28CC7AFCDFB (void);
// 0x0000007A UnityEngine.GameObject DevionGames.UIWidgets.RadialMenu/<>c::<Update>b__5_0(UnityEngine.EventSystems.RaycastResult)
extern void U3CU3Ec_U3CUpdateU3Eb__5_0_m06D51817C5CF14E860D18EC96446107A52F1AA46 (void);
// 0x0000007B System.Boolean DevionGames.UIWidgets.RadialMenu/<>c::<AddMenuItem>b__9_0(DevionGames.UIWidgets.MenuItem)
extern void U3CU3Ec_U3CAddMenuItemU3Eb__9_0_m1AA85EA16AD203332D456704A038D8503F2C68DD (void);
// 0x0000007C System.Void DevionGames.UIWidgets.RadialMenu/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mDF6A9E5DB2456213B4BDB9393F987F12442D1C12 (void);
// 0x0000007D System.Void DevionGames.UIWidgets.RadialMenu/<>c__DisplayClass6_1::.ctor()
extern void U3CU3Ec__DisplayClass6_1__ctor_m1312870924280D060F4F83AA7742A6E123A23BAD (void);
// 0x0000007E System.Void DevionGames.UIWidgets.RadialMenu/<>c__DisplayClass6_1::<Show>b__0()
extern void U3CU3Ec__DisplayClass6_1_U3CShowU3Eb__0_m7CA83E53B5E8C998CB8C1A684BD15C3C9277E1EC (void);
// 0x0000007F System.Void DevionGames.UIWidgets.ResizePanel::Awake()
extern void ResizePanel_Awake_mDEA10C29978562C4A9062C653871EF904BB0665E (void);
// 0x00000080 System.Void DevionGames.UIWidgets.ResizePanel::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnPointerDown_m0166B820404972EDBC3399EAE9ECC7CE11D623D7 (void);
// 0x00000081 System.Void DevionGames.UIWidgets.ResizePanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ResizePanel_OnDrag_mC7644BC7A9C34D80AD8EFB785493758146A38ABF (void);
// 0x00000082 System.Void DevionGames.UIWidgets.ResizePanel::.ctor()
extern void ResizePanel__ctor_m375895A0D4D1CD583C36B9D0535C0F81A3136294 (void);
// 0x00000083 System.Void DevionGames.UIWidgets.AudioVolume::Start()
extern void AudioVolume_Start_m6A14C229A6BB88D46AC0EB3B0C42216ABA5CC253 (void);
// 0x00000084 System.Void DevionGames.UIWidgets.AudioVolume::SetVolume(System.Single)
extern void AudioVolume_SetVolume_mBE76318B8DDBDB6E05018D8B65DA9A2AA08176BB (void);
// 0x00000085 System.Void DevionGames.UIWidgets.AudioVolume::.ctor()
extern void AudioVolume__ctor_mB5227F7623B28F1B2F19FE796D6681A99F08AC46 (void);
// 0x00000086 System.Void DevionGames.UIWidgets.Fullscreen::Start()
extern void Fullscreen_Start_mF994EB2A23016313301F5FD6EDCC8C37F7DA7F66 (void);
// 0x00000087 System.Void DevionGames.UIWidgets.Fullscreen::SetFullscreen(System.Boolean)
extern void Fullscreen_SetFullscreen_mE08ED96DBA569FB4DC3418F16106774B129455FB (void);
// 0x00000088 System.Void DevionGames.UIWidgets.Fullscreen::.ctor()
extern void Fullscreen__ctor_m1F0DA849CBD021740506BF5E29A79C146CF81878 (void);
// 0x00000089 System.Void DevionGames.UIWidgets.QualityLevel::Start()
extern void QualityLevel_Start_m436AB5B80DDB3C7A940E9AB356A773F8AD34C431 (void);
// 0x0000008A System.Void DevionGames.UIWidgets.QualityLevel::SetQualityLevel(System.Single)
extern void QualityLevel_SetQualityLevel_m6C041017745FB7083A576130439FA4D1750BA77E (void);
// 0x0000008B System.Void DevionGames.UIWidgets.QualityLevel::SetQualityLevel(System.Int32)
extern void QualityLevel_SetQualityLevel_m5C7C187F8DC95C553756C332D10E5F8B02EEF252 (void);
// 0x0000008C System.Void DevionGames.UIWidgets.QualityLevel::IncreaseQualityLevel()
extern void QualityLevel_IncreaseQualityLevel_mC7C90F60FC525C781B04C15319A2BF8547531C71 (void);
// 0x0000008D System.Void DevionGames.UIWidgets.QualityLevel::DecreaseQualityLevel()
extern void QualityLevel_DecreaseQualityLevel_mC32ED83A87315B1D9B94BACD6981775B2B33129C (void);
// 0x0000008E System.Void DevionGames.UIWidgets.QualityLevel::.ctor()
extern void QualityLevel__ctor_mCF87513731396A275E3096774D8FBF98FA96ADFD (void);
// 0x0000008F System.Void DevionGames.UIWidgets.ScreenResolution::Start()
extern void ScreenResolution_Start_mF2E33179EADB3F3853E2CD8FB476A2957C2BA3CD (void);
// 0x00000090 System.Void DevionGames.UIWidgets.ScreenResolution::SetResolution(System.Int32)
extern void ScreenResolution_SetResolution_m9B74CCDAA78D04C9015081A0282B98E90B1A9EC6 (void);
// 0x00000091 System.Void DevionGames.UIWidgets.ScreenResolution::SetResolution(System.Int32,System.Int32)
extern void ScreenResolution_SetResolution_m0343412C0A7470E8EAD08B18CE1554AF5D2D136B (void);
// 0x00000092 System.Void DevionGames.UIWidgets.ScreenResolution::IncreaseResolution()
extern void ScreenResolution_IncreaseResolution_mB7251126C7CCDE260C16A1063823A4F631D624C5 (void);
// 0x00000093 System.Void DevionGames.UIWidgets.ScreenResolution::DecreaseResolution()
extern void ScreenResolution_DecreaseResolution_mBCF685FDEB39026EEBDED0C8068F3FC06E8F2B19 (void);
// 0x00000094 System.Boolean DevionGames.UIWidgets.ScreenResolution::get_IsStandalone()
extern void ScreenResolution_get_IsStandalone_mB0A3F76DD0A6A2B3F863319A1A141B141F71ACD7 (void);
// 0x00000095 System.Void DevionGames.UIWidgets.ScreenResolution::.ctor()
extern void ScreenResolution__ctor_mC3B216AA89EBFE5194FDC8088B034F77A3E02954 (void);
// 0x00000096 System.Void DevionGames.UIWidgets.TooltipTrigger::Start()
extern void TooltipTrigger_Start_mA812E1D09FB0546176032A721127BAC1099987C3 (void);
// 0x00000097 System.Void DevionGames.UIWidgets.TooltipTrigger::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TooltipTrigger_OnPointerEnter_mBD86A38BF9F2B838AD3F397561447A5BFA16F754 (void);
// 0x00000098 System.Void DevionGames.UIWidgets.TooltipTrigger::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TooltipTrigger_OnPointerExit_mE8AE77B6C8248E40A728A8E3D11A13017EFD4270 (void);
// 0x00000099 System.Collections.IEnumerator DevionGames.UIWidgets.TooltipTrigger::DelayTooltip(System.Single)
extern void TooltipTrigger_DelayTooltip_m914A98EBA471C690E95541FDE6B1D3BE664B19F2 (void);
// 0x0000009A System.Void DevionGames.UIWidgets.TooltipTrigger::ShowTooltip()
extern void TooltipTrigger_ShowTooltip_mFEBC939EEF3A9E085C7DE3FFE58A35FE07054257 (void);
// 0x0000009B System.Void DevionGames.UIWidgets.TooltipTrigger::CloseTooltip()
extern void TooltipTrigger_CloseTooltip_m4CC02C1A0F5E899A54EB040FC032A38CE7F4BE26 (void);
// 0x0000009C System.Void DevionGames.UIWidgets.TooltipTrigger::.ctor()
extern void TooltipTrigger__ctor_mC0BC1093492BC61993DA42A4CB0DCD06504ED4F3 (void);
// 0x0000009D System.Void DevionGames.UIWidgets.TooltipTrigger/StringPair::.ctor()
extern void StringPair__ctor_m5AE38B12C303EADA30D89A118AE9135FD85A3CEC (void);
// 0x0000009E System.Void DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::.ctor(System.Int32)
extern void U3CDelayTooltipU3Ed__14__ctor_m039406E2ADA4CA0AD21A0F06D03082D9DC04E5FB (void);
// 0x0000009F System.Void DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::System.IDisposable.Dispose()
extern void U3CDelayTooltipU3Ed__14_System_IDisposable_Dispose_mF0870C95526BAE3BC8B1F2149DC8D405EDD4602C (void);
// 0x000000A0 System.Boolean DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::MoveNext()
extern void U3CDelayTooltipU3Ed__14_MoveNext_mC0405D8FF532553AFEDB304F3F905550294F569D (void);
// 0x000000A1 System.Object DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayTooltipU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DEC7C9FF58B39F959BE337BFBCE491F4BDF6CBB (void);
// 0x000000A2 System.Void DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::System.Collections.IEnumerator.Reset()
extern void U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_Reset_mEA8201EEA14E7762C4E2F9AA327CCEADDCD1B583 (void);
// 0x000000A3 System.Object DevionGames.UIWidgets.TooltipTrigger/<DelayTooltip>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_get_Current_m797FF401C8FF76D65A3F038EC70C2F06F35DD844 (void);
// 0x000000A4 System.Void DevionGames.UIWidgets.WidgetTrigger::Show(System.String)
extern void WidgetTrigger_Show_mF4DC18F76EE7848A5B02136D5678DE0BA32B7357 (void);
// 0x000000A5 System.Void DevionGames.UIWidgets.WidgetTrigger::Close(System.String)
extern void WidgetTrigger_Close_m5C5280BDEF217CEBD157A64CE9DC178551F59CEA (void);
// 0x000000A6 System.Void DevionGames.UIWidgets.WidgetTrigger::.ctor()
extern void WidgetTrigger__ctor_mC35F4F663AE9AB3F650A93B8A68D1E5CFB230B57 (void);
// 0x000000A7 System.Single DevionGames.UIWidgets.Spinner::get_current()
extern void Spinner_get_current_mE89E80DE712D9BF4487556FF3709CC252FD603CE (void);
// 0x000000A8 System.Void DevionGames.UIWidgets.Spinner::set_current(System.Single)
extern void Spinner_set_current_m69664C399FC30601448F44604C22F207382F8800 (void);
// 0x000000A9 System.Void DevionGames.UIWidgets.Spinner::SetCurrent(System.String)
extern void Spinner_SetCurrent_m15BE69EE76FBF8676C46FA4D804A58E1771CBB44 (void);
// 0x000000AA System.Void DevionGames.UIWidgets.Spinner::StartIncrease()
extern void Spinner_StartIncrease_m7126B0B869751CED59F2117117900F220D2ED1BC (void);
// 0x000000AB System.Void DevionGames.UIWidgets.Spinner::StartDecrease()
extern void Spinner_StartDecrease_m778D938C33CC31B61E4510715CDF8B64B200CBB4 (void);
// 0x000000AC System.Void DevionGames.UIWidgets.Spinner::Stop()
extern void Spinner_Stop_m8D73351EF3C0C3BD74ADB3B617E1277827F5D4BB (void);
// 0x000000AD System.Collections.IEnumerator DevionGames.UIWidgets.Spinner::Increase()
extern void Spinner_Increase_mC322782BB67E030A046096639EC45C9D17A41C6D (void);
// 0x000000AE System.Collections.IEnumerator DevionGames.UIWidgets.Spinner::Decrease()
extern void Spinner_Decrease_m85EC7B47F523CEC45A28D681B3CC10EEBD6CB5AE (void);
// 0x000000AF System.Void DevionGames.UIWidgets.Spinner::.ctor()
extern void Spinner__ctor_m8645B4A541257F0887FDF31AD6E38772B6935A98 (void);
// 0x000000B0 System.Void DevionGames.UIWidgets.Spinner/SpinnerEvent::.ctor()
extern void SpinnerEvent__ctor_m25214278EF5D51E8B92762451CA8849C79656713 (void);
// 0x000000B1 System.Void DevionGames.UIWidgets.Spinner/SpinnerTextEvent::.ctor()
extern void SpinnerTextEvent__ctor_m24440A9EF7B43B1E1F47CA02782C41C184EF76E9 (void);
// 0x000000B2 System.Void DevionGames.UIWidgets.Spinner/<Increase>d__15::.ctor(System.Int32)
extern void U3CIncreaseU3Ed__15__ctor_m332B26987322491F88F1ACA5BEC12CDF110CB2C2 (void);
// 0x000000B3 System.Void DevionGames.UIWidgets.Spinner/<Increase>d__15::System.IDisposable.Dispose()
extern void U3CIncreaseU3Ed__15_System_IDisposable_Dispose_m481F5174A264917BBCF83788D1C79981EAA63591 (void);
// 0x000000B4 System.Boolean DevionGames.UIWidgets.Spinner/<Increase>d__15::MoveNext()
extern void U3CIncreaseU3Ed__15_MoveNext_m4533FF1C7F16606ED65A4A630C616C4FEFE575ED (void);
// 0x000000B5 System.Object DevionGames.UIWidgets.Spinner/<Increase>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIncreaseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13FE33844E4F95E9E5D24DE179CB014E77E0922D (void);
// 0x000000B6 System.Void DevionGames.UIWidgets.Spinner/<Increase>d__15::System.Collections.IEnumerator.Reset()
extern void U3CIncreaseU3Ed__15_System_Collections_IEnumerator_Reset_m7248C18ABE14F91F640405744F9C560E91ACA18B (void);
// 0x000000B7 System.Object DevionGames.UIWidgets.Spinner/<Increase>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CIncreaseU3Ed__15_System_Collections_IEnumerator_get_Current_mF3D7E2B8E04DEFCDB6A2AF9082B74E8D059DC47B (void);
// 0x000000B8 System.Void DevionGames.UIWidgets.Spinner/<Decrease>d__16::.ctor(System.Int32)
extern void U3CDecreaseU3Ed__16__ctor_m54DDB1C4E9792F142928896771DAAB3C7AA55E34 (void);
// 0x000000B9 System.Void DevionGames.UIWidgets.Spinner/<Decrease>d__16::System.IDisposable.Dispose()
extern void U3CDecreaseU3Ed__16_System_IDisposable_Dispose_m35A0C2A44B9BE0684A5BDCE1CA243F5F7092587E (void);
// 0x000000BA System.Boolean DevionGames.UIWidgets.Spinner/<Decrease>d__16::MoveNext()
extern void U3CDecreaseU3Ed__16_MoveNext_mD9A6EE4287C0F77DA4B863B21EE999EB87B1D4BE (void);
// 0x000000BB System.Object DevionGames.UIWidgets.Spinner/<Decrease>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDecreaseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8122851D8FBC2D8CAFC52AF57A2467203978278A (void);
// 0x000000BC System.Void DevionGames.UIWidgets.Spinner/<Decrease>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDecreaseU3Ed__16_System_Collections_IEnumerator_Reset_m7ACF8BAEC2101194888F88932511F29761EA369B (void);
// 0x000000BD System.Object DevionGames.UIWidgets.Spinner/<Decrease>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDecreaseU3Ed__16_System_Collections_IEnumerator_get_Current_mEEB8F0EA23ED965BFD6BE4A6E05C1F67F2483401 (void);
// 0x000000BE System.Collections.Generic.KeyValuePair`2<System.String,System.String> DevionGames.UIWidgets.StringPairSlot::get_Target()
extern void StringPairSlot_get_Target_m8C96841652BB94BB7CABA273940138C86D182EB6 (void);
// 0x000000BF System.Void DevionGames.UIWidgets.StringPairSlot::set_Target(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void StringPairSlot_set_Target_mEBFDE0016D1CB5B9C1F37BE40F7E0EE772A88F0C (void);
// 0x000000C0 System.Void DevionGames.UIWidgets.StringPairSlot::Repaint()
extern void StringPairSlot_Repaint_mE601F67504E6977F373F76FBFF8B45795508F39F (void);
// 0x000000C1 System.Void DevionGames.UIWidgets.StringPairSlot::.ctor()
extern void StringPairSlot__ctor_m381230BB39DF44DEF71ED3906092BC9CE0F46222 (void);
// 0x000000C2 System.Void DevionGames.UIWidgets.Tab::Awake()
extern void Tab_Awake_mEA10A2F0DF31A8994EB2E73847FE8174CE3D0B5C (void);
// 0x000000C3 System.Void DevionGames.UIWidgets.Tab::Start()
extern void Tab_Start_mA8332695B3F24A5B2DB2676E40985E1578604609 (void);
// 0x000000C4 System.Void DevionGames.UIWidgets.Tab::Select()
extern void Tab_Select_mDAAC74939F3068482ED5A8EC4A7EE0F84E1189C6 (void);
// 0x000000C5 System.Void DevionGames.UIWidgets.Tab::Deselect(DevionGames.UIWidgets.Tab)
extern void Tab_Deselect_m5E3F7225062A2FA33F2EF1E60DA049E7016A1CFA (void);
// 0x000000C6 System.Void DevionGames.UIWidgets.Tab::.ctor()
extern void Tab__ctor_mA4AE9D5A59310A0C54A0581C537BDCB0E29861FE (void);
// 0x000000C7 System.Void DevionGames.UIWidgets.Tab/TabEvent::.ctor()
extern void TabEvent__ctor_mC9075F53B133074B4BFEA7826A96F8DDC7A28177 (void);
// 0x000000C8 System.Void DevionGames.UIWidgets.Tooltip::OnStart()
extern void Tooltip_OnStart_mEFCD49889B50F24577A861D905149E53356A3E1C (void);
// 0x000000C9 System.Void DevionGames.UIWidgets.Tooltip::Update()
extern void Tooltip_Update_m93A3A39877C43DEA70F962FCC57D282046B7104E (void);
// 0x000000CA System.Void DevionGames.UIWidgets.Tooltip::UpdatePosition()
extern void Tooltip_UpdatePosition_m00B1FD60B932FBFC513F083B6A75ADB5000D035A (void);
// 0x000000CB System.Void DevionGames.UIWidgets.Tooltip::Show()
extern void Tooltip_Show_m590E72FA6BDB88790F5503033BB8A362DC0A401C (void);
// 0x000000CC System.Void DevionGames.UIWidgets.Tooltip::Show(System.String)
extern void Tooltip_Show_m938C9CD44B640F89A126BAC9F5E4A15A9E30C49D (void);
// 0x000000CD System.Void DevionGames.UIWidgets.Tooltip::Show(System.String,System.String)
extern void Tooltip_Show_mF6CF35356F5DE860CE7BB46D55EE48484117C542 (void);
// 0x000000CE System.Void DevionGames.UIWidgets.Tooltip::Show(System.String,System.String,UnityEngine.Sprite,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>)
extern void Tooltip_Show_mEBEEC8B864ED8E4461EDD76C88679F8A572C8ABA (void);
// 0x000000CF System.Void DevionGames.UIWidgets.Tooltip::Show(System.String,System.String,UnityEngine.Sprite,System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>,System.Single,System.Boolean)
extern void Tooltip_Show_m6F3CDAEAD6E05A0482C25696A4893968F502634A (void);
// 0x000000D0 System.Void DevionGames.UIWidgets.Tooltip::Close()
extern void Tooltip_Close_mD93243DBD31E7CEFB0F596E397888EB407C6D10A (void);
// 0x000000D1 DevionGames.UIWidgets.StringPairSlot DevionGames.UIWidgets.Tooltip::CreateSlot()
extern void Tooltip_CreateSlot_m525E1D56281521F01BDD974F8CB13F28C397D6CC (void);
// 0x000000D2 System.Void DevionGames.UIWidgets.Tooltip::.ctor()
extern void Tooltip__ctor_m3EC8D155B71B28C38BAA8AC8A2F9D72FE639BF99 (void);
// 0x000000D3 System.Single DevionGames.UIWidgets.EasingEquations::GetValue(DevionGames.UIWidgets.EasingEquations/EaseType,System.Single,System.Single,System.Single)
extern void EasingEquations_GetValue_m8A9844FEFD063AF19C92CEFD0E84F5ED6BF955B4 (void);
// 0x000000D4 System.Single DevionGames.UIWidgets.EasingEquations::Linear(System.Single,System.Single,System.Single)
extern void EasingEquations_Linear_m8AA418D8174295B504085A6D86D69C03728FF7AC (void);
// 0x000000D5 System.Single DevionGames.UIWidgets.EasingEquations::CLerp(System.Single,System.Single,System.Single)
extern void EasingEquations_CLerp_m724A9F482CA4C5EFEEB7C38C13F2D4CD0F9FA4EF (void);
// 0x000000D6 System.Single DevionGames.UIWidgets.EasingEquations::Spring(System.Single,System.Single,System.Single)
extern void EasingEquations_Spring_mB3A66B66170B55B3D9316F86D643B5A70D5CADA5 (void);
// 0x000000D7 System.Single DevionGames.UIWidgets.EasingEquations::EaseInQuad(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInQuad_m84D48C68A17FCF903B0945BE82B6CE61F9183AEE (void);
// 0x000000D8 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutQuad(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutQuad_m94DFD9A379D6FD8E07FFE2A27B0B74ACF53DA2E4 (void);
// 0x000000D9 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutQuad(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutQuad_m9DC01AF74BBE2BFA1B05224E32F1D8EBAB1C0FCB (void);
// 0x000000DA System.Single DevionGames.UIWidgets.EasingEquations::EaseInCubic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInCubic_m1293CE867EC35E8BC2BDD34E9EB56D4A1851E264 (void);
// 0x000000DB System.Single DevionGames.UIWidgets.EasingEquations::EaseOutCubic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutCubic_m6B776535C2BD3DE309E50CDC42A615025BF81D29 (void);
// 0x000000DC System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutCubic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutCubic_mCEE19E466CD1B13042D6C2DC2C826E4178F97EA4 (void);
// 0x000000DD System.Single DevionGames.UIWidgets.EasingEquations::EaseInQuart(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInQuart_m114DA1D8C44DEA2FD1FC311EA8AFFBEB08CEFF91 (void);
// 0x000000DE System.Single DevionGames.UIWidgets.EasingEquations::EaseOutQuart(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutQuart_mD38A34E0F3F2A5CA1CBBB8A6D678DABBB2C81905 (void);
// 0x000000DF System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutQuart(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutQuart_m0F6B811DEC08B01430B13D83119C30EDA1E5056F (void);
// 0x000000E0 System.Single DevionGames.UIWidgets.EasingEquations::EaseInQuint(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInQuint_mDEE3E587C9A3BFA2D1D610D3216A178C29A2FAA1 (void);
// 0x000000E1 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutQuint(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutQuint_m64408A809BAF50F1FFF4048CDC88B65DC2AF6B46 (void);
// 0x000000E2 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutQuint(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutQuint_mE73C9712D9882A9444BCEE672A15EB08C31852BA (void);
// 0x000000E3 System.Single DevionGames.UIWidgets.EasingEquations::EaseInSine(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInSine_m73B114BF9CC516AE6B6F2AE1DA9A33F071B3E5EA (void);
// 0x000000E4 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutSine(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutSine_m03182B2C83533A6061CB0BD8EFD9712AB8521B3F (void);
// 0x000000E5 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutSine(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutSine_m9F9CDF0DBF51EB150EEC718ACB89A7DE8CBBE5B0 (void);
// 0x000000E6 System.Single DevionGames.UIWidgets.EasingEquations::EaseInExpo(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInExpo_m4942C117C29DCAB5877E79D59D162F87CD5DD9ED (void);
// 0x000000E7 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutExpo(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutExpo_mA71AAE9886C9EE79CAB066F4E3D84E01C090CE4F (void);
// 0x000000E8 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutExpo(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutExpo_m156C792FEE72487A4D361D116CE477A8A5A4B88B (void);
// 0x000000E9 System.Single DevionGames.UIWidgets.EasingEquations::EaseInCirc(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInCirc_m9F17E6BA9F781F0384F9359C6814221970A22019 (void);
// 0x000000EA System.Single DevionGames.UIWidgets.EasingEquations::EaseOutCirc(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutCirc_m4449842668433EFFA97B976620D382704008E205 (void);
// 0x000000EB System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutCirc(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutCirc_mF7770E0532730D38EE1FB3D9B82328A17C43F969 (void);
// 0x000000EC System.Single DevionGames.UIWidgets.EasingEquations::EaseInBounce(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInBounce_mFA84819F21A3A75994EB9ED3EFE113B4152CD49C (void);
// 0x000000ED System.Single DevionGames.UIWidgets.EasingEquations::EaseOutBounce(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutBounce_m585EAEAD9639A78D61C9495A7F9D836D6093AEB7 (void);
// 0x000000EE System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutBounce(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutBounce_m9FC65BFE721D7B5570206D38AD70A05D9E09482E (void);
// 0x000000EF System.Single DevionGames.UIWidgets.EasingEquations::EaseInBack(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInBack_m2E66A84534C1909FDE7C1F5BE96270D65E987A58 (void);
// 0x000000F0 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutBack(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutBack_m5D393B1A6E017F354E0AB81C4DC5BE19C27F6120 (void);
// 0x000000F1 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutBack(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutBack_m64F9B7691A0587B29FAC9A53545B728D53B1B44C (void);
// 0x000000F2 System.Single DevionGames.UIWidgets.EasingEquations::Punch(System.Single,System.Single)
extern void EasingEquations_Punch_m81179B587608074C2AAAABDE1129AFC884F15218 (void);
// 0x000000F3 System.Single DevionGames.UIWidgets.EasingEquations::EaseInElastic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInElastic_m9FA14A540D82BEE5C8F63BAC83F9E36E0A0CC2C0 (void);
// 0x000000F4 System.Single DevionGames.UIWidgets.EasingEquations::EaseOutElastic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseOutElastic_mC53E0D25A0582A83A906F6F2F8155F2BE0F09FB4 (void);
// 0x000000F5 System.Single DevionGames.UIWidgets.EasingEquations::EaseInOutElastic(System.Single,System.Single,System.Single)
extern void EasingEquations_EaseInOutElastic_mE7F3FBE915817EEE8B3AAAB6F16E28E84C0B8D6B (void);
// 0x000000F6 DevionGames.UIWidgets.EasingEquations/EaseType DevionGames.UIWidgets.FloatTween::get_easeType()
extern void FloatTween_get_easeType_m5AE9B9DAA1ADAFC83EE8335C4F0FB6257040DA45 (void);
// 0x000000F7 System.Void DevionGames.UIWidgets.FloatTween::set_easeType(DevionGames.UIWidgets.EasingEquations/EaseType)
extern void FloatTween_set_easeType_mB81F091D306B341F606E116423A990374DF45A6E (void);
// 0x000000F8 System.Single DevionGames.UIWidgets.FloatTween::get_startValue()
extern void FloatTween_get_startValue_mB4E95945BC704405477C63C7C9DB4256C20B4250 (void);
// 0x000000F9 System.Void DevionGames.UIWidgets.FloatTween::set_startValue(System.Single)
extern void FloatTween_set_startValue_m145998BBC39EBB2FF11C409D24D199D18E837F89 (void);
// 0x000000FA System.Single DevionGames.UIWidgets.FloatTween::get_targetValue()
extern void FloatTween_get_targetValue_m026D871C9654D86762C8CF1E948C61CDFBA9D72E (void);
// 0x000000FB System.Void DevionGames.UIWidgets.FloatTween::set_targetValue(System.Single)
extern void FloatTween_set_targetValue_m5926751985717165AE47AB308DFA02B2F6E80917 (void);
// 0x000000FC System.Single DevionGames.UIWidgets.FloatTween::get_duration()
extern void FloatTween_get_duration_m5FC9D0826EC286D6A338152866136DEE60A3EA19 (void);
// 0x000000FD System.Void DevionGames.UIWidgets.FloatTween::set_duration(System.Single)
extern void FloatTween_set_duration_m1A2F07057EB8A05C5E20F79B888CB7A6FC8BE8F1 (void);
// 0x000000FE System.Boolean DevionGames.UIWidgets.FloatTween::get_ignoreTimeScale()
extern void FloatTween_get_ignoreTimeScale_m10E4EF5A8DAD3C4AD74C368CB4D6735A6EF7A671 (void);
// 0x000000FF System.Void DevionGames.UIWidgets.FloatTween::set_ignoreTimeScale(System.Boolean)
extern void FloatTween_set_ignoreTimeScale_mE9CC9FC7278D1309995CE5DEF1341415B1BE43D9 (void);
// 0x00000100 System.Boolean DevionGames.UIWidgets.FloatTween::ValidTarget()
extern void FloatTween_ValidTarget_m95BC102F85EE8D62AA6F7D5ABB151BD824007A38 (void);
// 0x00000101 System.Void DevionGames.UIWidgets.FloatTween::TweenValue(System.Single)
extern void FloatTween_TweenValue_m3D5F0CD6439CF0663AE94A54E2C431A7B2058AF3 (void);
// 0x00000102 System.Void DevionGames.UIWidgets.FloatTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<System.Single>)
extern void FloatTween_AddOnChangedCallback_mF2139D9EE9D140F88AE5C8EEC68301313D6B58A4 (void);
// 0x00000103 System.Void DevionGames.UIWidgets.FloatTween::AddOnFinishCallback(UnityEngine.Events.UnityAction)
extern void FloatTween_AddOnFinishCallback_m7C1671512DC6795772CB7AEF93E031778D0A6E9B (void);
// 0x00000104 System.Void DevionGames.UIWidgets.FloatTween::OnFinish()
extern void FloatTween_OnFinish_m2685C1D9DE4C2B2CD3833639238B216BA42E0425 (void);
// 0x00000105 System.Void DevionGames.UIWidgets.FloatTween/FloatTweenCallback::.ctor()
extern void FloatTweenCallback__ctor_mFC58A0F5CCB7E46B6B7BFA48EA48E72C5910AFBC (void);
// 0x00000106 System.Void DevionGames.UIWidgets.FloatTween/FloatTweenFinishCallback::.ctor()
extern void FloatTweenFinishCallback__ctor_mBC2DDDB890D45A8FE7AA6ABF6255D70A4D28890B (void);
// 0x00000107 System.Void DevionGames.UIWidgets.ITweenValue::TweenValue(System.Single)
// 0x00000108 System.Boolean DevionGames.UIWidgets.ITweenValue::get_ignoreTimeScale()
// 0x00000109 System.Single DevionGames.UIWidgets.ITweenValue::get_duration()
// 0x0000010A System.Boolean DevionGames.UIWidgets.ITweenValue::ValidTarget()
// 0x0000010B System.Void DevionGames.UIWidgets.ITweenValue::OnFinish()
// 0x0000010C System.Collections.IEnumerator DevionGames.UIWidgets.TweenRunner`1::Start(T)
// 0x0000010D System.Void DevionGames.UIWidgets.TweenRunner`1::Init(UnityEngine.MonoBehaviour)
// 0x0000010E System.Void DevionGames.UIWidgets.TweenRunner`1::StartTween(T)
// 0x0000010F System.Void DevionGames.UIWidgets.TweenRunner`1::StopTween()
// 0x00000110 System.Void DevionGames.UIWidgets.TweenRunner`1::.ctor()
// 0x00000111 System.Void DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::.ctor(System.Int32)
// 0x00000112 System.Void DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::System.IDisposable.Dispose()
// 0x00000113 System.Boolean DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::MoveNext()
// 0x00000114 System.Object DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000115 System.Void DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::System.Collections.IEnumerator.Reset()
// 0x00000116 System.Object DevionGames.UIWidgets.TweenRunner`1/<Start>d__2::System.Collections.IEnumerator.get_Current()
// 0x00000117 DevionGames.UIWidgets.EasingEquations/EaseType DevionGames.UIWidgets.Vector3Tween::get_easeType()
extern void Vector3Tween_get_easeType_mE27B9489BC5A292E768BD80FC246A789282252B8 (void);
// 0x00000118 System.Void DevionGames.UIWidgets.Vector3Tween::set_easeType(DevionGames.UIWidgets.EasingEquations/EaseType)
extern void Vector3Tween_set_easeType_m8D0B0ACD8E02296885392239EE7CB50D7F5FE7F0 (void);
// 0x00000119 UnityEngine.Vector3 DevionGames.UIWidgets.Vector3Tween::get_startValue()
extern void Vector3Tween_get_startValue_m6074B9866D62069F059169C88B23D356813F03B5 (void);
// 0x0000011A System.Void DevionGames.UIWidgets.Vector3Tween::set_startValue(UnityEngine.Vector3)
extern void Vector3Tween_set_startValue_mF12AE96E079BA13ED7D4008C85727DBF87AA58CF (void);
// 0x0000011B UnityEngine.Vector3 DevionGames.UIWidgets.Vector3Tween::get_targetValue()
extern void Vector3Tween_get_targetValue_m23C2DE45D4516FC54E470705F956D328B72B027E (void);
// 0x0000011C System.Void DevionGames.UIWidgets.Vector3Tween::set_targetValue(UnityEngine.Vector3)
extern void Vector3Tween_set_targetValue_mB221AF0136B70F0AFE523E8C554E8918A6AAB428 (void);
// 0x0000011D System.Single DevionGames.UIWidgets.Vector3Tween::get_duration()
extern void Vector3Tween_get_duration_mC9B6E53B72E8AB8CD7365E4A8BF7A824465EAE0E (void);
// 0x0000011E System.Void DevionGames.UIWidgets.Vector3Tween::set_duration(System.Single)
extern void Vector3Tween_set_duration_mCF129F58AFC949013EE6B871DAE39629A7C580C6 (void);
// 0x0000011F System.Boolean DevionGames.UIWidgets.Vector3Tween::get_ignoreTimeScale()
extern void Vector3Tween_get_ignoreTimeScale_m38393D8728D506CE6D7BBAA7C564C963782FEF75 (void);
// 0x00000120 System.Void DevionGames.UIWidgets.Vector3Tween::set_ignoreTimeScale(System.Boolean)
extern void Vector3Tween_set_ignoreTimeScale_m00A8E47DC22181BCCB916EA4755F4F154EE6A050 (void);
// 0x00000121 System.Boolean DevionGames.UIWidgets.Vector3Tween::ValidTarget()
extern void Vector3Tween_ValidTarget_m93EA1330819CA2015F6FEA505E0BFC098701B5D2 (void);
// 0x00000122 System.Void DevionGames.UIWidgets.Vector3Tween::TweenValue(System.Single)
extern void Vector3Tween_TweenValue_m7E0DA22520A51A3D60CC7BE3EDA16ADFB92310A6 (void);
// 0x00000123 System.Void DevionGames.UIWidgets.Vector3Tween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Vector3>)
extern void Vector3Tween_AddOnChangedCallback_mF67324A4410FB7D4B0BA4029F80EA6B77D67AEF7 (void);
// 0x00000124 System.Void DevionGames.UIWidgets.Vector3Tween::AddOnFinishCallback(UnityEngine.Events.UnityAction)
extern void Vector3Tween_AddOnFinishCallback_m53F9863979C3EC02B11828B48FC7ABAE7EA09CC9 (void);
// 0x00000125 System.Void DevionGames.UIWidgets.Vector3Tween::OnFinish()
extern void Vector3Tween_OnFinish_m0A7A21F91630309FBA093DA6E6F33766B39D5C37 (void);
// 0x00000126 System.Void DevionGames.UIWidgets.Vector3Tween/Vector3TweenCallback::.ctor()
extern void Vector3TweenCallback__ctor_m3D1C7E31E90F13546CA7C782AC51C47104306867 (void);
// 0x00000127 System.Void DevionGames.UIWidgets.Vector3Tween/Vector3TweenFinishCallback::.ctor()
extern void Vector3TweenFinishCallback__ctor_mF52642B843EEBE01092AA33989E5838EBBCA2934 (void);
// 0x00000128 System.String[] DevionGames.UIWidgets.UIContainer`1::get_Callbacks()
// 0x00000129 System.Collections.ObjectModel.ReadOnlyCollection`1<DevionGames.UIWidgets.UISlot`1<T>> DevionGames.UIWidgets.UIContainer`1::get_Slots()
// 0x0000012A System.Void DevionGames.UIWidgets.UIContainer`1::OnAwake()
// 0x0000012B System.Boolean DevionGames.UIWidgets.UIContainer`1::AddItem(T)
// 0x0000012C System.Boolean DevionGames.UIWidgets.UIContainer`1::RemoveItem(System.Int32)
// 0x0000012D T DevionGames.UIWidgets.UIContainer`1::ReplaceItem(System.Int32,T)
// 0x0000012E System.Boolean DevionGames.UIWidgets.UIContainer`1::CanAddItem(T,DevionGames.UIWidgets.UISlot`1<T>&,System.Boolean)
// 0x0000012F System.Void DevionGames.UIWidgets.UIContainer`1::RefreshSlots()
// 0x00000130 DevionGames.UIWidgets.UISlot`1<T> DevionGames.UIWidgets.UIContainer`1::CreateSlot()
// 0x00000131 System.Void DevionGames.UIWidgets.UIContainer`1::DestroySlot(System.Int32)
// 0x00000132 System.Void DevionGames.UIWidgets.UIContainer`1::.ctor()
// 0x00000133 System.Void DevionGames.UIWidgets.UICursor::Awake()
extern void UICursor_Awake_m631C441404A6545059403BD4672FFEA0DBFE43AE (void);
// 0x00000134 System.Void DevionGames.UIWidgets.UICursor::Start()
extern void UICursor_Start_mCFBBDBABC9E8BA5F34D857326627F52D352DD318 (void);
// 0x00000135 System.Void DevionGames.UIWidgets.UICursor::Update()
extern void UICursor_Update_mA38909B4D9C4EFFD7BB57F8ADF7AED4588C13866 (void);
// 0x00000136 System.Void DevionGames.UIWidgets.UICursor::Clear()
extern void UICursor_Clear_mEC459A306806D62E6C950335C025F0A662CED54A (void);
// 0x00000137 System.Void DevionGames.UIWidgets.UICursor::Set(UnityEngine.Sprite)
extern void UICursor_Set_m6D97184AB88923C79EE17219F4C3430A6C2B46DE (void);
// 0x00000138 System.Void DevionGames.UIWidgets.UICursor::Set(UnityEngine.Sprite,UnityEngine.Vector2,System.Boolean,System.String)
extern void UICursor_Set_m1DF1A6044FCA6E654AE7D9CB710142347E4F9A44 (void);
// 0x00000139 System.Void DevionGames.UIWidgets.UICursor::SetAnimatorState(System.String)
extern void UICursor_SetAnimatorState_m3140E69AF1B9B31E1EF93A727D574F1188F042E2 (void);
// 0x0000013A System.Void DevionGames.UIWidgets.UICursor::.ctor()
extern void UICursor__ctor_mA5496A7D0F16504E17E8F0076C2602878A2E0E18 (void);
// 0x0000013B DevionGames.UIWidgets.UIContainer`1<T> DevionGames.UIWidgets.UISlot`1::get_Container()
// 0x0000013C System.Void DevionGames.UIWidgets.UISlot`1::set_Container(DevionGames.UIWidgets.UIContainer`1<T>)
// 0x0000013D System.Int32 DevionGames.UIWidgets.UISlot`1::get_Index()
// 0x0000013E System.Void DevionGames.UIWidgets.UISlot`1::set_Index(System.Int32)
// 0x0000013F T DevionGames.UIWidgets.UISlot`1::get_ObservedItem()
// 0x00000140 System.Void DevionGames.UIWidgets.UISlot`1::set_ObservedItem(T)
// 0x00000141 System.Boolean DevionGames.UIWidgets.UISlot`1::get_IsEmpty()
// 0x00000142 System.Void DevionGames.UIWidgets.UISlot`1::Repaint()
// 0x00000143 System.Boolean DevionGames.UIWidgets.UISlot`1::CanAddItem(T)
// 0x00000144 System.Void DevionGames.UIWidgets.UISlot`1::.ctor()
// 0x00000145 System.String DevionGames.UIWidgets.UIWidget::get_Name()
extern void UIWidget_get_Name_mE16241740E8F042B088BD63CA5D11C7F90EBF90B (void);
// 0x00000146 System.Void DevionGames.UIWidgets.UIWidget::set_Name(System.String)
extern void UIWidget_set_Name_m580DC27C5C1E00396061212BD19EB29469F7A607 (void);
// 0x00000147 System.String[] DevionGames.UIWidgets.UIWidget::get_Callbacks()
extern void UIWidget_get_Callbacks_m3E34C09721550CE3046B91ADE15D12CA1BB5901A (void);
// 0x00000148 System.Boolean DevionGames.UIWidgets.UIWidget::get_IgnoreTimeScale()
extern void UIWidget_get_IgnoreTimeScale_mF470882883F5E3AC0417210E5CC8E2F2D01FFECE (void);
// 0x00000149 System.Boolean DevionGames.UIWidgets.UIWidget::get_IsVisible()
extern void UIWidget_get_IsVisible_mEAC0E2D530D74C0C034BBC03911E445AA61D45B2 (void);
// 0x0000014A System.Boolean DevionGames.UIWidgets.UIWidget::get_IsLocked()
extern void UIWidget_get_IsLocked_m2CEE2B8763515850A94880C5A3520711FF21C92E (void);
// 0x0000014B System.Void DevionGames.UIWidgets.UIWidget::Awake()
extern void UIWidget_Awake_m7F616A3A3ECBD9A447516C2F2524439BDA55BDC7 (void);
// 0x0000014C System.Void DevionGames.UIWidgets.UIWidget::OnAwake()
extern void UIWidget_OnAwake_mE02173557D4CD93A48A80C4F2BAEBDFF56A2BF67 (void);
// 0x0000014D System.Void DevionGames.UIWidgets.UIWidget::Start()
extern void UIWidget_Start_m2FD5F5705D265598AB58C095151D48AD8823D98A (void);
// 0x0000014E System.Void DevionGames.UIWidgets.UIWidget::OnStart()
extern void UIWidget_OnStart_mB6AFD66AAB8CF245F0CAA1258BDF8DFF63FA499A (void);
// 0x0000014F System.Collections.IEnumerator DevionGames.UIWidgets.UIWidget::OnDelayedStart()
extern void UIWidget_OnDelayedStart_m29784EEA937B392528DF388ED658083EFFA584D7 (void);
// 0x00000150 System.Void DevionGames.UIWidgets.UIWidget::Update()
extern void UIWidget_Update_m43B50EEDC34958DDB9CEAF07E592BED2E44CC75A (void);
// 0x00000151 System.Void DevionGames.UIWidgets.UIWidget::Show()
extern void UIWidget_Show_m3C225D4387EBB51A179C3AD8699DFCF4762ACF1F (void);
// 0x00000152 System.Void DevionGames.UIWidgets.UIWidget::Close()
extern void UIWidget_Close_m67F15777E8821BCBD38F52B66C41BDD2E71151F7 (void);
// 0x00000153 System.Void DevionGames.UIWidgets.UIWidget::TweenCanvasGroupAlpha(System.Single,System.Single)
extern void UIWidget_TweenCanvasGroupAlpha_m94B0857E31B9BA737B9A58A5FD736A898F4673A9 (void);
// 0x00000154 System.Void DevionGames.UIWidgets.UIWidget::TweenTransformScale(UnityEngine.Vector3,UnityEngine.Vector3)
extern void UIWidget_TweenTransformScale_mD02C4E841628E1A9A1382547D158B6D20D020D51 (void);
// 0x00000155 System.Void DevionGames.UIWidgets.UIWidget::Toggle()
extern void UIWidget_Toggle_m0C404198641C48199C8AAB38587E421CE1970935 (void);
// 0x00000156 System.Void DevionGames.UIWidgets.UIWidget::Focus()
extern void UIWidget_Focus_m573FD812FB9415278E4783365FD1063F28B8C36C (void);
// 0x00000157 System.Void DevionGames.UIWidgets.UIWidget::OnDestroy()
extern void UIWidget_OnDestroy_m49945D1897E4A2D6043B514F757B6D35498462BE (void);
// 0x00000158 System.Void DevionGames.UIWidgets.UIWidget::Lock(System.Boolean)
extern void UIWidget_Lock_mE658823E4FC60AFC82BF0F6F537ACE3FB5090C8C (void);
// 0x00000159 System.Void DevionGames.UIWidgets.UIWidget::LockAll(System.Boolean)
extern void UIWidget_LockAll_mA823095BBB8194184F7A6D9D1E6AF7659C1EAD12 (void);
// 0x0000015A System.Void DevionGames.UIWidgets.UIWidget::.ctor()
extern void UIWidget__ctor_m267261D67D162731033FAC773BC3A968D86C3D21 (void);
// 0x0000015B System.Void DevionGames.UIWidgets.UIWidget::.cctor()
extern void UIWidget__cctor_m67E4979B5B8C9CF4240F0431F738CE36C9EE6EBA (void);
// 0x0000015C System.Void DevionGames.UIWidgets.UIWidget::<TweenTransformScale>b__48_0(UnityEngine.Vector3)
extern void UIWidget_U3CTweenTransformScaleU3Eb__48_0_m29A828C4EB884DCBFE7E10F8F1BEE1F195F63051 (void);
// 0x0000015D System.Void DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::.ctor(System.Int32)
extern void U3COnDelayedStartU3Ed__43__ctor_mC964BF60C0BFC9443251FA4E80D6BEC534845B60 (void);
// 0x0000015E System.Void DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::System.IDisposable.Dispose()
extern void U3COnDelayedStartU3Ed__43_System_IDisposable_Dispose_m2A142D07BA5F9D45043DFEFA39A96AE20A625D3B (void);
// 0x0000015F System.Boolean DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::MoveNext()
extern void U3COnDelayedStartU3Ed__43_MoveNext_mA271E2A9D464ACBAD540653831EEB3EC75A7B664 (void);
// 0x00000160 System.Object DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnDelayedStartU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96BC0C790C4AFAA8E5FABB91BAB4E09E6A031D68 (void);
// 0x00000161 System.Void DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::System.Collections.IEnumerator.Reset()
extern void U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_Reset_mA6EFD5C346DD2AA0C17B009E2EF9192CD345D206 (void);
// 0x00000162 System.Object DevionGames.UIWidgets.UIWidget/<OnDelayedStart>d__43::System.Collections.IEnumerator.get_Current()
extern void U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_get_Current_m34A7FD6BEE90C1B43DA48518D95DF0530B61B437 (void);
// 0x00000163 System.Void DevionGames.UIWidgets.UIWidget/<>c__DisplayClass47_0::.ctor()
extern void U3CU3Ec__DisplayClass47_0__ctor_m756D123C504F3C609E7A250A54A52826DF90691E (void);
// 0x00000164 System.Void DevionGames.UIWidgets.UIWidget/<>c__DisplayClass47_0::<TweenCanvasGroupAlpha>b__0(System.Single)
extern void U3CU3Ec__DisplayClass47_0_U3CTweenCanvasGroupAlphaU3Eb__0_mEAC040D6075F349790777E60A09A601B42DF17D1 (void);
// 0x00000165 System.Void DevionGames.UIWidgets.UIWidget/<>c__DisplayClass47_0::<TweenCanvasGroupAlpha>b__1()
extern void U3CU3Ec__DisplayClass47_0_U3CTweenCanvasGroupAlphaU3Eb__1_m93FE704DCFDE235395F2155BD87C0BA91A1FF0E4 (void);
// 0x00000166 System.Void DevionGames.UIWidgets.WidgetInputHandler::Update()
extern void WidgetInputHandler_Update_m2D661C09A63390AA469D93E9429FBB125DAA1272 (void);
// 0x00000167 System.Void DevionGames.UIWidgets.WidgetInputHandler::RegisterInput(UnityEngine.KeyCode,DevionGames.UIWidgets.UIWidget)
extern void WidgetInputHandler_RegisterInput_m8149D9F46777EAB5F4627663B2BFC1BE5750146D (void);
// 0x00000168 System.Void DevionGames.UIWidgets.WidgetInputHandler::UnregisterInput(UnityEngine.KeyCode,DevionGames.UIWidgets.UIWidget)
extern void WidgetInputHandler_UnregisterInput_m9F7D11ECC07855E4E6D3DB128050E6C8315F98A7 (void);
// 0x00000169 System.Void DevionGames.UIWidgets.WidgetInputHandler::.ctor()
extern void WidgetInputHandler__ctor_mDC061BBA76B0079A47155252F81B3779462778C1 (void);
// 0x0000016A System.Void DevionGames.UIWidgets.WidgetInputHandler/<>c::.cctor()
extern void U3CU3Ec__cctor_m85A8AD82EDE3A6C8F483D965E47039A8E2670565 (void);
// 0x0000016B System.Void DevionGames.UIWidgets.WidgetInputHandler/<>c::.ctor()
extern void U3CU3Ec__ctor_m5341B56E8A3712909F0DD4268CA6A59DB92EA2C4 (void);
// 0x0000016C System.Boolean DevionGames.UIWidgets.WidgetInputHandler/<>c::<RegisterInput>b__2_0(DevionGames.UIWidgets.UIWidget)
extern void U3CU3Ec_U3CRegisterInputU3Eb__2_0_mDF7522AC00D6269DD2BFF63F9EE7BDF88E3201F6 (void);
// 0x0000016D T DevionGames.UIWidgets.WidgetUtility::Find(System.String)
// 0x0000016E System.Void DevionGames.UIWidgets.WidgetUtility::.cctor()
extern void WidgetUtility__cctor_mA2E1018D44CD9867D124EB2CD8A617BC586B4713 (void);
// 0x0000016F System.Void DevionGames.UIWidgets.WidgetUtility::ChangedActiveScene(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void WidgetUtility_ChangedActiveScene_m22B3E05948F0DD246A398D01369E62F1E8F9DE8B (void);
// 0x00000170 T[] DevionGames.UIWidgets.WidgetUtility::FindAll(System.String)
// 0x00000171 T[] DevionGames.UIWidgets.WidgetUtility::FindAll()
// 0x00000172 System.Void DevionGames.UIWidgets.WidgetUtility::PlaySound(UnityEngine.AudioClip,System.Single)
extern void WidgetUtility_PlaySound_m3B12A87114B2568D072271E771B76EEDEECD395D (void);
// 0x00000173 System.String DevionGames.UIWidgets.WidgetUtility::ColorToHex(UnityEngine.Color32)
extern void WidgetUtility_ColorToHex_m2CB8C25CE4D32DBCE9114DC8FFF89C871037BDE5 (void);
// 0x00000174 UnityEngine.Color DevionGames.UIWidgets.WidgetUtility::HexToColor(System.String)
extern void WidgetUtility_HexToColor_m9A50C9B8889A44B33ED2D763D4BCFBE2FC127F30 (void);
// 0x00000175 System.String DevionGames.UIWidgets.WidgetUtility::ColorString(System.String,UnityEngine.Color)
extern void WidgetUtility_ColorString_m06633A32FBEC62185AF3DDCA2D9ACFAF8E1EF382 (void);
// 0x00000176 System.Void DevionGames.UIWidgets.WidgetUtility/<>c__DisplayClass4_0`1::.ctor()
// 0x00000177 System.Boolean DevionGames.UIWidgets.WidgetUtility/<>c__DisplayClass4_0`1::<FindAll>b__1(T)
// 0x00000178 System.Void DevionGames.UIWidgets.WidgetUtility/<>c__4`1::.cctor()
// 0x00000179 System.Void DevionGames.UIWidgets.WidgetUtility/<>c__4`1::.ctor()
// 0x0000017A System.Int32 DevionGames.UIWidgets.WidgetUtility/<>c__4`1::<FindAll>b__4_2(T)
// 0x0000017B System.Boolean DevionGames.UIWidgets.WidgetUtility/<>c__4`1::<FindAll>b__4_0(DevionGames.UIWidgets.UIWidget)
// 0x0000017C System.Void DevionGames.UIWidgets.WidgetUtility/<>c__5`1::.cctor()
// 0x0000017D System.Void DevionGames.UIWidgets.WidgetUtility/<>c__5`1::.ctor()
// 0x0000017E System.Int32 DevionGames.UIWidgets.WidgetUtility/<>c__5`1::<FindAll>b__5_1(T)
// 0x0000017F System.Boolean DevionGames.UIWidgets.WidgetUtility/<>c__5`1::<FindAll>b__5_0(DevionGames.UIWidgets.UIWidget)
static Il2CppMethodPointer s_methodPointers[383] = 
{
	Cooldown_get_IsCoolDown_m73568573CDF89BD7B2414CD2F16E2981E7F097E1,
	Cooldown_Update_mFD0E719ACD5054DF57D591A38F3C6BF91F50197F,
	Cooldown_OnPointerClick_mB1E8A16C67F7BCE34199934BA1411FEC51479F73,
	Cooldown_DoCooldown_mF8F31907E54EBE051374B5A30F5EECE27B4DD571,
	Cooldown_DoGlobalCooldown_mE5563FDBF30727988093DF9F72B24E9619241C97,
	Cooldown__ctor_m7404B43FFB012AB3C54BA4A3991D3F5943D00AFB,
	ContextMenuTrigger_Start_m170AA3DA4511EDBBAB31543A0D85CF7F0D2476C7,
	ContextMenuTrigger_OnPointerDown_m0B288937B9E2A8C561287AB6E8ECEE7DFB784B08,
	ContextMenuTrigger__ctor_m0CF6940F1CD5F456F29D1EB2D6BA39F71F07E3D0,
	U3CU3Ec__DisplayClass3_0__ctor_mEC535FD7763138804E7E0092A43FC6E682DCC478,
	U3CU3Ec__DisplayClass3_0_U3COnPointerDownU3Eb__0_m7819F8DA6AB371A7917D4D143321C92D74667511,
	DialogBoxTrigger_Start_m84D6AE20B404E699643ABF1968FED21244E3D8B6,
	DialogBoxTrigger_Show_m73155C5B98109262E20263FD39842C159FA37FC9,
	DialogBoxTrigger_ShowWithCallback_mCCA1C4B788F2C3BA90418061F7D22FD37320973B,
	DialogBoxTrigger_OnDialogResult_m25280F538EEC55301BE84308FEF894CDA855C7E5,
	DialogBoxTrigger__ctor_mC2C39738E3FF4DBE8E3282D6337C358DE06E33E1,
	NotificationTrigger_Start_m073E7887BABD374DAEB286DD507F7228573F3C83,
	NotificationTrigger_AddRandomNotification_m4F49DD95F4591365BD79C01953B85DB8AB21D90E,
	NotificationTrigger_AddNotification_m12C2CC7C75E1E35E0B90BAAA5B2347BBC99971F5,
	NotificationTrigger_AddNotification_m608FE8D6D7061B69193E022A6E9CC95452C83030,
	NotificationTrigger__ctor_m9AA3653E5A3635E79B55FCC26DFA5D899030715E,
	RadialMenuTrigger_Start_m66A735291156902C99F4C8C0EEC103B9F7EC325A,
	RadialMenuTrigger_OnPointerDown_mA0E9853FA88A661D2407416BB752E9839FE65960,
	RadialMenuTrigger__ctor_mA6651BCBD32B93E382D610623B56165B63D052FF,
	U3CU3Ec__cctor_m531831D00E7AC4531B9A0F9F79ACAD1C8069D527,
	U3CU3Ec__ctor_m3874D5CF04172132EC9F262D3970EF4B266CE225,
	U3CU3Ec_U3COnPointerDownU3Eb__3_0_m41DE04D1852B78BB6B412A02C0D21B6607A0FB0B,
	AnimateRawImage_Start_m012EF1F43B7782FF9AE363156C883ADCAD635439,
	AnimateRawImage_Update_mA52CDC0E999ADA67DFB9412A93107054C5F9DABD,
	AnimateRawImage__ctor_mB004EAB319D6EF7469BAC1850D264D2DB5D71531,
	Chat_OnStart_mFF6CD1A4C28863996872CF3F46F2B7FD6AE85B87,
	Chat_Submit_m15E66E4BDABEF8DA3D13868005BB065D6A3BB069,
	Chat_OnSubmit_mA7B784F7CAFB74DF8490BF0D4767714AFA253CD7,
	Chat_ApplyFilter_mDE0624BDC906F737114673F4F45FEE1CFFE82B28,
	Chat__ctor_m05E5B630B65AB241D7C2C8EC890D0477D48DF9FD,
	Chat_U3COnStartU3Eb__6_0_m3F9CA70497463E6A6E0A11EA176E9EBCE59CE02A,
	ContextMenu_Show_mFFBCF2896931C0EBEE4AB5F69CAE6E1444D22277,
	ContextMenu_Update_mFA3D8E0ECC19EFA5DC96FF0FA67305BD70231248,
	ContextMenu_Clear_m3727DEC3FFDBFAB8DB453FDE6E0841A908348958,
	ContextMenu_AddMenuItem_m47C561A3F529F55CE0A401B0EBCE5CDBBB43783B,
	ContextMenu__ctor_mB85995A02987AC50C1DBED6FB233CCECE8F7742B,
	U3CU3Ec__DisplayClass5_0__ctor_mAA49C078B22AE5A9955338DE9BBCBE5C05DA51B3,
	U3CU3Ec__DisplayClass5_0_U3CAddMenuItemU3Eb__1_m8B3F76895CDE6FC0804A140A80CDBCDA88CB1FF1,
	U3CU3Ec__cctor_m87B27DF16FA6DC0402043E0B3377E8BBCC4D701C,
	U3CU3Ec__ctor_m3303C0188F91DA0DE4262AF4C1143412E6BA0817,
	U3CU3Ec_U3CAddMenuItemU3Eb__5_0_m7BCF9CFE0D7796A4FA4BD627D56080D2C8594861,
	DialogBox_OnAwake_mFCE03E18FE368AA420A3C121C7C9DE2E1384A057,
	DialogBox_Show_mC24BE23C2C0B4C61FD24D23149B66CD070814D4F,
	DialogBox_Show_m65033F147B563EAFC7DC9A5C2D1124EB28176757,
	DialogBox_Show_m29679F5AD10130585BB848ED347461F89932B474,
	DialogBox_Show_m9EE2AB39BFE7EA8D3551D237A37E43A155468460,
	DialogBox_AddButton_m90C82A8819FDE717FF6BF4AF20A7E369D696EA3E,
	DialogBox__ctor_mFB4BB7615E9E5BFF1657F70B985A3D0E17EE84B5,
	DialogBox_U3CU3En__0_m22421082B06171A00E7C8468E080BC9D2D8112D4,
	U3CU3Ec__DisplayClass11_0__ctor_mECC5C6B45C792556D15FF978DF85CEC19370F5B8,
	U3CU3Ec__DisplayClass11_1__ctor_m16D09220D81C5EF732282F017E81F796F265C681,
	U3CU3Ec__DisplayClass11_1_U3CShowU3Eb__0_m18E49568C6729F62EA15EA8D3B1B13E374E3917C,
	U3CU3Ec__cctor_mD5FF6584BBAEDAE48DC6C4DE48C7BE9163C2B505,
	U3CU3Ec__ctor_m1B21A29526FFC89B2BCADB28E31A877C049B0343,
	U3CU3Ec_U3CAddButtonU3Eb__12_0_mB7719FB5D5C09B9560C76F7E17EB3676C46BD201,
	DragPanel_Awake_mCEC3EFDE078650597701B6143894F570E7603E3F,
	DragPanel_OnPointerDown_mFF9F34CF85A195252D2F9F1BDCC7F899DD2CCFD1,
	DragPanel_OnDrag_m12F44736E5964D022E0A56E5BBFF04242CBF56BA,
	DragPanel_ClampToWindow_m4666F4A71EB3BF5BAA87076957E251AF40C5EC8F,
	DragPanel__ctor_m5C5C6358B652E132FA6EEE11757ED3C5A1853250,
	FloatingText_Awake_m8EEDC0A55D5E79F62FB309D2BC50517CE1BD1755,
	FloatingText_LateUpdate_mA93D4F71DDB20ADBEDC3835B0A2F56F8ED82BDE8,
	FloatingText_SetText_m877938FE22AE975B6999ED5DA322F06DB084B089,
	FloatingText__ctor_mB469FE8D67D72956FA7712D1D710EF59F76AD609,
	FloatingTextManager_Awake_mA1E68F55D1F2DC5E844A4C480F4C3582169CC21F,
	FloatingTextManager_Add_m06E6F93434AC0F0F182BB6D8BB9B5EDFDA557CC3,
	FloatingTextManager_Remove_m0439BD708813F4CF97666D5E8471A249B3B180D8,
	FloatingTextManager__ctor_mB1650C2D78EC15E26686EA60CFA162A97BE21ED8,
	FloatingTextManager__cctor_mF422DD5587CA1C377F35A361889F92516BA91054,
	HorizontalCompass_Start_m9D9365B85813440343BCA779263F548008D27300,
	HorizontalCompass_Update_m93423DFB5CB12DA83AD5E2DC604C2BE4F127BFC7,
	HorizontalCompass__ctor_m71F8F6F5B8B264A2B1BA67B7424956856407F498,
	NULL,
	Joystick_get_position_m390650DB815A6124E96EDB0BD0AB2CEC88950D54,
	Joystick_Start_mAF35CEE5345DA532B2C55D15F8C10BD74CD98979,
	Joystick_OnDrag_mB58C2EC401953F2CB2A13F0CFA60260A146DEBAA,
	Joystick_OnPointerDown_m9021414382BBC67E7B9FD1E12972D8D5BF6F5A2C,
	Joystick_OnPointerUp_mC516EBA9BFDD22057F59A6279A3C50CDC1D396D6,
	Joystick_Update_m933849F08D227A79FE6901247F5CC020088366F5,
	Joystick__ctor_m9495DE01496E7A44DFC162A646379CF9C5CA8950,
	JoystickEvent__ctor_mF0E1BD657D265382052866039D0E773C231EBAA8,
	MenuItem_get_onTrigger_mA321E3B33EFBB90EA3C55828ABBD1A2562E8BF76,
	MenuItem_set_onTrigger_mCDF6B7AD05496985504D9EE1366D1A3A44B51BE1,
	MenuItem_Press_m0DF9DC9268BE82E2B9EC1D377589F5A4608B2A83,
	MenuItem_OnPointerClick_m38F5FD8BEE3EC1513ADE018CF7DFF7422DE462B4,
	MenuItem_OnPointerEnter_mC70E7E41D196D71AA3C0DBDAE71D290EA56FC5ED,
	MenuItem__ctor_m8A9DEBBABD7EDE931DB7ECF916BF2E3A0F65FC50,
	Notification_AddItem_mB6F6943FD716314F45BF3BD5E099DE7FFFA4C073,
	Notification_AddItem_m661D7EEB2F92E26A6F8013A135FC116D9FCCDF8B,
	Notification_CanAddItem_m1B60DA3020A1805E349CF0EA056DFA573022978F,
	Notification__ctor_m60989F008960E012D4F10C8FD897A9EE1873BBF5,
	NotificationOptions__ctor_mCC89E14FDC37EC8CCAE24ECBFFDF4AE2E19BA04E,
	NotificationOptions__ctor_m1A4308800A2DA93BB9419B90E3666B8424EE1067,
	NotificationSlot_Repaint_m1D58D29FC1D3E1C000AD3AB7F71E69C672905809,
	NotificationSlot_DelayCrossFade_m057D06679446D003369D6582B06181A5988D9893,
	NotificationSlot_DelayCrossFade_m6F7ABE7FFFB3561DE95092EDAD10CB573B0FCCD5,
	NotificationSlot__ctor_m932A6ED57D98D60BB9177C6A75E975204DC38518,
	U3CDelayCrossFadeU3Ed__5__ctor_m7561C710C476831299D12A048E7370021E44A700,
	U3CDelayCrossFadeU3Ed__5_System_IDisposable_Dispose_mC49A564E2431BEB17C9C1B8EC89C866C456269B1,
	U3CDelayCrossFadeU3Ed__5_MoveNext_mE93ABBCAD5AE0C55ED51209D9E589D7DA59B703F,
	U3CDelayCrossFadeU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BBF68BDD827FB3FEC158DBEE89016274372D1E0,
	U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_Reset_m4AB5F3841F027E448E456C6E561CE2D0BCD08DC3,
	U3CDelayCrossFadeU3Ed__5_System_Collections_IEnumerator_get_Current_m2381C612381549E92122E9AD564C8777E90E48DC,
	Progressbar_OnStart_m669E838CA384436217BC260D255CCC0C95426082,
	Progressbar_SetProgress_m4A94EDF6AF68785BB2C19BBAE8F7DD11EE2A1E43,
	Progressbar_Show_m5A879C13A1EB6C573FD344AA2F71B9A3D220FF0A,
	Progressbar_Show_mC7891DA8BC2EED84F0469C26E6872C51FAA101C6,
	Progressbar__ctor_m259FF7CCC442FEA33812C1455A368EC0DD570656,
	RadialMenu_Update_mD82AAA9253F732A68CE031739246BAC7A3BD1C4F,
	RadialMenu_Show_m9F278454880A13C4D38666ED4FD258DE98D57EF7,
	RadialMenu_Close_m8AE726F8A3767756E4944F9760CD9F5DBE991A9E,
	RadialMenu_Show_m3F83A4BD09B3D15F16C468F87CF2ECCAB2DE4324,
	RadialMenu_AddMenuItem_mEA5246B804E4AC49521F0B061032162DC1F1FC76,
	RadialMenu__ctor_mF7E6C2F018219AC87B20CA34BFF937DF069E4EDD,
	U3CU3Ec__cctor_m9D05263F50BA419457200EF786E4842F94461D0C,
	U3CU3Ec__ctor_m9896A3FECF0201ED05AAF56C17CAB28CC7AFCDFB,
	U3CU3Ec_U3CUpdateU3Eb__5_0_m06D51817C5CF14E860D18EC96446107A52F1AA46,
	U3CU3Ec_U3CAddMenuItemU3Eb__9_0_m1AA85EA16AD203332D456704A038D8503F2C68DD,
	U3CU3Ec__DisplayClass6_0__ctor_mDF6A9E5DB2456213B4BDB9393F987F12442D1C12,
	U3CU3Ec__DisplayClass6_1__ctor_m1312870924280D060F4F83AA7742A6E123A23BAD,
	U3CU3Ec__DisplayClass6_1_U3CShowU3Eb__0_m7CA83E53B5E8C998CB8C1A684BD15C3C9277E1EC,
	ResizePanel_Awake_mDEA10C29978562C4A9062C653871EF904BB0665E,
	ResizePanel_OnPointerDown_m0166B820404972EDBC3399EAE9ECC7CE11D623D7,
	ResizePanel_OnDrag_mC7644BC7A9C34D80AD8EFB785493758146A38ABF,
	ResizePanel__ctor_m375895A0D4D1CD583C36B9D0535C0F81A3136294,
	AudioVolume_Start_m6A14C229A6BB88D46AC0EB3B0C42216ABA5CC253,
	AudioVolume_SetVolume_mBE76318B8DDBDB6E05018D8B65DA9A2AA08176BB,
	AudioVolume__ctor_mB5227F7623B28F1B2F19FE796D6681A99F08AC46,
	Fullscreen_Start_mF994EB2A23016313301F5FD6EDCC8C37F7DA7F66,
	Fullscreen_SetFullscreen_mE08ED96DBA569FB4DC3418F16106774B129455FB,
	Fullscreen__ctor_m1F0DA849CBD021740506BF5E29A79C146CF81878,
	QualityLevel_Start_m436AB5B80DDB3C7A940E9AB356A773F8AD34C431,
	QualityLevel_SetQualityLevel_m6C041017745FB7083A576130439FA4D1750BA77E,
	QualityLevel_SetQualityLevel_m5C7C187F8DC95C553756C332D10E5F8B02EEF252,
	QualityLevel_IncreaseQualityLevel_mC7C90F60FC525C781B04C15319A2BF8547531C71,
	QualityLevel_DecreaseQualityLevel_mC32ED83A87315B1D9B94BACD6981775B2B33129C,
	QualityLevel__ctor_mCF87513731396A275E3096774D8FBF98FA96ADFD,
	ScreenResolution_Start_mF2E33179EADB3F3853E2CD8FB476A2957C2BA3CD,
	ScreenResolution_SetResolution_m9B74CCDAA78D04C9015081A0282B98E90B1A9EC6,
	ScreenResolution_SetResolution_m0343412C0A7470E8EAD08B18CE1554AF5D2D136B,
	ScreenResolution_IncreaseResolution_mB7251126C7CCDE260C16A1063823A4F631D624C5,
	ScreenResolution_DecreaseResolution_mBCF685FDEB39026EEBDED0C8068F3FC06E8F2B19,
	ScreenResolution_get_IsStandalone_mB0A3F76DD0A6A2B3F863319A1A141B141F71ACD7,
	ScreenResolution__ctor_mC3B216AA89EBFE5194FDC8088B034F77A3E02954,
	TooltipTrigger_Start_mA812E1D09FB0546176032A721127BAC1099987C3,
	TooltipTrigger_OnPointerEnter_mBD86A38BF9F2B838AD3F397561447A5BFA16F754,
	TooltipTrigger_OnPointerExit_mE8AE77B6C8248E40A728A8E3D11A13017EFD4270,
	TooltipTrigger_DelayTooltip_m914A98EBA471C690E95541FDE6B1D3BE664B19F2,
	TooltipTrigger_ShowTooltip_mFEBC939EEF3A9E085C7DE3FFE58A35FE07054257,
	TooltipTrigger_CloseTooltip_m4CC02C1A0F5E899A54EB040FC032A38CE7F4BE26,
	TooltipTrigger__ctor_mC0BC1093492BC61993DA42A4CB0DCD06504ED4F3,
	StringPair__ctor_m5AE38B12C303EADA30D89A118AE9135FD85A3CEC,
	U3CDelayTooltipU3Ed__14__ctor_m039406E2ADA4CA0AD21A0F06D03082D9DC04E5FB,
	U3CDelayTooltipU3Ed__14_System_IDisposable_Dispose_mF0870C95526BAE3BC8B1F2149DC8D405EDD4602C,
	U3CDelayTooltipU3Ed__14_MoveNext_mC0405D8FF532553AFEDB304F3F905550294F569D,
	U3CDelayTooltipU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DEC7C9FF58B39F959BE337BFBCE491F4BDF6CBB,
	U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_Reset_mEA8201EEA14E7762C4E2F9AA327CCEADDCD1B583,
	U3CDelayTooltipU3Ed__14_System_Collections_IEnumerator_get_Current_m797FF401C8FF76D65A3F038EC70C2F06F35DD844,
	WidgetTrigger_Show_mF4DC18F76EE7848A5B02136D5678DE0BA32B7357,
	WidgetTrigger_Close_m5C5280BDEF217CEBD157A64CE9DC178551F59CEA,
	WidgetTrigger__ctor_mC35F4F663AE9AB3F650A93B8A68D1E5CFB230B57,
	Spinner_get_current_mE89E80DE712D9BF4487556FF3709CC252FD603CE,
	Spinner_set_current_m69664C399FC30601448F44604C22F207382F8800,
	Spinner_SetCurrent_m15BE69EE76FBF8676C46FA4D804A58E1771CBB44,
	Spinner_StartIncrease_m7126B0B869751CED59F2117117900F220D2ED1BC,
	Spinner_StartDecrease_m778D938C33CC31B61E4510715CDF8B64B200CBB4,
	Spinner_Stop_m8D73351EF3C0C3BD74ADB3B617E1277827F5D4BB,
	Spinner_Increase_mC322782BB67E030A046096639EC45C9D17A41C6D,
	Spinner_Decrease_m85EC7B47F523CEC45A28D681B3CC10EEBD6CB5AE,
	Spinner__ctor_m8645B4A541257F0887FDF31AD6E38772B6935A98,
	SpinnerEvent__ctor_m25214278EF5D51E8B92762451CA8849C79656713,
	SpinnerTextEvent__ctor_m24440A9EF7B43B1E1F47CA02782C41C184EF76E9,
	U3CIncreaseU3Ed__15__ctor_m332B26987322491F88F1ACA5BEC12CDF110CB2C2,
	U3CIncreaseU3Ed__15_System_IDisposable_Dispose_m481F5174A264917BBCF83788D1C79981EAA63591,
	U3CIncreaseU3Ed__15_MoveNext_m4533FF1C7F16606ED65A4A630C616C4FEFE575ED,
	U3CIncreaseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13FE33844E4F95E9E5D24DE179CB014E77E0922D,
	U3CIncreaseU3Ed__15_System_Collections_IEnumerator_Reset_m7248C18ABE14F91F640405744F9C560E91ACA18B,
	U3CIncreaseU3Ed__15_System_Collections_IEnumerator_get_Current_mF3D7E2B8E04DEFCDB6A2AF9082B74E8D059DC47B,
	U3CDecreaseU3Ed__16__ctor_m54DDB1C4E9792F142928896771DAAB3C7AA55E34,
	U3CDecreaseU3Ed__16_System_IDisposable_Dispose_m35A0C2A44B9BE0684A5BDCE1CA243F5F7092587E,
	U3CDecreaseU3Ed__16_MoveNext_mD9A6EE4287C0F77DA4B863B21EE999EB87B1D4BE,
	U3CDecreaseU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8122851D8FBC2D8CAFC52AF57A2467203978278A,
	U3CDecreaseU3Ed__16_System_Collections_IEnumerator_Reset_m7ACF8BAEC2101194888F88932511F29761EA369B,
	U3CDecreaseU3Ed__16_System_Collections_IEnumerator_get_Current_mEEB8F0EA23ED965BFD6BE4A6E05C1F67F2483401,
	StringPairSlot_get_Target_m8C96841652BB94BB7CABA273940138C86D182EB6,
	StringPairSlot_set_Target_mEBFDE0016D1CB5B9C1F37BE40F7E0EE772A88F0C,
	StringPairSlot_Repaint_mE601F67504E6977F373F76FBFF8B45795508F39F,
	StringPairSlot__ctor_m381230BB39DF44DEF71ED3906092BC9CE0F46222,
	Tab_Awake_mEA10A2F0DF31A8994EB2E73847FE8174CE3D0B5C,
	Tab_Start_mA8332695B3F24A5B2DB2676E40985E1578604609,
	Tab_Select_mDAAC74939F3068482ED5A8EC4A7EE0F84E1189C6,
	Tab_Deselect_m5E3F7225062A2FA33F2EF1E60DA049E7016A1CFA,
	Tab__ctor_mA4AE9D5A59310A0C54A0581C537BDCB0E29861FE,
	TabEvent__ctor_mC9075F53B133074B4BFEA7826A96F8DDC7A28177,
	Tooltip_OnStart_mEFCD49889B50F24577A861D905149E53356A3E1C,
	Tooltip_Update_m93A3A39877C43DEA70F962FCC57D282046B7104E,
	Tooltip_UpdatePosition_m00B1FD60B932FBFC513F083B6A75ADB5000D035A,
	Tooltip_Show_m590E72FA6BDB88790F5503033BB8A362DC0A401C,
	Tooltip_Show_m938C9CD44B640F89A126BAC9F5E4A15A9E30C49D,
	Tooltip_Show_mF6CF35356F5DE860CE7BB46D55EE48484117C542,
	Tooltip_Show_mEBEEC8B864ED8E4461EDD76C88679F8A572C8ABA,
	Tooltip_Show_m6F3CDAEAD6E05A0482C25696A4893968F502634A,
	Tooltip_Close_mD93243DBD31E7CEFB0F596E397888EB407C6D10A,
	Tooltip_CreateSlot_m525E1D56281521F01BDD974F8CB13F28C397D6CC,
	Tooltip__ctor_m3EC8D155B71B28C38BAA8AC8A2F9D72FE639BF99,
	EasingEquations_GetValue_m8A9844FEFD063AF19C92CEFD0E84F5ED6BF955B4,
	EasingEquations_Linear_m8AA418D8174295B504085A6D86D69C03728FF7AC,
	EasingEquations_CLerp_m724A9F482CA4C5EFEEB7C38C13F2D4CD0F9FA4EF,
	EasingEquations_Spring_mB3A66B66170B55B3D9316F86D643B5A70D5CADA5,
	EasingEquations_EaseInQuad_m84D48C68A17FCF903B0945BE82B6CE61F9183AEE,
	EasingEquations_EaseOutQuad_m94DFD9A379D6FD8E07FFE2A27B0B74ACF53DA2E4,
	EasingEquations_EaseInOutQuad_m9DC01AF74BBE2BFA1B05224E32F1D8EBAB1C0FCB,
	EasingEquations_EaseInCubic_m1293CE867EC35E8BC2BDD34E9EB56D4A1851E264,
	EasingEquations_EaseOutCubic_m6B776535C2BD3DE309E50CDC42A615025BF81D29,
	EasingEquations_EaseInOutCubic_mCEE19E466CD1B13042D6C2DC2C826E4178F97EA4,
	EasingEquations_EaseInQuart_m114DA1D8C44DEA2FD1FC311EA8AFFBEB08CEFF91,
	EasingEquations_EaseOutQuart_mD38A34E0F3F2A5CA1CBBB8A6D678DABBB2C81905,
	EasingEquations_EaseInOutQuart_m0F6B811DEC08B01430B13D83119C30EDA1E5056F,
	EasingEquations_EaseInQuint_mDEE3E587C9A3BFA2D1D610D3216A178C29A2FAA1,
	EasingEquations_EaseOutQuint_m64408A809BAF50F1FFF4048CDC88B65DC2AF6B46,
	EasingEquations_EaseInOutQuint_mE73C9712D9882A9444BCEE672A15EB08C31852BA,
	EasingEquations_EaseInSine_m73B114BF9CC516AE6B6F2AE1DA9A33F071B3E5EA,
	EasingEquations_EaseOutSine_m03182B2C83533A6061CB0BD8EFD9712AB8521B3F,
	EasingEquations_EaseInOutSine_m9F9CDF0DBF51EB150EEC718ACB89A7DE8CBBE5B0,
	EasingEquations_EaseInExpo_m4942C117C29DCAB5877E79D59D162F87CD5DD9ED,
	EasingEquations_EaseOutExpo_mA71AAE9886C9EE79CAB066F4E3D84E01C090CE4F,
	EasingEquations_EaseInOutExpo_m156C792FEE72487A4D361D116CE477A8A5A4B88B,
	EasingEquations_EaseInCirc_m9F17E6BA9F781F0384F9359C6814221970A22019,
	EasingEquations_EaseOutCirc_m4449842668433EFFA97B976620D382704008E205,
	EasingEquations_EaseInOutCirc_mF7770E0532730D38EE1FB3D9B82328A17C43F969,
	EasingEquations_EaseInBounce_mFA84819F21A3A75994EB9ED3EFE113B4152CD49C,
	EasingEquations_EaseOutBounce_m585EAEAD9639A78D61C9495A7F9D836D6093AEB7,
	EasingEquations_EaseInOutBounce_m9FC65BFE721D7B5570206D38AD70A05D9E09482E,
	EasingEquations_EaseInBack_m2E66A84534C1909FDE7C1F5BE96270D65E987A58,
	EasingEquations_EaseOutBack_m5D393B1A6E017F354E0AB81C4DC5BE19C27F6120,
	EasingEquations_EaseInOutBack_m64F9B7691A0587B29FAC9A53545B728D53B1B44C,
	EasingEquations_Punch_m81179B587608074C2AAAABDE1129AFC884F15218,
	EasingEquations_EaseInElastic_m9FA14A540D82BEE5C8F63BAC83F9E36E0A0CC2C0,
	EasingEquations_EaseOutElastic_mC53E0D25A0582A83A906F6F2F8155F2BE0F09FB4,
	EasingEquations_EaseInOutElastic_mE7F3FBE915817EEE8B3AAAB6F16E28E84C0B8D6B,
	FloatTween_get_easeType_m5AE9B9DAA1ADAFC83EE8335C4F0FB6257040DA45,
	FloatTween_set_easeType_mB81F091D306B341F606E116423A990374DF45A6E,
	FloatTween_get_startValue_mB4E95945BC704405477C63C7C9DB4256C20B4250,
	FloatTween_set_startValue_m145998BBC39EBB2FF11C409D24D199D18E837F89,
	FloatTween_get_targetValue_m026D871C9654D86762C8CF1E948C61CDFBA9D72E,
	FloatTween_set_targetValue_m5926751985717165AE47AB308DFA02B2F6E80917,
	FloatTween_get_duration_m5FC9D0826EC286D6A338152866136DEE60A3EA19,
	FloatTween_set_duration_m1A2F07057EB8A05C5E20F79B888CB7A6FC8BE8F1,
	FloatTween_get_ignoreTimeScale_m10E4EF5A8DAD3C4AD74C368CB4D6735A6EF7A671,
	FloatTween_set_ignoreTimeScale_mE9CC9FC7278D1309995CE5DEF1341415B1BE43D9,
	FloatTween_ValidTarget_m95BC102F85EE8D62AA6F7D5ABB151BD824007A38,
	FloatTween_TweenValue_m3D5F0CD6439CF0663AE94A54E2C431A7B2058AF3,
	FloatTween_AddOnChangedCallback_mF2139D9EE9D140F88AE5C8EEC68301313D6B58A4,
	FloatTween_AddOnFinishCallback_m7C1671512DC6795772CB7AEF93E031778D0A6E9B,
	FloatTween_OnFinish_m2685C1D9DE4C2B2CD3833639238B216BA42E0425,
	FloatTweenCallback__ctor_mFC58A0F5CCB7E46B6B7BFA48EA48E72C5910AFBC,
	FloatTweenFinishCallback__ctor_mBC2DDDB890D45A8FE7AA6ABF6255D70A4D28890B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Vector3Tween_get_easeType_mE27B9489BC5A292E768BD80FC246A789282252B8,
	Vector3Tween_set_easeType_m8D0B0ACD8E02296885392239EE7CB50D7F5FE7F0,
	Vector3Tween_get_startValue_m6074B9866D62069F059169C88B23D356813F03B5,
	Vector3Tween_set_startValue_mF12AE96E079BA13ED7D4008C85727DBF87AA58CF,
	Vector3Tween_get_targetValue_m23C2DE45D4516FC54E470705F956D328B72B027E,
	Vector3Tween_set_targetValue_mB221AF0136B70F0AFE523E8C554E8918A6AAB428,
	Vector3Tween_get_duration_mC9B6E53B72E8AB8CD7365E4A8BF7A824465EAE0E,
	Vector3Tween_set_duration_mCF129F58AFC949013EE6B871DAE39629A7C580C6,
	Vector3Tween_get_ignoreTimeScale_m38393D8728D506CE6D7BBAA7C564C963782FEF75,
	Vector3Tween_set_ignoreTimeScale_m00A8E47DC22181BCCB916EA4755F4F154EE6A050,
	Vector3Tween_ValidTarget_m93EA1330819CA2015F6FEA505E0BFC098701B5D2,
	Vector3Tween_TweenValue_m7E0DA22520A51A3D60CC7BE3EDA16ADFB92310A6,
	Vector3Tween_AddOnChangedCallback_mF67324A4410FB7D4B0BA4029F80EA6B77D67AEF7,
	Vector3Tween_AddOnFinishCallback_m53F9863979C3EC02B11828B48FC7ABAE7EA09CC9,
	Vector3Tween_OnFinish_m0A7A21F91630309FBA093DA6E6F33766B39D5C37,
	Vector3TweenCallback__ctor_m3D1C7E31E90F13546CA7C782AC51C47104306867,
	Vector3TweenFinishCallback__ctor_mF52642B843EEBE01092AA33989E5838EBBCA2934,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UICursor_Awake_m631C441404A6545059403BD4672FFEA0DBFE43AE,
	UICursor_Start_mCFBBDBABC9E8BA5F34D857326627F52D352DD318,
	UICursor_Update_mA38909B4D9C4EFFD7BB57F8ADF7AED4588C13866,
	UICursor_Clear_mEC459A306806D62E6C950335C025F0A662CED54A,
	UICursor_Set_m6D97184AB88923C79EE17219F4C3430A6C2B46DE,
	UICursor_Set_m1DF1A6044FCA6E654AE7D9CB710142347E4F9A44,
	UICursor_SetAnimatorState_m3140E69AF1B9B31E1EF93A727D574F1188F042E2,
	UICursor__ctor_mA5496A7D0F16504E17E8F0076C2602878A2E0E18,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UIWidget_get_Name_mE16241740E8F042B088BD63CA5D11C7F90EBF90B,
	UIWidget_set_Name_m580DC27C5C1E00396061212BD19EB29469F7A607,
	UIWidget_get_Callbacks_m3E34C09721550CE3046B91ADE15D12CA1BB5901A,
	UIWidget_get_IgnoreTimeScale_mF470882883F5E3AC0417210E5CC8E2F2D01FFECE,
	UIWidget_get_IsVisible_mEAC0E2D530D74C0C034BBC03911E445AA61D45B2,
	UIWidget_get_IsLocked_m2CEE2B8763515850A94880C5A3520711FF21C92E,
	UIWidget_Awake_m7F616A3A3ECBD9A447516C2F2524439BDA55BDC7,
	UIWidget_OnAwake_mE02173557D4CD93A48A80C4F2BAEBDFF56A2BF67,
	UIWidget_Start_m2FD5F5705D265598AB58C095151D48AD8823D98A,
	UIWidget_OnStart_mB6AFD66AAB8CF245F0CAA1258BDF8DFF63FA499A,
	UIWidget_OnDelayedStart_m29784EEA937B392528DF388ED658083EFFA584D7,
	UIWidget_Update_m43B50EEDC34958DDB9CEAF07E592BED2E44CC75A,
	UIWidget_Show_m3C225D4387EBB51A179C3AD8699DFCF4762ACF1F,
	UIWidget_Close_m67F15777E8821BCBD38F52B66C41BDD2E71151F7,
	UIWidget_TweenCanvasGroupAlpha_m94B0857E31B9BA737B9A58A5FD736A898F4673A9,
	UIWidget_TweenTransformScale_mD02C4E841628E1A9A1382547D158B6D20D020D51,
	UIWidget_Toggle_m0C404198641C48199C8AAB38587E421CE1970935,
	UIWidget_Focus_m573FD812FB9415278E4783365FD1063F28B8C36C,
	UIWidget_OnDestroy_m49945D1897E4A2D6043B514F757B6D35498462BE,
	UIWidget_Lock_mE658823E4FC60AFC82BF0F6F537ACE3FB5090C8C,
	UIWidget_LockAll_mA823095BBB8194184F7A6D9D1E6AF7659C1EAD12,
	UIWidget__ctor_m267261D67D162731033FAC773BC3A968D86C3D21,
	UIWidget__cctor_m67E4979B5B8C9CF4240F0431F738CE36C9EE6EBA,
	UIWidget_U3CTweenTransformScaleU3Eb__48_0_m29A828C4EB884DCBFE7E10F8F1BEE1F195F63051,
	U3COnDelayedStartU3Ed__43__ctor_mC964BF60C0BFC9443251FA4E80D6BEC534845B60,
	U3COnDelayedStartU3Ed__43_System_IDisposable_Dispose_m2A142D07BA5F9D45043DFEFA39A96AE20A625D3B,
	U3COnDelayedStartU3Ed__43_MoveNext_mA271E2A9D464ACBAD540653831EEB3EC75A7B664,
	U3COnDelayedStartU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96BC0C790C4AFAA8E5FABB91BAB4E09E6A031D68,
	U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_Reset_mA6EFD5C346DD2AA0C17B009E2EF9192CD345D206,
	U3COnDelayedStartU3Ed__43_System_Collections_IEnumerator_get_Current_m34A7FD6BEE90C1B43DA48518D95DF0530B61B437,
	U3CU3Ec__DisplayClass47_0__ctor_m756D123C504F3C609E7A250A54A52826DF90691E,
	U3CU3Ec__DisplayClass47_0_U3CTweenCanvasGroupAlphaU3Eb__0_mEAC040D6075F349790777E60A09A601B42DF17D1,
	U3CU3Ec__DisplayClass47_0_U3CTweenCanvasGroupAlphaU3Eb__1_m93FE704DCFDE235395F2155BD87C0BA91A1FF0E4,
	WidgetInputHandler_Update_m2D661C09A63390AA469D93E9429FBB125DAA1272,
	WidgetInputHandler_RegisterInput_m8149D9F46777EAB5F4627663B2BFC1BE5750146D,
	WidgetInputHandler_UnregisterInput_m9F7D11ECC07855E4E6D3DB128050E6C8315F98A7,
	WidgetInputHandler__ctor_mDC061BBA76B0079A47155252F81B3779462778C1,
	U3CU3Ec__cctor_m85A8AD82EDE3A6C8F483D965E47039A8E2670565,
	U3CU3Ec__ctor_m5341B56E8A3712909F0DD4268CA6A59DB92EA2C4,
	U3CU3Ec_U3CRegisterInputU3Eb__2_0_mDF7522AC00D6269DD2BFF63F9EE7BDF88E3201F6,
	NULL,
	WidgetUtility__cctor_mA2E1018D44CD9867D124EB2CD8A617BC586B4713,
	WidgetUtility_ChangedActiveScene_m22B3E05948F0DD246A398D01369E62F1E8F9DE8B,
	NULL,
	NULL,
	WidgetUtility_PlaySound_m3B12A87114B2568D072271E771B76EEDEECD395D,
	WidgetUtility_ColorToHex_m2CB8C25CE4D32DBCE9114DC8FFF89C871037BDE5,
	WidgetUtility_HexToColor_m9A50C9B8889A44B33ED2D763D4BCFBE2FC127F30,
	WidgetUtility_ColorString_m06633A32FBEC62185AF3DDCA2D9ACFAF8E1EF382,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void FloatTween_get_easeType_m5AE9B9DAA1ADAFC83EE8335C4F0FB6257040DA45_AdjustorThunk (void);
extern void FloatTween_set_easeType_mB81F091D306B341F606E116423A990374DF45A6E_AdjustorThunk (void);
extern void FloatTween_get_startValue_mB4E95945BC704405477C63C7C9DB4256C20B4250_AdjustorThunk (void);
extern void FloatTween_set_startValue_m145998BBC39EBB2FF11C409D24D199D18E837F89_AdjustorThunk (void);
extern void FloatTween_get_targetValue_m026D871C9654D86762C8CF1E948C61CDFBA9D72E_AdjustorThunk (void);
extern void FloatTween_set_targetValue_m5926751985717165AE47AB308DFA02B2F6E80917_AdjustorThunk (void);
extern void FloatTween_get_duration_m5FC9D0826EC286D6A338152866136DEE60A3EA19_AdjustorThunk (void);
extern void FloatTween_set_duration_m1A2F07057EB8A05C5E20F79B888CB7A6FC8BE8F1_AdjustorThunk (void);
extern void FloatTween_get_ignoreTimeScale_m10E4EF5A8DAD3C4AD74C368CB4D6735A6EF7A671_AdjustorThunk (void);
extern void FloatTween_set_ignoreTimeScale_mE9CC9FC7278D1309995CE5DEF1341415B1BE43D9_AdjustorThunk (void);
extern void FloatTween_ValidTarget_m95BC102F85EE8D62AA6F7D5ABB151BD824007A38_AdjustorThunk (void);
extern void FloatTween_TweenValue_m3D5F0CD6439CF0663AE94A54E2C431A7B2058AF3_AdjustorThunk (void);
extern void FloatTween_AddOnChangedCallback_mF2139D9EE9D140F88AE5C8EEC68301313D6B58A4_AdjustorThunk (void);
extern void FloatTween_AddOnFinishCallback_m7C1671512DC6795772CB7AEF93E031778D0A6E9B_AdjustorThunk (void);
extern void FloatTween_OnFinish_m2685C1D9DE4C2B2CD3833639238B216BA42E0425_AdjustorThunk (void);
extern void Vector3Tween_get_easeType_mE27B9489BC5A292E768BD80FC246A789282252B8_AdjustorThunk (void);
extern void Vector3Tween_set_easeType_m8D0B0ACD8E02296885392239EE7CB50D7F5FE7F0_AdjustorThunk (void);
extern void Vector3Tween_get_startValue_m6074B9866D62069F059169C88B23D356813F03B5_AdjustorThunk (void);
extern void Vector3Tween_set_startValue_mF12AE96E079BA13ED7D4008C85727DBF87AA58CF_AdjustorThunk (void);
extern void Vector3Tween_get_targetValue_m23C2DE45D4516FC54E470705F956D328B72B027E_AdjustorThunk (void);
extern void Vector3Tween_set_targetValue_mB221AF0136B70F0AFE523E8C554E8918A6AAB428_AdjustorThunk (void);
extern void Vector3Tween_get_duration_mC9B6E53B72E8AB8CD7365E4A8BF7A824465EAE0E_AdjustorThunk (void);
extern void Vector3Tween_set_duration_mCF129F58AFC949013EE6B871DAE39629A7C580C6_AdjustorThunk (void);
extern void Vector3Tween_get_ignoreTimeScale_m38393D8728D506CE6D7BBAA7C564C963782FEF75_AdjustorThunk (void);
extern void Vector3Tween_set_ignoreTimeScale_m00A8E47DC22181BCCB916EA4755F4F154EE6A050_AdjustorThunk (void);
extern void Vector3Tween_ValidTarget_m93EA1330819CA2015F6FEA505E0BFC098701B5D2_AdjustorThunk (void);
extern void Vector3Tween_TweenValue_m7E0DA22520A51A3D60CC7BE3EDA16ADFB92310A6_AdjustorThunk (void);
extern void Vector3Tween_AddOnChangedCallback_mF67324A4410FB7D4B0BA4029F80EA6B77D67AEF7_AdjustorThunk (void);
extern void Vector3Tween_AddOnFinishCallback_m53F9863979C3EC02B11828B48FC7ABAE7EA09CC9_AdjustorThunk (void);
extern void Vector3Tween_OnFinish_m0A7A21F91630309FBA093DA6E6F33766B39D5C37_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[30] = 
{
	{ 0x060000F6, FloatTween_get_easeType_m5AE9B9DAA1ADAFC83EE8335C4F0FB6257040DA45_AdjustorThunk },
	{ 0x060000F7, FloatTween_set_easeType_mB81F091D306B341F606E116423A990374DF45A6E_AdjustorThunk },
	{ 0x060000F8, FloatTween_get_startValue_mB4E95945BC704405477C63C7C9DB4256C20B4250_AdjustorThunk },
	{ 0x060000F9, FloatTween_set_startValue_m145998BBC39EBB2FF11C409D24D199D18E837F89_AdjustorThunk },
	{ 0x060000FA, FloatTween_get_targetValue_m026D871C9654D86762C8CF1E948C61CDFBA9D72E_AdjustorThunk },
	{ 0x060000FB, FloatTween_set_targetValue_m5926751985717165AE47AB308DFA02B2F6E80917_AdjustorThunk },
	{ 0x060000FC, FloatTween_get_duration_m5FC9D0826EC286D6A338152866136DEE60A3EA19_AdjustorThunk },
	{ 0x060000FD, FloatTween_set_duration_m1A2F07057EB8A05C5E20F79B888CB7A6FC8BE8F1_AdjustorThunk },
	{ 0x060000FE, FloatTween_get_ignoreTimeScale_m10E4EF5A8DAD3C4AD74C368CB4D6735A6EF7A671_AdjustorThunk },
	{ 0x060000FF, FloatTween_set_ignoreTimeScale_mE9CC9FC7278D1309995CE5DEF1341415B1BE43D9_AdjustorThunk },
	{ 0x06000100, FloatTween_ValidTarget_m95BC102F85EE8D62AA6F7D5ABB151BD824007A38_AdjustorThunk },
	{ 0x06000101, FloatTween_TweenValue_m3D5F0CD6439CF0663AE94A54E2C431A7B2058AF3_AdjustorThunk },
	{ 0x06000102, FloatTween_AddOnChangedCallback_mF2139D9EE9D140F88AE5C8EEC68301313D6B58A4_AdjustorThunk },
	{ 0x06000103, FloatTween_AddOnFinishCallback_m7C1671512DC6795772CB7AEF93E031778D0A6E9B_AdjustorThunk },
	{ 0x06000104, FloatTween_OnFinish_m2685C1D9DE4C2B2CD3833639238B216BA42E0425_AdjustorThunk },
	{ 0x06000117, Vector3Tween_get_easeType_mE27B9489BC5A292E768BD80FC246A789282252B8_AdjustorThunk },
	{ 0x06000118, Vector3Tween_set_easeType_m8D0B0ACD8E02296885392239EE7CB50D7F5FE7F0_AdjustorThunk },
	{ 0x06000119, Vector3Tween_get_startValue_m6074B9866D62069F059169C88B23D356813F03B5_AdjustorThunk },
	{ 0x0600011A, Vector3Tween_set_startValue_mF12AE96E079BA13ED7D4008C85727DBF87AA58CF_AdjustorThunk },
	{ 0x0600011B, Vector3Tween_get_targetValue_m23C2DE45D4516FC54E470705F956D328B72B027E_AdjustorThunk },
	{ 0x0600011C, Vector3Tween_set_targetValue_mB221AF0136B70F0AFE523E8C554E8918A6AAB428_AdjustorThunk },
	{ 0x0600011D, Vector3Tween_get_duration_mC9B6E53B72E8AB8CD7365E4A8BF7A824465EAE0E_AdjustorThunk },
	{ 0x0600011E, Vector3Tween_set_duration_mCF129F58AFC949013EE6B871DAE39629A7C580C6_AdjustorThunk },
	{ 0x0600011F, Vector3Tween_get_ignoreTimeScale_m38393D8728D506CE6D7BBAA7C564C963782FEF75_AdjustorThunk },
	{ 0x06000120, Vector3Tween_set_ignoreTimeScale_m00A8E47DC22181BCCB916EA4755F4F154EE6A050_AdjustorThunk },
	{ 0x06000121, Vector3Tween_ValidTarget_m93EA1330819CA2015F6FEA505E0BFC098701B5D2_AdjustorThunk },
	{ 0x06000122, Vector3Tween_TweenValue_m7E0DA22520A51A3D60CC7BE3EDA16ADFB92310A6_AdjustorThunk },
	{ 0x06000123, Vector3Tween_AddOnChangedCallback_mF67324A4410FB7D4B0BA4029F80EA6B77D67AEF7_AdjustorThunk },
	{ 0x06000124, Vector3Tween_AddOnFinishCallback_m53F9863979C3EC02B11828B48FC7ABAE7EA09CC9_AdjustorThunk },
	{ 0x06000125, Vector3Tween_OnFinish_m0A7A21F91630309FBA093DA6E6F33766B39D5C37_AdjustorThunk },
};
static const int32_t s_InvokerIndices[383] = 
{
	2941,
	2975,
	2448,
	1477,
	2484,
	2975,
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2434,
	2975,
	2975,
	2975,
	2448,
	2484,
	2975,
	2975,
	2448,
	2975,
	4649,
	2975,
	2434,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	1898,
	2975,
	2975,
	2975,
	2975,
	2975,
	1088,
	2975,
	2975,
	2975,
	4649,
	2975,
	2133,
	2975,
	931,
	931,
	627,
	332,
	1898,
	2975,
	2975,
	2975,
	2975,
	2975,
	4649,
	2975,
	2133,
	2975,
	2448,
	2448,
	2281,
	2975,
	2975,
	2975,
	619,
	2975,
	2975,
	3641,
	4566,
	2975,
	4649,
	2975,
	2975,
	2975,
	-1,
	2968,
	2975,
	2448,
	2448,
	2448,
	2975,
	2975,
	2975,
	2912,
	2448,
	2975,
	2448,
	2448,
	2975,
	1147,
	1147,
	830,
	2975,
	2448,
	2975,
	2975,
	1444,
	477,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2484,
	2975,
	2448,
	2975,
	2975,
	931,
	2975,
	2975,
	1898,
	2975,
	4649,
	2975,
	1900,
	2133,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	2975,
	2975,
	2484,
	2975,
	2975,
	2477,
	2975,
	2975,
	2484,
	2434,
	2975,
	2975,
	2975,
	2975,
	2434,
	1325,
	2975,
	2975,
	2941,
	2975,
	2975,
	2448,
	2448,
	1903,
	2975,
	2975,
	2975,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2448,
	2448,
	2975,
	2948,
	2484,
	2448,
	2975,
	2975,
	2975,
	2912,
	2912,
	2975,
	2975,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2798,
	2344,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	1444,
	627,
	207,
	2975,
	2912,
	2975,
	3594,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	3834,
	4189,
	3834,
	3834,
	3834,
	2897,
	2434,
	2948,
	2484,
	2948,
	2484,
	2948,
	2484,
	2941,
	2477,
	2941,
	2484,
	2448,
	2448,
	2975,
	2975,
	2975,
	2484,
	2941,
	2948,
	2941,
	2975,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2897,
	2434,
	2970,
	2508,
	2970,
	2508,
	2948,
	2484,
	2941,
	2477,
	2941,
	2484,
	2448,
	2448,
	2975,
	2975,
	2975,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2975,
	2975,
	2975,
	4649,
	4566,
	3657,
	2448,
	2975,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2912,
	2448,
	2912,
	2941,
	2941,
	2941,
	2975,
	2975,
	2975,
	2975,
	2912,
	2975,
	2975,
	2975,
	1477,
	1489,
	2975,
	2975,
	2975,
	2477,
	4571,
	2975,
	4649,
	2508,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2484,
	2975,
	2975,
	4241,
	4241,
	2975,
	4649,
	2975,
	2133,
	-1,
	4649,
	4287,
	-1,
	-1,
	4281,
	4468,
	4366,
	4061,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x0200003B, { 0, 6 } },
	{ 0x0200003C, { 6, 1 } },
	{ 0x02000040, { 7, 29 } },
	{ 0x02000042, { 36, 3 } },
	{ 0x02000049, { 65, 1 } },
	{ 0x0200004A, { 66, 5 } },
	{ 0x0200004B, { 71, 5 } },
	{ 0x0600016D, { 39, 2 } },
	{ 0x06000170, { 41, 15 } },
	{ 0x06000171, { 56, 9 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[76] = 
{
	{ (Il2CppRGCTXDataType)2, 982 },
	{ (Il2CppRGCTXDataType)3, 352 },
	{ (Il2CppRGCTXDataType)3, 17860 },
	{ (Il2CppRGCTXDataType)2, 551 },
	{ (Il2CppRGCTXDataType)3, 17859 },
	{ (Il2CppRGCTXDataType)2, 3967 },
	{ (Il2CppRGCTXDataType)2, 694 },
	{ (Il2CppRGCTXDataType)3, 10794 },
	{ (Il2CppRGCTXDataType)2, 3004 },
	{ (Il2CppRGCTXDataType)3, 10597 },
	{ (Il2CppRGCTXDataType)3, 17885 },
	{ (Il2CppRGCTXDataType)3, 17883 },
	{ (Il2CppRGCTXDataType)3, 17910 },
	{ (Il2CppRGCTXDataType)3, 17887 },
	{ (Il2CppRGCTXDataType)3, 10796 },
	{ (Il2CppRGCTXDataType)3, 10797 },
	{ (Il2CppRGCTXDataType)3, 17912 },
	{ (Il2CppRGCTXDataType)2, 553 },
	{ (Il2CppRGCTXDataType)3, 10599 },
	{ (Il2CppRGCTXDataType)3, 17915 },
	{ (Il2CppRGCTXDataType)3, 17909 },
	{ (Il2CppRGCTXDataType)3, 10598 },
	{ (Il2CppRGCTXDataType)3, 17886 },
	{ (Il2CppRGCTXDataType)3, 17911 },
	{ (Il2CppRGCTXDataType)3, 17884 },
	{ (Il2CppRGCTXDataType)3, 21694 },
	{ (Il2CppRGCTXDataType)3, 21962 },
	{ (Il2CppRGCTXDataType)3, 22121 },
	{ (Il2CppRGCTXDataType)3, 10795 },
	{ (Il2CppRGCTXDataType)3, 17914 },
	{ (Il2CppRGCTXDataType)3, 17913 },
	{ (Il2CppRGCTXDataType)3, 10793 },
	{ (Il2CppRGCTXDataType)3, 17888 },
	{ (Il2CppRGCTXDataType)3, 15505 },
	{ (Il2CppRGCTXDataType)2, 3025 },
	{ (Il2CppRGCTXDataType)3, 10792 },
	{ (Il2CppRGCTXDataType)3, 17916 },
	{ (Il2CppRGCTXDataType)3, 17917 },
	{ (Il2CppRGCTXDataType)2, 554 },
	{ (Il2CppRGCTXDataType)3, 22955 },
	{ (Il2CppRGCTXDataType)3, 21857 },
	{ (Il2CppRGCTXDataType)2, 957 },
	{ (Il2CppRGCTXDataType)3, 181 },
	{ (Il2CppRGCTXDataType)3, 21693 },
	{ (Il2CppRGCTXDataType)3, 182 },
	{ (Il2CppRGCTXDataType)2, 1832 },
	{ (Il2CppRGCTXDataType)3, 7549 },
	{ (Il2CppRGCTXDataType)3, 21979 },
	{ (Il2CppRGCTXDataType)2, 939 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)2, 1833 },
	{ (Il2CppRGCTXDataType)3, 7550 },
	{ (Il2CppRGCTXDataType)3, 21893 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)3, 21792 },
	{ (Il2CppRGCTXDataType)3, 21939 },
	{ (Il2CppRGCTXDataType)3, 21692 },
	{ (Il2CppRGCTXDataType)2, 942 },
	{ (Il2CppRGCTXDataType)3, 102 },
	{ (Il2CppRGCTXDataType)2, 1831 },
	{ (Il2CppRGCTXDataType)3, 7548 },
	{ (Il2CppRGCTXDataType)3, 21892 },
	{ (Il2CppRGCTXDataType)3, 101 },
	{ (Il2CppRGCTXDataType)3, 21791 },
	{ (Il2CppRGCTXDataType)3, 21938 },
	{ (Il2CppRGCTXDataType)2, 702 },
	{ (Il2CppRGCTXDataType)2, 940 },
	{ (Il2CppRGCTXDataType)3, 96 },
	{ (Il2CppRGCTXDataType)2, 940 },
	{ (Il2CppRGCTXDataType)2, 700 },
	{ (Il2CppRGCTXDataType)1, 700 },
	{ (Il2CppRGCTXDataType)2, 943 },
	{ (Il2CppRGCTXDataType)3, 103 },
	{ (Il2CppRGCTXDataType)2, 943 },
	{ (Il2CppRGCTXDataType)2, 701 },
	{ (Il2CppRGCTXDataType)1, 701 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_UIWidgets_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_UIWidgets_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_UIWidgets_CodeGenModule = 
{
	"DevionGames.UIWidgets.dll",
	383,
	s_methodPointers,
	30,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	76,
	s_rgctxValues,
	NULL,
	g_DevionGames_UIWidgets_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
