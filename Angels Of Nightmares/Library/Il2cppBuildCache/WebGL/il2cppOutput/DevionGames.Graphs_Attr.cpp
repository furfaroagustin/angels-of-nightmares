﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// DevionGames.Graphs.NodeStyleAttribute
struct NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B;
// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.ComponentMenu::m_ComponentMenu
	String_t* ___m_ComponentMenu_0;

public:
	inline static int32_t get_offset_of_m_ComponentMenu_0() { return static_cast<int32_t>(offsetof(ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8, ___m_ComponentMenu_0)); }
	inline String_t* get_m_ComponentMenu_0() const { return ___m_ComponentMenu_0; }
	inline String_t** get_address_of_m_ComponentMenu_0() { return &___m_ComponentMenu_0; }
	inline void set_m_ComponentMenu_0(String_t* value)
	{
		___m_ComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentMenu_0), (void*)value);
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.Graphs.InputAttribute
struct InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean DevionGames.Graphs.InputAttribute::label
	bool ___label_0;
	// System.Boolean DevionGames.Graphs.InputAttribute::port
	bool ___port_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___label_0)); }
	inline bool get_label_0() const { return ___label_0; }
	inline bool* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(bool value)
	{
		___label_0 = value;
	}

	inline static int32_t get_offset_of_port_1() { return static_cast<int32_t>(offsetof(InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8, ___port_1)); }
	inline bool get_port_1() const { return ___port_1; }
	inline bool* get_address_of_port_1() { return &___port_1; }
	inline void set_port_1(bool value)
	{
		___port_1 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// DevionGames.Graphs.NodeStyleAttribute
struct NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.Graphs.NodeStyleAttribute::iconPath
	String_t* ___iconPath_0;
	// System.Boolean DevionGames.Graphs.NodeStyleAttribute::displayHeader
	bool ___displayHeader_1;
	// System.String DevionGames.Graphs.NodeStyleAttribute::category
	String_t* ___category_2;

public:
	inline static int32_t get_offset_of_iconPath_0() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___iconPath_0)); }
	inline String_t* get_iconPath_0() const { return ___iconPath_0; }
	inline String_t** get_address_of_iconPath_0() { return &___iconPath_0; }
	inline void set_iconPath_0(String_t* value)
	{
		___iconPath_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iconPath_0), (void*)value);
	}

	inline static int32_t get_offset_of_displayHeader_1() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___displayHeader_1)); }
	inline bool get_displayHeader_1() const { return ___displayHeader_1; }
	inline bool* get_address_of_displayHeader_1() { return &___displayHeader_1; }
	inline void set_displayHeader_1(bool value)
	{
		___displayHeader_1 = value;
	}

	inline static int32_t get_offset_of_category_2() { return static_cast<int32_t>(offsetof(NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B, ___category_2)); }
	inline String_t* get_category_2() const { return ___category_2; }
	inline String_t** get_address_of_category_2() { return &___category_2; }
	inline void set_category_2(String_t* value)
	{
		___category_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___category_2), (void*)value);
	}
};


// DevionGames.Graphs.OutputAttribute
struct OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void DevionGames.ComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3 (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String,System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, String_t* ___iconPath0, bool ___displayHeader1, String_t* ___category2, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.InputAttribute::.ctor(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * __this, bool ___label0, bool ___port1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.OutputAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * __this, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, bool ___displayHeader0, String_t* ___category1, const RuntimeMethod* method);
// System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * __this, bool ___displayHeader0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
static void DevionGames_Graphs_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
}
static void FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_CustomAttributesCacheGenerator_m_Ports(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x41\x64\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x41\x64\x64"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x41\x64\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D(tmp, true, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x41\x64\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x44\x69\x76\x69\x64\x65"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x45\x76\x61\x6C\x75\x61\x74\x65"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB(tmp, true, NULL);
	}
}
static void Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_time(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_curve(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, false, NULL);
	}
}
static void Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x41\x64\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D(tmp, true, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x41\x64\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x4D\x75\x6C\x74\x69\x70\x6C\x79"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x50\x6F\x77"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x50\x6F\x77"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_f(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_p(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[0];
		NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D(tmp, true, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x52\x61\x6E\x64\x6F\x6D"), NULL);
	}
}
static void Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x52\x6F\x75\x6E\x64"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x52\x6F\x75\x6E\x64"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x53\x71\x72\x74"), NULL);
	}
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[1];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x53\x71\x72\x74"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
}
static void Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[0];
		NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B(tmp, il2cpp_codegen_string_new_wrapper("\x49\x63\x6F\x6E\x73\x2F\x53\x75\x62\x74\x72\x61\x63\x74"), false, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x68\x2F\x53\x75\x62\x74\x72\x61\x63\x74"), NULL);
	}
}
static void Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 * tmp = (OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672 *)cache->attributes[0];
		OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A(tmp, NULL);
	}
}
static void InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_m_Connections(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_m_FieldTypeName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_Port_GetValues_m3F3BB0A9FC7B143D5AFCB3BC7D90D753C2FB7801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_0_0_0_var), NULL);
	}
}
static void Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1__ctor_m0A101000308F90E63197067958BFFD543CD4D7AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_IDisposable_Dispose_m4CE0E62A895CD8C09C8D39F1432D85C13BD9D9A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mB81D29B219F5D602DB67635975EC4F83D7101252(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerator_Reset_mA7F2396F9A73F6A6FB3EC9D740DB2C3C7A1D6005(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerator_get_Current_mC3C7C08801DA181E4542F933E756B6773EA5421E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m42D5397036DB3F88F7F4CC59414767B29DFE36A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerable_GetEnumerator_m995F944C5299B59BE20CBA309C3A008E9C2682CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x6D\x75\x6C\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x47\x72\x61\x70\x68\x73\x2F\x46\x6F\x72\x6D\x75\x6C\x61"), NULL);
	}
}
static void Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA_CustomAttributesCacheGenerator_m_Graph(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B * tmp = (NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B *)cache->attributes[0];
		NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB(tmp, true, NULL);
	}
}
static void FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_CustomAttributesCacheGenerator_result(CustomAttributesCache* cache)
{
	{
		InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 * tmp = (InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8 *)cache->attributes[0];
		InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C(tmp, false, true, NULL);
	}
}
static void Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9_CustomAttributesCacheGenerator_serializedObjects(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec__3_1_t269A0A84737B9C1B101F9B44EF59A3327C6DAEE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator_GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator_GraphUtility_ConvertToArray_m21F8FF4A9222C371AFE2EBDCB72E07E8D2A3C4DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_CustomAttributesCacheGenerator_id(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_CustomAttributesCacheGenerator_position(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_Graphs_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_Graphs_AttributeGenerators[78] = 
{
	U3CU3Ec_tE162F8CBE1863C4B5A89BE2D19E9F634BEA69646_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_tB8AA5CCCCCDED4C90B4804244F385A61FFE27A40_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_tF15D03302417D34C5C98073C8DB0E50D72604680_CustomAttributesCacheGenerator,
	Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator,
	Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator,
	Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator,
	Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator,
	Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator,
	Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator,
	Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator,
	Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator,
	Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator,
	Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator,
	Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator,
	InputAttribute_t472419C027211BA2215E05AD923A14C8EA296AC8_CustomAttributesCacheGenerator,
	OutputAttribute_tA46CE1390109231B2EA8CCB8E4B3484FC75D9672_CustomAttributesCacheGenerator,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_tD4ED9D6D8E5D55577C3F2E23C315C73B81CFE3F8_CustomAttributesCacheGenerator,
	Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA_CustomAttributesCacheGenerator,
	U3CU3Ec_t6AF98DC5585A09BE5DF9E8CE58B1FC5C4E50876F_CustomAttributesCacheGenerator,
	U3CU3Ec_t56EF29189FDC5A66D9DF98509E99B581C2586E9C_CustomAttributesCacheGenerator,
	FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_CustomAttributesCacheGenerator,
	U3CU3Ec__3_1_t269A0A84737B9C1B101F9B44EF59A3327C6DAEE4_CustomAttributesCacheGenerator,
	GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t11617DA8AEDFC1E84A3593E1C9D651083EE15509_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_1_t8A846A6E3A71589E863AD941F6AB0D4AC84656A7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t543B4544DEC8DC72A425AA92E773960C43F0F55D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_1_t5563080C2834E28FD1FCBCAA2F217C10DD6C39D0_CustomAttributesCacheGenerator,
	NodeStyleAttribute_tF81D0F32A1023C0522C1956C9C00160B00DAEE9B_CustomAttributesCacheGenerator,
	FlowNode_t9814105C6CCB5D0E2536932960E060EF3A3EE195_CustomAttributesCacheGenerator_m_Ports,
	Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_a,
	Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_b,
	Add_t5898E7271190FC8C8DAF39C23C6DF588FD83D5A4_CustomAttributesCacheGenerator_output,
	Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator_value,
	Ceil_tD8DA2B49479AAB2D1DAEE84F44DA299824037323_CustomAttributesCacheGenerator_output,
	Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_a,
	Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_b,
	Divide_t0E2802337E30A27C1C488271C61C2214772281FF_CustomAttributesCacheGenerator_output,
	Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_time,
	Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_curve,
	Evaluate_tB0B6B3B00C4BC15253DF56E50A97FF4D55B99D39_CustomAttributesCacheGenerator_output,
	Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator_value,
	Floor_t2F42E789814BE9B7D244A83ECCD83714CEC1A4C9_CustomAttributesCacheGenerator_output,
	Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_a,
	Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_b,
	Multiply_t6A832C7372A7747DA5A795E54168B922700D2B6F_CustomAttributesCacheGenerator_output,
	Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_f,
	Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_p,
	Pow_tF5D104DB38B58DA27646CB016D47D60A81C5A5EC_CustomAttributesCacheGenerator_output,
	Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_a,
	Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_b,
	Random_t1E48F93FC97E47B6D8D4279E361113F3337747F7_CustomAttributesCacheGenerator_output,
	Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator_value,
	Round_t56F3F60F828073AF3CF529B4967D0DEA7BC2FC3C_CustomAttributesCacheGenerator_output,
	Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator_value,
	Sqrt_t392717D34D5F63AD31E060BCEB856073EBB2BBDF_CustomAttributesCacheGenerator_output,
	Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_a,
	Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_b,
	Subtract_tCF83E491F27DA9D36E5F73C1A87DDFFEA324AD71_CustomAttributesCacheGenerator_output,
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_m_Connections,
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_m_FieldTypeName,
	Formula_tD3EA1D6E444749FAD3ACAE440E9BFA8F0B7D16EA_CustomAttributesCacheGenerator_m_Graph,
	FormulaOutput_t52B41400CE886BB4427FA79272B56E2B77D56BC5_CustomAttributesCacheGenerator_result,
	Graph_t1C08328620FBDF4E0D248DE14CEC06475C4DD5B9_CustomAttributesCacheGenerator_serializedObjects,
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_CustomAttributesCacheGenerator_id,
	Node_tD0AFD8B27090B24E967D338520DEF077662A61DF_CustomAttributesCacheGenerator_position,
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_Port_GetValues_m3F3BB0A9FC7B143D5AFCB3BC7D90D753C2FB7801,
	Port_t2C4EFE45D169844652A9EE42454BE7B51C5307C5_CustomAttributesCacheGenerator_Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1__ctor_m0A101000308F90E63197067958BFFD543CD4D7AC,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_IDisposable_Dispose_m4CE0E62A895CD8C09C8D39F1432D85C13BD9D9A6,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_mB81D29B219F5D602DB67635975EC4F83D7101252,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerator_Reset_mA7F2396F9A73F6A6FB3EC9D740DB2C3C7A1D6005,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerator_get_Current_mC3C7C08801DA181E4542F933E756B6773EA5421E,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m42D5397036DB3F88F7F4CC59414767B29DFE36A6,
	U3CGetValuesU3Ed__16_1_t5C2C5CA351FED2578CEA8E5BB70123F8453D3234_CustomAttributesCacheGenerator_U3CGetValuesU3Ed__16_1_System_Collections_IEnumerable_GetEnumerator_m995F944C5299B59BE20CBA309C3A008E9C2682CF,
	GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator_GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC,
	GraphUtility_t5B16094ACB12853D01FBA9A1F549D533E23EEC4D_CustomAttributesCacheGenerator_GraphUtility_ConvertToArray_m21F8FF4A9222C371AFE2EBDCB72E07E8D2A3C4DF,
	DevionGames_Graphs_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
