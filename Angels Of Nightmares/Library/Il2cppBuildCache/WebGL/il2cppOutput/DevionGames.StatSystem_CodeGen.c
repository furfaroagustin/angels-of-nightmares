﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Object DevionGames.Graphs.GetStatCurrentValue::OnRequestValue(DevionGames.Graphs.Port)
extern void GetStatCurrentValue_OnRequestValue_m85270F6EFDC3636B24E0E7F9B3BFC72E7AA0C5DC (void);
// 0x00000002 System.Void DevionGames.Graphs.GetStatCurrentValue::.ctor()
extern void GetStatCurrentValue__ctor_mF8E281CB6F3D55E50FEB5BAF18DA6FD6BB328F41 (void);
// 0x00000003 System.Object DevionGames.Graphs.GetStatValue::OnRequestValue(DevionGames.Graphs.Port)
extern void GetStatValue_OnRequestValue_mCECCC851AC9A912E0F388DC08F4328238F0FAC13 (void);
// 0x00000004 System.Void DevionGames.Graphs.GetStatValue::.ctor()
extern void GetStatValue__ctor_m92D9D9416D276C81C6E7B8F2024680F5671FCF2A (void);
// 0x00000005 System.Void DevionGames.Graphs.StatNode::.ctor()
extern void StatNode__ctor_m636576AA00A84331DA39A29E237AF6C4A747588C (void);
// 0x00000006 System.Void DevionGames.StatSystem.AddEffect::OnStart()
extern void AddEffect_OnStart_m6B8CC3190AB4E447F4E27E16795ED7C74FD90720 (void);
// 0x00000007 DevionGames.ActionStatus DevionGames.StatSystem.AddEffect::OnUpdate()
extern void AddEffect_OnUpdate_mEFE31A1925D11327854EC59EF613F638A270534E (void);
// 0x00000008 System.Void DevionGames.StatSystem.AddEffect::.ctor()
extern void AddEffect__ctor_m13812D8BAB644AB5A4E05F3BEC977052CB800C62 (void);
// 0x00000009 System.Void DevionGames.StatSystem.AddModifier::OnStart()
extern void AddModifier_OnStart_m1BA23D4A31530A5070B6F8A950B789253D61920E (void);
// 0x0000000A DevionGames.ActionStatus DevionGames.StatSystem.AddModifier::OnUpdate()
extern void AddModifier_OnUpdate_m46BD504372FFC7F236B5BA2578990677C1CCFEC8 (void);
// 0x0000000B System.Void DevionGames.StatSystem.AddModifier::.ctor()
extern void AddModifier__ctor_m688A95AA5E48CFD6E760D902542BA5676ACD32BE (void);
// 0x0000000C System.Void DevionGames.StatSystem.AddValue::OnStart()
extern void AddValue_OnStart_m531DF2588CECCAA9690E40D150400F34184412BB (void);
// 0x0000000D DevionGames.ActionStatus DevionGames.StatSystem.AddValue::OnUpdate()
extern void AddValue_OnUpdate_m3FD1E446C880A7EFDF4EF9F39D4971A0BCE03015 (void);
// 0x0000000E System.Void DevionGames.StatSystem.AddValue::.ctor()
extern void AddValue__ctor_m39A733023DFBCC9719EE70F350F24F91EB63FA1E (void);
// 0x0000000F System.Void DevionGames.StatSystem.ApplyDamage::OnStart()
extern void ApplyDamage_OnStart_m1E84992C7AB10DC3BDB39ADB29393CF1F3A1DAA6 (void);
// 0x00000010 DevionGames.ActionStatus DevionGames.StatSystem.ApplyDamage::OnUpdate()
extern void ApplyDamage_OnUpdate_m335B41314F21178742920FB7E74BCB65A96AE176 (void);
// 0x00000011 System.Void DevionGames.StatSystem.ApplyDamage::.ctor()
extern void ApplyDamage__ctor_m6A0B5B4A6A8523F3695D8A5CE877A4BE3694F413 (void);
// 0x00000012 System.Void DevionGames.StatSystem.Compare::OnStart()
extern void Compare_OnStart_m6125A8EC4752751A87ECE73BE3C9EF9BCF802B8A (void);
// 0x00000013 DevionGames.ActionStatus DevionGames.StatSystem.Compare::OnUpdate()
extern void Compare_OnUpdate_m0987CC000DE2A8DFFC9506703503FDD8D7F84116 (void);
// 0x00000014 System.Void DevionGames.StatSystem.Compare::.ctor()
extern void Compare__ctor_mC528D082F978B4D1E647FC6AD348036898E74068 (void);
// 0x00000015 System.Void DevionGames.StatSystem.Refresh::OnStart()
extern void Refresh_OnStart_mF7D41B9D2A98C2329F81C834E18B58528DBF07F0 (void);
// 0x00000016 DevionGames.ActionStatus DevionGames.StatSystem.Refresh::OnUpdate()
extern void Refresh_OnUpdate_m4F8A830E353DF2BA8FA8CDF4243AEC3F8A9D2CE5 (void);
// 0x00000017 System.Void DevionGames.StatSystem.Refresh::.ctor()
extern void Refresh__ctor_m80D9148BC5F0F9E7C8D9E445794747016472ECFA (void);
// 0x00000018 System.Void DevionGames.StatSystem.RemoveEffect::OnStart()
extern void RemoveEffect_OnStart_m65FE7F426F7FEE59292FE99BD8674A10B51D8F54 (void);
// 0x00000019 DevionGames.ActionStatus DevionGames.StatSystem.RemoveEffect::OnUpdate()
extern void RemoveEffect_OnUpdate_m37B4A26FE7C6DE1A5BEEBDFD238DAFAF339C8AFD (void);
// 0x0000001A System.Void DevionGames.StatSystem.RemoveEffect::.ctor()
extern void RemoveEffect__ctor_m8CBB9AAE9F3955FBBCD1725FD405A4ED35EAA80B (void);
// 0x0000001B System.Void DevionGames.StatSystem.RemoveModifier::OnStart()
extern void RemoveModifier_OnStart_m43E27EB614D49E884DFBAE022894C8769211E968 (void);
// 0x0000001C DevionGames.ActionStatus DevionGames.StatSystem.RemoveModifier::OnUpdate()
extern void RemoveModifier_OnUpdate_m928B506F635191C44BE9BC58E790816C8E5F3481 (void);
// 0x0000001D System.Void DevionGames.StatSystem.RemoveModifier::.ctor()
extern void RemoveModifier__ctor_m04D0AB9CA3049F48E235F4D1D4281BB8BE03A8BE (void);
// 0x0000001E System.Void DevionGames.StatSystem.SubtractValue::OnStart()
extern void SubtractValue_OnStart_m1AFE9DE102C992F87AD070ACA692CB41A87B90C3 (void);
// 0x0000001F DevionGames.ActionStatus DevionGames.StatSystem.SubtractValue::OnUpdate()
extern void SubtractValue_OnUpdate_m0724980C02C1E445B4E3F7DCE4C2274F75531EA2 (void);
// 0x00000020 System.Void DevionGames.StatSystem.SubtractValue::.ctor()
extern void SubtractValue__ctor_m8868C677E9D2CF51A2009CB2D006C5C26DE8FB02 (void);
// 0x00000021 System.Single DevionGames.StatSystem.Attribute::get_CurrentValue()
extern void Attribute_get_CurrentValue_m42B73D7DE53ECA2E6639E5A0C49626BF2FA11001 (void);
// 0x00000022 System.Void DevionGames.StatSystem.Attribute::set_CurrentValue(System.Single)
extern void Attribute_set_CurrentValue_m5C778D3A72708AA8510196435A3F4D94F799840C (void);
// 0x00000023 System.Void DevionGames.StatSystem.Attribute::Initialize(DevionGames.StatSystem.StatsHandler,DevionGames.StatSystem.StatOverride)
extern void Attribute_Initialize_m4CA609984558D0C1C37238FCC67882D31E3C929F (void);
// 0x00000024 System.Void DevionGames.StatSystem.Attribute::ApplyStartValues()
extern void Attribute_ApplyStartValues_m86FEC3377C027AC261AB8451A1A61D2DDD42CE30 (void);
// 0x00000025 System.String DevionGames.StatSystem.Attribute::ToString()
extern void Attribute_ToString_mECB5989B233575BC445369CEC91D859612CA83DD (void);
// 0x00000026 System.Void DevionGames.StatSystem.Attribute::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Attribute_GetObjectData_m9A7B298E1871E1560016428A6A4939107361C88D (void);
// 0x00000027 System.Void DevionGames.StatSystem.Attribute::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Attribute_SetObjectData_m4FA09F11F1C425EDECE84A167C7C2E70E4C50C6C (void);
// 0x00000028 System.Void DevionGames.StatSystem.Attribute::.ctor()
extern void Attribute__ctor_m0BC2BF9E7329687AD682E65D0072920049DF7FA7 (void);
// 0x00000029 System.Void DevionGames.StatSystem.Attribute::<Initialize>b__6_0()
extern void Attribute_U3CInitializeU3Eb__6_0_m0BBCA00F943A9FBA8AE42E8FD86E20B3CBBC849C (void);
// 0x0000002A System.Void DevionGames.StatSystem.DamageData::.ctor()
extern void DamageData__ctor_m3169037C4EEF0702CA76278A67C9701B91A22306 (void);
// 0x0000002B System.Void DevionGames.StatSystem.Level::Initialize(DevionGames.StatSystem.StatsHandler,DevionGames.StatSystem.StatOverride)
extern void Level_Initialize_m1E20936967AA250223871CBAC8D57BA06320F8B2 (void);
// 0x0000002C System.Void DevionGames.StatSystem.Level::.ctor()
extern void Level__ctor_m80419ADF7BE4FC2A0D28276173CB302304D8B3D3 (void);
// 0x0000002D System.Void DevionGames.StatSystem.Level::<Initialize>b__1_0()
extern void Level_U3CInitializeU3Eb__1_0_mE2793F4F14D9C59BD7D7F25A213A75670D7FF277 (void);
// 0x0000002E DevionGames.StatSystem.StatsHandler DevionGames.StatSystem.SelectableUIStat::GetStatsHandler()
extern void SelectableUIStat_GetStatsHandler_m807814FD8FDEFC29A21DB750B6BF6F20988CB98A (void);
// 0x0000002F System.Void DevionGames.StatSystem.SelectableUIStat::.ctor()
extern void SelectableUIStat__ctor_m1FF357B18ED9916E0A6B8FD26001F7A1E8147355 (void);
// 0x00000030 System.Void DevionGames.StatSystem.NotificationExtension::Show(DevionGames.UIWidgets.NotificationOptions,UnityEngine.Events.UnityAction`1<System.Int32>,System.String[])
extern void NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683 (void);
// 0x00000031 System.Void DevionGames.StatSystem.NotificationExtension::Show(DevionGames.UIWidgets.NotificationOptions,System.String[])
extern void NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C (void);
// 0x00000032 System.String DevionGames.StatSystem.Stat::get_Name()
extern void Stat_get_Name_m4C241784213E6D132541F96068877DE82CB7397C (void);
// 0x00000033 System.Void DevionGames.StatSystem.Stat::set_Name(System.String)
extern void Stat_set_Name_m18168D7EED0FF7DE38C3A987AEB28147ACA1DCE8 (void);
// 0x00000034 System.Single DevionGames.StatSystem.Stat::get_Value()
extern void Stat_get_Value_m015D1D798EBAB3AC2BA1200410012083D4ACC9A8 (void);
// 0x00000035 System.Void DevionGames.StatSystem.Stat::Initialize(DevionGames.StatSystem.StatsHandler,DevionGames.StatSystem.StatOverride)
extern void Stat_Initialize_m1A0D21E8E3832355893D258A596480CB92C88303 (void);
// 0x00000036 System.Void DevionGames.StatSystem.Stat::ApplyStartValues()
extern void Stat_ApplyStartValues_mEF564948E9FA75030AB6919E4F72C1896C95690E (void);
// 0x00000037 System.Void DevionGames.StatSystem.Stat::Add(System.Single)
extern void Stat_Add_m751AF43A7D6FB9BB513A1FC108F01FABE37F7D9B (void);
// 0x00000038 System.Void DevionGames.StatSystem.Stat::Subtract(System.Single)
extern void Stat_Subtract_mA32352722484F7AECCE94DD0320E12C2256C3C2A (void);
// 0x00000039 System.Void DevionGames.StatSystem.Stat::CalculateValue()
extern void Stat_CalculateValue_mE96117B79490AC28F87DF6B0C457CB50CF6372B9 (void);
// 0x0000003A System.Void DevionGames.StatSystem.Stat::CalculateValue(System.Boolean)
extern void Stat_CalculateValue_m3687707E970C1216A5D21BCEBF36D559D279F509 (void);
// 0x0000003B System.Void DevionGames.StatSystem.Stat::AddModifier(DevionGames.StatSystem.StatModifier)
extern void Stat_AddModifier_m9B30CC3CA452C7A6D71FD7941E10E0B91F58E6C8 (void);
// 0x0000003C System.Boolean DevionGames.StatSystem.Stat::RemoveModifier(DevionGames.StatSystem.StatModifier)
extern void Stat_RemoveModifier_m60382B7E3765B0D60C2B2B8B4DC0E76F138D2695 (void);
// 0x0000003D System.Boolean DevionGames.StatSystem.Stat::RemoveModifiersFromSource(System.Object)
extern void Stat_RemoveModifiersFromSource_m42CD288C49933E0908E0ED1AC2736608E1FD5574 (void);
// 0x0000003E DevionGames.Graphs.Graph DevionGames.StatSystem.Stat::GetGraph()
extern void Stat_GetGraph_m870803E699BEB0BC1383ACD113FBB09D0D229481 (void);
// 0x0000003F System.String DevionGames.StatSystem.Stat::ToString()
extern void Stat_ToString_m43EF7ED8275BD584262A88079E644B56AFB2120B (void);
// 0x00000040 System.Void DevionGames.StatSystem.Stat::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Stat_GetObjectData_m7DC6BA8EBAB5DFEADDE4E9A613BA12EC68CEE229 (void);
// 0x00000041 System.Void DevionGames.StatSystem.Stat::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Stat_SetObjectData_m66939FD338FF466FAE464F6AD015E66685CE8200 (void);
// 0x00000042 System.Void DevionGames.StatSystem.Stat::.ctor()
extern void Stat__ctor_m8E301F151937E02BEA880F7C8F070FEE052F1F0E (void);
// 0x00000043 System.Void DevionGames.StatSystem.Stat/<>c::.cctor()
extern void U3CU3Ec__cctor_m8DCB6E4FBED417F757227DC5486A2ABB9B74D01E (void);
// 0x00000044 System.Void DevionGames.StatSystem.Stat/<>c::.ctor()
extern void U3CU3Ec__ctor_m0F41254AC68CFE4D813B8E7BD26F012A38CA3939 (void);
// 0x00000045 System.Int32 DevionGames.StatSystem.Stat/<>c::<CalculateValue>b__20_0(DevionGames.StatSystem.StatModifier,DevionGames.StatSystem.StatModifier)
extern void U3CU3Ec_U3CCalculateValueU3Eb__20_0_m2F4BE30D02F990976E29E05F31D9F34BF4850AD1 (void);
// 0x00000046 System.Void DevionGames.StatSystem.Stat/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m8DCA86ECB196D853C749AA25B00A256F24B53A52 (void);
// 0x00000047 System.Boolean DevionGames.StatSystem.Stat/<>c__DisplayClass23_0::<RemoveModifiersFromSource>b__0(DevionGames.StatSystem.StatModifier)
extern void U3CU3Ec__DisplayClass23_0_U3CRemoveModifiersFromSourceU3Eb__0_mCB0E73DB2BF6B71E50C6714337741C30B0361DBA (void);
// 0x00000048 System.Void DevionGames.StatSystem.StatOverride::.ctor()
extern void StatOverride__ctor_m4A5CB2B0E4789219064EC92810C300E3C50EE287 (void);
// 0x00000049 System.Void DevionGames.StatSystem.StatCallback::Initialize(DevionGames.StatSystem.StatsHandler,DevionGames.StatSystem.Stat)
extern void StatCallback_Initialize_mCCC7D7E438437D8ED2A4CC56B56EB6A900E665C4 (void);
// 0x0000004A System.Void DevionGames.StatSystem.StatCallback::Update()
extern void StatCallback_Update_m57CEA6335895380DB0A17B13C1469FE9F35B0819 (void);
// 0x0000004B System.Void DevionGames.StatSystem.StatCallback::OnValueChange()
extern void StatCallback_OnValueChange_m6DEF9DE0CE63AF14AF3BE1CCD38CA553F18397C6 (void);
// 0x0000004C System.Void DevionGames.StatSystem.StatCallback::OnCurrentValueChange()
extern void StatCallback_OnCurrentValueChange_mEE36EBC43C007C45070FF5AFBC5B4BF0C147B273 (void);
// 0x0000004D System.Boolean DevionGames.StatSystem.StatCallback::TriggerCallback(System.Single)
extern void StatCallback_TriggerCallback_m89E2713F6C610121286A2D86980BDAB6C2824700 (void);
// 0x0000004E System.Void DevionGames.StatSystem.StatCallback::.ctor()
extern void StatCallback__ctor_mC86FC5634A0FA69341DE81BA49E3CB1A1E7BD65C (void);
// 0x0000004F System.Void DevionGames.StatSystem.StatDatabase::.ctor()
extern void StatDatabase__ctor_m95AF74F817AB25D116CC142F5ED877B36492B49A (void);
// 0x00000050 System.String DevionGames.StatSystem.StatEffect::get_Name()
extern void StatEffect_get_Name_mB2A4EB0E14E275A5A7821038CA69AF9ED62FA9EC (void);
// 0x00000051 System.Void DevionGames.StatSystem.StatEffect::set_Name(System.String)
extern void StatEffect_set_Name_m840B4F1A660DE5AD8B911C77D2833F01BAA28688 (void);
// 0x00000052 System.Void DevionGames.StatSystem.StatEffect::Initialize(DevionGames.StatSystem.StatsHandler)
extern void StatEffect_Initialize_m05F540A4605ACA62FE21377E24395135D19A9BE7 (void);
// 0x00000053 System.Void DevionGames.StatSystem.StatEffect::Execute()
extern void StatEffect_Execute_m1F026421ADC858162B52E9546A39C1537134E992 (void);
// 0x00000054 System.Void DevionGames.StatSystem.StatEffect::.ctor()
extern void StatEffect__ctor_m7D77ED005044504A55782E9DA62A5E82B294C591 (void);
// 0x00000055 System.Single DevionGames.StatSystem.StatModifier::get_Value()
extern void StatModifier_get_Value_mAF6522BA125F56A0CD9302F48345704CE7E01F49 (void);
// 0x00000056 DevionGames.StatSystem.StatModType DevionGames.StatSystem.StatModifier::get_Type()
extern void StatModifier_get_Type_m26EA7F60A7D6C3225F61362BF60C24D0870919F6 (void);
// 0x00000057 System.Void DevionGames.StatSystem.StatModifier::.ctor()
extern void StatModifier__ctor_mD015FB38D5E25CC6B7FA809ACC6918530E60961B (void);
// 0x00000058 System.Void DevionGames.StatSystem.StatModifier::.ctor(System.Single,DevionGames.StatSystem.StatModType,System.Object)
extern void StatModifier__ctor_m09281338EEB665C6A8A30FDB72011A9158972C48 (void);
// 0x00000059 System.Void DevionGames.StatSystem.StatPickerAttribute::.ctor()
extern void StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57 (void);
// 0x0000005A System.String DevionGames.StatSystem.StatsHandler::get_HandlerName()
extern void StatsHandler_get_HandlerName_m6F407A14AF1EAAB776979728F8500472092CECDA (void);
// 0x0000005B System.Void DevionGames.StatSystem.StatsHandler::Start()
extern void StatsHandler_Start_m1CF2F7C84A50514AFA3BA4E6CE7F9EF8BACBE968 (void);
// 0x0000005C System.Void DevionGames.StatSystem.StatsHandler::Update()
extern void StatsHandler_Update_mE7DAD7AA15E61E0C14556CEBE606DAB779111CAE (void);
// 0x0000005D System.Void DevionGames.StatSystem.StatsHandler::ApplyDamage(System.Object[])
extern void StatsHandler_ApplyDamage_m60F3194EE0D02BD32C18DD7DC603F188064646A1 (void);
// 0x0000005E System.Void DevionGames.StatSystem.StatsHandler::ApplyDamage(System.String,System.Single)
extern void StatsHandler_ApplyDamage_m12B3040193E68553743D61BC1BDB6504C98905E9 (void);
// 0x0000005F System.Void DevionGames.StatSystem.StatsHandler::TriggerAnimationEvent(UnityEngine.AnimationEvent)
extern void StatsHandler_TriggerAnimationEvent_m71818736D094973CCAA2F008A42820E229BBF741 (void);
// 0x00000060 System.Void DevionGames.StatSystem.StatsHandler::SendDamage(UnityEngine.Object)
extern void StatsHandler_SendDamage_m30120EC97050353A3C418E8B53F5181DA1FC84E5 (void);
// 0x00000061 System.Void DevionGames.StatSystem.StatsHandler::SendDamage(UnityEngine.GameObject,UnityEngine.Object)
extern void StatsHandler_SendDamage_mB3ADB38061373204E556815D0F1D10D56AB93DED (void);
// 0x00000062 System.Collections.IEnumerator DevionGames.StatSystem.StatsHandler::Knockback(UnityEngine.GameObject,DevionGames.StatSystem.DamageData)
extern void StatsHandler_Knockback_m35F1925FA3CBAA5C555AB778563D0904505EE453 (void);
// 0x00000063 System.Void DevionGames.StatSystem.StatsHandler::DisplayDamage(UnityEngine.GameObject,System.Single,UnityEngine.Color,UnityEngine.Vector3)
extern void StatsHandler_DisplayDamage_m6F079AD91613ECD54E93D000C14F95E935B73735 (void);
// 0x00000064 System.Void DevionGames.StatSystem.StatsHandler::PlaySound(UnityEngine.AudioClip,UnityEngine.Audio.AudioMixerGroup,System.Single)
extern void StatsHandler_PlaySound_m9DC10CD2BF73188FF79BEECBCF448949A5AA29D7 (void);
// 0x00000065 DevionGames.StatSystem.Stat DevionGames.StatSystem.StatsHandler::GetStat(DevionGames.StatSystem.Stat)
extern void StatsHandler_GetStat_m0B18DD554D8ACADE3FF82987E4F7D7A625C5BFAA (void);
// 0x00000066 DevionGames.StatSystem.Stat DevionGames.StatSystem.StatsHandler::GetStat(System.String)
extern void StatsHandler_GetStat_m0498F40E8B696A4D416AA8B4B7D1CDEC1CCBDE77 (void);
// 0x00000067 System.Boolean DevionGames.StatSystem.StatsHandler::CanApplyDamage(System.String,System.Single)
extern void StatsHandler_CanApplyDamage_m7D80384BF8C797C8E83CB16250F792C45F64D095 (void);
// 0x00000068 System.Void DevionGames.StatSystem.StatsHandler::AddEffect(DevionGames.StatSystem.StatEffect)
extern void StatsHandler_AddEffect_m930AFA3CC226FE276DD5893B09082CFA3663FA7A (void);
// 0x00000069 System.Void DevionGames.StatSystem.StatsHandler::RemoveEffect(DevionGames.StatSystem.StatEffect)
extern void StatsHandler_RemoveEffect_mA449D8FB7EC94C5DC07BE136D0CBA67496AB2BFB (void);
// 0x0000006A System.Void DevionGames.StatSystem.StatsHandler::AddModifier(System.Object[])
extern void StatsHandler_AddModifier_m00A2077DE623C21EEBE4AA640CAC57D2A5571F44 (void);
// 0x0000006B System.Void DevionGames.StatSystem.StatsHandler::AddModifier(System.String,System.Single,DevionGames.StatSystem.StatModType,System.Object)
extern void StatsHandler_AddModifier_mF38F92BCB670601B1E1EAB563E06AB83725B5AAF (void);
// 0x0000006C System.Boolean DevionGames.StatSystem.StatsHandler::RemoveModifiersFromSource(System.Object[])
extern void StatsHandler_RemoveModifiersFromSource_mF94CEDF454AFDC022AF3EBDCE7A9880CD6D05429 (void);
// 0x0000006D System.Boolean DevionGames.StatSystem.StatsHandler::RemoveModifiersFromSource(System.String,System.Object)
extern void StatsHandler_RemoveModifiersFromSource_mD94814A73999DD4392EA1E5C9D09D97A385ED82F (void);
// 0x0000006E System.Single DevionGames.StatSystem.StatsHandler::GetStatValue(System.String)
extern void StatsHandler_GetStatValue_mB1620C38656A562AB74954E26B4CF852A4AFAB0A (void);
// 0x0000006F System.Single DevionGames.StatSystem.StatsHandler::GetStatCurrentValue(System.String)
extern void StatsHandler_GetStatCurrentValue_mEA0290431449947BDAA2193639CB0A1092C7973E (void);
// 0x00000070 System.Void DevionGames.StatSystem.StatsHandler::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void StatsHandler_GetObjectData_mEE47E80B6E562A3205E738B3CCC84A419F82C77D (void);
// 0x00000071 System.Void DevionGames.StatSystem.StatsHandler::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void StatsHandler_SetObjectData_m5335D9B8AAD63622FBD4CA3C37BCF156565920A8 (void);
// 0x00000072 System.Void DevionGames.StatSystem.StatsHandler::.ctor()
extern void StatsHandler__ctor_mCAEBD59001B6F94F78391D9A36EDD1D46A00F00D (void);
// 0x00000073 System.Void DevionGames.StatSystem.StatsHandler/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m1D557EC5B8E9DAF9C80C47014CEDA18C2F052084 (void);
// 0x00000074 System.Boolean DevionGames.StatSystem.StatsHandler/<>c__DisplayClass15_0::<SendDamage>b__0(DevionGames.StatSystem.Stat)
extern void U3CU3Ec__DisplayClass15_0_U3CSendDamageU3Eb__0_m01FC4926FBD3CA21C1F4678759EE93EA97725545 (void);
// 0x00000075 System.Boolean DevionGames.StatSystem.StatsHandler/<>c__DisplayClass15_0::<SendDamage>b__1(DevionGames.StatSystem.Stat)
extern void U3CU3Ec__DisplayClass15_0_U3CSendDamageU3Eb__1_mF2C110DC5AA4F72786F24E3424DDC2116E8CC224 (void);
// 0x00000076 System.Void DevionGames.StatSystem.StatsHandler/<Knockback>d__16::.ctor(System.Int32)
extern void U3CKnockbackU3Ed__16__ctor_m329A5D30C415B9BC16139C55C8A93BCA9CADA4FF (void);
// 0x00000077 System.Void DevionGames.StatSystem.StatsHandler/<Knockback>d__16::System.IDisposable.Dispose()
extern void U3CKnockbackU3Ed__16_System_IDisposable_Dispose_m19B8A236E9BBC6DE44B2F7BFEFE39D7C4AE91D85 (void);
// 0x00000078 System.Boolean DevionGames.StatSystem.StatsHandler/<Knockback>d__16::MoveNext()
extern void U3CKnockbackU3Ed__16_MoveNext_m179310028064534A5D338056C2BB65E44AB53DC0 (void);
// 0x00000079 System.Object DevionGames.StatSystem.StatsHandler/<Knockback>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKnockbackU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC80C7DFD86C02829859923D00C6CE90D187846CC (void);
// 0x0000007A System.Void DevionGames.StatSystem.StatsHandler/<Knockback>d__16::System.Collections.IEnumerator.Reset()
extern void U3CKnockbackU3Ed__16_System_Collections_IEnumerator_Reset_mC85E4C24234040A47E11AEF9943F6263705A0D0C (void);
// 0x0000007B System.Object DevionGames.StatSystem.StatsHandler/<Knockback>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CKnockbackU3Ed__16_System_Collections_IEnumerator_get_Current_m81D4E93D7F6955995104EC73C52CA904DF63C84B (void);
// 0x0000007C System.Void DevionGames.StatSystem.StatsHandler/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m1A5570E6B0BA87810553AF787C0D3B9E92AA028A (void);
// 0x0000007D System.Boolean DevionGames.StatSystem.StatsHandler/<>c__DisplayClass20_0::<GetStat>b__0(DevionGames.StatSystem.Stat)
extern void U3CU3Ec__DisplayClass20_0_U3CGetStatU3Eb__0_mC1117E9DB34B7B163C0D7F0D07A11642ED76B776 (void);
// 0x0000007E System.Void DevionGames.StatSystem.StatsHandler/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mA27C64B824DB9A0C98CB360A703113FECB4039A0 (void);
// 0x0000007F System.Boolean DevionGames.StatSystem.StatsHandler/<>c__DisplayClass23_0::<RemoveEffect>b__0(DevionGames.StatSystem.StatEffect)
extern void U3CU3Ec__DisplayClass23_0_U3CRemoveEffectU3Eb__0_m1E69FF20C82354EA3607E034F286A2DDC337CBE0 (void);
// 0x00000080 DevionGames.StatSystem.StatsManager DevionGames.StatSystem.StatsManager::get_current()
extern void StatsManager_get_current_mE3F1AC8E59F5687301B0E9F6B9FEB553D2D04EC2 (void);
// 0x00000081 DevionGames.StatSystem.StatDatabase DevionGames.StatSystem.StatsManager::get_Database()
extern void StatsManager_get_Database_mFB3A18F55E06DFF6C2214F540250DA1E9DECC409 (void);
// 0x00000082 DevionGames.StatSystem.Configuration.Default DevionGames.StatSystem.StatsManager::get_DefaultSettings()
extern void StatsManager_get_DefaultSettings_m07A714DA40593E29B15EA2DF3A35ED0FE912BF1A (void);
// 0x00000083 DevionGames.StatSystem.Configuration.UI DevionGames.StatSystem.StatsManager::get_UI()
extern void StatsManager_get_UI_mD4F09C3F8CECE67482B6D4291C8FAB338A44CA52 (void);
// 0x00000084 DevionGames.StatSystem.Configuration.Notifications DevionGames.StatSystem.StatsManager::get_Notifications()
extern void StatsManager_get_Notifications_mACEA7366BCD43C8C9B2820CBAB17650DB13BBF54 (void);
// 0x00000085 DevionGames.StatSystem.Configuration.SavingLoading DevionGames.StatSystem.StatsManager::get_SavingLoading()
extern void StatsManager_get_SavingLoading_mD7CA9CA66DCDA2C509F8D452710BC3C8E497006A (void);
// 0x00000086 T DevionGames.StatSystem.StatsManager::GetSetting()
// 0x00000087 System.Void DevionGames.StatSystem.StatsManager::Awake()
extern void StatsManager_Awake_mC6C53EFF02EF9456C540847C495CCA51B1EDEA0C (void);
// 0x00000088 System.Void DevionGames.StatSystem.StatsManager::Start()
extern void StatsManager_Start_mE3D1184346DCAEACF82CBEDE5BCE75496CC6E83C (void);
// 0x00000089 System.Void DevionGames.StatSystem.StatsManager::Save()
extern void StatsManager_Save_m07888B7F59390D7FD6BD5DEF4EB7C745CDD3A41E (void);
// 0x0000008A System.Void DevionGames.StatSystem.StatsManager::Save(System.String)
extern void StatsManager_Save_mA8211AA10412C1512A6902648D28D6E8668A4956 (void);
// 0x0000008B System.Void DevionGames.StatSystem.StatsManager::Load()
extern void StatsManager_Load_mAFE3D50D31AB98A10B64268F3BA7A190FD7AEF3E (void);
// 0x0000008C System.Void DevionGames.StatSystem.StatsManager::Load(System.String)
extern void StatsManager_Load_m2A7522334DD64DF2ABBA044B9B7F00C3D57968BB (void);
// 0x0000008D System.Collections.IEnumerator DevionGames.StatSystem.StatsManager::DelayedLoading(System.Single)
extern void StatsManager_DelayedLoading_mA76B40F29EB936EE5BB4E369EAB5E2DAFFE78061 (void);
// 0x0000008E System.Collections.IEnumerator DevionGames.StatSystem.StatsManager::RepeatSaving(System.Single)
extern void StatsManager_RepeatSaving_m8939025EB8F979E7226934EFBCC988CCF2E0EB2A (void);
// 0x0000008F System.Void DevionGames.StatSystem.StatsManager::RegisterStatsHandler(DevionGames.StatSystem.StatsHandler)
extern void StatsManager_RegisterStatsHandler_mAEA8FF81263FAB0F43A5BD7529009B876F54F84D (void);
// 0x00000090 DevionGames.StatSystem.StatsHandler DevionGames.StatSystem.StatsManager::GetStatsHandler(System.String)
extern void StatsManager_GetStatsHandler_m3C6C5CFACC990A4600374EF36843B120FE335D93 (void);
// 0x00000091 System.Void DevionGames.StatSystem.StatsManager::.ctor()
extern void StatsManager__ctor_m1656BEBD871D6E533B69E2024C563958FAA31D53 (void);
// 0x00000092 System.Void DevionGames.StatSystem.StatsManager/<>c__18`1::.cctor()
// 0x00000093 System.Void DevionGames.StatSystem.StatsManager/<>c__18`1::.ctor()
// 0x00000094 System.Boolean DevionGames.StatSystem.StatsManager/<>c__18`1::<GetSetting>b__18_0(DevionGames.StatSystem.Configuration.Settings)
// 0x00000095 System.Void DevionGames.StatSystem.StatsManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mB1F38346916D41AE2362AA24E63859F00C3879FD (void);
// 0x00000096 System.Void DevionGames.StatSystem.StatsManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m01406ED584D6666E590098A6FF7EEC7881DD9B6A (void);
// 0x00000097 System.Boolean DevionGames.StatSystem.StatsManager/<>c::<Save>b__24_0(DevionGames.StatSystem.StatsHandler)
extern void U3CU3Ec_U3CSaveU3Eb__24_0_m4B112986E010025D673793C0BF2A3819A6ED0749 (void);
// 0x00000098 System.Boolean DevionGames.StatSystem.StatsManager/<>c::<Save>b__24_1(System.String)
extern void U3CU3Ec_U3CSaveU3Eb__24_1_m468416CDEF70A7C68A37706C92D95298FD3B9F45 (void);
// 0x00000099 System.Boolean DevionGames.StatSystem.StatsManager/<>c::<Load>b__26_0(DevionGames.StatSystem.StatsHandler)
extern void U3CU3Ec_U3CLoadU3Eb__26_0_m1BB6746A208F965EEDC192A05413B2AE547F8D22 (void);
// 0x0000009A System.Void DevionGames.StatSystem.StatsManager/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m0663D9D2451C51BAD83E1D54A5B0CB3A8FF532E8 (void);
// 0x0000009B System.Boolean DevionGames.StatSystem.StatsManager/<>c__DisplayClass26_0::<Load>b__1(DevionGames.StatSystem.StatsHandler)
extern void U3CU3Ec__DisplayClass26_0_U3CLoadU3Eb__1_mE74311A5C7EBF3350F30E851AC945AA45F65EF48 (void);
// 0x0000009C System.Void DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::.ctor(System.Int32)
extern void U3CDelayedLoadingU3Ed__27__ctor_m7AD70427BFA5E4E78ED6D1793DEA46C70C9C18D9 (void);
// 0x0000009D System.Void DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::System.IDisposable.Dispose()
extern void U3CDelayedLoadingU3Ed__27_System_IDisposable_Dispose_mCC1EF335F94627F7B4749AFC0033C996788FA255 (void);
// 0x0000009E System.Boolean DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::MoveNext()
extern void U3CDelayedLoadingU3Ed__27_MoveNext_mC5D6E39169D8611D822228219C5E11A649D82030 (void);
// 0x0000009F System.Object DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedLoadingU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m530C66EE210D0708E9926EB9697F65CF71A7B32A (void);
// 0x000000A0 System.Void DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::System.Collections.IEnumerator.Reset()
extern void U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_Reset_mA3A2DE31353E38C08644726C9B7CB196EB8C9BFA (void);
// 0x000000A1 System.Object DevionGames.StatSystem.StatsManager/<DelayedLoading>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_get_Current_m49D96F059D1AF1D6808DAEF1A67D39601B2AF2D0 (void);
// 0x000000A2 System.Void DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::.ctor(System.Int32)
extern void U3CRepeatSavingU3Ed__28__ctor_m43C748022A5E0D8F59EE964A5C05DB2407978720 (void);
// 0x000000A3 System.Void DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::System.IDisposable.Dispose()
extern void U3CRepeatSavingU3Ed__28_System_IDisposable_Dispose_m5CCB625264F2C834B7889E1C3B9DDFCF6A7C6DBD (void);
// 0x000000A4 System.Boolean DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::MoveNext()
extern void U3CRepeatSavingU3Ed__28_MoveNext_m0913C7AF114CE3A3E7532799928F2EC88EE3AC57 (void);
// 0x000000A5 System.Object DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatSavingU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEABC31FC27F603FB8556D196F5C635CE72DF6C31 (void);
// 0x000000A6 System.Void DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::System.Collections.IEnumerator.Reset()
extern void U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_Reset_mC778C8BA155D7599A36976C2DD04F6BF5167DDAF (void);
// 0x000000A7 System.Object DevionGames.StatSystem.StatsManager/<RepeatSaving>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_get_Current_m8F8CFDE00DF78F2A342D78E713428C0740AD7AF7 (void);
// 0x000000A8 System.Void DevionGames.StatSystem.StatsManager/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m411AA6D405B2A2436C3557E9AF072E66AC2D8FDD (void);
// 0x000000A9 System.Boolean DevionGames.StatSystem.StatsManager/<>c__DisplayClass30_0::<GetStatsHandler>b__0(DevionGames.StatSystem.StatsHandler)
extern void U3CU3Ec__DisplayClass30_0_U3CGetStatsHandlerU3Eb__0_mE5562F4DE0A6B299CB9513B34C8974A27FC869A0 (void);
// 0x000000AA System.Void DevionGames.StatSystem.UIStat::Start()
extern void UIStat_Start_mE909FCEA8B87CB4728127E423EF46619663D751B (void);
// 0x000000AB System.Void DevionGames.StatSystem.UIStat::Update()
extern void UIStat_Update_m230DF1B8271FA566E5C66CDDFF600C67334E1167 (void);
// 0x000000AC DevionGames.StatSystem.StatsHandler DevionGames.StatSystem.UIStat::GetStatsHandler()
extern void UIStat_GetStatsHandler_mDB72938E94ACEE60812C9C87FE08F959064BC8A4 (void);
// 0x000000AD System.Void DevionGames.StatSystem.UIStat::Repaint()
extern void UIStat_Repaint_m8DDF3DD41323A8B949BCBDFA46FB29814906C267 (void);
// 0x000000AE System.Void DevionGames.StatSystem.UIStat::OnCharacterLoaded(DevionGames.CallbackEventData)
extern void UIStat_OnCharacterLoaded_m7198D6B00AFBA82B2A6B7F16638DE5F24552820C (void);
// 0x000000AF System.Void DevionGames.StatSystem.UIStat::.ctor()
extern void UIStat__ctor_m7F997BE660EC875EB9012EB338E1EA3EBB0FC06B (void);
// 0x000000B0 System.Void DevionGames.StatSystem.UIStat::<Start>b__11_0()
extern void UIStat_U3CStartU3Eb__11_0_mC9B417C247447A9DE1F134711F317B0D51314EC3 (void);
// 0x000000B1 System.String DevionGames.StatSystem.Configuration.Default::get_Name()
extern void Default_get_Name_mB431F3DA48884B5523E236BBD3667C1FC163ECCD (void);
// 0x000000B2 System.Void DevionGames.StatSystem.Configuration.Default::.ctor()
extern void Default__ctor_m08150F075666B43B18BFAEFD477A87682A7D3E02 (void);
// 0x000000B3 System.String DevionGames.StatSystem.Configuration.Notifications::get_Name()
extern void Notifications_get_Name_m347F5CC2A21C397DC1F1BDA15D8340F0FA791255 (void);
// 0x000000B4 System.Void DevionGames.StatSystem.Configuration.Notifications::.ctor()
extern void Notifications__ctor_mE4C37CFD1BFBB91FC257EABEBC9FD73F29E22466 (void);
// 0x000000B5 System.String DevionGames.StatSystem.Configuration.SavingLoading::get_Name()
extern void SavingLoading_get_Name_m52EBFB0D147B6EDEC49AC65236EB682B95F0FD47 (void);
// 0x000000B6 System.Void DevionGames.StatSystem.Configuration.SavingLoading::.ctor()
extern void SavingLoading__ctor_mCFFFE674ABE5DE144773CA8A3B280ED593081346 (void);
// 0x000000B7 System.String DevionGames.StatSystem.Configuration.Settings::get_Name()
extern void Settings_get_Name_m380E1B7B0022DD49A14CFD26D525EE084B577041 (void);
// 0x000000B8 System.Void DevionGames.StatSystem.Configuration.Settings::set_Name(System.String)
extern void Settings_set_Name_m926E529150C93202D9A97FF2A44E6581A1DCBBBF (void);
// 0x000000B9 System.Void DevionGames.StatSystem.Configuration.Settings::.ctor()
extern void Settings__ctor_m7F0DE3E076929648A455224FF748B26CE4165C02 (void);
// 0x000000BA System.String DevionGames.StatSystem.Configuration.UI::get_Name()
extern void UI_get_Name_mFB9EE2CA62A979706151BB904ED7F7C7F808AE5B (void);
// 0x000000BB DevionGames.UIWidgets.Notification DevionGames.StatSystem.Configuration.UI::get_notification()
extern void UI_get_notification_m5C412F0A0715628C7AF317CC61AF4C3E9D8EB2D4 (void);
// 0x000000BC DevionGames.UIWidgets.DialogBox DevionGames.StatSystem.Configuration.UI::get_dialogBox()
extern void UI_get_dialogBox_mEA874F6637B67ABEBC5ECCC90D0DD951F0D86BF9 (void);
// 0x000000BD System.Void DevionGames.StatSystem.Configuration.UI::.ctor()
extern void UI__ctor_mC958BCCBD9A8E457D90216624F3E76170E94CD99 (void);
static Il2CppMethodPointer s_methodPointers[189] = 
{
	GetStatCurrentValue_OnRequestValue_m85270F6EFDC3636B24E0E7F9B3BFC72E7AA0C5DC,
	GetStatCurrentValue__ctor_mF8E281CB6F3D55E50FEB5BAF18DA6FD6BB328F41,
	GetStatValue_OnRequestValue_mCECCC851AC9A912E0F388DC08F4328238F0FAC13,
	GetStatValue__ctor_m92D9D9416D276C81C6E7B8F2024680F5671FCF2A,
	StatNode__ctor_m636576AA00A84331DA39A29E237AF6C4A747588C,
	AddEffect_OnStart_m6B8CC3190AB4E447F4E27E16795ED7C74FD90720,
	AddEffect_OnUpdate_mEFE31A1925D11327854EC59EF613F638A270534E,
	AddEffect__ctor_m13812D8BAB644AB5A4E05F3BEC977052CB800C62,
	AddModifier_OnStart_m1BA23D4A31530A5070B6F8A950B789253D61920E,
	AddModifier_OnUpdate_m46BD504372FFC7F236B5BA2578990677C1CCFEC8,
	AddModifier__ctor_m688A95AA5E48CFD6E760D902542BA5676ACD32BE,
	AddValue_OnStart_m531DF2588CECCAA9690E40D150400F34184412BB,
	AddValue_OnUpdate_m3FD1E446C880A7EFDF4EF9F39D4971A0BCE03015,
	AddValue__ctor_m39A733023DFBCC9719EE70F350F24F91EB63FA1E,
	ApplyDamage_OnStart_m1E84992C7AB10DC3BDB39ADB29393CF1F3A1DAA6,
	ApplyDamage_OnUpdate_m335B41314F21178742920FB7E74BCB65A96AE176,
	ApplyDamage__ctor_m6A0B5B4A6A8523F3695D8A5CE877A4BE3694F413,
	Compare_OnStart_m6125A8EC4752751A87ECE73BE3C9EF9BCF802B8A,
	Compare_OnUpdate_m0987CC000DE2A8DFFC9506703503FDD8D7F84116,
	Compare__ctor_mC528D082F978B4D1E647FC6AD348036898E74068,
	Refresh_OnStart_mF7D41B9D2A98C2329F81C834E18B58528DBF07F0,
	Refresh_OnUpdate_m4F8A830E353DF2BA8FA8CDF4243AEC3F8A9D2CE5,
	Refresh__ctor_m80D9148BC5F0F9E7C8D9E445794747016472ECFA,
	RemoveEffect_OnStart_m65FE7F426F7FEE59292FE99BD8674A10B51D8F54,
	RemoveEffect_OnUpdate_m37B4A26FE7C6DE1A5BEEBDFD238DAFAF339C8AFD,
	RemoveEffect__ctor_m8CBB9AAE9F3955FBBCD1725FD405A4ED35EAA80B,
	RemoveModifier_OnStart_m43E27EB614D49E884DFBAE022894C8769211E968,
	RemoveModifier_OnUpdate_m928B506F635191C44BE9BC58E790816C8E5F3481,
	RemoveModifier__ctor_m04D0AB9CA3049F48E235F4D1D4281BB8BE03A8BE,
	SubtractValue_OnStart_m1AFE9DE102C992F87AD070ACA692CB41A87B90C3,
	SubtractValue_OnUpdate_m0724980C02C1E445B4E3F7DCE4C2274F75531EA2,
	SubtractValue__ctor_m8868C677E9D2CF51A2009CB2D006C5C26DE8FB02,
	Attribute_get_CurrentValue_m42B73D7DE53ECA2E6639E5A0C49626BF2FA11001,
	Attribute_set_CurrentValue_m5C778D3A72708AA8510196435A3F4D94F799840C,
	Attribute_Initialize_m4CA609984558D0C1C37238FCC67882D31E3C929F,
	Attribute_ApplyStartValues_m86FEC3377C027AC261AB8451A1A61D2DDD42CE30,
	Attribute_ToString_mECB5989B233575BC445369CEC91D859612CA83DD,
	Attribute_GetObjectData_m9A7B298E1871E1560016428A6A4939107361C88D,
	Attribute_SetObjectData_m4FA09F11F1C425EDECE84A167C7C2E70E4C50C6C,
	Attribute__ctor_m0BC2BF9E7329687AD682E65D0072920049DF7FA7,
	Attribute_U3CInitializeU3Eb__6_0_m0BBCA00F943A9FBA8AE42E8FD86E20B3CBBC849C,
	DamageData__ctor_m3169037C4EEF0702CA76278A67C9701B91A22306,
	Level_Initialize_m1E20936967AA250223871CBAC8D57BA06320F8B2,
	Level__ctor_m80419ADF7BE4FC2A0D28276173CB302304D8B3D3,
	Level_U3CInitializeU3Eb__1_0_mE2793F4F14D9C59BD7D7F25A213A75670D7FF277,
	SelectableUIStat_GetStatsHandler_m807814FD8FDEFC29A21DB750B6BF6F20988CB98A,
	SelectableUIStat__ctor_m1FF357B18ED9916E0A6B8FD26001F7A1E8147355,
	NotificationExtension_Show_m00491BBB3752194CAE2BAE07C329399BCB33A683,
	NotificationExtension_Show_m140B806C07F81DDA7C8122547C894F798C5D210C,
	Stat_get_Name_m4C241784213E6D132541F96068877DE82CB7397C,
	Stat_set_Name_m18168D7EED0FF7DE38C3A987AEB28147ACA1DCE8,
	Stat_get_Value_m015D1D798EBAB3AC2BA1200410012083D4ACC9A8,
	Stat_Initialize_m1A0D21E8E3832355893D258A596480CB92C88303,
	Stat_ApplyStartValues_mEF564948E9FA75030AB6919E4F72C1896C95690E,
	Stat_Add_m751AF43A7D6FB9BB513A1FC108F01FABE37F7D9B,
	Stat_Subtract_mA32352722484F7AECCE94DD0320E12C2256C3C2A,
	Stat_CalculateValue_mE96117B79490AC28F87DF6B0C457CB50CF6372B9,
	Stat_CalculateValue_m3687707E970C1216A5D21BCEBF36D559D279F509,
	Stat_AddModifier_m9B30CC3CA452C7A6D71FD7941E10E0B91F58E6C8,
	Stat_RemoveModifier_m60382B7E3765B0D60C2B2B8B4DC0E76F138D2695,
	Stat_RemoveModifiersFromSource_m42CD288C49933E0908E0ED1AC2736608E1FD5574,
	Stat_GetGraph_m870803E699BEB0BC1383ACD113FBB09D0D229481,
	Stat_ToString_m43EF7ED8275BD584262A88079E644B56AFB2120B,
	Stat_GetObjectData_m7DC6BA8EBAB5DFEADDE4E9A613BA12EC68CEE229,
	Stat_SetObjectData_m66939FD338FF466FAE464F6AD015E66685CE8200,
	Stat__ctor_m8E301F151937E02BEA880F7C8F070FEE052F1F0E,
	U3CU3Ec__cctor_m8DCB6E4FBED417F757227DC5486A2ABB9B74D01E,
	U3CU3Ec__ctor_m0F41254AC68CFE4D813B8E7BD26F012A38CA3939,
	U3CU3Ec_U3CCalculateValueU3Eb__20_0_m2F4BE30D02F990976E29E05F31D9F34BF4850AD1,
	U3CU3Ec__DisplayClass23_0__ctor_m8DCA86ECB196D853C749AA25B00A256F24B53A52,
	U3CU3Ec__DisplayClass23_0_U3CRemoveModifiersFromSourceU3Eb__0_mCB0E73DB2BF6B71E50C6714337741C30B0361DBA,
	StatOverride__ctor_m4A5CB2B0E4789219064EC92810C300E3C50EE287,
	StatCallback_Initialize_mCCC7D7E438437D8ED2A4CC56B56EB6A900E665C4,
	StatCallback_Update_m57CEA6335895380DB0A17B13C1469FE9F35B0819,
	StatCallback_OnValueChange_m6DEF9DE0CE63AF14AF3BE1CCD38CA553F18397C6,
	StatCallback_OnCurrentValueChange_mEE36EBC43C007C45070FF5AFBC5B4BF0C147B273,
	StatCallback_TriggerCallback_m89E2713F6C610121286A2D86980BDAB6C2824700,
	StatCallback__ctor_mC86FC5634A0FA69341DE81BA49E3CB1A1E7BD65C,
	StatDatabase__ctor_m95AF74F817AB25D116CC142F5ED877B36492B49A,
	StatEffect_get_Name_mB2A4EB0E14E275A5A7821038CA69AF9ED62FA9EC,
	StatEffect_set_Name_m840B4F1A660DE5AD8B911C77D2833F01BAA28688,
	StatEffect_Initialize_m05F540A4605ACA62FE21377E24395135D19A9BE7,
	StatEffect_Execute_m1F026421ADC858162B52E9546A39C1537134E992,
	StatEffect__ctor_m7D77ED005044504A55782E9DA62A5E82B294C591,
	StatModifier_get_Value_mAF6522BA125F56A0CD9302F48345704CE7E01F49,
	StatModifier_get_Type_m26EA7F60A7D6C3225F61362BF60C24D0870919F6,
	StatModifier__ctor_mD015FB38D5E25CC6B7FA809ACC6918530E60961B,
	StatModifier__ctor_m09281338EEB665C6A8A30FDB72011A9158972C48,
	StatPickerAttribute__ctor_mA5C603E4795406EB48BABA6AE25BA9254F2ADB57,
	StatsHandler_get_HandlerName_m6F407A14AF1EAAB776979728F8500472092CECDA,
	StatsHandler_Start_m1CF2F7C84A50514AFA3BA4E6CE7F9EF8BACBE968,
	StatsHandler_Update_mE7DAD7AA15E61E0C14556CEBE606DAB779111CAE,
	StatsHandler_ApplyDamage_m60F3194EE0D02BD32C18DD7DC603F188064646A1,
	StatsHandler_ApplyDamage_m12B3040193E68553743D61BC1BDB6504C98905E9,
	StatsHandler_TriggerAnimationEvent_m71818736D094973CCAA2F008A42820E229BBF741,
	StatsHandler_SendDamage_m30120EC97050353A3C418E8B53F5181DA1FC84E5,
	StatsHandler_SendDamage_mB3ADB38061373204E556815D0F1D10D56AB93DED,
	StatsHandler_Knockback_m35F1925FA3CBAA5C555AB778563D0904505EE453,
	StatsHandler_DisplayDamage_m6F079AD91613ECD54E93D000C14F95E935B73735,
	StatsHandler_PlaySound_m9DC10CD2BF73188FF79BEECBCF448949A5AA29D7,
	StatsHandler_GetStat_m0B18DD554D8ACADE3FF82987E4F7D7A625C5BFAA,
	StatsHandler_GetStat_m0498F40E8B696A4D416AA8B4B7D1CDEC1CCBDE77,
	StatsHandler_CanApplyDamage_m7D80384BF8C797C8E83CB16250F792C45F64D095,
	StatsHandler_AddEffect_m930AFA3CC226FE276DD5893B09082CFA3663FA7A,
	StatsHandler_RemoveEffect_mA449D8FB7EC94C5DC07BE136D0CBA67496AB2BFB,
	StatsHandler_AddModifier_m00A2077DE623C21EEBE4AA640CAC57D2A5571F44,
	StatsHandler_AddModifier_mF38F92BCB670601B1E1EAB563E06AB83725B5AAF,
	StatsHandler_RemoveModifiersFromSource_mF94CEDF454AFDC022AF3EBDCE7A9880CD6D05429,
	StatsHandler_RemoveModifiersFromSource_mD94814A73999DD4392EA1E5C9D09D97A385ED82F,
	StatsHandler_GetStatValue_mB1620C38656A562AB74954E26B4CF852A4AFAB0A,
	StatsHandler_GetStatCurrentValue_mEA0290431449947BDAA2193639CB0A1092C7973E,
	StatsHandler_GetObjectData_mEE47E80B6E562A3205E738B3CCC84A419F82C77D,
	StatsHandler_SetObjectData_m5335D9B8AAD63622FBD4CA3C37BCF156565920A8,
	StatsHandler__ctor_mCAEBD59001B6F94F78391D9A36EDD1D46A00F00D,
	U3CU3Ec__DisplayClass15_0__ctor_m1D557EC5B8E9DAF9C80C47014CEDA18C2F052084,
	U3CU3Ec__DisplayClass15_0_U3CSendDamageU3Eb__0_m01FC4926FBD3CA21C1F4678759EE93EA97725545,
	U3CU3Ec__DisplayClass15_0_U3CSendDamageU3Eb__1_mF2C110DC5AA4F72786F24E3424DDC2116E8CC224,
	U3CKnockbackU3Ed__16__ctor_m329A5D30C415B9BC16139C55C8A93BCA9CADA4FF,
	U3CKnockbackU3Ed__16_System_IDisposable_Dispose_m19B8A236E9BBC6DE44B2F7BFEFE39D7C4AE91D85,
	U3CKnockbackU3Ed__16_MoveNext_m179310028064534A5D338056C2BB65E44AB53DC0,
	U3CKnockbackU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC80C7DFD86C02829859923D00C6CE90D187846CC,
	U3CKnockbackU3Ed__16_System_Collections_IEnumerator_Reset_mC85E4C24234040A47E11AEF9943F6263705A0D0C,
	U3CKnockbackU3Ed__16_System_Collections_IEnumerator_get_Current_m81D4E93D7F6955995104EC73C52CA904DF63C84B,
	U3CU3Ec__DisplayClass20_0__ctor_m1A5570E6B0BA87810553AF787C0D3B9E92AA028A,
	U3CU3Ec__DisplayClass20_0_U3CGetStatU3Eb__0_mC1117E9DB34B7B163C0D7F0D07A11642ED76B776,
	U3CU3Ec__DisplayClass23_0__ctor_mA27C64B824DB9A0C98CB360A703113FECB4039A0,
	U3CU3Ec__DisplayClass23_0_U3CRemoveEffectU3Eb__0_m1E69FF20C82354EA3607E034F286A2DDC337CBE0,
	StatsManager_get_current_mE3F1AC8E59F5687301B0E9F6B9FEB553D2D04EC2,
	StatsManager_get_Database_mFB3A18F55E06DFF6C2214F540250DA1E9DECC409,
	StatsManager_get_DefaultSettings_m07A714DA40593E29B15EA2DF3A35ED0FE912BF1A,
	StatsManager_get_UI_mD4F09C3F8CECE67482B6D4291C8FAB338A44CA52,
	StatsManager_get_Notifications_mACEA7366BCD43C8C9B2820CBAB17650DB13BBF54,
	StatsManager_get_SavingLoading_mD7CA9CA66DCDA2C509F8D452710BC3C8E497006A,
	NULL,
	StatsManager_Awake_mC6C53EFF02EF9456C540847C495CCA51B1EDEA0C,
	StatsManager_Start_mE3D1184346DCAEACF82CBEDE5BCE75496CC6E83C,
	StatsManager_Save_m07888B7F59390D7FD6BD5DEF4EB7C745CDD3A41E,
	StatsManager_Save_mA8211AA10412C1512A6902648D28D6E8668A4956,
	StatsManager_Load_mAFE3D50D31AB98A10B64268F3BA7A190FD7AEF3E,
	StatsManager_Load_m2A7522334DD64DF2ABBA044B9B7F00C3D57968BB,
	StatsManager_DelayedLoading_mA76B40F29EB936EE5BB4E369EAB5E2DAFFE78061,
	StatsManager_RepeatSaving_m8939025EB8F979E7226934EFBCC988CCF2E0EB2A,
	StatsManager_RegisterStatsHandler_mAEA8FF81263FAB0F43A5BD7529009B876F54F84D,
	StatsManager_GetStatsHandler_m3C6C5CFACC990A4600374EF36843B120FE335D93,
	StatsManager__ctor_m1656BEBD871D6E533B69E2024C563958FAA31D53,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mB1F38346916D41AE2362AA24E63859F00C3879FD,
	U3CU3Ec__ctor_m01406ED584D6666E590098A6FF7EEC7881DD9B6A,
	U3CU3Ec_U3CSaveU3Eb__24_0_m4B112986E010025D673793C0BF2A3819A6ED0749,
	U3CU3Ec_U3CSaveU3Eb__24_1_m468416CDEF70A7C68A37706C92D95298FD3B9F45,
	U3CU3Ec_U3CLoadU3Eb__26_0_m1BB6746A208F965EEDC192A05413B2AE547F8D22,
	U3CU3Ec__DisplayClass26_0__ctor_m0663D9D2451C51BAD83E1D54A5B0CB3A8FF532E8,
	U3CU3Ec__DisplayClass26_0_U3CLoadU3Eb__1_mE74311A5C7EBF3350F30E851AC945AA45F65EF48,
	U3CDelayedLoadingU3Ed__27__ctor_m7AD70427BFA5E4E78ED6D1793DEA46C70C9C18D9,
	U3CDelayedLoadingU3Ed__27_System_IDisposable_Dispose_mCC1EF335F94627F7B4749AFC0033C996788FA255,
	U3CDelayedLoadingU3Ed__27_MoveNext_mC5D6E39169D8611D822228219C5E11A649D82030,
	U3CDelayedLoadingU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m530C66EE210D0708E9926EB9697F65CF71A7B32A,
	U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_Reset_mA3A2DE31353E38C08644726C9B7CB196EB8C9BFA,
	U3CDelayedLoadingU3Ed__27_System_Collections_IEnumerator_get_Current_m49D96F059D1AF1D6808DAEF1A67D39601B2AF2D0,
	U3CRepeatSavingU3Ed__28__ctor_m43C748022A5E0D8F59EE964A5C05DB2407978720,
	U3CRepeatSavingU3Ed__28_System_IDisposable_Dispose_m5CCB625264F2C834B7889E1C3B9DDFCF6A7C6DBD,
	U3CRepeatSavingU3Ed__28_MoveNext_m0913C7AF114CE3A3E7532799928F2EC88EE3AC57,
	U3CRepeatSavingU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEABC31FC27F603FB8556D196F5C635CE72DF6C31,
	U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_Reset_mC778C8BA155D7599A36976C2DD04F6BF5167DDAF,
	U3CRepeatSavingU3Ed__28_System_Collections_IEnumerator_get_Current_m8F8CFDE00DF78F2A342D78E713428C0740AD7AF7,
	U3CU3Ec__DisplayClass30_0__ctor_m411AA6D405B2A2436C3557E9AF072E66AC2D8FDD,
	U3CU3Ec__DisplayClass30_0_U3CGetStatsHandlerU3Eb__0_mE5562F4DE0A6B299CB9513B34C8974A27FC869A0,
	UIStat_Start_mE909FCEA8B87CB4728127E423EF46619663D751B,
	UIStat_Update_m230DF1B8271FA566E5C66CDDFF600C67334E1167,
	UIStat_GetStatsHandler_mDB72938E94ACEE60812C9C87FE08F959064BC8A4,
	UIStat_Repaint_m8DDF3DD41323A8B949BCBDFA46FB29814906C267,
	UIStat_OnCharacterLoaded_m7198D6B00AFBA82B2A6B7F16638DE5F24552820C,
	UIStat__ctor_m7F997BE660EC875EB9012EB338E1EA3EBB0FC06B,
	UIStat_U3CStartU3Eb__11_0_mC9B417C247447A9DE1F134711F317B0D51314EC3,
	Default_get_Name_mB431F3DA48884B5523E236BBD3667C1FC163ECCD,
	Default__ctor_m08150F075666B43B18BFAEFD477A87682A7D3E02,
	Notifications_get_Name_m347F5CC2A21C397DC1F1BDA15D8340F0FA791255,
	Notifications__ctor_mE4C37CFD1BFBB91FC257EABEBC9FD73F29E22466,
	SavingLoading_get_Name_m52EBFB0D147B6EDEC49AC65236EB682B95F0FD47,
	SavingLoading__ctor_mCFFFE674ABE5DE144773CA8A3B280ED593081346,
	Settings_get_Name_m380E1B7B0022DD49A14CFD26D525EE084B577041,
	Settings_set_Name_m926E529150C93202D9A97FF2A44E6581A1DCBBBF,
	Settings__ctor_m7F0DE3E076929648A455224FF748B26CE4165C02,
	UI_get_Name_mFB9EE2CA62A979706151BB904ED7F7C7F808AE5B,
	UI_get_notification_m5C412F0A0715628C7AF317CC61AF4C3E9D8EB2D4,
	UI_get_dialogBox_mEA874F6637B67ABEBC5ECCC90D0DD951F0D86BF9,
	UI__ctor_mC958BCCBD9A8E457D90216624F3E76170E94CD99,
};
static const int32_t s_InvokerIndices[189] = 
{
	1898,
	2975,
	1898,
	2975,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2948,
	2484,
	1444,
	2975,
	2912,
	2448,
	2448,
	2975,
	2975,
	2975,
	1444,
	2975,
	2975,
	2912,
	2975,
	3926,
	4276,
	2912,
	2448,
	2948,
	1444,
	2975,
	2484,
	2484,
	2975,
	2477,
	2448,
	2133,
	2133,
	2912,
	2912,
	2448,
	2448,
	2975,
	4649,
	2975,
	1030,
	2975,
	2133,
	2975,
	1444,
	2975,
	2975,
	2975,
	2171,
	2975,
	2975,
	2912,
	2448,
	2448,
	2975,
	2975,
	2948,
	2897,
	2975,
	975,
	2975,
	2912,
	2975,
	2975,
	2448,
	1450,
	2448,
	2448,
	1444,
	1088,
	642,
	934,
	1898,
	1898,
	1150,
	2448,
	2448,
	2448,
	643,
	2133,
	1147,
	2246,
	2246,
	2448,
	2448,
	2975,
	2975,
	2133,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2133,
	2975,
	2133,
	4622,
	4622,
	4622,
	4622,
	4622,
	4622,
	-1,
	2975,
	2975,
	4649,
	4566,
	4649,
	4566,
	1903,
	1903,
	4566,
	4481,
	2975,
	-1,
	-1,
	-1,
	4649,
	2975,
	2133,
	2133,
	2133,
	2975,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2133,
	2975,
	2975,
	2912,
	2975,
	2448,
	2975,
	2975,
	2912,
	2975,
	2912,
	2975,
	2912,
	2975,
	2912,
	2448,
	2975,
	2912,
	2912,
	2912,
	2975,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x02000025, { 3, 4 } },
	{ 0x06000086, { 0, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)2, 927 },
	{ (Il2CppRGCTXDataType)3, 71 },
	{ (Il2CppRGCTXDataType)2, 259 },
	{ (Il2CppRGCTXDataType)2, 928 },
	{ (Il2CppRGCTXDataType)3, 72 },
	{ (Il2CppRGCTXDataType)2, 928 },
	{ (Il2CppRGCTXDataType)1, 680 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_StatSystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_StatSystem_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_StatSystem_CodeGenModule = 
{
	"DevionGames.StatSystem.dll",
	189,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_DevionGames_StatSystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
