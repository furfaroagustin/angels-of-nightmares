﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AddScore::Start()
extern void AddScore_Start_m2CECC366B94A630ECF872F63132CFA39489B6974 (void);
// 0x00000002 System.Void AddScore::OnTriggerEnter(UnityEngine.Collider)
extern void AddScore_OnTriggerEnter_m9683F927AD134B84211856ED1D43EF9529E2A9B8 (void);
// 0x00000003 System.Void AddScore::Fin()
extern void AddScore_Fin_m6321518FF170B3611652A4DAB77BB3115AEA21C8 (void);
// 0x00000004 System.Void AddScore::.ctor()
extern void AddScore__ctor_m39BA7F92305F30511F00F407945F7A2B02365973 (void);
// 0x00000005 System.Void Die::Start()
extern void Die_Start_m67A3A33B7036827D8D2CB99333FEC637BF2A4ADF (void);
// 0x00000006 System.Void Die::Update()
extern void Die_Update_m7F694C97563A3CC38968E9FE0ACB6E3B79C3C105 (void);
// 0x00000007 System.Void Die::OnCollisionEnter(UnityEngine.Collision)
extern void Die_OnCollisionEnter_mB87777D11312C05E24C70EFA61C852C72C547B91 (void);
// 0x00000008 System.Void Die::RestaBarra(System.Int32)
extern void Die_RestaBarra_m16793F3E0A3262D74B04AABEBBAA391F2B3BF5A7 (void);
// 0x00000009 System.Void Die::SumarBarra(System.Int32)
extern void Die_SumarBarra_m064E2C53EA448C3EE694E2FAAA05202BAE8174B4 (void);
// 0x0000000A System.Void Die::Fin()
extern void Die_Fin_mA3F4E61DCEC80E61C7C4562F20D128B4DFE9D697 (void);
// 0x0000000B System.Void Die::.ctor()
extern void Die__ctor_m69277E6E4F98D03F466D17392670F8ABB40C1F5D (void);
// 0x0000000C System.Void HealtBar::SetMaxHealth(System.Int32)
extern void HealtBar_SetMaxHealth_m8CFB045BD2FD54FA0A1063D46DA454BE5E61CE0B (void);
// 0x0000000D System.Void HealtBar::setHealth(System.Int32)
extern void HealtBar_setHealth_mEAE63824DEB2EAAFFC1481B73A09EF3116E1DDA6 (void);
// 0x0000000E System.Void HealtBar::.ctor()
extern void HealtBar__ctor_m70D11B02753C2852C3F27A19FF3E0F2879B69C0D (void);
// 0x0000000F System.Void Menu::EscenaJuego()
extern void Menu_EscenaJuego_m05F7043C4A9ADA7D61DDC0C5D20490C70457406A (void);
// 0x00000010 System.Void Menu::CargarNivel(System.String)
extern void Menu_CargarNivel_mF353A0D0775C71D0CB0AEDBAF1AA3A2A99A688FA (void);
// 0x00000011 System.Void Menu::.ctor()
extern void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (void);
// 0x00000012 System.Void Pause::Start()
extern void Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC (void);
// 0x00000013 System.Void Pause::Settings()
extern void Pause_Settings_mD30A751D51933D495A40658095A9C8FB969C515F (void);
// 0x00000014 System.Void Pause::Stop()
extern void Pause_Stop_m1E584C228C2BCF9C735FA97AB8BA059C820AA6A4 (void);
// 0x00000015 System.Void Pause::Play()
extern void Pause_Play_mEF4CF8B350D2A55CF9AEBAFB249F06A5BD6B4F1A (void);
// 0x00000016 System.Void Pause::Restart()
extern void Pause_Restart_m534DC75B82B5CEF183E318BA5E5D0FD6CCB3AADB (void);
// 0x00000017 System.Void Pause::Quit()
extern void Pause_Quit_mCDE8B0EE9E6C1F2CD7ED66CAC4C23BD195E982F3 (void);
// 0x00000018 System.Void Pause::Menu()
extern void Pause_Menu_m96B5575F6631358E7FD55E0503A1CF497FAAF509 (void);
// 0x00000019 System.Void Pause::.ctor()
extern void Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928 (void);
// 0x0000001A System.Void Quit::Exit()
extern void Quit_Exit_m97F7318E67A0708CC8F4187B4BC441BE02F75DA1 (void);
// 0x0000001B System.Void Quit::.ctor()
extern void Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4 (void);
// 0x0000001C System.Void SliderVolume::Start()
extern void SliderVolume_Start_m03E96ECF4FCBE895C1EB972F9B520D1B844F9548 (void);
// 0x0000001D System.Void SliderVolume::ChangeSlider(System.Single)
extern void SliderVolume_ChangeSlider_m33FA89BE2C05F9A62FE02E7C15613B23C4B1F22B (void);
// 0x0000001E System.Void SliderVolume::ReviMute()
extern void SliderVolume_ReviMute_m4A60A607320E30B4A8AD3BE61BC82B748C13E06E (void);
// 0x0000001F System.Void SliderVolume::.ctor()
extern void SliderVolume__ctor_m56BDF89A5DB8221EE3D40DA3822E366991309AED (void);
// 0x00000020 System.Void ControlRed::Awake()
extern void ControlRed_Awake_m19D85637E3F1BB6726E8345CD5D813FE418E7830 (void);
// 0x00000021 System.Void ControlRed::Start()
extern void ControlRed_Start_m23C11C9A0031FA48AABFB0B09744BE50C721BB15 (void);
// 0x00000022 System.Void ControlRed::Update()
extern void ControlRed_Update_mC888523955814CE25D3752ED09FFA976285E6BD8 (void);
// 0x00000023 System.Void ControlRed::.ctor()
extern void ControlRed__ctor_mEF7307A0F005B55555272843F536833645A91E73 (void);
// 0x00000024 System.Void Da?o::Start()
extern void DaUF1o_Start_mE704F1D1E6DE47B3EFF52E50DC89CAAB4DC7A3E9 (void);
// 0x00000025 System.Void Da?o::OnCollisionEnter(UnityEngine.Collision)
extern void DaUF1o_OnCollisionEnter_m1B393B1ADC820834A8DCFF499E69F9AB0C65DD1C (void);
// 0x00000026 System.Void Da?o::.ctor()
extern void DaUF1o__ctor_m1B109991F875B794458D2645D286CB712A9BEFD5 (void);
// 0x00000027 System.Void GestorPhoton::Start()
extern void GestorPhoton_Start_m04782C9CB53B13F023D931BB415241C4099C161C (void);
// 0x00000028 System.Void GestorPhoton::OnConnectedToMaster()
extern void GestorPhoton_OnConnectedToMaster_m7B18A72517952A9A1F4BDFE6E98F53123B30E8A6 (void);
// 0x00000029 System.Void GestorPhoton::OnJoinedLobby()
extern void GestorPhoton_OnJoinedLobby_m9A21314F6EACB4074F0139579A8FAB8FFEB9B64F (void);
// 0x0000002A System.Void GestorPhoton::OnJoinedRoom()
extern void GestorPhoton_OnJoinedRoom_m2B3E32763051448B48BA389C43E61DE1323BF332 (void);
// 0x0000002B System.Void GestorPhoton::.ctor()
extern void GestorPhoton__ctor_m49E9993096FD830BF8B0EF2ED13870CAF8283B05 (void);
// 0x0000002C System.Void RedPlayer::Start()
extern void RedPlayer_Start_m0C40256C16CA66CF6EF347B0CE4A373FAD719016 (void);
// 0x0000002D System.Void RedPlayer::.ctor()
extern void RedPlayer__ctor_m262EABC76B0F6E67F1977BBE392D15DA9320BF76 (void);
// 0x0000002E System.Void ScaleChange::Start()
extern void ScaleChange_Start_m29AA7831AD05AB8E8E488E246B639855EE2A9DAD (void);
// 0x0000002F System.Void ScaleChange::Update()
extern void ScaleChange_Update_m4CE746D380D1EEC45ECE5259F632E3258198EB47 (void);
// 0x00000030 System.Void ScaleChange::.ctor()
extern void ScaleChange__ctor_m5281007D39C05C8DB35F08E47D2A58D61EC407DC (void);
// 0x00000031 System.Void Vida::Start()
extern void Vida_Start_mC68F7B8FDD945434F957D43FAE8BFCE5604ED5B5 (void);
// 0x00000032 System.Void Vida::Update()
extern void Vida_Update_m005476FD340EA5FBC4FCBE634840107FEE383B6D (void);
// 0x00000033 System.Void Vida::RevisarVida()
extern void Vida_RevisarVida_m78416A96AB8D758B441FB5F421C052FE92C53888 (void);
// 0x00000034 System.Void Vida::.ctor()
extern void Vida__ctor_m510046C9FAF7857DB658C85226601374DE81FF92 (void);
// 0x00000035 System.Void SM_AnimSpeedRandomizer::Start()
extern void SM_AnimSpeedRandomizer_Start_m34CB399D7A1D350BDC933C9FD578993787EB288A (void);
// 0x00000036 System.Void SM_AnimSpeedRandomizer::.ctor()
extern void SM_AnimSpeedRandomizer__ctor_m1FEB51070F32C87E818C4ABB62822204416BBF5C (void);
// 0x00000037 System.Void SM_DestroyTimed::Start()
extern void SM_DestroyTimed_Start_m7955412CE525AE89CEE5CB322E2F95E0561A6F7B (void);
// 0x00000038 System.Void SM_DestroyTimed::.ctor()
extern void SM_DestroyTimed__ctor_mE2ECD56496ECE4C65A3C23FED1FB90D62246C759 (void);
// 0x00000039 System.Void SM_PrefabGenerator::Start()
extern void SM_PrefabGenerator_Start_m1C4FDCA60EEDDE08F2B59D79301A7E7F72009645 (void);
// 0x0000003A System.Void SM_PrefabGenerator::Update()
extern void SM_PrefabGenerator_Update_m35704230751A42C5E3650386D9C1EC6DB3E96FCD (void);
// 0x0000003B System.Void SM_PrefabGenerator::.ctor()
extern void SM_PrefabGenerator__ctor_m2BF4BFEB5DF8F7DAE9628C5769F3B59E3DF9FB20 (void);
// 0x0000003C System.Void SM_RandomScale::Start()
extern void SM_RandomScale_Start_m17C10CECD63750B10FA922EF2162285A8D91F537 (void);
// 0x0000003D System.Void SM_RandomScale::.ctor()
extern void SM_RandomScale__ctor_m02B93D617B679B615B2AAD21E7640E70CC5907AD (void);
// 0x0000003E System.Void SM_Rotate::Update()
extern void SM_Rotate_Update_m709C2BAC99B54CF3A721D63124F8C705F6203C08 (void);
// 0x0000003F System.Void SM_Rotate::.ctor()
extern void SM_Rotate__ctor_mD53D8CF9151F19AEE53289845D876B4D622FD010 (void);
// 0x00000040 System.Void SM_TrailFade::Start()
extern void SM_TrailFade_Start_m4E212644808F7BE0C20714D2DFFE0936F4EBD46D (void);
// 0x00000041 System.Void SM_TrailFade::Update()
extern void SM_TrailFade_Update_m9AE70198C70A9AE476DCEE3693C17B24C8395F48 (void);
// 0x00000042 System.Void SM_TrailFade::.ctor()
extern void SM_TrailFade__ctor_m5AE36331FD8544DDDBB49E87E7CFAC30110FDC7E (void);
// 0x00000043 System.Void AutoDestruct::OnEnable()
extern void AutoDestruct_OnEnable_m5EF169A9A4679B97619C7A5AAEAA8D1A9B95180A (void);
// 0x00000044 System.Collections.IEnumerator AutoDestruct::CheckIfAlive()
extern void AutoDestruct_CheckIfAlive_m758B6C75FBD89B016EBF4B7973952DEFD89F3FF4 (void);
// 0x00000045 System.Void AutoDestruct::.ctor()
extern void AutoDestruct__ctor_m1526AE4C7AB5CA87703F739DFEBC08E37243141A (void);
// 0x00000046 System.Void AutoDestruct/<CheckIfAlive>d__2::.ctor(System.Int32)
extern void U3CCheckIfAliveU3Ed__2__ctor_mE861A1479D83DDBD45E04455F88A9ABC8F7B3629 (void);
// 0x00000047 System.Void AutoDestruct/<CheckIfAlive>d__2::System.IDisposable.Dispose()
extern void U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_mF9C430A85778F8F953F6B37B38506DB8BF54B628 (void);
// 0x00000048 System.Boolean AutoDestruct/<CheckIfAlive>d__2::MoveNext()
extern void U3CCheckIfAliveU3Ed__2_MoveNext_m64DF7E4D3986D3FE107899B386EC610F2C7F2D53 (void);
// 0x00000049 System.Object AutoDestruct/<CheckIfAlive>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5660EB52541E449718B33912722463F06EA9D354 (void);
// 0x0000004A System.Void AutoDestruct/<CheckIfAlive>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_m7EE48880783D1C70E97A944CEF859F3D4E10A04D (void);
// 0x0000004B System.Object AutoDestruct/<CheckIfAlive>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mF375ADE33206A8D43EFF846E13BB2A702EB34C86 (void);
// 0x0000004C System.Boolean ExitGames.Client.Photon.WaitForRealSeconds::get_keepWaiting()
extern void WaitForRealSeconds_get_keepWaiting_mD9A23362895518F4DBCA3056F1392D212FDFD52D (void);
// 0x0000004D System.Void ExitGames.Client.Photon.WaitForRealSeconds::.ctor(System.Single)
extern void WaitForRealSeconds__ctor_mCD44B820C0EAD126888F40FC5550D0575883EB1D (void);
// 0x0000004E System.Void ExitGames.Client.Photon.SocketWebTcp::.ctor(ExitGames.Client.Photon.PeerBase)
extern void SocketWebTcp__ctor_mBE61108B23D010BDE1EC08D28DF8BE6C29B3B7D3 (void);
// 0x0000004F System.Void ExitGames.Client.Photon.SocketWebTcp::Dispose()
extern void SocketWebTcp_Dispose_m72C16701C68338227C54E9BBFC7755889F382AB8 (void);
// 0x00000050 System.Boolean ExitGames.Client.Photon.SocketWebTcp::Connect()
extern void SocketWebTcp_Connect_m99316FBEA4213B7F8C29AD51993E1131E755650A (void);
// 0x00000051 System.Boolean ExitGames.Client.Photon.SocketWebTcp::ReadProxyConfigScheme(System.String,System.String,System.String&)
extern void SocketWebTcp_ReadProxyConfigScheme_m80A185D528226B9786F100ABA13FFC4E1799FE77 (void);
// 0x00000052 System.Boolean ExitGames.Client.Photon.SocketWebTcp::Disconnect()
extern void SocketWebTcp_Disconnect_mDE38D652C2A4964254F7430A173C248314041FB9 (void);
// 0x00000053 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketWebTcp::Send(System.Byte[],System.Int32)
extern void SocketWebTcp_Send_m9A7126088DDAB7C714157EBB7EBBB5887A7D19BC (void);
// 0x00000054 ExitGames.Client.Photon.PhotonSocketError ExitGames.Client.Photon.SocketWebTcp::Receive(System.Byte[]&)
extern void SocketWebTcp_Receive_m541CB3D1E7958A7981960BAAF7DAB83A7073BAF0 (void);
// 0x00000055 System.Collections.IEnumerator ExitGames.Client.Photon.SocketWebTcp::ReceiveLoop()
extern void SocketWebTcp_ReceiveLoop_mF9F183CCBC8DB8C2DE989DDE47BC3AD512E59C32 (void);
// 0x00000056 System.Void ExitGames.Client.Photon.SocketWebTcp::<Connect>b__5_0(ExitGames.Client.Photon.DebugLevel,System.String)
extern void SocketWebTcp_U3CConnectU3Eb__5_0_m97ACA5A29AAE7611CE90A5AC30E486BB7D69411F (void);
// 0x00000057 System.Void ExitGames.Client.Photon.SocketWebTcp/MonoBehaviourExt::.ctor()
extern void MonoBehaviourExt__ctor_mCBC834820BA80499919028FDFFB3DB2F8A71223A (void);
// 0x00000058 System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::.ctor(System.Int32)
extern void U3CReceiveLoopU3Ed__13__ctor_mCDCF340A15757B92764F2D5B92D61109DA7D53F2 (void);
// 0x00000059 System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.IDisposable.Dispose()
extern void U3CReceiveLoopU3Ed__13_System_IDisposable_Dispose_mA2A91E841E6E9C44E451EA6CD329444B9F1D8A87 (void);
// 0x0000005A System.Boolean ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::MoveNext()
extern void U3CReceiveLoopU3Ed__13_MoveNext_m7C10AD5BD6C177DACBCBC0AB3660E590864C2AEA (void);
// 0x0000005B System.Object ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReceiveLoopU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BEC599CA0BB96847FB4B63F8BDC064C9EB10660 (void);
// 0x0000005C System.Void ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.IEnumerator.Reset()
extern void U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_Reset_m963A4FE5B939246FAD8613A5075447A95C61459F (void);
// 0x0000005D System.Object ExitGames.Client.Photon.SocketWebTcp/<ReceiveLoop>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_get_Current_mDC9710DB0C8BC7A59212D9117410111F2B853B76 (void);
// 0x0000005E System.Action`2<ExitGames.Client.Photon.DebugLevel,System.String> ExitGames.Client.Photon.WebSocket::get_DebugReturn()
extern void WebSocket_get_DebugReturn_m3247892DD17A80D39830618E4DA8F418C769CA10 (void);
// 0x0000005F System.Void ExitGames.Client.Photon.WebSocket::set_DebugReturn(System.Action`2<ExitGames.Client.Photon.DebugLevel,System.String>)
extern void WebSocket_set_DebugReturn_mF7D5A5524220369A5D357D2E85B11546DE36D9D6 (void);
// 0x00000060 System.Void ExitGames.Client.Photon.WebSocket::.ctor(System.Uri,System.String,System.String)
extern void WebSocket__ctor_mC9273E1CE95FFBFCC78FF42170C1384DF3F81546 (void);
// 0x00000061 System.String ExitGames.Client.Photon.WebSocket::get_ProxyAddress()
extern void WebSocket_get_ProxyAddress_mFF4DE1366346A69998ABA214BBE30BC008121DB2 (void);
// 0x00000062 System.Void ExitGames.Client.Photon.WebSocket::SendString(System.String)
extern void WebSocket_SendString_m75FB55EE26B4353F5A4F42CB7E211511BCD4E0DB (void);
// 0x00000063 System.String ExitGames.Client.Photon.WebSocket::RecvString()
extern void WebSocket_RecvString_mAE363C5A4A1ACE1B23869C713F17B5726E22786E (void);
// 0x00000064 System.Int32 ExitGames.Client.Photon.WebSocket::SocketCreate(System.String,System.String)
extern void WebSocket_SocketCreate_mBFE71221C853FFF81A35E99BB055090753F9DFF4 (void);
// 0x00000065 System.Int32 ExitGames.Client.Photon.WebSocket::SocketState(System.Int32)
extern void WebSocket_SocketState_mB73648C8D1477F78650B7558A6F5D2FE3E69CB5B (void);
// 0x00000066 System.Void ExitGames.Client.Photon.WebSocket::SocketSend(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketSend_mE514AEA4FDF5C337C89403C36B7CCC6AA4E0CE17 (void);
// 0x00000067 System.Void ExitGames.Client.Photon.WebSocket::SocketRecv(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketRecv_m75B6AAD5A6277781F956DBAB8DF5A34C0B3F3819 (void);
// 0x00000068 System.Int32 ExitGames.Client.Photon.WebSocket::SocketRecvLength(System.Int32)
extern void WebSocket_SocketRecvLength_m6A1B9596D407D0D60F9670F9B83A84BDF2C75066 (void);
// 0x00000069 System.Void ExitGames.Client.Photon.WebSocket::SocketClose(System.Int32)
extern void WebSocket_SocketClose_m01A526A136E63EF88AD6CAC35F4A2FF9D54A60E1 (void);
// 0x0000006A System.Int32 ExitGames.Client.Photon.WebSocket::SocketError(System.Int32,System.Byte[],System.Int32)
extern void WebSocket_SocketError_m16E676C036A8AB654C229A9118ADC904407F0A35 (void);
// 0x0000006B System.Void ExitGames.Client.Photon.WebSocket::Send(System.Byte[])
extern void WebSocket_Send_m392898365DCDE2DB3078EAA304C218118FDCE530 (void);
// 0x0000006C System.Byte[] ExitGames.Client.Photon.WebSocket::Recv()
extern void WebSocket_Recv_m832D37383EC105FCE284B18134BF41E7E3F30240 (void);
// 0x0000006D System.Void ExitGames.Client.Photon.WebSocket::Connect()
extern void WebSocket_Connect_m7B5CF2421641042D7A2D4E6A3504AC7215183B6A (void);
// 0x0000006E System.Void ExitGames.Client.Photon.WebSocket::Close()
extern void WebSocket_Close_mDD987F81E705E7286187959E4F5F3795D1A13E60 (void);
// 0x0000006F System.Boolean ExitGames.Client.Photon.WebSocket::get_Connected()
extern void WebSocket_get_Connected_m85B98EF74D8A3DC7B6B7450D6DE4B62D4972219B (void);
// 0x00000070 System.String ExitGames.Client.Photon.WebSocket::get_Error()
extern void WebSocket_get_Error_m84716075C633D3238F2630BC1CEAB1FE6F392B15 (void);
// 0x00000071 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Start()
extern void ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421 (void);
// 0x00000072 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::Update()
extern void ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2 (void);
// 0x00000073 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnected()
extern void ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114 (void);
// 0x00000074 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnConnectedToMaster()
extern void ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2 (void);
// 0x00000075 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnDisconnected(Photon.Realtime.DisconnectCause)
extern void ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7 (void);
// 0x00000076 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationResponse(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459 (void);
// 0x00000077 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCustomAuthenticationFailed(System.String)
extern void ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B (void);
// 0x00000078 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionListReceived(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E (void);
// 0x00000079 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0 (void);
// 0x0000007A System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLobbyStatisticsUpdate(System.Collections.Generic.List`1<Photon.Realtime.TypedLobbyInfo>)
extern void ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7 (void);
// 0x0000007B System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedLobby()
extern void ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF (void);
// 0x0000007C System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftLobby()
extern void ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4 (void);
// 0x0000007D System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnFriendListUpdate(System.Collections.Generic.List`1<Photon.Realtime.FriendInfo>)
extern void ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB (void);
// 0x0000007E System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreatedRoom()
extern void ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583 (void);
// 0x0000007F System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnCreateRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24 (void);
// 0x00000080 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinedRoom()
extern void ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B (void);
// 0x00000081 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRoomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C (void);
// 0x00000082 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnJoinRandomFailed(System.Int16,System.String)
extern void ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69 (void);
// 0x00000083 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnLeftRoom()
extern void ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137 (void);
// 0x00000084 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::OnRegionPingCompleted(Photon.Realtime.RegionHandler)
extern void ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17 (void);
// 0x00000085 System.Void Photon.Realtime.Demo.ConnectAndJoinRandomLb::.ctor()
extern void ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1 (void);
// 0x00000086 Photon.Chat.ChatAppSettings Photon.Chat.Demo.AppSettingsExtensions::GetChatSettings(Photon.Realtime.AppSettings)
extern void AppSettingsExtensions_GetChatSettings_m32D1D0AF37E161CF26E9180F659F6657F6F24D00 (void);
// 0x00000087 System.Void Photon.Chat.Demo.ChannelSelector::SetChannel(System.String)
extern void ChannelSelector_SetChannel_m29F99876C0AF817AEFBDB5539DF70F23558D2A7E (void);
// 0x00000088 System.Void Photon.Chat.Demo.ChannelSelector::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ChannelSelector_OnPointerClick_mA5A9AB30717F59D434855F47CAF95543C7A64B9C (void);
// 0x00000089 System.Void Photon.Chat.Demo.ChannelSelector::.ctor()
extern void ChannelSelector__ctor_mB3DF2FA2ED74E01976D4D3ACFB50CB096FEF18F0 (void);
// 0x0000008A System.Void Photon.Chat.Demo.ChatAppIdCheckerUI::Update()
extern void ChatAppIdCheckerUI_Update_m1D3579AE4EE35E468A353AA67EBD5D9F7C87E2E9 (void);
// 0x0000008B System.Void Photon.Chat.Demo.ChatAppIdCheckerUI::.ctor()
extern void ChatAppIdCheckerUI__ctor_mA1D182C1AED55533944CD356D13AA447574B7870 (void);
// 0x0000008C System.String Photon.Chat.Demo.ChatGui::get_UserName()
extern void ChatGui_get_UserName_mE13C9B7C725EC8F90975DE2D4ED7F3CC28596ACF (void);
// 0x0000008D System.Void Photon.Chat.Demo.ChatGui::set_UserName(System.String)
extern void ChatGui_set_UserName_m8955E5E94B1B8469E4FCD87D10041F53A80E9E0B (void);
// 0x0000008E System.Void Photon.Chat.Demo.ChatGui::Start()
extern void ChatGui_Start_m9C36C012AF6740959AD6855FCE3C7156D42B636A (void);
// 0x0000008F System.Void Photon.Chat.Demo.ChatGui::Connect()
extern void ChatGui_Connect_mF3437B3F3E848B574D498715B4FB45CBB8A769CD (void);
// 0x00000090 System.Void Photon.Chat.Demo.ChatGui::OnDestroy()
extern void ChatGui_OnDestroy_mA771028772B2BE041F6A8611CB67E6E369654D8E (void);
// 0x00000091 System.Void Photon.Chat.Demo.ChatGui::OnApplicationQuit()
extern void ChatGui_OnApplicationQuit_mCF2A69BA18FF51991BB0F7F5A6898B9C8D19B099 (void);
// 0x00000092 System.Void Photon.Chat.Demo.ChatGui::Update()
extern void ChatGui_Update_mF777D28857ABC30C851D6DD32B3A36BBE7880958 (void);
// 0x00000093 System.Void Photon.Chat.Demo.ChatGui::OnEnterSend()
extern void ChatGui_OnEnterSend_mD1E43BC5C5ED3F65C8287C7CCB2D09BE0E508375 (void);
// 0x00000094 System.Void Photon.Chat.Demo.ChatGui::OnClickSend()
extern void ChatGui_OnClickSend_mA34A7BDF93AF080B0926A4D503AC77035C6345D7 (void);
// 0x00000095 System.Void Photon.Chat.Demo.ChatGui::SendChatMessage(System.String)
extern void ChatGui_SendChatMessage_mC76BD609B9B8E024027BD69E1915E4577B65E76F (void);
// 0x00000096 System.Void Photon.Chat.Demo.ChatGui::PostHelpToCurrentChannel()
extern void ChatGui_PostHelpToCurrentChannel_m14AD0594316C6C38A2B85F390B52595A1CF8FD17 (void);
// 0x00000097 System.Void Photon.Chat.Demo.ChatGui::DebugReturn(ExitGames.Client.Photon.DebugLevel,System.String)
extern void ChatGui_DebugReturn_m7E7552B5CD94F91CE5C41CD7813EDE70B6176567 (void);
// 0x00000098 System.Void Photon.Chat.Demo.ChatGui::OnConnected()
extern void ChatGui_OnConnected_m63335F6ADB99CA4C1DBD70CE3D3BE669DE0E53B9 (void);
// 0x00000099 System.Void Photon.Chat.Demo.ChatGui::OnDisconnected()
extern void ChatGui_OnDisconnected_m4AAE28BB7B47BAD132651AFC8E27F573A36E3AFF (void);
// 0x0000009A System.Void Photon.Chat.Demo.ChatGui::OnChatStateChange(Photon.Chat.ChatState)
extern void ChatGui_OnChatStateChange_m166507E98B7F98C6E52223B9404BD384411F8BEA (void);
// 0x0000009B System.Void Photon.Chat.Demo.ChatGui::OnSubscribed(System.String[],System.Boolean[])
extern void ChatGui_OnSubscribed_m52519890868FA6E484E27F5E39B02833EB54DDB3 (void);
// 0x0000009C System.Void Photon.Chat.Demo.ChatGui::OnSubscribed(System.String,System.String[],System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnSubscribed_mDDD167FAC1109BBC42B8C8C9772AC321D52A5FE4 (void);
// 0x0000009D System.Void Photon.Chat.Demo.ChatGui::InstantiateChannelButton(System.String)
extern void ChatGui_InstantiateChannelButton_mC727F0E0569BAB8AA5DAC9B2EF8A95D49A4A5032 (void);
// 0x0000009E System.Void Photon.Chat.Demo.ChatGui::InstantiateFriendButton(System.String)
extern void ChatGui_InstantiateFriendButton_m24C82847C192124605436330992D637C8A3E7EFB (void);
// 0x0000009F System.Void Photon.Chat.Demo.ChatGui::OnUnsubscribed(System.String[])
extern void ChatGui_OnUnsubscribed_m968F1850ADFE61F878F461E87AF20857A0EE1B94 (void);
// 0x000000A0 System.Void Photon.Chat.Demo.ChatGui::OnGetMessages(System.String,System.String[],System.Object[])
extern void ChatGui_OnGetMessages_mE3CAA812F933B7B5C8B4277B56988B18343CEB07 (void);
// 0x000000A1 System.Void Photon.Chat.Demo.ChatGui::OnPrivateMessage(System.String,System.Object,System.String)
extern void ChatGui_OnPrivateMessage_mB6EF6D8710F673B8D9B6403F3AF1D13EFEB9269F (void);
// 0x000000A2 System.Void Photon.Chat.Demo.ChatGui::OnStatusUpdate(System.String,System.Int32,System.Boolean,System.Object)
extern void ChatGui_OnStatusUpdate_mDE1FAF6055E1758AB9AF5D7B772F9FEA94DB3BDB (void);
// 0x000000A3 System.Void Photon.Chat.Demo.ChatGui::OnUserSubscribed(System.String,System.String)
extern void ChatGui_OnUserSubscribed_m2E2516927F409CEF9D5FB44497AD6B113222409A (void);
// 0x000000A4 System.Void Photon.Chat.Demo.ChatGui::OnUserUnsubscribed(System.String,System.String)
extern void ChatGui_OnUserUnsubscribed_m2BC2DFEA285C488D13166EBA5698B72550F78654 (void);
// 0x000000A5 System.Void Photon.Chat.Demo.ChatGui::OnChannelPropertiesChanged(System.String,System.String,System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnChannelPropertiesChanged_mFF2EFCD0C671520155F17F256257E821E51D1926 (void);
// 0x000000A6 System.Void Photon.Chat.Demo.ChatGui::OnUserPropertiesChanged(System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.Object,System.Object>)
extern void ChatGui_OnUserPropertiesChanged_m5B5A07B865481CAC5E65D4C28AD060C27EC37DD5 (void);
// 0x000000A7 System.Void Photon.Chat.Demo.ChatGui::OnErrorInfo(System.String,System.String,System.Object)
extern void ChatGui_OnErrorInfo_m1EA237F6DAE99E2F52D45C421B2AB1DCD90E4673 (void);
// 0x000000A8 System.Void Photon.Chat.Demo.ChatGui::AddMessageToSelectedChannel(System.String)
extern void ChatGui_AddMessageToSelectedChannel_m4E2AF559DB50F8C53DA41268C1175E09AA471A1C (void);
// 0x000000A9 System.Void Photon.Chat.Demo.ChatGui::ShowChannel(System.String)
extern void ChatGui_ShowChannel_m696F566CA7F08601752BB3E3508AFA025AC587C3 (void);
// 0x000000AA System.Void Photon.Chat.Demo.ChatGui::OpenDashboard()
extern void ChatGui_OpenDashboard_mEF6A4AC58094283E6942489BED251351E305A01E (void);
// 0x000000AB System.Void Photon.Chat.Demo.ChatGui::.ctor()
extern void ChatGui__ctor_mB1A092C06A5A45E3A7A337A023E08878833B120F (void);
// 0x000000AC System.Void Photon.Chat.Demo.ChatGui::.cctor()
extern void ChatGui__cctor_m7B39238B5EAA662C1906D002E4FB09E9EB0F7100 (void);
// 0x000000AD System.Void Photon.Chat.Demo.FriendItem::set_FriendId(System.String)
extern void FriendItem_set_FriendId_m8359A1EA2AC4624D7D288C1DDC2350975623F7CD (void);
// 0x000000AE System.String Photon.Chat.Demo.FriendItem::get_FriendId()
extern void FriendItem_get_FriendId_mD5028C5F9C1D12F917261C119C45684B3E4B544D (void);
// 0x000000AF System.Void Photon.Chat.Demo.FriendItem::Awake()
extern void FriendItem_Awake_mDEBF0CC81C911D57EBD8690494E68E9F88ED6F4D (void);
// 0x000000B0 System.Void Photon.Chat.Demo.FriendItem::OnFriendStatusUpdate(System.Int32,System.Boolean,System.Object)
extern void FriendItem_OnFriendStatusUpdate_m00D8E4A7CFFF6F81A81ACCB5EDFC1142EEAC5718 (void);
// 0x000000B1 System.Void Photon.Chat.Demo.FriendItem::.ctor()
extern void FriendItem__ctor_mECF93E0BF74E35EB73BF307FFC9DB30D77343F38 (void);
// 0x000000B2 System.Boolean Photon.Chat.Demo.IgnoreUiRaycastWhenInactive::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_m677C47DFEBF604215800A9B741A3300BD0A3947D (void);
// 0x000000B3 System.Void Photon.Chat.Demo.IgnoreUiRaycastWhenInactive::.ctor()
extern void IgnoreUiRaycastWhenInactive__ctor_m710699723A0DE8CB36160AACC822FF3198C5F009 (void);
// 0x000000B4 System.Void Photon.Chat.Demo.NamePickGui::Start()
extern void NamePickGui_Start_m261A0D587E971FB69F2A604F924BFACE3431E964 (void);
// 0x000000B5 System.Void Photon.Chat.Demo.NamePickGui::EndEditOnEnter()
extern void NamePickGui_EndEditOnEnter_m9CBE79633FA01A8D57125C5DEB62629B95C98755 (void);
// 0x000000B6 System.Void Photon.Chat.Demo.NamePickGui::StartChat()
extern void NamePickGui_StartChat_m956F5E92C89AB50B4590BC5023FC40023530002E (void);
// 0x000000B7 System.Void Photon.Chat.Demo.NamePickGui::.ctor()
extern void NamePickGui__ctor_m2AB2D19C88F5FAFCB399BB58B98BD25B50F19881 (void);
// 0x000000B8 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::OnEnable()
extern void EventSystemSpawner_OnEnable_m92046CF7C2B99309677158A5205E37FA11EE9302 (void);
// 0x000000B9 System.Void Photon.Chat.UtilityScripts.EventSystemSpawner::.ctor()
extern void EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331 (void);
// 0x000000BA System.Void Photon.Chat.UtilityScripts.OnStartDelete::Start()
extern void OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33 (void);
// 0x000000BB System.Void Photon.Chat.UtilityScripts.OnStartDelete::.ctor()
extern void OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270 (void);
// 0x000000BC System.Void Photon.Chat.UtilityScripts.TextButtonTransition::Awake()
extern void TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E (void);
// 0x000000BD System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnEnable()
extern void TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F (void);
// 0x000000BE System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnDisable()
extern void TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72 (void);
// 0x000000BF System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07 (void);
// 0x000000C0 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02 (void);
// 0x000000C1 System.Void Photon.Chat.UtilityScripts.TextButtonTransition::.ctor()
extern void TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570 (void);
// 0x000000C2 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnEnable()
extern void TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2 (void);
// 0x000000C3 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnDisable()
extern void TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404 (void);
// 0x000000C4 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnValueChanged(System.Boolean)
extern void TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB (void);
// 0x000000C5 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5 (void);
// 0x000000C6 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20 (void);
// 0x000000C7 System.Void Photon.Chat.UtilityScripts.TextToggleIsOnTransition::.ctor()
extern void TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04 (void);
// 0x000000C8 System.Void DevionGames.YesNoDialog::OnStart()
extern void YesNoDialog_OnStart_m15C0C305AF068DACFE29DC2BD62E4D60982176FD (void);
// 0x000000C9 DevionGames.ActionStatus DevionGames.YesNoDialog::OnUpdate()
extern void YesNoDialog_OnUpdate_m9C6B1630E4E29EAEABEA0402ACFB40B59C294280 (void);
// 0x000000CA System.Void DevionGames.YesNoDialog::OnClose(DevionGames.CallbackEventData)
extern void YesNoDialog_OnClose_mA1099FF95DF9E68D0BC78C35B91062C58B0F1991 (void);
// 0x000000CB System.Void DevionGames.YesNoDialog::OnResponse(System.Int32)
extern void YesNoDialog_OnResponse_mACC597AB7B60C065A4009A8003544B9D0CECC79E (void);
// 0x000000CC System.Void DevionGames.YesNoDialog::.ctor()
extern void YesNoDialog__ctor_m569321A952C47C78B7C782DA0751989B5F5FC9AD (void);
// 0x000000CD System.Void UnityStandardAssets.Water.Water::OnWillRenderObject()
extern void Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54 (void);
// 0x000000CE System.Void UnityStandardAssets.Water.Water::OnDisable()
extern void Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD (void);
// 0x000000CF System.Void UnityStandardAssets.Water.Water::Update()
extern void Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84 (void);
// 0x000000D0 System.Void UnityStandardAssets.Water.Water::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011 (void);
// 0x000000D1 System.Void UnityStandardAssets.Water.Water::CreateWaterObjects(UnityEngine.Camera,UnityEngine.Camera&,UnityEngine.Camera&)
extern void Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207 (void);
// 0x000000D2 UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::GetWaterMode()
extern void Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D (void);
// 0x000000D3 UnityStandardAssets.Water.Water/WaterMode UnityStandardAssets.Water.Water::FindHardwareWaterSupport()
extern void Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789 (void);
// 0x000000D4 UnityEngine.Vector4 UnityStandardAssets.Water.Water::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2 (void);
// 0x000000D5 System.Void UnityStandardAssets.Water.Water::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0 (void);
// 0x000000D6 System.Void UnityStandardAssets.Water.Water::.ctor()
extern void Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC (void);
// 0x000000D7 System.Void UnityStandardAssets.Water.Displace::Awake()
extern void Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7 (void);
// 0x000000D8 System.Void UnityStandardAssets.Water.Displace::OnEnable()
extern void Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8 (void);
// 0x000000D9 System.Void UnityStandardAssets.Water.Displace::OnDisable()
extern void Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08 (void);
// 0x000000DA System.Void UnityStandardAssets.Water.Displace::.ctor()
extern void Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8 (void);
// 0x000000DB System.Void UnityStandardAssets.Water.GerstnerDisplace::.ctor()
extern void GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF (void);
// 0x000000DC System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern void MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B (void);
// 0x000000DD System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern void MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA (void);
// 0x000000DE System.Void UnityStandardAssets.Water.PlanarReflection::Start()
extern void PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874 (void);
// 0x000000DF UnityEngine.Camera UnityStandardAssets.Water.PlanarReflection::CreateReflectionCameraFor(UnityEngine.Camera)
extern void PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734 (void);
// 0x000000E0 System.Void UnityStandardAssets.Water.PlanarReflection::SetStandardCameraParameter(UnityEngine.Camera,UnityEngine.LayerMask)
extern void PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9 (void);
// 0x000000E1 UnityEngine.RenderTexture UnityStandardAssets.Water.PlanarReflection::CreateTextureFor(UnityEngine.Camera)
extern void PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF (void);
// 0x000000E2 System.Void UnityStandardAssets.Water.PlanarReflection::RenderHelpCameras(UnityEngine.Camera)
extern void PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241 (void);
// 0x000000E3 System.Void UnityStandardAssets.Water.PlanarReflection::LateUpdate()
extern void PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C (void);
// 0x000000E4 System.Void UnityStandardAssets.Water.PlanarReflection::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1 (void);
// 0x000000E5 System.Void UnityStandardAssets.Water.PlanarReflection::OnEnable()
extern void PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281 (void);
// 0x000000E6 System.Void UnityStandardAssets.Water.PlanarReflection::OnDisable()
extern void PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36 (void);
// 0x000000E7 System.Void UnityStandardAssets.Water.PlanarReflection::RenderReflectionFor(UnityEngine.Camera,UnityEngine.Camera)
extern void PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987 (void);
// 0x000000E8 System.Void UnityStandardAssets.Water.PlanarReflection::SaneCameraSettings(UnityEngine.Camera)
extern void PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E (void);
// 0x000000E9 UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB (void);
// 0x000000EA UnityEngine.Matrix4x4 UnityStandardAssets.Water.PlanarReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271 (void);
// 0x000000EB System.Single UnityStandardAssets.Water.PlanarReflection::Sgn(System.Single)
extern void PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A (void);
// 0x000000EC UnityEngine.Vector4 UnityStandardAssets.Water.PlanarReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B (void);
// 0x000000ED System.Void UnityStandardAssets.Water.PlanarReflection::.ctor()
extern void PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F (void);
// 0x000000EE System.Void UnityStandardAssets.Water.SpecularLighting::Start()
extern void SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE (void);
// 0x000000EF System.Void UnityStandardAssets.Water.SpecularLighting::Update()
extern void SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1 (void);
// 0x000000F0 System.Void UnityStandardAssets.Water.SpecularLighting::.ctor()
extern void SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E (void);
// 0x000000F1 System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern void WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6 (void);
// 0x000000F2 System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern void WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6 (void);
// 0x000000F3 System.Void UnityStandardAssets.Water.WaterBase::Update()
extern void WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C (void);
// 0x000000F4 System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern void WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC (void);
// 0x000000F5 System.Void UnityStandardAssets.Water.WaterTile::Start()
extern void WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944 (void);
// 0x000000F6 System.Void UnityStandardAssets.Water.WaterTile::AcquireComponents()
extern void WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B (void);
// 0x000000F7 System.Void UnityStandardAssets.Water.WaterTile::OnWillRenderObject()
extern void WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9 (void);
// 0x000000F8 System.Void UnityStandardAssets.Water.WaterTile::.ctor()
extern void WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5 (void);
static Il2CppMethodPointer s_methodPointers[248] = 
{
	AddScore_Start_m2CECC366B94A630ECF872F63132CFA39489B6974,
	AddScore_OnTriggerEnter_m9683F927AD134B84211856ED1D43EF9529E2A9B8,
	AddScore_Fin_m6321518FF170B3611652A4DAB77BB3115AEA21C8,
	AddScore__ctor_m39BA7F92305F30511F00F407945F7A2B02365973,
	Die_Start_m67A3A33B7036827D8D2CB99333FEC637BF2A4ADF,
	Die_Update_m7F694C97563A3CC38968E9FE0ACB6E3B79C3C105,
	Die_OnCollisionEnter_mB87777D11312C05E24C70EFA61C852C72C547B91,
	Die_RestaBarra_m16793F3E0A3262D74B04AABEBBAA391F2B3BF5A7,
	Die_SumarBarra_m064E2C53EA448C3EE694E2FAAA05202BAE8174B4,
	Die_Fin_mA3F4E61DCEC80E61C7C4562F20D128B4DFE9D697,
	Die__ctor_m69277E6E4F98D03F466D17392670F8ABB40C1F5D,
	HealtBar_SetMaxHealth_m8CFB045BD2FD54FA0A1063D46DA454BE5E61CE0B,
	HealtBar_setHealth_mEAE63824DEB2EAAFFC1481B73A09EF3116E1DDA6,
	HealtBar__ctor_m70D11B02753C2852C3F27A19FF3E0F2879B69C0D,
	Menu_EscenaJuego_m05F7043C4A9ADA7D61DDC0C5D20490C70457406A,
	Menu_CargarNivel_mF353A0D0775C71D0CB0AEDBAF1AA3A2A99A688FA,
	Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134,
	Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC,
	Pause_Settings_mD30A751D51933D495A40658095A9C8FB969C515F,
	Pause_Stop_m1E584C228C2BCF9C735FA97AB8BA059C820AA6A4,
	Pause_Play_mEF4CF8B350D2A55CF9AEBAFB249F06A5BD6B4F1A,
	Pause_Restart_m534DC75B82B5CEF183E318BA5E5D0FD6CCB3AADB,
	Pause_Quit_mCDE8B0EE9E6C1F2CD7ED66CAC4C23BD195E982F3,
	Pause_Menu_m96B5575F6631358E7FD55E0503A1CF497FAAF509,
	Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928,
	Quit_Exit_m97F7318E67A0708CC8F4187B4BC441BE02F75DA1,
	Quit__ctor_mEB6E7FE6F53362692AD96E4DEE1D3906A592A7C4,
	SliderVolume_Start_m03E96ECF4FCBE895C1EB972F9B520D1B844F9548,
	SliderVolume_ChangeSlider_m33FA89BE2C05F9A62FE02E7C15613B23C4B1F22B,
	SliderVolume_ReviMute_m4A60A607320E30B4A8AD3BE61BC82B748C13E06E,
	SliderVolume__ctor_m56BDF89A5DB8221EE3D40DA3822E366991309AED,
	ControlRed_Awake_m19D85637E3F1BB6726E8345CD5D813FE418E7830,
	ControlRed_Start_m23C11C9A0031FA48AABFB0B09744BE50C721BB15,
	ControlRed_Update_mC888523955814CE25D3752ED09FFA976285E6BD8,
	ControlRed__ctor_mEF7307A0F005B55555272843F536833645A91E73,
	DaUF1o_Start_mE704F1D1E6DE47B3EFF52E50DC89CAAB4DC7A3E9,
	DaUF1o_OnCollisionEnter_m1B393B1ADC820834A8DCFF499E69F9AB0C65DD1C,
	DaUF1o__ctor_m1B109991F875B794458D2645D286CB712A9BEFD5,
	GestorPhoton_Start_m04782C9CB53B13F023D931BB415241C4099C161C,
	GestorPhoton_OnConnectedToMaster_m7B18A72517952A9A1F4BDFE6E98F53123B30E8A6,
	GestorPhoton_OnJoinedLobby_m9A21314F6EACB4074F0139579A8FAB8FFEB9B64F,
	GestorPhoton_OnJoinedRoom_m2B3E32763051448B48BA389C43E61DE1323BF332,
	GestorPhoton__ctor_m49E9993096FD830BF8B0EF2ED13870CAF8283B05,
	RedPlayer_Start_m0C40256C16CA66CF6EF347B0CE4A373FAD719016,
	RedPlayer__ctor_m262EABC76B0F6E67F1977BBE392D15DA9320BF76,
	ScaleChange_Start_m29AA7831AD05AB8E8E488E246B639855EE2A9DAD,
	ScaleChange_Update_m4CE746D380D1EEC45ECE5259F632E3258198EB47,
	ScaleChange__ctor_m5281007D39C05C8DB35F08E47D2A58D61EC407DC,
	Vida_Start_mC68F7B8FDD945434F957D43FAE8BFCE5604ED5B5,
	Vida_Update_m005476FD340EA5FBC4FCBE634840107FEE383B6D,
	Vida_RevisarVida_m78416A96AB8D758B441FB5F421C052FE92C53888,
	Vida__ctor_m510046C9FAF7857DB658C85226601374DE81FF92,
	SM_AnimSpeedRandomizer_Start_m34CB399D7A1D350BDC933C9FD578993787EB288A,
	SM_AnimSpeedRandomizer__ctor_m1FEB51070F32C87E818C4ABB62822204416BBF5C,
	SM_DestroyTimed_Start_m7955412CE525AE89CEE5CB322E2F95E0561A6F7B,
	SM_DestroyTimed__ctor_mE2ECD56496ECE4C65A3C23FED1FB90D62246C759,
	SM_PrefabGenerator_Start_m1C4FDCA60EEDDE08F2B59D79301A7E7F72009645,
	SM_PrefabGenerator_Update_m35704230751A42C5E3650386D9C1EC6DB3E96FCD,
	SM_PrefabGenerator__ctor_m2BF4BFEB5DF8F7DAE9628C5769F3B59E3DF9FB20,
	SM_RandomScale_Start_m17C10CECD63750B10FA922EF2162285A8D91F537,
	SM_RandomScale__ctor_m02B93D617B679B615B2AAD21E7640E70CC5907AD,
	SM_Rotate_Update_m709C2BAC99B54CF3A721D63124F8C705F6203C08,
	SM_Rotate__ctor_mD53D8CF9151F19AEE53289845D876B4D622FD010,
	SM_TrailFade_Start_m4E212644808F7BE0C20714D2DFFE0936F4EBD46D,
	SM_TrailFade_Update_m9AE70198C70A9AE476DCEE3693C17B24C8395F48,
	SM_TrailFade__ctor_m5AE36331FD8544DDDBB49E87E7CFAC30110FDC7E,
	AutoDestruct_OnEnable_m5EF169A9A4679B97619C7A5AAEAA8D1A9B95180A,
	AutoDestruct_CheckIfAlive_m758B6C75FBD89B016EBF4B7973952DEFD89F3FF4,
	AutoDestruct__ctor_m1526AE4C7AB5CA87703F739DFEBC08E37243141A,
	U3CCheckIfAliveU3Ed__2__ctor_mE861A1479D83DDBD45E04455F88A9ABC8F7B3629,
	U3CCheckIfAliveU3Ed__2_System_IDisposable_Dispose_mF9C430A85778F8F953F6B37B38506DB8BF54B628,
	U3CCheckIfAliveU3Ed__2_MoveNext_m64DF7E4D3986D3FE107899B386EC610F2C7F2D53,
	U3CCheckIfAliveU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5660EB52541E449718B33912722463F06EA9D354,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_Reset_m7EE48880783D1C70E97A944CEF859F3D4E10A04D,
	U3CCheckIfAliveU3Ed__2_System_Collections_IEnumerator_get_Current_mF375ADE33206A8D43EFF846E13BB2A702EB34C86,
	WaitForRealSeconds_get_keepWaiting_mD9A23362895518F4DBCA3056F1392D212FDFD52D,
	WaitForRealSeconds__ctor_mCD44B820C0EAD126888F40FC5550D0575883EB1D,
	SocketWebTcp__ctor_mBE61108B23D010BDE1EC08D28DF8BE6C29B3B7D3,
	SocketWebTcp_Dispose_m72C16701C68338227C54E9BBFC7755889F382AB8,
	SocketWebTcp_Connect_m99316FBEA4213B7F8C29AD51993E1131E755650A,
	SocketWebTcp_ReadProxyConfigScheme_m80A185D528226B9786F100ABA13FFC4E1799FE77,
	SocketWebTcp_Disconnect_mDE38D652C2A4964254F7430A173C248314041FB9,
	SocketWebTcp_Send_m9A7126088DDAB7C714157EBB7EBBB5887A7D19BC,
	SocketWebTcp_Receive_m541CB3D1E7958A7981960BAAF7DAB83A7073BAF0,
	SocketWebTcp_ReceiveLoop_mF9F183CCBC8DB8C2DE989DDE47BC3AD512E59C32,
	SocketWebTcp_U3CConnectU3Eb__5_0_m97ACA5A29AAE7611CE90A5AC30E486BB7D69411F,
	MonoBehaviourExt__ctor_mCBC834820BA80499919028FDFFB3DB2F8A71223A,
	U3CReceiveLoopU3Ed__13__ctor_mCDCF340A15757B92764F2D5B92D61109DA7D53F2,
	U3CReceiveLoopU3Ed__13_System_IDisposable_Dispose_mA2A91E841E6E9C44E451EA6CD329444B9F1D8A87,
	U3CReceiveLoopU3Ed__13_MoveNext_m7C10AD5BD6C177DACBCBC0AB3660E590864C2AEA,
	U3CReceiveLoopU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BEC599CA0BB96847FB4B63F8BDC064C9EB10660,
	U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_Reset_m963A4FE5B939246FAD8613A5075447A95C61459F,
	U3CReceiveLoopU3Ed__13_System_Collections_IEnumerator_get_Current_mDC9710DB0C8BC7A59212D9117410111F2B853B76,
	WebSocket_get_DebugReturn_m3247892DD17A80D39830618E4DA8F418C769CA10,
	WebSocket_set_DebugReturn_mF7D5A5524220369A5D357D2E85B11546DE36D9D6,
	WebSocket__ctor_mC9273E1CE95FFBFCC78FF42170C1384DF3F81546,
	WebSocket_get_ProxyAddress_mFF4DE1366346A69998ABA214BBE30BC008121DB2,
	WebSocket_SendString_m75FB55EE26B4353F5A4F42CB7E211511BCD4E0DB,
	WebSocket_RecvString_mAE363C5A4A1ACE1B23869C713F17B5726E22786E,
	WebSocket_SocketCreate_mBFE71221C853FFF81A35E99BB055090753F9DFF4,
	WebSocket_SocketState_mB73648C8D1477F78650B7558A6F5D2FE3E69CB5B,
	WebSocket_SocketSend_mE514AEA4FDF5C337C89403C36B7CCC6AA4E0CE17,
	WebSocket_SocketRecv_m75B6AAD5A6277781F956DBAB8DF5A34C0B3F3819,
	WebSocket_SocketRecvLength_m6A1B9596D407D0D60F9670F9B83A84BDF2C75066,
	WebSocket_SocketClose_m01A526A136E63EF88AD6CAC35F4A2FF9D54A60E1,
	WebSocket_SocketError_m16E676C036A8AB654C229A9118ADC904407F0A35,
	WebSocket_Send_m392898365DCDE2DB3078EAA304C218118FDCE530,
	WebSocket_Recv_m832D37383EC105FCE284B18134BF41E7E3F30240,
	WebSocket_Connect_m7B5CF2421641042D7A2D4E6A3504AC7215183B6A,
	WebSocket_Close_mDD987F81E705E7286187959E4F5F3795D1A13E60,
	WebSocket_get_Connected_m85B98EF74D8A3DC7B6B7450D6DE4B62D4972219B,
	WebSocket_get_Error_m84716075C633D3238F2630BC1CEAB1FE6F392B15,
	ConnectAndJoinRandomLb_Start_m8784F9A942632995E527C9AAC79D40049C1B4421,
	ConnectAndJoinRandomLb_Update_m27F506BAFEF2C3264B9030218D0ADB86478750E2,
	ConnectAndJoinRandomLb_OnConnected_m5879B131543322FA6CC2D9FB2B4C196698680114,
	ConnectAndJoinRandomLb_OnConnectedToMaster_mCCF5E7C0A7A1A8486F3571F05B2E0662C7C939F2,
	ConnectAndJoinRandomLb_OnDisconnected_m2576DA0537A899046151D7FA51E25AC367F800A7,
	ConnectAndJoinRandomLb_OnCustomAuthenticationResponse_m8F6E456A2C572923AD8E720088C22588A1DB1459,
	ConnectAndJoinRandomLb_OnCustomAuthenticationFailed_m447EA30CA679C12865C767F01D060A3A2EF74C5B,
	ConnectAndJoinRandomLb_OnRegionListReceived_m6F54D1CDC5C51EF9ED8C44E439E83210851CDA8E,
	ConnectAndJoinRandomLb_OnRoomListUpdate_m91AEF2D4CB5CC06810FED71B26B736DDC2938DA0,
	ConnectAndJoinRandomLb_OnLobbyStatisticsUpdate_m02972ED26C4B5FCECE1E7A0B482A32E53363F2F7,
	ConnectAndJoinRandomLb_OnJoinedLobby_m171BA1094BCA0FC7A1D13EA07C858CD2D2F423EF,
	ConnectAndJoinRandomLb_OnLeftLobby_m1BCA515302514D0EF88687D2216D67A9D724B0F4,
	ConnectAndJoinRandomLb_OnFriendListUpdate_m6E1EBEFEC84BA9FFA5296B547431BDB692E053DB,
	ConnectAndJoinRandomLb_OnCreatedRoom_mB1906ADF16C593829B0DD191F319716A97B10583,
	ConnectAndJoinRandomLb_OnCreateRoomFailed_mD11765F8AA8A45B8AC5555207281A43ABA829D24,
	ConnectAndJoinRandomLb_OnJoinedRoom_mCE9BDE69A13EA84E9ACA73E064A695B9D3754E8B,
	ConnectAndJoinRandomLb_OnJoinRoomFailed_m5622896774D36D4960735E32F353869D9D6DF94C,
	ConnectAndJoinRandomLb_OnJoinRandomFailed_m4BCA35CBE444C5AD28D7D713E92E893198F91B69,
	ConnectAndJoinRandomLb_OnLeftRoom_m488901B35215F9B70BACC7A08ABB123F3B605137,
	ConnectAndJoinRandomLb_OnRegionPingCompleted_mD76EE8F39A7AE8EF7E071B9E1C5D1A52452FEF17,
	ConnectAndJoinRandomLb__ctor_mD06217DD59635BD5DC39E8E14274D6604FDF85C1,
	AppSettingsExtensions_GetChatSettings_m32D1D0AF37E161CF26E9180F659F6657F6F24D00,
	ChannelSelector_SetChannel_m29F99876C0AF817AEFBDB5539DF70F23558D2A7E,
	ChannelSelector_OnPointerClick_mA5A9AB30717F59D434855F47CAF95543C7A64B9C,
	ChannelSelector__ctor_mB3DF2FA2ED74E01976D4D3ACFB50CB096FEF18F0,
	ChatAppIdCheckerUI_Update_m1D3579AE4EE35E468A353AA67EBD5D9F7C87E2E9,
	ChatAppIdCheckerUI__ctor_mA1D182C1AED55533944CD356D13AA447574B7870,
	ChatGui_get_UserName_mE13C9B7C725EC8F90975DE2D4ED7F3CC28596ACF,
	ChatGui_set_UserName_m8955E5E94B1B8469E4FCD87D10041F53A80E9E0B,
	ChatGui_Start_m9C36C012AF6740959AD6855FCE3C7156D42B636A,
	ChatGui_Connect_mF3437B3F3E848B574D498715B4FB45CBB8A769CD,
	ChatGui_OnDestroy_mA771028772B2BE041F6A8611CB67E6E369654D8E,
	ChatGui_OnApplicationQuit_mCF2A69BA18FF51991BB0F7F5A6898B9C8D19B099,
	ChatGui_Update_mF777D28857ABC30C851D6DD32B3A36BBE7880958,
	ChatGui_OnEnterSend_mD1E43BC5C5ED3F65C8287C7CCB2D09BE0E508375,
	ChatGui_OnClickSend_mA34A7BDF93AF080B0926A4D503AC77035C6345D7,
	ChatGui_SendChatMessage_mC76BD609B9B8E024027BD69E1915E4577B65E76F,
	ChatGui_PostHelpToCurrentChannel_m14AD0594316C6C38A2B85F390B52595A1CF8FD17,
	ChatGui_DebugReturn_m7E7552B5CD94F91CE5C41CD7813EDE70B6176567,
	ChatGui_OnConnected_m63335F6ADB99CA4C1DBD70CE3D3BE669DE0E53B9,
	ChatGui_OnDisconnected_m4AAE28BB7B47BAD132651AFC8E27F573A36E3AFF,
	ChatGui_OnChatStateChange_m166507E98B7F98C6E52223B9404BD384411F8BEA,
	ChatGui_OnSubscribed_m52519890868FA6E484E27F5E39B02833EB54DDB3,
	ChatGui_OnSubscribed_mDDD167FAC1109BBC42B8C8C9772AC321D52A5FE4,
	ChatGui_InstantiateChannelButton_mC727F0E0569BAB8AA5DAC9B2EF8A95D49A4A5032,
	ChatGui_InstantiateFriendButton_m24C82847C192124605436330992D637C8A3E7EFB,
	ChatGui_OnUnsubscribed_m968F1850ADFE61F878F461E87AF20857A0EE1B94,
	ChatGui_OnGetMessages_mE3CAA812F933B7B5C8B4277B56988B18343CEB07,
	ChatGui_OnPrivateMessage_mB6EF6D8710F673B8D9B6403F3AF1D13EFEB9269F,
	ChatGui_OnStatusUpdate_mDE1FAF6055E1758AB9AF5D7B772F9FEA94DB3BDB,
	ChatGui_OnUserSubscribed_m2E2516927F409CEF9D5FB44497AD6B113222409A,
	ChatGui_OnUserUnsubscribed_m2BC2DFEA285C488D13166EBA5698B72550F78654,
	ChatGui_OnChannelPropertiesChanged_mFF2EFCD0C671520155F17F256257E821E51D1926,
	ChatGui_OnUserPropertiesChanged_m5B5A07B865481CAC5E65D4C28AD060C27EC37DD5,
	ChatGui_OnErrorInfo_m1EA237F6DAE99E2F52D45C421B2AB1DCD90E4673,
	ChatGui_AddMessageToSelectedChannel_m4E2AF559DB50F8C53DA41268C1175E09AA471A1C,
	ChatGui_ShowChannel_m696F566CA7F08601752BB3E3508AFA025AC587C3,
	ChatGui_OpenDashboard_mEF6A4AC58094283E6942489BED251351E305A01E,
	ChatGui__ctor_mB1A092C06A5A45E3A7A337A023E08878833B120F,
	ChatGui__cctor_m7B39238B5EAA662C1906D002E4FB09E9EB0F7100,
	FriendItem_set_FriendId_m8359A1EA2AC4624D7D288C1DDC2350975623F7CD,
	FriendItem_get_FriendId_mD5028C5F9C1D12F917261C119C45684B3E4B544D,
	FriendItem_Awake_mDEBF0CC81C911D57EBD8690494E68E9F88ED6F4D,
	FriendItem_OnFriendStatusUpdate_m00D8E4A7CFFF6F81A81ACCB5EDFC1142EEAC5718,
	FriendItem__ctor_mECF93E0BF74E35EB73BF307FFC9DB30D77343F38,
	IgnoreUiRaycastWhenInactive_IsRaycastLocationValid_m677C47DFEBF604215800A9B741A3300BD0A3947D,
	IgnoreUiRaycastWhenInactive__ctor_m710699723A0DE8CB36160AACC822FF3198C5F009,
	NamePickGui_Start_m261A0D587E971FB69F2A604F924BFACE3431E964,
	NamePickGui_EndEditOnEnter_m9CBE79633FA01A8D57125C5DEB62629B95C98755,
	NamePickGui_StartChat_m956F5E92C89AB50B4590BC5023FC40023530002E,
	NamePickGui__ctor_m2AB2D19C88F5FAFCB399BB58B98BD25B50F19881,
	EventSystemSpawner_OnEnable_m92046CF7C2B99309677158A5205E37FA11EE9302,
	EventSystemSpawner__ctor_m6FCC0DAF89825B22BEDC803B5E02B42FB86CF331,
	OnStartDelete_Start_m033082A59BA5941F5740FAD3CDD2D4501430DD33,
	OnStartDelete__ctor_m0B816BC930476E7580303D8E9E8E07444F0C5270,
	TextButtonTransition_Awake_m5402B2656DCEF5F78D8BA1D3D7D77EBD7F07AC9E,
	TextButtonTransition_OnEnable_mD3229078DB78111EDC0B823872D1E381F3C15A5F,
	TextButtonTransition_OnDisable_mAB2CCC24F8B8F1D664154DC1D0D7E47A925A3B72,
	TextButtonTransition_OnPointerEnter_m28E7599E058554E4C13D7EA152521BCF4C104B07,
	TextButtonTransition_OnPointerExit_m25F271B30D439D9AD93483E1CC08FC198A3E9A02,
	TextButtonTransition__ctor_m9E3AFEA1BEFD79F8F0D16DA749243B7D43B78570,
	TextToggleIsOnTransition_OnEnable_mA240AFB3178800D5275FB5BD05D54449EC49BAF2,
	TextToggleIsOnTransition_OnDisable_m824A3816D8873B0A5F86F4F401FF080163560404,
	TextToggleIsOnTransition_OnValueChanged_m2C3DAB9655DD596BFC184D0A5B4F83D36C8B2FFB,
	TextToggleIsOnTransition_OnPointerEnter_m01E7951E3FE9C9C9A1FC1D5E26EFF4041799E0E5,
	TextToggleIsOnTransition_OnPointerExit_mBF6DDD1410475F0C6118F9C85F572E669FBB4C20,
	TextToggleIsOnTransition__ctor_mA401C98E0CC8F9B8728B46138CCF941C6E471F04,
	YesNoDialog_OnStart_m15C0C305AF068DACFE29DC2BD62E4D60982176FD,
	YesNoDialog_OnUpdate_m9C6B1630E4E29EAEABEA0402ACFB40B59C294280,
	YesNoDialog_OnClose_mA1099FF95DF9E68D0BC78C35B91062C58B0F1991,
	YesNoDialog_OnResponse_mACC597AB7B60C065A4009A8003544B9D0CECC79E,
	YesNoDialog__ctor_m569321A952C47C78B7C782DA0751989B5F5FC9AD,
	Water_OnWillRenderObject_m030DC7B6995FEC2690883CAA454A4A72C0F5BD54,
	Water_OnDisable_mBC5924D74718A4A22BA7AFA98296F9E0247F6FFD,
	Water_Update_m67CF13DF8C862994509BF70D888E84C4CEB8BD84,
	Water_UpdateCameraModes_m5A9B5A17F3C6714DC1E67305196CA9C477947011,
	Water_CreateWaterObjects_m759D0CAE5A10475C04C250DF826FC862BDF6A207,
	Water_GetWaterMode_mFF2310EBA5942F0B15B55B884F1923AF390F258D,
	Water_FindHardwareWaterSupport_mCC07F7400748514F5A89E92835812E047D763789,
	Water_CameraSpacePlane_mDE8B72357B143694C254E3411D4A36DB272F1CD2,
	Water_CalculateReflectionMatrix_mD27C10A49F7964539E94CBAC003DA65EA42003C0,
	Water__ctor_mF11768BB5C88F59E159BDDA9D4713A95BCDC7EDC,
	Displace_Awake_mE99DEB129DED3AF1991C408D98A52E342B4643A7,
	Displace_OnEnable_m952A8DDD7BF19B262520923CACCAE21927044BD8,
	Displace_OnDisable_m344E8D36AD672D92E465CD50F28E512A817B8B08,
	Displace__ctor_mA627066AA526C79EF3AA8D4FCDF70261CEA5CFA8,
	GerstnerDisplace__ctor_m718281CC56902E6BF82FAC236AB3043BFA0E9BFF,
	MeshContainer__ctor_mF1E5D7C018B2F9F756CECB8C1A0497406F833E3B,
	MeshContainer_Update_mA307DB7261AAD0254EBC0013D2DB7E30C12B94CA,
	PlanarReflection_Start_mF4B62F45289EDE1798379F46BB22090CFE446874,
	PlanarReflection_CreateReflectionCameraFor_m536231E7168798D1B53C3C69B4C3CE24F96C1734,
	PlanarReflection_SetStandardCameraParameter_m18039B2FBF79EDCC215634AA15719B070D665DF9,
	PlanarReflection_CreateTextureFor_m5CC4B46CAFD4C2B62F6B2E90A3DDC566F82B73AF,
	PlanarReflection_RenderHelpCameras_m8EC47193F6B3B38703D2FC5FC26A69115FB93241,
	PlanarReflection_LateUpdate_m67497232C28C05149AAA4EC85A3CCAB96D4F259C,
	PlanarReflection_WaterTileBeingRendered_mAD0310016829E5BB8BAE9F0313A4654C4C7039C1,
	PlanarReflection_OnEnable_m08B218FDAEA9D8E040927BEF9E011202783CC281,
	PlanarReflection_OnDisable_mE5DCEA557577A4B4CD3790829AF59BE81E4CAF36,
	PlanarReflection_RenderReflectionFor_m743FA341870F000B1626F691C9DED6E3714D5987,
	PlanarReflection_SaneCameraSettings_m73A5C561F9B670BC714B3A4B5B6722BF8AD30C1E,
	PlanarReflection_CalculateObliqueMatrix_mFA8D1838E4D40288C73A944485471F15A9EE13AB,
	PlanarReflection_CalculateReflectionMatrix_mC0F530AB64BBB6ADCAF28DD8E014D9388CEF9271,
	PlanarReflection_Sgn_m2C181CAD6E9DADF5B8F618D2D01649A826F8939A,
	PlanarReflection_CameraSpacePlane_mBBA681992D91FE0286F9694E11D44BFE640A698B,
	PlanarReflection__ctor_m8188E385A4C4CE7AB26873A83DB4377EA390A66F,
	SpecularLighting_Start_mFE455D641AAD002581EA31536A30C2F01B650CEE,
	SpecularLighting_Update_mC376C1CE2ECA4F4121AFE286F1F863EAF07720B1,
	SpecularLighting__ctor_mA0D46E411DF992BD42BCA24875516A8A5A2AAB3E,
	WaterBase_UpdateShader_mF024410B93ACDEEB86F2784367D4371789A8ECC6,
	WaterBase_WaterTileBeingRendered_m5AE3A512DBA0F49C5480DC7AC0832C69DA2007D6,
	WaterBase_Update_m97AB1D3EC79F89868388DEE04B9C12AC7C4F516C,
	WaterBase__ctor_mEF380720D189D0DF1DE0F40ACB847C00D12362CC,
	WaterTile_Start_m59B733629B793EA675F0934D0283148809B2D944,
	WaterTile_AcquireComponents_mFD2764B5DDB1CC33CA9AF55BB0D1D570EC92AD0B,
	WaterTile_OnWillRenderObject_m003657E9F9D7BBC9A618E204B3769E6F849C31E9,
	WaterTile__ctor_m5B94DD8133797CA9BF11522024360A0A1049F5E5,
};
static const int32_t s_InvokerIndices[248] = 
{
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2448,
	2434,
	2434,
	2975,
	2975,
	2434,
	2434,
	2975,
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2484,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2941,
	2484,
	2448,
	2975,
	2941,
	837,
	2941,
	1029,
	1741,
	2912,
	1466,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2912,
	2448,
	931,
	2912,
	2448,
	2912,
	4016,
	4408,
	3899,
	3899,
	4408,
	4563,
	3701,
	2448,
	2912,
	2975,
	2975,
	2941,
	2912,
	2975,
	2975,
	2975,
	2975,
	2434,
	2448,
	2448,
	2448,
	2448,
	2448,
	2975,
	2975,
	2448,
	2975,
	1227,
	2975,
	1227,
	1227,
	2975,
	2448,
	2975,
	4481,
	2448,
	2448,
	2975,
	2975,
	2975,
	2912,
	2448,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2975,
	1466,
	2975,
	2975,
	2434,
	1444,
	931,
	2448,
	2448,
	2448,
	931,
	931,
	611,
	1444,
	1444,
	931,
	627,
	931,
	2448,
	2448,
	2975,
	2975,
	4649,
	2448,
	2912,
	2975,
	892,
	2975,
	1173,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	2975,
	2975,
	2975,
	2477,
	2448,
	2448,
	2975,
	2975,
	2897,
	2448,
	2434,
	2975,
	2975,
	2975,
	2975,
	1444,
	906,
	2897,
	2897,
	544,
	4235,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2448,
	2975,
	2975,
	1898,
	1442,
	1898,
	2448,
	2975,
	1444,
	2975,
	2975,
	1444,
	2448,
	4045,
	4045,
	4534,
	544,
	2975,
	2975,
	2975,
	2975,
	2975,
	1444,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	248,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
