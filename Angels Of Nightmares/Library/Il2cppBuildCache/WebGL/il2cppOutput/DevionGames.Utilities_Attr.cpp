﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.InspectorLabelAttribute::label
	String_t* ___label_0;
	// System.String DevionGames.InspectorLabelAttribute::tooltip
	String_t* ___tooltip_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_tooltip_1() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___tooltip_1)); }
	inline String_t* get_tooltip_1() const { return ___tooltip_1; }
	inline String_t** get_address_of_tooltip_1() { return &___tooltip_1; }
	inline void set_tooltip_1(String_t* value)
	{
		___tooltip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
// System.Void DevionGames.EnumFlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818 (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * __this, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
static void DevionGames_Utilities_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
}
static void WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_GetBaseType_mE6ACF7E58CB0D935D01E896D429EAF4B58DDBDEF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_GetAssembly_m3CAE53A6F8F7706260CC43FDCB1B8EA3FA795D6E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_IsValueType_m9F0ED94D95D39AE9356387E07522334ABBE0D477(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_IsGenericType_mC31B6B3F7CA5148D1A5895401883948F5FBF1619(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_EventName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_Argument(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_BoolValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_IntValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_FloatValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_StringValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_Vector2Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_Vector3Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ColorValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ObjectValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ArgumentType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CategoryAttribute_t00D1E9A26C30FB014666758E8314D3D4BAC21D80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void ExcludeFromCreation_tA13B8A2EE5025B384CD5005322E7054B1F8AA53A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 4LL, NULL);
	}
}
static void AudioEventListener_t8193DCABDC5CCC32F0C399F9E5B77FC3C28C35BF_CustomAttributesCacheGenerator_m_AudioGroups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioGroup_t681CD4CCAE050210175606283FE625757D83EDEB_CustomAttributesCacheGenerator_m_AudioMixerGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t558253EC4F9509BDBBD5D70FD9DD790A82880908_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioPlaylist_t27C69D58F419717F2DD786B03F144094A4CBBF7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x50\x6C\x61\x79\x6C\x69\x73\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x55\x74\x69\x6C\x69\x74\x69\x65\x73\x2F\x41\x75\x64\x69\x6F\x20\x50\x6C\x61\x79\x6C\x69\x73\x74"), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void AudioPlaylist_t27C69D58F419717F2DD786B03F144094A4CBBF7A_CustomAttributesCacheGenerator_m_Clips(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Blackboard_tD41236197B376285B11AE6FA9179ADF57A2D0CC5_CustomAttributesCacheGenerator_m_Variables(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_tF86B9ACE4D6CA085CC105EF6084B086BF5B5354A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t46E2B26FF6EDE67E297EC6A65A56A3EA9B1B15FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoolVariable_t964482B5AFA3726CA4CDA3BCF117E8C3B7621539_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorVariable_t8BA926ABCC9FF08D374F626078BB071FCDCB58E2_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FloatVariable_t2F4C56D55824729E8C2C98F672CE9F46A6B9BCB6_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameObjectVariable_t2739B8C8BC52D87CD32E1F493D0C45B3D65F6FF6_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IntVariable_tF3B8C1DFAFDCA60549A92F2E2BD5A2EE65B79AF2_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectVariable_t242A31C6A0BC2CD9AD5EDB748F0A64249187C949_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StringVariable_t2B9ED31907D05A092003CD7501F880C6B5F338AC_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tCAEB8BBE7316824FA6B253F03EF08B6F7D752933_CustomAttributesCacheGenerator_m_Name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Variable_tCAEB8BBE7316824FA6B253F03EF08B6F7D752933_CustomAttributesCacheGenerator_m_IsShared(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Vector2Variable_tD75D3D8D2722C6AC255CE28D5C01A76CB8078C06_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Vector3Variable_t88B0AF5CFE9BC79A1C25E8B081BF97C34CBF4C46_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D_CustomAttributesCacheGenerator_delegates(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CoroutineQueue_tFD678B571DCE425A59370C9D66F3F57ECA82955E_CustomAttributesCacheGenerator_CoroutineQueue_Process_mA4F1B1CA22EBA0AC12050CE9D6CB58193071F76D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_0_0_0_var), NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7__ctor_mA35CD4DE0A2227D3286F26FD712B83DABCFEBFA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_IDisposable_Dispose_m2839E313A558FE75EC388F0735F6E5BB247B5CBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED87547CE0E935996285759923C65E910ADC90DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_IEnumerator_Reset_m70CD4B91FCDAAF12A0674773E8D497436327CF60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_IEnumerator_get_Current_m2B21F63871756331688DBDD7C68F246498644548(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LookAtMainCamera_t1E79C5AFD390608513C184133CB999E5A81C7431_CustomAttributesCacheGenerator_LookAtMainCamera_SearchCamera_m6FA58F13F9317393645F50E5496431BE77B0993B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_0_0_0_var), NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6__ctor_m05514C886D4BDBB39C51CAF3FA8ABD053B9F8EAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_IDisposable_Dispose_m18B8EAE4684219FCB4D38506E56021D26E4BD8CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88445D17B8BB4050E4C069580E8637BD7971F47A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_Reset_m9E922F1F4C94E1398E5CFD22EF6A07F03A9BF7C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_get_Current_mFD0288E149200F0FD3B5715E4E00F236506B7782(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_LookAtCameraForward(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_AutoDestruct(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_DestructDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_m_Tag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_m_Offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_Name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_Description(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_VariableType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectProperty_t884CE4F2057507B28EB34C3673DEEC6468645B01_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectProperty_t884CE4F2057507B28EB34C3673DEEC6468645B01_CustomAttributesCacheGenerator_typeIndex(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_AudioClip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_AudioMixerGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_Volume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_Delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_PlayAudioClip_Start_m76002B53FCC6F1E3B598DE5FF7898B38593FE0B2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mEF45D7D81D28D0AD72ADE751ADF86D4F214D0CEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m36730405D9679212EDBA4DF350C053A8EF8B4062(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m862A8A274013AD886436DFF6E0E832D3CDF8584F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m933C41702153F8200693D84ACFA4E211767628BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA428A2D99B93A9A2EE31DBF3B6027A222715320D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Source(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Execution(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Interval(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_PropertyBinding_IntervalUpdate_mF7DB2D8DB3B459788E759EED9F73261399142692(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_0_0_0_var), NULL);
	}
}
static void PropertyRef_tB3E458D7016833AA73CCD1A5763E484F775950B3_CustomAttributesCacheGenerator_m_Component(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PropertyRef_tB3E458D7016833AA73CCD1A5763E484F775950B3_CustomAttributesCacheGenerator_m_PropertyPath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8__ctor_mE569C36E5154F28C9464783AB286F1E5D95562AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_IDisposable_Dispose_m938B36B9E9E68F9FE94E5F6031E53984C29619D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69063FDBF5B4831E0C85C71F09F5244D00998219(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_Reset_m158FF84C726C4B2D5EE0C60A67E4A41B6595A38C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_get_Current_mEA80CCCA18AB0E4401F2CC817C77857E79112045(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SaveUIComponent_t90AACB04362133EAB2AD1BA050F659FE48D33C20_CustomAttributesCacheGenerator_key(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SaveUIComponent_t90AACB04362133EAB2AD1BA050F659FE48D33C20_CustomAttributesCacheGenerator_target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectableObjectName_tDF07B0FBA32EB2BA1FF11737E64C778897CE607C_CustomAttributesCacheGenerator_m_ObjectName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_selectionType(CustomAttributesCache* cache)
{
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[0];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_SelectionDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_SelectionKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79"), NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_RaycastOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_LayerMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_deselectionType(CustomAttributesCache* cache)
{
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[0];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_DeselectionKey(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_DeselectionDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[1];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void SelectionInputType_t0FF634A7E6E0C5D302851777E0F7B33A432839B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void DeselectionInputType_t3A210E11A94493CDCAD9F3DF3ED73EE7A126DB7E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CU3Ec_t8D29BF82BDF018B419E2C9C653EF8C8C2D912831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleMinimap_tBCA10AE78E93ECCC469E82191F238087E3C52035_CustomAttributesCacheGenerator_m_PlayerTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleMinimap_tBCA10AE78E93ECCC469E82191F238087E3C52035_CustomAttributesCacheGenerator_m_RotateWithPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tCBD44BF3AF72DBE3BDD78BC4967772B36C8D83F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_m_Delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_m_OnDestroy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_TimedDestroy_Start_mEE92664FA2142DA2498F6CA5D17A402CB0EF473D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m7225B152290395438805F0521BA532F23E6B00E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mAE86168E862DE97101A48AFA3587AADA5E43F452(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD36C0E919D3C31F79BBCC4389881C2BED18A196C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mE031A47C4C3F5D56B6551FCC569394B5E232A9CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m3E74EBF644324A5D42FAF8353F61C09FC047608A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Combonent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Enable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_TimedEnable_WaitAndSetEnabled_m5518E3F9CECA33EB77D46E4D85B003C30614045C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_0_0_0_var), NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4__ctor_mC300D871D35BC85F42A31B653EC7EB79ECC920CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_IDisposable_Dispose_mA75D4D3AC769C6B475A840A7950BBBC00120B27F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB87EB7D92D24427B6B9AFDACCC861D3682416F68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_Reset_m1A49C73502745B49FD532606CA5CFFCA5E644573(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_get_Current_m06302F4BD4A6E352124BFED7190F24F7B4069423(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_FindChild_m3A4D6BC66B989B7F4BE684488D74F9EBF3E1DB97(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_Stretch_mED921D4043429CE5C026BEE8BFCCD0F7F3730C0D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_Stretch_mB9ECC09B408195B08506E68C49F33FCEC9B91464(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_HasFlag_m2E5FDF62C8E7EBEB9B4B1E2FFBC000C0F9C2EB5B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetAllMethods_mA26E4013A1C68B62ED790F49F6C0D3DBBAAE9D21(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetSerializedField_m3DFCAF7A79CDB3857A8B3B453E1AE088F665408A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetSerializedFields_m1D6F2A8FD044D8FF49D5FD9B27E0AB8BA0B2DCB8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_BaseTypesAndSelf_mAD2EBB949D685E2D62DED747B3900570BAAD4FF7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[1];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_0_0_0_var), NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_BaseTypes_mD100E7396D2DA9A100981F3005331402805CB914(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[1];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_0_0_0_var), NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetCustomAttributes_m970226B2F710129950C216ADC49BBED958C7A84C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetCustomAttribute_mA5B351D3AA1F71C75B9126D1306E64127D6470E8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_HasAttribute_mE0D9B248AC3A20581FBB03BFBB7601A882371ADF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_HasAttribute_mD6E30A724B936B8A733B759E7BB8644DB2DF7637(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_Contains_m5D2130FBCFC37784529F87A9FB57C0113DE69999(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec_tE32FD87EA82337433CF77822E5F378235DE98BEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t22966FEA1B190D0A07CB414205ED6AF56E2FB238_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12__ctor_mC16CA045DB26529930070E385A119F5FC28687D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_IDisposable_Dispose_m266119160E53905B8BA612E023AEBDC2F1B800E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m18C4E6212AD8F107EBAC79DAD65CA1156E82DAC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_Reset_m93A48609980842C0527D5EF5AEDBF49ECC60484C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_get_Current_m632B11DDF786C85B0002A8C4916C13FF49A1F7C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m0E1C888F34B391FF5A69ED92DF7FADA9AFFD611F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m07AA84023DF74C6E52C7DA01FC413312FA9B7F8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13__ctor_mE5EFE42FB994CC40FAA0D05432A1D4E87D246034(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_IDisposable_Dispose_m90A8E9CA4A62940322702B943C802246F7198F07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m478F8E1330BFB7CD178E7001520F4E03ACBC54C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_Reset_m656B190DF87DC9078C1AD524603995BF40E749D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_get_Current_m989A6A05C7EFB1780FE9588AE15B7020A3D3DDB1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mFF576A29276D4CBABC0891FB8734F1D5AE81B00A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB6B2F041696200200F5DEA07D880E2F7C57DA539(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_Utilities_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_Utilities_AttributeGenerators[161] = 
{
	WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator,
	CategoryAttribute_t00D1E9A26C30FB014666758E8314D3D4BAC21D80_CustomAttributesCacheGenerator,
	ExcludeFromCreation_tA13B8A2EE5025B384CD5005322E7054B1F8AA53A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t558253EC4F9509BDBBD5D70FD9DD790A82880908_CustomAttributesCacheGenerator,
	AudioPlaylist_t27C69D58F419717F2DD786B03F144094A4CBBF7A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_tF86B9ACE4D6CA085CC105EF6084B086BF5B5354A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t46E2B26FF6EDE67E297EC6A65A56A3EA9B1B15FD_CustomAttributesCacheGenerator,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator,
	SelectionInputType_t0FF634A7E6E0C5D302851777E0F7B33A432839B9_CustomAttributesCacheGenerator,
	DeselectionInputType_t3A210E11A94493CDCAD9F3DF3ED73EE7A126DB7E_CustomAttributesCacheGenerator,
	U3CU3Ec_t8D29BF82BDF018B419E2C9C653EF8C8C2D912831_CustomAttributesCacheGenerator,
	U3CU3Ec_tCBD44BF3AF72DBE3BDD78BC4967772B36C8D83F1_CustomAttributesCacheGenerator,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator,
	UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator,
	U3CU3Ec_tE32FD87EA82337433CF77822E5F378235DE98BEB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t22966FEA1B190D0A07CB414205ED6AF56E2FB238_CustomAttributesCacheGenerator,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator,
	AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_Type,
	AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_EventName,
	AnimationEventSender_t87A846AF79E5B7300952B52EBB3325575EA0018D_CustomAttributesCacheGenerator_m_Argument,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_BoolValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_IntValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_FloatValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_StringValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_Vector2Value,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_Vector3Value,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ColorValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ObjectValue,
	ArgumentVariable_tC65800A23AD06763917E26C22055F90E3253F3AB_CustomAttributesCacheGenerator_m_ArgumentType,
	AudioEventListener_t8193DCABDC5CCC32F0C399F9E5B77FC3C28C35BF_CustomAttributesCacheGenerator_m_AudioGroups,
	AudioGroup_t681CD4CCAE050210175606283FE625757D83EDEB_CustomAttributesCacheGenerator_m_AudioMixerGroup,
	AudioPlaylist_t27C69D58F419717F2DD786B03F144094A4CBBF7A_CustomAttributesCacheGenerator_m_Clips,
	Blackboard_tD41236197B376285B11AE6FA9179ADF57A2D0CC5_CustomAttributesCacheGenerator_m_Variables,
	BoolVariable_t964482B5AFA3726CA4CDA3BCF117E8C3B7621539_CustomAttributesCacheGenerator_m_Value,
	ColorVariable_t8BA926ABCC9FF08D374F626078BB071FCDCB58E2_CustomAttributesCacheGenerator_m_Value,
	FloatVariable_t2F4C56D55824729E8C2C98F672CE9F46A6B9BCB6_CustomAttributesCacheGenerator_m_Value,
	GameObjectVariable_t2739B8C8BC52D87CD32E1F493D0C45B3D65F6FF6_CustomAttributesCacheGenerator_m_Value,
	IntVariable_tF3B8C1DFAFDCA60549A92F2E2BD5A2EE65B79AF2_CustomAttributesCacheGenerator_m_Value,
	ObjectVariable_t242A31C6A0BC2CD9AD5EDB748F0A64249187C949_CustomAttributesCacheGenerator_m_Value,
	StringVariable_t2B9ED31907D05A092003CD7501F880C6B5F338AC_CustomAttributesCacheGenerator_m_Value,
	Variable_tCAEB8BBE7316824FA6B253F03EF08B6F7D752933_CustomAttributesCacheGenerator_m_Name,
	Variable_tCAEB8BBE7316824FA6B253F03EF08B6F7D752933_CustomAttributesCacheGenerator_m_IsShared,
	Vector2Variable_tD75D3D8D2722C6AC255CE28D5C01A76CB8078C06_CustomAttributesCacheGenerator_m_Value,
	Vector3Variable_t88B0AF5CFE9BC79A1C25E8B081BF97C34CBF4C46_CustomAttributesCacheGenerator_m_Value,
	CallbackHandler_t4D826810B08DFBEA873355C230791E30672D0D2D_CustomAttributesCacheGenerator_delegates,
	MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_Speed,
	MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_LookAtCameraForward,
	MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_AutoDestruct,
	MoveForward_t8995C7FB46C2AE60F113073C234AB8298C6F58A9_CustomAttributesCacheGenerator_m_DestructDelay,
	MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_m_Tag,
	MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_speed,
	MoveTo_t57ACEF3562C657056E5E733E14660997B3ABAF2A_CustomAttributesCacheGenerator_m_Offset,
	NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_Name,
	NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_Description,
	NamedVariable_tD02FC658232561CD984A1921D84FFD6774C5E760_CustomAttributesCacheGenerator_m_VariableType,
	ObjectProperty_t884CE4F2057507B28EB34C3673DEEC6468645B01_CustomAttributesCacheGenerator_name,
	ObjectProperty_t884CE4F2057507B28EB34C3673DEEC6468645B01_CustomAttributesCacheGenerator_typeIndex,
	PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_AudioClip,
	PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_AudioMixerGroup,
	PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_Volume,
	PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_m_Delay,
	PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Source,
	PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Target,
	PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Execution,
	PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_m_Interval,
	PropertyRef_tB3E458D7016833AA73CCD1A5763E484F775950B3_CustomAttributesCacheGenerator_m_Component,
	PropertyRef_tB3E458D7016833AA73CCD1A5763E484F775950B3_CustomAttributesCacheGenerator_m_PropertyPath,
	SaveUIComponent_t90AACB04362133EAB2AD1BA050F659FE48D33C20_CustomAttributesCacheGenerator_key,
	SaveUIComponent_t90AACB04362133EAB2AD1BA050F659FE48D33C20_CustomAttributesCacheGenerator_target,
	SelectableObjectName_tDF07B0FBA32EB2BA1FF11737E64C778897CE607C_CustomAttributesCacheGenerator_m_ObjectName,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_selectionType,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_SelectionDistance,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_SelectionKey,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_RaycastOffset,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_LayerMask,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_deselectionType,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_DeselectionKey,
	SelectionHandler_t3033C6930807E4F92D8DECC772A59B38DBEBF9C6_CustomAttributesCacheGenerator_m_DeselectionDistance,
	SimpleMinimap_tBCA10AE78E93ECCC469E82191F238087E3C52035_CustomAttributesCacheGenerator_m_PlayerTag,
	SimpleMinimap_tBCA10AE78E93ECCC469E82191F238087E3C52035_CustomAttributesCacheGenerator_m_RotateWithPlayer,
	TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_m_Delay,
	TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_m_OnDestroy,
	TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Delay,
	TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Combonent,
	TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_m_Enable,
	WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_GetBaseType_mE6ACF7E58CB0D935D01E896D429EAF4B58DDBDEF,
	WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_GetAssembly_m3CAE53A6F8F7706260CC43FDCB1B8EA3FA795D6E,
	WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_IsValueType_m9F0ED94D95D39AE9356387E07522334ABBE0D477,
	WindowsRuntimeExtension_t08D5124450F4C5B4756DB960BB55625DC868A564_CustomAttributesCacheGenerator_WindowsRuntimeExtension_IsGenericType_mC31B6B3F7CA5148D1A5895401883948F5FBF1619,
	CoroutineQueue_tFD678B571DCE425A59370C9D66F3F57ECA82955E_CustomAttributesCacheGenerator_CoroutineQueue_Process_mA4F1B1CA22EBA0AC12050CE9D6CB58193071F76D,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7__ctor_mA35CD4DE0A2227D3286F26FD712B83DABCFEBFA3,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_IDisposable_Dispose_m2839E313A558FE75EC388F0735F6E5BB247B5CBF,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED87547CE0E935996285759923C65E910ADC90DC,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_IEnumerator_Reset_m70CD4B91FCDAAF12A0674773E8D497436327CF60,
	U3CProcessU3Ed__7_t1A294C59CE318D40C43CEF9F02E600DD7A22F7E1_CustomAttributesCacheGenerator_U3CProcessU3Ed__7_System_Collections_IEnumerator_get_Current_m2B21F63871756331688DBDD7C68F246498644548,
	LookAtMainCamera_t1E79C5AFD390608513C184133CB999E5A81C7431_CustomAttributesCacheGenerator_LookAtMainCamera_SearchCamera_m6FA58F13F9317393645F50E5496431BE77B0993B,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6__ctor_m05514C886D4BDBB39C51CAF3FA8ABD053B9F8EAC,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_IDisposable_Dispose_m18B8EAE4684219FCB4D38506E56021D26E4BD8CE,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88445D17B8BB4050E4C069580E8637BD7971F47A,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_Reset_m9E922F1F4C94E1398E5CFD22EF6A07F03A9BF7C8,
	U3CSearchCameraU3Ed__6_tC8CCEC67CB7C64E93666ECB0E70FB882B0BD8BEC_CustomAttributesCacheGenerator_U3CSearchCameraU3Ed__6_System_Collections_IEnumerator_get_Current_mFD0288E149200F0FD3B5715E4E00F236506B7782,
	PlayAudioClip_t1CC35AF839309999B56F053FF8B471D00CC95208_CustomAttributesCacheGenerator_PlayAudioClip_Start_m76002B53FCC6F1E3B598DE5FF7898B38593FE0B2,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mEF45D7D81D28D0AD72ADE751ADF86D4F214D0CEE,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m36730405D9679212EDBA4DF350C053A8EF8B4062,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m862A8A274013AD886436DFF6E0E832D3CDF8584F,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m933C41702153F8200693D84ACFA4E211767628BA,
	U3CStartU3Ed__4_t7FE10656C54E9311C647740690F1C0E95CDC7B41_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA428A2D99B93A9A2EE31DBF3B6027A222715320D,
	PropertyBinding_tA893E27B2896F444145F9567B71DCC3B05AB30C0_CustomAttributesCacheGenerator_PropertyBinding_IntervalUpdate_mF7DB2D8DB3B459788E759EED9F73261399142692,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8__ctor_mE569C36E5154F28C9464783AB286F1E5D95562AC,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_IDisposable_Dispose_m938B36B9E9E68F9FE94E5F6031E53984C29619D6,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m69063FDBF5B4831E0C85C71F09F5244D00998219,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_Reset_m158FF84C726C4B2D5EE0C60A67E4A41B6595A38C,
	U3CIntervalUpdateU3Ed__8_t3240ECFF6443DB4F167C1615526EC8AC351C01BB_CustomAttributesCacheGenerator_U3CIntervalUpdateU3Ed__8_System_Collections_IEnumerator_get_Current_mEA80CCCA18AB0E4401F2CC817C77857E79112045,
	TimedDestroy_tD5DEA21E8C738E7F49C86CED8649A6E36F8CBE38_CustomAttributesCacheGenerator_TimedDestroy_Start_mEE92664FA2142DA2498F6CA5D17A402CB0EF473D,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2__ctor_m7225B152290395438805F0521BA532F23E6B00E1,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_IDisposable_Dispose_mAE86168E862DE97101A48AFA3587AADA5E43F452,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD36C0E919D3C31F79BBCC4389881C2BED18A196C,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_mE031A47C4C3F5D56B6551FCC569394B5E232A9CD,
	U3CStartU3Ed__2_tE605C105A69099B7C89D2D11106ED5AE7DFF26C7_CustomAttributesCacheGenerator_U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m3E74EBF644324A5D42FAF8353F61C09FC047608A,
	TimedEnable_tE645653E82C72843D588FF4014E7681FE6312CF7_CustomAttributesCacheGenerator_TimedEnable_WaitAndSetEnabled_m5518E3F9CECA33EB77D46E4D85B003C30614045C,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4__ctor_mC300D871D35BC85F42A31B653EC7EB79ECC920CA,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_IDisposable_Dispose_mA75D4D3AC769C6B475A840A7950BBBC00120B27F,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB87EB7D92D24427B6B9AFDACCC861D3682416F68,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_Reset_m1A49C73502745B49FD532606CA5CFFCA5E644573,
	U3CWaitAndSetEnabledU3Ed__4_t4112EB5478D1B5455BFDFAE4D556EAB35D4DDA03_CustomAttributesCacheGenerator_U3CWaitAndSetEnabledU3Ed__4_System_Collections_IEnumerator_get_Current_m06302F4BD4A6E352124BFED7190F24F7B4069423,
	UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_FindChild_m3A4D6BC66B989B7F4BE684488D74F9EBF3E1DB97,
	UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_Stretch_mED921D4043429CE5C026BEE8BFCCD0F7F3730C0D,
	UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_Stretch_mB9ECC09B408195B08506E68C49F33FCEC9B91464,
	UnityTools_t3CD04D0AEC2BB72E1234885B058FECD168F73D22_CustomAttributesCacheGenerator_UnityTools_HasFlag_m2E5FDF62C8E7EBEB9B4B1E2FFBC000C0F9C2EB5B,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetAllMethods_mA26E4013A1C68B62ED790F49F6C0D3DBBAAE9D21,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetSerializedField_m3DFCAF7A79CDB3857A8B3B453E1AE088F665408A,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetAllSerializedFields_m8AD3F794F5BE9A7E1E670E7919250B6B25767B48,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetSerializedFields_m1D6F2A8FD044D8FF49D5FD9B27E0AB8BA0B2DCB8,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_BaseTypesAndSelf_mAD2EBB949D685E2D62DED747B3900570BAAD4FF7,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_BaseTypes_mD100E7396D2DA9A100981F3005331402805CB914,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetCustomAttributes_m970226B2F710129950C216ADC49BBED958C7A84C,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_GetCustomAttribute_mA5B351D3AA1F71C75B9126D1306E64127D6470E8,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_HasAttribute_mE0D9B248AC3A20581FBB03BFBB7601A882371ADF,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_HasAttribute_mD6E30A724B936B8A733B759E7BB8644DB2DF7637,
	Utility_tF41F600D4C9642D0BF943D997FA9018464E1565D_CustomAttributesCacheGenerator_Utility_Contains_m5D2130FBCFC37784529F87A9FB57C0113DE69999,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12__ctor_mC16CA045DB26529930070E385A119F5FC28687D8,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_IDisposable_Dispose_m266119160E53905B8BA612E023AEBDC2F1B800E0,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m18C4E6212AD8F107EBAC79DAD65CA1156E82DAC1,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_Reset_m93A48609980842C0527D5EF5AEDBF49ECC60484C,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerator_get_Current_m632B11DDF786C85B0002A8C4916C13FF49A1F7C3,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_m0E1C888F34B391FF5A69ED92DF7FADA9AFFD611F,
	U3CBaseTypesAndSelfU3Ed__12_tDF140C62115436A6AC4A46F037F2EA91D6815260_CustomAttributesCacheGenerator_U3CBaseTypesAndSelfU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m07AA84023DF74C6E52C7DA01FC413312FA9B7F8E,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13__ctor_mE5EFE42FB994CC40FAA0D05432A1D4E87D246034,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_IDisposable_Dispose_m90A8E9CA4A62940322702B943C802246F7198F07,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_TypeU3E_get_Current_m478F8E1330BFB7CD178E7001520F4E03ACBC54C7,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_Reset_m656B190DF87DC9078C1AD524603995BF40E749D7,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerator_get_Current_m989A6A05C7EFB1780FE9588AE15B7020A3D3DDB1,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_Generic_IEnumerableU3CSystem_TypeU3E_GetEnumerator_mFF576A29276D4CBABC0891FB8734F1D5AE81B00A,
	U3CBaseTypesU3Ed__13_t4FD2ED19DAF54A62A322C931E14424E2611904DA_CustomAttributesCacheGenerator_U3CBaseTypesU3Ed__13_System_Collections_IEnumerable_GetEnumerator_mB6B2F041696200200F5DEA07D880E2F7C57DA539,
	DevionGames_Utilities_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
