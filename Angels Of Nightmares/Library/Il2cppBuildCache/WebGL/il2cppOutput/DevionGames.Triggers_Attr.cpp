﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.IconAttribute
struct IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426;
// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.ComponentMenu
struct ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String DevionGames.ComponentMenu::m_ComponentMenu
	String_t* ___m_ComponentMenu_0;

public:
	inline static int32_t get_offset_of_m_ComponentMenu_0() { return static_cast<int32_t>(offsetof(ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8, ___m_ComponentMenu_0)); }
	inline String_t* get_m_ComponentMenu_0() const { return ___m_ComponentMenu_0; }
	inline String_t** get_address_of_m_ComponentMenu_0() { return &___m_ComponentMenu_0; }
	inline void set_m_ComponentMenu_0(String_t* value)
	{
		___m_ComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentMenu_0), (void*)value);
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// DevionGames.IconAttribute
struct IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type DevionGames.IconAttribute::type
	Type_t * ___type_0;
	// System.String DevionGames.IconAttribute::path
	String_t* ___path_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_0), (void*)value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_1), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C 
{
public:
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::className
	String_t* ___className_0;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpace
	String_t* ___nameSpace_1;
	// System.String UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assembly
	String_t* ___assembly_2;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::classHasChanged
	bool ___classHasChanged_3;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::nameSpaceHasChanged
	bool ___nameSpaceHasChanged_4;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::assemblyHasChanged
	bool ___assemblyHasChanged_5;
	// System.Boolean UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::autoUdpateAPI
	bool ___autoUdpateAPI_6;

public:
	inline static int32_t get_offset_of_className_0() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___className_0)); }
	inline String_t* get_className_0() const { return ___className_0; }
	inline String_t** get_address_of_className_0() { return &___className_0; }
	inline void set_className_0(String_t* value)
	{
		___className_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___className_0), (void*)value);
	}

	inline static int32_t get_offset_of_nameSpace_1() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpace_1)); }
	inline String_t* get_nameSpace_1() const { return ___nameSpace_1; }
	inline String_t** get_address_of_nameSpace_1() { return &___nameSpace_1; }
	inline void set_nameSpace_1(String_t* value)
	{
		___nameSpace_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameSpace_1), (void*)value);
	}

	inline static int32_t get_offset_of_assembly_2() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assembly_2)); }
	inline String_t* get_assembly_2() const { return ___assembly_2; }
	inline String_t** get_address_of_assembly_2() { return &___assembly_2; }
	inline void set_assembly_2(String_t* value)
	{
		___assembly_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assembly_2), (void*)value);
	}

	inline static int32_t get_offset_of_classHasChanged_3() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___classHasChanged_3)); }
	inline bool get_classHasChanged_3() const { return ___classHasChanged_3; }
	inline bool* get_address_of_classHasChanged_3() { return &___classHasChanged_3; }
	inline void set_classHasChanged_3(bool value)
	{
		___classHasChanged_3 = value;
	}

	inline static int32_t get_offset_of_nameSpaceHasChanged_4() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___nameSpaceHasChanged_4)); }
	inline bool get_nameSpaceHasChanged_4() const { return ___nameSpaceHasChanged_4; }
	inline bool* get_address_of_nameSpaceHasChanged_4() { return &___nameSpaceHasChanged_4; }
	inline void set_nameSpaceHasChanged_4(bool value)
	{
		___nameSpaceHasChanged_4 = value;
	}

	inline static int32_t get_offset_of_assemblyHasChanged_5() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___assemblyHasChanged_5)); }
	inline bool get_assemblyHasChanged_5() const { return ___assemblyHasChanged_5; }
	inline bool* get_address_of_assemblyHasChanged_5() { return &___assemblyHasChanged_5; }
	inline void set_assemblyHasChanged_5(bool value)
	{
		___assemblyHasChanged_5 = value;
	}

	inline static int32_t get_offset_of_autoUdpateAPI_6() { return static_cast<int32_t>(offsetof(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C, ___autoUdpateAPI_6)); }
	inline bool get_autoUdpateAPI_6() const { return ___autoUdpateAPI_6; }
	inline bool* get_address_of_autoUdpateAPI_6() { return &___autoUdpateAPI_6; }
	inline void set_autoUdpateAPI_6(bool value)
	{
		___autoUdpateAPI_6 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_pinvoke
{
	char* ___className_0;
	char* ___nameSpace_1;
	char* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};
// Native definition for COM marshalling of UnityEngine.Scripting.APIUpdating.MovedFromAttributeData
struct MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C_marshaled_com
{
	Il2CppChar* ___className_0;
	Il2CppChar* ___nameSpace_1;
	Il2CppChar* ___assembly_2;
	int32_t ___classHasChanged_3;
	int32_t ___nameSpaceHasChanged_4;
	int32_t ___assemblyHasChanged_5;
	int32_t ___autoUdpateAPI_6;
};

// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.SerializeReference
struct SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.InspectorLabelAttribute::label
	String_t* ___label_0;
	// System.String DevionGames.InspectorLabelAttribute::tooltip
	String_t* ___tooltip_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_tooltip_1() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___tooltip_1)); }
	inline String_t* get_tooltip_1() const { return ___tooltip_1; }
	inline String_t** get_address_of_tooltip_1() { return &___tooltip_1; }
	inline void set_tooltip_1(String_t* value)
	{
		___tooltip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_1), (void*)value);
	}
};


// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Scripting.APIUpdating.MovedFromAttributeData UnityEngine.Scripting.APIUpdating.MovedFromAttribute::data
	MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8, ___data_0)); }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  get_data_0() const { return ___data_0; }
	inline MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C * get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(MovedFromAttributeData_tD215FAE7C2C99058DABB245C5A5EC95AEF05533C  value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___className_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___nameSpace_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___data_0))->___assembly_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.Boolean,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * __this, bool ___autoUpdateAPI0, String_t* ___sourceNamespace1, String_t* ___sourceAssembly2, String_t* ___sourceClassName3, const RuntimeMethod* method);
// System.Void DevionGames.EnumFlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818 (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void DevionGames.IconAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330 (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void DevionGames.ComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3 (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void DevionGames.IconAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * __this, String_t* ___path0, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, const RuntimeMethod* method);
static void DevionGames_Triggers_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_CustomAttributesCacheGenerator_triggerType(CustomAttributesCache* cache)
{
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[0];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
}
static void TriggerInputType_t625D91275DC8F4E0D10B0709F654548DA96BEB97_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator_actions(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator_m_Interruptable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tD99376E4E2C8C97C9DE54ED036D642E1BB355A22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator_m_Enabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x6F\x6E\x54\x65\x6D\x70\x6C\x61\x74\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x76\x69\x6F\x6E\x20\x47\x61\x6D\x65\x73\x2F\x54\x72\x69\x67\x67\x65\x72\x73\x2F\x41\x63\x74\x69\x6F\x6E\x20\x54\x65\x6D\x70\x6C\x61\x74\x65"), NULL);
	}
}
static void ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A_CustomAttributesCacheGenerator_actions(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void Actions_t74A166366FD940F9CCE34095DF1DBC5F81C565B7_CustomAttributesCacheGenerator_actions(CustomAttributesCache* cache)
{
	{
		SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 * tmp = (SerializeReference_t83057B8E7EDCEB5FBB3C32C696FC0422BFFF3677 *)cache->attributes[0];
		SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E(tmp, NULL);
	}
}
static void ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x41\x70\x70\x6C\x79\x52\x6F\x6F\x74\x4D\x6F\x74\x69\x6F\x6E"), NULL);
	}
}
static void ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator_m_Apply(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x43\x72\x6F\x73\x73\x46\x61\x64\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_AnimatorState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_TransitionDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x49\x73\x49\x6E\x54\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_layer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_invertResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x49\x73\x20\x4E\x61\x6D\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
}
static void IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_layer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x53\x65\x74\x20\x42\x6F\x6F\x6C"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_ParameterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x53\x65\x74\x20\x46\x6C\x6F\x61\x74"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_ParameterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_DampTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x53\x65\x74\x20\x49\x6E\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_ParameterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x53\x65\x74\x20\x52\x61\x6E\x64\x6F\x6D\x20\x46\x6C\x6F\x61\x74"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_ParameterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_DampTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Min(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Max(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_RoundToInt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72\x2F\x53\x65\x74\x20\x54\x72\x69\x67\x67\x65\x72"), NULL);
	}
}
static void SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator_m_ParameterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x2F\x50\x6C\x61\x79"), NULL);
	}
}
static void Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_Clip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_AudioMixerGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_Volume(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DeleteVariable_t8A5AF74430815E8026E3C8C6D9A08F76336D433E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x61\x63\x6B\x62\x6F\x61\x72\x64\x2F\x44\x65\x6C\x65\x74\x65\x20\x56\x61\x72\x69\x61\x62\x6C\x65"), NULL);
	}
}
static void DeleteVariable_t8A5AF74430815E8026E3C8C6D9A08F76336D433E_CustomAttributesCacheGenerator_m_VariableName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x61\x63\x6B\x62\x6F\x61\x72\x64\x2F\x49\x6E\x76\x6F\x6B\x65\x20\x57\x69\x74\x68\x20\x56\x61\x72\x69\x61\x62\x6C\x65"), NULL);
	}
}
static void InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x69\x6E\x76\x6F\x6B\x65\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64"), NULL);
	}
}
static void InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_VariableArguments(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x61\x63\x6B\x62\x6F\x61\x72\x64\x2F\x53\x65\x74\x20\x42\x6F\x6F\x6C\x20\x56\x61\x72\x69\x61\x62\x6C\x65"), NULL);
	}
}
static void SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator_m_VariableName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x61\x63\x6B\x62\x6F\x61\x72\x64\x2F\x53\x65\x74\x20\x46\x6C\x6F\x61\x74\x20\x56\x61\x72\x69\x61\x62\x6C\x65"), NULL);
	}
}
static void SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_VariableName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_DampTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2F\x43\x6F\x6D\x70\x61\x72\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x69\x6E\x76\x6F\x6B\x65\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64"), NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x66\x6F\x72\x20\x6D\x65\x74\x68\x6F\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Condition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Number(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2F\x49\x6E\x76\x6F\x6B\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
}
static void Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x69\x6E\x76\x6F\x6B\x65\x20\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x67\x75\x6D\x65\x6E\x74\x73\x20\x66\x6F\x72\x20\x6D\x65\x74\x68\x6F\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2F\x53\x65\x74\x20\x45\x6E\x61\x62\x6C\x65\x64"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
}
static void SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_ComponentName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_Enable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Log_tF94CFE8CD62CD9AE43FA86DD28B59901867361F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67\x2F\x4C\x6F\x67"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Log_tF94CFE8CD62CD9AE43FA86DD28B59901867361F7_CustomAttributesCacheGenerator_m_Message(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x42\x72\x6F\x61\x64\x63\x61\x73\x74\x4D\x65\x73\x73\x61\x67\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_methodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Argument(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Options(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x43\x6F\x6D\x70\x61\x72\x65\x20\x54\x61\x67"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator_m_Tag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x44\x65\x73\x74\x72\x6F\x79"), NULL);
	}
}
static void Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator_m_Delay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x49\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Original(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_BoneName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_IgnorePlayerCollision(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x49\x6E\x73\x74\x61\x6E\x74\x69\x61\x74\x65\x20\x41\x74\x20\x4D\x6F\x75\x73\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator_m_Original(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator_m_IgnorePlayerCollision(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x53\x65\x6E\x64\x4D\x65\x73\x73\x61\x67\x65"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
}
static void SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_methodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Argument(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Options(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x53\x65\x6E\x64\x4D\x65\x73\x73\x61\x67\x65\x55\x70\x77\x61\x72\x64\x73"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_methodName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Argument(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Options(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x2F\x53\x65\x74\x20\x4E\x61\x6D\x65"), NULL);
	}
}
static void SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x76\x65\x72\x6C\x61\x70\x20\x53\x70\x68\x65\x72\x65"), NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_Radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_LayerMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_HitSuccessLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_QueryTriggerInteraction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x52\x61\x79\x63\x61\x73\x74"), NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_MaxDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_LayerMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_HitLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_QueryTriggerInteraction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x53\x70\x68\x65\x72\x65\x43\x61\x73\x74"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_MaxDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_LayerMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_HitSuccessLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_QueryTriggerInteraction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x2F\x53\x65\x74\x20\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x73"), NULL);
	}
}
static void SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator_m_Constraints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadScene_tCFEC21FD0078806E527D190FC7B306A5ADECC422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x65\x6E\x65\x4D\x61\x6E\x61\x67\x65\x72\x2F\x4C\x6F\x61\x64\x20\x53\x63\x65\x6E\x65"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void LoadScene_tCFEC21FD0078806E527D190FC7B306A5ADECC422_CustomAttributesCacheGenerator_m_Scene(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4C\x6F\x6F\x6B\x20\x41\x74\x20\x4D\x6F\x75\x73\x65"), NULL);
	}
}
static void LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_MaxDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4C\x6F\x6F\x6B\x20\x41\x74\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
}
static void LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Position(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookAtTrigger_t044DDAD0D6613475E62430912E7E08C8D9685AAE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4C\x6F\x6F\x6B\x20\x41\x74\x20\x54\x72\x69\x67\x67\x65\x72"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void LookAtTrigger_t044DDAD0D6613475E62430912E7E08C8D9685AAE_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LookForward_t2D060141AD9CB4599B4CF4D8E5004DE133CC4C31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4C\x6F\x6F\x6B\x20\x46\x6F\x72\x77\x61\x72\x64"), NULL);
	}
}
static void LookForward_t2D060141AD9CB4599B4CF4D8E5004DE133CC4C31_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x4D\x6F\x76\x65\x20\x54\x6F\x77\x61\x72\x64\x73"), NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Position(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_LookAtPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_UsePath(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[0];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x53\x65\x74\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[1];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[2];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
}
static void SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_Position(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_SetCameraRelative(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[1];
		IconAttribute__ctor_m0B656540FC6456448BF29D97AA62745B6663F330(tmp, il2cpp_codegen_type_get_object(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_0_0_0_var), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[2];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x2F\x53\x65\x74\x20\x50\x6F\x73\x69\x74\x69\x6F\x6E\x20\x54\x6F\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_Tag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_DefaultPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_SetCameraRelative(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tAFC87F2F5DBFF892F0E48BB7C28F976AA3403680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Wait_t250FFE7875CE3A6DD1140719FA107F46BDDC1C64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 * tmp = (IconAttribute_tDF751FEC000E99C5B1653A7D3B212E80B2FF7426 *)cache->attributes[0];
		IconAttribute__ctor_m90E47E0ECB56AC5D52EAD3F851B97A2C5B6D48AD(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65"), NULL);
	}
	{
		ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 * tmp = (ComponentMenu_t28CF3AC4F15A918C344EFF2680F1819C531756D8 *)cache->attributes[1];
		ComponentMenu__ctor_mB3FCE4765A949E2A65FCC6D81A1C52587E6796B3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x2F\x57\x61\x69\x74"), NULL);
	}
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[2];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void Wait_t250FFE7875CE3A6DD1140719FA107F46BDDC1C64_CustomAttributesCacheGenerator_duration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void IAction_t48DF1EB911EB6996F856D40E00BDC50CFA91878E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 * tmp = (MovedFromAttribute_t7DFA9E51FA9540D9D5EB8D41E363D2BC51F43BC8 *)cache->attributes[0];
		MovedFromAttribute__ctor_mA14E9BEFEB7154D461D78BF7EABE797753BAD09C(tmp, true, NULL, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x6D\x62\x6C\x79\x2D\x43\x53\x68\x61\x72\x70"), NULL, NULL);
	}
}
static void U3CU3Ec_t2187C0191068A726E35467196D44C539A89ADBEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TriggerRaycaster_t0E8A2FC245A09A3C2791FD20E81CAF53838B7D93_CustomAttributesCacheGenerator_m_LayerMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerRaycaster_t0E8A2FC245A09A3C2791FD20E81CAF53838B7D93_CustomAttributesCacheGenerator_TriggerRaycaster_Initialize_mCBB8C03C0080AB308E85ED0FA7CC92789B311786(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_Triggers_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_Triggers_AttributeGenerators[182] = 
{
	BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_CustomAttributesCacheGenerator,
	TriggerInputType_t625D91275DC8F4E0D10B0709F654548DA96BEB97_CustomAttributesCacheGenerator,
	BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator,
	U3CU3Ec_tD99376E4E2C8C97C9DE54ED036D642E1BB355A22_CustomAttributesCacheGenerator,
	Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator,
	ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A_CustomAttributesCacheGenerator,
	ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator,
	CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator,
	IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator,
	IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator,
	SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator,
	SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator,
	SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator,
	SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator,
	Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator,
	DeleteVariable_t8A5AF74430815E8026E3C8C6D9A08F76336D433E_CustomAttributesCacheGenerator,
	InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator,
	SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator,
	SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator,
	Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator,
	SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator,
	Log_tF94CFE8CD62CD9AE43FA86DD28B59901867361F7_CustomAttributesCacheGenerator,
	BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator,
	CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator,
	Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator,
	InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator,
	SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator,
	SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator,
	SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator,
	SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator,
	LoadScene_tCFEC21FD0078806E527D190FC7B306A5ADECC422_CustomAttributesCacheGenerator,
	LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator,
	LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator,
	LookAtTrigger_t044DDAD0D6613475E62430912E7E08C8D9685AAE_CustomAttributesCacheGenerator,
	LookForward_t2D060141AD9CB4599B4CF4D8E5004DE133CC4C31_CustomAttributesCacheGenerator,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator,
	SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator,
	SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator,
	U3CU3Ec_tAFC87F2F5DBFF892F0E48BB7C28F976AA3403680_CustomAttributesCacheGenerator,
	Wait_t250FFE7875CE3A6DD1140719FA107F46BDDC1C64_CustomAttributesCacheGenerator,
	IAction_t48DF1EB911EB6996F856D40E00BDC50CFA91878E_CustomAttributesCacheGenerator,
	U3CU3Ec_t2187C0191068A726E35467196D44C539A89ADBEF_CustomAttributesCacheGenerator,
	BaseTrigger_t4E970CBCC01D8D1DF1CDD1CE10A22265318B9F3F_CustomAttributesCacheGenerator_triggerType,
	BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator_actions,
	BehaviorTrigger_t032A0E631EA30FB8F18CAD1B4BB895DEDB3EA495_CustomAttributesCacheGenerator_m_Interruptable,
	Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator_m_Type,
	Action_t1DA2ADFBBADE17BD0699A9CAD17DD51B1789D34B_CustomAttributesCacheGenerator_m_Enabled,
	ActionTemplate_tB503156C63E4DFAAA4CF2B7C6BFFBBB5C41F442A_CustomAttributesCacheGenerator_actions,
	Actions_t74A166366FD940F9CCE34095DF1DBC5F81C565B7_CustomAttributesCacheGenerator_actions,
	ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator_m_Target,
	ApplyRootMotion_tF0303851CB2C7B19E31292E6D3F3AE93099D4569_CustomAttributesCacheGenerator_m_Apply,
	CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_Target,
	CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_AnimatorState,
	CrossFade_t3C57DB29EABDD81AD245689462564E856BF9BFE8_CustomAttributesCacheGenerator_m_TransitionDuration,
	IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_m_Target,
	IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_layer,
	IsInTransition_tA420BF24B3CEAB52B8BBE98C02F87F79FCEE8ACC_CustomAttributesCacheGenerator_invertResult,
	IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_m_Target,
	IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_layer,
	IsName_t147B086291057BFB7ADAFFD3F31CF9ADDB521FC0_CustomAttributesCacheGenerator_name,
	SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_Target,
	SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_ParameterName,
	SetBool_t8DFFD1FC1682E451C703B92A74EA1A38C6730DC0_CustomAttributesCacheGenerator_m_Value,
	SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_Target,
	SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_ParameterName,
	SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_Value,
	SetFloat_t410C0510298ACBB1437E44DF859FF71872909F5D_CustomAttributesCacheGenerator_m_DampTime,
	SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_Target,
	SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_ParameterName,
	SetInt_tC30A8466BAF9BC0DAAED6238A64B4F49E9BF0338_CustomAttributesCacheGenerator_m_Value,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Target,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_ParameterName,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_DampTime,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Min,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_Max,
	SetRandomFloat_t16EF1FAF5C0F3F0F7F67D572F64A210DB4849532_CustomAttributesCacheGenerator_m_RoundToInt,
	SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator_m_Target,
	SetTrigger_t2B7A42836E83074F7433C324CDCD32A6B01A5B60_CustomAttributesCacheGenerator_m_ParameterName,
	Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_Clip,
	Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_AudioMixerGroup,
	Play_tDD0CCB40E92339766F4D5AE2A749D5EF1762AB42_CustomAttributesCacheGenerator_m_Volume,
	DeleteVariable_t8A5AF74430815E8026E3C8C6D9A08F76336D433E_CustomAttributesCacheGenerator_m_VariableName,
	InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_Target,
	InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_ComponentName,
	InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_MethodName,
	InvokeWithVariable_tEC899AB7F680D74C0105377E0D526C0AD6D72AD8_CustomAttributesCacheGenerator_m_VariableArguments,
	SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator_m_VariableName,
	SetBoolVariable_t20B972EA949FCB35C4EE67953C918A8D7AD2D207_CustomAttributesCacheGenerator_m_Value,
	SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_VariableName,
	SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_Value,
	SetFloatVariable_tE10F6696573750E2C90226403DD445EF4D17ABD0_CustomAttributesCacheGenerator_m_DampTime,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Target,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_ComponentName,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_MethodName,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Arguments,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Condition,
	CompareNumber_t09CEAE05BA3A72FCFF2A6457ECEF0DAB4BFDE2FA_CustomAttributesCacheGenerator_m_Number,
	Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_Target,
	Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_ComponentName,
	Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_MethodName,
	Invoke_tA4A54C55EB98BE2FB77E6487861017BB61F89570_CustomAttributesCacheGenerator_m_Arguments,
	SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_Target,
	SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_ComponentName,
	SetEnabled_tE321472181FCE9B3ACA16BC6DD038984810AE42E_CustomAttributesCacheGenerator_m_Enable,
	Log_tF94CFE8CD62CD9AE43FA86DD28B59901867361F7_CustomAttributesCacheGenerator_m_Message,
	BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Target,
	BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_methodName,
	BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Argument,
	BroadcastMessage_t0B531BF4696F252A24DFB0995173F5C79C34CA8E_CustomAttributesCacheGenerator_m_Options,
	CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator_m_Target,
	CompareTag_tD148D4A86BE63B601B9C0BB26700ACD35973E856_CustomAttributesCacheGenerator_m_Tag,
	Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator_m_Target,
	Destroy_t96DDBFDF2AFBC9EC8C4FC42BF3F411F0AFAF88FD_CustomAttributesCacheGenerator_m_Delay,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Target,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Original,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_BoneName,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_Offset,
	Instantiate_tFB53F62498F2AFCDB7142DB1A6222C76F5D00D2F_CustomAttributesCacheGenerator_m_IgnorePlayerCollision,
	InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator_m_Original,
	InstantiateAtMouse_t0B7B7C57030E04BE6EA10BCF8CD524E13C2B0EF6_CustomAttributesCacheGenerator_m_IgnorePlayerCollision,
	SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Target,
	SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_methodName,
	SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Argument,
	SendMessage_tD1458A7E123FE56B0A7F0CE199563A500068BF5F_CustomAttributesCacheGenerator_m_Options,
	SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Target,
	SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_methodName,
	SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Argument,
	SendMessageUpwards_tD702DEE98DCFC6D12C67D9A5D5B8036FFF5DC844_CustomAttributesCacheGenerator_m_Options,
	SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator_m_Target,
	SetName_t82E06F4D353950C14216E8D5CEDD9BC6C14468EB_CustomAttributesCacheGenerator_m_Value,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_Target,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_Radius,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_LayerMask,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_HitSuccessLayer,
	OverlapSphere_tB3C37B88E753F079A117C9D15DD610CD708DE086_CustomAttributesCacheGenerator_m_QueryTriggerInteraction,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Target,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Offset,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_Direction,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_MaxDistance,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_LayerMask,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_HitLayer,
	Raycast_t2B7E9A8328C2E2F13D619461375A070B7BBDFFFB_CustomAttributesCacheGenerator_m_QueryTriggerInteraction,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Target,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Direction,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_Radius,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_MaxDistance,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_LayerMask,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_HitSuccessLayer,
	SphereCast_t98E3506910EF4D62814D48A16B4866847A1A9607_CustomAttributesCacheGenerator_m_QueryTriggerInteraction,
	SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator_m_Target,
	SetConstraints_t37F2364EA7C06AA34640403760B726E83B1C3948_CustomAttributesCacheGenerator_m_Constraints,
	LoadScene_tCFEC21FD0078806E527D190FC7B306A5ADECC422_CustomAttributesCacheGenerator_m_Scene,
	LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_Target,
	LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_MaxDistance,
	LookAtMouse_t2DB653C2AFAF9F0CA59708EBDE0EBF2BB3B059F1_CustomAttributesCacheGenerator_m_Speed,
	LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Target,
	LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Position,
	LookAtPosition_t09D9419A265100B1C9F2B9BE5F65FB0C4D54C9C1_CustomAttributesCacheGenerator_m_Speed,
	LookAtTrigger_t044DDAD0D6613475E62430912E7E08C8D9685AAE_CustomAttributesCacheGenerator_m_Speed,
	LookForward_t2D060141AD9CB4599B4CF4D8E5004DE133CC4C31_CustomAttributesCacheGenerator_m_Target,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Target,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Position,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_Speed,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_LookAtPosition,
	MoveTowards_t0A61AAC4589ACEE4FA772B89BA9ED6FDA6A8D74C_CustomAttributesCacheGenerator_m_UsePath,
	SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_Target,
	SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_Position,
	SetPosition_tCEBDACB1CB1AA0107809B2188EECD26E5944832C_CustomAttributesCacheGenerator_m_SetCameraRelative,
	SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_Target,
	SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_Tag,
	SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_DefaultPosition,
	SetPositionToTarget_tE0F9809CDE0B356FBC073EFA297839555E189183_CustomAttributesCacheGenerator_m_SetCameraRelative,
	Wait_t250FFE7875CE3A6DD1140719FA107F46BDDC1C64_CustomAttributesCacheGenerator_duration,
	TriggerRaycaster_t0E8A2FC245A09A3C2791FD20E81CAF53838B7D93_CustomAttributesCacheGenerator_m_LayerMask,
	TriggerRaycaster_t0E8A2FC245A09A3C2791FD20E81CAF53838B7D93_CustomAttributesCacheGenerator_TriggerRaycaster_Initialize_mCBB8C03C0080AB308E85ED0FA7CC92789B311786,
	DevionGames_Triggers_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
