﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DevionGames.Graphs.FlowGraph::.ctor()
extern void FlowGraph__ctor_mC5D4BA1D3523EE75C16FF16899853513F3416DF8 (void);
// 0x00000002 System.Void DevionGames.Graphs.EventNode::.ctor()
extern void EventNode__ctor_mA5F5325FE2F3816BAC25F88D8BB33B2BFEA6B7AF (void);
// 0x00000003 System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_Ports()
extern void FlowNode_get_Ports_m79D5C805ABCF819440DA1A96B204CDD1D1ED203C (void);
// 0x00000004 System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_InputPorts()
extern void FlowNode_get_InputPorts_m641161DFC39978BF5843693573B736DD902E5003 (void);
// 0x00000005 System.Collections.Generic.List`1<DevionGames.Graphs.Port> DevionGames.Graphs.FlowNode::get_OutputPorts()
extern void FlowNode_get_OutputPorts_mCF2AD3A195A09DBACF4C9A47BF5E35AFC75B0185 (void);
// 0x00000006 System.Void DevionGames.Graphs.FlowNode::.ctor()
extern void FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18 (void);
// 0x00000007 System.Object DevionGames.Graphs.FlowNode::OnRequestValue(DevionGames.Graphs.Port)
// 0x00000008 T DevionGames.Graphs.FlowNode::GetInputValue(System.String,T)
// 0x00000009 T DevionGames.Graphs.FlowNode::GetOutputValue(System.String)
// 0x0000000A DevionGames.Graphs.Port DevionGames.Graphs.FlowNode::GetPort(System.String)
extern void FlowNode_GetPort_m05E579E4664F3400D3C4CB47FFC9881D665EA16C (void);
// 0x0000000B DevionGames.Graphs.Port DevionGames.Graphs.FlowNode::GetPort(System.Int32)
extern void FlowNode_GetPort_mD43CFCAB159FA796FE9B5561D57578CCED163749 (void);
// 0x0000000C System.Void DevionGames.Graphs.FlowNode::AddPort(DevionGames.Graphs.Port)
extern void FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580 (void);
// 0x0000000D System.Void DevionGames.Graphs.FlowNode::DisconnectAllPorts()
extern void FlowNode_DisconnectAllPorts_m8FA2AC33D4D8E4D4074B258674528E9A596FF306 (void);
// 0x0000000E System.Void DevionGames.Graphs.FlowNode::OnAfterDeserialize()
extern void FlowNode_OnAfterDeserialize_mE0BB937EA97F69C1349E8CB75548A56475EE9A06 (void);
// 0x0000000F System.Void DevionGames.Graphs.FlowNode/<>c::.cctor()
extern void U3CU3Ec__cctor_m167FAA2AF67ACF2FDCA39B9D488EABDCAC42A5CB (void);
// 0x00000010 System.Void DevionGames.Graphs.FlowNode/<>c::.ctor()
extern void U3CU3Ec__ctor_m22B16B9EF02763B8D48635AEFF43B1AD3D089375 (void);
// 0x00000011 System.Boolean DevionGames.Graphs.FlowNode/<>c::<get_InputPorts>b__4_0(DevionGames.Graphs.Port)
extern void U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12 (void);
// 0x00000012 System.Boolean DevionGames.Graphs.FlowNode/<>c::<get_OutputPorts>b__6_0(DevionGames.Graphs.Port)
extern void U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024 (void);
// 0x00000013 System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mA1FED964843DC56317790239CA60118DE619C3E1 (void);
// 0x00000014 System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass11_0::<GetPort>b__0(DevionGames.Graphs.Port)
extern void U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2 (void);
// 0x00000015 System.Void DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m676586F14260D951119E7BF393C9CFF81BFAC368 (void);
// 0x00000016 System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::<OnAfterDeserialize>b__0(DevionGames.Graphs.Node)
extern void U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74 (void);
// 0x00000017 System.Boolean DevionGames.Graphs.FlowNode/<>c__DisplayClass15_0::<OnAfterDeserialize>b__1(DevionGames.Graphs.Port)
extern void U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D (void);
// 0x00000018 System.Object DevionGames.Graphs.Add::OnRequestValue(DevionGames.Graphs.Port)
extern void Add_OnRequestValue_mD7164393B4970FDF303893B0B7A86A2FD1C31352 (void);
// 0x00000019 System.Void DevionGames.Graphs.Add::.ctor()
extern void Add__ctor_mA02D0EE426DFDA33FB078F4C18A2AD7BFEA46BD8 (void);
// 0x0000001A System.Object DevionGames.Graphs.Ceil::OnRequestValue(DevionGames.Graphs.Port)
extern void Ceil_OnRequestValue_m5517F0F2AD7C5F0FC8805C08B7ED71FA6FE859AA (void);
// 0x0000001B System.Void DevionGames.Graphs.Ceil::.ctor()
extern void Ceil__ctor_mA7BFC45C9A0C72BBA00F2FD8796D63874D9EADF2 (void);
// 0x0000001C System.Object DevionGames.Graphs.Divide::OnRequestValue(DevionGames.Graphs.Port)
extern void Divide_OnRequestValue_m63DD9454A9668215E87AC7BFE7BEAFBA7BACA569 (void);
// 0x0000001D System.Void DevionGames.Graphs.Divide::.ctor()
extern void Divide__ctor_m768E398CBC9CA2A31AA301164A4304BA77501A15 (void);
// 0x0000001E System.Object DevionGames.Graphs.Evaluate::OnRequestValue(DevionGames.Graphs.Port)
extern void Evaluate_OnRequestValue_mA88B77FCF48A285E95133A367A493898A7D8DA20 (void);
// 0x0000001F System.Void DevionGames.Graphs.Evaluate::.ctor()
extern void Evaluate__ctor_m4CD5D7B75DD466CD7162EF27CA3817CCAF7708EE (void);
// 0x00000020 System.Object DevionGames.Graphs.Floor::OnRequestValue(DevionGames.Graphs.Port)
extern void Floor_OnRequestValue_mF8135CE73DE6D7B1A20D18ABF135B9D525CBEEB4 (void);
// 0x00000021 System.Void DevionGames.Graphs.Floor::.ctor()
extern void Floor__ctor_mF7F70D54AD4BF6E48FA82A476FB28F54210C87D0 (void);
// 0x00000022 System.Object DevionGames.Graphs.Multiply::OnRequestValue(DevionGames.Graphs.Port)
extern void Multiply_OnRequestValue_m12B9F16BB14F1A5F24A370B56310EBCF276ACF97 (void);
// 0x00000023 System.Void DevionGames.Graphs.Multiply::.ctor()
extern void Multiply__ctor_m4CDABF04EE5DFF6D888B8113BEFD69151B2C7C0C (void);
// 0x00000024 System.Object DevionGames.Graphs.Pow::OnRequestValue(DevionGames.Graphs.Port)
extern void Pow_OnRequestValue_m918133B94951E54F34D3F3475044283601DDAE86 (void);
// 0x00000025 System.Void DevionGames.Graphs.Pow::.ctor()
extern void Pow__ctor_mBF0D82A1F3793DD04BF9E9B0BCC908CDE3ABE841 (void);
// 0x00000026 System.Object DevionGames.Graphs.Random::OnRequestValue(DevionGames.Graphs.Port)
extern void Random_OnRequestValue_m69F5784D76FFADFB0825503946738B2146E59142 (void);
// 0x00000027 System.Void DevionGames.Graphs.Random::.ctor()
extern void Random__ctor_m746A033DFACA2417C81D672D6403D559B1808806 (void);
// 0x00000028 System.Object DevionGames.Graphs.Round::OnRequestValue(DevionGames.Graphs.Port)
extern void Round_OnRequestValue_m25DA35698444213736F8C1EE0566D9FF1B259B07 (void);
// 0x00000029 System.Void DevionGames.Graphs.Round::.ctor()
extern void Round__ctor_m9D72E6F59BF85C9DC172786F0D32DAC5C1E4DD4A (void);
// 0x0000002A System.Object DevionGames.Graphs.Sqrt::OnRequestValue(DevionGames.Graphs.Port)
extern void Sqrt_OnRequestValue_m69560EBE3810A61BC25E93279618E449EB3626AA (void);
// 0x0000002B System.Void DevionGames.Graphs.Sqrt::.ctor()
extern void Sqrt__ctor_m059BE1D90DD04C3BD50DF2BFAE67D9782BD02B33 (void);
// 0x0000002C System.Object DevionGames.Graphs.Subtract::OnRequestValue(DevionGames.Graphs.Port)
extern void Subtract_OnRequestValue_mC8A574FDD03810050CB5725D51076CA0FA9C5BD9 (void);
// 0x0000002D System.Void DevionGames.Graphs.Subtract::.ctor()
extern void Subtract__ctor_mEC0BBD326FE3D99DF2D1BA366D611E0449FDF065 (void);
// 0x0000002E System.Void DevionGames.Graphs.InputAttribute::.ctor()
extern void InputAttribute__ctor_mDAD5BDE5412881B68D4BC6F98B4D321854C431AC (void);
// 0x0000002F System.Void DevionGames.Graphs.InputAttribute::.ctor(System.Boolean,System.Boolean)
extern void InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C (void);
// 0x00000030 System.Void DevionGames.Graphs.OutputAttribute::.ctor()
extern void OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A (void);
// 0x00000031 System.Void DevionGames.Graphs.Edge::.ctor()
extern void Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B (void);
// 0x00000032 System.Type DevionGames.Graphs.Port::get_fieldType()
extern void Port_get_fieldType_m9E4E776EAECFC8FF152934C0C586A86AB4B32A16 (void);
// 0x00000033 System.Collections.Generic.List`1<DevionGames.Graphs.Edge> DevionGames.Graphs.Port::get_Connections()
extern void Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2 (void);
// 0x00000034 System.Void DevionGames.Graphs.Port::.ctor()
extern void Port__ctor_mFE59DCF3A05D932B3E0C947B0FE3AEB15EDAE77A (void);
// 0x00000035 System.Void DevionGames.Graphs.Port::.ctor(DevionGames.Graphs.FlowNode,System.String,System.Type,DevionGames.Graphs.PortCapacity,DevionGames.Graphs.PortDirection)
extern void Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7 (void);
// 0x00000036 T DevionGames.Graphs.Port::GetValue(T)
// 0x00000037 System.Collections.Generic.IEnumerable`1<T> DevionGames.Graphs.Port::GetValues()
// 0x00000038 System.Void DevionGames.Graphs.Port::Connect(DevionGames.Graphs.Port)
extern void Port_Connect_mA37C1FF40FA49F61D2C5CFA23C3C0402B153E335 (void);
// 0x00000039 System.Void DevionGames.Graphs.Port::Disconnect(DevionGames.Graphs.Port)
extern void Port_Disconnect_m939822E708D2C1C1461AF57D4F764B9EE60AB94E (void);
// 0x0000003A System.Void DevionGames.Graphs.Port::DisconnectAll()
extern void Port_DisconnectAll_m9A9345796CFB57DFE8DAE23B538E0D30E23886F0 (void);
// 0x0000003B System.Boolean DevionGames.Graphs.Port::<DisconnectAll>b__19_0(DevionGames.Graphs.Edge)
extern void Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89 (void);
// 0x0000003C System.Void DevionGames.Graphs.Port/<GetValues>d__16`1::.ctor(System.Int32)
// 0x0000003D System.Void DevionGames.Graphs.Port/<GetValues>d__16`1::System.IDisposable.Dispose()
// 0x0000003E System.Boolean DevionGames.Graphs.Port/<GetValues>d__16`1::MoveNext()
// 0x0000003F System.Void DevionGames.Graphs.Port/<GetValues>d__16`1::<>m__Finally1()
// 0x00000040 T DevionGames.Graphs.Port/<GetValues>d__16`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000041 System.Void DevionGames.Graphs.Port/<GetValues>d__16`1::System.Collections.IEnumerator.Reset()
// 0x00000042 System.Object DevionGames.Graphs.Port/<GetValues>d__16`1::System.Collections.IEnumerator.get_Current()
// 0x00000043 System.Collections.Generic.IEnumerator`1<T> DevionGames.Graphs.Port/<GetValues>d__16`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000044 System.Collections.IEnumerator DevionGames.Graphs.Port/<GetValues>d__16`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000045 System.Void DevionGames.Graphs.Port/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m180DB3EEA83BAF5B3727F12436285AD5EEFA327D (void);
// 0x00000046 System.Boolean DevionGames.Graphs.Port/<>c__DisplayClass18_0::<Disconnect>b__0(DevionGames.Graphs.Edge)
extern void U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9 (void);
// 0x00000047 System.Boolean DevionGames.Graphs.Port/<>c__DisplayClass18_0::<Disconnect>b__1(DevionGames.Graphs.Edge)
extern void U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0 (void);
// 0x00000048 DevionGames.Graphs.Graph DevionGames.Graphs.Formula::GetGraph()
extern void Formula_GetGraph_m46FB2CCDE30D85D1770364AA7E7DABDDAA452382 (void);
// 0x00000049 System.Single DevionGames.Graphs.Formula::op_Implicit(DevionGames.Graphs.Formula)
extern void Formula_op_Implicit_m7DA6C405115DE7683C94CB4963C772EC638F68FE (void);
// 0x0000004A System.Void DevionGames.Graphs.Formula::.ctor()
extern void Formula__ctor_mEBD93C95EAF59860D1551CF3CCBFFDF15C342F56 (void);
// 0x0000004B System.Void DevionGames.Graphs.Formula/<>c::.cctor()
extern void U3CU3Ec__cctor_m61839727659D0A26BEDCB6DD9E672088C93EE319 (void);
// 0x0000004C System.Void DevionGames.Graphs.Formula/<>c::.ctor()
extern void U3CU3Ec__ctor_m9342E5C7F5E94A50D12343A0C4E500DBAB1264CE (void);
// 0x0000004D System.Boolean DevionGames.Graphs.Formula/<>c::<op_Implicit>b__2_0(DevionGames.Graphs.Node)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843 (void);
// 0x0000004E System.Void DevionGames.Graphs.FormulaGraph::.ctor()
extern void FormulaGraph__ctor_mECDA736FBD6605313442A8ABFCBE7BDFFCA3E919 (void);
// 0x0000004F System.Single DevionGames.Graphs.FormulaGraph::op_Implicit(DevionGames.Graphs.FormulaGraph)
extern void FormulaGraph_op_Implicit_m9E3DBD69E33B87822AD0975C96BE9C0973C020E3 (void);
// 0x00000050 System.Void DevionGames.Graphs.FormulaGraph/<>c::.cctor()
extern void U3CU3Ec__cctor_m14A0C8539147C4E2766C9207BE1DE8CBEAFD7EF8 (void);
// 0x00000051 System.Void DevionGames.Graphs.FormulaGraph/<>c::.ctor()
extern void U3CU3Ec__ctor_mEA18434FBED31CEAABB9A3DE9DD064F7E138682B (void);
// 0x00000052 System.Boolean DevionGames.Graphs.FormulaGraph/<>c::<op_Implicit>b__1_0(DevionGames.Graphs.Node)
extern void U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C (void);
// 0x00000053 System.Object DevionGames.Graphs.FormulaOutput::OnRequestValue(DevionGames.Graphs.Port)
extern void FormulaOutput_OnRequestValue_m5E4E152E713B280130D5D0E847665DF4DA94467A (void);
// 0x00000054 System.Void DevionGames.Graphs.FormulaOutput::.ctor()
extern void FormulaOutput__ctor_mAFCB23A3D06F0C69DE2899825C89B65461D4A7A1 (void);
// 0x00000055 System.Collections.Generic.List`1<T> DevionGames.Graphs.Graph::FindNodesOfType()
// 0x00000056 System.Void DevionGames.Graphs.Graph::OnBeforeSerialize()
extern void Graph_OnBeforeSerialize_m9975C27B1F141DE195EA9C09FB35A78885C19105 (void);
// 0x00000057 System.Void DevionGames.Graphs.Graph::OnAfterDeserialize()
extern void Graph_OnAfterDeserialize_mDB70B72D05D24A32572B4D96296DDA52E60715F4 (void);
// 0x00000058 System.Void DevionGames.Graphs.Graph::.ctor()
extern void Graph__ctor_mF13B8DA9134092BC46305E78DF7A4631353C0AFD (void);
// 0x00000059 System.Void DevionGames.Graphs.Graph/<>c__3`1::.cctor()
// 0x0000005A System.Void DevionGames.Graphs.Graph/<>c__3`1::.ctor()
// 0x0000005B System.Boolean DevionGames.Graphs.Graph/<>c__3`1::<FindNodesOfType>b__3_0(DevionGames.Graphs.Node)
// 0x0000005C T DevionGames.Graphs.GraphUtility::AddNode(DevionGames.Graphs.Graph)
// 0x0000005D DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::AddNode(DevionGames.Graphs.Graph,System.Type)
extern void GraphUtility_AddNode_m3D1B9E811B1DD86FE2785ABC80DC371EF2AA237F (void);
// 0x0000005E System.String DevionGames.Graphs.GraphUtility::NicifyVariableName(System.String)
extern void GraphUtility_NicifyVariableName_m9037A019C3386722DBA5ED850159659A2C4CDD1A (void);
// 0x0000005F System.Void DevionGames.Graphs.GraphUtility::RemoveNodes(DevionGames.Graphs.Graph,DevionGames.Graphs.FlowNode[])
extern void GraphUtility_RemoveNodes_m5493AE1B5960EF5E3DBE6E0BB93578628C1AD2CC (void);
// 0x00000060 System.Void DevionGames.Graphs.GraphUtility::RemoveNodes(DevionGames.Graphs.Graph,DevionGames.Graphs.Node[])
extern void GraphUtility_RemoveNodes_mF82276CB474A6052B82236BFDBE047B545C7F882 (void);
// 0x00000061 System.Void DevionGames.Graphs.GraphUtility::CreatePorts(DevionGames.Graphs.FlowNode)
extern void GraphUtility_CreatePorts_m5D6C07B357EBCE3B7C322F94E1AC9696D7E5960B (void);
// 0x00000062 System.Void DevionGames.Graphs.GraphUtility::Save(DevionGames.Graphs.Graph)
extern void GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F (void);
// 0x00000063 System.Collections.Generic.Dictionary`2<System.String,System.Object> DevionGames.Graphs.GraphUtility::SerializeNode(DevionGames.Graphs.Node,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void GraphUtility_SerializeNode_m28E8EB533906E6BD25710874F0EA9D1DA56327C0 (void);
// 0x00000064 System.Void DevionGames.Graphs.GraphUtility::SerializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC (void);
// 0x00000065 System.Void DevionGames.Graphs.GraphUtility::SerializeValue(System.String,System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>&,System.Collections.Generic.List`1<UnityEngine.Object>&)
extern void GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1 (void);
// 0x00000066 System.Void DevionGames.Graphs.GraphUtility::Load(DevionGames.Graphs.Graph)
extern void GraphUtility_Load_m4C2361BA90120FD465B90AC31F423B405C220B9F (void);
// 0x00000067 DevionGames.Graphs.Node DevionGames.Graphs.GraphUtility::DeserializeNode(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void GraphUtility_DeserializeNode_m6E75890D459E8DD137AE264D5FA32E13BEC40570 (void);
// 0x00000068 System.Void DevionGames.Graphs.GraphUtility::DeserializeFields(System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48 (void);
// 0x00000069 System.Object DevionGames.Graphs.GraphUtility::DeserializeValue(System.String,System.Object,System.Reflection.FieldInfo,System.Type,System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.Collections.Generic.List`1<UnityEngine.Object>)
extern void GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD (void);
// 0x0000006A System.Object DevionGames.Graphs.GraphUtility::ConvertToArray(System.Collections.IList)
extern void GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC (void);
// 0x0000006B System.Object DevionGames.Graphs.GraphUtility::ConvertToArray(System.Collections.IList,System.Type)
extern void GraphUtility_ConvertToArray_m21F8FF4A9222C371AFE2EBDCB72E07E8D2A3C4DF (void);
// 0x0000006C System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mF3C6BAD036536D3343CAD9AAD7F1D00950AD5FBE (void);
// 0x0000006D System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_0::<RemoveNodes>b__0(DevionGames.Graphs.Node)
extern void U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7 (void);
// 0x0000006E System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::.ctor()
extern void U3CU3Ec__DisplayClass3_1__ctor_mC51161D4D15A0B5F1E17AE8EC55CF6FC4E3381C4 (void);
// 0x0000006F System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass3_1::<RemoveNodes>b__1(DevionGames.Graphs.FlowNode)
extern void U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E (void);
// 0x00000070 System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m6F79A73FE4250F82E7C386C679CA9A8EFE39B4EF (void);
// 0x00000071 System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_0::<RemoveNodes>b__0(DevionGames.Graphs.Node)
extern void U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C (void);
// 0x00000072 System.Void DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m1DF92E78F67367063CE6255316D751DCAA3D76DE (void);
// 0x00000073 System.Boolean DevionGames.Graphs.GraphUtility/<>c__DisplayClass4_1::<RemoveNodes>b__1(DevionGames.Graphs.Node)
extern void U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E (void);
// 0x00000074 DevionGames.Graphs.Graph DevionGames.Graphs.IGraphProvider::GetGraph()
// 0x00000075 System.Void DevionGames.Graphs.Node::.ctor()
extern void Node__ctor_m607E8DB7A61022982F71AE46459C355F082BA030 (void);
// 0x00000076 System.Void DevionGames.Graphs.Node::OnAfterDeserialize()
extern void Node_OnAfterDeserialize_mEAF1B1B19243148A7CC0169F286508742D16401E (void);
// 0x00000077 System.Void DevionGames.Graphs.Node::OnBeforeSerialize()
extern void Node_OnBeforeSerialize_mDD694FF6132DB887E9FD4EF15693528A44F7C707 (void);
// 0x00000078 System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean)
extern void NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB (void);
// 0x00000079 System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.Boolean,System.String)
extern void NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D (void);
// 0x0000007A System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String)
extern void NodeStyleAttribute__ctor_mC0692DD36473F4A4FA86CE04E2EB293DE110927B (void);
// 0x0000007B System.Void DevionGames.Graphs.NodeStyleAttribute::.ctor(System.String,System.Boolean,System.String)
extern void NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B (void);
static Il2CppMethodPointer s_methodPointers[123] = 
{
	FlowGraph__ctor_mC5D4BA1D3523EE75C16FF16899853513F3416DF8,
	EventNode__ctor_mA5F5325FE2F3816BAC25F88D8BB33B2BFEA6B7AF,
	FlowNode_get_Ports_m79D5C805ABCF819440DA1A96B204CDD1D1ED203C,
	FlowNode_get_InputPorts_m641161DFC39978BF5843693573B736DD902E5003,
	FlowNode_get_OutputPorts_mCF2AD3A195A09DBACF4C9A47BF5E35AFC75B0185,
	FlowNode__ctor_m993AD649800838812B971F4B6C43156C321FFC18,
	NULL,
	NULL,
	NULL,
	FlowNode_GetPort_m05E579E4664F3400D3C4CB47FFC9881D665EA16C,
	FlowNode_GetPort_mD43CFCAB159FA796FE9B5561D57578CCED163749,
	FlowNode_AddPort_m7CB403DEF322425C39811ADED030E8AC54F44580,
	FlowNode_DisconnectAllPorts_m8FA2AC33D4D8E4D4074B258674528E9A596FF306,
	FlowNode_OnAfterDeserialize_mE0BB937EA97F69C1349E8CB75548A56475EE9A06,
	U3CU3Ec__cctor_m167FAA2AF67ACF2FDCA39B9D488EABDCAC42A5CB,
	U3CU3Ec__ctor_m22B16B9EF02763B8D48635AEFF43B1AD3D089375,
	U3CU3Ec_U3Cget_InputPortsU3Eb__4_0_mD7EB9CCE77CDE70F3BA7A6BB2281638B727FCF12,
	U3CU3Ec_U3Cget_OutputPortsU3Eb__6_0_m1DCDA7F96265F52E648A9F39BFDDB937D64C4024,
	U3CU3Ec__DisplayClass11_0__ctor_mA1FED964843DC56317790239CA60118DE619C3E1,
	U3CU3Ec__DisplayClass11_0_U3CGetPortU3Eb__0_m2FB62DE1815A7E1C41F85E244D234273E081A4C2,
	U3CU3Ec__DisplayClass15_0__ctor_m676586F14260D951119E7BF393C9CFF81BFAC368,
	U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__0_m33F8C1E48E7F69775342F72071DFA2486A638B74,
	U3CU3Ec__DisplayClass15_0_U3COnAfterDeserializeU3Eb__1_mE9651E82259F4013D3E1ECAD91B276B90F69F07D,
	Add_OnRequestValue_mD7164393B4970FDF303893B0B7A86A2FD1C31352,
	Add__ctor_mA02D0EE426DFDA33FB078F4C18A2AD7BFEA46BD8,
	Ceil_OnRequestValue_m5517F0F2AD7C5F0FC8805C08B7ED71FA6FE859AA,
	Ceil__ctor_mA7BFC45C9A0C72BBA00F2FD8796D63874D9EADF2,
	Divide_OnRequestValue_m63DD9454A9668215E87AC7BFE7BEAFBA7BACA569,
	Divide__ctor_m768E398CBC9CA2A31AA301164A4304BA77501A15,
	Evaluate_OnRequestValue_mA88B77FCF48A285E95133A367A493898A7D8DA20,
	Evaluate__ctor_m4CD5D7B75DD466CD7162EF27CA3817CCAF7708EE,
	Floor_OnRequestValue_mF8135CE73DE6D7B1A20D18ABF135B9D525CBEEB4,
	Floor__ctor_mF7F70D54AD4BF6E48FA82A476FB28F54210C87D0,
	Multiply_OnRequestValue_m12B9F16BB14F1A5F24A370B56310EBCF276ACF97,
	Multiply__ctor_m4CDABF04EE5DFF6D888B8113BEFD69151B2C7C0C,
	Pow_OnRequestValue_m918133B94951E54F34D3F3475044283601DDAE86,
	Pow__ctor_mBF0D82A1F3793DD04BF9E9B0BCC908CDE3ABE841,
	Random_OnRequestValue_m69F5784D76FFADFB0825503946738B2146E59142,
	Random__ctor_m746A033DFACA2417C81D672D6403D559B1808806,
	Round_OnRequestValue_m25DA35698444213736F8C1EE0566D9FF1B259B07,
	Round__ctor_m9D72E6F59BF85C9DC172786F0D32DAC5C1E4DD4A,
	Sqrt_OnRequestValue_m69560EBE3810A61BC25E93279618E449EB3626AA,
	Sqrt__ctor_m059BE1D90DD04C3BD50DF2BFAE67D9782BD02B33,
	Subtract_OnRequestValue_mC8A574FDD03810050CB5725D51076CA0FA9C5BD9,
	Subtract__ctor_mEC0BBD326FE3D99DF2D1BA366D611E0449FDF065,
	InputAttribute__ctor_mDAD5BDE5412881B68D4BC6F98B4D321854C431AC,
	InputAttribute__ctor_m2BCF6C4D130614431FBE13159C1AE191A656E53C,
	OutputAttribute__ctor_mA103A5CBBA603650ED17EDAEB26F692106BA955A,
	Edge__ctor_mB1D39EFDCB26B9CA0437BFBA134E28E6FFC0819B,
	Port_get_fieldType_m9E4E776EAECFC8FF152934C0C586A86AB4B32A16,
	Port_get_Connections_m777D48BDC83409629EA61137B8259AEEB7EDEAE2,
	Port__ctor_mFE59DCF3A05D932B3E0C947B0FE3AEB15EDAE77A,
	Port__ctor_m2A48891E79BE68995AD5E74814342786B1B089F7,
	NULL,
	NULL,
	Port_Connect_mA37C1FF40FA49F61D2C5CFA23C3C0402B153E335,
	Port_Disconnect_m939822E708D2C1C1461AF57D4F764B9EE60AB94E,
	Port_DisconnectAll_m9A9345796CFB57DFE8DAE23B538E0D30E23886F0,
	Port_U3CDisconnectAllU3Eb__19_0_m742B59969866404811B073DB12FC49D0F3E99D89,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass18_0__ctor_m180DB3EEA83BAF5B3727F12436285AD5EEFA327D,
	U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__0_m37A91ED87A48B1D49A4A89D4760C27EE4E843BC9,
	U3CU3Ec__DisplayClass18_0_U3CDisconnectU3Eb__1_mAC363427540BC7865166CFA86583B741AD252AC0,
	Formula_GetGraph_m46FB2CCDE30D85D1770364AA7E7DABDDAA452382,
	Formula_op_Implicit_m7DA6C405115DE7683C94CB4963C772EC638F68FE,
	Formula__ctor_mEBD93C95EAF59860D1551CF3CCBFFDF15C342F56,
	U3CU3Ec__cctor_m61839727659D0A26BEDCB6DD9E672088C93EE319,
	U3CU3Ec__ctor_m9342E5C7F5E94A50D12343A0C4E500DBAB1264CE,
	U3CU3Ec_U3Cop_ImplicitU3Eb__2_0_m874F51EA7C30FE88A0704E91452116C0E88B5843,
	FormulaGraph__ctor_mECDA736FBD6605313442A8ABFCBE7BDFFCA3E919,
	FormulaGraph_op_Implicit_m9E3DBD69E33B87822AD0975C96BE9C0973C020E3,
	U3CU3Ec__cctor_m14A0C8539147C4E2766C9207BE1DE8CBEAFD7EF8,
	U3CU3Ec__ctor_mEA18434FBED31CEAABB9A3DE9DD064F7E138682B,
	U3CU3Ec_U3Cop_ImplicitU3Eb__1_0_mB596C83CE8E3521EF47A9AAE9845B7FF3586993C,
	FormulaOutput_OnRequestValue_m5E4E152E713B280130D5D0E847665DF4DA94467A,
	FormulaOutput__ctor_mAFCB23A3D06F0C69DE2899825C89B65461D4A7A1,
	NULL,
	Graph_OnBeforeSerialize_m9975C27B1F141DE195EA9C09FB35A78885C19105,
	Graph_OnAfterDeserialize_mDB70B72D05D24A32572B4D96296DDA52E60715F4,
	Graph__ctor_mF13B8DA9134092BC46305E78DF7A4631353C0AFD,
	NULL,
	NULL,
	NULL,
	NULL,
	GraphUtility_AddNode_m3D1B9E811B1DD86FE2785ABC80DC371EF2AA237F,
	GraphUtility_NicifyVariableName_m9037A019C3386722DBA5ED850159659A2C4CDD1A,
	GraphUtility_RemoveNodes_m5493AE1B5960EF5E3DBE6E0BB93578628C1AD2CC,
	GraphUtility_RemoveNodes_mF82276CB474A6052B82236BFDBE047B545C7F882,
	GraphUtility_CreatePorts_m5D6C07B357EBCE3B7C322F94E1AC9696D7E5960B,
	GraphUtility_Save_m167A22F48CA32B438DB5E44458286C0D622B383F,
	GraphUtility_SerializeNode_m28E8EB533906E6BD25710874F0EA9D1DA56327C0,
	GraphUtility_SerializeFields_mB9DEF7411E2926A22EE5E7C178AE914279C034AC,
	GraphUtility_SerializeValue_m86C0174A4784D179E48B141E8038CB47E7C0D8D1,
	GraphUtility_Load_m4C2361BA90120FD465B90AC31F423B405C220B9F,
	GraphUtility_DeserializeNode_m6E75890D459E8DD137AE264D5FA32E13BEC40570,
	GraphUtility_DeserializeFields_mD4B31BAD79D6FFBD7AF44BCC126623B5D99E4B48,
	GraphUtility_DeserializeValue_mB0B97B1935E3D12F72E499084596EABD1E9081DD,
	GraphUtility_ConvertToArray_m7C4E72D8E87E38F0FC2653EDFE970BF762358FDC,
	GraphUtility_ConvertToArray_m21F8FF4A9222C371AFE2EBDCB72E07E8D2A3C4DF,
	U3CU3Ec__DisplayClass3_0__ctor_mF3C6BAD036536D3343CAD9AAD7F1D00950AD5FBE,
	U3CU3Ec__DisplayClass3_0_U3CRemoveNodesU3Eb__0_m06BAA3ABF83893342674A51B0184DD2D62FB4CD7,
	U3CU3Ec__DisplayClass3_1__ctor_mC51161D4D15A0B5F1E17AE8EC55CF6FC4E3381C4,
	U3CU3Ec__DisplayClass3_1_U3CRemoveNodesU3Eb__1_mE695693D556A9A48EDEDE24D9848FE2B5D7C292E,
	U3CU3Ec__DisplayClass4_0__ctor_m6F79A73FE4250F82E7C386C679CA9A8EFE39B4EF,
	U3CU3Ec__DisplayClass4_0_U3CRemoveNodesU3Eb__0_m7F57C923DF486D565840854E79CB9D51F0B2C41C,
	U3CU3Ec__DisplayClass4_1__ctor_m1DF92E78F67367063CE6255316D751DCAA3D76DE,
	U3CU3Ec__DisplayClass4_1_U3CRemoveNodesU3Eb__1_mA1ECB3557AC79324B8A141F5F56F1EAB5E855F0E,
	NULL,
	Node__ctor_m607E8DB7A61022982F71AE46459C355F082BA030,
	Node_OnAfterDeserialize_mEAF1B1B19243148A7CC0169F286508742D16401E,
	Node_OnBeforeSerialize_mDD694FF6132DB887E9FD4EF15693528A44F7C707,
	NodeStyleAttribute__ctor_mDE713BFE3FF98285FB14F7B097973DB1FE4DBABB,
	NodeStyleAttribute__ctor_m6E5F49956222E01294AF36576A7DF1C420572E8D,
	NodeStyleAttribute__ctor_mC0692DD36473F4A4FA86CE04E2EB293DE110927B,
	NodeStyleAttribute__ctor_m8682BCDD2A768066B019352F7C741B1970E4A30B,
};
static const int32_t s_InvokerIndices[123] = 
{
	2975,
	2975,
	2912,
	2912,
	2912,
	2975,
	1898,
	-1,
	-1,
	1898,
	1894,
	2448,
	2975,
	2975,
	4649,
	2975,
	2133,
	2133,
	2975,
	2133,
	2975,
	2133,
	2133,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	1898,
	2975,
	2975,
	1467,
	2975,
	2975,
	2912,
	2912,
	2975,
	330,
	-1,
	-1,
	2448,
	2448,
	2975,
	2133,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2975,
	2133,
	2133,
	2912,
	4532,
	2975,
	4649,
	2975,
	2133,
	2975,
	4532,
	4649,
	2975,
	2133,
	1898,
	2975,
	-1,
	2975,
	2975,
	2975,
	-1,
	-1,
	-1,
	-1,
	4069,
	4481,
	4276,
	4276,
	4566,
	4566,
	4059,
	3910,
	3638,
	4566,
	4069,
	3926,
	3157,
	4481,
	4069,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2912,
	2975,
	2975,
	2975,
	2477,
	1466,
	2448,
	941,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x02000019, { 7, 9 } },
	{ 0x02000021, { 20, 4 } },
	{ 0x06000008, { 0, 1 } },
	{ 0x06000009, { 1, 1 } },
	{ 0x06000036, { 2, 3 } },
	{ 0x06000037, { 5, 2 } },
	{ 0x06000055, { 16, 4 } },
	{ 0x0600005C, { 24, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)3, 22467 },
	{ (Il2CppRGCTXDataType)3, 22468 },
	{ (Il2CppRGCTXDataType)3, 22469 },
	{ (Il2CppRGCTXDataType)1, 238 },
	{ (Il2CppRGCTXDataType)2, 238 },
	{ (Il2CppRGCTXDataType)2, 970 },
	{ (Il2CppRGCTXDataType)3, 285 },
	{ (Il2CppRGCTXDataType)3, 287 },
	{ (Il2CppRGCTXDataType)3, 22470 },
	{ (Il2CppRGCTXDataType)2, 2347 },
	{ (Il2CppRGCTXDataType)2, 2459 },
	{ (Il2CppRGCTXDataType)3, 289 },
	{ (Il2CppRGCTXDataType)2, 658 },
	{ (Il2CppRGCTXDataType)2, 971 },
	{ (Il2CppRGCTXDataType)3, 286 },
	{ (Il2CppRGCTXDataType)3, 288 },
	{ (Il2CppRGCTXDataType)2, 936 },
	{ (Il2CppRGCTXDataType)3, 89 },
	{ (Il2CppRGCTXDataType)3, 21788 },
	{ (Il2CppRGCTXDataType)3, 21961 },
	{ (Il2CppRGCTXDataType)2, 937 },
	{ (Il2CppRGCTXDataType)3, 90 },
	{ (Il2CppRGCTXDataType)2, 937 },
	{ (Il2CppRGCTXDataType)1, 626 },
	{ (Il2CppRGCTXDataType)1, 171 },
	{ (Il2CppRGCTXDataType)2, 171 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_Graphs_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_Graphs_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_Graphs_CodeGenModule = 
{
	"DevionGames.Graphs.dll",
	123,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
	g_DevionGames_Graphs_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
