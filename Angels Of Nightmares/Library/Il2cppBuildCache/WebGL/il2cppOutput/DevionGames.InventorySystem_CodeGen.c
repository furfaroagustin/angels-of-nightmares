﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ItemContainerCallbackTester::Awake()
extern void ItemContainerCallbackTester_Awake_mC739F6DF4A7D769775604B8995A501AA16709997 (void);
// 0x00000002 System.Void ItemContainerCallbackTester::.ctor()
extern void ItemContainerCallbackTester__ctor_m886B54FBB3E7FA956AFCC39270444BDB378A3231 (void);
// 0x00000003 System.Void ItemContainerCallbackTester/<>c::.cctor()
extern void U3CU3Ec__cctor_mE15630DCF665B958BBC4902FB3FA3F5F7A9D2FA3 (void);
// 0x00000004 System.Void ItemContainerCallbackTester/<>c::.ctor()
extern void U3CU3Ec__ctor_m933F59E5DF1ED926E1DAC9D75D0E33959989E6A0 (void);
// 0x00000005 System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_0(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CAwakeU3Eb__0_0_mDC226B2EF59870C30E06385F95F55CDDF6BE44FC (void);
// 0x00000006 System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_1(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CAwakeU3Eb__0_1_m708DB13EFC48E9873C0F0AC74270CFE8E3E0E59D (void);
// 0x00000007 System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_2(DevionGames.InventorySystem.Item)
extern void U3CU3Ec_U3CAwakeU3Eb__0_2_mE6978D97020C48DEC7277BBE683DE79E500F5E5E (void);
// 0x00000008 System.Void ItemContainerCallbackTester/<>c::<Awake>b__0_3(DevionGames.InventorySystem.Item,System.Int32)
extern void U3CU3Ec_U3CAwakeU3Eb__0_3_m76E2A6AF9DFADC710C802AB13979BF7CEC5B7CC8 (void);
// 0x00000009 System.Void DisplayBestName::Start()
extern void DisplayBestName_Start_mB682DD1EFA7B036D385880D5C0B438858863E32C (void);
// 0x0000000A System.Void DisplayBestName::Update()
extern void DisplayBestName_Update_m6461D4EB24F3DBF3149B7C20769BE73F0DCA1A74 (void);
// 0x0000000B System.Void DisplayBestName::.ctor()
extern void DisplayBestName__ctor_mE5726C180A325CFAF3A35A007A7B0F1CBA89A0C1 (void);
// 0x0000000C System.Collections.IEnumerator DevionGames.InventorySystem.AreaOfEffect::Start()
extern void AreaOfEffect_Start_m1BCF771F26D8968CFE154EFB7A768BEF77B6DF8C (void);
// 0x0000000D System.Void DevionGames.InventorySystem.AreaOfEffect::.ctor()
extern void AreaOfEffect__ctor_m792675EEC8D631A7A8E5DE879D992E736036CE29 (void);
// 0x0000000E System.Void DevionGames.InventorySystem.AreaOfEffect/<>c::.cctor()
extern void U3CU3Ec__cctor_m685D13559AFBFB9A60F414B523E49297F43F5872 (void);
// 0x0000000F System.Void DevionGames.InventorySystem.AreaOfEffect/<>c::.ctor()
extern void U3CU3Ec__ctor_mF2BED9B4A1E0350101EA87B357BA2926588774D6 (void);
// 0x00000010 System.Boolean DevionGames.InventorySystem.AreaOfEffect/<>c::<Start>b__5_0(UnityEngine.Collider)
extern void U3CU3Ec_U3CStartU3Eb__5_0_m5D1C8E26D405F8CAA5E7E92927233157873FA572 (void);
// 0x00000011 System.Void DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::.ctor(System.Int32)
extern void U3CStartU3Ed__5__ctor_mDD0FCA4B6533BB95B3546AC33074448CB76D9693 (void);
// 0x00000012 System.Void DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::System.IDisposable.Dispose()
extern void U3CStartU3Ed__5_System_IDisposable_Dispose_m54A36E781AE43ABCDAB8A1519129451345E14150 (void);
// 0x00000013 System.Boolean DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::MoveNext()
extern void U3CStartU3Ed__5_MoveNext_mD6E09E763F30501671D6739811E4AE9AC47FE41F (void);
// 0x00000014 System.Object DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D5E1F2EC2C0F803F81E71054DA06B5F02A1EEE4 (void);
// 0x00000015 System.Void DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m2E89927814E781AFD1A1114C71DC06BFF9538222 (void);
// 0x00000016 System.Object DevionGames.InventorySystem.AreaOfEffect/<Start>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m21DA2F9D0680E2C3450473D914ACD575EC2130AA (void);
// 0x00000017 System.String DevionGames.InventorySystem.CategoryAttribute::get_Category()
extern void CategoryAttribute_get_Category_mE4489E5DFF4EF0D5114E9D816A5D3FCFA33948F0 (void);
// 0x00000018 System.Void DevionGames.InventorySystem.CategoryAttribute::.ctor(System.String)
extern void CategoryAttribute__ctor_m17974B18B10E4976D8A74983B1B4B053F81C7AA5 (void);
// 0x00000019 System.Void DevionGames.InventorySystem.CategoryPickerAttribute::.ctor()
extern void CategoryPickerAttribute__ctor_m27FFA39A126E099223A5122AB04E697EFF9E946B (void);
// 0x0000001A System.Void DevionGames.InventorySystem.CategoryPickerAttribute::.ctor(System.Boolean)
extern void CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64 (void);
// 0x0000001B System.Void DevionGames.InventorySystem.CurrencyPickerAttribute::.ctor()
extern void CurrencyPickerAttribute__ctor_m8E052CF026BBCD035C21987DE5F77697649DF2C8 (void);
// 0x0000001C System.Void DevionGames.InventorySystem.CurrencyPickerAttribute::.ctor(System.Boolean)
extern void CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D (void);
// 0x0000001D System.Void DevionGames.InventorySystem.EquipmentPickerAttribute::.ctor()
extern void EquipmentPickerAttribute__ctor_mB766502E0C1E811C8E20338B4CD8659A488A97EA (void);
// 0x0000001E System.Void DevionGames.InventorySystem.EquipmentPickerAttribute::.ctor(System.Boolean)
extern void EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1 (void);
// 0x0000001F System.Void DevionGames.InventorySystem.ItemGroupPickerAttribute::.ctor()
extern void ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030 (void);
// 0x00000020 System.Void DevionGames.InventorySystem.ItemGroupPickerAttribute::.ctor(System.Boolean)
extern void ItemGroupPickerAttribute__ctor_m6FD8C9704175B55C9FB52EB40C40301C0DAF820E (void);
// 0x00000021 System.Void DevionGames.InventorySystem.ItemPickerAttribute::.ctor()
extern void ItemPickerAttribute__ctor_mBFC500175B67C72650C23BDBD2288D9B635523E8 (void);
// 0x00000022 System.Void DevionGames.InventorySystem.ItemPickerAttribute::.ctor(System.Boolean)
extern void ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C (void);
// 0x00000023 System.Void DevionGames.InventorySystem.PickerAttribute::.ctor()
extern void PickerAttribute__ctor_m379B6F1EDA67E38B1D5200FAE8995769767D021A (void);
// 0x00000024 System.Void DevionGames.InventorySystem.PickerAttribute::.ctor(System.Boolean)
extern void PickerAttribute__ctor_m696E2C2E2A75EDF1A4102EF474B0E4C857E81943 (void);
// 0x00000025 System.Void DevionGames.InventorySystem.RarityPickerAttribute::.ctor()
extern void RarityPickerAttribute__ctor_m1F668D2B795D249B61C01A84427D6CBF4E6C06B1 (void);
// 0x00000026 System.Void DevionGames.InventorySystem.RarityPickerAttribute::.ctor(System.Boolean)
extern void RarityPickerAttribute__ctor_m7DB777170B060BE32956BC1990EDD7FD24E65CE5 (void);
// 0x00000027 DevionGames.InventorySystem.Category DevionGames.InventorySystem.Category::get_Parent()
extern void Category_get_Parent_mEC13083704DDFBB5061BC78872E9B0CA08877179 (void);
// 0x00000028 System.Void DevionGames.InventorySystem.Category::set_Parent(DevionGames.InventorySystem.Category)
extern void Category_set_Parent_m533C6ECDF3DCE1264B2C8A6D238F1C8E5D9028BA (void);
// 0x00000029 System.String DevionGames.InventorySystem.Category::get_Name()
extern void Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F (void);
// 0x0000002A System.Void DevionGames.InventorySystem.Category::set_Name(System.String)
extern void Category_set_Name_m6CD1D0B68EFB3775A5CB509D45B8902932800AE9 (void);
// 0x0000002B UnityEngine.Color DevionGames.InventorySystem.Category::get_EditorColor()
extern void Category_get_EditorColor_mDE4C7CC6F7948D4FE5418A8EA9DF6B1BDA26B99A (void);
// 0x0000002C System.Single DevionGames.InventorySystem.Category::get_Cooldown()
extern void Category_get_Cooldown_m150926D0BFBE2FAE18D0646DEC9021D7B71D1DF4 (void);
// 0x0000002D System.Boolean DevionGames.InventorySystem.Category::IsAssignable(DevionGames.InventorySystem.Category)
extern void Category_IsAssignable_m3DE13E19A8451FCBC1976ED09B90E99BC6B9901E (void);
// 0x0000002E System.Void DevionGames.InventorySystem.Category::.ctor()
extern void Category__ctor_mE45EF7C13AEA41AEE07418CAE27753C66C272511 (void);
// 0x0000002F System.Void DevionGames.InventorySystem.CurrencyConversion::.ctor()
extern void CurrencyConversion__ctor_mC28E7B1EA45F562260AE0C72324BCC30A6F1F496 (void);
// 0x00000030 System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentHandler/EquipmentBone> DevionGames.InventorySystem.EquipmentHandler::get_Bones()
extern void EquipmentHandler_get_Bones_m062658235B4182336CC92A9B6C15B9FC27124A80 (void);
// 0x00000031 System.Void DevionGames.InventorySystem.EquipmentHandler::set_Bones(System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentHandler/EquipmentBone>)
extern void EquipmentHandler_set_Bones_m99F4831852F6EFEEAF58C997851ABC6305237406 (void);
// 0x00000032 System.Collections.Generic.List`1<DevionGames.InventorySystem.VisibleItem> DevionGames.InventorySystem.EquipmentHandler::get_VisibleItems()
extern void EquipmentHandler_get_VisibleItems_mCBA2C05511F4415F3911BED1E714BA57ED5E7501 (void);
// 0x00000033 System.Void DevionGames.InventorySystem.EquipmentHandler::set_VisibleItems(System.Collections.Generic.List`1<DevionGames.InventorySystem.VisibleItem>)
extern void EquipmentHandler_set_VisibleItems_m0AAFF9A39DAFB9A008F2B842B3296B0200D322D0 (void);
// 0x00000034 System.Void DevionGames.InventorySystem.EquipmentHandler::Start()
extern void EquipmentHandler_Start_m91BAFAB7E4B3412F44B855FB6366D2D5E3D40541 (void);
// 0x00000035 System.Void DevionGames.InventorySystem.EquipmentHandler::OnAddItem(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void EquipmentHandler_OnAddItem_m82C55350B4CF0D04D9C83FA081D717AEB01D1B2B (void);
// 0x00000036 System.Void DevionGames.InventorySystem.EquipmentHandler::OnRemoveItem(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void EquipmentHandler_OnRemoveItem_mD47338FF91E2B3A7FC988D49C5142CF0841DFDDF (void);
// 0x00000037 System.Void DevionGames.InventorySystem.EquipmentHandler::EquipItem(DevionGames.InventorySystem.EquipmentItem)
extern void EquipmentHandler_EquipItem_m360DE17606DED7C910CD7882765A5A3E2DB03E16 (void);
// 0x00000038 System.Void DevionGames.InventorySystem.EquipmentHandler::UnEquipItem(DevionGames.InventorySystem.EquipmentItem)
extern void EquipmentHandler_UnEquipItem_mCDCF40160D9A6BEF09AF3A67B325BF1DFA2D11CE (void);
// 0x00000039 System.Void DevionGames.InventorySystem.EquipmentHandler::UpdateEquipment()
extern void EquipmentHandler_UpdateEquipment_mEA143A5B9AFCD4F10E86E442C4B8732946981F61 (void);
// 0x0000003A UnityEngine.Transform DevionGames.InventorySystem.EquipmentHandler::GetBone(DevionGames.InventorySystem.EquipmentRegion)
extern void EquipmentHandler_GetBone_m510D1291A594A6CAF82F886525CE3E72E1E3731E (void);
// 0x0000003B System.Void DevionGames.InventorySystem.EquipmentHandler::.ctor()
extern void EquipmentHandler__ctor_mC2FA6080FD451F59CF6F88D65DF2221E7828F451 (void);
// 0x0000003C System.Void DevionGames.InventorySystem.EquipmentHandler/EquipmentBone::.ctor()
extern void EquipmentBone__ctor_mD471A7062AA004CCACE63EA16051DD81C642B8B0 (void);
// 0x0000003D System.Void DevionGames.InventorySystem.EquipmentHandler/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m0D404DA9A5D86ECCC984D3CB2389B2CCEB852738 (void);
// 0x0000003E System.Boolean DevionGames.InventorySystem.EquipmentHandler/<>c__DisplayClass14_0::<EquipItem>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass14_0_U3CEquipItemU3Eb__0_m3FD5FC9769C5AA8186463C508D617058BF254A71 (void);
// 0x0000003F System.Void DevionGames.InventorySystem.EquipmentHandler/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m5B4067DF36AF6F9DFE43311BA98B05BAC343CEC0 (void);
// 0x00000040 System.Boolean DevionGames.InventorySystem.EquipmentHandler/<>c__DisplayClass17_0::<GetBone>b__0(DevionGames.InventorySystem.EquipmentHandler/EquipmentBone)
extern void U3CU3Ec__DisplayClass17_0_U3CGetBoneU3Eb__0_mDCDFE57746CB04060A33FCCB4CD5A1F5EA794073 (void);
// 0x00000041 System.Void DevionGames.InventorySystem.MeleeWeapon::.ctor()
extern void MeleeWeapon__ctor_mC5A8AC0D00107393FFE6F703986086802CAB3887 (void);
// 0x00000042 System.Void DevionGames.InventorySystem.ShootableWeapon::OnItemActivated(System.Boolean)
extern void ShootableWeapon_OnItemActivated_m17B22FA72A6FD547E0150CC883967A0BEE114CFE (void);
// 0x00000043 System.Void DevionGames.InventorySystem.ShootableWeapon::Update()
extern void ShootableWeapon_Update_m5AEDB5D849D9756CB3CA94DF467A8F29FA0CDBDA (void);
// 0x00000044 System.Void DevionGames.InventorySystem.ShootableWeapon::Use()
extern void ShootableWeapon_Use_m8976248588AB7D996040B954FE559DD523609244 (void);
// 0x00000045 System.Boolean DevionGames.InventorySystem.ShootableWeapon::CanUse()
extern void ShootableWeapon_CanUse_mFF1E4AF0246B0BD422CFAA2DD8A0680759AEE0C7 (void);
// 0x00000046 System.Boolean DevionGames.InventorySystem.ShootableWeapon::TryReload()
extern void ShootableWeapon_TryReload_mF2074AEC6E1BBF765001D97526675920E392E6EE (void);
// 0x00000047 System.Void DevionGames.InventorySystem.ShootableWeapon::OnStopUse()
extern void ShootableWeapon_OnStopUse_mCF994B42B44FE63AFE18BD03309E8A70A99F8D47 (void);
// 0x00000048 System.Void DevionGames.InventorySystem.ShootableWeapon::OnEndReload()
extern void ShootableWeapon_OnEndReload_mBAAD7E4E23295F76F4C412DB894DDF809D468F9D (void);
// 0x00000049 System.Void DevionGames.InventorySystem.ShootableWeapon::CreateCurrentProjectile()
extern void ShootableWeapon_CreateCurrentProjectile_mE21A60BBA70FCD32731D1182278091BAF96B1CD0 (void);
// 0x0000004A System.Void DevionGames.InventorySystem.ShootableWeapon::.ctor()
extern void ShootableWeapon__ctor_mBC58068E02CE8AF626157B069D95270665B8168C (void);
// 0x0000004B System.Void DevionGames.InventorySystem.StaticItem::.ctor()
extern void StaticItem__ctor_mDD661D949ED72198CEC6C373D45315D81E5F2080 (void);
// 0x0000004C System.String[] DevionGames.InventorySystem.VisibleItem::get_Callbacks()
extern void VisibleItem_get_Callbacks_mC7F094E29A684C67C78BC1DA5B0AF2FC69B31DBF (void);
// 0x0000004D System.Void DevionGames.InventorySystem.VisibleItem::Start()
extern void VisibleItem_Start_mAFAE5C514C1AAF3F38CEF0A5D4E6C255F3A09875 (void);
// 0x0000004E System.Void DevionGames.InventorySystem.VisibleItem::Awake()
extern void VisibleItem_Awake_m8BAD1E0F55204193D3D8F27D126C497F311B43B4 (void);
// 0x0000004F System.Void DevionGames.InventorySystem.VisibleItem::Update()
extern void VisibleItem_Update_m0BB597F4AD10557B304082C782C9FD948C6C37B6 (void);
// 0x00000050 System.Void DevionGames.InventorySystem.VisibleItem::OnItemEquip(DevionGames.InventorySystem.Item)
extern void VisibleItem_OnItemEquip_m331DA56D076AB64DBFE3B25A407EE13A77912756 (void);
// 0x00000051 System.Void DevionGames.InventorySystem.VisibleItem::OnItemUnEquip(DevionGames.InventorySystem.Item)
extern void VisibleItem_OnItemUnEquip_mA757BA1D99796D1026EDE7CF92DABB76FC24AA65 (void);
// 0x00000052 System.Void DevionGames.InventorySystem.VisibleItem::IgnoreCollision(UnityEngine.GameObject)
extern void VisibleItem_IgnoreCollision_mC6E2D2C982B8C2BA3C0930C7230A26F93A62024E (void);
// 0x00000053 System.Void DevionGames.InventorySystem.VisibleItem::.ctor()
extern void VisibleItem__ctor_m5A81A0319E91581C4A7B8D663D706D4083850CBC (void);
// 0x00000054 UnityEngine.GameObject DevionGames.InventorySystem.VisibleItem/Attachment::Instantiate(DevionGames.InventorySystem.EquipmentHandler)
extern void Attachment_Instantiate_m7D9EE00E6517589518F548E0A0883EB44F0D25F9 (void);
// 0x00000055 System.Void DevionGames.InventorySystem.VisibleItem/Attachment::.ctor()
extern void Attachment__ctor_mD76902D6EE601C99BF3CF5292A7D6F332C26C2C0 (void);
// 0x00000056 System.String[] DevionGames.InventorySystem.Weapon::get_Callbacks()
extern void Weapon_get_Callbacks_mF9BD0999F9FB8935DF5754480CA0C330A0AD548F (void);
// 0x00000057 System.Boolean DevionGames.InventorySystem.Weapon::get_IsActive()
extern void Weapon_get_IsActive_m3DFACF02A0AED62AB8C6235AA67C8ECA6711F399 (void);
// 0x00000058 System.Void DevionGames.InventorySystem.Weapon::set_IsActive(System.Boolean)
extern void Weapon_set_IsActive_m22C9DBDD52DA81C7BB0F269D63337B404CB276FA (void);
// 0x00000059 System.Void DevionGames.InventorySystem.Weapon::OnItemEquip(DevionGames.InventorySystem.Item)
extern void Weapon_OnItemEquip_mCC6601E920715FC50B31BE2921F6D2B9B3908686 (void);
// 0x0000005A System.Void DevionGames.InventorySystem.Weapon::OnItemUnEquip(DevionGames.InventorySystem.Item)
extern void Weapon_OnItemUnEquip_m5705311A10556594D5411542BE040555C349E297 (void);
// 0x0000005B System.Void DevionGames.InventorySystem.Weapon::Update()
extern void Weapon_Update_mB512BCF1F820D8335D38F2C38305B098DE418C21 (void);
// 0x0000005C System.Void DevionGames.InventorySystem.Weapon::TryStartUse()
extern void Weapon_TryStartUse_mE9D75B3B5B65FAC166DAB3EBEBE45310AA49F771 (void);
// 0x0000005D System.Boolean DevionGames.InventorySystem.Weapon::CanUse()
extern void Weapon_CanUse_m29D2768FAFAAB1D72426C90B5A6D50D15471326E (void);
// 0x0000005E System.Void DevionGames.InventorySystem.Weapon::TryStopUse()
extern void Weapon_TryStopUse_m27A67116E6C6928B8A2B4E04D2E366285E6065EE (void);
// 0x0000005F System.Boolean DevionGames.InventorySystem.Weapon::CanUnuse()
extern void Weapon_CanUnuse_m3DC11D2DCE95A0E63C3C65BAEAA7EC0F23F27A1C (void);
// 0x00000060 System.Void DevionGames.InventorySystem.Weapon::StopUse()
extern void Weapon_StopUse_m8D618148B84A4D20C6F2ED666040C828805FE79C (void);
// 0x00000061 System.Void DevionGames.InventorySystem.Weapon::OnStopUse()
extern void Weapon_OnStopUse_m153574121A19FE690A034C28F3F4F91AD3BDC0AD (void);
// 0x00000062 System.Void DevionGames.InventorySystem.Weapon::StartUse()
extern void Weapon_StartUse_mB41B59F6D143A16C45DD841FEC44AC6A652C034D (void);
// 0x00000063 System.Void DevionGames.InventorySystem.Weapon::OnStartUse()
extern void Weapon_OnStartUse_mCD6D7DD6FB0DB9445AA053A26B66136076537ED9 (void);
// 0x00000064 System.Void DevionGames.InventorySystem.Weapon::Use()
extern void Weapon_Use_m60F5BF06C56EA2AA4D09DF096C0748C627E0E947 (void);
// 0x00000065 System.Void DevionGames.InventorySystem.Weapon::UseItem()
extern void Weapon_UseItem_m5AF108A20A06BE402484E882204D4476AE7BF172 (void);
// 0x00000066 System.Void DevionGames.InventorySystem.Weapon::OnEndUse()
extern void Weapon_OnEndUse_mD27F8D4A0A0F573DC9DDECF54B83AD5357BD1671 (void);
// 0x00000067 System.Void DevionGames.InventorySystem.Weapon::PauseItemUpdate(System.Boolean)
extern void Weapon_PauseItemUpdate_m11EFC7666793B0FE3A8EA6537972CC0E500E9501 (void);
// 0x00000068 System.Void DevionGames.InventorySystem.Weapon::OnItemActivated(System.Boolean)
extern void Weapon_OnItemActivated_mBC86304A259342A9CDB8259A3B41C65CC25D03F8 (void);
// 0x00000069 System.Void DevionGames.InventorySystem.Weapon::OnAnimatorIK(System.Int32)
extern void Weapon_OnAnimatorIK_mED78BE8C7D510D62620CF26D0CC61EFA83562483 (void);
// 0x0000006A System.Void DevionGames.InventorySystem.Weapon::.ctor()
extern void Weapon__ctor_mAFCCCF86E43E31E0DDBA2F89B648469BBEA3BACA (void);
// 0x0000006B System.String DevionGames.InventorySystem.EquipmentRegion::get_Name()
extern void EquipmentRegion_get_Name_mE9156A7BE449EA2F9B3BDD27CCCDFE8741A6FC07 (void);
// 0x0000006C System.Void DevionGames.InventorySystem.EquipmentRegion::set_Name(System.String)
extern void EquipmentRegion_set_Name_mDDE8F2923FC64102912253FEBD6ADFBD129CC525 (void);
// 0x0000006D System.Void DevionGames.InventorySystem.EquipmentRegion::.ctor()
extern void EquipmentRegion__ctor_m84E39EEF627B8A3595616790E22B1A18DA4692CC (void);
// 0x0000006E System.Boolean DevionGames.InventorySystem.IGenerator::get_enabled()
// 0x0000006F System.Void DevionGames.InventorySystem.IGenerator::set_enabled(System.Boolean)
// 0x00000070 System.Void DevionGames.InventorySystem.ItemGenerator::Start()
extern void ItemGenerator_Start_m56995A90E5D014D0F546703DEB8BD66FC8937711 (void);
// 0x00000071 System.Collections.Generic.List`1<DevionGames.InventorySystem.Item> DevionGames.InventorySystem.ItemGenerator::GenerateItems()
extern void ItemGenerator_GenerateItems_mBCEAB097D2E5ADF652DC73FBDC2C445A1074B379 (void);
// 0x00000072 System.Void DevionGames.InventorySystem.ItemGenerator::.ctor()
extern void ItemGenerator__ctor_m7DD90283517E45B2800B3C60C98B622216ECEC27 (void);
// 0x00000073 System.Boolean DevionGames.InventorySystem.ItemGenerator::DevionGames.InventorySystem.IGenerator.get_enabled()
extern void ItemGenerator_DevionGames_InventorySystem_IGenerator_get_enabled_mF98E361579B397CB0447516CE29E9EFB25AE20A3 (void);
// 0x00000074 System.Void DevionGames.InventorySystem.ItemGenerator::DevionGames.InventorySystem.IGenerator.set_enabled(System.Boolean)
extern void ItemGenerator_DevionGames_InventorySystem_IGenerator_set_enabled_m60B8377308D22A8E536CE0F3BB542B9DC2EAF286 (void);
// 0x00000075 System.Int32 DevionGames.InventorySystem.ItemGenerator::<GenerateItems>b__3_0(System.Int32)
extern void ItemGenerator_U3CGenerateItemsU3Eb__3_0_m674BC7BF5C695E2D0CEB7586F350F7EF62C8A8A8 (void);
// 0x00000076 System.Void DevionGames.InventorySystem.ItemGeneratorData::.ctor()
extern void ItemGeneratorData__ctor_mF2EF403776B28D83DAF5B0545611F03D20166E70 (void);
// 0x00000077 System.Void DevionGames.InventorySystem.ItemGroupGenerator::Start()
extern void ItemGroupGenerator_Start_m78CF28AA1AE46A3C8B3095A2EEBD4DA9F057C4EF (void);
// 0x00000078 System.Collections.Generic.List`1<DevionGames.InventorySystem.Item> DevionGames.InventorySystem.ItemGroupGenerator::GenerateItems()
extern void ItemGroupGenerator_GenerateItems_m83BE43AD49F680DD2EA72A4C37C8CC70BAF29319 (void);
// 0x00000079 System.Void DevionGames.InventorySystem.ItemGroupGenerator::.ctor()
extern void ItemGroupGenerator__ctor_m82AA47D12AD0CA828A07C32DABE5837C56B4C6AC (void);
// 0x0000007A System.Boolean DevionGames.InventorySystem.ItemGroupGenerator::DevionGames.InventorySystem.IGenerator.get_enabled()
extern void ItemGroupGenerator_DevionGames_InventorySystem_IGenerator_get_enabled_mF44C88E7C736D8E48F6A64DD69C17746945B03C7 (void);
// 0x0000007B System.Void DevionGames.InventorySystem.ItemGroupGenerator::DevionGames.InventorySystem.IGenerator.set_enabled(System.Boolean)
extern void ItemGroupGenerator_DevionGames_InventorySystem_IGenerator_set_enabled_m6BFC2B44643AE53AF3CA8C65EA6E909738C3B0DD (void);
// 0x0000007C System.Void DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6232303D57B0FECFF722519E957D0ACA81A52419 (void);
// 0x0000007D System.Boolean DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::<GenerateItems>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__0_m61D18D2567B3EB8CF59726B611E5049FE8970034 (void);
// 0x0000007E System.Boolean DevionGames.InventorySystem.ItemGroupGenerator/<>c__DisplayClass9_0::<GenerateItems>b__1(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__1_m45DB1312E7BDEE24A58170DC0D53CEB1DCD4F791 (void);
// 0x0000007F DevionGames.InventorySystem.InventoryManager DevionGames.InventorySystem.InventoryManager::get_current()
extern void InventoryManager_get_current_mFB04BF99D34138705FC3BF9AFD8A4F0257BB35E7 (void);
// 0x00000080 DevionGames.InventorySystem.ItemDatabase DevionGames.InventorySystem.InventoryManager::get_Database()
extern void InventoryManager_get_Database_m37E60244F2CB2229598CCBDEF7765F8528EC46AB (void);
// 0x00000081 DevionGames.InventorySystem.Configuration.Default DevionGames.InventorySystem.InventoryManager::get_DefaultSettings()
extern void InventoryManager_get_DefaultSettings_mEBB604D541294EBFA1C7790A00061D0E3302791E (void);
// 0x00000082 DevionGames.InventorySystem.Configuration.UI DevionGames.InventorySystem.InventoryManager::get_UI()
extern void InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649 (void);
// 0x00000083 DevionGames.InventorySystem.Configuration.Notifications DevionGames.InventorySystem.InventoryManager::get_Notifications()
extern void InventoryManager_get_Notifications_mEDB9294A13E142ADD091E293C9061DD95D73067A (void);
// 0x00000084 DevionGames.InventorySystem.Configuration.SavingLoading DevionGames.InventorySystem.InventoryManager::get_SavingLoading()
extern void InventoryManager_get_SavingLoading_m1A94EDF9A0184831AFC2CC9947605D6E8886B76F (void);
// 0x00000085 DevionGames.InventorySystem.Configuration.Input DevionGames.InventorySystem.InventoryManager::get_Input()
extern void InventoryManager_get_Input_m2922836FE3BE47126B894DAEADA94DB0358EA2F9 (void);
// 0x00000086 T DevionGames.InventorySystem.InventoryManager::GetSetting()
// 0x00000087 DevionGames.PlayerInfo DevionGames.InventorySystem.InventoryManager::get_PlayerInfo()
extern void InventoryManager_get_PlayerInfo_mEFAEBE0D32A3439A0E5FACE44A9BD10502CAC6ED (void);
// 0x00000088 System.Boolean DevionGames.InventorySystem.InventoryManager::get_IsLoaded()
extern void InventoryManager_get_IsLoaded_m9FF27BA29FE0DE34CD22FC1353087F06DC19262E (void);
// 0x00000089 System.Void DevionGames.InventorySystem.InventoryManager::Awake()
extern void InventoryManager_Awake_m5B556D0929DD27FA99FADBE627A4255D2E624D99 (void);
// 0x0000008A System.Void DevionGames.InventorySystem.InventoryManager::Start()
extern void InventoryManager_Start_mD77178711350B9B78A323DFA03DBFC5A1CC16185 (void);
// 0x0000008B System.Void DevionGames.InventorySystem.InventoryManager::ChangedActiveScene(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void InventoryManager_ChangedActiveScene_m0448E967DA6D1EEDE4001E777BEDF2FA77A074E4 (void);
// 0x0000008C UnityEngine.Bounds DevionGames.InventorySystem.InventoryManager::GetBounds(UnityEngine.GameObject)
extern void InventoryManager_GetBounds_m7490F78E6B2A3B1806058F81D20E63F53C816E53 (void);
// 0x0000008D System.Collections.IEnumerator DevionGames.InventorySystem.InventoryManager::DelayedLoading(System.Single)
extern void InventoryManager_DelayedLoading_mC2BAB7CBA7F1F2CF9F329B7DB66264EE52C056A3 (void);
// 0x0000008E System.Collections.IEnumerator DevionGames.InventorySystem.InventoryManager::RepeatSaving(System.Single)
extern void InventoryManager_RepeatSaving_m5D8702EC560A6F60DC5CF51077CDBB350BE26F0E (void);
// 0x0000008F System.Void DevionGames.InventorySystem.InventoryManager::Save()
extern void InventoryManager_Save_m29DDA4CC814F6EACEDCC19F98047F20F4D72EFE5 (void);
// 0x00000090 System.Void DevionGames.InventorySystem.InventoryManager::Save(System.String)
extern void InventoryManager_Save_m111038F104D8392D39BD065C62CA487710B0D315 (void);
// 0x00000091 System.Void DevionGames.InventorySystem.InventoryManager::Serialize(System.String&,System.String&)
extern void InventoryManager_Serialize_m987466A40A471EF596529710FB5DC687E0E03323 (void);
// 0x00000092 System.Void DevionGames.InventorySystem.InventoryManager::Load()
extern void InventoryManager_Load_mFA8E753F77FE12B1D52E3F0C99A1F60AC14BEC29 (void);
// 0x00000093 System.Void DevionGames.InventorySystem.InventoryManager::Load(System.String)
extern void InventoryManager_Load_m1A8274A328F990EB2E9D244FA357B5E5A093C7FD (void);
// 0x00000094 System.Void DevionGames.InventorySystem.InventoryManager::Load(System.String,System.String)
extern void InventoryManager_Load_m29705DEEDBC512871267BE117AAAEBF0C4CDF3C4 (void);
// 0x00000095 System.Boolean DevionGames.InventorySystem.InventoryManager::HasSavedData()
extern void InventoryManager_HasSavedData_m7F27DF6225D8EE91A5A7F0391702FABBF4C45E20 (void);
// 0x00000096 System.Boolean DevionGames.InventorySystem.InventoryManager::HasSavedData(System.String)
extern void InventoryManager_HasSavedData_m4C28884B48653F35A1A886123B683C065C22F967 (void);
// 0x00000097 System.Void DevionGames.InventorySystem.InventoryManager::LoadUI(System.String)
extern void InventoryManager_LoadUI_m70C94265BA670CE84621921A2B4834A0AA6C21B9 (void);
// 0x00000098 System.Void DevionGames.InventorySystem.InventoryManager::LoadScene(System.String)
extern void InventoryManager_LoadScene_m8F8CC773388786FCB3050F1A1073216B0600C1E9 (void);
// 0x00000099 UnityEngine.GameObject DevionGames.InventorySystem.InventoryManager::GetPrefab(System.String)
extern void InventoryManager_GetPrefab_m9D187129F4716B3867C2916D1C3E34F0F3034D15 (void);
// 0x0000009A UnityEngine.GameObject DevionGames.InventorySystem.InventoryManager::CreateCollection(System.String,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void InventoryManager_CreateCollection_m9CB850A14F4D2785E47A84E7AB7194C43DD179F9 (void);
// 0x0000009B UnityEngine.GameObject DevionGames.InventorySystem.InventoryManager::Instantiate(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void InventoryManager_Instantiate_m0E90DED729E0832634C2577F5409BBD57733E23B (void);
// 0x0000009C System.Void DevionGames.InventorySystem.InventoryManager::Destroy(UnityEngine.GameObject)
extern void InventoryManager_Destroy_m6214D00210BA54942F08EB0D2C868759F1D66A5A (void);
// 0x0000009D DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.InventoryManager::CreateInstances(DevionGames.InventorySystem.ItemGroup)
extern void InventoryManager_CreateInstances_mF67891B0A9830819D61BA1DC62F30E403EEA4673 (void);
// 0x0000009E DevionGames.InventorySystem.Item DevionGames.InventorySystem.InventoryManager::CreateInstance(DevionGames.InventorySystem.Item)
extern void InventoryManager_CreateInstance_m36833D1D23D7CDD61094A02489F67AA472183A93 (void);
// 0x0000009F DevionGames.InventorySystem.Item DevionGames.InventorySystem.InventoryManager::CreateInstance(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.ItemModifierList)
extern void InventoryManager_CreateInstance_m7F8CB08E8C8B5E7CF0A08201230827CF19D209ED (void);
// 0x000000A0 DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.InventoryManager::CreateInstances(DevionGames.InventorySystem.Item[])
extern void InventoryManager_CreateInstances_mC646412A6BAF31B0AB5C3DED3AADC3D1055E497D (void);
// 0x000000A1 DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.InventoryManager::CreateInstances(DevionGames.InventorySystem.Item[],System.Int32[],DevionGames.InventorySystem.ItemModifierList[])
extern void InventoryManager_CreateInstances_mF80625F6891BD4AED9B99CE1B5A1D7159F534BF1 (void);
// 0x000000A2 System.Void DevionGames.InventorySystem.InventoryManager::.ctor()
extern void InventoryManager__ctor_mB5093BF24A06BF7EF36E923D3A8179790E2EEF78 (void);
// 0x000000A3 System.Void DevionGames.InventorySystem.InventoryManager::.cctor()
extern void InventoryManager__cctor_m221200C861C12CCE7050A1EE47180A519A1C7493 (void);
// 0x000000A4 System.Void DevionGames.InventorySystem.InventoryManager/<>c__23`1::.cctor()
// 0x000000A5 System.Void DevionGames.InventorySystem.InventoryManager/<>c__23`1::.ctor()
// 0x000000A6 System.Boolean DevionGames.InventorySystem.InventoryManager/<>c__23`1::<GetSetting>b__23_0(DevionGames.InventorySystem.Configuration.Settings)
// 0x000000A7 System.Void DevionGames.InventorySystem.InventoryManager/<>c::.cctor()
extern void U3CU3Ec__cctor_mB402ECA7F064DD7AACF31C481E27071B30BD4202 (void);
// 0x000000A8 System.Void DevionGames.InventorySystem.InventoryManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m63622E9ED83CB47C3E47EE3FE7C877A155D772D2 (void);
// 0x000000A9 System.Void DevionGames.InventorySystem.InventoryManager/<>c::<Awake>b__33_0()
extern void U3CU3Ec_U3CAwakeU3Eb__33_0_mE160AFD43EAED028ABA61B4EB0DDC873121B65F0 (void);
// 0x000000AA System.Boolean DevionGames.InventorySystem.InventoryManager/<>c::<Save>b__40_0(System.String)
extern void U3CU3Ec_U3CSaveU3Eb__40_0_m8247E5AEBD285075F3A123EB22CF369250EC2F72 (void);
// 0x000000AB System.Boolean DevionGames.InventorySystem.InventoryManager/<>c::<Save>b__40_1(System.String)
extern void U3CU3Ec_U3CSaveU3Eb__40_1_m7BA85CAB63DE153D9FFD50C51401F31E5FDD8AE1 (void);
// 0x000000AC System.Boolean DevionGames.InventorySystem.InventoryManager/<>c::<Serialize>b__41_2(DevionGames.InventorySystem.ItemCollection)
extern void U3CU3Ec_U3CSerializeU3Eb__41_2_mB76292D7BB891B21B2BD1378B31780FA0690DC73 (void);
// 0x000000AD System.Boolean DevionGames.InventorySystem.InventoryManager/<>c::<Serialize>b__41_3(DevionGames.InventorySystem.ItemCollection)
extern void U3CU3Ec_U3CSerializeU3Eb__41_3_mF318BFBC58FDB6FA304A7CC2434E585215FBAE06 (void);
// 0x000000AE System.Boolean DevionGames.InventorySystem.InventoryManager/<>c::<LoadScene>b__48_0(DevionGames.InventorySystem.ItemCollection)
extern void U3CU3Ec_U3CLoadSceneU3Eb__48_0_mB4408815982A69C7A35AF6086BB3C513421454BC (void);
// 0x000000AF System.Void DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::.ctor(System.Int32)
extern void U3CDelayedLoadingU3Ed__37__ctor_m3325202AFC5623802BE84D44B0A7DDC9DE29095E (void);
// 0x000000B0 System.Void DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::System.IDisposable.Dispose()
extern void U3CDelayedLoadingU3Ed__37_System_IDisposable_Dispose_m71FFD192424C855DB335AC3AD8EB218444508066 (void);
// 0x000000B1 System.Boolean DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::MoveNext()
extern void U3CDelayedLoadingU3Ed__37_MoveNext_m30FD2E3EF1AFA7ADF26E3A358FFD5D17291358B6 (void);
// 0x000000B2 System.Object DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedLoadingU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE895F35F30A0DC7759F22C9E344873FD0F449B (void);
// 0x000000B3 System.Void DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::System.Collections.IEnumerator.Reset()
extern void U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_Reset_m5E31818C9301990803658065761372C698261518 (void);
// 0x000000B4 System.Object DevionGames.InventorySystem.InventoryManager/<DelayedLoading>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_get_Current_m3B16221AB84E47B898C986B4DDC35AF4D0511C1C (void);
// 0x000000B5 System.Void DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::.ctor(System.Int32)
extern void U3CRepeatSavingU3Ed__38__ctor_m6BABEB7DF2E5AA16D319ED98FCF12AD557763172 (void);
// 0x000000B6 System.Void DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::System.IDisposable.Dispose()
extern void U3CRepeatSavingU3Ed__38_System_IDisposable_Dispose_m06C133222BF9EF721288B8864DD7BC84857CC580 (void);
// 0x000000B7 System.Boolean DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::MoveNext()
extern void U3CRepeatSavingU3Ed__38_MoveNext_m44BC6F57F59888CB6FCAAF324C522EF5CDFD3717 (void);
// 0x000000B8 System.Object DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatSavingU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9434F8BE699510BBA853AE850EDA9590EE455611 (void);
// 0x000000B9 System.Void DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::System.Collections.IEnumerator.Reset()
extern void U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_Reset_m4E523C49553E9E05315E213B36ED6E4A9BE347A5 (void);
// 0x000000BA System.Object DevionGames.InventorySystem.InventoryManager/<RepeatSaving>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_get_Current_m5BC6CCD95656D52496CF70BBEDFB46BBC364A187 (void);
// 0x000000BB System.Void DevionGames.InventorySystem.InventoryManager/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_mC41321735C1D85A5A6B30DE2008606D4D9E581F6 (void);
// 0x000000BC System.Void DevionGames.InventorySystem.InventoryManager/<>c__DisplayClass41_0::<Serialize>b__0(UnityEngine.GameObject)
extern void U3CU3Ec__DisplayClass41_0_U3CSerializeU3Eb__0_mE39020694A5FB2731BCA1FB59C69BFCCA572A0A7 (void);
// 0x000000BD System.Void DevionGames.InventorySystem.InventoryManager/<>c__DisplayClass41_0::<Serialize>b__1(UnityEngine.GameObject)
extern void U3CU3Ec__DisplayClass41_0_U3CSerializeU3Eb__1_mC64543C490A4E9D2408333AC0A0161D869C67975 (void);
// 0x000000BE System.Void DevionGames.InventorySystem.ItemCollection::Awake()
extern void ItemCollection_Awake_m693D404A6DAF0F8123CA7DA29AEE76480D39121E (void);
// 0x000000BF System.Void DevionGames.InventorySystem.ItemCollection::Initialize()
extern void ItemCollection_Initialize_mF3156F7E8CF9487D9CF2EF55EC98D0996F13EE10 (void);
// 0x000000C0 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemCollection::get_Item(System.Int32)
extern void ItemCollection_get_Item_m7AF9F02E7A66AACC6CD48D40D2226554AD436746 (void);
// 0x000000C1 System.Void DevionGames.InventorySystem.ItemCollection::set_Item(System.Int32,DevionGames.InventorySystem.Item)
extern void ItemCollection_set_Item_m2B003BE3327D8C11B066D4AB21D0F01D43E6D31D (void);
// 0x000000C2 System.Int32 DevionGames.InventorySystem.ItemCollection::get_Count()
extern void ItemCollection_get_Count_m1B93C4F9667813E1A5EBE5DBDEC1AF20BFC4ED55 (void);
// 0x000000C3 System.Boolean DevionGames.InventorySystem.ItemCollection::get_IsEmpty()
extern void ItemCollection_get_IsEmpty_mA43141264C534C0135DA6C7C0D96E93AF2A202BC (void);
// 0x000000C4 System.Collections.Generic.IEnumerator`1<DevionGames.InventorySystem.Item> DevionGames.InventorySystem.ItemCollection::GetEnumerator()
extern void ItemCollection_GetEnumerator_m4CA97070C1FB3F01816BB6F2C477C0728F5F2168 (void);
// 0x000000C5 System.Collections.IEnumerator DevionGames.InventorySystem.ItemCollection::System.Collections.IEnumerable.GetEnumerator()
extern void ItemCollection_System_Collections_IEnumerable_GetEnumerator_m77653EA77A992A5836A813A3AF48CEF34B7E55FC (void);
// 0x000000C6 System.Void DevionGames.InventorySystem.ItemCollection::Add(DevionGames.InventorySystem.Item[])
extern void ItemCollection_Add_m4ADAD380A52AB62943A405E671E94DFCBC5635D2 (void);
// 0x000000C7 System.Void DevionGames.InventorySystem.ItemCollection::Add(DevionGames.InventorySystem.Item,System.Boolean)
extern void ItemCollection_Add_m575A8C7545EA8C045E37B2DFBEA59DF5DBCC4854 (void);
// 0x000000C8 System.Boolean DevionGames.InventorySystem.ItemCollection::Remove(DevionGames.InventorySystem.Item)
extern void ItemCollection_Remove_mCA3BDDFF65CC1C611F9A760F23CEB105D67F1270 (void);
// 0x000000C9 System.Void DevionGames.InventorySystem.ItemCollection::Insert(System.Int32,DevionGames.InventorySystem.Item)
extern void ItemCollection_Insert_m5DD9AF26212975815009D9D44B9E2077D6B83CAE (void);
// 0x000000CA System.Void DevionGames.InventorySystem.ItemCollection::RemoveAt(System.Int32)
extern void ItemCollection_RemoveAt_mFCC5D8BC4D0F560133BDD7E7EBCB9E86F2127633 (void);
// 0x000000CB System.Void DevionGames.InventorySystem.ItemCollection::Clear()
extern void ItemCollection_Clear_mF3871DB53E6610D93DB8B48CE065639F043012D7 (void);
// 0x000000CC System.Void DevionGames.InventorySystem.ItemCollection::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ItemCollection_GetObjectData_m6AF56B2A3FF4609070387119A3D0802908DF02E9 (void);
// 0x000000CD System.Void DevionGames.InventorySystem.ItemCollection::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void ItemCollection_SetObjectData_mAEC37CB1881D2D391A230019F4AD17AF6F177FAA (void);
// 0x000000CE System.Void DevionGames.InventorySystem.ItemCollection::CombineCurrencies()
extern void ItemCollection_CombineCurrencies_m096417AEFB9B954B56B70D414C3AF5F01D437823 (void);
// 0x000000CF System.Void DevionGames.InventorySystem.ItemCollection::.ctor()
extern void ItemCollection__ctor_m41AC127F351CD9C2D242A0780D312FF9AD524E3A (void);
// 0x000000D0 System.Void DevionGames.InventorySystem.ItemCollection/<>c::.cctor()
extern void U3CU3Ec__cctor_m68FECCBCD3AB0712E1183230371F4ECD3A414237 (void);
// 0x000000D1 System.Void DevionGames.InventorySystem.ItemCollection/<>c::.ctor()
extern void U3CU3Ec__ctor_m493F8859F494A62BD4A97F72FE1F1E78BE0CAA35 (void);
// 0x000000D2 System.Boolean DevionGames.InventorySystem.ItemCollection/<>c::<Clear>b__22_0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec_U3CClearU3Eb__22_0_m49BED5F879949D295F8F88ACF2E18F6B998152E8 (void);
// 0x000000D3 System.Boolean DevionGames.InventorySystem.ItemCollection/<>c::<CombineCurrencies>b__25_0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec_U3CCombineCurrenciesU3Eb__25_0_m6400172305D5FA5BC0249B6975DCDD3366DA578C (void);
// 0x000000D4 System.Void DevionGames.InventorySystem.ItemCollection/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m48B137162483D13B1983C403695CED849BAA6D25 (void);
// 0x000000D5 System.Boolean DevionGames.InventorySystem.ItemCollection/<>c__DisplayClass24_0::<SetObjectData>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass24_0_U3CSetObjectDataU3Eb__0_m0D1038DC858E12DCA1BDE1BCE424CEE81C333EA4 (void);
// 0x000000D6 System.Void DevionGames.InventorySystem.ItemCollectionPopulator::Start()
extern void ItemCollectionPopulator_Start_m3B23C47B91052B351D702198151618BBAB280D52 (void);
// 0x000000D7 System.Void DevionGames.InventorySystem.ItemCollectionPopulator::.ctor()
extern void ItemCollectionPopulator__ctor_m5EA80706CD847E77758E31AE3AC7893B1C79FAAA (void);
// 0x000000D8 System.Void DevionGames.InventorySystem.ItemCondition::.ctor()
extern void ItemCondition__ctor_m14B34DB4A8A6A6ACD26DFF63A2B5DE04ACEE6F0D (void);
// 0x000000D9 System.Void DevionGames.InventorySystem.ItemContainerPopulator::Start()
extern void ItemContainerPopulator_Start_m1F73EF5E6A5086FE713A199F2F3F16A4763B8F8A (void);
// 0x000000DA System.Void DevionGames.InventorySystem.ItemContainerPopulator::.ctor()
extern void ItemContainerPopulator__ctor_mB84DA902A75727F5750E849B94C019B5E9EDC5EB (void);
// 0x000000DB System.Void DevionGames.InventorySystem.ItemContainerPopulator/Entry::.ctor()
extern void Entry__ctor_m45CCEFBA047A44BA862051D9E41682E0CE7D5B9A (void);
// 0x000000DC System.Collections.Generic.List`1<DevionGames.InventorySystem.Item> DevionGames.InventorySystem.ItemDatabase::get_allItems()
extern void ItemDatabase_get_allItems_m1635746DD2CCB010B14FF1ED3E40F8F07C693484 (void);
// 0x000000DD UnityEngine.GameObject DevionGames.InventorySystem.ItemDatabase::GetItemPrefab(System.String)
extern void ItemDatabase_GetItemPrefab_m06BCB4230BB9B4BE40DA62EFD28B63178F479A13 (void);
// 0x000000DE DevionGames.InventorySystem.ItemGroup DevionGames.InventorySystem.ItemDatabase::GetItemGroup(System.String)
extern void ItemDatabase_GetItemGroup_mE6A9BCBA97AC58E90332945EAC7824F516E4BBCE (void);
// 0x000000DF System.Void DevionGames.InventorySystem.ItemDatabase::Merge(DevionGames.InventorySystem.ItemDatabase)
extern void ItemDatabase_Merge_mD2169A3EC7B4CCF5585B1DE84CBD0CDA71FD9866 (void);
// 0x000000E0 System.Void DevionGames.InventorySystem.ItemDatabase::.ctor()
extern void ItemDatabase__ctor_m070BF716C00550CEC2AD98392EB55C4279B0AD4E (void);
// 0x000000E1 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_0(DevionGames.InventorySystem.Item)
extern void ItemDatabase_U3CMergeU3Eb__11_0_m3B3F48AA98F934853BDB4646CB7A0DC823F031EF (void);
// 0x000000E2 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_1(DevionGames.InventorySystem.Currency)
extern void ItemDatabase_U3CMergeU3Eb__11_1_m77CBC2BC2BA3096537EEF5F026EED3C48B19664E (void);
// 0x000000E3 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_2(DevionGames.InventorySystem.Rarity)
extern void ItemDatabase_U3CMergeU3Eb__11_2_m808DCA78EC4202BB44A393415ED0270388CB6730 (void);
// 0x000000E4 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_3(DevionGames.InventorySystem.Category)
extern void ItemDatabase_U3CMergeU3Eb__11_3_mF0E9185A251DD88FFAE0ABDAE689984884A2B9E2 (void);
// 0x000000E5 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_4(DevionGames.InventorySystem.EquipmentRegion)
extern void ItemDatabase_U3CMergeU3Eb__11_4_m96F50044707291B8066224E9D89807BE41574364 (void);
// 0x000000E6 System.Boolean DevionGames.InventorySystem.ItemDatabase::<Merge>b__11_5(DevionGames.InventorySystem.ItemGroup)
extern void ItemDatabase_U3CMergeU3Eb__11_5_mF41FB56C477F6EE7E2939F43286360F76F89BAD6 (void);
// 0x000000E7 System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mDFA45F3813076EFE9C1C15C593F8EE23B9595E60 (void);
// 0x000000E8 System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass10_0::<GetItemGroup>b__0(DevionGames.InventorySystem.ItemGroup)
extern void U3CU3Ec__DisplayClass10_0_U3CGetItemGroupU3Eb__0_mD5336BEA23CBD87A0405598E9C5500320C41AA73 (void);
// 0x000000E9 System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m02D9EA5414D3D7771C112C640679955B6FACEED3 (void);
// 0x000000EA System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_0::<Merge>b__6(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass11_0_U3CMergeU3Eb__6_m6E4398A632AF5281525451DA84FEB2FAC092D816 (void);
// 0x000000EB System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1::.ctor()
extern void U3CU3Ec__DisplayClass11_1__ctor_m3CDDC3F336D033E80AE18380C8DB444B501E1FB0 (void);
// 0x000000EC System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_1::<Merge>b__7(DevionGames.InventorySystem.Currency)
extern void U3CU3Ec__DisplayClass11_1_U3CMergeU3Eb__7_m4A06110E76F0AFDFDDF63DF70CD57758A8C5B936 (void);
// 0x000000ED System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2::.ctor()
extern void U3CU3Ec__DisplayClass11_2__ctor_mBEA76D40F3FB5FB4161CDCFDB17A34172AF7D7F0 (void);
// 0x000000EE System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_2::<Merge>b__8(DevionGames.InventorySystem.Rarity)
extern void U3CU3Ec__DisplayClass11_2_U3CMergeU3Eb__8_m3BE2DAA1450D7210C235221F1DFE0F5CFB952854 (void);
// 0x000000EF System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3::.ctor()
extern void U3CU3Ec__DisplayClass11_3__ctor_m9C281F43BD87D90C7617E368A682AEABB8E7EF84 (void);
// 0x000000F0 System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_3::<Merge>b__9(DevionGames.InventorySystem.Category)
extern void U3CU3Ec__DisplayClass11_3_U3CMergeU3Eb__9_m03575315A10CFFD04FBFCFB9F7C6BD58EC3370CA (void);
// 0x000000F1 System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4::.ctor()
extern void U3CU3Ec__DisplayClass11_4__ctor_m7D20B898349551DFE3B704D9BF98064B834DF9C7 (void);
// 0x000000F2 System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_4::<Merge>b__10(DevionGames.InventorySystem.EquipmentRegion)
extern void U3CU3Ec__DisplayClass11_4_U3CMergeU3Eb__10_m8F16DA14EA9489646CE196D106818187836C4533 (void);
// 0x000000F3 System.Void DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5::.ctor()
extern void U3CU3Ec__DisplayClass11_5__ctor_mF78EC76B71BF3B03892829A68B4478EB4CE85A77 (void);
// 0x000000F4 System.Boolean DevionGames.InventorySystem.ItemDatabase/<>c__DisplayClass11_5::<Merge>b__11(DevionGames.InventorySystem.ItemGroup)
extern void U3CU3Ec__DisplayClass11_5_U3CMergeU3Eb__11_m6FCD5A6F233BB10323DA645180E16C611E5D7A1C (void);
// 0x000000F5 System.Void DevionGames.InventorySystem.ItemEventData::.ctor(DevionGames.InventorySystem.Item)
extern void ItemEventData__ctor_mA35E230581C34A69BFEC494AE2976B6A1F94D765 (void);
// 0x000000F6 System.String DevionGames.InventorySystem.ItemGroup::get_Name()
extern void ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B (void);
// 0x000000F7 System.Void DevionGames.InventorySystem.ItemGroup::set_Name(System.String)
extern void ItemGroup_set_Name_m3D1A99C7594965F41661D0599ECC0495C5A2E988 (void);
// 0x000000F8 DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.ItemGroup::get_Items()
extern void ItemGroup_get_Items_m6B2F0E3AAE4E4C656103716D023E2B89D5BEC2EA (void);
// 0x000000F9 System.Int32[] DevionGames.InventorySystem.ItemGroup::get_Amounts()
extern void ItemGroup_get_Amounts_m4CD2F43F8352EF1EB8DB7D34DA9249E320CFBD7E (void);
// 0x000000FA System.Collections.Generic.List`1<DevionGames.InventorySystem.ItemModifierList> DevionGames.InventorySystem.ItemGroup::get_Modifiers()
extern void ItemGroup_get_Modifiers_m4D238521E86C6252714114C71096BEF2CB60A16C (void);
// 0x000000FB System.Void DevionGames.InventorySystem.ItemGroup::.ctor()
extern void ItemGroup__ctor_m809A51C5C9154EAD7E84AD5EC21FB33DF770BF22 (void);
// 0x000000FC System.Int32 DevionGames.InventorySystem.Currency::get_MaxStack()
extern void Currency_get_MaxStack_m0B419DD7B2AA59B743A042CB3FAA828445AAADE8 (void);
// 0x000000FD System.Void DevionGames.InventorySystem.Currency::.ctor()
extern void Currency__ctor_mB6DB2EBD0C7F1CD4E75982CCAC6B4EE859655DAD (void);
// 0x000000FE UnityEngine.GameObject DevionGames.InventorySystem.EquipmentItem::get_EquipPrefab()
extern void EquipmentItem_get_EquipPrefab_mCCC2417F80AD679D81124A1FB2B0EF1C84BC7A8F (void);
// 0x000000FF System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentRegion> DevionGames.InventorySystem.EquipmentItem::get_Region()
extern void EquipmentItem_get_Region_m065A5969CE54EFEB34FF7374EF58622767B41E0E (void);
// 0x00000100 System.Void DevionGames.InventorySystem.EquipmentItem::set_Region(System.Collections.Generic.List`1<DevionGames.InventorySystem.EquipmentRegion>)
extern void EquipmentItem_set_Region_m6D649B9B2C942DB09D15CCD60E5CCC46056D6BCE (void);
// 0x00000101 System.Void DevionGames.InventorySystem.EquipmentItem::.ctor()
extern void EquipmentItem__ctor_mACBD87A107E347FED6C7012B25D923AC5AFBFE8F (void);
// 0x00000102 System.String DevionGames.InventorySystem.Item::get_Id()
extern void Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55 (void);
// 0x00000103 System.Void DevionGames.InventorySystem.Item::set_Id(System.String)
extern void Item_set_Id_m167E3C57A18376DA4EF46CC5C2593C8004697DE1 (void);
// 0x00000104 System.String DevionGames.InventorySystem.Item::get_Name()
extern void Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB (void);
// 0x00000105 System.Void DevionGames.InventorySystem.Item::set_Name(System.String)
extern void Item_set_Name_m17EC1514286117F156C004E23C98A4CDE7C2472E (void);
// 0x00000106 System.String DevionGames.InventorySystem.Item::get_DisplayName()
extern void Item_get_DisplayName_m4DE5829073A0A2EA30EA47CBD49B513704C12909 (void);
// 0x00000107 System.Void DevionGames.InventorySystem.Item::set_DisplayName(System.String)
extern void Item_set_DisplayName_m2FC5FD256CD876F3EE0171726EF2284731BF1E96 (void);
// 0x00000108 UnityEngine.Sprite DevionGames.InventorySystem.Item::get_Icon()
extern void Item_get_Icon_m5458CA78A43074CA889D686A51B3E8ADB864514C (void);
// 0x00000109 System.Void DevionGames.InventorySystem.Item::set_Icon(UnityEngine.Sprite)
extern void Item_set_Icon_mA2711FB5CA50C6C1F548F8F84DCCF32DAC2FF125 (void);
// 0x0000010A UnityEngine.GameObject DevionGames.InventorySystem.Item::get_Prefab()
extern void Item_get_Prefab_mD522B985D924BDE518E5A5D62EACB2E762C375D4 (void);
// 0x0000010B System.Void DevionGames.InventorySystem.Item::set_Prefab(UnityEngine.GameObject)
extern void Item_set_Prefab_mA077E6C383C84BA880C64E8B2E35122C8DAA01D2 (void);
// 0x0000010C System.String DevionGames.InventorySystem.Item::get_Description()
extern void Item_get_Description_mB7F9BB622A01FD5720F1573FA9716698D415833F (void);
// 0x0000010D DevionGames.InventorySystem.Category DevionGames.InventorySystem.Item::get_Category()
extern void Item_get_Category_mA2301BF94DD3C9FCE88171937B00B79DB07844E0 (void);
// 0x0000010E System.Void DevionGames.InventorySystem.Item::set_Category(DevionGames.InventorySystem.Category)
extern void Item_set_Category_m349DE64378D71A5A6770244EA5CB8CE948EED857 (void);
// 0x0000010F DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.Item::get_DefaultRarity()
extern void Item_get_DefaultRarity_m6886F014737E6B6D318351B9F5C6A3E2D4F16275 (void);
// 0x00000110 DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.Item::get_Rarity()
extern void Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6 (void);
// 0x00000111 System.Void DevionGames.InventorySystem.Item::set_Rarity(DevionGames.InventorySystem.Rarity)
extern void Item_set_Rarity_m03F630817FA8F647C99649BD9BDC9C1433BB297E (void);
// 0x00000112 System.Boolean DevionGames.InventorySystem.Item::get_IsSellable()
extern void Item_get_IsSellable_m3DEAC715603E2BA6AEAD713758DFFD0FD577806B (void);
// 0x00000113 System.Void DevionGames.InventorySystem.Item::set_IsSellable(System.Boolean)
extern void Item_set_IsSellable_m5BA77C0C37EF21E38E3EC617F3AAB6231FDEA299 (void);
// 0x00000114 System.Int32 DevionGames.InventorySystem.Item::get_BuyPrice()
extern void Item_get_BuyPrice_m46EB74805728696A53C379A637EEF269A45395B1 (void);
// 0x00000115 System.Boolean DevionGames.InventorySystem.Item::get_CanBuyBack()
extern void Item_get_CanBuyBack_m3402923063AE1D7AED52A41E8081549A72D1F417 (void);
// 0x00000116 DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::get_BuyCurrency()
extern void Item_get_BuyCurrency_m2A371F5A4BBA33885093CC75E3B9984F97BC12AE (void);
// 0x00000117 System.Void DevionGames.InventorySystem.Item::set_BuyCurrency(DevionGames.InventorySystem.Currency)
extern void Item_set_BuyCurrency_m5ED60538F150ED3F05E15A4D3F7849E11E8DDA64 (void);
// 0x00000118 System.Int32 DevionGames.InventorySystem.Item::get_SellPrice()
extern void Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603 (void);
// 0x00000119 DevionGames.InventorySystem.Currency DevionGames.InventorySystem.Item::get_SellCurrency()
extern void Item_get_SellCurrency_m52419C8985F5C719A86C5EB23928E6D0A6665421 (void);
// 0x0000011A System.Void DevionGames.InventorySystem.Item::set_SellCurrency(DevionGames.InventorySystem.Currency)
extern void Item_set_SellCurrency_mE9291662A3F081856328B73285884756A3778B8D (void);
// 0x0000011B System.Int32 DevionGames.InventorySystem.Item::get_Stack()
extern void Item_get_Stack_mC5F93507A3A6EF0338C8998718305F33B4AACCD8 (void);
// 0x0000011C System.Void DevionGames.InventorySystem.Item::set_Stack(System.Int32)
extern void Item_set_Stack_m29FBA73C29132F15E2CC8CE325EB96853F401710 (void);
// 0x0000011D System.Int32 DevionGames.InventorySystem.Item::get_MaxStack()
extern void Item_get_MaxStack_m001C7CD1991CEB33C51B605638E599F6586784D1 (void);
// 0x0000011E System.Boolean DevionGames.InventorySystem.Item::get_IsDroppable()
extern void Item_get_IsDroppable_mC460868B08AEAD6DF94CC07E9F784C7CD1B3242F (void);
// 0x0000011F UnityEngine.AudioClip DevionGames.InventorySystem.Item::get_DropSound()
extern void Item_get_DropSound_mDA9BE818659DA3E7576D4FD781BEAE7E34E84E75 (void);
// 0x00000120 UnityEngine.GameObject DevionGames.InventorySystem.Item::get_OverridePrefab()
extern void Item_get_OverridePrefab_mCDB40AE39079E2951E3A26FBE5C6A1A9762F058E (void);
// 0x00000121 System.Boolean DevionGames.InventorySystem.Item::get_IsCraftable()
extern void Item_get_IsCraftable_m53555B158AEA489EB356BDEB57B3B090227F6358 (void);
// 0x00000122 System.Single DevionGames.InventorySystem.Item::get_CraftingDuration()
extern void Item_get_CraftingDuration_m561C905DD5CC974FFDF0173DACC656A976386B4C (void);
// 0x00000123 System.Boolean DevionGames.InventorySystem.Item::get_UseCraftingSkill()
extern void Item_get_UseCraftingSkill_mB1B254CB27615E03774C38C82DF65A263476E5EA (void);
// 0x00000124 System.String DevionGames.InventorySystem.Item::get_SkillWindow()
extern void Item_get_SkillWindow_m71BF6B435DDE78D0A779CA74B2BF2C28DC3E021F (void);
// 0x00000125 DevionGames.InventorySystem.Skill DevionGames.InventorySystem.Item::get_CraftingSkill()
extern void Item_get_CraftingSkill_m1C9D2B74FA8F0E4F11954FCD926EC3CA6E2FBFA6 (void);
// 0x00000126 System.Boolean DevionGames.InventorySystem.Item::get_RemoveIngredientsWhenFailed()
extern void Item_get_RemoveIngredientsWhenFailed_m8EE0146655784C5A101F3D52A393D8F80D8E3D4A (void);
// 0x00000127 System.Single DevionGames.InventorySystem.Item::get_MinCraftingSkillValue()
extern void Item_get_MinCraftingSkillValue_mBD3D7F63DED5A1085970C8FDD7059C0F8F6A3BF3 (void);
// 0x00000128 System.String DevionGames.InventorySystem.Item::get_CraftingAnimatorState()
extern void Item_get_CraftingAnimatorState_m2C74AAA92FCF29EB3C358036C9FD3C436E4CFA24 (void);
// 0x00000129 DevionGames.InventorySystem.ItemModifierList DevionGames.InventorySystem.Item::get_CraftingModifier()
extern void Item_get_CraftingModifier_m079342E1042104C92F04609D62102A177551F568 (void);
// 0x0000012A System.Void DevionGames.InventorySystem.Item::set_CraftingModifier(DevionGames.InventorySystem.ItemModifierList)
extern void Item_set_CraftingModifier_mFDACA6275476D5A8561B5C0B47B04544DD79371B (void);
// 0x0000012B DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Item::get_Container()
extern void Item_get_Container_m7F5E433EB6970B45D2C036DC702BC4412F561CDF (void);
// 0x0000012C DevionGames.InventorySystem.Slot DevionGames.InventorySystem.Item::get_Slot()
extern void Item_get_Slot_m7C1D63F71DA9273702C9E0C88E1E007D5B08195B (void);
// 0x0000012D System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.Item::get_Slots()
extern void Item_get_Slots_mAE3B8ECD03E6B17DDF146D9AA5AA2251D9E4EDEE (void);
// 0x0000012E System.Void DevionGames.InventorySystem.Item::set_Slots(System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot>)
extern void Item_set_Slots_mDBB2DFEB8025B1F02534B7FC2BCC5DE47EA07429 (void);
// 0x0000012F System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.Item::get_ReferencedSlots()
extern void Item_get_ReferencedSlots_m15DB7D9F803C55237A745ADA270D87566B24DB02 (void);
// 0x00000130 System.Void DevionGames.InventorySystem.Item::set_ReferencedSlots(System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot>)
extern void Item_set_ReferencedSlots_m0AC9C0D223583A478E52D2418C5D20D50B59DBE6 (void);
// 0x00000131 System.Void DevionGames.InventorySystem.Item::AddProperty(System.String,System.Object)
extern void Item_AddProperty_m8969A08002CDF0D695861C88A67E57CCC8581748 (void);
// 0x00000132 System.Void DevionGames.InventorySystem.Item::RemoveProperty(System.String)
extern void Item_RemoveProperty_mA5106B1FC81339A9D1CEBEB6BE86373B90626D91 (void);
// 0x00000133 DevionGames.ObjectProperty DevionGames.InventorySystem.Item::FindProperty(System.String)
extern void Item_FindProperty_m8E9DE0170CAB1A0036E814F1E84ED3D0AEEB8FF3 (void);
// 0x00000134 DevionGames.ObjectProperty[] DevionGames.InventorySystem.Item::GetProperties()
extern void Item_GetProperties_m45761DE618E4D045D6B312B96A91A02F004E0029 (void);
// 0x00000135 System.Void DevionGames.InventorySystem.Item::SetProperties(DevionGames.ObjectProperty[])
extern void Item_SetProperties_mFBC76141B41920E66BA56FB442B2F9A2D787699B (void);
// 0x00000136 System.Void DevionGames.InventorySystem.Item::OnEnable()
extern void Item_OnEnable_m9A8B0DBE7D3EB46AF649C8801B911B39B16ED79F (void);
// 0x00000137 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> DevionGames.InventorySystem.Item::GetPropertyInfo()
extern void Item_GetPropertyInfo_m9375BAAD43D6C4B78059B15451509F462D127453 (void);
// 0x00000138 System.String DevionGames.InventorySystem.Item::FormatPropertyValue(DevionGames.ObjectProperty)
extern void Item_FormatPropertyValue_m815C7728414997897FA7B785FBFA2B7635820E5D (void);
// 0x00000139 System.Void DevionGames.InventorySystem.Item::Use()
extern void Item_Use_mDEF5065C85B35D44020E09EF36AF1E877C850CF0 (void);
// 0x0000013A System.Void DevionGames.InventorySystem.Item::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Item_GetObjectData_m4F8AEEAE984B434681DC1333E1C57622B4DE79A9 (void);
// 0x0000013B System.Void DevionGames.InventorySystem.Item::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Item_SetObjectData_mF61BBCE72F73B1EB004D8B6EAD53CA2A29176F15 (void);
// 0x0000013C System.Void DevionGames.InventorySystem.Item::.ctor()
extern void Item__ctor_mC5E29BFEF8CEF634E46ADEF707858EC8AB27592F (void);
// 0x0000013D System.Void DevionGames.InventorySystem.Item/Ingredient::.ctor()
extern void Ingredient__ctor_m04BEA8A6F71AC8C94F4E609CCBD6061D8B243CC5 (void);
// 0x0000013E System.Void DevionGames.InventorySystem.Item/<>c__DisplayClass115_0::.ctor()
extern void U3CU3Ec__DisplayClass115_0__ctor_m99C151AC043E2B1402AA40A3BEACA6FD37E0CDC5 (void);
// 0x0000013F System.Boolean DevionGames.InventorySystem.Item/<>c__DisplayClass115_0::<RemoveProperty>b__0(DevionGames.ObjectProperty)
extern void U3CU3Ec__DisplayClass115_0_U3CRemovePropertyU3Eb__0_m858859620F96F1E803A20AA5195E2331F31D72D4 (void);
// 0x00000140 System.Void DevionGames.InventorySystem.Item/<>c__DisplayClass116_0::.ctor()
extern void U3CU3Ec__DisplayClass116_0__ctor_m2A45B266B276928DF4EFD7BB7EF2375D7F0AE4B4 (void);
// 0x00000141 System.Boolean DevionGames.InventorySystem.Item/<>c__DisplayClass116_0::<FindProperty>b__0(DevionGames.ObjectProperty)
extern void U3CU3Ec__DisplayClass116_0_U3CFindPropertyU3Eb__0_m5DA2BF5197E5E8BF6961A0D7EA2622FBEAB5CCC0 (void);
// 0x00000142 System.Single DevionGames.InventorySystem.Skill::get_CurrentValue()
extern void Skill_get_CurrentValue_m4CB43217FFFEA726849A3AE4BCD4C176E6258752 (void);
// 0x00000143 System.Void DevionGames.InventorySystem.Skill::set_CurrentValue(System.Single)
extern void Skill_set_CurrentValue_m05259265E725BDDE56B4DA1937993DB5CC10531B (void);
// 0x00000144 DevionGames.InventorySystem.SkillModifier DevionGames.InventorySystem.Skill::get_GainModifier()
extern void Skill_get_GainModifier_mA9583C5DD66E63548DEA93641758ECDD2169E195 (void);
// 0x00000145 System.Void DevionGames.InventorySystem.Skill::set_GainModifier(DevionGames.InventorySystem.SkillModifier)
extern void Skill_set_GainModifier_m09C88D51AF393712758DC788F2689C1399E11141 (void);
// 0x00000146 System.Boolean DevionGames.InventorySystem.Skill::CheckSkill()
extern void Skill_CheckSkill_m767FE99ADF0FAB6BC4A11C25E930F1BAB3391777 (void);
// 0x00000147 System.Void DevionGames.InventorySystem.Skill::GetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Skill_GetObjectData_mE6039C02AC74CE3A3EA143B31A9C6A3BE6E1F186 (void);
// 0x00000148 System.Void DevionGames.InventorySystem.Skill::SetObjectData(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Skill_SetObjectData_m676DBBFB2939E154461D607F535FD64A0366930F (void);
// 0x00000149 System.Void DevionGames.InventorySystem.Skill::.ctor()
extern void Skill__ctor_m4EA6EF4E275B3FA01E6B518694B11BD521B9BCEE (void);
// 0x0000014A System.Single DevionGames.InventorySystem.UsableItem::get_Cooldown()
extern void UsableItem_get_Cooldown_m92F24ECB8F5B92C83F774F4A08B07D5269B928FB (void);
// 0x0000014B System.Void DevionGames.InventorySystem.UsableItem::OnEnable()
extern void UsableItem_OnEnable_m6480298E8907F939BAD07BD72F53AFBB43819565 (void);
// 0x0000014C System.Void DevionGames.InventorySystem.UsableItem::Use()
extern void UsableItem_Use_mB0193A80D617795ED9C1E4128E6E3A7D0B82703F (void);
// 0x0000014D System.Collections.IEnumerator DevionGames.InventorySystem.UsableItem::SequenceCoroutine()
extern void UsableItem_SequenceCoroutine_m318A407A7B94147DD078E37D2D08C43C775EA045 (void);
// 0x0000014E System.Void DevionGames.InventorySystem.UsableItem::.ctor()
extern void UsableItem__ctor_mE23FE937BEFC8E75ACFAF37237025DB681CF1819 (void);
// 0x0000014F System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::.ctor(System.Int32)
extern void U3CSequenceCoroutineU3Ed__9__ctor_mD0D6AB8E654C1550E7FCD108F6B70B84E7BB1B05 (void);
// 0x00000150 System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.IDisposable.Dispose()
extern void U3CSequenceCoroutineU3Ed__9_System_IDisposable_Dispose_m12DBD516F68B381F1311AF75F52DC4079F86D56C (void);
// 0x00000151 System.Boolean DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::MoveNext()
extern void U3CSequenceCoroutineU3Ed__9_MoveNext_m242BC4E9F62F2AA66CBD209A64F38ACC8BECC5A5 (void);
// 0x00000152 System.Object DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m887024614CCE999B4A10D64456BC90F39F930864 (void);
// 0x00000153 System.Void DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.IEnumerator.Reset()
extern void U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863 (void);
// 0x00000154 System.Object DevionGames.InventorySystem.UsableItem/<SequenceCoroutine>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_m2DC9FF56B6AA56E737B6002C8AE95781725548AD (void);
// 0x00000155 System.Void DevionGames.InventorySystem.IModifier`1::Modify(T)
// 0x00000156 System.Void DevionGames.InventorySystem.ItemModifier::Modify(DevionGames.InventorySystem.Item)
// 0x00000157 System.Void DevionGames.InventorySystem.ItemModifier::.ctor()
extern void ItemModifier__ctor_mA89A1F54C43102B98529F4AFB5A71A5169173733 (void);
// 0x00000158 System.Void DevionGames.InventorySystem.ItemModifierHandler::ApplyModifiers(DevionGames.InventorySystem.Item)
extern void ItemModifierHandler_ApplyModifiers_m7B1676F97ADCADBE31EF165033F0A8355B5323BF (void);
// 0x00000159 System.Void DevionGames.InventorySystem.ItemModifierHandler::ApplyModifiers(DevionGames.InventorySystem.Item[])
extern void ItemModifierHandler_ApplyModifiers_m606BDEFCCCBC841FE4FE1454D7CB1C26BDE9A001 (void);
// 0x0000015A System.Void DevionGames.InventorySystem.ItemModifierHandler::.ctor()
extern void ItemModifierHandler__ctor_m68E489CB1EDBFBF054E1D3D0D9E5F51CA1015E30 (void);
// 0x0000015B System.Void DevionGames.InventorySystem.ItemModifierList::Modify(DevionGames.InventorySystem.Item)
extern void ItemModifierList_Modify_m60ED1BB76F55CD94927DFFEFD8889124814FD5AE (void);
// 0x0000015C System.Void DevionGames.InventorySystem.ItemModifierList::.ctor()
extern void ItemModifierList__ctor_mE9687C66E8AB78D39A8D614C02327B4D1D73EA4F (void);
// 0x0000015D System.Void DevionGames.InventorySystem.PropertyModifier::Modify(DevionGames.InventorySystem.Item)
extern void PropertyModifier_Modify_mEC0DE08982F8122141A400A99941702636D615FE (void);
// 0x0000015E System.Void DevionGames.InventorySystem.PropertyModifier::.ctor()
extern void PropertyModifier__ctor_m5A0A979E4E6ED5E55D997738229C882417D36C2F (void);
// 0x0000015F System.Void DevionGames.InventorySystem.RarityModifier::Modify(DevionGames.InventorySystem.Item)
extern void RarityModifier_Modify_m6E9B5AEE0896325223464BDF01142AE080A9153D (void);
// 0x00000160 System.Void DevionGames.InventorySystem.RarityModifier::ApplyPropertyMultiplier(DevionGames.InventorySystem.Item,System.Single)
extern void RarityModifier_ApplyPropertyMultiplier_mBB5BAE333006D6EF8C13B5289B1C5552E389358C (void);
// 0x00000161 DevionGames.InventorySystem.Rarity DevionGames.InventorySystem.RarityModifier::SelectRarity(System.Collections.Generic.List`1<DevionGames.InventorySystem.Rarity>)
extern void RarityModifier_SelectRarity_mD43C33889A2503405CA7ECD360792C3376978799 (void);
// 0x00000162 System.Void DevionGames.InventorySystem.RarityModifier::.ctor()
extern void RarityModifier__ctor_m4F3231F880BA070B6A6135B37CB5C972E2E42169 (void);
// 0x00000163 System.Void DevionGames.InventorySystem.SkillModifier::Modify(DevionGames.InventorySystem.Skill)
extern void SkillModifier_Modify_mEE05B09BC52EEA902EFC48614A75740E23CDCF7E (void);
// 0x00000164 System.Void DevionGames.InventorySystem.SkillModifier::.ctor()
extern void SkillModifier__ctor_mAA3110007C103374A4645D92A655BCEEFBAE9EB6 (void);
// 0x00000165 System.Void DevionGames.InventorySystem.StackModifier::Modify(DevionGames.InventorySystem.Item)
extern void StackModifier_Modify_m988FBF18B703FE307729F8D7D33E072E481679EF (void);
// 0x00000166 System.Void DevionGames.InventorySystem.StackModifier::.ctor()
extern void StackModifier__ctor_m2AE8A5A2BB738B83C75590B533A8E8F7D57343CE (void);
// 0x00000167 System.Void DevionGames.InventorySystem.PlaceItem::Start()
extern void PlaceItem_Start_m08173BC180D32D6A775849B90BCC23FBDF87A13E (void);
// 0x00000168 System.Void DevionGames.InventorySystem.PlaceItem::Update()
extern void PlaceItem_Update_m26A3F61C13E8B061795C427F466FFC3D90E1A0B6 (void);
// 0x00000169 System.Void DevionGames.InventorySystem.PlaceItem::Build()
extern void PlaceItem_Build_mB87F750289475FEAF081BECCAED80A6C66DAC5D9 (void);
// 0x0000016A System.Void DevionGames.InventorySystem.PlaceItem::SetColor(UnityEngine.Color)
extern void PlaceItem_SetColor_mC71799A90FA0C7CC124B3C757335E3CB0C7113F7 (void);
// 0x0000016B System.Boolean DevionGames.InventorySystem.PlaceItem::CanPlace()
extern void PlaceItem_CanPlace_m591E85D95FBE79114F7812C1F6C7C67372720B4A (void);
// 0x0000016C System.Single DevionGames.InventorySystem.PlaceItem::GetMaxCornerHeight()
extern void PlaceItem_GetMaxCornerHeight_m5E122304A4342B9ABFBB2682F8C07919208F8BAE (void);
// 0x0000016D System.Void DevionGames.InventorySystem.PlaceItem::.ctor()
extern void PlaceItem__ctor_m0C53F427F341834E2CA63A46DEAAE2CCBB9CBE1D (void);
// 0x0000016E System.Void DevionGames.InventorySystem.Projectile::Start()
extern void Projectile_Start_m5BE8CDE1249DB66840856B6EC50CB3FEDFF8F22C (void);
// 0x0000016F System.Void DevionGames.InventorySystem.Projectile::FixedUpdate()
extern void Projectile_FixedUpdate_mCADC3918A8902EA129725776F7ADDCE3FD86CE5C (void);
// 0x00000170 UnityEngine.GameObject DevionGames.InventorySystem.Projectile::GetTarget()
extern void Projectile_GetTarget_m5541F87662A9DABFCF0872BE01AC8B42D6FC73FF (void);
// 0x00000171 System.Void DevionGames.InventorySystem.Projectile::SetStartDirection(UnityEngine.Vector3)
extern void Projectile_SetStartDirection_m12038BC58FC764E2F34FB510D076012F24251CAF (void);
// 0x00000172 System.Boolean DevionGames.InventorySystem.Projectile::CheckFieldOfView(UnityEngine.GameObject,UnityEngine.GameObject,System.Single)
extern void Projectile_CheckFieldOfView_mE9FC77367A6497CA4B6F270CD34819FF5CEE3FDD (void);
// 0x00000173 System.Void DevionGames.InventorySystem.Projectile::OnCollisionEnter(UnityEngine.Collision)
extern void Projectile_OnCollisionEnter_m5F8B93D57791399E93CC0FDDEE8B6984C4C1450D (void);
// 0x00000174 System.Void DevionGames.InventorySystem.Projectile::OnHit(UnityEngine.Transform,UnityEngine.Vector3)
extern void Projectile_OnHit_mDAFE6ABB55F814B9BDD2B81BB9694A19F5331B2D (void);
// 0x00000175 System.Void DevionGames.InventorySystem.Projectile::.ctor()
extern void Projectile__ctor_m0EC4D9F2D3003EB7F00FF17A4973D9002437730C (void);
// 0x00000176 System.Void DevionGames.InventorySystem.Projectile/<>c::.cctor()
extern void U3CU3Ec__cctor_m6690FDA4E8AD1A72FDEB11F4442DBE7D8A3F0A18 (void);
// 0x00000177 System.Void DevionGames.InventorySystem.Projectile/<>c::.ctor()
extern void U3CU3Ec__ctor_mAE34D66987FBBB74F62935C8E358292FFD8E1453 (void);
// 0x00000178 System.Boolean DevionGames.InventorySystem.Projectile/<>c::<GetTarget>b__18_0(UnityEngine.Collider)
extern void U3CU3Ec_U3CGetTargetU3Eb__18_0_m82749985C0FEC325E7953876524E65224292FB8D (void);
// 0x00000179 System.String DevionGames.InventorySystem.Rarity::get_Name()
extern void Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21 (void);
// 0x0000017A System.Void DevionGames.InventorySystem.Rarity::set_Name(System.String)
extern void Rarity_set_Name_m18C9AF8987D0433B04ED1772B2F1159C59C48FD9 (void);
// 0x0000017B System.Boolean DevionGames.InventorySystem.Rarity::get_UseAsNamePrefix()
extern void Rarity_get_UseAsNamePrefix_m1BFC426CD23E5432C0CF2B0214CFABDE147B7FF2 (void);
// 0x0000017C UnityEngine.Color DevionGames.InventorySystem.Rarity::get_Color()
extern void Rarity_get_Color_m9D433820F051AD1EEEFADCFE348BAADA21066420 (void);
// 0x0000017D System.Void DevionGames.InventorySystem.Rarity::set_Color(UnityEngine.Color)
extern void Rarity_set_Color_mACE4035145A4E96C87A3700261CA6FF5ABB6F2DA (void);
// 0x0000017E System.Int32 DevionGames.InventorySystem.Rarity::get_Chance()
extern void Rarity_get_Chance_m175BDB7BBC2C946ACC12AE807D30556F190CED4E (void);
// 0x0000017F System.Void DevionGames.InventorySystem.Rarity::set_Chance(System.Int32)
extern void Rarity_set_Chance_m158CECA2A13EFFC8B2B5150555AC03DAEDA62B7B (void);
// 0x00000180 System.Single DevionGames.InventorySystem.Rarity::get_Multiplier()
extern void Rarity_get_Multiplier_m865268FA667BB95371E9E23A2ED3F36D1FABC0A7 (void);
// 0x00000181 System.Void DevionGames.InventorySystem.Rarity::set_Multiplier(System.Single)
extern void Rarity_set_Multiplier_mF3491850D66D4AAFD6BE0CF6D27718587E987FCD (void);
// 0x00000182 System.Single DevionGames.InventorySystem.Rarity::get_PriceMultiplier()
extern void Rarity_get_PriceMultiplier_m6F427CB2501D3E68BDB1D42D5549314510B521FD (void);
// 0x00000183 System.Void DevionGames.InventorySystem.Rarity::set_PriceMultiplier(System.Single)
extern void Rarity_set_PriceMultiplier_mFA3E782987E7A4DFC8F2538E2322908A8238DC4C (void);
// 0x00000184 System.Void DevionGames.InventorySystem.Rarity::.ctor()
extern void Rarity__ctor_m2A5936C41FC4C9D7CD921C37956C9ABE3AE7F95E (void);
// 0x00000185 System.Void DevionGames.InventorySystem.NotificationExtension::Show(DevionGames.UIWidgets.NotificationOptions,System.String[])
extern void NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1 (void);
// 0x00000186 System.Void DevionGames.InventorySystem.CompareNumberNotify::OnStart()
extern void CompareNumberNotify_OnStart_m951E077108A2AB943365D4FBD820EDCD5C69AEEB (void);
// 0x00000187 DevionGames.ActionStatus DevionGames.InventorySystem.CompareNumberNotify::OnUpdate()
extern void CompareNumberNotify_OnUpdate_mBC555EE9D27AE831B5945B773592347C0A658C4D (void);
// 0x00000188 System.Void DevionGames.InventorySystem.CompareNumberNotify::.ctor()
extern void CompareNumberNotify__ctor_m92420E92461EEC51DFA51B14FFB7773CCE706AF5 (void);
// 0x00000189 System.Void DevionGames.InventorySystem.InvokeNotify::OnStart()
extern void InvokeNotify_OnStart_mAF015EEC274AA0278FB344F84568961558C85D63 (void);
// 0x0000018A DevionGames.ActionStatus DevionGames.InventorySystem.InvokeNotify::OnUpdate()
extern void InvokeNotify_OnUpdate_m5B40CB44635F0D150DDB0EBBE54BE0B01A1B6F02 (void);
// 0x0000018B System.Void DevionGames.InventorySystem.InvokeNotify::.ctor()
extern void InvokeNotify__ctor_mD850C60BB3D3767C7549F44D9000B88E69CE0316 (void);
// 0x0000018C System.Void DevionGames.InventorySystem.CanPickup::OnStart()
extern void CanPickup_OnStart_mE261AB8ECE6B43A506DD38FA342093ACEC65CC71 (void);
// 0x0000018D DevionGames.ActionStatus DevionGames.InventorySystem.CanPickup::OnUpdate()
extern void CanPickup_OnUpdate_m43A3BECF904A969EF5313D2A5BD493E035C85DD6 (void);
// 0x0000018E System.Void DevionGames.InventorySystem.CanPickup::.ctor()
extern void CanPickup__ctor_m5ACCE1B60AF8E0407917E38CFC9E21E06D6A9F25 (void);
// 0x0000018F DevionGames.ActionStatus DevionGames.InventorySystem.HasCategoryItem::OnUpdate()
extern void HasCategoryItem_OnUpdate_m9C71C562C952256CA48A72E4FC0CD0E4C9B9F5AB (void);
// 0x00000190 System.Void DevionGames.InventorySystem.HasCategoryItem::.ctor()
extern void HasCategoryItem__ctor_m276EEBFE9B8303A2A30FBDC71F997E5640926CFA (void);
// 0x00000191 DevionGames.ActionStatus DevionGames.InventorySystem.HasGroupItem::OnUpdate()
extern void HasGroupItem_OnUpdate_m4B6826A3642BC56E93DEFAAE9F3B944650021F52 (void);
// 0x00000192 System.Void DevionGames.InventorySystem.HasGroupItem::.ctor()
extern void HasGroupItem__ctor_mBE92C33C043577A66C0A99E0F966409056EDC2C4 (void);
// 0x00000193 DevionGames.ActionStatus DevionGames.InventorySystem.HasItem::OnUpdate()
extern void HasItem_OnUpdate_m566ECE7943A7A063FD4D5B769145D67A4FFFC311 (void);
// 0x00000194 System.Void DevionGames.InventorySystem.HasItem::.ctor()
extern void HasItem__ctor_mD297085A790CBA7F897E71D304CFA276499DC551 (void);
// 0x00000195 System.Void DevionGames.InventorySystem.Lock::OnStart()
extern void Lock_OnStart_m41293A641BEDBD3288B5D614E54A4FFB6AA172BD (void);
// 0x00000196 DevionGames.ActionStatus DevionGames.InventorySystem.Lock::OnUpdate()
extern void Lock_OnUpdate_m839EA5F9AF1E8EA39155E598A0AB6659599686CC (void);
// 0x00000197 System.Void DevionGames.InventorySystem.Lock::.ctor()
extern void Lock__ctor_m3433C011B796CC21D419CE95F833946C5B9BDEFD (void);
// 0x00000198 DevionGames.ActionStatus DevionGames.InventorySystem.LockAll::OnUpdate()
extern void LockAll_OnUpdate_m60015AC729F9D040F30CF6D1B2A9CD844245D003 (void);
// 0x00000199 System.Void DevionGames.InventorySystem.LockAll::.ctor()
extern void LockAll__ctor_mD8299DEAD074C98EB232844FD3E23169E17A84CA (void);
// 0x0000019A System.Void DevionGames.InventorySystem.Pickup::OnStart()
extern void Pickup_OnStart_m2AA2A7004B2B3C22EF443A8A35375BA6541ED77B (void);
// 0x0000019B DevionGames.ActionStatus DevionGames.InventorySystem.Pickup::OnUpdate()
extern void Pickup_OnUpdate_m5EF4DEBEEB7038B97688C814A8BB0BA058EEC149 (void);
// 0x0000019C DevionGames.ActionStatus DevionGames.InventorySystem.Pickup::PickupItems()
extern void Pickup_PickupItems_mF079F45DF4A77A2A1ACBE23E7E8E5F1F73D61E3B (void);
// 0x0000019D System.Void DevionGames.InventorySystem.Pickup::DropItem(DevionGames.InventorySystem.Item)
extern void Pickup_DropItem_m01642CB6A93C450F365D2DEF5AAB4F05AF1A8605 (void);
// 0x0000019E System.Void DevionGames.InventorySystem.Pickup::.ctor()
extern void Pickup__ctor_mDF4A6AD02532D0603ACF05A661441CE1D06EDC88 (void);
// 0x0000019F System.Void DevionGames.InventorySystem.Pickup::<OnStart>b__4_0()
extern void Pickup_U3COnStartU3Eb__4_0_m20B8279A641750A5962EEC43C0DB6F093D93C95D (void);
// 0x000001A0 DevionGames.ActionStatus DevionGames.InventorySystem.Save::OnUpdate()
extern void Save_OnUpdate_mB7333962A717C8687143C0133175032BF951BCBB (void);
// 0x000001A1 System.Void DevionGames.InventorySystem.Save::.ctor()
extern void Save__ctor_m13276C645E34589C2D840CFE1B3F5779F88BE3C5 (void);
// 0x000001A2 System.Void DevionGames.InventorySystem.ShowWindow::OnSequenceStart()
extern void ShowWindow_OnSequenceStart_m5BFCDAEFD34EBEF4759CBAC5D17CAD3BC5E7BF63 (void);
// 0x000001A3 System.Void DevionGames.InventorySystem.ShowWindow::OnTriggerUnUsed(UnityEngine.GameObject)
extern void ShowWindow_OnTriggerUnUsed_mCC90EA833A95E3787F42BAA210764275B05AD57B (void);
// 0x000001A4 DevionGames.ActionStatus DevionGames.InventorySystem.ShowWindow::OnUpdate()
extern void ShowWindow_OnUpdate_mF6C09EA6D93B25248A0EB651298E8F827BD8A76D (void);
// 0x000001A5 System.Void DevionGames.InventorySystem.ShowWindow::.ctor()
extern void ShowWindow__ctor_m96FCDBCE8A397C30FE26B3278D4D25FB4ED185C3 (void);
// 0x000001A6 System.Void DevionGames.InventorySystem.ShowWindow::<OnSequenceStart>b__5_0(DevionGames.CallbackEventData)
extern void ShowWindow_U3COnSequenceStartU3Eb__5_0_mE8B5127A81299FDC5B31D9129E6BD811DDFF3A04 (void);
// 0x000001A7 System.Void DevionGames.InventorySystem.ShowWindow::<OnSequenceStart>b__5_1()
extern void ShowWindow_U3COnSequenceStartU3Eb__5_1_mFB0E22A61357DA278D83156338B29F01990D4DB3 (void);
// 0x000001A8 DevionGames.ActionStatus DevionGames.InventorySystem.RaycastNotify::OnUpdate()
extern void RaycastNotify_OnUpdate_mC0A258CA695955DAB0FA65701B838CA299693166 (void);
// 0x000001A9 System.Void DevionGames.InventorySystem.RaycastNotify::.ctor()
extern void RaycastNotify__ctor_m09D1936822A63545EAB8EEBE9D2327B9CB145656 (void);
// 0x000001AA System.Void DevionGames.InventorySystem.CheckSkill::OnStart()
extern void CheckSkill_OnStart_m623E049DFC207E9021B6CFF8F21135B99B462500 (void);
// 0x000001AB DevionGames.ActionStatus DevionGames.InventorySystem.CheckSkill::OnUpdate()
extern void CheckSkill_OnUpdate_m74C47D735A7837E83B099E6AA7E13093973D500B (void);
// 0x000001AC System.Void DevionGames.InventorySystem.CheckSkill::.ctor()
extern void CheckSkill__ctor_mEC0BF1818AC022820899BAE823A310B70988754C (void);
// 0x000001AD DevionGames.ActionStatus DevionGames.InventorySystem.Close::OnUpdate()
extern void Close_OnUpdate_m2180376A33212DF47FEE4275A4147656F1AA2564 (void);
// 0x000001AE System.Void DevionGames.InventorySystem.Close::.ctor()
extern void Close__ctor_mFCE7C28BD03C8F9F7579F06DA9CCC8E4A6DF1622 (void);
// 0x000001AF DevionGames.ActionStatus DevionGames.InventorySystem.ShowNotification::OnUpdate()
extern void ShowNotification_OnUpdate_mE9113D77B4861183C5626AC2A2765A3228842D7F (void);
// 0x000001B0 System.Void DevionGames.InventorySystem.ShowNotification::.ctor()
extern void ShowNotification__ctor_mD3783DAD38280108F59B6D2C2A3D9829F21DC532 (void);
// 0x000001B1 System.Void DevionGames.InventorySystem.ShowProgressbar::OnStart()
extern void ShowProgressbar_OnStart_m33D5F7EB7C6D514FACAE881127BDD6C257982768 (void);
// 0x000001B2 DevionGames.ActionStatus DevionGames.InventorySystem.ShowProgressbar::OnUpdate()
extern void ShowProgressbar_OnUpdate_m7B52403D82A425DC1C37B45F008A9BD07347ED39 (void);
// 0x000001B3 System.Void DevionGames.InventorySystem.ShowProgressbar::OnInterrupt()
extern void ShowProgressbar_OnInterrupt_m5FFFD1E02030C3CA0C9134389D72FC31CD4D2547 (void);
// 0x000001B4 System.Void DevionGames.InventorySystem.ShowProgressbar::.ctor()
extern void ShowProgressbar__ctor_m3ED0255D36E5991BA0080A17FB103BC5E34BA3F4 (void);
// 0x000001B5 System.String[] DevionGames.InventorySystem.CraftingTrigger::get_Callbacks()
extern void CraftingTrigger_get_Callbacks_m4E5E0F9C4C5F28EE6168C7BD7062D1B14EF33007 (void);
// 0x000001B6 System.Void DevionGames.InventorySystem.CraftingTrigger::Execute(DevionGames.InventorySystem.ITriggerFailedCraftStart,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
extern void CraftingTrigger_Execute_m9FBF9BC4E7F878672D91DCF546C3533AA92B63C8 (void);
// 0x000001B7 System.Void DevionGames.InventorySystem.CraftingTrigger::Execute(DevionGames.InventorySystem.ITriggerCraftStart,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void CraftingTrigger_Execute_m9564B10617AED6B3FDFF8EA32FFEA242C09A3B7B (void);
// 0x000001B8 System.Void DevionGames.InventorySystem.CraftingTrigger::Execute(DevionGames.InventorySystem.ITriggerCraftItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void CraftingTrigger_Execute_m045DC5CCFE2815CEF745CB2114B01C67A86D127C (void);
// 0x000001B9 System.Void DevionGames.InventorySystem.CraftingTrigger::Execute(DevionGames.InventorySystem.ITriggerFailedToCraftItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
extern void CraftingTrigger_Execute_m90FF3CC58298921D33F709203B544FC004D28AE8 (void);
// 0x000001BA System.Void DevionGames.InventorySystem.CraftingTrigger::Execute(DevionGames.InventorySystem.ITriggerCraftStop,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void CraftingTrigger_Execute_m1868C3831BD25BA5073D7D41DE8D78DAAFE0472E (void);
// 0x000001BB System.Void DevionGames.InventorySystem.CraftingTrigger::Start()
extern void CraftingTrigger_Start_mD54D97D1CF287651A9AF03E372A5687E83C17F3D (void);
// 0x000001BC System.Boolean DevionGames.InventorySystem.CraftingTrigger::OverrideUse(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item)
extern void CraftingTrigger_OverrideUse_m0E0C64AD64C90AF3FE5E9FF5546AB86C41A123D5 (void);
// 0x000001BD System.Void DevionGames.InventorySystem.CraftingTrigger::Update()
extern void CraftingTrigger_Update_mEFDAD961489B387B251DAB89A52CCEE49A453572 (void);
// 0x000001BE System.Void DevionGames.InventorySystem.CraftingTrigger::OnTriggerInterrupted()
extern void CraftingTrigger_OnTriggerInterrupted_m6050953FB59D6C257D0A6BF7C1041926942EE671 (void);
// 0x000001BF System.Single DevionGames.InventorySystem.CraftingTrigger::GetCraftingProgress()
extern void CraftingTrigger_GetCraftingProgress_m4E70A372B2DCF7D6ADFC7D9B8D01F1CD5C3E1633 (void);
// 0x000001C0 System.Void DevionGames.InventorySystem.CraftingTrigger::StartCrafting(DevionGames.InventorySystem.Item,System.Int32)
extern void CraftingTrigger_StartCrafting_m5C8BF11A0EC0B7AF9C2477013886EAE602C821A4 (void);
// 0x000001C1 System.Boolean DevionGames.InventorySystem.CraftingTrigger::HasIngredients(DevionGames.InventorySystem.ItemContainer,DevionGames.InventorySystem.Item)
extern void CraftingTrigger_HasIngredients_mCBED78ACA1ABF6D7A57CF4EBCA561892D592704C (void);
// 0x000001C2 System.Void DevionGames.InventorySystem.CraftingTrigger::StopCrafting(DevionGames.InventorySystem.Item)
extern void CraftingTrigger_StopCrafting_mADF0752F766BEAFBFB182212590979D2667E44BF (void);
// 0x000001C3 System.Collections.IEnumerator DevionGames.InventorySystem.CraftingTrigger::CraftItems(DevionGames.InventorySystem.Item,System.Int32)
extern void CraftingTrigger_CraftItems_mF124FE6834334A72E006E5E84175D9156221722C (void);
// 0x000001C4 System.Collections.IEnumerator DevionGames.InventorySystem.CraftingTrigger::CraftItem(DevionGames.InventorySystem.Item)
extern void CraftingTrigger_CraftItem_m3FE76B80A298C4764A8874300B7E20F3EC5B3E1F (void);
// 0x000001C5 System.Void DevionGames.InventorySystem.CraftingTrigger::RegisterCallbacks()
extern void CraftingTrigger_RegisterCallbacks_m046CAFC04522A779EC4A1D9B9B59242FF35D2F6F (void);
// 0x000001C6 System.Void DevionGames.InventorySystem.CraftingTrigger::.ctor()
extern void CraftingTrigger__ctor_mEDA3A766D4ABD7A060D8BD4EEDE04E0ABB3DEB74 (void);
// 0x000001C7 System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::.ctor(System.Int32)
extern void U3CCraftItemsU3Ed__26__ctor_m05708EC26C38D37AB8EB955B751847FAB7A7A502 (void);
// 0x000001C8 System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::System.IDisposable.Dispose()
extern void U3CCraftItemsU3Ed__26_System_IDisposable_Dispose_m82915AB51DD42745B2BF0291F3903587E90F2878 (void);
// 0x000001C9 System.Boolean DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::MoveNext()
extern void U3CCraftItemsU3Ed__26_MoveNext_m68F080A8B767617F4A8F9928C7A06DA3F4FAFCF1 (void);
// 0x000001CA System.Object DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCraftItemsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED7A8123703EA3554A6C3957508C98C877F48B1 (void);
// 0x000001CB System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::System.Collections.IEnumerator.Reset()
extern void U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_Reset_m03ED5FA780BBAFC1BF44C998C90A7B700D99A7A2 (void);
// 0x000001CC System.Object DevionGames.InventorySystem.CraftingTrigger/<CraftItems>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_get_Current_m8AB92C4E2BE021342710E731BEC348A687EAF066 (void);
// 0x000001CD System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::.ctor(System.Int32)
extern void U3CCraftItemU3Ed__27__ctor_m6BE66FA9C0DD8A3C7BA790E9845C78A41C720F09 (void);
// 0x000001CE System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::System.IDisposable.Dispose()
extern void U3CCraftItemU3Ed__27_System_IDisposable_Dispose_mF7F94E569233B993520A310A200FE681D03224E0 (void);
// 0x000001CF System.Boolean DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::MoveNext()
extern void U3CCraftItemU3Ed__27_MoveNext_m3C26F1B98C75F516ADD48F52812A74DAD27CE1C1 (void);
// 0x000001D0 System.Object DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCraftItemU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m620CEE326E27BF4715D831A62854CAFC587F85CE (void);
// 0x000001D1 System.Void DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::System.Collections.IEnumerator.Reset()
extern void U3CCraftItemU3Ed__27_System_Collections_IEnumerator_Reset_m9944ED8FEEFABC00B312E796326D3892ADE1126D (void);
// 0x000001D2 System.Object DevionGames.InventorySystem.CraftingTrigger/<CraftItem>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CCraftItemU3Ed__27_System_Collections_IEnumerator_get_Current_m448E7912969C96DD23A01894022C78EE4501FA94 (void);
// 0x000001D3 System.Void DevionGames.InventorySystem.DisplayCursor::DoDisplayCursor(System.Boolean)
extern void DisplayCursor_DoDisplayCursor_mE001BC466F85BCA385350A2D24DFE44EECD9E2DF (void);
// 0x000001D4 System.Void DevionGames.InventorySystem.DisplayCursor::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void DisplayCursor_OnPointerEnter_m4A79CD0D2484DEA066565607C1BDCF1F13CA6759 (void);
// 0x000001D5 System.Void DevionGames.InventorySystem.DisplayCursor::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void DisplayCursor_OnPointerExit_mD978DCA2C8F91A656E13006EFC1520684EC47AF3 (void);
// 0x000001D6 System.Void DevionGames.InventorySystem.DisplayCursor::.ctor()
extern void DisplayCursor__ctor_m3E7700C0C9A393DDBE38750B1D54C7DC59749916 (void);
// 0x000001D7 System.Void DevionGames.InventorySystem.DisplayName::DoDisplayName(System.Boolean)
extern void DisplayName_DoDisplayName_m44312D8E254F3449B3853A05BD8A519EAC26CAB8 (void);
// 0x000001D8 System.Void DevionGames.InventorySystem.DisplayName::Start()
extern void DisplayName_Start_m3D28C80CCA2B9A52390AE774242D64AADE8B9411 (void);
// 0x000001D9 System.Void DevionGames.InventorySystem.DisplayName::OnDestroy()
extern void DisplayName_OnDestroy_mAD6DB80A11770AAD34A06CC3D2AA7F5E5B41E53A (void);
// 0x000001DA System.Void DevionGames.InventorySystem.DisplayName::OnPointerEnterTrigger()
extern void DisplayName_OnPointerEnterTrigger_mB357666458E22609DF50777399963E618D702FB7 (void);
// 0x000001DB System.Void DevionGames.InventorySystem.DisplayName::OnPointerExitTrigger()
extern void DisplayName_OnPointerExitTrigger_mBFF2FFC0B88DA3195E11BC19E8D5DB1F91363732 (void);
// 0x000001DC System.Void DevionGames.InventorySystem.DisplayName::OnCameInRange(UnityEngine.GameObject)
extern void DisplayName_OnCameInRange_mF855B0EF2217A4AE8AFA833503D25BC220A7EF4A (void);
// 0x000001DD System.Void DevionGames.InventorySystem.DisplayName::OnTriggerUsed(UnityEngine.GameObject)
extern void DisplayName_OnTriggerUsed_m078DB5BD1EF568E5562E432ABEECE88961E14240 (void);
// 0x000001DE System.Void DevionGames.InventorySystem.DisplayName::OnTriggerUnUsed(UnityEngine.GameObject)
extern void DisplayName_OnTriggerUnUsed_m3AAD02CFEA82F5988C0D6B6278402193A4D76B51 (void);
// 0x000001DF System.Void DevionGames.InventorySystem.DisplayName::OnWentOutOfRange(UnityEngine.GameObject)
extern void DisplayName_OnWentOutOfRange_mEE0AFB53BFB72A2E6F6E829EE1F6237FF3BD9CEC (void);
// 0x000001E0 System.Void DevionGames.InventorySystem.DisplayName::.ctor()
extern void DisplayName__ctor_m7D67D6EC70F9F6F3840C3F5E144EFA4E3BC5819C (void);
// 0x000001E1 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::Start()
extern void DisplayTriggerTooltip_Start_m77E33B7BAE28631B98312C53050954CEF53A05C9 (void);
// 0x000001E2 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::Update()
extern void DisplayTriggerTooltip_Update_mC585F5804E17188C963CC7293A1E04BE006F378A (void);
// 0x000001E3 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::DoDisplayTooltip(System.Boolean)
extern void DisplayTriggerTooltip_DoDisplayTooltip_mE264A0BFF96AEDE99C60F567DB2DF0FCF694EAFE (void);
// 0x000001E4 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::OnDestroy()
extern void DisplayTriggerTooltip_OnDestroy_mC5D495CF4A8F7DE4080977B157704FE2ADCB8E5A (void);
// 0x000001E5 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::OnWentOutOfRange(UnityEngine.GameObject)
extern void DisplayTriggerTooltip_OnWentOutOfRange_m86649A7149BBC91D917978BB9F287C47F81DB96D (void);
// 0x000001E6 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::OnTriggerUsed(UnityEngine.GameObject)
extern void DisplayTriggerTooltip_OnTriggerUsed_m4A7DCF7469B6F8CBD1E28F3DCFCF271D0469E287 (void);
// 0x000001E7 System.Void DevionGames.InventorySystem.DisplayTriggerTooltip::.ctor()
extern void DisplayTriggerTooltip__ctor_m562B1C04B07785350E860997B617F6A0CDD8D9FC (void);
// 0x000001E8 DevionGames.PlayerInfo DevionGames.InventorySystem.Trigger::get_PlayerInfo()
extern void Trigger_get_PlayerInfo_m6FF58FE6F4591F4D9FC26337676118DBA70A2958 (void);
// 0x000001E9 System.Void DevionGames.InventorySystem.Trigger::StartUse()
extern void Trigger_StartUse_mC9C0B6E4D6BE74CF6D35680356EB1702D866094A (void);
// 0x000001EA System.Void DevionGames.InventorySystem.Trigger::StartUse(DevionGames.InventorySystem.ItemContainer)
extern void Trigger_StartUse_m33331F5147D1E52B9A108723ECAEC33BCFF4FCA9 (void);
// 0x000001EB System.Void DevionGames.InventorySystem.Trigger::StopUse()
extern void Trigger_StopUse_m13ECB22A5748D3D2F7D9221016C59E4A91287743 (void);
// 0x000001EC System.Boolean DevionGames.InventorySystem.Trigger::OverrideUse(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item)
extern void Trigger_OverrideUse_m8521E2D6B1AD0838FC70A5A78370E1714EEE920C (void);
// 0x000001ED System.Void DevionGames.InventorySystem.Trigger::DisplayInUse()
extern void Trigger_DisplayInUse_m2EB6CDB1C607C3ADF2B9573154E2CFF4316CE431 (void);
// 0x000001EE System.Void DevionGames.InventorySystem.Trigger::DisplayOutOfRange()
extern void Trigger_DisplayOutOfRange_m7629EF16421EE59AF5311FCB7D841B57C07BC3B2 (void);
// 0x000001EF System.Void DevionGames.InventorySystem.Trigger::ExecuteEvent(DevionGames.InventorySystem.Trigger/ItemEventFunction`1<T>,DevionGames.InventorySystem.Item,System.Boolean)
// 0x000001F0 System.Void DevionGames.InventorySystem.Trigger::ExecuteEvent(DevionGames.InventorySystem.Trigger/FailureItemEventFunction`1<T>,DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Trigger/FailureCause,System.Boolean)
// 0x000001F1 System.Void DevionGames.InventorySystem.Trigger::.ctor()
extern void Trigger__ctor_m7FEEB7543A276C3265E04DE09570E2F623DAFACE (void);
// 0x000001F2 System.Void DevionGames.InventorySystem.Trigger/ItemEventFunction`1::.ctor(System.Object,System.IntPtr)
// 0x000001F3 System.Void DevionGames.InventorySystem.Trigger/ItemEventFunction`1::Invoke(T,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x000001F4 System.IAsyncResult DevionGames.InventorySystem.Trigger/ItemEventFunction`1::BeginInvoke(T,DevionGames.InventorySystem.Item,UnityEngine.GameObject,System.AsyncCallback,System.Object)
// 0x000001F5 System.Void DevionGames.InventorySystem.Trigger/ItemEventFunction`1::EndInvoke(System.IAsyncResult)
// 0x000001F6 System.Void DevionGames.InventorySystem.Trigger/FailureItemEventFunction`1::.ctor(System.Object,System.IntPtr)
// 0x000001F7 System.Void DevionGames.InventorySystem.Trigger/FailureItemEventFunction`1::Invoke(T,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
// 0x000001F8 System.IAsyncResult DevionGames.InventorySystem.Trigger/FailureItemEventFunction`1::BeginInvoke(T,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause,System.AsyncCallback,System.Object)
// 0x000001F9 System.Void DevionGames.InventorySystem.Trigger/FailureItemEventFunction`1::EndInvoke(System.IAsyncResult)
// 0x000001FA System.Boolean DevionGames.InventorySystem.Trigger2D::CanUse()
extern void Trigger2D_CanUse_mDEE6528214B9B5160F62A9DD7EE85D034B9C134A (void);
// 0x000001FB System.Void DevionGames.InventorySystem.Trigger2D::CreateTriggerCollider()
extern void Trigger2D_CreateTriggerCollider_m8A5D438610CD2C287E9038977AAC1A63C62F697C (void);
// 0x000001FC System.Void DevionGames.InventorySystem.Trigger2D::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Trigger2D_OnTriggerEnter2D_m2CB7F42E6EE87DF248B0CCE2EDF77AB435BF8195 (void);
// 0x000001FD System.Void DevionGames.InventorySystem.Trigger2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Trigger2D_OnTriggerExit2D_m3B1B2DF520BE915820FE16F2543A42AC58BC963F (void);
// 0x000001FE System.Void DevionGames.InventorySystem.Trigger2D::.ctor()
extern void Trigger2D__ctor_mE816FDFE5F23E6F4477E925A36BC361B15EBBA44 (void);
// 0x000001FF System.Void DevionGames.InventorySystem.ITriggerSelectSellItem::OnSelectSellItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000200 System.Void DevionGames.InventorySystem.ITriggerSoldItem::OnSoldItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000201 System.Void DevionGames.InventorySystem.ITriggerFailedToSellItem::OnFailedToSellItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
// 0x00000202 System.Void DevionGames.InventorySystem.ITriggerSelectBuyItem::OnSelectBuyItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000203 System.Void DevionGames.InventorySystem.ITriggerBoughtItem::OnBoughtItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000204 System.Void DevionGames.InventorySystem.ITriggerFailedToBuyItem::OnFailedToBuyItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
// 0x00000205 System.Void DevionGames.InventorySystem.ITriggerCraftStart::OnCraftStart(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000206 System.Void DevionGames.InventorySystem.ITriggerFailedCraftStart::OnFailedCraftStart(DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
// 0x00000207 System.Void DevionGames.InventorySystem.ITriggerCraftItem::OnCraftItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x00000208 System.Void DevionGames.InventorySystem.ITriggerFailedToCraftItem::OnFailedToCraftItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
// 0x00000209 System.Void DevionGames.InventorySystem.ITriggerCraftStop::OnCraftStop(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
// 0x0000020A System.Void DevionGames.InventorySystem.TriggerTooltip::Show(System.String,System.String)
extern void TriggerTooltip_Show_m7096137670C8921BA2AE6D771BBEDB34DC0E9610 (void);
// 0x0000020B System.Void DevionGames.InventorySystem.TriggerTooltip::.ctor()
extern void TriggerTooltip__ctor_m9199E6CE4D6DCE0CAD89C9EF8D4F116D7903B138 (void);
// 0x0000020C System.String[] DevionGames.InventorySystem.VendorTrigger::get_Callbacks()
extern void VendorTrigger_get_Callbacks_mB95FAB02CD03C9E8F23792FAD2CC369B0D3EE7D4 (void);
// 0x0000020D System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerSelectSellItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void VendorTrigger_Execute_m1FB6E5E6D76E8F3469E026DD3D0889AE6FD8A10B (void);
// 0x0000020E System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerSoldItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void VendorTrigger_Execute_mB468A500B9E7CF8484CD92F319127E19DC46FDE9 (void);
// 0x0000020F System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerFailedToSellItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
extern void VendorTrigger_Execute_m7A19E5208044F9DAB59CC7BBCAC04F3FAB2D97A6 (void);
// 0x00000210 System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerSelectBuyItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void VendorTrigger_Execute_m59E0B0A0941B968EAFFA62818E5076DABC24345B (void);
// 0x00000211 System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerBoughtItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void VendorTrigger_Execute_m8A64AD3388D20CDCA5972CE75639EBDF247B89DB (void);
// 0x00000212 System.Void DevionGames.InventorySystem.VendorTrigger::Execute(DevionGames.InventorySystem.ITriggerFailedToBuyItem,DevionGames.InventorySystem.Item,UnityEngine.GameObject,DevionGames.InventorySystem.Trigger/FailureCause)
extern void VendorTrigger_Execute_m5B0AD53EA75C3CCFE8A44871437E09612C41FE6A (void);
// 0x00000213 System.Void DevionGames.InventorySystem.VendorTrigger::Start()
extern void VendorTrigger_Start_m02A2D2094E0EE88D9904BD86C9F0FF78521FB7A7 (void);
// 0x00000214 System.Boolean DevionGames.InventorySystem.VendorTrigger::OverrideUse(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item)
extern void VendorTrigger_OverrideUse_mFD3D591C866682D5ED054F344F0FEADC401C0164 (void);
// 0x00000215 System.Void DevionGames.InventorySystem.VendorTrigger::BuyItem(DevionGames.InventorySystem.Item,System.Int32,System.Boolean)
extern void VendorTrigger_BuyItem_mBDE414BF84F09670EECB23C15BC94D0701288BC8 (void);
// 0x00000216 System.Void DevionGames.InventorySystem.VendorTrigger::SellItem(DevionGames.InventorySystem.Item,System.Int32,System.Boolean)
extern void VendorTrigger_SellItem_m7D80333E0630E8CEEE68ADD4A58B37294A26920F (void);
// 0x00000217 System.Void DevionGames.InventorySystem.VendorTrigger::OnTriggerUnUsed(UnityEngine.GameObject)
extern void VendorTrigger_OnTriggerUnUsed_m76EC28B4537AB55C61F1F88D32EBBFD5942499AA (void);
// 0x00000218 System.Void DevionGames.InventorySystem.VendorTrigger::RegisterCallbacks()
extern void VendorTrigger_RegisterCallbacks_m6955EFD93658A7827CBD8A50355DCC2EA1450A91 (void);
// 0x00000219 System.Void DevionGames.InventorySystem.VendorTrigger::.ctor()
extern void VendorTrigger__ctor_m7DF4C19841E1729BD14632240433F7A3B3B3B48C (void);
// 0x0000021A System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m69D42811E74A45807862D31B1B2A830C3766D91E (void);
// 0x0000021B System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::<BuyItem>b__0(System.Single)
extern void U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__0_m3E4E578EE14C745E917B0945F8A99A4816F03BEC (void);
// 0x0000021C System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass29_0::<BuyItem>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__1_m067E57B859F20A200EA30FF708AAC00D845880E2 (void);
// 0x0000021D System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m1ECEAC40554536940ACB202A3E0E8C68A8ECED44 (void);
// 0x0000021E System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::<SellItem>b__0(System.Single)
extern void U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__0_mFB6B707D8C5596F790B68146050394B343394D1D (void);
// 0x0000021F System.Void DevionGames.InventorySystem.VendorTrigger/<>c__DisplayClass30_0::<SellItem>b__1(System.Int32)
extern void U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__1_m890270DE06975B621F379787F02D1F12DFE6C206 (void);
// 0x00000220 DevionGames.InventorySystem.Currency DevionGames.InventorySystem.CurrencySlot::GetDefaultCurrency()
extern void CurrencySlot_GetDefaultCurrency_mFDDC88693651D35192FFBDF9A99D39174BAD0268 (void);
// 0x00000221 System.Void DevionGames.InventorySystem.CurrencySlot::Repaint()
extern void CurrencySlot_Repaint_m8CA34D2D0ABEA22B6CCE54E6613D7FCE5C7934A4 (void);
// 0x00000222 System.Boolean DevionGames.InventorySystem.CurrencySlot::CanAddItem(DevionGames.InventorySystem.Item)
extern void CurrencySlot_CanAddItem_mB10DC0A23BA3FBBAE36C0CD37828D1076B159722 (void);
// 0x00000223 System.Boolean DevionGames.InventorySystem.CurrencySlot::CanUse()
extern void CurrencySlot_CanUse_mBF19A4C1FCC6E79F02EB40DB5E0236AC23EA94DF (void);
// 0x00000224 System.Void DevionGames.InventorySystem.CurrencySlot::.ctor()
extern void CurrencySlot__ctor_mB6242077E47CD0F81E7F13A3984FB506710E4FAF (void);
// 0x00000225 System.String[] DevionGames.InventorySystem.ItemContainer::get_Callbacks()
extern void ItemContainer_get_Callbacks_m430B92A765395BC06F037E5D92DA379DA226246E (void);
// 0x00000226 System.Void DevionGames.InventorySystem.ItemContainer::add_OnAddItem(DevionGames.InventorySystem.ItemContainer/AddItemDelegate)
extern void ItemContainer_add_OnAddItem_m6F08BB8215855B35F3A4882D4C7199A01E88140F (void);
// 0x00000227 System.Void DevionGames.InventorySystem.ItemContainer::remove_OnAddItem(DevionGames.InventorySystem.ItemContainer/AddItemDelegate)
extern void ItemContainer_remove_OnAddItem_mBEB15F3259A72C56DBDCEC44B655E7581862944B (void);
// 0x00000228 System.Void DevionGames.InventorySystem.ItemContainer::add_OnFailedToAddItem(DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate)
extern void ItemContainer_add_OnFailedToAddItem_mF136263709491EE7361ED20778D2ED92098B3AD9 (void);
// 0x00000229 System.Void DevionGames.InventorySystem.ItemContainer::remove_OnFailedToAddItem(DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate)
extern void ItemContainer_remove_OnFailedToAddItem_m5F5C7AE7262C71144C90C461FD403F761EE1DDF8 (void);
// 0x0000022A System.Void DevionGames.InventorySystem.ItemContainer::add_OnRemoveItem(DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate)
extern void ItemContainer_add_OnRemoveItem_mAA53246093667369BD96800B5BE7337B49310595 (void);
// 0x0000022B System.Void DevionGames.InventorySystem.ItemContainer::remove_OnRemoveItem(DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate)
extern void ItemContainer_remove_OnRemoveItem_mD1A95A983E701FEF301966422CADED1F6B250712 (void);
// 0x0000022C System.Void DevionGames.InventorySystem.ItemContainer::add_OnFailedToRemoveItem(DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate)
extern void ItemContainer_add_OnFailedToRemoveItem_mFC55C620F984D08E5F652BAA8C9AE802CCD4466F (void);
// 0x0000022D System.Void DevionGames.InventorySystem.ItemContainer::remove_OnFailedToRemoveItem(DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate)
extern void ItemContainer_remove_OnFailedToRemoveItem_m070D8D92D2BED088C68AEBB705DC98A8851405FC (void);
// 0x0000022E System.Void DevionGames.InventorySystem.ItemContainer::add_OnTryUseItem(DevionGames.InventorySystem.ItemContainer/UseItemDelegate)
extern void ItemContainer_add_OnTryUseItem_m13E4D934B09A83CF7AACC3B3A203381F7CA2A5C6 (void);
// 0x0000022F System.Void DevionGames.InventorySystem.ItemContainer::remove_OnTryUseItem(DevionGames.InventorySystem.ItemContainer/UseItemDelegate)
extern void ItemContainer_remove_OnTryUseItem_m357C5DBF2E81B7D8E5BD6145B3FF59F710EB4147 (void);
// 0x00000230 System.Void DevionGames.InventorySystem.ItemContainer::add_OnUseItem(DevionGames.InventorySystem.ItemContainer/UseItemDelegate)
extern void ItemContainer_add_OnUseItem_mA3FD14B23D326CFA04E5B49D1E2A32A1158B6265 (void);
// 0x00000231 System.Void DevionGames.InventorySystem.ItemContainer::remove_OnUseItem(DevionGames.InventorySystem.ItemContainer/UseItemDelegate)
extern void ItemContainer_remove_OnUseItem_m6663E4FF2A9E58EFC9DBBA22B8CEACCDC067728C (void);
// 0x00000232 System.Void DevionGames.InventorySystem.ItemContainer::add_OnDropItem(DevionGames.InventorySystem.ItemContainer/DropItemDelegate)
extern void ItemContainer_add_OnDropItem_m8682C5772C245FA7CBAC60D8003F3E7057F43F01 (void);
// 0x00000233 System.Void DevionGames.InventorySystem.ItemContainer::remove_OnDropItem(DevionGames.InventorySystem.ItemContainer/DropItemDelegate)
extern void ItemContainer_remove_OnDropItem_m7496A6F4ECCC526C2CE232496525DF8DBDE56A6F (void);
// 0x00000234 System.Boolean DevionGames.InventorySystem.ItemContainer::get_UseReferences()
extern void ItemContainer_get_UseReferences_m886E9F1BE6FB49CB6B4EEB8037A4AE4E3706EBA6 (void);
// 0x00000235 System.Void DevionGames.InventorySystem.ItemContainer::set_UseReferences(System.Boolean)
extern void ItemContainer_set_UseReferences_m80BA931B9EF989E037DCE83397D039F00BAF24AB (void);
// 0x00000236 System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanDragIn()
extern void ItemContainer_get_CanDragIn_mF7361583BAC183B9DFF8E3EB97DB497E7A6C67F8 (void);
// 0x00000237 System.Void DevionGames.InventorySystem.ItemContainer::set_CanDragIn(System.Boolean)
extern void ItemContainer_set_CanDragIn_mC70FD0A9623FD6F0129F43DF3558F32D67921102 (void);
// 0x00000238 System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanDragOut()
extern void ItemContainer_get_CanDragOut_m3803AB9EB1CDA716A44440A5E7CA8AFF4B160B9E (void);
// 0x00000239 System.Void DevionGames.InventorySystem.ItemContainer::set_CanDragOut(System.Boolean)
extern void ItemContainer_set_CanDragOut_m44E74550F379F38923AE218C9850B8C0DBB05DD3 (void);
// 0x0000023A System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanDropItems()
extern void ItemContainer_get_CanDropItems_m8AD607878A2D4ADF1DC46C3B6E5810D727F09F05 (void);
// 0x0000023B System.Void DevionGames.InventorySystem.ItemContainer::set_CanDropItems(System.Boolean)
extern void ItemContainer_set_CanDropItems_m2F7BC33C1E313ED7E1F2C5842FCEB70AB77489E3 (void);
// 0x0000023C System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanReferenceItems()
extern void ItemContainer_get_CanReferenceItems_m0CC7A6C984D3A27CE66BC448DB20310A6E8F7360 (void);
// 0x0000023D System.Void DevionGames.InventorySystem.ItemContainer::set_CanReferenceItems(System.Boolean)
extern void ItemContainer_set_CanReferenceItems_m25F8B7E970128DE67251742D47E860F8BC34F49F (void);
// 0x0000023E System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanSellItems()
extern void ItemContainer_get_CanSellItems_m80671895B820323E225C7D9929DA7311C45EB12E (void);
// 0x0000023F System.Void DevionGames.InventorySystem.ItemContainer::set_CanSellItems(System.Boolean)
extern void ItemContainer_set_CanSellItems_m350307587EB5B2CDE647AF1F5144773264FA2EB9 (void);
// 0x00000240 System.Boolean DevionGames.InventorySystem.ItemContainer::get_CanUseItems()
extern void ItemContainer_get_CanUseItems_mB1AC2D039716DAFFA59577CE8185DC9977A555FB (void);
// 0x00000241 System.Void DevionGames.InventorySystem.ItemContainer::set_CanUseItems(System.Boolean)
extern void ItemContainer_set_CanUseItems_m1DCBCF4BC04EBCC69E2008324EC26CBEDD6B4422 (void);
// 0x00000242 System.Boolean DevionGames.InventorySystem.ItemContainer::get_UseContextMenu()
extern void ItemContainer_get_UseContextMenu_m79696406BD3011BFBC8588C7034142284BBB1EEE (void);
// 0x00000243 System.Void DevionGames.InventorySystem.ItemContainer::set_UseContextMenu(System.Boolean)
extern void ItemContainer_set_UseContextMenu_mF2A96A125DA52A9B23D8E48C0B5542FCD6AB3A0C (void);
// 0x00000244 System.Boolean DevionGames.InventorySystem.ItemContainer::get_ShowTooltips()
extern void ItemContainer_get_ShowTooltips_mFFE1A9C51F3ADD3B86B60FB6FA6D23AB038EE375 (void);
// 0x00000245 System.Void DevionGames.InventorySystem.ItemContainer::set_ShowTooltips(System.Boolean)
extern void ItemContainer_set_ShowTooltips_mA7BBA24457B7E7777A121849DF0D00C935B1C50D (void);
// 0x00000246 System.Boolean DevionGames.InventorySystem.ItemContainer::get_MoveUsedItem()
extern void ItemContainer_get_MoveUsedItem_m05583740F34FE3FB7C0114BA4CB8CA966491C42A (void);
// 0x00000247 System.Void DevionGames.InventorySystem.ItemContainer::set_MoveUsedItem(System.Boolean)
extern void ItemContainer_set_MoveUsedItem_m7C405EF9D91A0ED97B7958D377D1C182611A4358 (void);
// 0x00000248 System.Collections.ObjectModel.ReadOnlyCollection`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.ItemContainer::get_Slots()
extern void ItemContainer_get_Slots_m9ABC844E225686950EF23CA6912E271A7A4683F0 (void);
// 0x00000249 System.Void DevionGames.InventorySystem.ItemContainer::set_Collection(DevionGames.InventorySystem.ItemCollection)
extern void ItemContainer_set_Collection_mF7D8D5129842D984B685408C10F6627A7EEDD339 (void);
// 0x0000024A System.Void DevionGames.InventorySystem.ItemContainer::OnAwake()
extern void ItemContainer_OnAwake_m4D7A994C8B452D91B7446413226F3FB8B88A143D (void);
// 0x0000024B System.Void DevionGames.InventorySystem.ItemContainer::Show()
extern void ItemContainer_Show_m82BD26DA543040AF783F1013F38DE05857B7ABA0 (void);
// 0x0000024C System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrSwap(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Slot)
extern void ItemContainer_StackOrSwap_m9C36637BD2BC2CEAD905F331DB8549A8D7640464 (void);
// 0x0000024D System.Boolean DevionGames.InventorySystem.ItemContainer::SwapItems(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Slot)
extern void ItemContainer_SwapItems_mD4A389472FD3F7511C484E09AC444A367D6701B4 (void);
// 0x0000024E System.Boolean DevionGames.InventorySystem.ItemContainer::CanSwapItems(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Slot)
extern void ItemContainer_CanSwapItems_mBADFF21BAF22099B01618F2D744FE6AAF66D8339 (void);
// 0x0000024F System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrAdd(DevionGames.InventorySystem.Item)
extern void ItemContainer_StackOrAdd_m32ED8FC2BD0F2833A2CEE97DD79471D58F6249B7 (void);
// 0x00000250 System.Boolean DevionGames.InventorySystem.ItemContainer::StackOrAdd(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item)
extern void ItemContainer_StackOrAdd_m9D3889D6867733E0E222EE25708BA4B33B1FCE7F (void);
// 0x00000251 System.Boolean DevionGames.InventorySystem.ItemContainer::AddItem(DevionGames.InventorySystem.Item)
extern void ItemContainer_AddItem_m45707A58E18CA42C00FE8FDE649778172EAF362F (void);
// 0x00000252 System.Boolean DevionGames.InventorySystem.ItemContainer::StackItem(DevionGames.InventorySystem.Item)
extern void ItemContainer_StackItem_m0963DCA3CA10E58EA02D71DD957FDD397BD0B58F (void);
// 0x00000253 System.Boolean DevionGames.InventorySystem.ItemContainer::CanMoveItems(DevionGames.InventorySystem.Item[],DevionGames.InventorySystem.Slot[],DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.ItemContainer,System.Collections.Generic.Dictionary`2<DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item>&)
extern void ItemContainer_CanMoveItems_mFDCA0E96A61ED16519CD3010EE12EBE389EE94A4 (void);
// 0x00000254 System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItem(System.Int32,DevionGames.InventorySystem.Item)
extern void ItemContainer_CanAddItem_m89559D55F36E091775E9489CF0BDF9A2F135D7C5 (void);
// 0x00000255 System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItem(DevionGames.InventorySystem.Item)
extern void ItemContainer_CanAddItem_m988BAB0CF27817603A8795FCFC83650A04EE7F0F (void);
// 0x00000256 System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItem(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot&,System.Boolean)
extern void ItemContainer_CanAddItem_m0294FFF413FA53B672AE27EBC531C731EC52970E (void);
// 0x00000257 DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.ItemContainer::ReplaceItem(System.Int32,DevionGames.InventorySystem.Item)
extern void ItemContainer_ReplaceItem_mB81F990D28D42425DF196360278BF315F5D1B01A (void);
// 0x00000258 System.Boolean DevionGames.InventorySystem.ItemContainer::RemoveItem(System.Int32)
extern void ItemContainer_RemoveItem_m442184B9A27E6793D49DA2E8FF6F160282721CAD (void);
// 0x00000259 System.Boolean DevionGames.InventorySystem.ItemContainer::RemoveItem(DevionGames.InventorySystem.Item,System.Int32)
extern void ItemContainer_RemoveItem_m1B55B66940BE4ADA95F61D11DED9016A4DF9E163 (void);
// 0x0000025A System.Boolean DevionGames.InventorySystem.ItemContainer::RemoveItem(DevionGames.InventorySystem.Item)
extern void ItemContainer_RemoveItem_mC338A9FEC1C841F5C636E25A03454B551FAE56F2 (void);
// 0x0000025B System.Void DevionGames.InventorySystem.ItemContainer::RemoveItems(System.Boolean)
extern void ItemContainer_RemoveItems_mFDC958EDE4ABDCE610E65D02A3A22FBFBEE3571B (void);
// 0x0000025C System.Boolean DevionGames.InventorySystem.ItemContainer::HasItem(DevionGames.InventorySystem.Item,System.Int32)
extern void ItemContainer_HasItem_mA2C88A92AD22193C2A0874E9ECB28C99604C18A0 (void);
// 0x0000025D System.Boolean DevionGames.InventorySystem.ItemContainer::HasItem(DevionGames.InventorySystem.Item,System.Int32,System.Int32&)
extern void ItemContainer_HasItem_mF82011BE226C2A8997F155147ADE9EEC708194B7 (void);
// 0x0000025E System.Boolean DevionGames.InventorySystem.ItemContainer::HasCategoryItem(DevionGames.InventorySystem.Category)
extern void ItemContainer_HasCategoryItem_m37BE48F329E6CA802B851C1316C5B3AA3AA9BDAB (void);
// 0x0000025F DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.ItemContainer::GetItems(System.String)
extern void ItemContainer_GetItems_m56C723978EE49119738E4B277CE15FF5220E319C (void);
// 0x00000260 T[] DevionGames.InventorySystem.ItemContainer::GetItems(System.Boolean)
// 0x00000261 DevionGames.InventorySystem.Item[] DevionGames.InventorySystem.ItemContainer::GetItems(System.Type,System.Boolean)
extern void ItemContainer_GetItems_m4246D4036F30A13FC363DBE809B76F598B1C3465 (void);
// 0x00000262 System.Collections.Generic.List`1<DevionGames.InventorySystem.Slot> DevionGames.InventorySystem.ItemContainer::GetRequiredSlots(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_GetRequiredSlots_m891D63411C34176E8571DD95E0DFA60E68ED62F8 (void);
// 0x00000263 DevionGames.InventorySystem.Slot[] DevionGames.InventorySystem.ItemContainer::GetSlots(DevionGames.InventorySystem.Item)
extern void ItemContainer_GetSlots_m3B60B2100FCEA634134CD298B06FE49006421008 (void);
// 0x00000264 System.Void DevionGames.InventorySystem.ItemContainer::RefreshSlots()
extern void ItemContainer_RefreshSlots_m0815EAC8814A18767A1000AC6EC4B7B184FEF5E0 (void);
// 0x00000265 T[] DevionGames.InventorySystem.ItemContainer::GetSlots()
// 0x00000266 DevionGames.InventorySystem.Slot[] DevionGames.InventorySystem.ItemContainer::GetSlots(System.Type)
extern void ItemContainer_GetSlots_m1BBEC714F27C3B8F039218A24A3461D9D365BF21 (void);
// 0x00000267 DevionGames.InventorySystem.Slot DevionGames.InventorySystem.ItemContainer::CreateSlot()
extern void ItemContainer_CreateSlot_m305D490944D3BB4EC741749A056BF55F5376D665 (void);
// 0x00000268 System.Void DevionGames.InventorySystem.ItemContainer::DestroySlot(System.Int32)
extern void ItemContainer_DestroySlot_m6B23DA272D26997718FD307344F9E7C014C7569D (void);
// 0x00000269 System.Boolean DevionGames.InventorySystem.ItemContainer::CanStack(DevionGames.InventorySystem.Slot,DevionGames.InventorySystem.Item)
extern void ItemContainer_CanStack_m27BD3EC3A922F7ABDFF2D1722D9198FDED130FF8 (void);
// 0x0000026A System.Boolean DevionGames.InventorySystem.ItemContainer::CanStack(DevionGames.InventorySystem.Item)
extern void ItemContainer_CanStack_m07BB90DD5837266FDBC51D8E72B6B31060EB6121 (void);
// 0x0000026B System.Void DevionGames.InventorySystem.ItemContainer::ShowByCategory(UnityEngine.UI.Dropdown)
extern void ItemContainer_ShowByCategory_m1E4F918BA029988F630B43C7B7293667BCF1BA9D (void);
// 0x0000026C System.Void DevionGames.InventorySystem.ItemContainer::TryConvertCurrency(DevionGames.InventorySystem.Currency)
extern void ItemContainer_TryConvertCurrency_m9932F8E0B77686C448C60461AC44F5C7D8D94124 (void);
// 0x0000026D System.Boolean DevionGames.InventorySystem.ItemContainer::TryRemove(DevionGames.InventorySystem.Currency,DevionGames.InventorySystem.Currency,System.Int32)
extern void ItemContainer_TryRemove_m27A91D490DE767F09B5BC54BD2FD0A5BAFD84AE9 (void);
// 0x0000026E System.Void DevionGames.InventorySystem.ItemContainer::ConvertToSmallestCurrency()
extern void ItemContainer_ConvertToSmallestCurrency_m67222F3830855C2301586FB78C5D4F40FE07E962 (void);
// 0x0000026F System.Void DevionGames.InventorySystem.ItemContainer::RegisterCallbacks()
extern void ItemContainer_RegisterCallbacks_mEDCC158D6B34407B36B593876DF13976EDB40B98 (void);
// 0x00000270 System.Void DevionGames.InventorySystem.ItemContainer::RemoveItems(System.String,System.Boolean)
extern void ItemContainer_RemoveItems_m48224E4FD9B0BE8B9ADEFC803E4080CD907688E8 (void);
// 0x00000271 System.Boolean DevionGames.InventorySystem.ItemContainer::RemoveItem(System.String,DevionGames.InventorySystem.Item,System.Int32)
extern void ItemContainer_RemoveItem_mDA2FB6E407FAF92E373B33E88430BE233D57A534 (void);
// 0x00000272 System.Void DevionGames.InventorySystem.ItemContainer::RemoveItemCompletely(DevionGames.InventorySystem.Item)
extern void ItemContainer_RemoveItemCompletely_m84F8F228DD2ADCF5E5CAA66CD9BAB2CA12B8AA5F (void);
// 0x00000273 System.Void DevionGames.InventorySystem.ItemContainer::RemoveItemReferences(DevionGames.InventorySystem.Item)
extern void ItemContainer_RemoveItemReferences_m4D22DBAC832D19C7B74A168066EB0894153AC8A3 (void);
// 0x00000274 System.Boolean DevionGames.InventorySystem.ItemContainer::HasItem(System.String,DevionGames.InventorySystem.Item,System.Int32)
extern void ItemContainer_HasItem_mF5A07B865CF19628171AC8C1761A164A7439E8A0 (void);
// 0x00000275 System.Boolean DevionGames.InventorySystem.ItemContainer::HasCategoryItem(System.String,DevionGames.InventorySystem.Category)
extern void ItemContainer_HasCategoryItem_m28B9FBB7E233963FBEF653D3624B9335EBFA70C0 (void);
// 0x00000276 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer::GetItem(System.String,System.String)
extern void ItemContainer_GetItem_m74A975B1494BCBBB4F08A4CC0445A37DFCDEA0C7 (void);
// 0x00000277 System.Int32 DevionGames.InventorySystem.ItemContainer::GetItemAmount(System.String,System.String)
extern void ItemContainer_GetItemAmount_m0C6626D41270D4933B48CCC8559126F5F145BB3F (void);
// 0x00000278 System.Boolean DevionGames.InventorySystem.ItemContainer::AddItems(System.String,DevionGames.InventorySystem.Item[],System.Boolean)
extern void ItemContainer_AddItems_m2DAD47F2EF7EB7117495BDF84D4D410E3434D6D2 (void);
// 0x00000279 System.Boolean DevionGames.InventorySystem.ItemContainer::AddItem(System.String,DevionGames.InventorySystem.Item,System.Boolean)
extern void ItemContainer_AddItem_mE2B47D0AE7869FAC38817EB4B2E3288157FEC3DA (void);
// 0x0000027A System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItems(System.String,DevionGames.InventorySystem.Item[],System.Boolean)
extern void ItemContainer_CanAddItems_m5FF40693304EEA74AE375447E04224E356F70729 (void);
// 0x0000027B System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItem(System.String,DevionGames.InventorySystem.Item,System.Boolean)
extern void ItemContainer_CanAddItem_m4AA9BE43B8DE4F0B6224A5F5C169F4B4066EC8D2 (void);
// 0x0000027C System.Boolean DevionGames.InventorySystem.ItemContainer::CanAddItem(System.String,DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot&,System.Boolean)
extern void ItemContainer_CanAddItem_m5CFF98E13151728E3D68DDE01A2EF7651DD36C32 (void);
// 0x0000027D System.Void DevionGames.InventorySystem.ItemContainer::Cooldown(DevionGames.InventorySystem.Item,System.Single)
extern void ItemContainer_Cooldown_m53D9A1344878735EC32DC68DE280F6608792BA58 (void);
// 0x0000027E System.Void DevionGames.InventorySystem.ItemContainer::CooldownSlots(DevionGames.InventorySystem.Item,System.Single)
extern void ItemContainer_CooldownSlots_m106B1D6234D9368B78592EA69B2E7D9C582CBF04 (void);
// 0x0000027F System.Void DevionGames.InventorySystem.ItemContainer::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void ItemContainer_OnDrop_mB8CC55EC8055A1A2AE97600831EEE38529841F64 (void);
// 0x00000280 System.Void DevionGames.InventorySystem.ItemContainer::NotifyDropItem(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void ItemContainer_NotifyDropItem_m3A11A27C4E289FA6E7C2069BDA6011CEC778F7DC (void);
// 0x00000281 System.Void DevionGames.InventorySystem.ItemContainer::NotifyUseItem(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_NotifyUseItem_m5C8B71756A4C9D90F7DA9E90B9FD56AA91E3FA0D (void);
// 0x00000282 System.Void DevionGames.InventorySystem.ItemContainer::NotifyTryUseItem(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_NotifyTryUseItem_m97458D79157A300D98B62BAC11892BBE0FFEE3B9 (void);
// 0x00000283 System.Void DevionGames.InventorySystem.ItemContainer::NotifyAddItem(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_NotifyAddItem_m40EFB8770013D08E11B855846DAAE55ABE064A17 (void);
// 0x00000284 System.Void DevionGames.InventorySystem.ItemContainer::NotifyRemoveItem(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void ItemContainer_NotifyRemoveItem_m535AF19F9A2BD14B9F3066C94EC035F05F32EDFA (void);
// 0x00000285 System.Void DevionGames.InventorySystem.ItemContainer::MoveTo(System.String)
extern void ItemContainer_MoveTo_m0E08B60485E79E7DA4070AD1FB802F50BDBD47E9 (void);
// 0x00000286 System.Void DevionGames.InventorySystem.ItemContainer::.ctor()
extern void ItemContainer__ctor_m3DFFCA54118261C90245DCBB2EE83F97E3B134DF (void);
// 0x00000287 System.Boolean DevionGames.InventorySystem.ItemContainer::<RefreshSlots>b__107_0(DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRefreshSlotsU3Eb__107_0_mF6C72F5C3896151C109389D48E435DC10859EE32 (void);
// 0x00000288 System.Boolean DevionGames.InventorySystem.ItemContainer::<RefreshSlots>b__107_1(DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRefreshSlotsU3Eb__107_1_m6144049BEF060A7BEC093D7A7706BF94A0F327D2 (void);
// 0x00000289 System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_0(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_0_mCE609769E36DC6BA9AEF19D864F4EE26C44D8B61 (void);
// 0x0000028A System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_1(DevionGames.InventorySystem.Item)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_1_mA462942C146F1C1325A8D9A2C67C34873D517BE7 (void);
// 0x0000028B System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_2(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_2_m02F2650744A2810F336C05A2A398F9F462B74E47 (void);
// 0x0000028C System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_3(DevionGames.InventorySystem.Item,System.Int32)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_3_m24F684F75E88E23C1511354E2C4E257D1DC4D8AA (void);
// 0x0000028D System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_4(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_4_mA33BD64949C76B533C6C857BA3B7566879614A31 (void);
// 0x0000028E System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_5(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_5_m618FF67AA406E6EDB72AFBE5A98E4C87A159A5D1 (void);
// 0x0000028F System.Void DevionGames.InventorySystem.ItemContainer::<RegisterCallbacks>b__118_6(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void ItemContainer_U3CRegisterCallbacksU3Eb__118_6_mE1357A708738D310C3CB47D278809DBB3E9A9C6E (void);
// 0x00000290 System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::.ctor(System.Object,System.IntPtr)
extern void AddItemDelegate__ctor_mF2268FBBCFA478A58C45517C1834CF8741BEA035 (void);
// 0x00000291 System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::Invoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void AddItemDelegate_Invoke_m17AC8582C1C6B83700F3A8565DF5E2429D2C5639 (void);
// 0x00000292 System.IAsyncResult DevionGames.InventorySystem.ItemContainer/AddItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
extern void AddItemDelegate_BeginInvoke_m15DEB3D486173B0D743BAC50AFE5259C303D6D9D (void);
// 0x00000293 System.Void DevionGames.InventorySystem.ItemContainer/AddItemDelegate::EndInvoke(System.IAsyncResult)
extern void AddItemDelegate_EndInvoke_m226FB3D152835C80CC2FDEA27756D804500C4BCF (void);
// 0x00000294 System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::.ctor(System.Object,System.IntPtr)
extern void FailedToAddItemDelegate__ctor_mD9E18790126B624D7F7A9BB7CCDF3CB30802F599 (void);
// 0x00000295 System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::Invoke(DevionGames.InventorySystem.Item)
extern void FailedToAddItemDelegate_Invoke_m94F697D87B83FC1B3E34A8B5A692CFDA52A21861 (void);
// 0x00000296 System.IAsyncResult DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.AsyncCallback,System.Object)
extern void FailedToAddItemDelegate_BeginInvoke_m7194A1E8ADF1D6099EF4E5BA25259CF8A9A60100 (void);
// 0x00000297 System.Void DevionGames.InventorySystem.ItemContainer/FailedToAddItemDelegate::EndInvoke(System.IAsyncResult)
extern void FailedToAddItemDelegate_EndInvoke_mD384609F467F1242B69BBACFAE1C181D44189548 (void);
// 0x00000298 System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::.ctor(System.Object,System.IntPtr)
extern void RemoveItemDelegate__ctor_m2905BD9846BC2731CF6996336F0CE07888995A69 (void);
// 0x00000299 System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::Invoke(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void RemoveItemDelegate_Invoke_m2ECEC5C5550F7D87FA9A91773D0C2D9A3FDE7954 (void);
// 0x0000029A System.IAsyncResult DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
extern void RemoveItemDelegate_BeginInvoke_m322D3DDC0FA1B4549A78FAFB7CA152F0B5B4F534 (void);
// 0x0000029B System.Void DevionGames.InventorySystem.ItemContainer/RemoveItemDelegate::EndInvoke(System.IAsyncResult)
extern void RemoveItemDelegate_EndInvoke_m4A9B4D2CAD1806CFBFE7BC4345BE2024751E10CE (void);
// 0x0000029C System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::.ctor(System.Object,System.IntPtr)
extern void FailedToRemoveItemDelegate__ctor_m622BB21F6F97E0EA79643BAB94171FAE3C2CDBAD (void);
// 0x0000029D System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::Invoke(DevionGames.InventorySystem.Item,System.Int32)
extern void FailedToRemoveItemDelegate_Invoke_m8619D6D8F748159DF1414F9564F2E8B356C109F1 (void);
// 0x0000029E System.IAsyncResult DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,System.Int32,System.AsyncCallback,System.Object)
extern void FailedToRemoveItemDelegate_BeginInvoke_mE77F6610C7253ED04B62D97D35B2FABBF393BF55 (void);
// 0x0000029F System.Void DevionGames.InventorySystem.ItemContainer/FailedToRemoveItemDelegate::EndInvoke(System.IAsyncResult)
extern void FailedToRemoveItemDelegate_EndInvoke_m8AB2E1F3632FC60922E844770AF1B575AFAD6D6A (void);
// 0x000002A0 System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::.ctor(System.Object,System.IntPtr)
extern void UseItemDelegate__ctor_m93D0DA58F472A45F5917457C5BC91A668B9B0B0F (void);
// 0x000002A1 System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::Invoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void UseItemDelegate_Invoke_m6407B74DD0985B6249634E9E9FD907673466838A (void);
// 0x000002A2 System.IAsyncResult DevionGames.InventorySystem.ItemContainer/UseItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot,System.AsyncCallback,System.Object)
extern void UseItemDelegate_BeginInvoke_m591AF4B80D624E5768332949B7ADF353785FEA5B (void);
// 0x000002A3 System.Void DevionGames.InventorySystem.ItemContainer/UseItemDelegate::EndInvoke(System.IAsyncResult)
extern void UseItemDelegate_EndInvoke_m3B26098D85EA5622BA629929E36B1ACC0542CA32 (void);
// 0x000002A4 System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::.ctor(System.Object,System.IntPtr)
extern void DropItemDelegate__ctor_m8D123DDC38F650FAABEF63459C433CB1401643E2 (void);
// 0x000002A5 System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::Invoke(DevionGames.InventorySystem.Item,UnityEngine.GameObject)
extern void DropItemDelegate_Invoke_m38B05498F02933153863260A9B367FC55FDCB8CA (void);
// 0x000002A6 System.IAsyncResult DevionGames.InventorySystem.ItemContainer/DropItemDelegate::BeginInvoke(DevionGames.InventorySystem.Item,UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void DropItemDelegate_BeginInvoke_m4F122299CACE8F277D03AA67BBBE857FB8E99A34 (void);
// 0x000002A7 System.Void DevionGames.InventorySystem.ItemContainer/DropItemDelegate::EndInvoke(System.IAsyncResult)
extern void DropItemDelegate_EndInvoke_m79D54AB9FDDEA9E80C3461AD87A54B6B88949E36 (void);
// 0x000002A8 System.Void DevionGames.InventorySystem.ItemContainer/MoveItemCondition::.ctor()
extern void MoveItemCondition__ctor_mE201579B85E5F4CE2808AFBAD2E1E2005BE8B13F (void);
// 0x000002A9 System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0::.ctor()
extern void U3CU3Ec__DisplayClass80_0__ctor_mE8D7F64D411CBAA882074E7457CC70CC3FF971DC (void);
// 0x000002AA System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass80_0::<set_Collection>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass80_0_U3Cset_CollectionU3Eb__0_m6095F4B55A92EB379D2DFD8CEB923F8CFCC3BA36 (void);
// 0x000002AB System.Void DevionGames.InventorySystem.ItemContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_mDC20D61C387E3636652BF5ED4C133D9E70F7A130 (void);
// 0x000002AC System.Void DevionGames.InventorySystem.ItemContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m5FF95C64EB459C197C94DF56E962B8CE03869143 (void);
// 0x000002AD System.Boolean DevionGames.InventorySystem.ItemContainer/<>c::<set_Collection>b__80_1(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3Cset_CollectionU3Eb__80_1_mAB12A5CC4B8AA6AD4537BB28C8FB09EB5E90FEED (void);
// 0x000002AE System.Boolean DevionGames.InventorySystem.ItemContainer/<>c::<SwapItems>b__84_0(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CSwapItemsU3Eb__84_0_mC6E4C294ADB82B1396F0023AC55DC4B1C4EBC262 (void);
// 0x000002AF DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<SwapItems>b__84_1(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CSwapItemsU3Eb__84_1_m85A1FBD9BBD42C21335B9B2F158E79B7FA1F3D9A (void);
// 0x000002B0 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c::<SwapItems>b__84_2(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CSwapItemsU3Eb__84_2_m436D6230E4C5F79F36B0867C5A390A29854601C4 (void);
// 0x000002B1 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<SwapItems>b__84_3(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CSwapItemsU3Eb__84_3_mCED3312C00C1B6DC60599E3B57C604529D615510 (void);
// 0x000002B2 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c::<CanSwapItems>b__85_0(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CCanSwapItemsU3Eb__85_0_m0941039A9F18E51D24C76A204EA65838C6E8CC86 (void);
// 0x000002B3 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<CanSwapItems>b__85_1(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CCanSwapItemsU3Eb__85_1_m2D1B4223F998B672A71DE00428E7B2387BC6B373 (void);
// 0x000002B4 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c::<CanSwapItems>b__85_2(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CCanSwapItemsU3Eb__85_2_m7AFD6B3BE23F8D54469ED7A0ACABD2A972DE23AF (void);
// 0x000002B5 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<CanSwapItems>b__85_3(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CCanSwapItemsU3Eb__85_3_m8225658ECEF2429A3BA9BA13D07B94853A2B9E79 (void);
// 0x000002B6 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<GetItems>b__102_2(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CGetItemsU3Eb__102_2_m214099CDE978E2E0EE419C113C13BEA5CF267119 (void);
// 0x000002B7 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<GetItems>b__102_5(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CGetItemsU3Eb__102_5_mAAD60B056E6C84A1A40B8EF7B8892FD84EAC1390 (void);
// 0x000002B8 DevionGames.InventorySystem.Item DevionGames.InventorySystem.ItemContainer/<>c::<GetItems>b__104_2(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec_U3CGetItemsU3Eb__104_2_m8BBE48E7E70E9668D9BDED4146EC6E34DAA92EF2 (void);
// 0x000002B9 DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.ItemContainer/<>c::<GetRequiredSlots>b__105_0(DevionGames.InventorySystem.Restrictions.EquipmentRegion)
extern void U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_0_m4D938E0318E37CB2600A7107E7C121526809B4E9 (void);
// 0x000002BA DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.ItemContainer/<>c::<GetRequiredSlots>b__105_1(DevionGames.InventorySystem.Restrictions.EquipmentRegion)
extern void U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_1_m4EB648B1B0DD2AB5CB9DFA52C3E4DAF6A552E338 (void);
// 0x000002BB DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.ItemContainer/<>c::<GetRequiredSlots>b__105_2(DevionGames.InventorySystem.Restrictions.EquipmentRegion)
extern void U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_2_m3EDF16A852394877CFE01696DAC17D1EA131B709 (void);
// 0x000002BC DevionGames.InventorySystem.EquipmentRegion DevionGames.InventorySystem.ItemContainer/<>c::<GetRequiredSlots>b__105_3(DevionGames.InventorySystem.Restrictions.EquipmentRegion)
extern void U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_3_m0064D8984D714BD49AF35DDFAAF6F7EF65F7C041 (void);
// 0x000002BD System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0::.ctor()
extern void U3CU3Ec__DisplayClass89_0__ctor_m1A71997BCF90B26DA38001C9925544D93A2D7B54 (void);
// 0x000002BE System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass89_0::<StackItem>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass89_0_U3CStackItemU3Eb__0_mF52A8D11865660EBD84951A4400D666A53868D5C (void);
// 0x000002BF System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0::.ctor()
extern void U3CU3Ec__DisplayClass96_0__ctor_m75FF4066883B13125127A4D0460B9A4C24E37DC0 (void);
// 0x000002C0 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass96_0::<RemoveItem>b__0(DevionGames.InventorySystem.CurrencySlot)
extern void U3CU3Ec__DisplayClass96_0_U3CRemoveItemU3Eb__0_m0A60574A1E1614D6FCACD6B1672E13141DBEE4FF (void);
// 0x000002C1 System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::.ctor()
extern void U3CU3Ec__DisplayClass102_0__ctor_m108CEB315C1D77B0F72AA76D244842D6F1CF1C9F (void);
// 0x000002C2 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__0_mBDAB476C9D67ECDE4AD3C6A3B2484ED417B31E31 (void);
// 0x000002C3 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__1(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__1_mED1368D649E7DED7D196F9813DF74093414C824F (void);
// 0x000002C4 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__3(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__3_m729D5B7D3BF765F343BC8745A2DE0B934B66F53B (void);
// 0x000002C5 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass102_0::<GetItems>b__4(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__4_m9C9DF7F81EAD43AC830AC1C27D7C773DFDE31595 (void);
// 0x000002C6 System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::.ctor()
extern void U3CU3Ec__DisplayClass104_0__ctor_m2D4AB0C53651DC47779AFC73EF092FDEDF06F928 (void);
// 0x000002C7 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::<GetItems>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__0_mF0841795DEE68EFFF6B94EAFDFFA23AE33DA1DBE (void);
// 0x000002C8 System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass104_0::<GetItems>b__1(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__1_m3820BF183A18195071D48E250E91998BE4C7AD9A (void);
// 0x000002C9 System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0::.ctor()
extern void U3CU3Ec__DisplayClass109_0__ctor_m09A2B3A8E3D9EC8DE701B5F4AC42ED39426AEC82 (void);
// 0x000002CA System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass109_0::<GetSlots>b__0(DevionGames.InventorySystem.Slot)
extern void U3CU3Ec__DisplayClass109_0_U3CGetSlotsU3Eb__0_m1C807F6F95479080823BE5C6506FACA76128C795 (void);
// 0x000002CB System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0::.ctor()
extern void U3CU3Ec__DisplayClass113_0__ctor_mA019AC50A788A394195BFFB149D7BB01088E1740 (void);
// 0x000002CC System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass113_0::<CanStack>b__0(DevionGames.InventorySystem.Item)
extern void U3CU3Ec__DisplayClass113_0_U3CCanStackU3Eb__0_m9067D6C95F15D9F823B907B14349BD398AE1B4AC (void);
// 0x000002CD System.Void DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0::.ctor()
extern void U3CU3Ec__DisplayClass115_0__ctor_m05FA62D60598F38F92EB40836F86FD6706ED543D (void);
// 0x000002CE System.Boolean DevionGames.InventorySystem.ItemContainer/<>c__DisplayClass115_0::<TryConvertCurrency>b__0(DevionGames.InventorySystem.Currency)
extern void U3CU3Ec__DisplayClass115_0_U3CTryConvertCurrencyU3Eb__0_m3A812DE5CC1E70DBC85E164BCD0263D1A294D6A4 (void);
// 0x000002CF System.Boolean DevionGames.InventorySystem.ItemSlot::get_IsCooldown()
extern void ItemSlot_get_IsCooldown_m7E35F4DD053180BECF5E25F464E1A8A03587C6AD (void);
// 0x000002D0 DevionGames.InventorySystem.ItemSlot/DragObject DevionGames.InventorySystem.ItemSlot::get_dragObject()
extern void ItemSlot_get_dragObject_mF58AED88A7DEE4E8F5CDC1C9B25E96783EA0CBDD (void);
// 0x000002D1 System.Void DevionGames.InventorySystem.ItemSlot::set_dragObject(DevionGames.InventorySystem.ItemSlot/DragObject)
extern void ItemSlot_set_dragObject_mB57DD77E62186F3BB6DBC3B5A87F9C4DCE8C7FEB (void);
// 0x000002D2 System.Void DevionGames.InventorySystem.ItemSlot::Start()
extern void ItemSlot_Start_mAF0FBE259214DE446143EB7F11FDF15E069EF982 (void);
// 0x000002D3 System.Void DevionGames.InventorySystem.ItemSlot::Update()
extern void ItemSlot_Update_mED8E63DA67E01A4A17ECE8F068ACEFDA56FF2E56 (void);
// 0x000002D4 System.Void DevionGames.InventorySystem.ItemSlot::Repaint()
extern void ItemSlot_Repaint_m2EE1807FAD1BCD52D9810F30C0701D2933719482 (void);
// 0x000002D5 System.Void DevionGames.InventorySystem.ItemSlot::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnPointerEnter_m464BB78BD3D158675C8D100F670C52954C66B5B5 (void);
// 0x000002D6 System.Collections.IEnumerator DevionGames.InventorySystem.ItemSlot::DelayTooltip(System.Single)
extern void ItemSlot_DelayTooltip_mCDF50D2E4203BBF294CCD3C362877EF229129E22 (void);
// 0x000002D7 System.Void DevionGames.InventorySystem.ItemSlot::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnPointerExit_mCE0336FFD9DF7B229927197EC204432E929E040B (void);
// 0x000002D8 System.Void DevionGames.InventorySystem.ItemSlot::ShowTooltip()
extern void ItemSlot_ShowTooltip_m5090E51A37977734F5E23BE0A1278E519ED25975 (void);
// 0x000002D9 System.Void DevionGames.InventorySystem.ItemSlot::CloseTooltip()
extern void ItemSlot_CloseTooltip_m215C3719174255BF28529F0F82F595A4AC00036C (void);
// 0x000002DA System.Void DevionGames.InventorySystem.ItemSlot::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnPointerDown_mEBA00BF5D898AE9352301559C9DC11150168038B (void);
// 0x000002DB System.Void DevionGames.InventorySystem.ItemSlot::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnPointerUp_m5D50C31660CB9BA8682BF241ECAA4658BB48F11C (void);
// 0x000002DC System.Void DevionGames.InventorySystem.ItemSlot::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnBeginDrag_m157CCE145E9E93311DDA5371C8C6D4D69C495CAD (void);
// 0x000002DD System.Void DevionGames.InventorySystem.ItemSlot::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnDrag_m175FE7ED9F202DCDCE2D5853B25C45A9BD0905EC (void);
// 0x000002DE System.Void DevionGames.InventorySystem.ItemSlot::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnEndDrag_mD7F568BC1158D9E8B7CA193B40B37B84CD2B4F59 (void);
// 0x000002DF System.Void DevionGames.InventorySystem.ItemSlot::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void ItemSlot_OnDrop_m2CC269A4B190E024E7F70C8CBA291669187A7175 (void);
// 0x000002E0 System.Void DevionGames.InventorySystem.ItemSlot::DropItem()
extern void ItemSlot_DropItem_mCD465B68D04A945C0D985768D00879A391FB2FCD (void);
// 0x000002E1 System.Void DevionGames.InventorySystem.ItemSlot::Unstack()
extern void ItemSlot_Unstack_mE12057C80077D4F9DE741D5CC629D38D8148C381 (void);
// 0x000002E2 System.Void DevionGames.InventorySystem.ItemSlot::Cooldown(System.Single)
extern void ItemSlot_Cooldown_m06FA70BB3F074F60AB906F4D5543BE94AF2AFD77 (void);
// 0x000002E3 System.Void DevionGames.InventorySystem.ItemSlot::UpdateCooldown()
extern void ItemSlot_UpdateCooldown_m5F265BCBD8E16ED7427AEE2AF7D19F96B5675DD9 (void);
// 0x000002E4 System.Void DevionGames.InventorySystem.ItemSlot::Use()
extern void ItemSlot_Use_m4D3102D74728C0B889358583164B82494DD48712 (void);
// 0x000002E5 System.Boolean DevionGames.InventorySystem.ItemSlot::CanUse()
extern void ItemSlot_CanUse_mD2F94A16AAA5C415B9CC07F8296F0081CC30C1C1 (void);
// 0x000002E6 System.Void DevionGames.InventorySystem.ItemSlot::.ctor()
extern void ItemSlot__ctor_m51ED946F7977677545F314211358C017760FB5B3 (void);
// 0x000002E7 System.Void DevionGames.InventorySystem.ItemSlot/DragObject::.ctor(DevionGames.InventorySystem.Slot)
extern void DragObject__ctor_m53544633AA81943EDC3A6B5202B1EC8D01E2EDD9 (void);
// 0x000002E8 System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::.ctor(System.Int32)
extern void U3CDelayTooltipU3Ed__23__ctor_mC3C9A902CAE14C3E35402B4ABC70E985052EC50A (void);
// 0x000002E9 System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.IDisposable.Dispose()
extern void U3CDelayTooltipU3Ed__23_System_IDisposable_Dispose_mDBAAF22BCE80C94EFF75167DACF2A5C68702F4B7 (void);
// 0x000002EA System.Boolean DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::MoveNext()
extern void U3CDelayTooltipU3Ed__23_MoveNext_mEA8769B233EF0085F559D38AD27DD6F5980CE16C (void);
// 0x000002EB System.Object DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayTooltipU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE227150E52923041CF13D04F059B3D1DF70AFA12 (void);
// 0x000002EC System.Void DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.IEnumerator.Reset()
extern void U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40 (void);
// 0x000002ED System.Object DevionGames.InventorySystem.ItemSlot/<DelayTooltip>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_get_Current_m646B7B909036413EDA2A5337E19D2265E39CCFE0 (void);
// 0x000002EE System.Void DevionGames.InventorySystem.ItemTooltip::Show(DevionGames.InventorySystem.Item)
extern void ItemTooltip_Show_mE22095D2C8D8409FA24F5192E41ED057F0191B90 (void);
// 0x000002EF System.Void DevionGames.InventorySystem.ItemTooltip::.ctor()
extern void ItemTooltip__ctor_mDC0D3F8CCE819ADA00301BE99EBB49A11A83070C (void);
// 0x000002F0 System.Void DevionGames.InventorySystem.LoadSaveMenu::Start()
extern void LoadSaveMenu_Start_m815A5760838250E1522912586890545D614F9603 (void);
// 0x000002F1 System.Void DevionGames.InventorySystem.LoadSaveMenu::UpdateLoadingStates()
extern void LoadSaveMenu_UpdateLoadingStates_m7D1C0E3E74FF2AFEDD210A5CE99C0B1CBFAD11F8 (void);
// 0x000002F2 System.Void DevionGames.InventorySystem.LoadSaveMenu::Save()
extern void LoadSaveMenu_Save_m6D9AA8BFEDA584D5C4CA7F263877B515C6F912FC (void);
// 0x000002F3 UnityEngine.UI.Button DevionGames.InventorySystem.LoadSaveMenu::CreateSlot(System.String)
extern void LoadSaveMenu_CreateSlot_m93CD3DF3C657D3B5B0292A20CF8E8E06D1AD56D9 (void);
// 0x000002F4 System.Void DevionGames.InventorySystem.LoadSaveMenu::.ctor()
extern void LoadSaveMenu__ctor_m0BCA46D50DF5A1724A10D9C78738B6AD69A0C88C (void);
// 0x000002F5 System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m3A2560E9B5E817FC27123B8E0810600A18A27DBF (void);
// 0x000002F6 System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c__DisplayClass3_0::<UpdateLoadingStates>b__1()
extern void U3CU3Ec__DisplayClass3_0_U3CUpdateLoadingStatesU3Eb__1_mA3F22CDB8B8853D8F30E121C17E1D9340137F869 (void);
// 0x000002F7 System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c::.cctor()
extern void U3CU3Ec__cctor_mD28322611AA857CE92686FB0AF29DFC61C834539 (void);
// 0x000002F8 System.Void DevionGames.InventorySystem.LoadSaveMenu/<>c::.ctor()
extern void U3CU3Ec__ctor_m1E6B4B8589A5ABDFCCF7D8419EA65EA392E063D2 (void);
// 0x000002F9 System.Boolean DevionGames.InventorySystem.LoadSaveMenu/<>c::<UpdateLoadingStates>b__3_0(System.String)
extern void U3CU3Ec_U3CUpdateLoadingStatesU3Eb__3_0_mA9313CB46D208854AC5BA7F4708B2461D8D5F765 (void);
// 0x000002FA System.Void DevionGames.InventorySystem.Restriction::Start()
extern void Restriction_Start_mA33943ABC9BB64CE1D92ABEB6C3B27AC77564DFC (void);
// 0x000002FB System.Boolean DevionGames.InventorySystem.Restriction::CanAddItem(DevionGames.InventorySystem.Item)
// 0x000002FC System.Void DevionGames.InventorySystem.Restriction::.ctor()
extern void Restriction__ctor_m73D1492FAA023A4B479FB3202D512777F0586D56 (void);
// 0x000002FD System.Void DevionGames.InventorySystem.SkillSlot::Repaint()
extern void SkillSlot_Repaint_m799B29E7C12BC5EA9E57A5E2BC676900B17679CD (void);
// 0x000002FE System.Void DevionGames.InventorySystem.SkillSlot::.ctor()
extern void SkillSlot__ctor_mA4D31FDEA2AE1538E896357AA3F822C3612931F8 (void);
// 0x000002FF DevionGames.InventorySystem.Item DevionGames.InventorySystem.Slot::get_ObservedItem()
extern void Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438 (void);
// 0x00000300 System.Void DevionGames.InventorySystem.Slot::set_ObservedItem(DevionGames.InventorySystem.Item)
extern void Slot_set_ObservedItem_m6A6A15932BE698DD97D355777537F304756F6B8E (void);
// 0x00000301 System.Boolean DevionGames.InventorySystem.Slot::get_IsEmpty()
extern void Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644 (void);
// 0x00000302 DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Slot::get_Container()
extern void Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D (void);
// 0x00000303 System.Void DevionGames.InventorySystem.Slot::set_Container(DevionGames.InventorySystem.ItemContainer)
extern void Slot_set_Container_mF957BE50CFCFE81C0B2A3689FED3E63A189A9B0A (void);
// 0x00000304 System.Int32 DevionGames.InventorySystem.Slot::get_Index()
extern void Slot_get_Index_mC035A81C33D92B432421E15673C429D96FEB405D (void);
// 0x00000305 System.Void DevionGames.InventorySystem.Slot::set_Index(System.Int32)
extern void Slot_set_Index_mA161DA540B1A22C9500DB08DA2518C5995BA419F (void);
// 0x00000306 System.String[] DevionGames.InventorySystem.Slot::get_Callbacks()
extern void Slot_get_Callbacks_mC864B799AB48906B25FA7BA39D45269F431F668D (void);
// 0x00000307 System.Void DevionGames.InventorySystem.Slot::Start()
extern void Slot_Start_mDC2C24E5C799A7A40E6D79093C550E14DD441C73 (void);
// 0x00000308 System.Void DevionGames.InventorySystem.Slot::Repaint()
extern void Slot_Repaint_m565CCB140D3ACABE2BC6E626392788C8C6191ED2 (void);
// 0x00000309 System.Void DevionGames.InventorySystem.Slot::Use()
extern void Slot_Use_m724393A2DBC52D27CAC940BE3CC6679EC6BE37D5 (void);
// 0x0000030A System.Boolean DevionGames.InventorySystem.Slot::CanUse()
extern void Slot_CanUse_m447C22028EEA1FDCCFE6F3C98B10F723E964545A (void);
// 0x0000030B System.Boolean DevionGames.InventorySystem.Slot::MoveItem()
extern void Slot_MoveItem_mE136033AC55CB0324AD82934ABBF8905D24A6A71 (void);
// 0x0000030C System.Boolean DevionGames.InventorySystem.Slot::CanAddItem(DevionGames.InventorySystem.Item)
extern void Slot_CanAddItem_mB368D86612B48F657E358522B539B49807187008 (void);
// 0x0000030D System.Void DevionGames.InventorySystem.Slot::.ctor()
extern void Slot__ctor_m107758161035B186D1B80DE3264F0426B8345AEB (void);
// 0x0000030E System.Void DevionGames.InventorySystem.Slot::<Start>b__21_0(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void Slot_U3CStartU3Eb__21_0_m5CD98F68009EA39E8EDB441D9887291DBEE66B4E (void);
// 0x0000030F System.Void DevionGames.InventorySystem.Slot::<Start>b__21_1(DevionGames.InventorySystem.Item,System.Int32,DevionGames.InventorySystem.Slot)
extern void Slot_U3CStartU3Eb__21_1_mAB216A805AF038A9A2BC7A513F96E17CA95F80D2 (void);
// 0x00000310 System.Void DevionGames.InventorySystem.Slot::<Start>b__21_2(DevionGames.InventorySystem.Item,DevionGames.InventorySystem.Slot)
extern void Slot_U3CStartU3Eb__21_2_m8C54B7225F8484EA7180EBA03E79C8861BA89842 (void);
// 0x00000311 System.Void DevionGames.InventorySystem.Stack::OnAwake()
extern void Stack_OnAwake_m068165EA1E28AEC47124B266C043708639748285 (void);
// 0x00000312 System.Void DevionGames.InventorySystem.Stack::SetItem(DevionGames.InventorySystem.Item)
extern void Stack_SetItem_mE4D1190E51600CE03FA689A0DC4B53C5DFA6C55C (void);
// 0x00000313 System.Void DevionGames.InventorySystem.Stack::Unstack()
extern void Stack_Unstack_m3B5A828D94801A2C1E920560844EC5695ACE2736 (void);
// 0x00000314 System.Void DevionGames.InventorySystem.Stack::Cancel()
extern void Stack_Cancel_m9205D346C53ADBB202AB11E296C0C20188DF4F25 (void);
// 0x00000315 System.Void DevionGames.InventorySystem.Stack::UpdatePosition()
extern void Stack_UpdatePosition_m4EF7999E418124E253DB31309994EAA4F556A6C5 (void);
// 0x00000316 UnityEngine.Vector3 DevionGames.InventorySystem.Stack::GetCurrentPosition()
extern void Stack_GetCurrentPosition_m9E8E0693C19795114485378D6B97A16C65014155 (void);
// 0x00000317 System.Void DevionGames.InventorySystem.Stack::.ctor()
extern void Stack__ctor_m41554333544C4C23A7D0C5F35F1C70F93AFFC217 (void);
// 0x00000318 System.Void DevionGames.InventorySystem.SwapItems::Update()
extern void SwapItems_Update_mD301DFFC126C8E7D3F86B1ED94403786613E4764 (void);
// 0x00000319 System.Void DevionGames.InventorySystem.SwapItems::.ctor()
extern void SwapItems__ctor_m810E08131DBC91B024BE900E19A6219736032541 (void);
// 0x0000031A System.Boolean DevionGames.InventorySystem.Restrictions.Category::CanAddItem(DevionGames.InventorySystem.Item)
extern void Category_CanAddItem_m3AC928E43CFBBD036ED3A8C0FD9870D6113D9A5B (void);
// 0x0000031B System.Void DevionGames.InventorySystem.Restrictions.Category::.ctor()
extern void Category__ctor_m48E23508BBBCE8D29BA2EA932A50C51CC5A3ED6D (void);
// 0x0000031C System.Boolean DevionGames.InventorySystem.Restrictions.EquipmentRegion::CanAddItem(DevionGames.InventorySystem.Item)
extern void EquipmentRegion_CanAddItem_mDDC3EE182634BBE2B1C55CD91E417DD0CF8D8184 (void);
// 0x0000031D System.Void DevionGames.InventorySystem.Restrictions.EquipmentRegion::.ctor()
extern void EquipmentRegion__ctor_m9FCB3D2690A44AD5E994568B4BBEFC5210A6559D (void);
// 0x0000031E System.Void DevionGames.InventorySystem.Restrictions.EquipmentRegion/<>c::.cctor()
extern void U3CU3Ec__cctor_m898FE93565963CDCA4C5FD58A162EFA7481A06E2 (void);
// 0x0000031F System.Void DevionGames.InventorySystem.Restrictions.EquipmentRegion/<>c::.ctor()
extern void U3CU3Ec__ctor_m8054FA16482957556832C61FEA0736F1DD6E75A0 (void);
// 0x00000320 System.String DevionGames.InventorySystem.Restrictions.EquipmentRegion/<>c::<CanAddItem>b__1_0(DevionGames.InventorySystem.Restrictions.EquipmentRegion)
extern void U3CU3Ec_U3CCanAddItemU3Eb__1_0_mD154A1004E5E1812D5C26031384B4B8F61C243A1 (void);
// 0x00000321 System.Boolean DevionGames.InventorySystem.Restrictions.Profession::CanAddItem(DevionGames.InventorySystem.Item)
extern void Profession_CanAddItem_m0C177965FC8B4D94450AE62568AFEE6C177DBB5E (void);
// 0x00000322 System.Void DevionGames.InventorySystem.Restrictions.Profession::.ctor()
extern void Profession__ctor_mFC2B0D3117342EEA0413D71C8D696C6A80188876 (void);
// 0x00000323 System.String DevionGames.InventorySystem.Configuration.Default::get_Name()
extern void Default_get_Name_m118E17DFF5F04E12CCEDF0074225FFB6F1056A4D (void);
// 0x00000324 System.Void DevionGames.InventorySystem.Configuration.Default::.ctor()
extern void Default__ctor_m4D51FAA260DCA005BA48C7024C884E8090B9DC5D (void);
// 0x00000325 System.String DevionGames.InventorySystem.Configuration.Input::get_Name()
extern void Input_get_Name_m820946AF7BB254EF759D5081891A9A6051E7A54C (void);
// 0x00000326 System.Void DevionGames.InventorySystem.Configuration.Input::.ctor()
extern void Input__ctor_m551ECEBF905B6DFAEF715A45CB3BAA3AA9E72EFD (void);
// 0x00000327 System.String DevionGames.InventorySystem.Configuration.Notifications::get_Name()
extern void Notifications_get_Name_mC58B822A8DC678841E522FE3CFA16E0D3D986AC6 (void);
// 0x00000328 System.Void DevionGames.InventorySystem.Configuration.Notifications::.ctor()
extern void Notifications__ctor_mBBEC089CC8975B176A7211F666DC47C5EBEBDBA6 (void);
// 0x00000329 System.String DevionGames.InventorySystem.Configuration.SavingLoading::get_Name()
extern void SavingLoading_get_Name_m027FFB1FE517C8822A10DF12189B20EBA2C1714D (void);
// 0x0000032A System.Void DevionGames.InventorySystem.Configuration.SavingLoading::.ctor()
extern void SavingLoading__ctor_m83C74039610A9CC4095EBED837C0156129D31B0B (void);
// 0x0000032B System.String DevionGames.InventorySystem.Configuration.Settings::get_Name()
extern void Settings_get_Name_mE84C149ACA7AEB1C4B847AF628E1761763D352A4 (void);
// 0x0000032C System.Void DevionGames.InventorySystem.Configuration.Settings::set_Name(System.String)
extern void Settings_set_Name_m109A8676338159E9DEFD50086681617C7971AD8D (void);
// 0x0000032D System.Void DevionGames.InventorySystem.Configuration.Settings::.ctor()
extern void Settings__ctor_m7A23B6793F3E326D9C1BB1C3FA7CA1AB3EC4D7E1 (void);
// 0x0000032E System.String DevionGames.InventorySystem.Configuration.UI::get_Name()
extern void UI_get_Name_m883362039AF6A1710A3044A654BFC331C86AF242 (void);
// 0x0000032F DevionGames.UIWidgets.Notification DevionGames.InventorySystem.Configuration.UI::get_notification()
extern void UI_get_notification_m9691D24776B668D433A63FFE54019C20299468D5 (void);
// 0x00000330 DevionGames.UIWidgets.Tooltip DevionGames.InventorySystem.Configuration.UI::get_tooltip()
extern void UI_get_tooltip_mFED99C380C15D406CD8D1B2C243D2FCF6AE4F46F (void);
// 0x00000331 DevionGames.InventorySystem.ItemContainer DevionGames.InventorySystem.Configuration.UI::get_sellPriceTooltip()
extern void UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347 (void);
// 0x00000332 DevionGames.InventorySystem.Stack DevionGames.InventorySystem.Configuration.UI::get_stack()
extern void UI_get_stack_m33F884EEEE122CDC9EDEC8F40ECF94D9D6613D04 (void);
// 0x00000333 DevionGames.UIWidgets.ContextMenu DevionGames.InventorySystem.Configuration.UI::get_contextMenu()
extern void UI_get_contextMenu_m06AF481ACFA3CC92B4A5F2E6CC6CAB7E52C9EB28 (void);
// 0x00000334 System.Void DevionGames.InventorySystem.Configuration.UI::.ctor()
extern void UI__ctor_m60696CFAC3C2B10CD38287BE7401E74B83CB10E6 (void);
// 0x00000335 DevionGames.ActionStatus DevionGames.InventorySystem.ItemActions.AddItem::OnUpdate()
extern void AddItem_OnUpdate_m3922033C93621FC754A637E4F5FF207747C58B80 (void);
// 0x00000336 System.Void DevionGames.InventorySystem.ItemActions.AddItem::.ctor()
extern void AddItem__ctor_mB1BF65EB51819C0CBDDE9181F6E34CD27442F1F3 (void);
// 0x00000337 DevionGames.ActionStatus DevionGames.InventorySystem.ItemActions.Cooldown::OnUpdate()
extern void Cooldown_OnUpdate_mD337C2B941B614A3CD584C4C814AC8ECD6CC8543 (void);
// 0x00000338 System.Void DevionGames.InventorySystem.ItemActions.Cooldown::.ctor()
extern void Cooldown__ctor_mACBE06800D824E87DA739573B84F7DCE895AC10D (void);
// 0x00000339 DevionGames.ActionStatus DevionGames.InventorySystem.ItemActions.ReduceStack::OnUpdate()
extern void ReduceStack_OnUpdate_mF0ED6CDED3A98BB63533B9632579222C3520BC11 (void);
// 0x0000033A System.Void DevionGames.InventorySystem.ItemActions.ReduceStack::.ctor()
extern void ReduceStack__ctor_mBB0FC6873CA0C83B2B71E53F26F92CC139253EA4 (void);
// 0x0000033B DevionGames.ActionStatus DevionGames.InventorySystem.ItemActions.RemoveItem::OnUpdate()
extern void RemoveItem_OnUpdate_m12E10651D32AC9E815470E227B1DD1E6B961CA00 (void);
// 0x0000033C System.Void DevionGames.InventorySystem.ItemActions.RemoveItem::.ctor()
extern void RemoveItem__ctor_mEABFCED3B63C5B5A5A3389529227FD43587601BA (void);
// 0x0000033D System.Void DevionGames.InventorySystem.ItemActions.ItemAction::.ctor()
extern void ItemAction__ctor_mC8461D1E34868933D5AF3B6F72EC50F37EA20E8D (void);
static Il2CppMethodPointer s_methodPointers[829] = 
{
	ItemContainerCallbackTester_Awake_mC739F6DF4A7D769775604B8995A501AA16709997,
	ItemContainerCallbackTester__ctor_m886B54FBB3E7FA956AFCC39270444BDB378A3231,
	U3CU3Ec__cctor_mE15630DCF665B958BBC4902FB3FA3F5F7A9D2FA3,
	U3CU3Ec__ctor_m933F59E5DF1ED926E1DAC9D75D0E33959989E6A0,
	U3CU3Ec_U3CAwakeU3Eb__0_0_mDC226B2EF59870C30E06385F95F55CDDF6BE44FC,
	U3CU3Ec_U3CAwakeU3Eb__0_1_m708DB13EFC48E9873C0F0AC74270CFE8E3E0E59D,
	U3CU3Ec_U3CAwakeU3Eb__0_2_mE6978D97020C48DEC7277BBE683DE79E500F5E5E,
	U3CU3Ec_U3CAwakeU3Eb__0_3_m76E2A6AF9DFADC710C802AB13979BF7CEC5B7CC8,
	DisplayBestName_Start_mB682DD1EFA7B036D385880D5C0B438858863E32C,
	DisplayBestName_Update_m6461D4EB24F3DBF3149B7C20769BE73F0DCA1A74,
	DisplayBestName__ctor_mE5726C180A325CFAF3A35A007A7B0F1CBA89A0C1,
	AreaOfEffect_Start_m1BCF771F26D8968CFE154EFB7A768BEF77B6DF8C,
	AreaOfEffect__ctor_m792675EEC8D631A7A8E5DE879D992E736036CE29,
	U3CU3Ec__cctor_m685D13559AFBFB9A60F414B523E49297F43F5872,
	U3CU3Ec__ctor_mF2BED9B4A1E0350101EA87B357BA2926588774D6,
	U3CU3Ec_U3CStartU3Eb__5_0_m5D1C8E26D405F8CAA5E7E92927233157873FA572,
	U3CStartU3Ed__5__ctor_mDD0FCA4B6533BB95B3546AC33074448CB76D9693,
	U3CStartU3Ed__5_System_IDisposable_Dispose_m54A36E781AE43ABCDAB8A1519129451345E14150,
	U3CStartU3Ed__5_MoveNext_mD6E09E763F30501671D6739811E4AE9AC47FE41F,
	U3CStartU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D5E1F2EC2C0F803F81E71054DA06B5F02A1EEE4,
	U3CStartU3Ed__5_System_Collections_IEnumerator_Reset_m2E89927814E781AFD1A1114C71DC06BFF9538222,
	U3CStartU3Ed__5_System_Collections_IEnumerator_get_Current_m21DA2F9D0680E2C3450473D914ACD575EC2130AA,
	CategoryAttribute_get_Category_mE4489E5DFF4EF0D5114E9D816A5D3FCFA33948F0,
	CategoryAttribute__ctor_m17974B18B10E4976D8A74983B1B4B053F81C7AA5,
	CategoryPickerAttribute__ctor_m27FFA39A126E099223A5122AB04E697EFF9E946B,
	CategoryPickerAttribute__ctor_m5AE33672FF993D1263BE7DF5320731C0CFF56D64,
	CurrencyPickerAttribute__ctor_m8E052CF026BBCD035C21987DE5F77697649DF2C8,
	CurrencyPickerAttribute__ctor_mAE7EF9F8DCD09BB0EB8BECB72A714720FF980C9D,
	EquipmentPickerAttribute__ctor_mB766502E0C1E811C8E20338B4CD8659A488A97EA,
	EquipmentPickerAttribute__ctor_mE3EF79B63BB21532E25A130344FE0AC8BCEB31E1,
	ItemGroupPickerAttribute__ctor_m5FE94DF93433CD720DA7FF2940C0A5005657F030,
	ItemGroupPickerAttribute__ctor_m6FD8C9704175B55C9FB52EB40C40301C0DAF820E,
	ItemPickerAttribute__ctor_mBFC500175B67C72650C23BDBD2288D9B635523E8,
	ItemPickerAttribute__ctor_mF667A19CE4C4FE6CB338AEBD93B60967CA11FF9C,
	PickerAttribute__ctor_m379B6F1EDA67E38B1D5200FAE8995769767D021A,
	PickerAttribute__ctor_m696E2C2E2A75EDF1A4102EF474B0E4C857E81943,
	RarityPickerAttribute__ctor_m1F668D2B795D249B61C01A84427D6CBF4E6C06B1,
	RarityPickerAttribute__ctor_m7DB777170B060BE32956BC1990EDD7FD24E65CE5,
	Category_get_Parent_mEC13083704DDFBB5061BC78872E9B0CA08877179,
	Category_set_Parent_m533C6ECDF3DCE1264B2C8A6D238F1C8E5D9028BA,
	Category_get_Name_mAC4CA2F4D0BA755F8286E854808ACBCF3ABBAD0F,
	Category_set_Name_m6CD1D0B68EFB3775A5CB509D45B8902932800AE9,
	Category_get_EditorColor_mDE4C7CC6F7948D4FE5418A8EA9DF6B1BDA26B99A,
	Category_get_Cooldown_m150926D0BFBE2FAE18D0646DEC9021D7B71D1DF4,
	Category_IsAssignable_m3DE13E19A8451FCBC1976ED09B90E99BC6B9901E,
	Category__ctor_mE45EF7C13AEA41AEE07418CAE27753C66C272511,
	CurrencyConversion__ctor_mC28E7B1EA45F562260AE0C72324BCC30A6F1F496,
	EquipmentHandler_get_Bones_m062658235B4182336CC92A9B6C15B9FC27124A80,
	EquipmentHandler_set_Bones_m99F4831852F6EFEEAF58C997851ABC6305237406,
	EquipmentHandler_get_VisibleItems_mCBA2C05511F4415F3911BED1E714BA57ED5E7501,
	EquipmentHandler_set_VisibleItems_m0AAFF9A39DAFB9A008F2B842B3296B0200D322D0,
	EquipmentHandler_Start_m91BAFAB7E4B3412F44B855FB6366D2D5E3D40541,
	EquipmentHandler_OnAddItem_m82C55350B4CF0D04D9C83FA081D717AEB01D1B2B,
	EquipmentHandler_OnRemoveItem_mD47338FF91E2B3A7FC988D49C5142CF0841DFDDF,
	EquipmentHandler_EquipItem_m360DE17606DED7C910CD7882765A5A3E2DB03E16,
	EquipmentHandler_UnEquipItem_mCDCF40160D9A6BEF09AF3A67B325BF1DFA2D11CE,
	EquipmentHandler_UpdateEquipment_mEA143A5B9AFCD4F10E86E442C4B8732946981F61,
	EquipmentHandler_GetBone_m510D1291A594A6CAF82F886525CE3E72E1E3731E,
	EquipmentHandler__ctor_mC2FA6080FD451F59CF6F88D65DF2221E7828F451,
	EquipmentBone__ctor_mD471A7062AA004CCACE63EA16051DD81C642B8B0,
	U3CU3Ec__DisplayClass14_0__ctor_m0D404DA9A5D86ECCC984D3CB2389B2CCEB852738,
	U3CU3Ec__DisplayClass14_0_U3CEquipItemU3Eb__0_m3FD5FC9769C5AA8186463C508D617058BF254A71,
	U3CU3Ec__DisplayClass17_0__ctor_m5B4067DF36AF6F9DFE43311BA98B05BAC343CEC0,
	U3CU3Ec__DisplayClass17_0_U3CGetBoneU3Eb__0_mDCDFE57746CB04060A33FCCB4CD5A1F5EA794073,
	MeleeWeapon__ctor_mC5A8AC0D00107393FFE6F703986086802CAB3887,
	ShootableWeapon_OnItemActivated_m17B22FA72A6FD547E0150CC883967A0BEE114CFE,
	ShootableWeapon_Update_m5AEDB5D849D9756CB3CA94DF467A8F29FA0CDBDA,
	ShootableWeapon_Use_m8976248588AB7D996040B954FE559DD523609244,
	ShootableWeapon_CanUse_mFF1E4AF0246B0BD422CFAA2DD8A0680759AEE0C7,
	ShootableWeapon_TryReload_mF2074AEC6E1BBF765001D97526675920E392E6EE,
	ShootableWeapon_OnStopUse_mCF994B42B44FE63AFE18BD03309E8A70A99F8D47,
	ShootableWeapon_OnEndReload_mBAAD7E4E23295F76F4C412DB894DDF809D468F9D,
	ShootableWeapon_CreateCurrentProjectile_mE21A60BBA70FCD32731D1182278091BAF96B1CD0,
	ShootableWeapon__ctor_mBC58068E02CE8AF626157B069D95270665B8168C,
	StaticItem__ctor_mDD661D949ED72198CEC6C373D45315D81E5F2080,
	VisibleItem_get_Callbacks_mC7F094E29A684C67C78BC1DA5B0AF2FC69B31DBF,
	VisibleItem_Start_mAFAE5C514C1AAF3F38CEF0A5D4E6C255F3A09875,
	VisibleItem_Awake_m8BAD1E0F55204193D3D8F27D126C497F311B43B4,
	VisibleItem_Update_m0BB597F4AD10557B304082C782C9FD948C6C37B6,
	VisibleItem_OnItemEquip_m331DA56D076AB64DBFE3B25A407EE13A77912756,
	VisibleItem_OnItemUnEquip_mA757BA1D99796D1026EDE7CF92DABB76FC24AA65,
	VisibleItem_IgnoreCollision_mC6E2D2C982B8C2BA3C0930C7230A26F93A62024E,
	VisibleItem__ctor_m5A81A0319E91581C4A7B8D663D706D4083850CBC,
	Attachment_Instantiate_m7D9EE00E6517589518F548E0A0883EB44F0D25F9,
	Attachment__ctor_mD76902D6EE601C99BF3CF5292A7D6F332C26C2C0,
	Weapon_get_Callbacks_mF9BD0999F9FB8935DF5754480CA0C330A0AD548F,
	Weapon_get_IsActive_m3DFACF02A0AED62AB8C6235AA67C8ECA6711F399,
	Weapon_set_IsActive_m22C9DBDD52DA81C7BB0F269D63337B404CB276FA,
	Weapon_OnItemEquip_mCC6601E920715FC50B31BE2921F6D2B9B3908686,
	Weapon_OnItemUnEquip_m5705311A10556594D5411542BE040555C349E297,
	Weapon_Update_mB512BCF1F820D8335D38F2C38305B098DE418C21,
	Weapon_TryStartUse_mE9D75B3B5B65FAC166DAB3EBEBE45310AA49F771,
	Weapon_CanUse_m29D2768FAFAAB1D72426C90B5A6D50D15471326E,
	Weapon_TryStopUse_m27A67116E6C6928B8A2B4E04D2E366285E6065EE,
	Weapon_CanUnuse_m3DC11D2DCE95A0E63C3C65BAEAA7EC0F23F27A1C,
	Weapon_StopUse_m8D618148B84A4D20C6F2ED666040C828805FE79C,
	Weapon_OnStopUse_m153574121A19FE690A034C28F3F4F91AD3BDC0AD,
	Weapon_StartUse_mB41B59F6D143A16C45DD841FEC44AC6A652C034D,
	Weapon_OnStartUse_mCD6D7DD6FB0DB9445AA053A26B66136076537ED9,
	Weapon_Use_m60F5BF06C56EA2AA4D09DF096C0748C627E0E947,
	Weapon_UseItem_m5AF108A20A06BE402484E882204D4476AE7BF172,
	Weapon_OnEndUse_mD27F8D4A0A0F573DC9DDECF54B83AD5357BD1671,
	Weapon_PauseItemUpdate_m11EFC7666793B0FE3A8EA6537972CC0E500E9501,
	Weapon_OnItemActivated_mBC86304A259342A9CDB8259A3B41C65CC25D03F8,
	Weapon_OnAnimatorIK_mED78BE8C7D510D62620CF26D0CC61EFA83562483,
	Weapon__ctor_mAFCCCF86E43E31E0DDBA2F89B648469BBEA3BACA,
	EquipmentRegion_get_Name_mE9156A7BE449EA2F9B3BDD27CCCDFE8741A6FC07,
	EquipmentRegion_set_Name_mDDE8F2923FC64102912253FEBD6ADFBD129CC525,
	EquipmentRegion__ctor_m84E39EEF627B8A3595616790E22B1A18DA4692CC,
	NULL,
	NULL,
	ItemGenerator_Start_m56995A90E5D014D0F546703DEB8BD66FC8937711,
	ItemGenerator_GenerateItems_mBCEAB097D2E5ADF652DC73FBDC2C445A1074B379,
	ItemGenerator__ctor_m7DD90283517E45B2800B3C60C98B622216ECEC27,
	ItemGenerator_DevionGames_InventorySystem_IGenerator_get_enabled_mF98E361579B397CB0447516CE29E9EFB25AE20A3,
	ItemGenerator_DevionGames_InventorySystem_IGenerator_set_enabled_m60B8377308D22A8E536CE0F3BB542B9DC2EAF286,
	ItemGenerator_U3CGenerateItemsU3Eb__3_0_m674BC7BF5C695E2D0CEB7586F350F7EF62C8A8A8,
	ItemGeneratorData__ctor_mF2EF403776B28D83DAF5B0545611F03D20166E70,
	ItemGroupGenerator_Start_m78CF28AA1AE46A3C8B3095A2EEBD4DA9F057C4EF,
	ItemGroupGenerator_GenerateItems_m83BE43AD49F680DD2EA72A4C37C8CC70BAF29319,
	ItemGroupGenerator__ctor_m82AA47D12AD0CA828A07C32DABE5837C56B4C6AC,
	ItemGroupGenerator_DevionGames_InventorySystem_IGenerator_get_enabled_mF44C88E7C736D8E48F6A64DD69C17746945B03C7,
	ItemGroupGenerator_DevionGames_InventorySystem_IGenerator_set_enabled_m6BFC2B44643AE53AF3CA8C65EA6E909738C3B0DD,
	U3CU3Ec__DisplayClass9_0__ctor_m6232303D57B0FECFF722519E957D0ACA81A52419,
	U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__0_m61D18D2567B3EB8CF59726B611E5049FE8970034,
	U3CU3Ec__DisplayClass9_0_U3CGenerateItemsU3Eb__1_m45DB1312E7BDEE24A58170DC0D53CEB1DCD4F791,
	InventoryManager_get_current_mFB04BF99D34138705FC3BF9AFD8A4F0257BB35E7,
	InventoryManager_get_Database_m37E60244F2CB2229598CCBDEF7765F8528EC46AB,
	InventoryManager_get_DefaultSettings_mEBB604D541294EBFA1C7790A00061D0E3302791E,
	InventoryManager_get_UI_m83405D305CE6DBD0B4902BBE1AFBABBBF442D649,
	InventoryManager_get_Notifications_mEDB9294A13E142ADD091E293C9061DD95D73067A,
	InventoryManager_get_SavingLoading_m1A94EDF9A0184831AFC2CC9947605D6E8886B76F,
	InventoryManager_get_Input_m2922836FE3BE47126B894DAEADA94DB0358EA2F9,
	NULL,
	InventoryManager_get_PlayerInfo_mEFAEBE0D32A3439A0E5FACE44A9BD10502CAC6ED,
	InventoryManager_get_IsLoaded_m9FF27BA29FE0DE34CD22FC1353087F06DC19262E,
	InventoryManager_Awake_m5B556D0929DD27FA99FADBE627A4255D2E624D99,
	InventoryManager_Start_mD77178711350B9B78A323DFA03DBFC5A1CC16185,
	InventoryManager_ChangedActiveScene_m0448E967DA6D1EEDE4001E777BEDF2FA77A074E4,
	InventoryManager_GetBounds_m7490F78E6B2A3B1806058F81D20E63F53C816E53,
	InventoryManager_DelayedLoading_mC2BAB7CBA7F1F2CF9F329B7DB66264EE52C056A3,
	InventoryManager_RepeatSaving_m5D8702EC560A6F60DC5CF51077CDBB350BE26F0E,
	InventoryManager_Save_m29DDA4CC814F6EACEDCC19F98047F20F4D72EFE5,
	InventoryManager_Save_m111038F104D8392D39BD065C62CA487710B0D315,
	InventoryManager_Serialize_m987466A40A471EF596529710FB5DC687E0E03323,
	InventoryManager_Load_mFA8E753F77FE12B1D52E3F0C99A1F60AC14BEC29,
	InventoryManager_Load_m1A8274A328F990EB2E9D244FA357B5E5A093C7FD,
	InventoryManager_Load_m29705DEEDBC512871267BE117AAAEBF0C4CDF3C4,
	InventoryManager_HasSavedData_m7F27DF6225D8EE91A5A7F0391702FABBF4C45E20,
	InventoryManager_HasSavedData_m4C28884B48653F35A1A886123B683C065C22F967,
	InventoryManager_LoadUI_m70C94265BA670CE84621921A2B4834A0AA6C21B9,
	InventoryManager_LoadScene_m8F8CC773388786FCB3050F1A1073216B0600C1E9,
	InventoryManager_GetPrefab_m9D187129F4716B3867C2916D1C3E34F0F3034D15,
	InventoryManager_CreateCollection_m9CB850A14F4D2785E47A84E7AB7194C43DD179F9,
	InventoryManager_Instantiate_m0E90DED729E0832634C2577F5409BBD57733E23B,
	InventoryManager_Destroy_m6214D00210BA54942F08EB0D2C868759F1D66A5A,
	InventoryManager_CreateInstances_mF67891B0A9830819D61BA1DC62F30E403EEA4673,
	InventoryManager_CreateInstance_m36833D1D23D7CDD61094A02489F67AA472183A93,
	InventoryManager_CreateInstance_m7F8CB08E8C8B5E7CF0A08201230827CF19D209ED,
	InventoryManager_CreateInstances_mC646412A6BAF31B0AB5C3DED3AADC3D1055E497D,
	InventoryManager_CreateInstances_mF80625F6891BD4AED9B99CE1B5A1D7159F534BF1,
	InventoryManager__ctor_mB5093BF24A06BF7EF36E923D3A8179790E2EEF78,
	InventoryManager__cctor_m221200C861C12CCE7050A1EE47180A519A1C7493,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mB402ECA7F064DD7AACF31C481E27071B30BD4202,
	U3CU3Ec__ctor_m63622E9ED83CB47C3E47EE3FE7C877A155D772D2,
	U3CU3Ec_U3CAwakeU3Eb__33_0_mE160AFD43EAED028ABA61B4EB0DDC873121B65F0,
	U3CU3Ec_U3CSaveU3Eb__40_0_m8247E5AEBD285075F3A123EB22CF369250EC2F72,
	U3CU3Ec_U3CSaveU3Eb__40_1_m7BA85CAB63DE153D9FFD50C51401F31E5FDD8AE1,
	U3CU3Ec_U3CSerializeU3Eb__41_2_mB76292D7BB891B21B2BD1378B31780FA0690DC73,
	U3CU3Ec_U3CSerializeU3Eb__41_3_mF318BFBC58FDB6FA304A7CC2434E585215FBAE06,
	U3CU3Ec_U3CLoadSceneU3Eb__48_0_mB4408815982A69C7A35AF6086BB3C513421454BC,
	U3CDelayedLoadingU3Ed__37__ctor_m3325202AFC5623802BE84D44B0A7DDC9DE29095E,
	U3CDelayedLoadingU3Ed__37_System_IDisposable_Dispose_m71FFD192424C855DB335AC3AD8EB218444508066,
	U3CDelayedLoadingU3Ed__37_MoveNext_m30FD2E3EF1AFA7ADF26E3A358FFD5D17291358B6,
	U3CDelayedLoadingU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE895F35F30A0DC7759F22C9E344873FD0F449B,
	U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_Reset_m5E31818C9301990803658065761372C698261518,
	U3CDelayedLoadingU3Ed__37_System_Collections_IEnumerator_get_Current_m3B16221AB84E47B898C986B4DDC35AF4D0511C1C,
	U3CRepeatSavingU3Ed__38__ctor_m6BABEB7DF2E5AA16D319ED98FCF12AD557763172,
	U3CRepeatSavingU3Ed__38_System_IDisposable_Dispose_m06C133222BF9EF721288B8864DD7BC84857CC580,
	U3CRepeatSavingU3Ed__38_MoveNext_m44BC6F57F59888CB6FCAAF324C522EF5CDFD3717,
	U3CRepeatSavingU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9434F8BE699510BBA853AE850EDA9590EE455611,
	U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_Reset_m4E523C49553E9E05315E213B36ED6E4A9BE347A5,
	U3CRepeatSavingU3Ed__38_System_Collections_IEnumerator_get_Current_m5BC6CCD95656D52496CF70BBEDFB46BBC364A187,
	U3CU3Ec__DisplayClass41_0__ctor_mC41321735C1D85A5A6B30DE2008606D4D9E581F6,
	U3CU3Ec__DisplayClass41_0_U3CSerializeU3Eb__0_mE39020694A5FB2731BCA1FB59C69BFCCA572A0A7,
	U3CU3Ec__DisplayClass41_0_U3CSerializeU3Eb__1_mC64543C490A4E9D2408333AC0A0161D869C67975,
	ItemCollection_Awake_m693D404A6DAF0F8123CA7DA29AEE76480D39121E,
	ItemCollection_Initialize_mF3156F7E8CF9487D9CF2EF55EC98D0996F13EE10,
	ItemCollection_get_Item_m7AF9F02E7A66AACC6CD48D40D2226554AD436746,
	ItemCollection_set_Item_m2B003BE3327D8C11B066D4AB21D0F01D43E6D31D,
	ItemCollection_get_Count_m1B93C4F9667813E1A5EBE5DBDEC1AF20BFC4ED55,
	ItemCollection_get_IsEmpty_mA43141264C534C0135DA6C7C0D96E93AF2A202BC,
	ItemCollection_GetEnumerator_m4CA97070C1FB3F01816BB6F2C477C0728F5F2168,
	ItemCollection_System_Collections_IEnumerable_GetEnumerator_m77653EA77A992A5836A813A3AF48CEF34B7E55FC,
	ItemCollection_Add_m4ADAD380A52AB62943A405E671E94DFCBC5635D2,
	ItemCollection_Add_m575A8C7545EA8C045E37B2DFBEA59DF5DBCC4854,
	ItemCollection_Remove_mCA3BDDFF65CC1C611F9A760F23CEB105D67F1270,
	ItemCollection_Insert_m5DD9AF26212975815009D9D44B9E2077D6B83CAE,
	ItemCollection_RemoveAt_mFCC5D8BC4D0F560133BDD7E7EBCB9E86F2127633,
	ItemCollection_Clear_mF3871DB53E6610D93DB8B48CE065639F043012D7,
	ItemCollection_GetObjectData_m6AF56B2A3FF4609070387119A3D0802908DF02E9,
	ItemCollection_SetObjectData_mAEC37CB1881D2D391A230019F4AD17AF6F177FAA,
	ItemCollection_CombineCurrencies_m096417AEFB9B954B56B70D414C3AF5F01D437823,
	ItemCollection__ctor_m41AC127F351CD9C2D242A0780D312FF9AD524E3A,
	U3CU3Ec__cctor_m68FECCBCD3AB0712E1183230371F4ECD3A414237,
	U3CU3Ec__ctor_m493F8859F494A62BD4A97F72FE1F1E78BE0CAA35,
	U3CU3Ec_U3CClearU3Eb__22_0_m49BED5F879949D295F8F88ACF2E18F6B998152E8,
	U3CU3Ec_U3CCombineCurrenciesU3Eb__25_0_m6400172305D5FA5BC0249B6975DCDD3366DA578C,
	U3CU3Ec__DisplayClass24_0__ctor_m48B137162483D13B1983C403695CED849BAA6D25,
	U3CU3Ec__DisplayClass24_0_U3CSetObjectDataU3Eb__0_m0D1038DC858E12DCA1BDE1BCE424CEE81C333EA4,
	ItemCollectionPopulator_Start_m3B23C47B91052B351D702198151618BBAB280D52,
	ItemCollectionPopulator__ctor_m5EA80706CD847E77758E31AE3AC7893B1C79FAAA,
	ItemCondition__ctor_m14B34DB4A8A6A6ACD26DFF63A2B5DE04ACEE6F0D,
	ItemContainerPopulator_Start_m1F73EF5E6A5086FE713A199F2F3F16A4763B8F8A,
	ItemContainerPopulator__ctor_mB84DA902A75727F5750E849B94C019B5E9EDC5EB,
	Entry__ctor_m45CCEFBA047A44BA862051D9E41682E0CE7D5B9A,
	ItemDatabase_get_allItems_m1635746DD2CCB010B14FF1ED3E40F8F07C693484,
	ItemDatabase_GetItemPrefab_m06BCB4230BB9B4BE40DA62EFD28B63178F479A13,
	ItemDatabase_GetItemGroup_mE6A9BCBA97AC58E90332945EAC7824F516E4BBCE,
	ItemDatabase_Merge_mD2169A3EC7B4CCF5585B1DE84CBD0CDA71FD9866,
	ItemDatabase__ctor_m070BF716C00550CEC2AD98392EB55C4279B0AD4E,
	ItemDatabase_U3CMergeU3Eb__11_0_m3B3F48AA98F934853BDB4646CB7A0DC823F031EF,
	ItemDatabase_U3CMergeU3Eb__11_1_m77CBC2BC2BA3096537EEF5F026EED3C48B19664E,
	ItemDatabase_U3CMergeU3Eb__11_2_m808DCA78EC4202BB44A393415ED0270388CB6730,
	ItemDatabase_U3CMergeU3Eb__11_3_mF0E9185A251DD88FFAE0ABDAE689984884A2B9E2,
	ItemDatabase_U3CMergeU3Eb__11_4_m96F50044707291B8066224E9D89807BE41574364,
	ItemDatabase_U3CMergeU3Eb__11_5_mF41FB56C477F6EE7E2939F43286360F76F89BAD6,
	U3CU3Ec__DisplayClass10_0__ctor_mDFA45F3813076EFE9C1C15C593F8EE23B9595E60,
	U3CU3Ec__DisplayClass10_0_U3CGetItemGroupU3Eb__0_mD5336BEA23CBD87A0405598E9C5500320C41AA73,
	U3CU3Ec__DisplayClass11_0__ctor_m02D9EA5414D3D7771C112C640679955B6FACEED3,
	U3CU3Ec__DisplayClass11_0_U3CMergeU3Eb__6_m6E4398A632AF5281525451DA84FEB2FAC092D816,
	U3CU3Ec__DisplayClass11_1__ctor_m3CDDC3F336D033E80AE18380C8DB444B501E1FB0,
	U3CU3Ec__DisplayClass11_1_U3CMergeU3Eb__7_m4A06110E76F0AFDFDDF63DF70CD57758A8C5B936,
	U3CU3Ec__DisplayClass11_2__ctor_mBEA76D40F3FB5FB4161CDCFDB17A34172AF7D7F0,
	U3CU3Ec__DisplayClass11_2_U3CMergeU3Eb__8_m3BE2DAA1450D7210C235221F1DFE0F5CFB952854,
	U3CU3Ec__DisplayClass11_3__ctor_m9C281F43BD87D90C7617E368A682AEABB8E7EF84,
	U3CU3Ec__DisplayClass11_3_U3CMergeU3Eb__9_m03575315A10CFFD04FBFCFB9F7C6BD58EC3370CA,
	U3CU3Ec__DisplayClass11_4__ctor_m7D20B898349551DFE3B704D9BF98064B834DF9C7,
	U3CU3Ec__DisplayClass11_4_U3CMergeU3Eb__10_m8F16DA14EA9489646CE196D106818187836C4533,
	U3CU3Ec__DisplayClass11_5__ctor_mF78EC76B71BF3B03892829A68B4478EB4CE85A77,
	U3CU3Ec__DisplayClass11_5_U3CMergeU3Eb__11_m6FCD5A6F233BB10323DA645180E16C611E5D7A1C,
	ItemEventData__ctor_mA35E230581C34A69BFEC494AE2976B6A1F94D765,
	ItemGroup_get_Name_mCCA6ED817991597353AC2D5C175CA137057FE96B,
	ItemGroup_set_Name_m3D1A99C7594965F41661D0599ECC0495C5A2E988,
	ItemGroup_get_Items_m6B2F0E3AAE4E4C656103716D023E2B89D5BEC2EA,
	ItemGroup_get_Amounts_m4CD2F43F8352EF1EB8DB7D34DA9249E320CFBD7E,
	ItemGroup_get_Modifiers_m4D238521E86C6252714114C71096BEF2CB60A16C,
	ItemGroup__ctor_m809A51C5C9154EAD7E84AD5EC21FB33DF770BF22,
	Currency_get_MaxStack_m0B419DD7B2AA59B743A042CB3FAA828445AAADE8,
	Currency__ctor_mB6DB2EBD0C7F1CD4E75982CCAC6B4EE859655DAD,
	EquipmentItem_get_EquipPrefab_mCCC2417F80AD679D81124A1FB2B0EF1C84BC7A8F,
	EquipmentItem_get_Region_m065A5969CE54EFEB34FF7374EF58622767B41E0E,
	EquipmentItem_set_Region_m6D649B9B2C942DB09D15CCD60E5CCC46056D6BCE,
	EquipmentItem__ctor_mACBD87A107E347FED6C7012B25D923AC5AFBFE8F,
	Item_get_Id_mC68933B60BB1B8D24B21231BA02C070419C59E55,
	Item_set_Id_m167E3C57A18376DA4EF46CC5C2593C8004697DE1,
	Item_get_Name_m48A8B0F8397F7930E5BC3D85DD5E71468ED8BEAB,
	Item_set_Name_m17EC1514286117F156C004E23C98A4CDE7C2472E,
	Item_get_DisplayName_m4DE5829073A0A2EA30EA47CBD49B513704C12909,
	Item_set_DisplayName_m2FC5FD256CD876F3EE0171726EF2284731BF1E96,
	Item_get_Icon_m5458CA78A43074CA889D686A51B3E8ADB864514C,
	Item_set_Icon_mA2711FB5CA50C6C1F548F8F84DCCF32DAC2FF125,
	Item_get_Prefab_mD522B985D924BDE518E5A5D62EACB2E762C375D4,
	Item_set_Prefab_mA077E6C383C84BA880C64E8B2E35122C8DAA01D2,
	Item_get_Description_mB7F9BB622A01FD5720F1573FA9716698D415833F,
	Item_get_Category_mA2301BF94DD3C9FCE88171937B00B79DB07844E0,
	Item_set_Category_m349DE64378D71A5A6770244EA5CB8CE948EED857,
	Item_get_DefaultRarity_m6886F014737E6B6D318351B9F5C6A3E2D4F16275,
	Item_get_Rarity_m6E02A83FCD5D5C97BE560D537A88E9EFCB86F8D6,
	Item_set_Rarity_m03F630817FA8F647C99649BD9BDC9C1433BB297E,
	Item_get_IsSellable_m3DEAC715603E2BA6AEAD713758DFFD0FD577806B,
	Item_set_IsSellable_m5BA77C0C37EF21E38E3EC617F3AAB6231FDEA299,
	Item_get_BuyPrice_m46EB74805728696A53C379A637EEF269A45395B1,
	Item_get_CanBuyBack_m3402923063AE1D7AED52A41E8081549A72D1F417,
	Item_get_BuyCurrency_m2A371F5A4BBA33885093CC75E3B9984F97BC12AE,
	Item_set_BuyCurrency_m5ED60538F150ED3F05E15A4D3F7849E11E8DDA64,
	Item_get_SellPrice_m2BD301F4010A1F21450D705917877B7D98C5D603,
	Item_get_SellCurrency_m52419C8985F5C719A86C5EB23928E6D0A6665421,
	Item_set_SellCurrency_mE9291662A3F081856328B73285884756A3778B8D,
	Item_get_Stack_mC5F93507A3A6EF0338C8998718305F33B4AACCD8,
	Item_set_Stack_m29FBA73C29132F15E2CC8CE325EB96853F401710,
	Item_get_MaxStack_m001C7CD1991CEB33C51B605638E599F6586784D1,
	Item_get_IsDroppable_mC460868B08AEAD6DF94CC07E9F784C7CD1B3242F,
	Item_get_DropSound_mDA9BE818659DA3E7576D4FD781BEAE7E34E84E75,
	Item_get_OverridePrefab_mCDB40AE39079E2951E3A26FBE5C6A1A9762F058E,
	Item_get_IsCraftable_m53555B158AEA489EB356BDEB57B3B090227F6358,
	Item_get_CraftingDuration_m561C905DD5CC974FFDF0173DACC656A976386B4C,
	Item_get_UseCraftingSkill_mB1B254CB27615E03774C38C82DF65A263476E5EA,
	Item_get_SkillWindow_m71BF6B435DDE78D0A779CA74B2BF2C28DC3E021F,
	Item_get_CraftingSkill_m1C9D2B74FA8F0E4F11954FCD926EC3CA6E2FBFA6,
	Item_get_RemoveIngredientsWhenFailed_m8EE0146655784C5A101F3D52A393D8F80D8E3D4A,
	Item_get_MinCraftingSkillValue_mBD3D7F63DED5A1085970C8FDD7059C0F8F6A3BF3,
	Item_get_CraftingAnimatorState_m2C74AAA92FCF29EB3C358036C9FD3C436E4CFA24,
	Item_get_CraftingModifier_m079342E1042104C92F04609D62102A177551F568,
	Item_set_CraftingModifier_mFDACA6275476D5A8561B5C0B47B04544DD79371B,
	Item_get_Container_m7F5E433EB6970B45D2C036DC702BC4412F561CDF,
	Item_get_Slot_m7C1D63F71DA9273702C9E0C88E1E007D5B08195B,
	Item_get_Slots_mAE3B8ECD03E6B17DDF146D9AA5AA2251D9E4EDEE,
	Item_set_Slots_mDBB2DFEB8025B1F02534B7FC2BCC5DE47EA07429,
	Item_get_ReferencedSlots_m15DB7D9F803C55237A745ADA270D87566B24DB02,
	Item_set_ReferencedSlots_m0AC9C0D223583A478E52D2418C5D20D50B59DBE6,
	Item_AddProperty_m8969A08002CDF0D695861C88A67E57CCC8581748,
	Item_RemoveProperty_mA5106B1FC81339A9D1CEBEB6BE86373B90626D91,
	Item_FindProperty_m8E9DE0170CAB1A0036E814F1E84ED3D0AEEB8FF3,
	Item_GetProperties_m45761DE618E4D045D6B312B96A91A02F004E0029,
	Item_SetProperties_mFBC76141B41920E66BA56FB442B2F9A2D787699B,
	Item_OnEnable_m9A8B0DBE7D3EB46AF649C8801B911B39B16ED79F,
	Item_GetPropertyInfo_m9375BAAD43D6C4B78059B15451509F462D127453,
	Item_FormatPropertyValue_m815C7728414997897FA7B785FBFA2B7635820E5D,
	Item_Use_mDEF5065C85B35D44020E09EF36AF1E877C850CF0,
	Item_GetObjectData_m4F8AEEAE984B434681DC1333E1C57622B4DE79A9,
	Item_SetObjectData_mF61BBCE72F73B1EB004D8B6EAD53CA2A29176F15,
	Item__ctor_mC5E29BFEF8CEF634E46ADEF707858EC8AB27592F,
	Ingredient__ctor_m04BEA8A6F71AC8C94F4E609CCBD6061D8B243CC5,
	U3CU3Ec__DisplayClass115_0__ctor_m99C151AC043E2B1402AA40A3BEACA6FD37E0CDC5,
	U3CU3Ec__DisplayClass115_0_U3CRemovePropertyU3Eb__0_m858859620F96F1E803A20AA5195E2331F31D72D4,
	U3CU3Ec__DisplayClass116_0__ctor_m2A45B266B276928DF4EFD7BB7EF2375D7F0AE4B4,
	U3CU3Ec__DisplayClass116_0_U3CFindPropertyU3Eb__0_m5DA2BF5197E5E8BF6961A0D7EA2622FBEAB5CCC0,
	Skill_get_CurrentValue_m4CB43217FFFEA726849A3AE4BCD4C176E6258752,
	Skill_set_CurrentValue_m05259265E725BDDE56B4DA1937993DB5CC10531B,
	Skill_get_GainModifier_mA9583C5DD66E63548DEA93641758ECDD2169E195,
	Skill_set_GainModifier_m09C88D51AF393712758DC788F2689C1399E11141,
	Skill_CheckSkill_m767FE99ADF0FAB6BC4A11C25E930F1BAB3391777,
	Skill_GetObjectData_mE6039C02AC74CE3A3EA143B31A9C6A3BE6E1F186,
	Skill_SetObjectData_m676DBBFB2939E154461D607F535FD64A0366930F,
	Skill__ctor_m4EA6EF4E275B3FA01E6B518694B11BD521B9BCEE,
	UsableItem_get_Cooldown_m92F24ECB8F5B92C83F774F4A08B07D5269B928FB,
	UsableItem_OnEnable_m6480298E8907F939BAD07BD72F53AFBB43819565,
	UsableItem_Use_mB0193A80D617795ED9C1E4128E6E3A7D0B82703F,
	UsableItem_SequenceCoroutine_m318A407A7B94147DD078E37D2D08C43C775EA045,
	UsableItem__ctor_mE23FE937BEFC8E75ACFAF37237025DB681CF1819,
	U3CSequenceCoroutineU3Ed__9__ctor_mD0D6AB8E654C1550E7FCD108F6B70B84E7BB1B05,
	U3CSequenceCoroutineU3Ed__9_System_IDisposable_Dispose_m12DBD516F68B381F1311AF75F52DC4079F86D56C,
	U3CSequenceCoroutineU3Ed__9_MoveNext_m242BC4E9F62F2AA66CBD209A64F38ACC8BECC5A5,
	U3CSequenceCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m887024614CCE999B4A10D64456BC90F39F930864,
	U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_m1C47EA075B14753C5A1FF2838E5626CE90095863,
	U3CSequenceCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_m2DC9FF56B6AA56E737B6002C8AE95781725548AD,
	NULL,
	NULL,
	ItemModifier__ctor_mA89A1F54C43102B98529F4AFB5A71A5169173733,
	ItemModifierHandler_ApplyModifiers_m7B1676F97ADCADBE31EF165033F0A8355B5323BF,
	ItemModifierHandler_ApplyModifiers_m606BDEFCCCBC841FE4FE1454D7CB1C26BDE9A001,
	ItemModifierHandler__ctor_m68E489CB1EDBFBF054E1D3D0D9E5F51CA1015E30,
	ItemModifierList_Modify_m60ED1BB76F55CD94927DFFEFD8889124814FD5AE,
	ItemModifierList__ctor_mE9687C66E8AB78D39A8D614C02327B4D1D73EA4F,
	PropertyModifier_Modify_mEC0DE08982F8122141A400A99941702636D615FE,
	PropertyModifier__ctor_m5A0A979E4E6ED5E55D997738229C882417D36C2F,
	RarityModifier_Modify_m6E9B5AEE0896325223464BDF01142AE080A9153D,
	RarityModifier_ApplyPropertyMultiplier_mBB5BAE333006D6EF8C13B5289B1C5552E389358C,
	RarityModifier_SelectRarity_mD43C33889A2503405CA7ECD360792C3376978799,
	RarityModifier__ctor_m4F3231F880BA070B6A6135B37CB5C972E2E42169,
	SkillModifier_Modify_mEE05B09BC52EEA902EFC48614A75740E23CDCF7E,
	SkillModifier__ctor_mAA3110007C103374A4645D92A655BCEEFBAE9EB6,
	StackModifier_Modify_m988FBF18B703FE307729F8D7D33E072E481679EF,
	StackModifier__ctor_m2AE8A5A2BB738B83C75590B533A8E8F7D57343CE,
	PlaceItem_Start_m08173BC180D32D6A775849B90BCC23FBDF87A13E,
	PlaceItem_Update_m26A3F61C13E8B061795C427F466FFC3D90E1A0B6,
	PlaceItem_Build_mB87F750289475FEAF081BECCAED80A6C66DAC5D9,
	PlaceItem_SetColor_mC71799A90FA0C7CC124B3C757335E3CB0C7113F7,
	PlaceItem_CanPlace_m591E85D95FBE79114F7812C1F6C7C67372720B4A,
	PlaceItem_GetMaxCornerHeight_m5E122304A4342B9ABFBB2682F8C07919208F8BAE,
	PlaceItem__ctor_m0C53F427F341834E2CA63A46DEAAE2CCBB9CBE1D,
	Projectile_Start_m5BE8CDE1249DB66840856B6EC50CB3FEDFF8F22C,
	Projectile_FixedUpdate_mCADC3918A8902EA129725776F7ADDCE3FD86CE5C,
	Projectile_GetTarget_m5541F87662A9DABFCF0872BE01AC8B42D6FC73FF,
	Projectile_SetStartDirection_m12038BC58FC764E2F34FB510D076012F24251CAF,
	Projectile_CheckFieldOfView_mE9FC77367A6497CA4B6F270CD34819FF5CEE3FDD,
	Projectile_OnCollisionEnter_m5F8B93D57791399E93CC0FDDEE8B6984C4C1450D,
	Projectile_OnHit_mDAFE6ABB55F814B9BDD2B81BB9694A19F5331B2D,
	Projectile__ctor_m0EC4D9F2D3003EB7F00FF17A4973D9002437730C,
	U3CU3Ec__cctor_m6690FDA4E8AD1A72FDEB11F4442DBE7D8A3F0A18,
	U3CU3Ec__ctor_mAE34D66987FBBB74F62935C8E358292FFD8E1453,
	U3CU3Ec_U3CGetTargetU3Eb__18_0_m82749985C0FEC325E7953876524E65224292FB8D,
	Rarity_get_Name_m6E789C9855F51D1D7E2D5625B631C50427D42B21,
	Rarity_set_Name_m18C9AF8987D0433B04ED1772B2F1159C59C48FD9,
	Rarity_get_UseAsNamePrefix_m1BFC426CD23E5432C0CF2B0214CFABDE147B7FF2,
	Rarity_get_Color_m9D433820F051AD1EEEFADCFE348BAADA21066420,
	Rarity_set_Color_mACE4035145A4E96C87A3700261CA6FF5ABB6F2DA,
	Rarity_get_Chance_m175BDB7BBC2C946ACC12AE807D30556F190CED4E,
	Rarity_set_Chance_m158CECA2A13EFFC8B2B5150555AC03DAEDA62B7B,
	Rarity_get_Multiplier_m865268FA667BB95371E9E23A2ED3F36D1FABC0A7,
	Rarity_set_Multiplier_mF3491850D66D4AAFD6BE0CF6D27718587E987FCD,
	Rarity_get_PriceMultiplier_m6F427CB2501D3E68BDB1D42D5549314510B521FD,
	Rarity_set_PriceMultiplier_mFA3E782987E7A4DFC8F2538E2322908A8238DC4C,
	Rarity__ctor_m2A5936C41FC4C9D7CD921C37956C9ABE3AE7F95E,
	NotificationExtension_Show_m48DF9540C59F0326B6AAE33927949EC5A060CED1,
	CompareNumberNotify_OnStart_m951E077108A2AB943365D4FBD820EDCD5C69AEEB,
	CompareNumberNotify_OnUpdate_mBC555EE9D27AE831B5945B773592347C0A658C4D,
	CompareNumberNotify__ctor_m92420E92461EEC51DFA51B14FFB7773CCE706AF5,
	InvokeNotify_OnStart_mAF015EEC274AA0278FB344F84568961558C85D63,
	InvokeNotify_OnUpdate_m5B40CB44635F0D150DDB0EBBE54BE0B01A1B6F02,
	InvokeNotify__ctor_mD850C60BB3D3767C7549F44D9000B88E69CE0316,
	CanPickup_OnStart_mE261AB8ECE6B43A506DD38FA342093ACEC65CC71,
	CanPickup_OnUpdate_m43A3BECF904A969EF5313D2A5BD493E035C85DD6,
	CanPickup__ctor_m5ACCE1B60AF8E0407917E38CFC9E21E06D6A9F25,
	HasCategoryItem_OnUpdate_m9C71C562C952256CA48A72E4FC0CD0E4C9B9F5AB,
	HasCategoryItem__ctor_m276EEBFE9B8303A2A30FBDC71F997E5640926CFA,
	HasGroupItem_OnUpdate_m4B6826A3642BC56E93DEFAAE9F3B944650021F52,
	HasGroupItem__ctor_mBE92C33C043577A66C0A99E0F966409056EDC2C4,
	HasItem_OnUpdate_m566ECE7943A7A063FD4D5B769145D67A4FFFC311,
	HasItem__ctor_mD297085A790CBA7F897E71D304CFA276499DC551,
	Lock_OnStart_m41293A641BEDBD3288B5D614E54A4FFB6AA172BD,
	Lock_OnUpdate_m839EA5F9AF1E8EA39155E598A0AB6659599686CC,
	Lock__ctor_m3433C011B796CC21D419CE95F833946C5B9BDEFD,
	LockAll_OnUpdate_m60015AC729F9D040F30CF6D1B2A9CD844245D003,
	LockAll__ctor_mD8299DEAD074C98EB232844FD3E23169E17A84CA,
	Pickup_OnStart_m2AA2A7004B2B3C22EF443A8A35375BA6541ED77B,
	Pickup_OnUpdate_m5EF4DEBEEB7038B97688C814A8BB0BA058EEC149,
	Pickup_PickupItems_mF079F45DF4A77A2A1ACBE23E7E8E5F1F73D61E3B,
	Pickup_DropItem_m01642CB6A93C450F365D2DEF5AAB4F05AF1A8605,
	Pickup__ctor_mDF4A6AD02532D0603ACF05A661441CE1D06EDC88,
	Pickup_U3COnStartU3Eb__4_0_m20B8279A641750A5962EEC43C0DB6F093D93C95D,
	Save_OnUpdate_mB7333962A717C8687143C0133175032BF951BCBB,
	Save__ctor_m13276C645E34589C2D840CFE1B3F5779F88BE3C5,
	ShowWindow_OnSequenceStart_m5BFCDAEFD34EBEF4759CBAC5D17CAD3BC5E7BF63,
	ShowWindow_OnTriggerUnUsed_mCC90EA833A95E3787F42BAA210764275B05AD57B,
	ShowWindow_OnUpdate_mF6C09EA6D93B25248A0EB651298E8F827BD8A76D,
	ShowWindow__ctor_m96FCDBCE8A397C30FE26B3278D4D25FB4ED185C3,
	ShowWindow_U3COnSequenceStartU3Eb__5_0_mE8B5127A81299FDC5B31D9129E6BD811DDFF3A04,
	ShowWindow_U3COnSequenceStartU3Eb__5_1_mFB0E22A61357DA278D83156338B29F01990D4DB3,
	RaycastNotify_OnUpdate_mC0A258CA695955DAB0FA65701B838CA299693166,
	RaycastNotify__ctor_m09D1936822A63545EAB8EEBE9D2327B9CB145656,
	CheckSkill_OnStart_m623E049DFC207E9021B6CFF8F21135B99B462500,
	CheckSkill_OnUpdate_m74C47D735A7837E83B099E6AA7E13093973D500B,
	CheckSkill__ctor_mEC0BF1818AC022820899BAE823A310B70988754C,
	Close_OnUpdate_m2180376A33212DF47FEE4275A4147656F1AA2564,
	Close__ctor_mFCE7C28BD03C8F9F7579F06DA9CCC8E4A6DF1622,
	ShowNotification_OnUpdate_mE9113D77B4861183C5626AC2A2765A3228842D7F,
	ShowNotification__ctor_mD3783DAD38280108F59B6D2C2A3D9829F21DC532,
	ShowProgressbar_OnStart_m33D5F7EB7C6D514FACAE881127BDD6C257982768,
	ShowProgressbar_OnUpdate_m7B52403D82A425DC1C37B45F008A9BD07347ED39,
	ShowProgressbar_OnInterrupt_m5FFFD1E02030C3CA0C9134389D72FC31CD4D2547,
	ShowProgressbar__ctor_m3ED0255D36E5991BA0080A17FB103BC5E34BA3F4,
	CraftingTrigger_get_Callbacks_m4E5E0F9C4C5F28EE6168C7BD7062D1B14EF33007,
	CraftingTrigger_Execute_m9FBF9BC4E7F878672D91DCF546C3533AA92B63C8,
	CraftingTrigger_Execute_m9564B10617AED6B3FDFF8EA32FFEA242C09A3B7B,
	CraftingTrigger_Execute_m045DC5CCFE2815CEF745CB2114B01C67A86D127C,
	CraftingTrigger_Execute_m90FF3CC58298921D33F709203B544FC004D28AE8,
	CraftingTrigger_Execute_m1868C3831BD25BA5073D7D41DE8D78DAAFE0472E,
	CraftingTrigger_Start_mD54D97D1CF287651A9AF03E372A5687E83C17F3D,
	CraftingTrigger_OverrideUse_m0E0C64AD64C90AF3FE5E9FF5546AB86C41A123D5,
	CraftingTrigger_Update_mEFDAD961489B387B251DAB89A52CCEE49A453572,
	CraftingTrigger_OnTriggerInterrupted_m6050953FB59D6C257D0A6BF7C1041926942EE671,
	CraftingTrigger_GetCraftingProgress_m4E70A372B2DCF7D6ADFC7D9B8D01F1CD5C3E1633,
	CraftingTrigger_StartCrafting_m5C8BF11A0EC0B7AF9C2477013886EAE602C821A4,
	CraftingTrigger_HasIngredients_mCBED78ACA1ABF6D7A57CF4EBCA561892D592704C,
	CraftingTrigger_StopCrafting_mADF0752F766BEAFBFB182212590979D2667E44BF,
	CraftingTrigger_CraftItems_mF124FE6834334A72E006E5E84175D9156221722C,
	CraftingTrigger_CraftItem_m3FE76B80A298C4764A8874300B7E20F3EC5B3E1F,
	CraftingTrigger_RegisterCallbacks_m046CAFC04522A779EC4A1D9B9B59242FF35D2F6F,
	CraftingTrigger__ctor_mEDA3A766D4ABD7A060D8BD4EEDE04E0ABB3DEB74,
	U3CCraftItemsU3Ed__26__ctor_m05708EC26C38D37AB8EB955B751847FAB7A7A502,
	U3CCraftItemsU3Ed__26_System_IDisposable_Dispose_m82915AB51DD42745B2BF0291F3903587E90F2878,
	U3CCraftItemsU3Ed__26_MoveNext_m68F080A8B767617F4A8F9928C7A06DA3F4FAFCF1,
	U3CCraftItemsU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9ED7A8123703EA3554A6C3957508C98C877F48B1,
	U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_Reset_m03ED5FA780BBAFC1BF44C998C90A7B700D99A7A2,
	U3CCraftItemsU3Ed__26_System_Collections_IEnumerator_get_Current_m8AB92C4E2BE021342710E731BEC348A687EAF066,
	U3CCraftItemU3Ed__27__ctor_m6BE66FA9C0DD8A3C7BA790E9845C78A41C720F09,
	U3CCraftItemU3Ed__27_System_IDisposable_Dispose_mF7F94E569233B993520A310A200FE681D03224E0,
	U3CCraftItemU3Ed__27_MoveNext_m3C26F1B98C75F516ADD48F52812A74DAD27CE1C1,
	U3CCraftItemU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m620CEE326E27BF4715D831A62854CAFC587F85CE,
	U3CCraftItemU3Ed__27_System_Collections_IEnumerator_Reset_m9944ED8FEEFABC00B312E796326D3892ADE1126D,
	U3CCraftItemU3Ed__27_System_Collections_IEnumerator_get_Current_m448E7912969C96DD23A01894022C78EE4501FA94,
	DisplayCursor_DoDisplayCursor_mE001BC466F85BCA385350A2D24DFE44EECD9E2DF,
	DisplayCursor_OnPointerEnter_m4A79CD0D2484DEA066565607C1BDCF1F13CA6759,
	DisplayCursor_OnPointerExit_mD978DCA2C8F91A656E13006EFC1520684EC47AF3,
	DisplayCursor__ctor_m3E7700C0C9A393DDBE38750B1D54C7DC59749916,
	DisplayName_DoDisplayName_m44312D8E254F3449B3853A05BD8A519EAC26CAB8,
	DisplayName_Start_m3D28C80CCA2B9A52390AE774242D64AADE8B9411,
	DisplayName_OnDestroy_mAD6DB80A11770AAD34A06CC3D2AA7F5E5B41E53A,
	DisplayName_OnPointerEnterTrigger_mB357666458E22609DF50777399963E618D702FB7,
	DisplayName_OnPointerExitTrigger_mBFF2FFC0B88DA3195E11BC19E8D5DB1F91363732,
	DisplayName_OnCameInRange_mF855B0EF2217A4AE8AFA833503D25BC220A7EF4A,
	DisplayName_OnTriggerUsed_m078DB5BD1EF568E5562E432ABEECE88961E14240,
	DisplayName_OnTriggerUnUsed_m3AAD02CFEA82F5988C0D6B6278402193A4D76B51,
	DisplayName_OnWentOutOfRange_mEE0AFB53BFB72A2E6F6E829EE1F6237FF3BD9CEC,
	DisplayName__ctor_m7D67D6EC70F9F6F3840C3F5E144EFA4E3BC5819C,
	DisplayTriggerTooltip_Start_m77E33B7BAE28631B98312C53050954CEF53A05C9,
	DisplayTriggerTooltip_Update_mC585F5804E17188C963CC7293A1E04BE006F378A,
	DisplayTriggerTooltip_DoDisplayTooltip_mE264A0BFF96AEDE99C60F567DB2DF0FCF694EAFE,
	DisplayTriggerTooltip_OnDestroy_mC5D495CF4A8F7DE4080977B157704FE2ADCB8E5A,
	DisplayTriggerTooltip_OnWentOutOfRange_m86649A7149BBC91D917978BB9F287C47F81DB96D,
	DisplayTriggerTooltip_OnTriggerUsed_m4A7DCF7469B6F8CBD1E28F3DCFCF271D0469E287,
	DisplayTriggerTooltip__ctor_m562B1C04B07785350E860997B617F6A0CDD8D9FC,
	Trigger_get_PlayerInfo_m6FF58FE6F4591F4D9FC26337676118DBA70A2958,
	Trigger_StartUse_mC9C0B6E4D6BE74CF6D35680356EB1702D866094A,
	Trigger_StartUse_m33331F5147D1E52B9A108723ECAEC33BCFF4FCA9,
	Trigger_StopUse_m13ECB22A5748D3D2F7D9221016C59E4A91287743,
	Trigger_OverrideUse_m8521E2D6B1AD0838FC70A5A78370E1714EEE920C,
	Trigger_DisplayInUse_m2EB6CDB1C607C3ADF2B9573154E2CFF4316CE431,
	Trigger_DisplayOutOfRange_m7629EF16421EE59AF5311FCB7D841B57C07BC3B2,
	NULL,
	NULL,
	Trigger__ctor_m7FEEB7543A276C3265E04DE09570E2F623DAFACE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Trigger2D_CanUse_mDEE6528214B9B5160F62A9DD7EE85D034B9C134A,
	Trigger2D_CreateTriggerCollider_m8A5D438610CD2C287E9038977AAC1A63C62F697C,
	Trigger2D_OnTriggerEnter2D_m2CB7F42E6EE87DF248B0CCE2EDF77AB435BF8195,
	Trigger2D_OnTriggerExit2D_m3B1B2DF520BE915820FE16F2543A42AC58BC963F,
	Trigger2D__ctor_mE816FDFE5F23E6F4477E925A36BC361B15EBBA44,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TriggerTooltip_Show_m7096137670C8921BA2AE6D771BBEDB34DC0E9610,
	TriggerTooltip__ctor_m9199E6CE4D6DCE0CAD89C9EF8D4F116D7903B138,
	VendorTrigger_get_Callbacks_mB95FAB02CD03C9E8F23792FAD2CC369B0D3EE7D4,
	VendorTrigger_Execute_m1FB6E5E6D76E8F3469E026DD3D0889AE6FD8A10B,
	VendorTrigger_Execute_mB468A500B9E7CF8484CD92F319127E19DC46FDE9,
	VendorTrigger_Execute_m7A19E5208044F9DAB59CC7BBCAC04F3FAB2D97A6,
	VendorTrigger_Execute_m59E0B0A0941B968EAFFA62818E5076DABC24345B,
	VendorTrigger_Execute_m8A64AD3388D20CDCA5972CE75639EBDF247B89DB,
	VendorTrigger_Execute_m5B0AD53EA75C3CCFE8A44871437E09612C41FE6A,
	VendorTrigger_Start_m02A2D2094E0EE88D9904BD86C9F0FF78521FB7A7,
	VendorTrigger_OverrideUse_mFD3D591C866682D5ED054F344F0FEADC401C0164,
	VendorTrigger_BuyItem_mBDE414BF84F09670EECB23C15BC94D0701288BC8,
	VendorTrigger_SellItem_m7D80333E0630E8CEEE68ADD4A58B37294A26920F,
	VendorTrigger_OnTriggerUnUsed_m76EC28B4537AB55C61F1F88D32EBBFD5942499AA,
	VendorTrigger_RegisterCallbacks_m6955EFD93658A7827CBD8A50355DCC2EA1450A91,
	VendorTrigger__ctor_m7DF4C19841E1729BD14632240433F7A3B3B3B48C,
	U3CU3Ec__DisplayClass29_0__ctor_m69D42811E74A45807862D31B1B2A830C3766D91E,
	U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__0_m3E4E578EE14C745E917B0945F8A99A4816F03BEC,
	U3CU3Ec__DisplayClass29_0_U3CBuyItemU3Eb__1_m067E57B859F20A200EA30FF708AAC00D845880E2,
	U3CU3Ec__DisplayClass30_0__ctor_m1ECEAC40554536940ACB202A3E0E8C68A8ECED44,
	U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__0_mFB6B707D8C5596F790B68146050394B343394D1D,
	U3CU3Ec__DisplayClass30_0_U3CSellItemU3Eb__1_m890270DE06975B621F379787F02D1F12DFE6C206,
	CurrencySlot_GetDefaultCurrency_mFDDC88693651D35192FFBDF9A99D39174BAD0268,
	CurrencySlot_Repaint_m8CA34D2D0ABEA22B6CCE54E6613D7FCE5C7934A4,
	CurrencySlot_CanAddItem_mB10DC0A23BA3FBBAE36C0CD37828D1076B159722,
	CurrencySlot_CanUse_mBF19A4C1FCC6E79F02EB40DB5E0236AC23EA94DF,
	CurrencySlot__ctor_mB6242077E47CD0F81E7F13A3984FB506710E4FAF,
	ItemContainer_get_Callbacks_m430B92A765395BC06F037E5D92DA379DA226246E,
	ItemContainer_add_OnAddItem_m6F08BB8215855B35F3A4882D4C7199A01E88140F,
	ItemContainer_remove_OnAddItem_mBEB15F3259A72C56DBDCEC44B655E7581862944B,
	ItemContainer_add_OnFailedToAddItem_mF136263709491EE7361ED20778D2ED92098B3AD9,
	ItemContainer_remove_OnFailedToAddItem_m5F5C7AE7262C71144C90C461FD403F761EE1DDF8,
	ItemContainer_add_OnRemoveItem_mAA53246093667369BD96800B5BE7337B49310595,
	ItemContainer_remove_OnRemoveItem_mD1A95A983E701FEF301966422CADED1F6B250712,
	ItemContainer_add_OnFailedToRemoveItem_mFC55C620F984D08E5F652BAA8C9AE802CCD4466F,
	ItemContainer_remove_OnFailedToRemoveItem_m070D8D92D2BED088C68AEBB705DC98A8851405FC,
	ItemContainer_add_OnTryUseItem_m13E4D934B09A83CF7AACC3B3A203381F7CA2A5C6,
	ItemContainer_remove_OnTryUseItem_m357C5DBF2E81B7D8E5BD6145B3FF59F710EB4147,
	ItemContainer_add_OnUseItem_mA3FD14B23D326CFA04E5B49D1E2A32A1158B6265,
	ItemContainer_remove_OnUseItem_m6663E4FF2A9E58EFC9DBBA22B8CEACCDC067728C,
	ItemContainer_add_OnDropItem_m8682C5772C245FA7CBAC60D8003F3E7057F43F01,
	ItemContainer_remove_OnDropItem_m7496A6F4ECCC526C2CE232496525DF8DBDE56A6F,
	ItemContainer_get_UseReferences_m886E9F1BE6FB49CB6B4EEB8037A4AE4E3706EBA6,
	ItemContainer_set_UseReferences_m80BA931B9EF989E037DCE83397D039F00BAF24AB,
	ItemContainer_get_CanDragIn_mF7361583BAC183B9DFF8E3EB97DB497E7A6C67F8,
	ItemContainer_set_CanDragIn_mC70FD0A9623FD6F0129F43DF3558F32D67921102,
	ItemContainer_get_CanDragOut_m3803AB9EB1CDA716A44440A5E7CA8AFF4B160B9E,
	ItemContainer_set_CanDragOut_m44E74550F379F38923AE218C9850B8C0DBB05DD3,
	ItemContainer_get_CanDropItems_m8AD607878A2D4ADF1DC46C3B6E5810D727F09F05,
	ItemContainer_set_CanDropItems_m2F7BC33C1E313ED7E1F2C5842FCEB70AB77489E3,
	ItemContainer_get_CanReferenceItems_m0CC7A6C984D3A27CE66BC448DB20310A6E8F7360,
	ItemContainer_set_CanReferenceItems_m25F8B7E970128DE67251742D47E860F8BC34F49F,
	ItemContainer_get_CanSellItems_m80671895B820323E225C7D9929DA7311C45EB12E,
	ItemContainer_set_CanSellItems_m350307587EB5B2CDE647AF1F5144773264FA2EB9,
	ItemContainer_get_CanUseItems_mB1AC2D039716DAFFA59577CE8185DC9977A555FB,
	ItemContainer_set_CanUseItems_m1DCBCF4BC04EBCC69E2008324EC26CBEDD6B4422,
	ItemContainer_get_UseContextMenu_m79696406BD3011BFBC8588C7034142284BBB1EEE,
	ItemContainer_set_UseContextMenu_mF2A96A125DA52A9B23D8E48C0B5542FCD6AB3A0C,
	ItemContainer_get_ShowTooltips_mFFE1A9C51F3ADD3B86B60FB6FA6D23AB038EE375,
	ItemContainer_set_ShowTooltips_mA7BBA24457B7E7777A121849DF0D00C935B1C50D,
	ItemContainer_get_MoveUsedItem_m05583740F34FE3FB7C0114BA4CB8CA966491C42A,
	ItemContainer_set_MoveUsedItem_m7C405EF9D91A0ED97B7958D377D1C182611A4358,
	ItemContainer_get_Slots_m9ABC844E225686950EF23CA6912E271A7A4683F0,
	ItemContainer_set_Collection_mF7D8D5129842D984B685408C10F6627A7EEDD339,
	ItemContainer_OnAwake_m4D7A994C8B452D91B7446413226F3FB8B88A143D,
	ItemContainer_Show_m82BD26DA543040AF783F1013F38DE05857B7ABA0,
	ItemContainer_StackOrSwap_m9C36637BD2BC2CEAD905F331DB8549A8D7640464,
	ItemContainer_SwapItems_mD4A389472FD3F7511C484E09AC444A367D6701B4,
	ItemContainer_CanSwapItems_mBADFF21BAF22099B01618F2D744FE6AAF66D8339,
	ItemContainer_StackOrAdd_m32ED8FC2BD0F2833A2CEE97DD79471D58F6249B7,
	ItemContainer_StackOrAdd_m9D3889D6867733E0E222EE25708BA4B33B1FCE7F,
	ItemContainer_AddItem_m45707A58E18CA42C00FE8FDE649778172EAF362F,
	ItemContainer_StackItem_m0963DCA3CA10E58EA02D71DD957FDD397BD0B58F,
	ItemContainer_CanMoveItems_mFDCA0E96A61ED16519CD3010EE12EBE389EE94A4,
	ItemContainer_CanAddItem_m89559D55F36E091775E9489CF0BDF9A2F135D7C5,
	ItemContainer_CanAddItem_m988BAB0CF27817603A8795FCFC83650A04EE7F0F,
	ItemContainer_CanAddItem_m0294FFF413FA53B672AE27EBC531C731EC52970E,
	ItemContainer_ReplaceItem_mB81F990D28D42425DF196360278BF315F5D1B01A,
	ItemContainer_RemoveItem_m442184B9A27E6793D49DA2E8FF6F160282721CAD,
	ItemContainer_RemoveItem_m1B55B66940BE4ADA95F61D11DED9016A4DF9E163,
	ItemContainer_RemoveItem_mC338A9FEC1C841F5C636E25A03454B551FAE56F2,
	ItemContainer_RemoveItems_mFDC958EDE4ABDCE610E65D02A3A22FBFBEE3571B,
	ItemContainer_HasItem_mA2C88A92AD22193C2A0874E9ECB28C99604C18A0,
	ItemContainer_HasItem_mF82011BE226C2A8997F155147ADE9EEC708194B7,
	ItemContainer_HasCategoryItem_m37BE48F329E6CA802B851C1316C5B3AA3AA9BDAB,
	ItemContainer_GetItems_m56C723978EE49119738E4B277CE15FF5220E319C,
	NULL,
	ItemContainer_GetItems_m4246D4036F30A13FC363DBE809B76F598B1C3465,
	ItemContainer_GetRequiredSlots_m891D63411C34176E8571DD95E0DFA60E68ED62F8,
	ItemContainer_GetSlots_m3B60B2100FCEA634134CD298B06FE49006421008,
	ItemContainer_RefreshSlots_m0815EAC8814A18767A1000AC6EC4B7B184FEF5E0,
	NULL,
	ItemContainer_GetSlots_m1BBEC714F27C3B8F039218A24A3461D9D365BF21,
	ItemContainer_CreateSlot_m305D490944D3BB4EC741749A056BF55F5376D665,
	ItemContainer_DestroySlot_m6B23DA272D26997718FD307344F9E7C014C7569D,
	ItemContainer_CanStack_m27BD3EC3A922F7ABDFF2D1722D9198FDED130FF8,
	ItemContainer_CanStack_m07BB90DD5837266FDBC51D8E72B6B31060EB6121,
	ItemContainer_ShowByCategory_m1E4F918BA029988F630B43C7B7293667BCF1BA9D,
	ItemContainer_TryConvertCurrency_m9932F8E0B77686C448C60461AC44F5C7D8D94124,
	ItemContainer_TryRemove_m27A91D490DE767F09B5BC54BD2FD0A5BAFD84AE9,
	ItemContainer_ConvertToSmallestCurrency_m67222F3830855C2301586FB78C5D4F40FE07E962,
	ItemContainer_RegisterCallbacks_mEDCC158D6B34407B36B593876DF13976EDB40B98,
	ItemContainer_RemoveItems_m48224E4FD9B0BE8B9ADEFC803E4080CD907688E8,
	ItemContainer_RemoveItem_mDA2FB6E407FAF92E373B33E88430BE233D57A534,
	ItemContainer_RemoveItemCompletely_m84F8F228DD2ADCF5E5CAA66CD9BAB2CA12B8AA5F,
	ItemContainer_RemoveItemReferences_m4D22DBAC832D19C7B74A168066EB0894153AC8A3,
	ItemContainer_HasItem_mF5A07B865CF19628171AC8C1761A164A7439E8A0,
	ItemContainer_HasCategoryItem_m28B9FBB7E233963FBEF653D3624B9335EBFA70C0,
	ItemContainer_GetItem_m74A975B1494BCBBB4F08A4CC0445A37DFCDEA0C7,
	ItemContainer_GetItemAmount_m0C6626D41270D4933B48CCC8559126F5F145BB3F,
	ItemContainer_AddItems_m2DAD47F2EF7EB7117495BDF84D4D410E3434D6D2,
	ItemContainer_AddItem_mE2B47D0AE7869FAC38817EB4B2E3288157FEC3DA,
	ItemContainer_CanAddItems_m5FF40693304EEA74AE375447E04224E356F70729,
	ItemContainer_CanAddItem_m4AA9BE43B8DE4F0B6224A5F5C169F4B4066EC8D2,
	ItemContainer_CanAddItem_m5CFF98E13151728E3D68DDE01A2EF7651DD36C32,
	ItemContainer_Cooldown_m53D9A1344878735EC32DC68DE280F6608792BA58,
	ItemContainer_CooldownSlots_m106B1D6234D9368B78592EA69B2E7D9C582CBF04,
	ItemContainer_OnDrop_mB8CC55EC8055A1A2AE97600831EEE38529841F64,
	ItemContainer_NotifyDropItem_m3A11A27C4E289FA6E7C2069BDA6011CEC778F7DC,
	ItemContainer_NotifyUseItem_m5C8B71756A4C9D90F7DA9E90B9FD56AA91E3FA0D,
	ItemContainer_NotifyTryUseItem_m97458D79157A300D98B62BAC11892BBE0FFEE3B9,
	ItemContainer_NotifyAddItem_m40EFB8770013D08E11B855846DAAE55ABE064A17,
	ItemContainer_NotifyRemoveItem_m535AF19F9A2BD14B9F3066C94EC035F05F32EDFA,
	ItemContainer_MoveTo_m0E08B60485E79E7DA4070AD1FB802F50BDBD47E9,
	ItemContainer__ctor_m3DFFCA54118261C90245DCBB2EE83F97E3B134DF,
	ItemContainer_U3CRefreshSlotsU3Eb__107_0_mF6C72F5C3896151C109389D48E435DC10859EE32,
	ItemContainer_U3CRefreshSlotsU3Eb__107_1_m6144049BEF060A7BEC093D7A7706BF94A0F327D2,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_0_mCE609769E36DC6BA9AEF19D864F4EE26C44D8B61,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_1_mA462942C146F1C1325A8D9A2C67C34873D517BE7,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_2_m02F2650744A2810F336C05A2A398F9F462B74E47,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_3_m24F684F75E88E23C1511354E2C4E257D1DC4D8AA,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_4_mA33BD64949C76B533C6C857BA3B7566879614A31,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_5_m618FF67AA406E6EDB72AFBE5A98E4C87A159A5D1,
	ItemContainer_U3CRegisterCallbacksU3Eb__118_6_mE1357A708738D310C3CB47D278809DBB3E9A9C6E,
	AddItemDelegate__ctor_mF2268FBBCFA478A58C45517C1834CF8741BEA035,
	AddItemDelegate_Invoke_m17AC8582C1C6B83700F3A8565DF5E2429D2C5639,
	AddItemDelegate_BeginInvoke_m15DEB3D486173B0D743BAC50AFE5259C303D6D9D,
	AddItemDelegate_EndInvoke_m226FB3D152835C80CC2FDEA27756D804500C4BCF,
	FailedToAddItemDelegate__ctor_mD9E18790126B624D7F7A9BB7CCDF3CB30802F599,
	FailedToAddItemDelegate_Invoke_m94F697D87B83FC1B3E34A8B5A692CFDA52A21861,
	FailedToAddItemDelegate_BeginInvoke_m7194A1E8ADF1D6099EF4E5BA25259CF8A9A60100,
	FailedToAddItemDelegate_EndInvoke_mD384609F467F1242B69BBACFAE1C181D44189548,
	RemoveItemDelegate__ctor_m2905BD9846BC2731CF6996336F0CE07888995A69,
	RemoveItemDelegate_Invoke_m2ECEC5C5550F7D87FA9A91773D0C2D9A3FDE7954,
	RemoveItemDelegate_BeginInvoke_m322D3DDC0FA1B4549A78FAFB7CA152F0B5B4F534,
	RemoveItemDelegate_EndInvoke_m4A9B4D2CAD1806CFBFE7BC4345BE2024751E10CE,
	FailedToRemoveItemDelegate__ctor_m622BB21F6F97E0EA79643BAB94171FAE3C2CDBAD,
	FailedToRemoveItemDelegate_Invoke_m8619D6D8F748159DF1414F9564F2E8B356C109F1,
	FailedToRemoveItemDelegate_BeginInvoke_mE77F6610C7253ED04B62D97D35B2FABBF393BF55,
	FailedToRemoveItemDelegate_EndInvoke_m8AB2E1F3632FC60922E844770AF1B575AFAD6D6A,
	UseItemDelegate__ctor_m93D0DA58F472A45F5917457C5BC91A668B9B0B0F,
	UseItemDelegate_Invoke_m6407B74DD0985B6249634E9E9FD907673466838A,
	UseItemDelegate_BeginInvoke_m591AF4B80D624E5768332949B7ADF353785FEA5B,
	UseItemDelegate_EndInvoke_m3B26098D85EA5622BA629929E36B1ACC0542CA32,
	DropItemDelegate__ctor_m8D123DDC38F650FAABEF63459C433CB1401643E2,
	DropItemDelegate_Invoke_m38B05498F02933153863260A9B367FC55FDCB8CA,
	DropItemDelegate_BeginInvoke_m4F122299CACE8F277D03AA67BBBE857FB8E99A34,
	DropItemDelegate_EndInvoke_m79D54AB9FDDEA9E80C3461AD87A54B6B88949E36,
	MoveItemCondition__ctor_mE201579B85E5F4CE2808AFBAD2E1E2005BE8B13F,
	U3CU3Ec__DisplayClass80_0__ctor_mE8D7F64D411CBAA882074E7457CC70CC3FF971DC,
	U3CU3Ec__DisplayClass80_0_U3Cset_CollectionU3Eb__0_m6095F4B55A92EB379D2DFD8CEB923F8CFCC3BA36,
	U3CU3Ec__cctor_mDC20D61C387E3636652BF5ED4C133D9E70F7A130,
	U3CU3Ec__ctor_m5FF95C64EB459C197C94DF56E962B8CE03869143,
	U3CU3Ec_U3Cset_CollectionU3Eb__80_1_mAB12A5CC4B8AA6AD4537BB28C8FB09EB5E90FEED,
	U3CU3Ec_U3CSwapItemsU3Eb__84_0_mC6E4C294ADB82B1396F0023AC55DC4B1C4EBC262,
	U3CU3Ec_U3CSwapItemsU3Eb__84_1_m85A1FBD9BBD42C21335B9B2F158E79B7FA1F3D9A,
	U3CU3Ec_U3CSwapItemsU3Eb__84_2_m436D6230E4C5F79F36B0867C5A390A29854601C4,
	U3CU3Ec_U3CSwapItemsU3Eb__84_3_mCED3312C00C1B6DC60599E3B57C604529D615510,
	U3CU3Ec_U3CCanSwapItemsU3Eb__85_0_m0941039A9F18E51D24C76A204EA65838C6E8CC86,
	U3CU3Ec_U3CCanSwapItemsU3Eb__85_1_m2D1B4223F998B672A71DE00428E7B2387BC6B373,
	U3CU3Ec_U3CCanSwapItemsU3Eb__85_2_m7AFD6B3BE23F8D54469ED7A0ACABD2A972DE23AF,
	U3CU3Ec_U3CCanSwapItemsU3Eb__85_3_m8225658ECEF2429A3BA9BA13D07B94853A2B9E79,
	U3CU3Ec_U3CGetItemsU3Eb__102_2_m214099CDE978E2E0EE419C113C13BEA5CF267119,
	U3CU3Ec_U3CGetItemsU3Eb__102_5_mAAD60B056E6C84A1A40B8EF7B8892FD84EAC1390,
	U3CU3Ec_U3CGetItemsU3Eb__104_2_m8BBE48E7E70E9668D9BDED4146EC6E34DAA92EF2,
	U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_0_m4D938E0318E37CB2600A7107E7C121526809B4E9,
	U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_1_m4EB648B1B0DD2AB5CB9DFA52C3E4DAF6A552E338,
	U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_2_m3EDF16A852394877CFE01696DAC17D1EA131B709,
	U3CU3Ec_U3CGetRequiredSlotsU3Eb__105_3_m0064D8984D714BD49AF35DDFAAF6F7EF65F7C041,
	U3CU3Ec__DisplayClass89_0__ctor_m1A71997BCF90B26DA38001C9925544D93A2D7B54,
	U3CU3Ec__DisplayClass89_0_U3CStackItemU3Eb__0_mF52A8D11865660EBD84951A4400D666A53868D5C,
	U3CU3Ec__DisplayClass96_0__ctor_m75FF4066883B13125127A4D0460B9A4C24E37DC0,
	U3CU3Ec__DisplayClass96_0_U3CRemoveItemU3Eb__0_m0A60574A1E1614D6FCACD6B1672E13141DBEE4FF,
	U3CU3Ec__DisplayClass102_0__ctor_m108CEB315C1D77B0F72AA76D244842D6F1CF1C9F,
	U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__0_mBDAB476C9D67ECDE4AD3C6A3B2484ED417B31E31,
	U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__1_mED1368D649E7DED7D196F9813DF74093414C824F,
	U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__3_m729D5B7D3BF765F343BC8745A2DE0B934B66F53B,
	U3CU3Ec__DisplayClass102_0_U3CGetItemsU3Eb__4_m9C9DF7F81EAD43AC830AC1C27D7C773DFDE31595,
	U3CU3Ec__DisplayClass104_0__ctor_m2D4AB0C53651DC47779AFC73EF092FDEDF06F928,
	U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__0_mF0841795DEE68EFFF6B94EAFDFFA23AE33DA1DBE,
	U3CU3Ec__DisplayClass104_0_U3CGetItemsU3Eb__1_m3820BF183A18195071D48E250E91998BE4C7AD9A,
	U3CU3Ec__DisplayClass109_0__ctor_m09A2B3A8E3D9EC8DE701B5F4AC42ED39426AEC82,
	U3CU3Ec__DisplayClass109_0_U3CGetSlotsU3Eb__0_m1C807F6F95479080823BE5C6506FACA76128C795,
	U3CU3Ec__DisplayClass113_0__ctor_mA019AC50A788A394195BFFB149D7BB01088E1740,
	U3CU3Ec__DisplayClass113_0_U3CCanStackU3Eb__0_m9067D6C95F15D9F823B907B14349BD398AE1B4AC,
	U3CU3Ec__DisplayClass115_0__ctor_m05FA62D60598F38F92EB40836F86FD6706ED543D,
	U3CU3Ec__DisplayClass115_0_U3CTryConvertCurrencyU3Eb__0_m3A812DE5CC1E70DBC85E164BCD0263D1A294D6A4,
	ItemSlot_get_IsCooldown_m7E35F4DD053180BECF5E25F464E1A8A03587C6AD,
	ItemSlot_get_dragObject_mF58AED88A7DEE4E8F5CDC1C9B25E96783EA0CBDD,
	ItemSlot_set_dragObject_mB57DD77E62186F3BB6DBC3B5A87F9C4DCE8C7FEB,
	ItemSlot_Start_mAF0FBE259214DE446143EB7F11FDF15E069EF982,
	ItemSlot_Update_mED8E63DA67E01A4A17ECE8F068ACEFDA56FF2E56,
	ItemSlot_Repaint_m2EE1807FAD1BCD52D9810F30C0701D2933719482,
	ItemSlot_OnPointerEnter_m464BB78BD3D158675C8D100F670C52954C66B5B5,
	ItemSlot_DelayTooltip_mCDF50D2E4203BBF294CCD3C362877EF229129E22,
	ItemSlot_OnPointerExit_mCE0336FFD9DF7B229927197EC204432E929E040B,
	ItemSlot_ShowTooltip_m5090E51A37977734F5E23BE0A1278E519ED25975,
	ItemSlot_CloseTooltip_m215C3719174255BF28529F0F82F595A4AC00036C,
	ItemSlot_OnPointerDown_mEBA00BF5D898AE9352301559C9DC11150168038B,
	ItemSlot_OnPointerUp_m5D50C31660CB9BA8682BF241ECAA4658BB48F11C,
	ItemSlot_OnBeginDrag_m157CCE145E9E93311DDA5371C8C6D4D69C495CAD,
	ItemSlot_OnDrag_m175FE7ED9F202DCDCE2D5853B25C45A9BD0905EC,
	ItemSlot_OnEndDrag_mD7F568BC1158D9E8B7CA193B40B37B84CD2B4F59,
	ItemSlot_OnDrop_m2CC269A4B190E024E7F70C8CBA291669187A7175,
	ItemSlot_DropItem_mCD465B68D04A945C0D985768D00879A391FB2FCD,
	ItemSlot_Unstack_mE12057C80077D4F9DE741D5CC629D38D8148C381,
	ItemSlot_Cooldown_m06FA70BB3F074F60AB906F4D5543BE94AF2AFD77,
	ItemSlot_UpdateCooldown_m5F265BCBD8E16ED7427AEE2AF7D19F96B5675DD9,
	ItemSlot_Use_m4D3102D74728C0B889358583164B82494DD48712,
	ItemSlot_CanUse_mD2F94A16AAA5C415B9CC07F8296F0081CC30C1C1,
	ItemSlot__ctor_m51ED946F7977677545F314211358C017760FB5B3,
	DragObject__ctor_m53544633AA81943EDC3A6B5202B1EC8D01E2EDD9,
	U3CDelayTooltipU3Ed__23__ctor_mC3C9A902CAE14C3E35402B4ABC70E985052EC50A,
	U3CDelayTooltipU3Ed__23_System_IDisposable_Dispose_mDBAAF22BCE80C94EFF75167DACF2A5C68702F4B7,
	U3CDelayTooltipU3Ed__23_MoveNext_mEA8769B233EF0085F559D38AD27DD6F5980CE16C,
	U3CDelayTooltipU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE227150E52923041CF13D04F059B3D1DF70AFA12,
	U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_Reset_m9C78DE2E8204062746514122793A3FA0EFBD6F40,
	U3CDelayTooltipU3Ed__23_System_Collections_IEnumerator_get_Current_m646B7B909036413EDA2A5337E19D2265E39CCFE0,
	ItemTooltip_Show_mE22095D2C8D8409FA24F5192E41ED057F0191B90,
	ItemTooltip__ctor_mDC0D3F8CCE819ADA00301BE99EBB49A11A83070C,
	LoadSaveMenu_Start_m815A5760838250E1522912586890545D614F9603,
	LoadSaveMenu_UpdateLoadingStates_m7D1C0E3E74FF2AFEDD210A5CE99C0B1CBFAD11F8,
	LoadSaveMenu_Save_m6D9AA8BFEDA584D5C4CA7F263877B515C6F912FC,
	LoadSaveMenu_CreateSlot_m93CD3DF3C657D3B5B0292A20CF8E8E06D1AD56D9,
	LoadSaveMenu__ctor_m0BCA46D50DF5A1724A10D9C78738B6AD69A0C88C,
	U3CU3Ec__DisplayClass3_0__ctor_m3A2560E9B5E817FC27123B8E0810600A18A27DBF,
	U3CU3Ec__DisplayClass3_0_U3CUpdateLoadingStatesU3Eb__1_mA3F22CDB8B8853D8F30E121C17E1D9340137F869,
	U3CU3Ec__cctor_mD28322611AA857CE92686FB0AF29DFC61C834539,
	U3CU3Ec__ctor_m1E6B4B8589A5ABDFCCF7D8419EA65EA392E063D2,
	U3CU3Ec_U3CUpdateLoadingStatesU3Eb__3_0_mA9313CB46D208854AC5BA7F4708B2461D8D5F765,
	Restriction_Start_mA33943ABC9BB64CE1D92ABEB6C3B27AC77564DFC,
	NULL,
	Restriction__ctor_m73D1492FAA023A4B479FB3202D512777F0586D56,
	SkillSlot_Repaint_m799B29E7C12BC5EA9E57A5E2BC676900B17679CD,
	SkillSlot__ctor_mA4D31FDEA2AE1538E896357AA3F822C3612931F8,
	Slot_get_ObservedItem_mDDEAA9FBEB04AB4754566769C0B2CFD46DA5C438,
	Slot_set_ObservedItem_m6A6A15932BE698DD97D355777537F304756F6B8E,
	Slot_get_IsEmpty_m230D11B22A7AD7070376666C87CFCAB5897B1644,
	Slot_get_Container_mC2A26E3E498B2E8A166464459FA236F2BF7D5F4D,
	Slot_set_Container_mF957BE50CFCFE81C0B2A3689FED3E63A189A9B0A,
	Slot_get_Index_mC035A81C33D92B432421E15673C429D96FEB405D,
	Slot_set_Index_mA161DA540B1A22C9500DB08DA2518C5995BA419F,
	Slot_get_Callbacks_mC864B799AB48906B25FA7BA39D45269F431F668D,
	Slot_Start_mDC2C24E5C799A7A40E6D79093C550E14DD441C73,
	Slot_Repaint_m565CCB140D3ACABE2BC6E626392788C8C6191ED2,
	Slot_Use_m724393A2DBC52D27CAC940BE3CC6679EC6BE37D5,
	Slot_CanUse_m447C22028EEA1FDCCFE6F3C98B10F723E964545A,
	Slot_MoveItem_mE136033AC55CB0324AD82934ABBF8905D24A6A71,
	Slot_CanAddItem_mB368D86612B48F657E358522B539B49807187008,
	Slot__ctor_m107758161035B186D1B80DE3264F0426B8345AEB,
	Slot_U3CStartU3Eb__21_0_m5CD98F68009EA39E8EDB441D9887291DBEE66B4E,
	Slot_U3CStartU3Eb__21_1_mAB216A805AF038A9A2BC7A513F96E17CA95F80D2,
	Slot_U3CStartU3Eb__21_2_m8C54B7225F8484EA7180EBA03E79C8861BA89842,
	Stack_OnAwake_m068165EA1E28AEC47124B266C043708639748285,
	Stack_SetItem_mE4D1190E51600CE03FA689A0DC4B53C5DFA6C55C,
	Stack_Unstack_m3B5A828D94801A2C1E920560844EC5695ACE2736,
	Stack_Cancel_m9205D346C53ADBB202AB11E296C0C20188DF4F25,
	Stack_UpdatePosition_m4EF7999E418124E253DB31309994EAA4F556A6C5,
	Stack_GetCurrentPosition_m9E8E0693C19795114485378D6B97A16C65014155,
	Stack__ctor_m41554333544C4C23A7D0C5F35F1C70F93AFFC217,
	SwapItems_Update_mD301DFFC126C8E7D3F86B1ED94403786613E4764,
	SwapItems__ctor_m810E08131DBC91B024BE900E19A6219736032541,
	Category_CanAddItem_m3AC928E43CFBBD036ED3A8C0FD9870D6113D9A5B,
	Category__ctor_m48E23508BBBCE8D29BA2EA932A50C51CC5A3ED6D,
	EquipmentRegion_CanAddItem_mDDC3EE182634BBE2B1C55CD91E417DD0CF8D8184,
	EquipmentRegion__ctor_m9FCB3D2690A44AD5E994568B4BBEFC5210A6559D,
	U3CU3Ec__cctor_m898FE93565963CDCA4C5FD58A162EFA7481A06E2,
	U3CU3Ec__ctor_m8054FA16482957556832C61FEA0736F1DD6E75A0,
	U3CU3Ec_U3CCanAddItemU3Eb__1_0_mD154A1004E5E1812D5C26031384B4B8F61C243A1,
	Profession_CanAddItem_m0C177965FC8B4D94450AE62568AFEE6C177DBB5E,
	Profession__ctor_mFC2B0D3117342EEA0413D71C8D696C6A80188876,
	Default_get_Name_m118E17DFF5F04E12CCEDF0074225FFB6F1056A4D,
	Default__ctor_m4D51FAA260DCA005BA48C7024C884E8090B9DC5D,
	Input_get_Name_m820946AF7BB254EF759D5081891A9A6051E7A54C,
	Input__ctor_m551ECEBF905B6DFAEF715A45CB3BAA3AA9E72EFD,
	Notifications_get_Name_mC58B822A8DC678841E522FE3CFA16E0D3D986AC6,
	Notifications__ctor_mBBEC089CC8975B176A7211F666DC47C5EBEBDBA6,
	SavingLoading_get_Name_m027FFB1FE517C8822A10DF12189B20EBA2C1714D,
	SavingLoading__ctor_m83C74039610A9CC4095EBED837C0156129D31B0B,
	Settings_get_Name_mE84C149ACA7AEB1C4B847AF628E1761763D352A4,
	Settings_set_Name_m109A8676338159E9DEFD50086681617C7971AD8D,
	Settings__ctor_m7A23B6793F3E326D9C1BB1C3FA7CA1AB3EC4D7E1,
	UI_get_Name_m883362039AF6A1710A3044A654BFC331C86AF242,
	UI_get_notification_m9691D24776B668D433A63FFE54019C20299468D5,
	UI_get_tooltip_mFED99C380C15D406CD8D1B2C243D2FCF6AE4F46F,
	UI_get_sellPriceTooltip_m64A0ECF9C80ED86FD3BE8B0544BAEAD4687A5347,
	UI_get_stack_m33F884EEEE122CDC9EDEC8F40ECF94D9D6613D04,
	UI_get_contextMenu_m06AF481ACFA3CC92B4A5F2E6CC6CAB7E52C9EB28,
	UI__ctor_m60696CFAC3C2B10CD38287BE7401E74B83CB10E6,
	AddItem_OnUpdate_m3922033C93621FC754A637E4F5FF207747C58B80,
	AddItem__ctor_mB1BF65EB51819C0CBDDE9181F6E34CD27442F1F3,
	Cooldown_OnUpdate_mD337C2B941B614A3CD584C4C814AC8ECD6CC8543,
	Cooldown__ctor_mACBE06800D824E87DA739573B84F7DCE895AC10D,
	ReduceStack_OnUpdate_mF0ED6CDED3A98BB63533B9632579222C3520BC11,
	ReduceStack__ctor_mBB0FC6873CA0C83B2B71E53F26F92CC139253EA4,
	RemoveItem_OnUpdate_m12E10651D32AC9E815470E227B1DD1E6B961CA00,
	RemoveItem__ctor_mEABFCED3B63C5B5A5A3389529227FD43587601BA,
	ItemAction__ctor_mC8461D1E34868933D5AF3B6F72EC50F37EA20E8D,
};
static const int32_t s_InvokerIndices[829] = 
{
	2975,
	2975,
	4649,
	2975,
	920,
	1444,
	2448,
	1439,
	2975,
	2975,
	2975,
	2912,
	2975,
	4649,
	2975,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2912,
	2448,
	2975,
	2477,
	2975,
	2477,
	2975,
	2477,
	2975,
	2477,
	2975,
	2477,
	2975,
	2477,
	2975,
	2477,
	2912,
	2448,
	2912,
	2448,
	2862,
	2948,
	2133,
	2975,
	2975,
	2912,
	2448,
	2912,
	2448,
	2975,
	1444,
	920,
	2448,
	2448,
	2975,
	1898,
	2975,
	2975,
	2975,
	2133,
	2975,
	2133,
	2975,
	2477,
	2975,
	2975,
	2941,
	2941,
	2975,
	2975,
	2975,
	2975,
	2975,
	2912,
	2975,
	2975,
	2975,
	2448,
	2448,
	2448,
	2975,
	1898,
	2975,
	2912,
	2941,
	2477,
	2448,
	2448,
	2975,
	2975,
	2941,
	2975,
	2941,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2477,
	2477,
	2434,
	2975,
	2912,
	2448,
	2975,
	2941,
	2477,
	2975,
	2912,
	2975,
	2941,
	2477,
	1771,
	2975,
	2975,
	2912,
	2975,
	2941,
	2477,
	2975,
	2133,
	2133,
	4622,
	4622,
	4622,
	4622,
	4622,
	4622,
	4622,
	-1,
	2912,
	4636,
	2975,
	2975,
	4287,
	1608,
	1903,
	1903,
	4649,
	4566,
	4228,
	4649,
	4566,
	4276,
	4636,
	4518,
	4566,
	4566,
	4481,
	3771,
	3771,
	4566,
	4481,
	4481,
	3758,
	4481,
	3763,
	2975,
	4649,
	-1,
	-1,
	-1,
	4649,
	2975,
	2975,
	2133,
	2133,
	2133,
	2133,
	2133,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2975,
	2448,
	2448,
	2975,
	2975,
	1894,
	1338,
	2897,
	2941,
	2912,
	2912,
	2448,
	1449,
	2133,
	1338,
	2434,
	2975,
	2448,
	2448,
	2975,
	2975,
	4649,
	2975,
	2133,
	2133,
	2975,
	2133,
	2975,
	2975,
	2975,
	2975,
	2975,
	2975,
	2912,
	1898,
	1898,
	2448,
	2975,
	2133,
	2133,
	2133,
	2133,
	2133,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2448,
	2912,
	2448,
	2912,
	2912,
	2912,
	2975,
	2897,
	2975,
	2912,
	2912,
	2448,
	2975,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2912,
	2448,
	2912,
	2912,
	2448,
	4622,
	2912,
	2448,
	2941,
	2477,
	2897,
	2941,
	2912,
	2448,
	2897,
	2912,
	2448,
	2897,
	2434,
	2897,
	2941,
	2912,
	2912,
	2941,
	2948,
	2941,
	2912,
	2912,
	2941,
	2948,
	2912,
	2912,
	2448,
	2912,
	2912,
	2912,
	2448,
	2912,
	2448,
	1444,
	2448,
	1898,
	2912,
	2448,
	2975,
	2912,
	1898,
	2975,
	2448,
	2448,
	2975,
	2975,
	2975,
	2133,
	2975,
	2133,
	2948,
	2484,
	2912,
	2448,
	2941,
	2448,
	2448,
	2975,
	2948,
	2975,
	2975,
	2912,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	-1,
	2448,
	2975,
	2448,
	2448,
	2975,
	2448,
	2975,
	2448,
	2975,
	2448,
	1450,
	1898,
	2975,
	2448,
	2975,
	2448,
	2975,
	2975,
	2975,
	2975,
	2395,
	2941,
	2948,
	2975,
	2975,
	2975,
	2912,
	2508,
	842,
	2448,
	1453,
	2975,
	4649,
	2975,
	2133,
	2912,
	2448,
	2941,
	2862,
	2395,
	2897,
	2434,
	2948,
	2484,
	2948,
	2484,
	2975,
	4276,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	2897,
	2448,
	2975,
	2975,
	2897,
	2975,
	2975,
	2448,
	2897,
	2975,
	2448,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
	2897,
	2975,
	2975,
	2912,
	3647,
	3926,
	3926,
	3647,
	3926,
	2975,
	1147,
	2975,
	2975,
	2948,
	1439,
	1147,
	2448,
	1087,
	1898,
	2975,
	2975,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2477,
	2448,
	2448,
	2975,
	2477,
	2975,
	2975,
	2975,
	2975,
	2448,
	2448,
	2448,
	2448,
	2975,
	2975,
	2975,
	2477,
	2975,
	2448,
	2448,
	2975,
	2912,
	2975,
	2448,
	2975,
	1147,
	2975,
	2975,
	-1,
	-1,
	2975,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2941,
	2975,
	2448,
	2448,
	2975,
	1444,
	1444,
	929,
	1444,
	1444,
	929,
	1444,
	929,
	1444,
	929,
	1444,
	1444,
	2975,
	2912,
	3926,
	3926,
	3647,
	3926,
	3926,
	3647,
	2975,
	1147,
	921,
	921,
	2448,
	2975,
	2975,
	2975,
	2484,
	2434,
	2975,
	2484,
	2434,
	2912,
	2975,
	2133,
	2941,
	2975,
	2912,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2941,
	2477,
	2912,
	2448,
	2975,
	2975,
	1147,
	1147,
	1147,
	2133,
	1147,
	2133,
	2133,
	276,
	1136,
	2133,
	830,
	1077,
	2116,
	1145,
	2133,
	2477,
	1145,
	833,
	2133,
	1898,
	-1,
	1089,
	1088,
	1898,
	2975,
	-1,
	1898,
	2912,
	2434,
	1147,
	2133,
	2448,
	2448,
	839,
	2975,
	2975,
	4280,
	3810,
	4566,
	4566,
	3810,
	4148,
	4069,
	4016,
	3812,
	3812,
	3812,
	3812,
	3573,
	4281,
	1450,
	2448,
	1444,
	1444,
	1444,
	1444,
	920,
	2448,
	2975,
	2133,
	2133,
	1444,
	2448,
	920,
	1439,
	1444,
	1444,
	1444,
	1441,
	1444,
	465,
	2448,
	1441,
	2448,
	756,
	2448,
	1441,
	920,
	249,
	2448,
	1441,
	1439,
	458,
	2448,
	1441,
	1444,
	465,
	2448,
	1441,
	1444,
	465,
	2448,
	2975,
	2975,
	2133,
	4649,
	2975,
	2133,
	2133,
	1898,
	2133,
	1898,
	2133,
	1898,
	2133,
	1898,
	1898,
	1898,
	1898,
	1898,
	1898,
	1898,
	1898,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2133,
	2133,
	2133,
	2975,
	2133,
	2133,
	2975,
	2133,
	2975,
	2133,
	2975,
	2133,
	2941,
	4622,
	4566,
	2975,
	2975,
	2975,
	2448,
	1903,
	2448,
	2975,
	2975,
	2448,
	2448,
	2448,
	2448,
	2448,
	2448,
	2975,
	2975,
	2484,
	2975,
	2975,
	2941,
	2975,
	2448,
	2434,
	2975,
	2941,
	2912,
	2975,
	2912,
	2448,
	2975,
	2975,
	2975,
	2975,
	1898,
	2975,
	2975,
	2975,
	4649,
	2975,
	2133,
	2975,
	2133,
	2975,
	2975,
	2975,
	2912,
	2448,
	2941,
	2912,
	2448,
	2897,
	2434,
	2912,
	2975,
	2975,
	2975,
	2941,
	2941,
	2133,
	2975,
	1444,
	920,
	1444,
	2975,
	2448,
	2975,
	2975,
	2975,
	2970,
	2975,
	2975,
	2975,
	2133,
	2975,
	2133,
	2975,
	4649,
	2975,
	1898,
	2133,
	2975,
	2912,
	2975,
	2912,
	2975,
	2912,
	2975,
	2912,
	2975,
	2912,
	2448,
	2975,
	2912,
	2912,
	2912,
	2912,
	2912,
	2912,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2897,
	2975,
	2975,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x02000027, { 3, 4 } },
	{ 0x06000086, { 0, 3 } },
	{ 0x060001EF, { 7, 4 } },
	{ 0x060001F0, { 11, 4 } },
	{ 0x06000260, { 15, 3 } },
	{ 0x06000265, { 18, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[21] = 
{
	{ (Il2CppRGCTXDataType)2, 933 },
	{ (Il2CppRGCTXDataType)3, 84 },
	{ (Il2CppRGCTXDataType)2, 178 },
	{ (Il2CppRGCTXDataType)2, 934 },
	{ (Il2CppRGCTXDataType)3, 85 },
	{ (Il2CppRGCTXDataType)2, 934 },
	{ (Il2CppRGCTXDataType)1, 634 },
	{ (Il2CppRGCTXDataType)3, 21545 },
	{ (Il2CppRGCTXDataType)2, 292 },
	{ (Il2CppRGCTXDataType)3, 9538 },
	{ (Il2CppRGCTXDataType)1, 292 },
	{ (Il2CppRGCTXDataType)3, 21544 },
	{ (Il2CppRGCTXDataType)2, 291 },
	{ (Il2CppRGCTXDataType)3, 7293 },
	{ (Il2CppRGCTXDataType)1, 291 },
	{ (Il2CppRGCTXDataType)1, 179 },
	{ (Il2CppRGCTXDataType)3, 21789 },
	{ (Il2CppRGCTXDataType)3, 21936 },
	{ (Il2CppRGCTXDataType)1, 180 },
	{ (Il2CppRGCTXDataType)3, 21790 },
	{ (Il2CppRGCTXDataType)3, 21937 },
};
extern const CustomAttributesCacheGenerator g_DevionGames_InventorySystem_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_DevionGames_InventorySystem_CodeGenModule;
const Il2CppCodeGenModule g_DevionGames_InventorySystem_CodeGenModule = 
{
	"DevionGames.InventorySystem.dll",
	829,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	21,
	s_rgctxValues,
	NULL,
	g_DevionGames_InventorySystem_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
