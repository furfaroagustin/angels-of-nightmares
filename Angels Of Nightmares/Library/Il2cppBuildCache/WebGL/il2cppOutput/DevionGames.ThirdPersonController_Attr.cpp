﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// DevionGames.HeaderLineAttribute
struct HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// DevionGames.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DevionGames.EnumFlagsAttribute
struct EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// DevionGames.HeaderLineAttribute
struct HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.HeaderLineAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// DevionGames.InspectorLabelAttribute
struct InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String DevionGames.InspectorLabelAttribute::label
	String_t* ___label_0;
	// System.String DevionGames.InspectorLabelAttribute::tooltip
	String_t* ___tooltip_1;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___label_0)); }
	inline String_t* get_label_0() const { return ___label_0; }
	inline String_t** get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(String_t* value)
	{
		___label_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___label_0), (void*)value);
	}

	inline static int32_t get_offset_of_tooltip_1() { return static_cast<int32_t>(offsetof(InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2, ___tooltip_1)); }
	inline String_t* get_tooltip_1() const { return ___tooltip_1; }
	inline String_t** get_address_of_tooltip_1() { return &___tooltip_1; }
	inline void set_tooltip_1(String_t* value)
	{
		___tooltip_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// DevionGames.MinMaxSliderAttribute
struct MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single DevionGames.MinMaxSliderAttribute::max
	float ___max_0;
	// System.Single DevionGames.MinMaxSliderAttribute::min
	float ___min_1;

public:
	inline static int32_t get_offset_of_max_0() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744, ___max_0)); }
	inline float get_max_0() const { return ___max_0; }
	inline float* get_address_of_max_0() { return &___max_0; }
	inline void set_max_0(float value)
	{
		___max_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744, ___min_1)); }
	inline float get_min_1() const { return ___min_1; }
	inline float* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(float value)
	{
		___min_1 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void DevionGames.HeaderLineAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void DevionGames.MinMaxSliderAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52 (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void DevionGames.InspectorLabelAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void DevionGames.EnumFlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818 (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * __this, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
static void DevionGames_ThirdPersonController_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Instantiate_t7B6B8EAA4FAC3628A2C593DB012D6E5E44C2FCA1_CustomAttributesCacheGenerator_normalizedTime(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void PlaySound_t9CC9C5B6E23673598812B6CB70D285A67B17D39B_CustomAttributesCacheGenerator_volume(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void PlaySound_t9CC9C5B6E23673598812B6CB70D285A67B17D39B_CustomAttributesCacheGenerator_normalizedPlayTime(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_InputName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Activation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Crosshair(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Offset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Distance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnButton(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnSmoothing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_YawLimit(CustomAttributesCache* cache)
{
	{
		MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * tmp = (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 *)cache->attributes[0];
		MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52(tmp, -180.0f, 180.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_PitchLimit(CustomAttributesCache* cache)
{
	{
		MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * tmp = (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 *)cache->attributes[0];
		MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52(tmp, -90.0f, 90.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_VisibilityDelta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomLimit(CustomAttributesCache* cache)
{
	{
		MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 * tmp = (MinMaxSliderAttribute_t67993E08D328E96BE0BAA5714BEE601EE89D5744 *)cache->attributes[0];
		MinMaxSliderAttribute__ctor_mC6A7C542BD6EA9BDA1C9FF826B20756EAE86EE52(tmp, 0.0f, 25.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomSmoothing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_MoveSmoothing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CursorMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ConsumeInputOverUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CollisionLayer(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CollisionRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_OffsetPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Pitch(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Distance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_SpinTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_DontDestroyOnLoad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_Presets(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_LookOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_BodyWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_HeadWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_EyesWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_ClampWeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_FriendlyName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_InputName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_StartType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_StopType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_ConsumeInputOverUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_PauseItemUpdate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_TransitionDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_State(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_CameraPreset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_MoveToTargetInternal_m72F3F436692B580CD4928242CCBB777E42D9F73E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_0_0_0_var), NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_U3CStopMotionU3Eb__55_0_m2EC938B7561925B6F1F570CCA17EDC47640CBAF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_U3CStartMotionU3Eb__56_0_mCAB72567DA15AC8E57D4880F4C11C5365AF9A1B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70__ctor_mE0912DBF67EA22130A0FAC617B41761DFB0103EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_IDisposable_Dispose_m023D24E65C4F951C4520F067C13E7B7359BA5575(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58C1F09B256FCDB31569F143F4E939C24B2CC96D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_Reset_m4FCAF97E4E9531B458C76E05942EFD943AB035A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_get_Current_m015E37275E63AD25EC7633D4814097579FCE2700(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MotionTrigger_t6BBEC884CF3BCEAAB832D84A8CA45697E226B1C5_CustomAttributesCacheGenerator_triggerName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72"), NULL);
	}
}
static void ChangeHeight_t905D60738AFC9A654504BD8E5CEE901F651D2F30_CustomAttributesCacheGenerator_m_HeightAdjustment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChangeSpeed_t5118089473DE95058ADE0E0A61470CF49C8CA842_CustomAttributesCacheGenerator_m_SpeedMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_ClimbMask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MinForwardInput(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MaxHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MaxDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_ExtraForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKLeftHandOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKRightHandOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKWeightSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_UpdateLookAt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Fall_t49CF4157808852BFBCF74A5430D8FE82205E4A83_CustomAttributesCacheGenerator_m_GravityMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Fall_t49CF4157808852BFBCF74A5430D8FE82205E4A83_CustomAttributesCacheGenerator_m_FallMinHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Jump_tFC197EB2852C06B42C0A878B2C69DA70CB9762FC_CustomAttributesCacheGenerator_m_Force(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Jump_tFC197EB2852C06B42C0A878B2C69DA70CB9762FC_CustomAttributesCacheGenerator_m_RecurrenceDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Ladder_tFB5FE7145C735F80E4BED0D48008AE4FD4AD930A_CustomAttributesCacheGenerator_m_TriggerName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Ladder_tFB5FE7145C735F80E4BED0D48008AE4FD4AD930A_CustomAttributesCacheGenerator_Ladder_U3COnStartU3Eb__3_0_mBC4D49695B7E552BC419F620728064B13CD93DF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Push_tF889478B7F8063C81B2A4337CB663F647AA10D50_CustomAttributesCacheGenerator_m_Distance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Push_tF889478B7F8063C81B2A4337CB663F647AA10D50_CustomAttributesCacheGenerator_Push_U3COnStartU3Eb__5_0_mDCFA0375A9026A356B13D8E7B2FF6DD0F030CB0D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomMotion_t6C89423C0C8706A59E0FB7AEB3A19F0013E1D1C0_CustomAttributesCacheGenerator_m_DestinationStates(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SimpleMotion_tAF7F21E7607E3DC4B0EC00487FDA774AA76E3A66_CustomAttributesCacheGenerator_m_RecurrenceDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_HeightOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_OffsetSmoothing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_TriggerName(CustomAttributesCache* cache)
{
	{
		InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 * tmp = (InspectorLabelAttribute_tAC85C19AEE93FE20542C06F8876A10E5455193C2 *)cache->attributes[0];
		InspectorLabelAttribute__ctor_m5D9ED6C1DEA5D564556FF6DB1A936ED3F6F44B6C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Layer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Weight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_DontDestroyOnLoad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_ForwardInput(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[1];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74"), NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_HorizontalInput(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SpeedMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimType(CustomAttributesCache* cache)
{
	{
		EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C * tmp = (EnumFlagsAttribute_tF3A87EC5D0675F3AA5DC43B954B03D9924487F1C *)cache->attributes[0];
		EnumFlagsAttribute__ctor_mA178D406FEC16F31F960BD1789C7D6E8D09A7818(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimInput(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimRotation(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_RotationSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirDampening(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_GroundDampening(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_StepOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SlopeLimit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_GroundLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[1];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73"), NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SkinWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_IdleFriction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_MovementFriction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_StepFriction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirFriction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AudioMixerGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[1];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x6F\x74\x73\x74\x65\x70\x73"), NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_FootstepClips(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_UseChildAnimator(CustomAttributesCache* cache)
{
	{
		HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 * tmp = (HeaderLineAttribute_tE6F6932E81061B29A4CBBF7E7CE820327EE4DA82 *)cache->attributes[0];
		HeaderLineAttribute__ctor_mE46DC7F30DC7D8EE8E2E54C2BF341E910A5F17AC(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x6F\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_ForwardDampTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_HorizontalDampTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_Motions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_ThirdPersonController_U3CAwakeU3Eb__143_0_m69772D712CCC99CE0F597EFE68BC1E028F51ABB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass155_0_t9F14D4FA626DD1ADBB727D3F37A00626B3850010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AimType_tB7F442D7F0EECF1BE6FF92E7B6398408C24758DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_DevionGames_ThirdPersonController_AttributeGenerators[];
const CustomAttributesCacheGenerator g_DevionGames_ThirdPersonController_AttributeGenerators[112] = 
{
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass155_0_t9F14D4FA626DD1ADBB727D3F37A00626B3850010_CustomAttributesCacheGenerator,
	AimType_tB7F442D7F0EECF1BE6FF92E7B6398408C24758DE_CustomAttributesCacheGenerator,
	Instantiate_t7B6B8EAA4FAC3628A2C593DB012D6E5E44C2FCA1_CustomAttributesCacheGenerator_normalizedTime,
	PlaySound_t9CC9C5B6E23673598812B6CB70D285A67B17D39B_CustomAttributesCacheGenerator_volume,
	PlaySound_t9CC9C5B6E23673598812B6CB70D285A67B17D39B_CustomAttributesCacheGenerator_normalizedPlayTime,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Name,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_InputName,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Activation,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Crosshair,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Offset,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_Distance,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnButton,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnSpeed,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_TurnSmoothing,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_YawLimit,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_PitchLimit,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_VisibilityDelta,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomSpeed,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomLimit,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ZoomSmoothing,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_MoveSmoothing,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CursorMode,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_ConsumeInputOverUI,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CollisionLayer,
	CameraSettings_t6C46439A98DC7DC559BC856C67F8EB3E276F5AC4_CustomAttributesCacheGenerator_m_CollisionRadius,
	FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_OffsetPosition,
	FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Pitch,
	FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Distance,
	FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_Speed,
	FocusTarget_tF9E128A51559518B61B95E5DC97DCA7D6543E1C2_CustomAttributesCacheGenerator_m_SpinTarget,
	ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_DontDestroyOnLoad,
	ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_Target,
	ThirdPersonCamera_t09D43219C3DAB183B913B977AD67076C70D1928B_CustomAttributesCacheGenerator_m_Presets,
	CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_LookOffset,
	CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_BodyWeight,
	CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_HeadWeight,
	CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_EyesWeight,
	CharacterIK_tB4382979FA295F1372D647CECED00F2E13B5EF4A_CustomAttributesCacheGenerator_m_ClampWeight,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_FriendlyName,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_InputName,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_StartType,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_StopType,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_ConsumeInputOverUI,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_PauseItemUpdate,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_TransitionDuration,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_State,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_m_CameraPreset,
	MotionTrigger_t6BBEC884CF3BCEAAB832D84A8CA45697E226B1C5_CustomAttributesCacheGenerator_triggerName,
	ChangeHeight_t905D60738AFC9A654504BD8E5CEE901F651D2F30_CustomAttributesCacheGenerator_m_HeightAdjustment,
	ChangeSpeed_t5118089473DE95058ADE0E0A61470CF49C8CA842_CustomAttributesCacheGenerator_m_SpeedMultiplier,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_ClimbMask,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MinForwardInput,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MinHeight,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MaxHeight,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_MaxDistance,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_ExtraForce,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKLeftHandOffset,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKRightHandOffset,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_IKWeightSpeed,
	Climb_t080AB108435FA74736D44EA925AD56088BB8D26E_CustomAttributesCacheGenerator_m_UpdateLookAt,
	Fall_t49CF4157808852BFBCF74A5430D8FE82205E4A83_CustomAttributesCacheGenerator_m_GravityMultiplier,
	Fall_t49CF4157808852BFBCF74A5430D8FE82205E4A83_CustomAttributesCacheGenerator_m_FallMinHeight,
	Jump_tFC197EB2852C06B42C0A878B2C69DA70CB9762FC_CustomAttributesCacheGenerator_m_Force,
	Jump_tFC197EB2852C06B42C0A878B2C69DA70CB9762FC_CustomAttributesCacheGenerator_m_RecurrenceDelay,
	Ladder_tFB5FE7145C735F80E4BED0D48008AE4FD4AD930A_CustomAttributesCacheGenerator_m_TriggerName,
	Push_tF889478B7F8063C81B2A4337CB663F647AA10D50_CustomAttributesCacheGenerator_m_Distance,
	RandomMotion_t6C89423C0C8706A59E0FB7AEB3A19F0013E1D1C0_CustomAttributesCacheGenerator_m_DestinationStates,
	SimpleMotion_tAF7F21E7607E3DC4B0EC00487FDA774AA76E3A66_CustomAttributesCacheGenerator_m_RecurrenceDelay,
	Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_HeightOffset,
	Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_OffsetSmoothing,
	Swim_t2A3EB0847A5F603C03DE67F7C812E54B7BE67C16_CustomAttributesCacheGenerator_m_TriggerName,
	SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Type,
	SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Layer,
	SetLayerWeight_tA0227F7D6B7B1A7FF8798758C6220BE26F02619B_CustomAttributesCacheGenerator_m_Weight,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_DontDestroyOnLoad,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_ForwardInput,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_HorizontalInput,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SpeedMultiplier,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimType,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimInput,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AimRotation,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_RotationSpeed,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirSpeed,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirDampening,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_GroundDampening,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_StepOffset,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SlopeLimit,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_GroundLayer,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_SkinWidth,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_IdleFriction,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_MovementFriction,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_StepFriction,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AirFriction,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_AudioMixerGroup,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_FootstepClips,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_UseChildAnimator,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_ForwardDampTime,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_HorizontalDampTime,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_m_Motions,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_MoveToTargetInternal_m72F3F436692B580CD4928242CCBB777E42D9F73E,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_U3CStopMotionU3Eb__55_0_m2EC938B7561925B6F1F570CCA17EDC47640CBAF2,
	MotionState_t360AF516735229E24BA92E542AE57B5C7E1AFD0A_CustomAttributesCacheGenerator_MotionState_U3CStartMotionU3Eb__56_0_mCAB72567DA15AC8E57D4880F4C11C5365AF9A1B7,
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70__ctor_mE0912DBF67EA22130A0FAC617B41761DFB0103EB,
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_IDisposable_Dispose_m023D24E65C4F951C4520F067C13E7B7359BA5575,
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58C1F09B256FCDB31569F143F4E939C24B2CC96D,
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_Reset_m4FCAF97E4E9531B458C76E05942EFD943AB035A1,
	U3CMoveToTargetInternalU3Ed__70_tB4A849F5C4F5D321389DD76045DA44161F233D84_CustomAttributesCacheGenerator_U3CMoveToTargetInternalU3Ed__70_System_Collections_IEnumerator_get_Current_m015E37275E63AD25EC7633D4814097579FCE2700,
	Ladder_tFB5FE7145C735F80E4BED0D48008AE4FD4AD930A_CustomAttributesCacheGenerator_Ladder_U3COnStartU3Eb__3_0_mBC4D49695B7E552BC419F620728064B13CD93DF3,
	Push_tF889478B7F8063C81B2A4337CB663F647AA10D50_CustomAttributesCacheGenerator_Push_U3COnStartU3Eb__5_0_mDCFA0375A9026A356B13D8E7B2FF6DD0F030CB0D,
	ThirdPersonController_t51F3B8BF862BA45EC2778725B1735D5968E01FA4_CustomAttributesCacheGenerator_ThirdPersonController_U3CAwakeU3Eb__143_0_m69772D712CCC99CE0F597EFE68BC1E028F51ABB3,
	DevionGames_ThirdPersonController_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
