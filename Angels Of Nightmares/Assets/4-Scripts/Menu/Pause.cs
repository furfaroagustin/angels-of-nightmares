using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    [SerializeField]
    private GameObject botonStop;
    [SerializeField]
    private GameObject menuStop;
    [SerializeField]
    private GameObject voluem;
   
    private void Start()
    {
       
    }
    public void Settings()
    {
        Time.timeScale = 0f;
        voluem.SetActive(true);
    }
    public void Stop()
    {
        Time.timeScale = 0f;
        
        botonStop.SetActive(false);
        menuStop.SetActive(true);
        voluem.SetActive(false);
    }
    public void Play()
    {
        Time.timeScale = 1f;
        botonStop.SetActive(true);
        menuStop.SetActive(false);
        voluem.SetActive(false);
    }
    public void Restart()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Quit()
    {
        Debug.Log("Cerrate capo");
        Application.Quit();
    }
    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
